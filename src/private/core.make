ifdef BUILDDIR
THISDIR = $(SRCDIR)/private/core
TARGET_NAME = libcore

ifdef LIB_RUNTIME
OBJDIR_NAME = corelib
TARGET_SUFFIX = lib
DEBUG_SUFFIX = Debug
FILETYPE_SUFFIX = .a
FULLVER_X64 = 
FULLVER_X86 = 
SHORTVER = 
else
OBJDIR_NAME = core
TARGET_SUFFIX = 
DEBUG_SUFFIX = Debug
FILETYPE_SUFFIX = .so
FULLVER_X64 = .1.6.296
FULLVER_X86 = .1.6.296
SHORTVER = .1
endif

TARGET_DEBUG = $(TARGET_NAME)$(TARGET_SUFFIX)$(DEBUG_SUFFIX)$(FILETYPE_SUFFIX)
TARGET_DEBUG_SHORT = $(TARGET_NAME)$(TARGET_SUFFIX)$(DEBUG_SUFFIX)$(FILETYPE_SUFFIX)$(SHORTVER)
TARGET_DEBUG_FULL_X64 = $(TARGET_NAME)$(TARGET_SUFFIX)$(DEBUG_SUFFIX)$(FILETYPE_SUFFIX)$(FULLVER_X64)
TARGET_DEBUG_FULL_X86 = $(TARGET_NAME)$(TARGET_SUFFIX)$(DEBUG_SUFFIX)$(FILETYPE_SUFFIX)$(FULLVER_X86)

TARGET_RELEASE = $(TARGET_NAME)$(TARGET_SUFFIX)$(FILETYPE_SUFFIX)
TARGET_RELEASE_SHORT = $(TARGET_NAME)$(TARGET_SUFFIX)$(FILETYPE_SUFFIX)$(SHORTVER)
TARGET_RELEASE_FULL_X64 = $(TARGET_NAME)$(TARGET_SUFFIX)$(FILETYPE_SUFFIX)$(FULLVER_X64)
TARGET_RELEASE_FULL_X86 = $(TARGET_NAME)$(TARGET_SUFFIX)$(FILETYPE_SUFFIX)$(FULLVER_X86)

INC =  -I. -I./core -I./core/linux -I../include
ifdef LIB_RUNTIME
ifdef CLANG_CC
CFLAGS =  -Wnon-virtual-dtor -Wall -std=c++11 -include "stdafx.h" -D_GNU_SOURCE -D_LIB -DLIB_RUNTIME
else
CFLAGS =  -Wnon-virtual-dtor -Winit-self -Wunreachable-code -Wzero-as-null-pointer-constant -Wall -std=c++11 -Winvalid-pch -include "stdafx.h" -D_GNU_SOURCE -D_LIB -DLIB_RUNTIME
endif
else
ifdef CLANG_CC
CFLAGS =  -Wnon-virtual-dtor -Wall -std=c++11 -fexceptions -fvisibility=hidden -fPIC -include "stdafx.h" -D_GNU_SOURCE -DCORE_EXPORT
else
CFLAGS =  -Wnon-virtual-dtor -Winit-self -Wunreachable-code -Wzero-as-null-pointer-constant -Wall -std=c++11 -fexceptions -fvisibility=hidden -fPIC -Winvalid-pch -include "stdafx.h" -D_GNU_SOURCE -DCORE_EXPORT
endif
endif
RESINC = 
LIBDIR = 
LIB =  -lpthread -lrt
LDFLAGS = 

ifdef CLANG_CC
CFLAGS_DEBUG = $(CFLAGS)
CFLAGS_RELEASE = $(CFLAGS)
LDFLAGS_DEBUG = $(LDFLAGS)
LDFLAGS_RELEASE = $(LDFLAGS)
else
CFLAGS_DEBUG = $(CFLAGS) -pg
CFLAGS_RELEASE = $(CFLAGS)
LDFLAGS_DEBUG = $(LDFLAGS) -pg
LDFLAGS_RELEASE = $(LDFLAGS)
endif

INC_DEBUG_X64 =  $(INC)
CFLAGS_DEBUG_X64 =  $(CFLAGS_DEBUG) -m64 -g -D_DEBUG -DDEBUG
RESINC_DEBUG_X64 =  $(RESINC)
RCFLAGS_DEBUG_X64 =  $(RCFLAGS)
LIBDIR_DEBUG_X64 =  $(LIBDIR)
LIB_DEBUG_X64 = $(LIB)
ifdef LIB_RUNTIME
LDFLAGS_DEBUG_X64 =  $(LDFLAGS_DEBUG) -m64
else
LDFLAGS_DEBUG_X64 =  $(LDFLAGS_DEBUG) -m64 -Wl,-soname,$(TARGET_DEBUG_SHORT)
endif
OBJDIR_DEBUG_X64 = $(BUILDDIR)/x64/obj/debug/$(OBJDIR_NAME)
DEP_DEBUG_X64 = 
OUT_DEBUG_X64 = $(BUILDDIR)/x64/bin/$(TARGET_DEBUG_FULL_X64)

INC_RELEASE_X64 =  $(INC)
CFLAGS_RELEASE_X64 =  $(CFLAGS_RELEASE) -O2 -m64 -DNDEBUG
RESINC_RELEASE_X64 =  $(RESINC)
RCFLAGS_RELEASE_X64 =  $(RCFLAGS)
LIBDIR_RELEASE_X64 =  $(LIBDIR)
LIB_RELEASE_X64 = $(LIB)
ifdef LIB_RUNTIME
LDFLAGS_RELEASE_X64 =  $(LDFLAGS_RELEASE) -s -m64
else
LDFLAGS_RELEASE_X64 =  $(LDFLAGS_RELEASE) -s -m64 -Wl,-soname,$(TARGET_RELEASE_SHORT)
endif
OBJDIR_RELEASE_X64 = $(BUILDDIR)/x64/obj/release/$(OBJDIR_NAME)
DEP_RELEASE_X64 = 
OUT_RELEASE_X64 = $(BUILDDIR)/x64/bin/$(TARGET_RELEASE_FULL_X64)

INC_DEBUG_X86 =  $(INC)
CFLAGS_DEBUG_X86 =  $(CFLAGS_DEBUG) -m32 -g -D_DEBUG -DDEBUG
RESINC_DEBUG_X86 =  $(RESINC)
RCFLAGS_DEBUG_X86 =  $(RCFLAGS)
LIBDIR_DEBUG_X86 =  $(LIBDIR)
LIB_DEBUG_X86 = $(LIB)
ifdef LIB_RUNTIME
LDFLAGS_DEBUG_X86 =  $(LDFLAGS_DEBUG) -m32
else
LDFLAGS_DEBUG_X86 =  $(LDFLAGS_DEBUG) -m32 -Wl,-soname,$(TARGET_DEBUG_SHORT)
endif
OBJDIR_DEBUG_X86 = $(BUILDDIR)/x86/obj/debug/$(OBJDIR_NAME)
DEP_DEBUG_X86 = 
OUT_DEBUG_X86 = $(BUILDDIR)/x86/bin/$(TARGET_DEBUG_FULL_X86)

INC_RELEASE_X86 =  $(INC)
CFLAGS_RELEASE_X86 =  $(CFLAGS_RELEASE) -O2 -m32 -DNDEBUG
RESINC_RELEASE_X86 =  $(RESINC)
RCFLAGS_RELEASE_X86 =  $(RCFLAGS)
LIBDIR_RELEASE_X86 =  $(LIBDIR)
LIB_RELEASE_X86 = $(LIB)
ifdef LIB_RUNTIME
LDFLAGS_RELEASE_X86 =  $(LDFLAGS_RELEASE) -s -m32
else
LDFLAGS_RELEASE_X86 =  $(LDFLAGS_RELEASE) -s -m32 -Wl,-soname,$(TARGET_RELEASE_SHORT)
endif
OBJDIR_RELEASE_X86 = $(BUILDDIR)/x86/obj/release/$(OBJDIR_NAME)
DEP_RELEASE_X86 = 
OUT_RELEASE_X86 = $(BUILDDIR)/x86/bin/$(TARGET_RELEASE_FULL_X86)

OBJ_DEBUG_X64 = $(OBJDIR_DEBUG_X64)/core.o $(OBJDIR_DEBUG_X64)/linux/targetthread.o $(OBJDIR_DEBUG_X64)/thread.o $(OBJDIR_DEBUG_X64)/rtti.o $(OBJDIR_DEBUG_X64)/object.o $(OBJDIR_DEBUG_X64)/memmgr.o $(OBJDIR_DEBUG_X64)/linux/targetplatform.o $(OBJDIR_DEBUG_X64)/linux/targetevent.o $(OBJDIR_DEBUG_X64)/linux/targetcore.o $(OBJDIR_DEBUG_X64)/linux/stdafx.o $(OBJDIR_DEBUG_X64)/hash.o $(OBJDIR_DEBUG_X64)/event.o

OBJ_RELEASE_X64 = $(OBJDIR_RELEASE_X64)/core.o $(OBJDIR_RELEASE_X64)/linux/targetthread.o $(OBJDIR_RELEASE_X64)/thread.o $(OBJDIR_RELEASE_X64)/rtti.o $(OBJDIR_RELEASE_X64)/object.o $(OBJDIR_RELEASE_X64)/memmgr.o $(OBJDIR_RELEASE_X64)/linux/targetplatform.o $(OBJDIR_RELEASE_X64)/linux/targetevent.o $(OBJDIR_RELEASE_X64)/linux/targetcore.o $(OBJDIR_RELEASE_X64)/linux/stdafx.o $(OBJDIR_RELEASE_X64)/hash.o $(OBJDIR_RELEASE_X64)/event.o

OBJ_DEBUG_X86 = $(OBJDIR_DEBUG_X86)/core.o $(OBJDIR_DEBUG_X86)/linux/targetthread.o $(OBJDIR_DEBUG_X86)/thread.o $(OBJDIR_DEBUG_X86)/rtti.o $(OBJDIR_DEBUG_X86)/object.o $(OBJDIR_DEBUG_X86)/memmgr.o $(OBJDIR_DEBUG_X86)/linux/targetplatform.o $(OBJDIR_DEBUG_X86)/linux/targetevent.o $(OBJDIR_DEBUG_X86)/linux/targetcore.o $(OBJDIR_DEBUG_X86)/linux/stdafx.o $(OBJDIR_DEBUG_X86)/hash.o $(OBJDIR_DEBUG_X86)/event.o

OBJ_RELEASE_X86 = $(OBJDIR_RELEASE_X86)/core.o $(OBJDIR_RELEASE_X86)/linux/targetthread.o $(OBJDIR_RELEASE_X86)/thread.o $(OBJDIR_RELEASE_X86)/rtti.o $(OBJDIR_RELEASE_X86)/object.o $(OBJDIR_RELEASE_X86)/memmgr.o $(OBJDIR_RELEASE_X86)/linux/targetplatform.o $(OBJDIR_RELEASE_X86)/linux/targetevent.o $(OBJDIR_RELEASE_X86)/linux/targetcore.o $(OBJDIR_RELEASE_X86)/linux/stdafx.o $(OBJDIR_RELEASE_X86)/hash.o $(OBJDIR_RELEASE_X86)/event.o

all: debug_x64 release_x64 debug_x86 release_x86

clean: clean_debug_x64 clean_release_x64 clean_debug_x86 clean_release_x86

before_debug_x64: 
	test -d $(BUILDDIR)/x64/bin || mkdir -p $(BUILDDIR)/x64/bin
	test -d $(OBJDIR_DEBUG_X64) || mkdir -p $(OBJDIR_DEBUG_X64)
	test -d $(OBJDIR_DEBUG_X64)/linux || mkdir -p $(OBJDIR_DEBUG_X64)/linux

after_debug_x64:
ifdef LIB_RUNTIME
else
	ln -fs $(OUT_DEBUG_X64) $(BUILDDIR)/x64/bin/$(TARGET_DEBUG_SHORT)
	ln -fs $(BUILDDIR)/x64/bin/$(TARGET_DEBUG_SHORT) $(BUILDDIR)/x64/bin/$(TARGET_DEBUG)
endif

debug_x64: before_debug_x64 out_debug_x64 after_debug_x64

out_debug_x64: before_debug_x64 $(OBJ_DEBUG_X64) $(DEP_DEBUG_X64)
ifdef LIB_RUNTIME
	$(AR) rcs $(OUT_DEBUG_X64) $(OBJ_DEBUG_X64)
else
	$(LD) -shared $(LIBDIR_DEBUG_X64) $(OBJ_DEBUG_X64)  -o $(OUT_DEBUG_X64) $(LDFLAGS_DEBUG_X64) $(LIB_DEBUG_X64)
endif

$(OBJDIR_DEBUG_X64)/core.o: $(THISDIR)/core.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/core.cxx -o $(OBJDIR_DEBUG_X64)/core.o

$(OBJDIR_DEBUG_X64)/linux/targetthread.o: $(THISDIR)/linux/targetthread.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/linux/targetthread.cxx -o $(OBJDIR_DEBUG_X64)/linux/targetthread.o

$(OBJDIR_DEBUG_X64)/thread.o: $(THISDIR)/thread.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/thread.cxx -o $(OBJDIR_DEBUG_X64)/thread.o

$(OBJDIR_DEBUG_X64)/rtti.o: $(THISDIR)/rtti.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/rtti.cxx -o $(OBJDIR_DEBUG_X64)/rtti.o

$(OBJDIR_DEBUG_X64)/object.o: $(THISDIR)/object.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/object.cxx -o $(OBJDIR_DEBUG_X64)/object.o

$(OBJDIR_DEBUG_X64)/memmgr.o: $(THISDIR)/memmgr.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/memmgr.cxx -o $(OBJDIR_DEBUG_X64)/memmgr.o

$(OBJDIR_DEBUG_X64)/linux/targetplatform.o: $(THISDIR)/linux/targetplatform.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/linux/targetplatform.cxx -o $(OBJDIR_DEBUG_X64)/linux/targetplatform.o

$(OBJDIR_DEBUG_X64)/linux/targetevent.o: $(THISDIR)/linux/targetevent.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/linux/targetevent.cxx -o $(OBJDIR_DEBUG_X64)/linux/targetevent.o

$(OBJDIR_DEBUG_X64)/linux/targetcore.o: $(THISDIR)/linux/targetcore.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/linux/targetcore.cxx -o $(OBJDIR_DEBUG_X64)/linux/targetcore.o

$(OBJDIR_DEBUG_X64)/linux/stdafx.o: $(THISDIR)/linux/stdafx.cpp
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/linux/stdafx.cpp -o $(OBJDIR_DEBUG_X64)/linux/stdafx.o

$(OBJDIR_DEBUG_X64)/hash.o: $(THISDIR)/hash.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/hash.cxx -o $(OBJDIR_DEBUG_X64)/hash.o

$(OBJDIR_DEBUG_X64)/event.o: $(THISDIR)/event.cxx
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/event.cxx -o $(OBJDIR_DEBUG_X64)/event.o

clean_debug_x64: 
	rm -rf $(OBJDIR_DEBUG_X64)
	rm -rf $(BUILDDIR)/x64/bin

before_release_x64: 
	test -d $(BUILDDIR)/x64/bin || mkdir -p $(BUILDDIR)/x64/bin
	test -d $(OBJDIR_RELEASE_X64) || mkdir -p $(OBJDIR_RELEASE_X64)
	test -d $(OBJDIR_RELEASE_X64)/linux || mkdir -p $(OBJDIR_RELEASE_X64)/linux

after_release_x64: 
ifdef LIB_RUNTIME
else
	ln -fs $(OUT_RELEASE_X64) $(BUILDDIR)/x64/bin/$(TARGET_RELEASE_SHORT)
	ln -fs $(BUILDDIR)/x64/bin/$(TARGET_RELEASE_SHORT) $(BUILDDIR)/x64/bin/$(TARGET_RELEASE)
endif

release_x64: before_release_x64 out_release_x64 after_release_x64

out_release_x64: before_release_x64 $(OBJ_RELEASE_X64) $(DEP_RELEASE_X64)
ifdef LIB_RUNTIME
	$(AR) rcs $(OUT_RELEASE_X64) $(OBJ_RELEASE_X64)
else
	$(LD) -shared $(LIBDIR_RELEASE_X64) $(OBJ_RELEASE_X64)  -o $(OUT_RELEASE_X64) $(LDFLAGS_RELEASE_X64) $(LIB_RELEASE_X64)
endif

$(OBJDIR_RELEASE_X64)/core.o: $(THISDIR)/core.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/core.cxx -o $(OBJDIR_RELEASE_X64)/core.o

$(OBJDIR_RELEASE_X64)/linux/targetthread.o: $(THISDIR)/linux/targetthread.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/linux/targetthread.cxx -o $(OBJDIR_RELEASE_X64)/linux/targetthread.o

$(OBJDIR_RELEASE_X64)/thread.o: $(THISDIR)/thread.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/thread.cxx -o $(OBJDIR_RELEASE_X64)/thread.o

$(OBJDIR_RELEASE_X64)/rtti.o: $(THISDIR)/rtti.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/rtti.cxx -o $(OBJDIR_RELEASE_X64)/rtti.o

$(OBJDIR_RELEASE_X64)/object.o: $(THISDIR)/object.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/object.cxx -o $(OBJDIR_RELEASE_X64)/object.o

$(OBJDIR_RELEASE_X64)/memmgr.o: $(THISDIR)/memmgr.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/memmgr.cxx -o $(OBJDIR_RELEASE_X64)/memmgr.o

$(OBJDIR_RELEASE_X64)/linux/targetplatform.o: $(THISDIR)/linux/targetplatform.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/linux/targetplatform.cxx -o $(OBJDIR_RELEASE_X64)/linux/targetplatform.o

$(OBJDIR_RELEASE_X64)/linux/targetevent.o: $(THISDIR)/linux/targetevent.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/linux/targetevent.cxx -o $(OBJDIR_RELEASE_X64)/linux/targetevent.o

$(OBJDIR_RELEASE_X64)/linux/targetcore.o: $(THISDIR)/linux/targetcore.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/linux/targetcore.cxx -o $(OBJDIR_RELEASE_X64)/linux/targetcore.o

$(OBJDIR_RELEASE_X64)/linux/stdafx.o: $(THISDIR)/linux/stdafx.cpp
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/linux/stdafx.cpp -o $(OBJDIR_RELEASE_X64)/linux/stdafx.o

$(OBJDIR_RELEASE_X64)/hash.o: $(THISDIR)/hash.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/hash.cxx -o $(OBJDIR_RELEASE_X64)/hash.o

$(OBJDIR_RELEASE_X64)/event.o: $(THISDIR)/event.cxx
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/event.cxx -o $(OBJDIR_RELEASE_X64)/event.o

clean_release_x64: 
	rm -rf $(OBJDIR_RELEASE_X64)
	rm -rf $(BUILDDIR)/x64/bin

before_debug_x86: 
	test -d $(BUILDDIR)/x86/bin || mkdir -p $(BUILDDIR)/x86/bin
	test -d $(OBJDIR_DEBUG_X86) || mkdir -p $(OBJDIR_DEBUG_X86)
	test -d $(OBJDIR_DEBUG_X86)/linux || mkdir -p $(OBJDIR_DEBUG_X86)/linux

after_debug_x86: 
ifdef LIB_RUNTIME
else
	ln -fs $(OUT_DEBUG_X86) $(BUILDDIR)/x86/bin/$(TARGET_DEBUG_SHORT)
	ln -fs $(BUILDDIR)/x86/bin/$(TARGET_DEBUG_SHORT) $(BUILDDIR)/x86/bin/$(TARGET_DEBUG)
endif

debug_x86: before_debug_x86 out_debug_x86 after_debug_x86

out_debug_x86: before_debug_x86 $(OBJ_DEBUG_X86) $(DEP_DEBUG_X86)
ifdef LIB_RUNTIME
	$(AR) rcs $(OUT_DEBUG_X86) $(OBJ_DEBUG_X86)
else
	$(LD) -shared $(LIBDIR_DEBUG_X86) $(OBJ_DEBUG_X86)  -o $(OUT_DEBUG_X86) $(LDFLAGS_DEBUG_X86) $(LIB_DEBUG_X86)
endif

$(OBJDIR_DEBUG_X86)/core.o: $(THISDIR)/core.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/core.cxx -o $(OBJDIR_DEBUG_X86)/core.o

$(OBJDIR_DEBUG_X86)/linux/targetthread.o: $(THISDIR)/linux/targetthread.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/linux/targetthread.cxx -o $(OBJDIR_DEBUG_X86)/linux/targetthread.o

$(OBJDIR_DEBUG_X86)/thread.o: $(THISDIR)/thread.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/thread.cxx -o $(OBJDIR_DEBUG_X86)/thread.o

$(OBJDIR_DEBUG_X86)/rtti.o: $(THISDIR)/rtti.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/rtti.cxx -o $(OBJDIR_DEBUG_X86)/rtti.o

$(OBJDIR_DEBUG_X86)/object.o: $(THISDIR)/object.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/object.cxx -o $(OBJDIR_DEBUG_X86)/object.o

$(OBJDIR_DEBUG_X86)/memmgr.o: $(THISDIR)/memmgr.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/memmgr.cxx -o $(OBJDIR_DEBUG_X86)/memmgr.o

$(OBJDIR_DEBUG_X86)/linux/targetplatform.o: $(THISDIR)/linux/targetplatform.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/linux/targetplatform.cxx -o $(OBJDIR_DEBUG_X86)/linux/targetplatform.o

$(OBJDIR_DEBUG_X86)/linux/targetevent.o: $(THISDIR)/linux/targetevent.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/linux/targetevent.cxx -o $(OBJDIR_DEBUG_X86)/linux/targetevent.o

$(OBJDIR_DEBUG_X86)/linux/targetcore.o: $(THISDIR)/linux/targetcore.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/linux/targetcore.cxx -o $(OBJDIR_DEBUG_X86)/linux/targetcore.o

$(OBJDIR_DEBUG_X86)/linux/stdafx.o: $(THISDIR)/linux/stdafx.cpp
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/linux/stdafx.cpp -o $(OBJDIR_DEBUG_X86)/linux/stdafx.o

$(OBJDIR_DEBUG_X86)/hash.o: $(THISDIR)/hash.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/hash.cxx -o $(OBJDIR_DEBUG_X86)/hash.o

$(OBJDIR_DEBUG_X86)/event.o: $(THISDIR)/event.cxx
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/event.cxx -o $(OBJDIR_DEBUG_X86)/event.o

clean_debug_x86: 
	rm -rf $(OBJDIR_DEBUG_X86)
	rm -rf $(BUILDDIR)/x86/bin

before_release_x86: 
	test -d $(BUILDDIR)/x86/bin || mkdir -p $(BUILDDIR)/x86/bin
	test -d $(OBJDIR_RELEASE_X86) || mkdir -p $(OBJDIR_RELEASE_X86)
	test -d $(OBJDIR_RELEASE_X86)/linux || mkdir -p $(OBJDIR_RELEASE_X86)/linux

after_release_x86: 
ifdef LIB_RUNTIME
else
	ln -fs $(OUT_RELEASE_X86) $(BUILDDIR)/x86/bin/$(TARGET_RELEASE_SHORT)
	ln -fs $(BUILDDIR)/x86/bin/$(TARGET_RELEASE_SHORT) $(BUILDDIR)/x86/bin/$(TARGET_RELEASE)
endif

release_x86: before_release_x86 out_release_x86 after_release_x86

out_release_x86: before_release_x86 $(OBJ_RELEASE_X86) $(DEP_RELEASE_X86)
ifdef LIB_RUNTIME
	$(AR) rcs $(OUT_RELEASE_X86) $(OBJ_RELEASE_X86)
else
	$(LD) -shared $(LIBDIR_RELEASE_X86) $(OBJ_RELEASE_X86)  -o $(OUT_RELEASE_X86) $(LDFLAGS_RELEASE_X86) $(LIB_RELEASE_X86)
endif

$(OBJDIR_RELEASE_X86)/core.o: $(THISDIR)/core.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/core.cxx -o $(OBJDIR_RELEASE_X86)/core.o

$(OBJDIR_RELEASE_X86)/linux/targetthread.o: $(THISDIR)/linux/targetthread.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/linux/targetthread.cxx -o $(OBJDIR_RELEASE_X86)/linux/targetthread.o

$(OBJDIR_RELEASE_X86)/thread.o: $(THISDIR)/thread.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/thread.cxx -o $(OBJDIR_RELEASE_X86)/thread.o

$(OBJDIR_RELEASE_X86)/rtti.o: $(THISDIR)/rtti.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/rtti.cxx -o $(OBJDIR_RELEASE_X86)/rtti.o

$(OBJDIR_RELEASE_X86)/object.o: $(THISDIR)/object.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/object.cxx -o $(OBJDIR_RELEASE_X86)/object.o

$(OBJDIR_RELEASE_X86)/memmgr.o: $(THISDIR)/memmgr.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/memmgr.cxx -o $(OBJDIR_RELEASE_X86)/memmgr.o

$(OBJDIR_RELEASE_X86)/linux/targetplatform.o: $(THISDIR)/linux/targetplatform.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/linux/targetplatform.cxx -o $(OBJDIR_RELEASE_X86)/linux/targetplatform.o

$(OBJDIR_RELEASE_X86)/linux/targetevent.o: $(THISDIR)/linux/targetevent.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/linux/targetevent.cxx -o $(OBJDIR_RELEASE_X86)/linux/targetevent.o

$(OBJDIR_RELEASE_X86)/linux/targetcore.o: $(THISDIR)/linux/targetcore.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/linux/targetcore.cxx -o $(OBJDIR_RELEASE_X86)/linux/targetcore.o

$(OBJDIR_RELEASE_X86)/linux/stdafx.o: $(THISDIR)/linux/stdafx.cpp
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/linux/stdafx.cpp -o $(OBJDIR_RELEASE_X86)/linux/stdafx.o

$(OBJDIR_RELEASE_X86)/hash.o: $(THISDIR)/hash.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/hash.cxx -o $(OBJDIR_RELEASE_X86)/hash.o

$(OBJDIR_RELEASE_X86)/event.o: $(THISDIR)/event.cxx
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/event.cxx -o $(OBJDIR_RELEASE_X86)/event.o

clean_release_x86: 
	rm -rf $(OBJDIR_RELEASE_X86)
	rm -rf $(BUILDDIR)/x86/bin
else
error_build: 
	echo "!!!please use top Makefile to build!!!"
endif

.PHONY: before_debug_x64 after_debug_x64 clean_debug_x64 before_release_x64 after_release_x64 clean_release_x64 before_debug_x86 after_debug_x86 clean_debug_x86 before_release_x86 after_release_x86 clean_release_x86 error_build

