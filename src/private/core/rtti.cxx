// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRTTILinker
CRTTILinker::CRTTILinker(void)
{
}

CRTTILinker::~CRTTILinker(void)
{
}

CRTTILinker::CRTTILinker(const CRTTILinker&)
{
}

CRTTILinker& CRTTILinker::operator=(const CRTTILinker&)
{
    return (*this);
}

bool CRTTILinker::Init(void)
{
    m_RTTIs.SetHash(CContainerTraits::HASHD_SIZE);
    InitType();
    DEV_DUMP(TF("#---RTTI-Linker init okay---"));
    return true;
}

void CRTTILinker::Exit(void)
{
    ExitType();
    DEV_DUMP(TF("#---RTTI-Linker exit okay---"));
}

void CRTTILinker::Link(CRTTI* pRTTI)
{
    CSyncLockScope scope(m_RTTILock);
    if (m_RTTIs.Find(pRTTI->GetName()) == nullptr)
    {
        m_RTTIs.Add(pRTTI->GetName(), pRTTI);
        DEV_DEBUG(TF(" RTTI-Linker Link Class-Name[%s] CRTTI[%p]"), pRTTI->GetName(), pRTTI);
    }
    else
    {
        DEV_DEBUG(TF(" RTTI-Linker Link Class-Name[%s] CRTTI[%p] fail by Name exist"), pRTTI->GetName(), pRTTI);
    }
}

void CRTTILinker::Unlink(CRTTI* pRTTI)
{
    if (pRTTI->GetTraits() & CRTTI::TRAITS_TYPE)
    {
        RemoveType(*pRTTI);
    }
    {
        CSyncLockScope scope(m_RTTILock);
        PINDEX index = m_RTTIs.Find(pRTTI->GetName());
        if ((index != nullptr) && (m_RTTIs[index] == pRTTI))
        {
            m_RTTIs.RemoveAt(index);
            DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s] CRTTI[%p]"), pRTTI->GetName(), pRTTI);
        }
    }
}

CRTTI* CRTTILinker::Find(PCXStr pszName)
{
    CSyncLockScope scope(m_RTTILock);
    PINDEX index = m_RTTIs.Find(pszName);
    if (index != nullptr)
    {
        return m_RTTIs.GetValueAt(index);
    }
    return nullptr;
}

bool CRTTILinker::InitType(void)
{
    m_TypeEnums.SetHash(CContainerTraits::HASHD_SIZE);
    m_TypeVars.SetHash(CContainerTraits::HASHD_SIZE);
    m_TypeFuncs.SetHash(CContainerTraits::HASHD_SIZE);
    return true;
}

void CRTTILinker::ExitType(void)
{
    //CSyncLockScope scope(m_TypeLock);
    DEV_DUMP(TF("*-RTTI-Linker dump, Release Enums = %d, Vars = %d, Funcs = %d"), m_TypeEnums.GetSize(), m_TypeVars.GetSize(), m_TypeFuncs.GetSize());
    PINDEX index = m_TypeEnums.GetFirstIndex();
    while (index != nullptr)
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeEnums.GetNext(index);
        if (m_RTTIs.FindIndex(pPair->m_K) == nullptr)
        {
            continue;
        }
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Enums = %d"), pPair->m_K.GetBuffer(), pPair->m_V.GetSize());
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker exit Class-Name[%s]'s Enum-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
    }
    m_TypeEnums.RemoveAll();

    index = m_TypeVars.GetFirstIndex();
    while (index != nullptr)
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeVars.GetNext(index);
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Vars = %d"), pPair->m_K.GetBuffer(), pPair->m_V.GetSize());
        if (m_RTTIs.FindIndex(pPair->m_K) == nullptr)
        {
            continue;
        }
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker exit Class-Name[%s]'s Var-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
    }
    m_TypeVars.RemoveAll();

    index = m_TypeFuncs.GetFirstIndex();
    while (index != nullptr)
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.GetNext(index);
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Funcs = %d"), pPair->m_K.GetBuffer(), pPair->m_V.GetSize());
        if (m_RTTIs.FindIndex(pPair->m_K) == nullptr)
        {
            continue;
        }
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker exit Class-Name[%s]'s Func-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
    }
    m_TypeFuncs.RemoveAll();
}

bool CRTTILinker::RemoveType(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Types"), rtti.GetName());
    PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Enums = %d"), rtti.GetName(), pPair->m_V.GetSize());
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker Unlink Class-Name[%s]'s Enum-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
        m_TypeEnums.RemoveAt(reinterpret_cast<PINDEX>(pPair));
    }
    pPair = m_TypeVars.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Vars = %d"), rtti.GetName(), pPair->m_V.GetSize());
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker Unlink Class-Name[%s]'s Var-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
        m_TypeVars.RemoveAt(reinterpret_cast<PINDEX>(pPair));
    }
    pPair = m_TypeFuncs.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        DEV_DEBUG(TF(" RTTI-Linker Unlink Class-Name[%s]'s Funcs = %d"), rtti.GetName(), pPair->m_V.GetSize());
        for (PINDEX i = pPair->m_V.GetFirstIndex(); i != nullptr; )
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.GetNext(i);
#ifdef __RUNTIME_DEBUG__
            DEV_DUMP(TF(" RTTI-Linker Unlink Class-Name[%s]'s Func-Type : %s[%p-%x]"), pPair->m_K.GetBuffer(), pType->m_K.GetBuffer(), pType->m_V, pType->m_V->GetType());
#endif  // __RUNTIME_DEBUG__
            MDELETE pType->m_V;
        }
        pPair->m_V.RemoveAll();
        m_TypeFuncs.RemoveAt(reinterpret_cast<PINDEX>(pPair));
    }
    return true;
}

Int CRTTILinker::GetEnumSize(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V.GetSize();
    }
    return 0;
}

Int CRTTILinker::GetVarSize(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeVars.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V.GetSize();
    }
    return 0;
}

Int CRTTILinker::GetFuncSize(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V.GetSize();
    }
    return 0;
}

const CRTTIEnum* CRTTILinker::GetEnum(const CRTTI& rtti, PCXStr pszName)
{
    CSyncLockScope scope(m_TypeLock);
    const CRTTI* pRTTI = &rtti;
    do
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(pRTTI->GetName());
        if (pPair != nullptr)
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.Find(pszName);
            if (pType != nullptr)
            {
                return static_cast<CRTTIEnum*>(pType->m_V);
            }
        }
        pRTTI = pRTTI->GetBase();
    } while (pRTTI != nullptr);

    return nullptr;
}

const CRTTIType* CRTTILinker::GetVar(const CRTTI& rtti, PCXStr pszName)
{
    CSyncLockScope scope(m_TypeLock);
    const CRTTI* pRTTI = &rtti;
    do
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeVars.Find(pRTTI->GetName());
        if (pPair != nullptr)
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.Find(pszName);
            if (pType != nullptr)
            {
                return pType->m_V;
            }
        }
        pRTTI = pRTTI->GetBase();
    } while (pRTTI != nullptr);

    return nullptr;
}

const CRTTIType* CRTTILinker::GetFunc(const CRTTI& rtti, PCXStr pszName)
{
    CSyncLockScope scope(m_TypeLock);
    const CRTTI* pRTTI = &rtti;
    do
    {
        PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.Find(pRTTI->GetName());
        if (pPair != nullptr)
        {
            PAIR_RTTI_TYPE* pType = pPair->m_V.Find(pszName);
            if (pType != nullptr)
            {
                return pType->m_V;
            }
        }
        pRTTI = pRTTI->GetBase();
    } while (pRTTI != nullptr);

    return nullptr;
}

const MAP_RTTI_ENUM& CRTTILinker::GetEnumMap(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V;
    }
    return mc_TypeNone;
}

const MAP_RTTI_VAR& CRTTILinker::GetVarMap(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeVars.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V;
    }
    return mc_TypeNone;
}

const MAP_RTTI_FUNC& CRTTILinker::GetFuncMap(const CRTTI& rtti)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.Find(rtti.GetName());
    if (pPair != nullptr)
    {
        return pPair->m_V;
    }
    return mc_TypeNone;
}

bool CRTTILinker::SetEnumMap(const CRTTI& rtti, MAP_RTTI_ENUM& EnumMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        PINDEX index = m_TypeEnums.Add(rtti.GetName());
        return m_TypeEnums[index].Attach(EnumMap);
    }
    return false;
}

bool CRTTILinker::SetVarMap(const CRTTI& rtti, MAP_RTTI_VAR& VarMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeVars.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        PINDEX index = m_TypeVars.Add(rtti.GetName());
        return m_TypeVars[index].Attach(VarMap);
    }
    return false;
}

bool CRTTILinker::SetFuncMap(const CRTTI& rtti, MAP_RTTI_FUNC& FuncMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        PINDEX index = m_TypeFuncs.Add(rtti.GetName());
        return m_TypeFuncs[index].Attach(FuncMap);
    }
    return false;
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
bool CRTTILinker::SetEnumMap(const CRTTI& rtti, MAP_RTTI_ENUM&& EnumMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeEnums.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        return (m_TypeEnums.Add(rtti.GetName(), std::move(EnumMap)) != nullptr);
    }
    return false;
}

bool CRTTILinker::SetVarMap(const CRTTI& rtti, MAP_RTTI_VAR&& VarMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeVars.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        return (m_TypeVars.Add(rtti.GetName(), std::move(VarMap)) != nullptr);
    }
    return false;
}

bool CRTTILinker::SetFuncMap(const CRTTI& rtti, MAP_RTTI_FUNC&& FuncMap)
{
    CSyncLockScope scope(m_TypeLock);
    PAIR_MAP_REGTYPE* pPair = m_TypeFuncs.Find(rtti.GetName());
    if (pPair == nullptr)
    {
        return (m_TypeFuncs.Add(rtti.GetName(), std::move(FuncMap)) != nullptr);
    }
    return false;
}
#endif

///////////////////////////////////////////////////////////////////
// CRTTI
SELECTANY const CRTTIEnum CRTTI::msc_EnumNone(CObject::ClassRTTI(), TF("EnumNone"), VART_ENUM);

CRTTI* CRTTI::Find(PCXStr pszName)
{
    return GRTTI.Find(pszName);
}

CreateObject* CRTTI::FindCreate(PCXStr pszName)
{
    CRTTI* pRTTI = Find(pszName);
    if (pRTTI != nullptr)
    {
        return pRTTI->GetCreate();
    }
    return nullptr;
}

CObject* CRTTI::CreateByName(PCXStr pszName)
{
    CRTTI* pRTTI = Find(pszName);
    if (pRTTI != nullptr)
    {
        return pRTTI->Create();
    }
    return nullptr;
}

CRTTI::CRTTI(CPCXStr pszName, const CRTTI* pBase, const uintptr_t utTraits, const CreateObject* pCreate, const ReflectType* pReflect)
: m_pszName(pszName)
, m_pBase(pBase)
, m_utTraits(utTraits)
, m_pCreate(pCreate)
{
    GRTTI.Link(this);
    if (pReflect != nullptr)
    {
        pReflect(*this);
    }
}

CRTTI::~CRTTI(void)
{
    GRTTI.Unlink(this);
}

CRTTI::CRTTI(const CRTTI&)
: m_pszName(nullptr)
, m_pBase(nullptr)
, m_utTraits(0)
, m_pCreate(nullptr)
{
}

CRTTI& CRTTI::operator=(const CRTTI&)
{
    return (*this);
}

CPCXStr CRTTI::GetName(void) const
{
    return m_pszName;
}

const CRTTI* CRTTI::GetBase(void) const
{
    return static_cast<const CRTTI*>(m_pBase);
}

const uintptr_t CRTTI::GetTraits(void) const
{
    return m_utTraits;
}

const CreateObject* CRTTI::GetCreate(void) const
{
    return (m_pCreate);
}

const bool CRTTI::IsExactClass(const CRTTI& rtti) const
{
    return (this == &rtti);
}

const bool CRTTI::IsKindOf(const CRTTI &rtti) const
{
    const CRTTI* pRTTI = this;
    while (pRTTI != nullptr)
    {
        if (pRTTI == &rtti)
        {
            return true;
        }
        pRTTI = pRTTI->GetBase();
    }
    return false;
}

CObject* CRTTI::Create(void)
{
    if (m_pCreate != nullptr)
    {
        return (m_pCreate());
    }
    else
    {
        DEV_DEBUG(TF("CRTTI[%s] Trying to create which is not CreateObject"), m_pszName);
    }
    return nullptr;
}

const CRTTIEnum* CRTTI::GetEnum(PCXStr pszName) const
{
    return GRTTI.GetEnum(*this, pszName);
}

const CRTTIEnum* CRTTI::GetEnum(const CString& strName) const
{
    return GRTTI.GetEnum(*this, strName.GetBuffer());
}

const CRTTIType* CRTTI::GetVar(PCXStr pszName) const
{
    return GRTTI.GetVar(*this, pszName);
}

const CRTTIType* CRTTI::GetVar(const CString& strName) const
{
    return GRTTI.GetVar(*this, strName.GetBuffer());
}

const CRTTIType* CRTTI::GetFunc(PCXStr pszName) const
{
    return GRTTI.GetFunc(*this, pszName);
}

const CRTTIType* CRTTI::GetFunc(const CString& strName) const
{
    return GRTTI.GetFunc(*this, strName.GetBuffer());
}

Int CRTTI::GetEnumSize(void) const
{
    return GRTTI.GetEnumSize(*this);
}

Int CRTTI::GetVarSize(void) const
{
    return GRTTI.GetVarSize(*this);
}

Int CRTTI::GetFuncSize(void) const
{
    return GRTTI.GetFuncSize(*this);
}

const MAP_RTTI_ENUM& CRTTI::GetEnumMap(void) const
{
    return GRTTI.GetEnumMap(*this);
}

const MAP_RTTI_VAR& CRTTI::GetVarMap(void) const
{
    return GRTTI.GetVarMap(*this);
}

const MAP_RTTI_FUNC& CRTTI::GetFuncMap(void) const
{
    return GRTTI.GetFuncMap(*this);
}

bool CRTTI::SetEnumMap(MAP_RTTI_ENUM& EnumMap) const
{
    return GRTTI.SetEnumMap(*this, EnumMap);
}

bool CRTTI::SetVarMap(MAP_RTTI_VAR& VarMap) const
{
    return GRTTI.SetVarMap(*this, VarMap);
}

bool CRTTI::SetFuncMap(MAP_RTTI_FUNC& FuncMap) const
{
    return GRTTI.SetFuncMap(*this, FuncMap);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
bool CRTTI::SetEnumMap(MAP_RTTI_ENUM&& EnumMap) const
{
    return GRTTI.SetEnumMap(*this, std::move(EnumMap));
}

bool CRTTI::SetVarMap(MAP_RTTI_VAR&& VarMap) const
{
    return GRTTI.SetVarMap(*this, std::move(VarMap));
}

bool CRTTI::SetFuncMap(MAP_RTTI_FUNC&& FuncMap) const
{
    return GRTTI.SetFuncMap(*this, std::move(FuncMap));
}
#endif

