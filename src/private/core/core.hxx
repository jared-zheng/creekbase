// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CORE_HXX__
#define __CORE_HXX__

#pragma once

#include "rtti.hxx"
#include "hash.hxx"
#include "memmgr.hxx"
#include "platform.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CInitializer
class CInitializer
{
public:
    enum INIT_FLAG
    {
        INIT_FLAG_NONE,
        INIT_FLAG_INIT,
        INIT_FLAG_EXIT,
    };
public:
    static CInitializer* Get(void);
public:
    CInitializer(void);
    ~CInitializer(void);

    bool Init(void);
    bool Exit(void);

    CPlatformImp&   Platform(void);
    CMemManager&    Memory(void);
    CHashCRCImp&    Hash(void);
    CRTTILinker&    RTTI(void);
private:
    CInitializer(const CInitializer&);
    CInitializer& operator=(const CInitializer&);
private:
    UInt             m_uInitFlag;
    CPlatformImp     m_Platform;
    CMemManager      m_MemManager;
    CHashCRCImp      m_HashCRC;
    CRTTILinker      m_RTTILinker;
};

#define GPlatform     CInitializer::Get()->Platform()
#define GMemory       CInitializer::Get()->Memory()
#define GHash         CInitializer::Get()->Hash()
#define GRTTI         CInitializer::Get()->RTTI()

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CORE_HXX__