// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "object.hxx"
#include "event.hxx"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CoreInit
COREAPI bool CoreInit(void)
{
    if (CInitializer::Get()->Init())
    {
        if (SThreadPool::NewInstance() != nullptr)
        {
            SThreadPool::GetInstance()->Init();
        }
        if (SEventManager::NewInstance() != nullptr)
        {
            SEventManager::GetInstance()->Init();
        }
        if (SObjectLinker::NewInstance() != nullptr)
        {
            SObjectLinker::GetInstance()->Init();
        }
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CoreExit
COREAPI void CoreExit(void)
{
    if (CInitializer::Get()->Exit())
    {
        if (SObjectLinker::GetInstance() != nullptr)
        {
            SObjectLinker::GetInstance()->Exit();
            SObjectLinker::DelInstance();
        }
        if (SEventManager::GetInstance() != nullptr)
        {
            SEventManager::GetInstance()->Exit();
            SEventManager::DelInstance();
        }
        if (SThreadPool::GetInstance() != nullptr)
        {
            SThreadPool::GetInstance()->Exit();
            SThreadPool::DelInstance();
        }
    }
}

///////////////////////////////////////////////////////////////////
// CInitializer
CInitializer::CInitializer(void)
: m_uInitFlag(INIT_FLAG_NONE)
{
    m_Platform.Init();
    m_MemManager.Init();
    m_HashCRC.Init();
    m_RTTILinker.Init();
}

CInitializer::~CInitializer(void)
{
    m_RTTILinker.Exit();
    m_HashCRC.Exit();
    m_MemManager.Exit();
    m_Platform.Exit();
}

CInitializer::CInitializer(const CInitializer&)
: m_uInitFlag(INIT_FLAG_NONE)
{
}

CInitializer& CInitializer::operator=(const CInitializer&)
{
    return (*this);
}

bool CInitializer::Init(void)
{
    return (CAtomics::Exchange<UInt>(&m_uInitFlag, INIT_FLAG_INIT) == 0);
}

bool CInitializer::Exit(void)
{
    return (CAtomics::Exchange<UInt>(&m_uInitFlag, INIT_FLAG_EXIT) == INIT_FLAG_INIT);
}

CPlatformImp& CInitializer::Platform(void)
{
    return m_Platform;
}

CMemManager& CInitializer::Memory(void)
{
    return m_MemManager;
}

CHashCRCImp& CInitializer::Hash(void)
{
    return m_HashCRC;
}

CRTTILinker& CInitializer::RTTI(void)
{
    return m_RTTILinker;
}

