// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "atomics.h"
#include "core.hxx"
#include "thread.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CThreadPool
UInt CThreadPool::ms_uStartCount   = 0;
UInt CThreadPool::ms_uRunningCount = 0;
UInt CThreadPool::ms_uStopCount    = 0;
UInt CThreadPool::ms_uKillCount    = 0;

void CThreadPool::DefRoutine(TParam pParam)
{
    CAtomics::Increment<UInt>(&ms_uRunningCount);

    CThread* pThis = reinterpret_cast<CThread*>(pParam);
    if (pThis->GetType() == CThread::THREAD_TYPE_THREAD)
    {
        pThis->Routine();
    }
    else if (pThis->GetType() == CThread::THREAD_TYPE_RUNNABLE)
    {
        (static_cast<CRunnableThread*>(pThis))->Routine();
    }
    else
    {
        assert(pThis->GetType() == CThread::THREAD_TYPE_QUEUETASK);
        (static_cast<CQueueTaskThread*>(pThis))->Routine();
    }
    CAtomics::Decrement<UInt>(&ms_uRunningCount);
    CAtomics::Increment<UInt>(&ms_uStopCount);
}

CThreadPool::CThreadPool(void)
: m_uEnabled(TRUE)
, m_uMaxQueueTaskThread(0)
{
}

CThreadPool::~CThreadPool(void)
{
    assert(m_RunnableThreads.GetSize() == 0);
    assert(m_TaskIdleThreads.GetSize() == 0);
    assert(m_TaskWakeThreads.GetSize() == 0);
    assert(m_QueueTasks.GetSize()      == 0);
}

CThreadPool::CThreadPool(const CThreadPool&)
: m_uEnabled(TRUE)
, m_uMaxQueueTaskThread(0)
{
}

CThreadPool& CThreadPool::operator=(const CThreadPool&)
{
    return (*this);
}

UInt CThreadPool::Init(void)
{
    DEV_DUMP(TF("*-Thread-Pool init okay-"));
    return (UInt)RET_OKAY;
}

void CThreadPool::Exit(void)
{
    CAtomics::Exchange<UInt>(&m_uEnabled, FALSE);
    {
        {
            // clean runnable-thread
            CSyncLockScope scope(m_TPLockTRunnable);
            RemoveRunnableThreads();
        }
        {
            // clean queue task-thread
            CSyncLockScope scope(m_TPLockTQueue);
            RemoveQueueTaskThreads(m_TaskIdleThreads);
            RemoveQueueTaskThreads(m_TaskWakeThreads);
        }
    }
    DEV_DUMP(TF("*-Thread-Pool exit okay, [start: %d, running: %d, stop: %d, kill: %d], Queue Thread : %d, Un-handled QueueTask count : %d-"), ms_uStartCount, ms_uRunningCount, ms_uStopCount, ms_uKillCount, m_uMaxQueueTaskThread, m_QueueTasks.GetSize());
    {
        CSyncLockScope scope(m_TPLockTask);
        RemoveQueueTasks();
    }
}

bool CThreadPool::StartRunnable(CRunnable* pRunnable, Int nPriority, Int nPolicy)
{
    if (CAtomics::Exchange<UInt>(&m_uEnabled, TRUE) == TRUE)
    {
        CRunnableThread* pRunnableThread = nullptr;
        {
            CSyncLockScope scope(m_TPLockTRunnable);
            if ((m_RunnableThreads.GetSize() > 0) && (m_RunnableThreads.GetHead()->IsRunning() == false))
            {
                pRunnableThread = m_RunnableThreads.GetHead();
                m_RunnableThreads.MoveToTail(pRunnableThread->GetIndex());
                DEV_DEBUG(TF("  Select Runnable Thread[%p] running for %p"), pRunnableThread, pRunnable);
            }
        }
        if (pRunnableThread == nullptr)
        {
            pRunnableThread = MNEW CRunnableThread;
            if (pRunnableThread != nullptr)
            {
                pRunnableThread->m_nType = CThread::THREAD_TYPE_RUNNABLE;
                CSyncLockScope scope(m_TPLockTRunnable);
                PINDEX index = m_RunnableThreads.AddTail(pRunnableThread);
                pRunnableThread->SetIndex(index);
                DEV_DEBUG(TF("  Create Runnable Thread[%p] running for %p"), pRunnableThread, pRunnable);
            }
        }
        if (pRunnableThread != nullptr)
        {
            pRunnableThread->Add(pRunnable);
            if (pRunnableThread->Start())
            {
                if (nPolicy != -1)
                {
                    pRunnableThread->SetPriority(nPolicy, nPriority);
                }
                return true;
            }
            else
            {
                pRunnable->Release();

                CSyncLockScope scope(m_TPLockTRunnable);
                m_RunnableThreads.RemoveAt(pRunnableThread->GetIndex());
                MDELETE pRunnableThread;
            }
        }
    }
    return false;
}

void CThreadPool::StopRunnable(CRunnableThread* pThread)
{
    CSyncLockScope scope(m_TPLockTRunnable);
    if (CAtomics::Exchange<UInt>(&m_uEnabled, TRUE) == TRUE)
    {
        if (m_RunnableThreads.GetSize() > THREAD_RUNNABLE_MAX)
        {
            m_RunnableThreads.RemoveAt(pThread->GetIndex());
            MDELETE pThread;
        }
        else
        {
            m_RunnableThreads.MoveToHead(pThread->GetIndex());
        }
        DEV_DEBUG(TF("  Runnable Thread[%p] Stoped"), pThread);
    }
}

bool CThreadPool::RunTask(CQueueTask* pQueueTask)
{
    if (CAtomics::Exchange<UInt>(&m_uEnabled, TRUE) == TRUE)
    {
        if (m_uMaxQueueTaskThread == 0)
        {
            DEV_DEBUG(TF("*-Thread-Pool QueueTask Thread is empty, add QueueTask Thread[default=%d]-"), THREAD_QUEUETASK_INIT);
            SetTaskThread(THREAD_QUEUETASK_INIT);
        }

        pQueueTask->AddRef();

        CQueueTaskThread* pThread = nullptr;
        if (WakeQueueTaskThread(pThread))
        {
            return pThread->Add(pQueueTask);
        }
        else
        {
            CSyncLockScope scope(m_TPLockTask);
            return m_QueueTasks.In(pQueueTask);
        }
    }
    return false;
}

void CThreadPool::FinishTask(CQueueTaskThread* pThread)
{
    DEV_DEBUG(TF("  QueueTaskThread[%p] FinishTask"), pThread);
    if (CAtomics::Exchange<UInt>(&m_uEnabled, TRUE) == TRUE)
    {
        if (CheckQueueTaskThread(pThread))
        {
            CQueueTask* pTask = nullptr;
            if (GetQueueTask(pTask))
            {
                pThread->Add(pTask);
            }
            else
            {
                IdleQueueTaskThread(pThread);
            }
        }
    }
}

void CThreadPool::SetTaskThread(UInt uThreadCount)
{
    if (CAtomics::Exchange<UInt>(&m_uEnabled, TRUE) == TRUE)
    {
        uThreadCount = GPlatform.CheckThread(uThreadCount, THREAD_QUEUETASK_TIMES);
        if (uThreadCount < m_uMaxQueueTaskThread)
        {
            DecrQueueTaskThread(uThreadCount);
        }
        else if (uThreadCount > m_uMaxQueueTaskThread)
        {
            IncrQueueTaskThread(uThreadCount);
        }
    }
}

void CThreadPool::RemoveRunnableThreads(void)
{
    PINDEX index = m_RunnableThreads.GetHeadIndex();
    while (index != nullptr)
    {
        CRunnableThread* pThread = m_RunnableThreads.GetNext(index);
        pThread->Kill();
        MDELETE pThread;
    }
    m_RunnableThreads.RemoveAll();
}

void CThreadPool::RemoveQueueTaskThreads(LST_QUEUETASK_THREAD& LstThreads)
{
    PINDEX index = LstThreads.GetHeadIndex();
    while (index != nullptr)
    {
        CQueueTaskThread* pThread = LstThreads.GetNext(index);
        if (pThread->Stop((UInt)THREAD_WAIT_TIME) == false)
        {
            pThread->Kill();
        }
        MDELETE pThread;
    }
    LstThreads.RemoveAll();
}

void CThreadPool::RemoveQueueTasks(void)
{
    while (m_QueueTasks.IsEmpty() == false)
    {
        CQueueTask* pTask = m_QueueTasks.Out();

        TRY_EXCEPTION
            pTask->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
    }
}

bool CThreadPool::WakeQueueTaskThread(CQueueTaskThread*& pThread)
{
    CSyncLockScope scope(m_TPLockTQueue);
    if (m_TaskIdleThreads.GetSize() > 0)
    {
        pThread = m_TaskIdleThreads.RemoveHead();
        PINDEX index = m_TaskWakeThreads.AddTail(pThread);
        pThread->SetIndex(index);
    }
    return (pThread != nullptr);
}

bool CThreadPool::IdleQueueTaskThread(CQueueTaskThread* pThread)
{
    CSyncLockScope scope(m_TPLockTQueue);
    if (pThread->GetIndex() != nullptr)
    {
        m_TaskWakeThreads.RemoveAt(pThread->GetIndex());
        pThread->SetIndex(nullptr);
    }
    m_TaskIdleThreads.AddTail(pThread);
    return true;
}

bool CThreadPool::CheckQueueTaskThread(CQueueTaskThread* pThread)
{
    CSyncLockScope scope(m_TPLockTQueue);
    if (((UInt)m_TaskWakeThreads.GetSize() > m_uMaxQueueTaskThread) && (m_TaskIdleThreads.GetSize() == 0))
    {
        m_TaskWakeThreads.RemoveAt(pThread->GetIndex());
        if (pThread->Stop((UInt)THREAD_WAIT_TIME) == false)
        {
            pThread->Kill();
        }
        MDELETE pThread;
        DEV_DEBUG(TF("*-Thread-Pool check decr queue-task thread %d, wake=%d"), m_uMaxQueueTaskThread, m_TaskWakeThreads.GetSize());
        return false;
    }
    return true;
}

bool CThreadPool::GetQueueTask(CQueueTask*& pTask)
{
    CSyncLockScope scope(m_TPLockTask);
    if (m_QueueTasks.IsEmpty() == false)
    {
        pTask = m_QueueTasks.Out();
    }
    return (pTask != nullptr);
}

void CThreadPool::DecrQueueTaskThread(UInt uThreadCount)
{
    CSyncLockScope scope(m_TPLockTQueue);

    PINDEX index = m_TaskIdleThreads.GetHeadIndex();
    while ((index != nullptr) && (uThreadCount < m_uMaxQueueTaskThread))
    {
        CQueueTaskThread* pThread = m_TaskIdleThreads.RemoveHead();
        if (pThread->Stop((UInt)THREAD_WAIT_TIME) == false)
        {
            pThread->Kill();
        }
        MDELETE pThread;

        --m_uMaxQueueTaskThread;
        index = m_TaskIdleThreads.GetHeadIndex();
    }
    DEV_DEBUG(TF("*-Thread-Pool decr queue-task thread %d---%d, wake=%d"), m_uMaxQueueTaskThread, uThreadCount, m_TaskWakeThreads.GetSize());
    m_uMaxQueueTaskThread = uThreadCount;
}

void CThreadPool::IncrQueueTaskThread(UInt uThreadCount)
{
    CSyncLockScope scope(m_TPLockTQueue);

    for (UInt i = m_uMaxQueueTaskThread; i < uThreadCount; ++i)
    {
        CQueueTaskThread* pThread = MNEW CQueueTaskThread;
        if (pThread != nullptr)
        {
            pThread->m_nType = CThread::THREAD_TYPE_QUEUETASK;
            if (pThread->Start())
            {
                CQueueTask* pTask = nullptr;
                if (GetQueueTask(pTask))
                {
                    PINDEX index = m_TaskWakeThreads.AddTail(pThread);
                    pThread->SetIndex(index);

                    pThread->Add(pTask);
                }
                else
                {
                    m_TaskIdleThreads.AddTail(pThread);
                }
                ++m_uMaxQueueTaskThread;
            }
            else
            {
                MDELETE pThread;
                DEV_DEBUG(TF("  Start QueueTask Thread Failed!"));
            }
        }
        else
        {
            DEV_DEBUG(TF("  Create QueueTask Thread Failed!"));
        }
    }
}

///////////////////////////////////////////////////////////////////
// CThread
bool CThread::StartRunnable(CRunnable& RunnableRef, Int nPriority, Int nPolicy)
{
    if (SThreadPool::GetInstance() == nullptr)
    {
        return false;
    }
    return SThreadPool::GetInstance()->StartRunnable(&RunnableRef, nPriority, nPolicy);
}

bool CThread::StartRunnable(CRunnablePtr RunnablePtr, Int nPriority, Int nPolicy)
{
    if ((SThreadPool::GetInstance() == nullptr) || (RunnablePtr.Get() == nullptr))
    {
        return false;
    }
    return SThreadPool::GetInstance()->StartRunnable(RunnablePtr, nPriority, nPolicy);
}

bool CThread::QueueTask(CQueueTask& QueueTaskRef)
{
    if (SThreadPool::GetInstance() == nullptr)
    {
        return false;
    }
    return SThreadPool::GetInstance()->RunTask(&QueueTaskRef);
}

bool CThread::QueueTask(CQueueTaskPtr QueueTaskPtr)
{
    if ((SThreadPool::GetInstance() == nullptr) || (QueueTaskPtr.Get() == nullptr))
    {
        return false;
    }
    return SThreadPool::GetInstance()->RunTask(QueueTaskPtr);
}

void CThread::QueueThread(UInt uThreadCount)
{
    if (SThreadPool::GetInstance() != nullptr)
    {
        SThreadPool::GetInstance()->SetTaskThread(uThreadCount);
    }
}

///////////////////////////////////////////////////////////////////
CThread::CThread(void)
: m_ullCheck(0)
, m_uStatus(FALSE) // wait-status
, m_nType(THREAD_TYPE_THREAD)
{
}

CThread::CThread(const CThread&)
: m_ullCheck(0)
, m_uStatus(FALSE) // wait-status
, m_nType(THREAD_TYPE_THREAD)
{
}

CThread& CThread::operator=(const CThread&)
{
    return (*this);
}

bool CThread::Stop(UInt uWait)
{
    if (IsRunning())
    {
        if (Wait(uWait) != (Int)RET_OKAY)
        {
            DEV_DEBUG(TF("  Thread[%p---%#lX] Stop wait %d time-out"), this, m_hThread.m_TId, uWait);
            return false;
        }
    }
    if (m_hThread.IsValid())
    {
        DEV_DEBUG(TF("  Thread Stop Thread[%p---%#lX] succeeded"), this, m_hThread.m_TId);
        m_hThread.Reset();
    }
    return true;
}

void CThread::UpdateStatus(void)
{
    CAtomics::Exchange<ULLong>(&m_ullCheck, (ULLong)(CPlatform::GetRunningTime()));
}

bool CThread::CheckStatus(UInt uTimeout)
{
    if (GetWaitStatus())
    {
        DEV_TRACE(TF("  Thread CheckStatus Thread[%p---%#lX] Wait-Status"), this, m_hThread.m_TId);
        return false;
    }

    ULLong ullCheck = (ULLong)(CPlatform::GetRunningTime());
    DEV_TRACE(TF("  Thread CheckStatus Thread[%p---%#lX] Current %llX > %llX"), this, m_hThread.m_TId, ullCheck, (m_ullCheck + uTimeout));
    return (ullCheck > (CAtomics::CompareExchange<ULLong>(&m_ullCheck, 0, 0) + uTimeout));
}

bool CThread::GetWaitStatus(void)
{
    return (CAtomics::CompareExchange<UInt>(&m_uStatus, 0, 0) != FALSE);
}

void CThread::SetWaitStatus(bool bWait)
{
    CAtomics::Exchange<UInt>(&m_uStatus, bWait ? TRUE : FALSE);
}

void CThread::UpdateWaitStatus(bool bWait)
{
    SetWaitStatus(bWait);
    UpdateStatus();
    DEV_TRACE(TF("  Thread UpdateWaitStatus Thread[%p---%#lX] status = %d, check=%llX"), this, m_hThread.m_TId, m_uStatus, m_ullCheck);
}

TId CThread::GetId(void) const
{
    return m_hThread.m_TId;
}

Int CThread::GetType(void) const
{
    return m_nType;
}

bool CThread::OnStart(void)
{
    return true;
}

void CThread::OnStop(void)
{
}

void CThread::OnKill(void)
{
}

///////////////////////////////////////////////////////////////////
// CRunnableThread
CRunnableThread::CRunnableThread(void)
: m_Index(nullptr)
, m_pRunning(nullptr)
{
}

CRunnableThread::~CRunnableThread(void)
{
}

CRunnableThread::CRunnableThread(const CRunnableThread&)
: m_Index(nullptr)
, m_pRunning(nullptr)
{
}

CRunnableThread& CRunnableThread::operator=(const CRunnableThread&)
{
    return (*this);
}

PINDEX CRunnableThread::GetIndex(void)
{
    return m_Index;
}

void CRunnableThread::SetIndex(PINDEX index)
{
    m_Index = index;
}

bool CRunnableThread::Add(CRunnable* pRunnable)
{
    m_pRunning = pRunnable;
    m_pRunning->AddRef();
    DEV_DEBUG(TF("  RunnableThread[%p] Add Runnable %p"), this, m_pRunning);
    return true;
}

void CRunnableThread::Finish(void)
{
    m_hThread.Reset();
    if (m_pRunning != nullptr)
    {
        TRY_EXCEPTION
            m_pRunning->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
        m_pRunning = nullptr;
    }
    if (SThreadPool::GetInstance() != nullptr)
    {
        SThreadPool::GetInstance()->StopRunnable(this);
    }
}

bool CRunnableThread::OnStart(void)
{
    return m_pRunning->OnStart(GetId());
}

void CRunnableThread::OnStop(void)
{
    m_pRunning->OnStop();
}

void CRunnableThread::OnKill(void)
{
    if (m_pRunning != nullptr)
    {
        TRY_EXCEPTION
            m_pRunning->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
        m_pRunning = nullptr;
    }
}

bool CRunnableThread::Run(void)
{
    return m_pRunning->Run();
}

///////////////////////////////////////////////////////////////////
// CQueueTaskThread
CQueueTaskThread::CQueueTaskThread(void)
: m_Index(nullptr)
, m_pQueueTask(nullptr)
{
}

CQueueTaskThread::~CQueueTaskThread(void)
{
}

CQueueTaskThread::CQueueTaskThread(const CQueueTaskThread&)
: m_Index(nullptr)
, m_pQueueTask(nullptr)
{
}

CQueueTaskThread& CQueueTaskThread::operator=(const CQueueTaskThread&)
{
    return (*this);
}

bool CQueueTaskThread::Stop(UInt uWait)
{
    m_SyncEvent.Signal();
    return CThread::Stop(uWait);
}

PINDEX CQueueTaskThread::GetIndex(void)
{
    return m_Index;
}

void CQueueTaskThread::SetIndex(PINDEX index)
{
    m_Index = index;
}

bool CQueueTaskThread::Add(CQueueTask* pQueueTask)
{
    m_pQueueTask = pQueueTask;
    DEV_DEBUG(TF("  QueueTaskThread[%p] Add Task %p"), this, pQueueTask);
    m_SyncEvent.Signal();
    return true;
}

void CQueueTaskThread::Finish(void)
{
    m_hThread.Reset();
    if (m_pQueueTask != nullptr)
    {
        TRY_EXCEPTION
            m_pQueueTask->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
        m_pQueueTask = nullptr;
    }
    if (SThreadPool::GetInstance() != nullptr)
    {
        SThreadPool::GetInstance()->FinishTask(this);
    }
}

bool CQueueTaskThread::OnStart(void)
{
    return m_SyncEvent.IsValid();
}

void CQueueTaskThread::OnKill(void)
{
    if (m_pQueueTask != nullptr)
    {
        TRY_EXCEPTION
            m_pQueueTask->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
        m_pQueueTask = nullptr;
    }
}

bool CQueueTaskThread::Run(void)
{
    m_pQueueTask->Run(GetId());
    return true;
}

