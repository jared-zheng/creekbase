// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(__GNUC__) // __GNUC__

    static CInitializer GInitializer __attribute__((init_priority (101)));

#else
    #error "Compiler No Implement"
#endif  // __GNUC__

CInitializer* CInitializer::Get(void)
{
    return (&GInitializer);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

