// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "atomics.h"
#include "thread.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CThreadPool
TRet THREAD_FUNC CThreadPool::StaticRoutine(TParam pParam)
{
    DefRoutine(pParam);
    return nullptr;
}

///////////////////////////////////////////////////////////////////
// CThread
typedef void(*DESTR_FUNC)(void*);

UInt CThread::TlsCreateIndex(void* pParam)
{
    pthread_key_t tKey = 0;
    if (pthread_key_create(&tKey, (DESTR_FUNC)pParam) != RET_OKAY)
    {
        return (UInt)INDEX_INVALID;
    }
    return (UInt)tKey;
}

bool CThread::TlsDestroyIndex(UInt uIndex)
{
    return (pthread_key_delete((pthread_key_t)uIndex) == RET_OKAY);
}

bool CThread::TlsSetIndexValue(UInt uIndex, void* pValue)
{
    return (pthread_setspecific((pthread_key_t)uIndex, pValue) == RET_OKAY);
}

void* CThread::TlsGetIndexValue(UInt uIndex)
{
    return pthread_getspecific((pthread_key_t)uIndex);
}
///////////////////////////////////////////////////////////////////
CThread::~CThread(void)
{
    if (IsRunning())
    {
        pthread_cancel(m_hThread.m_TId);
        CAtomics::Decrement<UInt>(&(CThreadPool::ms_uRunningCount));
        CAtomics::Increment<UInt>(&(CThreadPool::ms_uKillCount));
    }
}

bool CThread::Start(const CThreadAttr* pAttr)
{
    if (IsRunning())
    {
        DEV_DEBUG(TF("  Thread[%p---%#lX] Start return false by thread already running"), this, m_hThread.m_TId);
        return false;
    }
    if (m_hThread.IsValid())
    {
        m_hThread.Reset();
    }

    CAtomics::Increment<UInt>(&(CThreadPool::ms_uStartCount));
    Int nRet = RET_OKAY;
    if (pAttr == nullptr)
    {
        nRet = pthread_create(&(m_hThread.m_TId), nullptr, &CThreadPool::StaticRoutine, this);
    }
    else
    {
        nRet = pthread_create(&(m_hThread.m_TId), &(pAttr->m_Attr), &CThreadPool::StaticRoutine, this);
    }
    UpdateStatus();
    DEV_DEBUG(TF("  Thread[%p---%#lX] Start return %d"), this, m_hThread.m_TId, nRet);
    return (nRet == RET_OKAY);
}

bool CThread::GetPriority(Int& nPriority, Int& nPolicy) const
{
    struct sched_param param;
    pthread_getschedparam(m_hThread.m_TId, &nPolicy, &param);
    nPriority = param.__sched_priority;
    return true;
}

bool CThread::SetPriority(Int nPriority, Int nPolicy)
{
    Int nOldPolicy   = 0;
    Int nOldPriority = 0;
    GetPriority(nOldPriority, nOldPolicy);

    if (nPolicy == -1)
    {
        nPriority  = sched_get_priority_max(nOldPolicy);
        nPriority -= sched_get_priority_min(nOldPolicy);
        nPriority /= 2;
    }
    if ((nPolicy != nOldPolicy) || (nPriority != nOldPriority))
    {
        struct sched_param param;
        param.__sched_priority = nPriority;
        return (pthread_setschedparam(m_hThread.m_TId, nPolicy, &param) == RET_OKAY);
    }
    return false;
}

bool CThread::IsRunning(void) const
{
    if (m_hThread.IsValid())
    {
        return (pthread_kill(m_hThread, 0) == RET_OKAY);
    }
    return false;
}

Int CThread::Suspend(bool)
{
    return RET_ERROR;
}

Int CThread::Kill(UInt uCode)
{
    if (uCode > 0)
    {
        return pthread_kill(m_hThread.m_TId, (Int)uCode);
    }
    Int nRet = RET_FAIL;
    if (IsRunning())
    {
        nRet = pthread_cancel(m_hThread.m_TId);
        if (nRet == RET_OKAY)
        {
            DEV_DEBUG(TF("  Thread Cancel Thread[%p---%#lX] succeeded"), this, m_hThread.m_TId);
            CAtomics::Decrement<UInt>(&(CThreadPool::ms_uRunningCount));
            CAtomics::Increment<UInt>(&(CThreadPool::ms_uKillCount));
            m_hThread.Reset();
        }
    }
    if (m_hThread.IsValid())
    {
        m_hThread.Reset();
    }
    if (nRet == RET_OKAY)
    {
        OnKill();
    }
    return RET_OKAY;
}

Int CThread::Wait(UInt uWait, bool)
{
    if (m_hThread.IsValid())
    {
        Int nRet = RET_OKAY;
        if (uWait == (UInt)TIMET_INFINITE)
        {
            nRet = pthread_join(m_hThread.m_TId, nullptr);
        }
        else if (uWait == (UInt)TIMET_IGNORE)
        {
            nRet = pthread_tryjoin_np(m_hThread.m_TId, nullptr);
        }
        else
        {
            struct timespec ts;
            clock_gettime(CLOCK_REALTIME, &ts);

            ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
            ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
            if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
            {
                ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
                ts.tv_sec  += 1;
            }
            nRet = pthread_timedjoin_np(m_hThread.m_TId, nullptr, &ts);
        }
        if (nRet != RET_OKAY)
        {
            DEV_DEBUG(TF("  Thread[%p---%#lX] Wait ERROR ID:%#X"), this, m_hThread.m_TId, nRet);
        }
        return nRet;
    }
    return (Int)RET_FAIL;
}

Int CThread::Detach(void)
{
    return pthread_detach(m_hThread.m_TId);
}

void CThread::Routine(void)
{
    Int nOldState = 0;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,      &nOldState);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &nOldState);

    if (OnStart())
    {
        for(;;)
        {
            if (Run() == false)
            {
                break;
            }
            pthread_testcancel();
        }
    }
    OnStop();
    m_hThread.Reset();
}

///////////////////////////////////////////////////////////////////
// CRunnableThread
void CRunnableThread::Routine(void)
{
    Int nOldState = 0;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,      &nOldState);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &nOldState);

    if (OnStart())
    {
        for(;;)
        {
            if (Run() == false)
            {
                break;
            }
            pthread_testcancel();
        }
    }
    OnStop();

    Finish();
}

///////////////////////////////////////////////////////////////////
// CQueueTaskThread
void CQueueTaskThread::Routine(void)
{
    Int nOldState = 0;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,      &nOldState);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &nOldState);

    if (OnStart())
    {
        for (;;)
        {
            DEV_DUMP(TF("  QueueTaskThread[%p---%#lX] wait"), this, m_hThread.m_TId);
            if ((m_SyncEvent.Wait() == RET_OKAY) && (m_pQueueTask != nullptr))
            {
                Run();
                // return to thread pool
                DEV_DUMP(TF("  QueueTaskThread[%p---%#lX] Finish"), this, m_hThread.m_TId);
                Finish();
            }
            else
            {
                DEV_DUMP(TF("  QueueTaskThread[%p---%#lX] quit"), this, m_hThread.m_TId);
                break;
            }
            pthread_testcancel();
        }
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

