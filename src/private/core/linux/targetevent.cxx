// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "event.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTickEventImp : thread for CPosixTimer
CTickEventImp::CTickEventImp(void)
: m_pHandler(nullptr)
, m_inxId(nullptr)
, m_llUpdate(0)
, m_utTick(0)
, m_utFlag(FALSE)
, m_uCount(0)
, m_uInterval(0)
, m_hTimer(false)
{
}

CTickEventImp::~CTickEventImp(void)
{
    assert(m_pHandler == nullptr);
}

CTickEventImp::CTickEventImp(const CTickEventImp&)
: m_pHandler(nullptr)
, m_inxId(nullptr)
, m_llUpdate(0)
, m_utTick(0)
, m_utFlag(FALSE)
, m_uCount(0)
, m_uInterval(0)
, m_hTimer(false)
{
}

CTickEventImp& CTickEventImp::operator=(const CTickEventImp&)
{
    return (*this);
}

bool CTickEventImp::Init(CEventHandler& EventHandlerRef, PINDEX index, uintptr_t utTick, UInt uCount, UInt uInterval)
{
    assert(m_utTick == 0);
    //Exit();

    TimerAttr Attr;
    MM_SAFE::Set(&Attr, 0, sizeof(TimerAttr));
    Attr.sigev_notify          = SIGEV_THREAD;
    Attr.sigev_notify_function = (AIOHANDLER)&CTickEventImp::StaticTickRoutine;
    Attr.sigev_value.sival_ptr = this;
    if (m_hTimer.CreateTimer(CLOCK_REALTIME, &Attr))
    {
        m_pHandler  = &EventHandlerRef;
        m_inxId     = index;
        m_llUpdate  = CPlatform::GetRunningTime();
        m_utTick    = utTick;
        m_utFlag    = FALSE;
        m_uCount    = DEF::Max<UInt>((UInt)1, uCount);
        m_uInterval = DEF::Max<UInt>((UInt)TICK_TIME_MIN, uInterval);
        return Signal();
    }
    DEV_DEBUG(TF("  !!!Tick Event(%lld) Init Timer handle invalid"), utTick);
    return false;
}

bool CTickEventImp::Exit(bool bKill)
{
    m_uCount = 0;
    if ((CAtomics::CompareExchange<uintptr_t>(&m_utFlag, TRUE, FALSE) == TRUE) && 
        (bKill == false))
    {
        return false;
    }

    if (CAtomics::Exchange<uintptr_t>(&m_utTick, 0) != 0)
    {
        m_hTimer.CloseTimer();
        Reset();
        return true;
    }
    return false;
}

bool CTickEventImp::Check(Int nMult)
{
    if (m_utTick > 0)
    {
        nMult = DEF::Max<Int>(nMult, CEventQueue::EVENT_COUNT_CHECK);
        UInt uTimeout = (m_uInterval * nMult);
        if (uTimeout == 0)
        {
            ++uTimeout;
        }
        DEV_DEBUG(TF("  !!!Tick Event(%lld) Check %lld >= %lld + %d"), m_utTick, CPlatform::GetRunningTime(), m_llUpdate, uTimeout);
        return (CPlatform::GetRunningTime() > (m_llUpdate + uTimeout));
    }
    return false;
}

bool CTickEventImp::Signal(void)
{
    struct itimerspec its;
    its.it_value.tv_sec  = (time_t)(m_uInterval / (UInt)TIMET_S2MS);
    its.it_value.tv_nsec = (Long)(m_uInterval % TIMET_S2MS) * TIMET_NS2MS;
    its.it_interval = its.it_value;
    m_pHandler->AddRef();
    if (m_hTimer.SetTimer(&its) == RET_OKAY)
    {
        DEV_DEBUG(TF("  !!!Tick Event(%lld) Init okay"), m_utTick);
        return true;
    }
    Reset();
    DEV_DEBUG(TF("  !!!Tick Event(%lld) SetTimer fail or start thread fail"), m_utTick);
    return false;
}

void CTickEventImp::Cache(void)
{
    if (CAtomics::Exchange<uintptr_t>(&m_utTick, 0) != 0)
    {
        DEV_DEBUG(TF("  !!!Tick Event(%lld) return to cache"), m_utTick);
        m_hTimer.CloseTimer();
        Reset();
        if (SEventManager::GetInstance() != nullptr)
        {
            SEventManager::GetInstance()->CacheTickEvent(m_inxId);
        }
    }
}

void CTickEventImp::Reset(void)
{
    CEventHandler* pHandler = (CEventHandler*)CAtomics::ExchangePtr(reinterpret_cast<void* volatile*>(&m_pHandler), nullptr);
    if (pHandler != nullptr)
    {
        TRY_EXCEPTION
            pHandler->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
    }
}

bool CTickEventImp::TickRoutine(void)
{
    m_llUpdate = CPlatform::GetRunningTime();
    if (m_uCount > 0)
    {
        if (m_uCount != (UInt)TIMET_INFINITE)
        {
            --m_uCount;
        }
        // DEV_TRACE(TF("  !!!Tick Event(%lld) TickRoutine Count=%#X"), m_utTick, m_uCount);
        if (CAtomics::CompareExchange<uintptr_t>(&m_utFlag, TRUE, FALSE) == FALSE)
        {
            if (m_pHandler->OnHandle(m_utTick, m_uCount) != (UInt)RET_OKAY)
            {
                m_uCount = 0;
            }
            CAtomics::Exchange<uintptr_t>(&m_utFlag, FALSE);
        }
        else
        {
            return true;
        }
    }
    return (m_uCount > 0);
}

void CTickEventImp::StaticTickRoutine(sigval_t sv)
{
    CTickEventImp* pTickEventImp = reinterpret_cast<CTickEventImp*>(sv.sival_ptr);
//    if (SET_EXCEPTION() == RET_OKAY)
//    {
//       // DEV_TRACE(TF("  !!!Tick Event StaticTickRoutine"));
        if (pTickEventImp->TickRoutine() == false)
        {
            pTickEventImp->Cache();
        }
//        UNSET_EXCEPTION();
//    }
//    else
//    {
//        DEV_DEBUG(TF("  !!!Tick Event StaticTickRoutine invalid ptr"));
//        pTickEventImp->m_uCount = 0;
//    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
