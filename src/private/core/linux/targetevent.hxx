// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_EVENT_HXX__
#define __TARGET_EVENT_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "event.h"
#include "thread.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTickEventImp : thread for CPosixTimer
class CTickEventImp : public MObject
{
public:
    enum TICK_TIME
    {
        TICK_TIME_MIN   = 1, // 1 MS
        TICK_TIME_CHECK = 2,
    };
public:
    CTickEventImp(void);
    virtual ~CTickEventImp(void);

    bool   Init(CEventHandler& EventHandlerRef, PINDEX index, uintptr_t utTick, UInt uCount, UInt uInterval);
    bool   Exit(bool bKill = false);

    bool   Check(Int nMult);
private:
    CTickEventImp(const CTickEventImp&);
    CTickEventImp& operator=(const CTickEventImp&);

    bool   Signal(void);
    void   Cache(void);
    void   Reset(void);

    bool   TickRoutine(void);
    static void StaticTickRoutine(sigval_t);
private:
    CEventHandler*   m_pHandler;
    PINDEX           m_inxId;
    LLong            m_llUpdate;
    uintptr_t        m_utTick;
    uintptr_t        m_utFlag;
    UInt             m_uCount;
    UInt             m_uInterval;
    CPosixTimer      m_hTimer;
};

///////////////////////////////////////////////////////////////////
// CEventQueueHandle
class CEventQueueHandle : public MObject
{
public:
    CEventQueueHandle(void);
    ~CEventQueueHandle(void);

    bool     IsValid(void) const;

    bool     Open(void);
    void     Close(void);

    bool     Signal(Int nFlag = 0);
    bool     Wait(void);
private:
    CEventQueueHandle(const CEventQueueHandle& aSrc);
    CEventQueueHandle& operator=(const CEventQueueHandle& aSrc);
public:
    volatile Int   m_nFlag;
    CSyncSema      m_hSema;
};

INLINE CEventQueueHandle::CEventQueueHandle(void)
: m_nFlag(0)
, m_hSema(false)
{
}

INLINE CEventQueueHandle::~CEventQueueHandle(void)
{
}

INLINE CEventQueueHandle::CEventQueueHandle(const CEventQueueHandle&)
: m_nFlag(0)
, m_hSema(false)
{
}

INLINE CEventQueueHandle& CEventQueueHandle::operator=(const CEventQueueHandle&)
{
    return (*this);
}

INLINE bool CEventQueueHandle::IsValid(void) const
{
    return m_hSema.IsValid();
}

INLINE bool CEventQueueHandle::Open(void)
{
    Close();
    return m_hSema.Open();
}

INLINE void CEventQueueHandle::Close(void)
{
    m_nFlag = 0;
    m_hSema.Close();
}

INLINE bool CEventQueueHandle::Signal(Int nFlag)
{
    CAtomics::Exchange(&m_nFlag, nFlag);
    return (m_hSema.Signal() == RET_OKAY);
}

INLINE bool CEventQueueHandle::Wait(void)
{
    if (m_hSema.Wait() == RET_OKAY)
    {
        return (CAtomics::Exchange(&m_nFlag, m_nFlag) > 0);
    }
    return false;
}

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_EVENT_HXX__
