// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <sys/select.h>
#include <sys/time.h>
#include <byteswap.h>
#include <syslog.h>
#include <linux/version.h>
#include "subsystem.h"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CPlatformImp
CPCXStr  CPlatformImp::STR_LOG_LEVELNAME     = TF("-DevLevel:");
CPCXStr  CPlatformImp::STR_LOG_FILENAME      = TF("DEV%ld_%04d%02d%02d_%02d%02d%02d_%03d.log");

CPCXStr  CPlatformImp::CONSOLE_CLR_RESET     = TF("\033[0m");  // reset
CPCXStr  CPlatformImp::CONSOLE_CLR_MEMMG     = TF("\033[32m"); // green
CPCXStr  CPlatformImp::CONSOLE_CLR_TRACE     = TF("\033[35m"); // purple
CPCXStr  CPlatformImp::CONSOLE_CLR_DEBUG     = TF("\033[34m"); // blue
CPCXStr  CPlatformImp::CONSOLE_CLR_INFO      = TF("\033[37m"); // gray
CPCXStr  CPlatformImp::CONSOLE_CLR_DUMP      = TF("\033[36m"); // cyan
CPCXStr  CPlatformImp::CONSOLE_CLR_WARN      = TF("\033[33m"); // brown
CPCXStr  CPlatformImp::CONSOLE_CLR_ERROR     = TF("\033[31m"); // red

CPCXStr  CPlatformImp::STR_PROC_CMDLINE      = TF("/proc/self/cmdline");
CPCXStr  CPlatformImp::STR_PROC_EXE          = TF("/proc/self/exe");

CPCXStr  CPlatformImp::STR_PROC_STAT         = TF("/proc/stat");

CPCXStr  CPlatformImp::STR_MEM_INFO          = TF("/proc/meminfo");
CPCXStr  CPlatformImp::STR_MEMINFO_TOTAL     = TF("MemTotal:");
CPCXStr  CPlatformImp::STR_MEMINFO_AVAILABLE = TF("MemAvailable:");

CPCXStr  CPlatformImp::STR_OS_VERSION        = TF("/proc/version");

#ifdef __MODERN_CXX_NOT_SUPPORTED
THREADLS CPlatformImp::SIG_JUMP*               ms_pSigJump = nullptr;
#else
THREADLS CPlatformImp::SIG_JUMP                CPlatformImp::ms_SigJump;
#endif

CPlatformImp::CPlatformImp(void)
: mc_ullRuntime
(
      __ARCH_TARGET__
#ifdef __ARCH_TARGET_BIGENDIAN__
    | ARCH_TARGET_BIGENDIAN
#endif
    | __COMPILER_TYPE__
    | __PLATFORM_TARGET__
    | __RUNTIME_CHARSET__
#ifdef __RUNTIME_DEBUG__
    | RUNTIME_CONFIG_DEBUG
#endif
#ifdef __RUNTIME_STATIC__
    | RUNTIME_CONFIG_STATIC
#endif
)
, m_uPageSize(0)
#ifdef __RUNTIME_DEBUG__
, m_uLogLevel(LOGL_INFO|LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#else
, m_uLogLevel(LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#endif
, m_uComLevel(0)
, m_uLogMaxSize(0)
, m_hLogFile(HANDLE_INVALID)
, m_llBaseTime(0)
, m_llBaseTick(0)
, m_pComponent(nullptr)
{
}

CPlatformImp::~CPlatformImp(void)
{
    assert(m_hLogFile == HANDLE_INVALID);
}

CPlatformImp::CPlatformImp(const CPlatformImp&)
: mc_ullRuntime
(
      __ARCH_TARGET__
#ifdef __ARCH_TARGET_BIGENDIAN__
    | ARCH_TARGET_BIGENDIAN
#endif
    | __PLATFORM_TARGET__
    | __RUNTIME_CHARSET__
#ifdef __RUNTIME_DEBUG__
    | RUNTIME_CONFIG_DEBUG
#endif
#ifdef __RUNTIME_STATIC__
    | RUNTIME_CONFIG_STATIC
#endif
)
, m_uPageSize(0)
#ifdef __RUNTIME_DEBUG__
, m_uLogLevel(LOGL_INFO|LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#else
, m_uLogLevel(LOGL_ERROR)
#endif
, m_uComLevel(0)
, m_uLogMaxSize(0)
, m_hLogFile(HANDLE_INVALID)
, m_llBaseTime(0)
, m_llBaseTick(0)
, m_pComponent(nullptr)
{
}

CPlatformImp& CPlatformImp::operator=(const CPlatformImp&)
{
    return (*this);
}

bool CPlatformImp::Init(void)
{
    InitCMD();
    InitTick();

    InitLogFile();
    InitMemTrace();

    GetPageSize();
    CPUInfo();
    return true;
}

void CPlatformImp::Exit(void)
{
    ExitMemTrace();
    ExitLogFile();
}

void CPlatformImp::SignalHandler(Int nSig)
{
    assert(nSig == SIGSEGV);
#ifdef __MODERN_CXX_NOT_SUPPORTED
    if (ms_pSigJump == nullptr)
    {
        ms_pSigJump = new CPlatformImp::SIG_JUMP;
    }
    if (ms_pSigJump->stFlag == TRUE)
    {
        DEV_DEBUG(TF("***Exception Resume for SIGSEGV detected!!!"));
        ms_pSigJump->stFlag = FALSE;
        siglongjmp(ms_pSigJump->JmpBuf, TRUE);
    }
#else
    if (ms_SigJump.stFlag == TRUE)
    {
        DEV_DEBUG(TF("***Exception Resume for SIGSEGV detected!!!"));
        ms_SigJump.stFlag = FALSE;
        siglongjmp(ms_SigJump.JmpBuf, TRUE);
    }
#endif
}

Int CPlatformImp::SetExceptHandler(EXCEPT_HANDLER ExceptHandler)
{
#ifdef __MODERN_CXX_NOT_SUPPORTED
    if (ms_pSigJump == nullptr)
    {
        ms_pSigJump = new CPlatformImp::SIG_JUMP;
    }
    if (ExceptHandler == true)
    {
        if (ms_pSigJump->stFlag == TRUE)
        {
            ms_pSigJump->stFlag = FALSE;
            sigaction(SIGSEGV, &(ms_pSigJump->OldAct), nullptr);
        }
        if (sigsetjmp(ms_pSigJump->JmpBuf, TRUE) != 0)
        {
            DEV_DEBUG(TF("***Exception Resume for SIGSEGV handled, continue!!!"));
            return RET_FAIL; // siglongjmp
        }
        ms_pSigJump->stFlag = TRUE;
        //DEV_DEBUG(TF("***Exception Resume for SIGSEGV ready!!!"));
        struct sigaction Act;
        Act.sa_handler = &CPlatformImp::SignalHandler;
        sigemptyset(&Act.sa_mask);
        Act.sa_flags = SA_RESTART | SA_RESETHAND | SA_NODEFER;
        sigaction(SIGSEGV, &Act, &(ms_pSigJump->OldAct));
        return RET_OKAY;
    }
    else if (ms_pSigJump->stFlag == TRUE)
    {
        //DEV_DEBUG(TF("***Exception Resume for SIGSEGV clear!!!"));
        ms_pSigJump->stFlag = FALSE;
        sigaction(SIGSEGV, &(ms_pSigJump->OldAct), nullptr);
        return RET_OKAY;
    }
#else
    if (ExceptHandler == true)
    {
        if (ms_SigJump.stFlag == TRUE)
        {
            ms_SigJump.stFlag = FALSE;
            sigaction(SIGSEGV, &(ms_SigJump.OldAct), nullptr);
        }
        if (sigsetjmp(ms_SigJump.JmpBuf, TRUE) != 0)
        {
            DEV_DEBUG(TF("***Exception Resume for SIGSEGV handled, continue!!!"));
            return RET_FAIL; // siglongjmp
        }
        ms_SigJump.stFlag = TRUE;
        //DEV_DEBUG(TF("***Exception Resume for SIGSEGV ready!!!"));
        struct sigaction Act;
        Act.sa_handler = &CPlatformImp::SignalHandler;
        sigemptyset(&Act.sa_mask);
        Act.sa_flags = SA_RESTART | SA_RESETHAND | SA_NODEFER;
        sigaction(SIGSEGV, &Act, &(ms_SigJump.OldAct));
        return RET_OKAY;
    }
    else if (ms_SigJump.stFlag == TRUE)
    {
        //DEV_DEBUG(TF("***Exception Resume for SIGSEGV clear!!!"));
        ms_SigJump.stFlag = FALSE;
        sigaction(SIGSEGV, &(ms_SigJump.OldAct), nullptr);
        return RET_OKAY;
    }
#endif
    return RET_ERROR;
}

ULLong CPlatformImp::GetRuntime(void) const
{
    return (ULLong)mc_ullRuntime;
}

UInt CPlatformImp::GetPageSize(void)
{
    if (m_uPageSize == 0)
    {
        m_uPageSize = (UInt)getpagesize();
    }
    return m_uPageSize;
}

UInt CPlatformImp::CheckThread(UInt uCount, UInt uTimes, UInt uMin) const
{
    return DEF::Maxmin<UInt>(uMin, uCount, (m_CPUInfo.uCores * uTimes));
}

ULong CPlatformImp::GetMemInfo(PXStr& pszBuf, PCXStr pszName, Int nNameLen) const
{
    PXStr pszData = (PXStr)CXChar::Str(pszBuf, pszName);
    if (pszData != nullptr)
    {
        for (pszData += nNameLen; CXChar::IsSpace(*pszData); ++pszData);
        if (*pszData != 0)
        {
            return CXChar::ToULong(pszData, &pszData);
        }
    }
    return 0;
}

LLong CPlatformImp::GetBaseTime(void) const
{
    return m_llBaseTime;
}

LLong CPlatformImp::GetBaseTick(void) const
{
    return m_llBaseTick;
}

void CPlatformImp::GetCPUInfo(CPlatform::CPUINFO& cpui) const
{
    cpui = m_CPUInfo;
}

bool CPlatformImp::DevelopPrint(CComponent* pComponent, UInt uLevel)
{
    if (pComponent != nullptr)
    {
        if (CAtomics::CompareExchangePtr((void* volatile*)&m_pComponent, pComponent, nullptr) == nullptr)
        {
            ComponentAddRef();
            m_uComLevel = (uLevel & LOGL_ALL);
            DEV_DUMP(TF("#---Linux Platform Develop Print addref CComponent %#p---"), pComponent);
            return true;
        }
    }
    else if (m_pComponent != nullptr)
    {
        DEV_DUMP(TF("#---Linux Platform Develop Print release CComponent ---"));
        m_uComLevel = 0;
        ComponentRelease();
        return true;
    }
    return false;
}

void CPlatformImp::DevelopPrint(UInt uLevel, PCXStr pszFormat, va_list args)
{
    if (uLevel & (m_uLogLevel|m_uComLevel))
    {
        CPlatform::TIMEINFO ti = { 0 };
        CPlatform::GetTimeInfo(ti);

        Int   nLen = 0;
        XChar szLogBuf[CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END];
        switch (uLevel)
        {
#ifdef __RUNTIME_DEBUG__
        case LOGL_MEMMG:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_MEMMG, CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[MEMMG][%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_TRACE:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_TRACE, CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[TRACE][%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_DEBUG:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_DEBUG, CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DEBUG][%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
#endif  // __RUNTIME_DEBUG__
        case LOGL_INFO:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_INFO , CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[INFO] [%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_DUMP:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_DUMP , CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DUMP] [%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_WARN:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_WARN , CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[WARN] [%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_ERROR:
            {
                CXChar::Copy(szLogBuf, CONSOLE_CLR_BEGIN + TRANSC_LOG_BUF + CONSOLE_CLR_END, CONSOLE_CLR_ERROR, CONSOLE_CLR_BEGIN);
                nLen = CXChar::Format(szLogBuf + CONSOLE_CLR_BEGIN, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[ERROR][%08lX]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        default:
            {
                return;
            }
        }
        if (nLen > 0)
        {
            nLen += CXChar::FormatV((szLogBuf + CONSOLE_CLR_BEGIN + nLen), (size_t)((size_t)TRANSC_LOG_BUF - (size_t)nLen), pszFormat, args);
            //
            szLogBuf[CONSOLE_CLR_BEGIN + nLen] = 0;
            if ((m_uLogLevel & LOGL_DEVPRINT) && (m_uLogLevel & uLevel))
            {
                syslog(LOG_DEBUG|LOG_USER, szLogBuf + CONSOLE_CLR_BEGIN);
            }
            if ((m_pComponent != nullptr) && (m_uComLevel & uLevel))
            {
                szLogBuf[CONSOLE_CLR_BEGIN + nLen] = TF('\n');
                szLogBuf[CONSOLE_CLR_BEGIN + nLen + 1] = 0;
                m_pComponent->Command(szLogBuf + CONSOLE_CLR_BEGIN, uLevel);
                szLogBuf[CONSOLE_CLR_BEGIN + nLen] = 0;
            }
            //
            if (m_uLogLevel & uLevel)
            {
                szLogBuf[CONSOLE_CLR_BEGIN + nLen] = TF('\n');
                ++nLen;
                szLogBuf[CONSOLE_CLR_BEGIN + nLen] = 0;
                PrintLogFile(szLogBuf + CONSOLE_CLR_BEGIN, nLen);
                CXChar::Copy(szLogBuf + CONSOLE_CLR_BEGIN + nLen, (TRANSC_LOG_BUF - nLen), CONSOLE_CLR_RESET, CONSOLE_CLR_END);
                nLen += CONSOLE_CLR_BEGIN + CONSOLE_CLR_END;
                write(STDOUT_FILENO, szLogBuf, nLen * sizeof(XChar));
            }
        }
    }
}

void* CPlatformImp::Alloc(size_t stSize, UInt uType)
{
    Int nProt = (uType == ALLOC_TYPE_NORMAL) ? (PROT_READ|PROT_WRITE) : (PROT_READ|PROT_WRITE|PROT_EXEC);
    return mmap(nullptr, stSize, nProt, MAP_PRIVATE|MAP_ANONYMOUS, HANDLE_INVALID, 0);
}

void  CPlatformImp::Free(void* pPtr, size_t stSize)
{
    munmap(pPtr, stSize);
}

void CPlatformImp::InitCMD(void)
{
    XChar szCMD[LMT_BUF] = { 0 };
    CPlatform::GetFullCmd(szCMD, LMT_BUF);
    PCXStr pszLog = CXChar::Str(szCMD, STR_LOG_LEVELNAME);
    if (pszLog != nullptr)
    {
        pszLog       += TRANSC_LOG_LEVEL;
        m_uLogLevel  = (UInt)CXChar::ToULong(pszLog, nullptr, RADIXT_HEX);
        m_uLogLevel &= (LOGL_DEVOUT|LOGL_DEVPRINT|LOGL_DEVFILE|LOGL_MEMMG|LOGL_ALL);
    }
}

void CPlatformImp::InitTick(void)
{
    // Get the main processor affinity mask
    cpu_set_t csAffinityMask;
    CPU_ZERO(&csAffinityMask);
    if (sched_getaffinity(0, sizeof(cpu_set_t), &csAffinityMask) == 0)
    {
        cpu_set_t csThreadMask;
        CPU_ZERO(&csThreadMask);
        // Find the lowest processor that our process is allows to run against
        Int nCount = CPU_COUNT(&csAffinityMask);
        for (Int i = 0; i < nCount; ++i)
        {
            if (CPU_ISSET(i, &csAffinityMask))
            {
                CPU_SET(i, &csThreadMask);
                // Set this as the processor that our thread must always run against
                // This must be a subset of the process affinity mask
                pthread_setaffinity_np(CPlatform::GetCurrentTId(), sizeof(cpu_set_t), &csThreadMask);
                break;
            }
        }
    }
    timespec tsTime;
    clock_gettime(CLOCK_MONOTONIC, &tsTime);

    m_llBaseTime = tsTime.tv_sec;
    m_llBaseTick = tsTime.tv_nsec;
}

bool CPlatformImp::InitLogFile(void)
{
    if (m_hLogFile == HANDLE_INVALID)
    {
        if ((m_uLogLevel & LOGL_DEVFILE) && (m_uLogLevel & LOGL_ALL))
        {
            CPlatform::TIMEINFO ti;
            CPlatform::GetTimeInfo(ti);
            XChar szLogFile[LMT_MAX_PATH] = { 0 };
            CXChar::Format(szLogFile, (size_t)LMT_MAX_PATH, STR_LOG_FILENAME,
                           CPlatform::GetCurrentPId(), ti.usYear, ti.usMonth, ti.usDay,
                           ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond);
            m_hLogFile = _topen(szLogFile, O_RDWR|O_CREAT|O_TRUNC, DEFFILEMODE);
            if (m_hLogFile != HANDLE_INVALID)
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                Byte  bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                write(m_hLogFile, bHead, BOML_UTF8);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                Byte  bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                write(m_hLogFile, bHead, BOML_UTF32);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                Byte  bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                write(m_hLogFile, bHead, BOML_UTF16);
#endif
            }
        }
    }
    return (m_hLogFile != HANDLE_INVALID);
}

void CPlatformImp::ExitLogFile(void)
{
    if (m_hLogFile != HANDLE_INVALID)
    {
        close(m_hLogFile);
        m_hLogFile = HANDLE_INVALID;
    }
}

void CPlatformImp::PrintLogFile(PCXStr pszLog, Int nLen)
{
    if (m_hLogFile != HANDLE_INVALID)
    {
        UInt uRet = nLen * sizeof(XChar);
        write(m_hLogFile, pszLog, uRet);

        uRet += CAtomics::Add<UInt>(&m_uLogMaxSize, uRet);
        if ((uRet >= TRANSC_LOG_MAXSIZE) &&
            (CAtomics::CompareExchange<UInt>(&m_uLogMaxSize, 0, uRet) == uRet))
        {
            ExitLogFile();
            InitLogFile();
        }
    }
}

bool CPlatformImp::InitMemTrace(void)
{
#if (__COMPILER_TYPE__ == COMPILER_TYPE_GCC)
#ifdef __RUNTIME_DEBUG__
    DEV_DUMP(TF("#---Linux Platform [%d] - GNU G++[%s] Build[%s, %s] Debug Version---"), LINUX_VERSION_CODE, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#else
    DEV_DUMP(TF("#---Linux Platform [%d] - GNU G++[%s] Build[%s, %s] Release Version---"), LINUX_VERSION_CODE, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_CLANG)
#ifdef __RUNTIME_DEBUG__
    DEV_DUMP(TF("#---Linux Platform [%d] - LLVM Clang[%s] Build[%s, %s] Debug Version---"), LINUX_VERSION_CODE, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#else
    DEV_DUMP(TF("#---Linux Platform [%d] - LLVM Clang[%s] Build[%s, %s] Release Version---"), LINUX_VERSION_CODE, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
    #error "Add Compile Info!!!"
#else
    #error "Add Compile Info!!!"
#endif
    return true;
}

void CPlatformImp::ExitMemTrace(void)
{
}

void CPlatformImp::CPUInfo(void)
{
    MM_SAFE::Set(&m_CPUInfo, 0, sizeof(CPlatform::CPUINFO));
    // CPU Logical processors
    m_CPUInfo.uCores = (UShort)get_nprocs();
    //
    FILE* pFile = _tpopen(TF("lscpu"), TF("r"));
    if (pFile != nullptr)
    {
        XChar szValue[LMT_MAX_NAME] = { 0 };
        while (_fgetts(szValue, LMT_MAX_NAME, pFile) != nullptr)
        {
            if (CXChar::Str(szValue, TF("Architecture:")) != nullptr)
            {
                PXStr pszValue = (szValue + 13);
                while (pszValue[0] == TF(' '))
                {
                    ++pszValue;
                }
                PXStr p = (PXStr)CXChar::RevChr(pszValue, TF('\n'));
                if (p != nullptr)
                {
                    *p = 0;
                }

                DEV_DEBUG(TF("Architecture: %s --- %d"), pszValue, CXChar::Length(pszValue));
                if (m_CPUInfo.szInfo[0] != 0)
                {
                   CXChar::Concat(m_CPUInfo.szInfo, LMT_MIN, TF("-"));
                }
                CXChar::Concat(m_CPUInfo.szInfo, LMT_MIN, pszValue);
            }
            if (CXChar::Str(szValue, TF("Model name:")) != nullptr)
            {
                PXStr pszValue = (szValue + 11);
                while (pszValue[0] == TF(' '))
                {
                    ++pszValue;
                }
                PXStr p = (PXStr)CXChar::RevChr(pszValue, TF('\n'));
                if (p != nullptr)
                {
                    *p = 0;
                }
                
                DEV_DEBUG(TF("Model name: %s --- %d"), pszValue, CXChar::Length(pszValue));
                if (m_CPUInfo.szInfo[0] != 0)
                {
                   CXChar::Concat(m_CPUInfo.szInfo, LMT_MIN, TF("-"));
                }
                CXChar::Concat(m_CPUInfo.szInfo, LMT_MIN, pszValue);
            }
        }
        pclose(pFile);
    }
}

void CPlatformImp::ComponentAddRef(void)
{
    m_pComponent->AddRef();
}

void CPlatformImp::ComponentRelease(void)
{
    TRY_EXCEPTION
        m_pComponent->Release();
        CAtomics::ExchangePtr((void* volatile*)&m_pComponent, nullptr);
    CATCH_EXCEPTION
    END_EXCEPTION
}

///////////////////////////////////////////////////////////////////
// CPlatform
CPlatform::CPlatform(void)
{
}

CPlatform::~CPlatform(void)
{
}

CPlatform::CPlatform(const CPlatform&)
{
}

CPlatform& CPlatform::operator=(const CPlatform&)
{
    return (*this);
}

ULLong CPlatform::GetRuntimeConfig(void)
{
    return (GPlatform.GetRuntime());
}

UInt CPlatform::GetPageSize(void)
{
    return (GPlatform.GetPageSize());
}

PId CPlatform::GetCurrentPId(void)
{
    return getpid();
}

TId CPlatform::GetCurrentTId(void)
{
    return pthread_self();
}

Int CPlatform::SleepEx(UInt uSleep, bool)
{
    struct timeval tSleep;
    tSleep.tv_sec  = (time_t)(uSleep / (UInt)TIMET_S2MS);
    tSleep.tv_usec = (__suseconds_t)(uSleep % TIMET_S2MS) * TIMET_XS2XS; // MS - US
    if (tSleep.tv_usec >= (__suseconds_t)TIMET_US2SEC)
    {
        tSleep.tv_usec -= (__suseconds_t)TIMET_US2SEC;
        tSleep.tv_sec  += 1;
    }
    return select(0, nullptr, nullptr, nullptr, &tSleep);
}

void CPlatform::YieldEx(void)
{
    pthread_yield();
}

UInt CPlatform::CheckThread(UInt uCount, UInt uTimes, UInt uMin)
{
    return GPlatform.CheckThread(uCount, uTimes, uMin);
}

UInt CPlatform::GetCPURate(CPUSTAT& cpus1, CPUSTAT& cpus2)
{
    UInt uRate = 0;
    if (cpus2.ullTotal != cpus1.ullTotal)
    {
        if (cpus2.ullTotal > cpus1.ullTotal)
        {
            ULLong ullUsed = (cpus2.ullUsed > cpus1.ullUsed) ? (cpus2.ullUsed - cpus1.ullUsed) : 0;
            ullUsed *= CPlatformImp::CPU_OCCUPY_RATE;
            
            uRate = (UInt)(ullUsed / (cpus2.ullTotal - cpus1.ullTotal));
        }
        else
        {
            ULLong ullUsed = (cpus1.ullUsed > cpus2.ullUsed) ? (cpus1.ullUsed - cpus2.ullUsed) : 0;
            ullUsed *= CPlatformImp::CPU_OCCUPY_RATE;

            uRate = (UInt)(ullUsed / (cpus1.ullTotal - cpus2.ullTotal));
        }
    }
    return uRate;
}

void CPlatform::GetCPUStat(CPUSTAT& cpus)
{
    CPlatformImp::CPU_OCCUPY cpuo;
    MM_SAFE::Set(&cpuo, 0, sizeof(CPlatformImp::CPU_OCCUPY));

    XChar szCpuStat[LMT_BUF] = { 0 };
    Handle hCpuStat = _topen(CPlatformImp::STR_PROC_STAT, O_RDONLY);
    if (hCpuStat != HANDLE_INVALID)
    {
        read(hCpuStat, szCpuStat, (LMT_BUF - 1) * sizeof(XChar));
        close(hCpuStat);
    }

    XChar szCpuName[LMT_KEY] = { 0 };
    _stscanf(szCpuStat, TF("%s %llu %llu %llu %llu %llu %llu %llu"),
             szCpuName, &cpuo.ullUser, &cpuo.ullNice, &cpuo.ullSystem,
             &cpuo.ullIdle, &cpuo.ullIOWait, &cpuo.ullIRQ, &cpuo.ullSIRQ);
    cpus.ullUsed   = (cpuo.ullUser + cpuo.ullNice + cpuo.ullSystem + cpuo.ullIOWait + cpuo.ullIRQ + cpuo.ullSIRQ);
    cpus.ullTotal  = (cpus.ullUsed + cpuo.ullIdle);
}

void CPlatform::GetCPUInfo(CPUINFO& cpui)
{
    GPlatform.GetCPUInfo(cpui);
}

void CPlatform::GetMemInfo(MEMINFO& mem)
{
    mem.uPageSize = (GPlatform.GetPageSize() >> CAPAP_10); // KB

    XChar szMemInfo[LMT_BUF] = { 0 };
    Handle hMemInfo = _topen(CPlatformImp::STR_MEM_INFO, O_RDONLY);
    if (hMemInfo != HANDLE_INVALID)
    {
        read(hMemInfo, szMemInfo, (LMT_BUF - 1) * sizeof(XChar));
        close(hMemInfo);
    }
    PXStr pszBuf    = szMemInfo;
    ULong ulTotal   = GPlatform.GetMemInfo(pszBuf, CPlatformImp::STR_MEMINFO_TOTAL,     CPlatformImp::MEMINFO_TOTAL);
    ULong ulAvail   = GPlatform.GetMemInfo(pszBuf, CPlatformImp::STR_MEMINFO_AVAILABLE, CPlatformImp::MEMINFO_AVAILABLE);
    // meminfo KB
    if (ulTotal == 0)
    {
        struct sysinfo si;
        sysinfo(&si);

        ulTotal  = si.totalram >> CAPAP_10; // KB
        ulAvail  = si.freeram  >> CAPAP_10; // KB
    }
    mem.uUsedPercent  = (UInt)(((ulTotal - ulAvail) * 100) / ulTotal);
    mem.uTotalPhys    = (UInt)(ulTotal >> CAPAP_10); // MB
    mem.uAvailPhys    = (UInt)(ulAvail >> CAPAP_10);
}

void CPlatform::GetOSInfo(OSINFO& os)
{
    os.uOSType = OST_LINUX;
    os.uMajor  = (LINUX_VERSION_CODE >> 16);
    os.uMinor  = (LINUX_VERSION_CODE >> 8) & 0xFF;
    os.uBuild  = (LINUX_VERSION_CODE & 0xFF);
    Handle hOSInfo = _topen(CPlatformImp::STR_OS_VERSION, O_RDONLY);
    if (hOSInfo != HANDLE_INVALID)
    {
        read(hOSInfo, os.szInfo, (LMT_MAX_NAME - 1) * sizeof(XChar));
        close(hOSInfo);
    }
    PXStr p = (PXStr)CXChar::Chr(os.szInfo, TF('\n'));
    if (p != nullptr)
    {
        *p = 0;
    }
}

void CPlatform::GetTimeInfo(TIMEINFO& ti, bool bLocal)
{
    time_t     tTime = time(nullptr);
    struct tm* pTime = nullptr;
    if (bLocal)
    {
        pTime = localtime(&tTime);
    }
    else
    {
        pTime = gmtime(&tTime);
    }
    ti.usYear    = pTime->tm_year + TIMET_TM_YEAR;
    ti.usMonth   = pTime->tm_mon  + TIMET_TM_MONTH;
    ti.usWeekDay = pTime->tm_wday;
    ti.usDay     = pTime->tm_mday;
    ti.usHour    = pTime->tm_hour;
    ti.usMinute  = pTime->tm_min;
    ti.usSecond  = pTime->tm_sec;

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ti.usMSecond = (ts.tv_nsec / TIMET_NS2MS);
}

ULLong CPlatform::GetTimeStamp(void)
{
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return (ULLong)((tv.tv_sec * TIMET_S2MS) + (tv.tv_usec / TIMET_XS2XS));
}

size_t CPlatform::GetFullCmd(PXStr pszBuf, size_t stBufLen)
{
    if ((pszBuf == nullptr) || (stBufLen < 1))
    {
        return 0;
    }
    Handle hCmdLine = _topen(CPlatformImp::STR_PROC_CMDLINE, O_RDONLY);
    if (hCmdLine != HANDLE_INVALID)
    {
        ssize_t sstRet = read(hCmdLine, pszBuf, (stBufLen - 1) * sizeof(XChar));
        close(hCmdLine);
        if (sstRet <= 0)
        {
            return 0;
        }
        size_t stLen = (size_t)sstRet / sizeof(XChar);
        pszBuf[stLen] = 0;

        for (size_t i = 1; i < stLen; ++i) // skip 1 root /
        {
            if (pszBuf[i] < TF(' '))
            {
                pszBuf[i] = TF(' ');
            }
        }
        // relative path
        if (pszBuf[0] == TF('.'))
        {
            XChar szPath[LMT_MAX_PATH];
            stLen = GetPathInfo(szPath, LMT_MAX_PATH, true);
            assert(stLen > 0);
            szPath[stLen] = 0;

            PCXStr p = CXChar::Chr(pszBuf, TF(' '));
            if (p != nullptr)
            {
                CXChar::Copy((szPath + stLen), (LMT_MAX_PATH - stLen), p);
                CXChar::Copy(pszBuf, stBufLen, szPath);
                stBufLen = CXChar::Length(pszBuf);
            }
        }
        else
        {
            stBufLen = stLen;
        }
        return stBufLen;
    }
    return 0;
}

size_t CPlatform::GetPathInfo(PXStr pszBuf, size_t stBufLen, bool bFull)
{
    ssize_t sstRet = _treadlink(CPlatformImp::STR_PROC_EXE, pszBuf, stBufLen);
    if (sstRet <= 0)
    {
        return 0;
    }
    stBufLen = (size_t)sstRet;
    pszBuf[stBufLen] = 0;

    if (bFull == false)
    {
        PXStr p = (PXStr)CXChar::RevChr(pszBuf, TF('/'));
        assert(p > pszBuf);
        ++p;
        *p = 0;

        stBufLen = (p - pszBuf);
    }
    return stBufLen;
}

PXStr CPlatform::SetLocal(int nLocal, PCXStr pszLanguage)
{
    return _tsetlocale(nLocal, pszLanguage);
}

Int CPlatform::SetExceptHandler(EXCEPT_HANDLER ExceptHandler)
{
    return GPlatform.SetExceptHandler(ExceptHandler);
}

LLong CPlatform::GetRunningTime(void)
{
    timespec tsTime;
    clock_gettime(CLOCK_MONOTONIC, &tsTime);

    if (tsTime.tv_nsec < GPlatform.GetBaseTick())
    {
        tsTime.tv_nsec += TIMET_NS2SEC;
        tsTime.tv_sec  -= 1;
    }
    LLong llTime = (tsTime.tv_sec - GPlatform.GetBaseTime()) * TIMET_S2MS;
    llTime += (tsTime.tv_nsec - GPlatform.GetBaseTick()) / TIMET_NS2MS;
    return llTime;
}

LLong CPlatform::GetOSRunningTime(void)
{
    timespec tsTime;
    clock_gettime(CLOCK_MONOTONIC, &tsTime);

    LLong llTime = tsTime.tv_sec * TIMET_S2MS;
    llTime += tsTime.tv_nsec / TIMET_NS2MS;
    return llTime;
}

LLong CPlatform::GetOSRunningTick(void)
{
    timespec tsTime;
    clock_gettime(CLOCK_MONOTONIC, &tsTime);

    LLong llTime = tsTime.tv_sec * TIMET_S2MS;
    llTime += tsTime.tv_nsec / TIMET_NS2MS;
    return llTime;
}

LLong CPlatform::GetOSBaseTick(void)
{
    return TIMET_S2MS;
}

LLong CPlatform::Tick2MilliSecond(LLong& llTick)
{
    return llTick; // MS already
}

UShort CPlatform::ByteSwap(UShort usValue)
{
    return bswap_16(usValue);
}

UInt CPlatform::ByteSwap(UInt uValue)
{
    return (UInt)bswap_32(uValue);
}

ULong CPlatform::ByteSwap(ULong ulValue)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return (ULong)bswap_64(ulValue);
#else
    return (ULong)bswap_32(ulValue);
#endif
}

ULLong CPlatform::ByteSwap(ULLong ullValue)
{
    return bswap_64(ullValue);
}

bool CPlatform::DevelopPrint(CComponent* pComponent, UInt uLevel)
{
    return GPlatform.DevelopPrint(pComponent, uLevel);
}

void CPlatform::DevelopPrint(UInt uLevel, PCXStr pszFormat, ...)
{
    if (uLevel & (LOGL_ALL|LOGL_MEMMG))
    {
        va_list vl;
        va_start(vl, pszFormat);
        GPlatform.DevelopPrint(uLevel, pszFormat, vl);
        va_end(vl);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
