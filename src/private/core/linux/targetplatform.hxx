// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_PLATFORM_HXX__
#define __TARGET_PLATFORM_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include <setjmp.h>
#include <signal.h>
#include "platform.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CPlatformImp
class CPlatformImp
{
public:
    enum TRANS_CONST
    {
        TRANSC_LOG_LEVEL   = 10,     // length of "-DevLevel:"
        TRANSC_LOG_BUF     = 8192,
        TRANSC_LOG_MAXSIZE = 4 * 1024 * 1024,
    };

    enum CONSOLE_CLR
    {
        CONSOLE_CLR_BEGIN  = 5,
        CONSOLE_CLR_END    = 4,
    };

    enum CPU_OCCUPY_CONST
    {
        CPU_OCCUPY_RATE = 10000,
    };

    enum MEMINFO_CONST
    {
        MEMINFO_TOTAL      = 9,
        MEMINFO_AVAILABLE  = 13,
        MEMINFO_ACTIVE     = 7,
        MEMINFO_SLAB       = 5,
        MEMINFO_KSTACK     = 12,
        MEMINFO_PTABLES    = 11,
    };

    typedef struct tagCPU_OCCUPY
    {
        ULLong   ullUser;
        ULLong   ullNice;
        ULLong   ullSystem;
        ULLong   ullIdle;
        ULLong   ullIOWait;
        ULLong   ullIRQ;
        ULLong   ullSIRQ;
        ULLong   ullReserved;
    }CPU_OCCUPY, *PCPU_OCCUPY;

    typedef struct tagSIG_JUMP
    {
    public:
        tagSIG_JUMP(void) : stFlag(FALSE) { }
        ~tagSIG_JUMP(void) { }
    public:
        size_t             stFlag;
        struct sigaction   OldAct;
        jmp_buf            JmpBuf;
    }SIG_JUMP, *PSIG_JUMP;

    static CPCXStr STR_LOG_LEVELNAME;
    static CPCXStr STR_LOG_FILENAME;
    // console color
    static CPCXStr CONSOLE_CLR_RESET;
    static CPCXStr CONSOLE_CLR_MEMMG;
    static CPCXStr CONSOLE_CLR_TRACE;
    static CPCXStr CONSOLE_CLR_DEBUG;
    static CPCXStr CONSOLE_CLR_INFO;
    static CPCXStr CONSOLE_CLR_DUMP;
    static CPCXStr CONSOLE_CLR_WARN;
    static CPCXStr CONSOLE_CLR_ERROR;
    // cmdline
    static CPCXStr STR_PROC_CMDLINE;
    static CPCXStr STR_PROC_EXE;
    // cpu
    static CPCXStr STR_PROC_STAT;
    // meminfo
    static CPCXStr STR_MEM_INFO;
    static CPCXStr STR_MEMINFO_TOTAL;
    static CPCXStr STR_MEMINFO_AVAILABLE;
    // osinfo
    static CPCXStr STR_OS_VERSION;
public:
    static void    SignalHandler(Int nSig);
public:
    CPlatformImp(void);
    ~CPlatformImp(void);

    bool    Init(void);
    void    Exit(void);

    Int     SetExceptHandler(EXCEPT_HANDLER ExceptHandler);

    ULLong  GetRuntime(void) const;
    UInt    GetPageSize(void);
    UInt    CheckThread(UInt uCount, UInt uTimes, UInt uMin = 1) const;
    ULong   GetMemInfo(PXStr& pszBuf, PCXStr pszName, Int nNameLen) const;

    LLong   GetBaseTime(void) const;
    LLong   GetBaseTick(void) const;

    void    GetCPUInfo(CPlatform::CPUINFO& cpui) const;

    bool    DevelopPrint(CComponent* pComponent, UInt uLevel);
    void    DevelopPrint(UInt uLevel, PCXStr pszFormat, va_list args);

    void*   Alloc(size_t stSize, UInt uType = ALLOC_TYPE_NORMAL);
    void    Free(void* pPtr, size_t stSize);

private:
    CPlatformImp(const CPlatformImp&);
    CPlatformImp& operator=(const CPlatformImp&);

    void    InitCMD(void);
    void    InitTick(void);

    bool    InitLogFile(void);
    void    ExitLogFile(void);
    void    PrintLogFile(PCXStr pszLog, Int nLen);

    bool    InitMemTrace(void);
    void    ExitMemTrace(void);

    void    CPUInfo(void);

    void    ComponentAddRef(void);
    void    ComponentRelease(void);
private:
#ifdef __MODERN_CXX_NOT_SUPPORTED
    //static THREADLS SIG_JUMP*  ms_pSigJump;
#else
    static THREADLS SIG_JUMP   ms_SigJump;
#endif
private:
    const ULLong               mc_ullRuntime;
    UInt                       m_uPageSize;
    UInt                       m_uLogLevel;
    UInt                       m_uComLevel;
    UInt                       m_uLogMaxSize;
    Handle                     m_hLogFile;
    LLong                      m_llBaseTime; // core init base time
    LLong                      m_llBaseTick; // core init base tick
    CComponent*                m_pComponent; // develop print to component
    CPlatform::CPUINFO         m_CPUInfo;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_PLATFORM_HXX__
