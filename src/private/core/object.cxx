// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "object.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CObjectLinker
CObjectLinker::CObjectLinker(void)
{
}

CObjectLinker::~CObjectLinker(void)
{
}

CObjectLinker::CObjectLinker(const CObjectLinker&)
{
}

CObjectLinker& CObjectLinker::operator=(const CObjectLinker&)
{
    return (*this);
}

UInt CObjectLinker::Init(void)
{
    assert(m_Objects.GetSize() == 0);
    DEV_DUMP(TF("*-Object-Linker init okay-"));
    return (UInt)RET_OKAY;
}

void CObjectLinker::Exit(void)
{
#ifdef __RUNTIME_DEBUG__
    ExitDump();
#endif
    m_Objects.RemoveAll();
    m_Modules.RemoveAll();
    DEV_DUMP(TF("*-Object-Linker exit okay-"));
}

PINDEX CObjectLinker::Link(CObject* pObj, const CUUID& uuid, size_t stFlag)
{
    CString strUUID;
    uuid.ToString(strUUID);
    return Link(pObj, strUUID.GetBuffer(), stFlag);
}

PINDEX CObjectLinker::Link(CObject* pObj, PCXStr pszName, size_t stFlag)
{
    CSyncLockScope scope(m_SyncLock);
    size_t stKey = CHash::Hash(pszName);

    PINDEX inxObj = m_Objects.FindIndex(stKey);
    if (inxObj != nullptr)
    {
        return nullptr;
    }
    inxObj = m_Objects.Add(stKey);
    if (inxObj == nullptr)
    {
        return nullptr;
    }
    m_Objects[inxObj].pObj   = pObj;
    m_Objects[inxObj].stFlag = stFlag;
    //if (stFlag & CObject::FLAG_MANAGED)
    //{
    //    pObj->AddRef();
    //}
    CXChar::Copy(m_Objects[inxObj].szName, (size_t)LMT_KEY, pszName);
    DEV_DUMP(TF("  Object-Linker link object(Class=%s, key=%s, ptr=%p, flag=%x) at %p"),
             pObj->GetRTTI().GetName(), pszName, pObj, stFlag, inxObj);
    return inxObj;
}

void CObjectLinker::Unlink(PINDEX index)
{
    CSyncLockScope scope(m_SyncLock);
    MAP_OBJECT_PAIR* pPair = m_Objects.GetAt(index);
    DEV_DUMP(TF("  Object-Linker unlink object(Class=%s, key=%s, ptr=%p, %x-%d) at %p"), pPair->m_V.pObj->GetRTTI().GetName(), pPair->m_V.szName, pPair->m_V.pObj, pPair->m_V.stFlag, pPair->m_V.stFlag, index);
    m_Objects.RemoveAt(index);
}

PINDEX CObjectLinker::Load(PINDEX& inxModule, CObject* pObj, const CUUID& uuid, size_t stFlag, PCXStr pszModule, const CModuleHandle& hModule)
{
    CString strUUID;
    uuid.ToString(strUUID);
    return Load(inxModule, pObj, strUUID.GetBuffer(), stFlag, pszModule, hModule);
}

PINDEX CObjectLinker::Load(PINDEX& inxModule, CObject* pObj, PCXStr pszName, size_t stFlag, PCXStr pszModule, const CModuleHandle& hModule)
{
    CSyncLockScope scope(m_SyncLock);
    size_t stKey = CHash::Hash(pszName);

    PINDEX inxObj = m_Objects.FindIndex(stKey);
    if (inxObj != nullptr)
    {
        return nullptr;
    }
    inxObj = m_Objects.Add(stKey);
    if (inxObj == nullptr)
    {
        return nullptr;
    }
    m_Objects[inxObj].pObj   = pObj;
    m_Objects[inxObj].stFlag = stFlag;
    CXChar::Copy(m_Objects[inxObj].szName, (size_t)LMT_KEY, pszName);
    DEV_DUMP(TF("  Object-Linker link object(Class=%s, key=%s, ptr=%p, flag=%x) at %p"),
             pObj->GetRTTI().GetName(), pszName, pObj, stFlag, inxObj);

    size_t stModule = CHash::Hash(pObj->GetRTTI().GetName());
    inxModule = m_Modules.FindIndex(stModule);
    if (inxModule != nullptr)
    {
        MAP_MODULE_PAIR* pPair = m_Modules.GetAt(inxModule);
        pPair->m_V.stRefCount += 1;
    }
    else
    {
        inxModule = m_Modules.Add(stModule);
        MAP_MODULE_PAIR* pPair = m_Modules.GetAt(inxModule);

        pPair->m_V.hModule = hModule.m_Module;
        pPair->m_V.strPath = pszModule;
    }
    return inxObj;
}

void CObjectLinker::Unload(PCXStr pszClass)
{
    CSyncLockScope scope(m_SyncLock);
    size_t stModule = CHash::Hash(pszClass);
    PINDEX index = m_Modules.FindIndex(stModule);
    if (index != nullptr)
    {
        MAP_MODULE_PAIR* pPair = m_Modules.GetAt(index);
        pPair->m_V.stRefCount -= 1;
        if (pPair->m_V.stRefCount == 0)
        {
            DEV_DEBUG(TF("  Object-Linker Link module(Path=%s, handle=%p, %d) at %p!"), pPair->m_V.strPath.GetBuffer(), (Module)pPair->m_V.hModule, pPair->m_V.stRefCount, index);
            m_Modules.RemoveAt(reinterpret_cast<PINDEX>(pPair));
        }
    }
}

Int CObjectLinker::Find(CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti, bool bExactClass)
{
    CSyncLockScope scope(m_SyncLock);
    Int nCount   = 0;
    PINDEX index = m_Objects.GetFirstIndex();
    while (index != nullptr)
    {
        const MAP_OBJECT_PAIR* pPair = m_Objects.GetNext(index);
        if (bExactClass)
        {
            if (pPair->m_V.pObj->GetRTTI().IsExactClass(rtti))
            {
                ObjectPtrs.Add(pPair->m_V.pObj);
                ++nCount;
            }
        }
        else if (pPair->m_V.pObj->GetRTTI().IsKindOf(rtti))
        {
            ObjectPtrs.Add(pPair->m_V.pObj);
            ++nCount;
        }
    }
    return nCount;
}

Int CObjectLinker::Find(CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName)
{
    CSyncLockScope scope(m_SyncLock);
    Int nCount   = 0;
    PINDEX index = m_Objects.GetFirstIndex();
    while (index != nullptr)
    {
        const MAP_OBJECT_PAIR* pPair = m_Objects.GetNext(index);
        if (CXChar::Cmp(pPair->m_V.pObj->GetRTTI().GetName(), pszClassName) == 0)
        {
            ObjectPtrs.Add(pPair->m_V.pObj);
            ++nCount;
        }
    }
    return nCount;
}

bool CObjectLinker::Find(CObjectPtr& ObjectPtr, size_t stKey, const CRTTI& rtti, bool bExactClass)
{
    CSyncLockScope scope(m_SyncLock);
    const MAP_OBJECT_PAIR* pPair = m_Objects.Find(stKey);
    if (pPair != nullptr)
    {
        if (bExactClass)
        {
            if (pPair->m_V.pObj->GetRTTI().IsExactClass(rtti))
            {
                ObjectPtr = pPair->m_V.pObj;
            }
        }
        else if (pPair->m_V.pObj->GetRTTI().IsKindOf(rtti))
        {
            ObjectPtr = pPair->m_V.pObj;
        }
    }
    return (ObjectPtr != nullptr);
}

bool CObjectLinker::Find(CObjectPtr& ObjectPtr, size_t stKey, PCXStr pszClassName)
{
    CSyncLockScope scope(m_SyncLock);
    const MAP_OBJECT_PAIR* pPair = m_Objects.Find(stKey);
    if (pPair != nullptr)
    {
        if (pszClassName != nullptr)
        {
            if (CXChar::Cmp(pPair->m_V.pObj->GetRTTI().GetName(), pszClassName) == 0)
            {
                ObjectPtr = pPair->m_V.pObj;
            }
        }
        else
        {
            ObjectPtr = pPair->m_V.pObj;
        }
    }
    return (ObjectPtr != nullptr);
}

bool CObjectLinker::GetName(PINDEX index, CString& strName)
{
    CSyncLockScope scope(m_SyncLock);
    strName = m_Objects[index].szName;
    return true;
}

bool CObjectLinker::GetName(PINDEX index, CStringKey& strNameFix)
{
    CSyncLockScope scope(m_SyncLock);
    strNameFix = m_Objects[index].szName;
    return true;
}

bool CObjectLinker::GetName(PINDEX index, PXStr pszName, size_t stLen)
{
    CSyncLockScope scope(m_SyncLock);
    CXChar::Copy(pszName, stLen, m_Objects[index].szName);
    return true;
}

size_t CObjectLinker::SetName(PINDEX& index, PCXStr pszNewName)
{
    CSyncLockScope scope(m_SyncLock);
    size_t stKey = CHash::Hash(pszNewName);

    PINDEX inxObj = m_Objects.FindIndex(stKey);
    if (inxObj != nullptr)
    {
        return 0;
    }
    inxObj = m_Objects.Add(stKey, m_Objects[index]);
    if (inxObj == nullptr)
    {
        return 0;
    }
    m_Objects.RemoveAt(index);
    index = inxObj;
    return stKey;
}

bool CObjectLinker::GetModule(PINDEX inxModule, CString& strPath)
{
    CSyncLockScope scope(m_SyncLock);
    MAP_MODULE_PAIR* pPair = m_Modules.GetAt(inxModule);
    if (pPair != nullptr)
    {
        strPath = pPair->m_V.strPath;
        return true;
    }
    return false;
}

#ifdef __RUNTIME_DEBUG__
void CObjectLinker::ExitDump(void)
{
    DEV_DUMP(TF("*-Object-Linker dump, Un-release Object Count = %d-"), m_Objects.GetSize());

    PINDEX index = m_Objects.GetFirstIndex();
    while (index != nullptr)
    {
        const MAP_OBJECT_PAIR* pPair = m_Objects.GetNext(index);
        TRY_EXCEPTION
            DEV_DUMP(TF("| Un-release Object : Object Name-%s[Class-%s, flag=%x]"), pPair->m_V.szName, pPair->m_V.pObj->GetRTTI().GetName(), pPair->m_V.stFlag);
        CATCH_EXCEPTION
        END_EXCEPTION
    }
}
#endif  // __RUNTIME_DEBUG__

///////////////////////////////////////////////////////////////////
// CObject
DEF_RTTI_TYPE_ROOT( CObject, (CRTTI::TRAITS_OBJECT|CRTTI::TRAITS_ABSTRACT) )
#ifndef __MODERN_CXX_NOT_SUPPORTED
    DEF_TYPE_SFUNCV( StaticLinkName,      CObject*, CObject, StaticLink,   PCXStr, PCXStr, PCXStr, size_t );
    DEF_TYPE_SFUNCV( StaticLinkUUID,      CObject*, CObject, StaticLink,   PCXStr, const CUUID&, PCXStr, size_t );
    DEF_TYPE_SFUNCV( StaticUnlinkName,    bool,     CObject, StaticUnlink, PCXStr, PCXStr );
    DEF_TYPE_SFUNCV( StaticUnlinkUUID,    bool,     CObject, StaticUnlink, PCXStr, const CUUID& );
    DEF_TYPE_SFUNCV( StaticFindClass,     Int,      CObject, StaticFind,   CTArray<CObjectPtr>&, PCXStr );
    DEF_TYPE_SFUNCV( StaticFindRTTI,      Int,      CObject, StaticFind,   CTArray<CObjectPtr>&, const CRTTI&, bool );
    DEF_TYPE_SFUNCV( StaticFindName,      bool,     CObject, StaticFind,   CObjectPtr&, PCXStr, const CRTTI&, bool );
    DEF_TYPE_SFUNCV( StaticFindUUID,      bool,     CObject, StaticFind,   CObjectPtr&, const CUUID&, const CRTTI&, bool );
    DEF_TYPE_SFUNCV( StaticFindNameClass, bool,     CObject, StaticFind,   CObjectPtr&, PCXStr, PCXStr );
    DEF_TYPE_SFUNCV( StaticFindUUIDClass, bool,     CObject, StaticFind,   CObjectPtr&, const CUUID&, PCXStr );
    DEF_TYPE_CFUNC( GetKey, PINDEX, CObject, GetKey );
    DEF_TYPE_CFUNCV( GetName,   bool, CObject, GetName,   CString& );
    DEF_TYPE_CFUNCV( GetModule, bool, CObject, GetModule, CString& );
    DEF_TYPE_FUNCV( Link,     bool, CObject, Link, PCXStr );
    DEF_TYPE_FUNCV( LinkUUID, bool, CObject, Link, const CUUID& );
    DEF_TYPE_FUNC( Unlink, void, CObject, Unlink );
#else
    DEF_TYPE_SFUNCV( StaticLinkName,      CObject*, CObject, StaticLink,   PCXStr pszClass, PCXStr pszName, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED );
    DEF_TYPE_SFUNCV( StaticLinkUUID,      CObject*, CObject, StaticLink,   PCXStr pszClass, const CUUID& uuid, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED );
    DEF_TYPE_SFUNCV( StaticUnlinkName,    bool,     CObject, StaticUnlink, PCXStr pszClass, PCXStr pszName = nullptr );
    DEF_TYPE_SFUNCV( StaticUnlinkUUID,    bool,     CObject, StaticUnlink, PCXStr pszClass, const CUUID& uuid );
    DEF_TYPE_SFUNCV( StaticFindClass,     Int,      CObject, StaticFind,   CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName = nullptr );
    DEF_TYPE_SFUNCV( StaticFindRTTI,      Int,      CObject, StaticFind,   CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );
    DEF_TYPE_SFUNCV( StaticFindName,      bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, PCXStr pszName, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );
    DEF_TYPE_SFUNCV( StaticFindUUID,      bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, const CUUID& uuid, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );
    DEF_TYPE_SFUNCV( StaticFindNameClass, bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, PCXStr pszName, PCXStr pszClassName = nullptr );
    DEF_TYPE_SFUNCV( StaticFindUUIDClass, bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, const CUUID& uuid, PCXStr pszClassName = nullptr );
    DEF_TYPE_CFUNC( GetKey, PINDEX, CObject, GetKey );
    DEF_TYPE_CFUNCV( GetName,   bool, CObject, GetName, CString& strName );
    DEF_TYPE_CFUNCV( GetModule, bool, CObject, GetModule, CString& strPath );
    DEF_TYPE_FUNCV( Link,     bool, CObject, Link, PCXStr pszName );
    DEF_TYPE_FUNCV( LinkUUID, bool, CObject, Link, const CUUID& uuid );
    DEF_TYPE_FUNC( Unlink, void, CObject, Unlink );
#endif
DEF_TYPE_END( CObject )

CObject* CObject::StaticLink(PCXStr pszClass, PCXStr pszName, PCXStr pszModule, size_t stFlag)
{
    if ((pszClass == nullptr) || (*pszClass == 0) || 
        (pszName == nullptr) || (*pszName == 0) || 
        (SObjectLinker::GetInstance() == nullptr))
    {
        return nullptr;
    }
    CModuleHandle ModuleHandle;
    if (ModuleHandle.Load(pszModule) == false)
    {
        return nullptr;
    }

    CObject* pObj = CRTTI::CreateByName(pszClass);
    if (pObj != nullptr)
    {
        pObj->m_inxKey = SObjectLinker::GetInstance()->Load(pObj->m_inxModule, pObj, pszName, stFlag, pszModule, ModuleHandle);
        if (pObj->m_inxKey != nullptr)
        {
            ModuleHandle.Detach();
        }
        else
        {
            MDELETE pObj;
            pObj = nullptr;
        }
    }
    return pObj;
}

CObject* CObject::StaticLink(PCXStr pszClass, const CUUID& uuid, PCXStr pszModule, size_t stFlag)
{
    if (SObjectLinker::GetInstance() == nullptr)
    {
        return nullptr;
    }
    CModuleHandle ModuleHandle;
    if (ModuleHandle.Load(pszModule) == false)
    {
        return nullptr;
    }

    CObject* pObj = CRTTI::CreateByName(pszClass);
    if (pObj != nullptr)
    {
        pObj->m_inxKey = SObjectLinker::GetInstance()->Load(pObj->m_inxModule, pObj, uuid, stFlag, pszModule, ModuleHandle);
        if (pObj->m_inxKey != nullptr)
        {
            ModuleHandle.Detach();
        }
        else
        {
            MDELETE pObj;
            pObj = nullptr;
        }
    }
    return pObj;
}


bool CObject::StaticUnlink(PCXStr pszClass, PCXStr)
{
    if (SObjectLinker::GetInstance() != nullptr)
    {
        SObjectLinker::GetInstance()->Unload(pszClass);
        return true;
    }
    return false;
}

bool CObject::StaticUnlink(PCXStr pszClass, const CUUID&)
{
    if (SObjectLinker::GetInstance() != nullptr)
    {
        SObjectLinker::GetInstance()->Unload(pszClass);
        return true;
    }
    return false;
}

Int CObject::StaticFind(CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName)
{
    if ((pszClassName == nullptr) || (SObjectLinker::GetInstance() == nullptr))
    {
        return 0;
    }
    return SObjectLinker::GetInstance()->Find(ObjectPtrs, pszClassName);
}

Int CObject::StaticFind(CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti, bool bExactClass)
{
    if (SObjectLinker::GetInstance() == nullptr)
    {
        return 0;
    }
    return SObjectLinker::GetInstance()->Find(ObjectPtrs, rtti, bExactClass);
}

bool CObject::StaticFind(CObjectPtr& ObjectPtr, PCXStr pszName, const CRTTI& rtti, bool bExactClass)
{
    if (SObjectLinker::GetInstance() == nullptr)
    {
        return false;
    }
    size_t stKey = CHash::Hash(pszName);
    return SObjectLinker::GetInstance()->Find(ObjectPtr, stKey, rtti, bExactClass);
}

bool CObject::StaticFind(CObjectPtr& ObjectPtr, const CUUID& uuid, const CRTTI& rtti, bool bExactClass)
{
    if (SObjectLinker::GetInstance() == nullptr)
    {
        return false;
    }
    CString strUUID;
    uuid.ToString(strUUID);
    size_t stKey = CHash::Hash(*strUUID);
    return SObjectLinker::GetInstance()->Find(ObjectPtr, stKey, rtti, bExactClass);
}

bool CObject::StaticFind(CObjectPtr& ObjectPtr, PCXStr pszName, PCXStr pszClassName)
{
    if ((pszClassName == nullptr) || (SObjectLinker::GetInstance() == nullptr))
    {
        return false;
    }
    size_t stKey = CHash::Hash(pszName);
    return SObjectLinker::GetInstance()->Find(ObjectPtr, stKey, pszClassName);
}

bool CObject::StaticFind(CObjectPtr& ObjectPtr, const CUUID& uuid, PCXStr pszClassName)
{
    if ((pszClassName == nullptr) || (SObjectLinker::GetInstance() == nullptr))
    {
        return false;
    }
    CString strUUID;
    uuid.ToString(strUUID);
    size_t stKey = CHash::Hash(*strUUID);
    return SObjectLinker::GetInstance()->Find(ObjectPtr, stKey, pszClassName);
}

CObject::CObject(void)
: m_inxKey(nullptr)
, m_inxModule(nullptr)
{
}

CObject::~CObject(void)
{
    if (m_inxKey != nullptr)
    {
        Unlink();
        m_inxKey = nullptr;
    }
}

UInt CObject::Init(void)
{
    return (UInt) RET_OKAY;
}

void CObject::Exit(void)
{
}

void CObject::Serialize(CStream&)
{
}

PINDEX CObject::GetKey(void) const
{
    return m_inxKey;
}

bool CObject::GetName(CString& strName) const
{
    if ((m_inxKey != nullptr) && (SObjectLinker::GetInstance() != nullptr))
    {
        return SObjectLinker::GetInstance()->GetName(m_inxKey, strName);
    }
    return false;
}

bool CObject::GetModule(CString& strPath) const
{
    if (m_inxModule != nullptr)
    {
        assert(SObjectLinker::GetInstance() != nullptr);
        return SObjectLinker::GetInstance()->GetModule(m_inxModule, strPath);
    }
    return false;
}

bool CObject::Link(const CUUID& uuid)
{
    if (m_inxKey == nullptr)
    {
        if (SObjectLinker::GetInstance() == nullptr)
        {
            return false;
        }
        m_inxKey = SObjectLinker::GetInstance()->Link(this, uuid);
        if (m_inxKey == nullptr)
        {
            return false;
        }
    }
    return true;
}

bool CObject::Link(PCXStr pszName)
{
    if (m_inxKey == nullptr)
    {
        if (SObjectLinker::GetInstance() == nullptr)
        {
            return false;
        }
        m_inxKey = SObjectLinker::GetInstance()->Link(this, pszName);
        if (m_inxKey == nullptr)
        {
            return false;
        }
    }
    return true;
}

void CObject::Unlink(void)
{
    if ((m_inxKey != nullptr) && (SObjectLinker::GetInstance() != nullptr))
    {
        SObjectLinker::GetInstance()->Unlink(m_inxKey);
    }
    m_inxKey    = nullptr;
    m_inxModule = nullptr;
}

///////////////////////////////////////////////////////////////////
// CSlotObject
CSlotObject::CSlotObject(CObject* pObj, uintptr_t utFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(utFunc)
, m_stType(stType)
{
    assert(pObj != nullptr);
    m_strClass = pObj->GetRTTI().GetName();
    pObj->GetName(m_strName);
    m_pObject = pObj;
    m_pRTTI   = &(pObj->GetRTTI());
}

CSlotObject::CSlotObject(CObject* pObj, CPCXStr pszFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(0)
, m_stType(stType)
{
    m_strFunc = pszFunc;
    assert(pObj != nullptr);
    m_strClass = pObj->GetRTTI().GetName();
    m_pObject = pObj;
    m_pRTTI   = &(pObj->GetRTTI());
}

CSlotObject::CSlotObject(const CRTTI& rtti, uintptr_t utFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(utFunc)
, m_stType(stType)
{
    m_strClass = rtti.GetName();
    // class static func
    m_pRTTI = &rtti;
}

CSlotObject::CSlotObject(const CRTTI& rtti, CPCXStr pszFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(0)
, m_stType(stType)
{
    m_strFunc = pszFunc;
    m_strClass = rtti.GetName();
    // class static func
    m_pRTTI = &rtti;
}

CSlotObject::CSlotObject(PCXStr pszClass, PCXStr pszName, uintptr_t utFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(utFunc)
, m_stType(stType)
{
    m_strClass = pszClass;
    m_strName  = pszName;
}

CSlotObject::CSlotObject(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
: m_pRTTI(nullptr)
, m_pObject(nullptr)
, m_utFunc(0)
, m_stType(stType)
{
    m_strFunc  = pszFunc;
    m_strClass = pszClass;
    m_strName  = pszName;
}

CSlotObject::~CSlotObject(void)
{
    m_pRTTI   = nullptr;
    m_pObject = nullptr;
    DEV_DEBUG(TF("  CSlotObject[type=%x] destructor class-name = %s, inst-name=%s, func-name=%s"), m_stType, *m_strClass, *m_strName, *m_strFunc);
}

CSlotObject::CSlotObject(const CSlotObject& aSrc)
: m_pRTTI(aSrc.m_pRTTI)
, m_pObject(aSrc.m_pObject)
, m_utFunc(aSrc.m_utFunc)
, m_stType(aSrc.m_stType)
, m_strFunc(aSrc.m_strFunc)
, m_strClass(aSrc.m_strClass)
, m_strName(aSrc.m_strName)
{
}

CSlotObject& CSlotObject::operator=(const CSlotObject& aSrc)
{
    if (this != &aSrc)
    {
        m_pRTTI    = aSrc.m_pRTTI;
        m_pObject  = aSrc.m_pObject;
        m_utFunc   = aSrc.m_utFunc;
        m_stType   = aSrc.m_stType;
        m_strFunc  = aSrc.m_strFunc;
        m_strClass = aSrc.m_strClass;
        m_strName  = aSrc.m_strName;
    }
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
CSlotObject::CSlotObject(CSlotObject&& aSrc)
: m_pRTTI(aSrc.m_pRTTI)
, m_pObject(aSrc.m_pObject)
, m_utFunc(aSrc.m_utFunc)
, m_stType(aSrc.m_stType)
, m_strFunc(std::move(aSrc.m_strFunc))
, m_strClass(std::move(aSrc.m_strClass))
, m_strName(std::move(aSrc.m_strName))
{
}

CSlotObject& CSlotObject::operator=(CSlotObject&& aSrc)
{
    if (this != &aSrc)
    {
        m_pRTTI    = aSrc.m_pRTTI;
        m_pObject  = aSrc.m_pObject;
        m_utFunc   = aSrc.m_utFunc;
        m_stType   = aSrc.m_stType;
        m_strFunc  = std::move(aSrc.m_strFunc);
        m_strClass = std::move(aSrc.m_strClass);
        m_strName  = std::move(aSrc.m_strName);
    }
    return (*this);
}
#endif

bool CSlotObject::operator==(const CSlotObject& aSrc)
{
    // TODO...
    return false;
}

CObjectPtr CSlotObject::GetObjectPtr(void)
{
    CObjectPtr ObjectPtr;
    ObjectPtr = m_pObject;
    return ObjectPtr;
}

const CRTTI* CSlotObject::GetRTTI(void)
{
    return m_pRTTI;
}

PCXStr CSlotObject::GetClass(void) const
{
    return m_strClass.GetBuffer();
}

PCXStr CSlotObject::GetName(void) const
{
    return m_strName.GetBuffer();
}

PCXStr CSlotObject::GetFuncName(void) const
{
    return m_strFunc.GetBuffer();
}

uintptr_t CSlotObject::GetFunc(void) const
{
    return m_utFunc;
}

size_t CSlotObject::GetType(void) const
{
    return m_stType;
}

bool CSlotObject::IsEvent(void) const
{
    return ((m_stType & (VART_EVENTQUEUE | VART_EVENTTICK)) != 0);
}

bool CSlotObject::IsEventQueue(void) const
{
    return ((m_stType & VART_EVENTQUEUE) != 0);
}

bool CSlotObject::IsEventTick(void) const
{
    return ((m_stType & VART_EVENTTICK) != 0);
}

bool CSlotObject::IsRTTIType(void) const
{
    return ((m_stType & VART_RTTITYPE) != 0);
}

bool CSlotObject::IsRTTIDynamic(void) const
{
    return ((m_stType & VART_RTTIDYNAMIC) != 0);
}

bool CSlotObject::IsFunction(void) const
{
    return ((m_stType & VART_FUNCTION) != 0);
}

bool CSlotObject::IsConst(void) const
{
    return ((m_stType & VART_CONST) != 0);
}

bool CSlotObject::IsStatic(void) const
{
    return ((m_stType & VART_STATIC) != 0);
}

