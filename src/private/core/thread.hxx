// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __THREAD_HXX__
#define __THREAD_HXX__

#pragma once

#include "container.h"
#include "singleton.h"
#include "thread.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CThreadPool
class CRunnableThread;
class CQueueTaskThread;
class CThreadPool : public MObject
{
public:
    enum THREAD_CONST
    {
        THREAD_QUEUETASK_INIT  = 1,
        THREAD_QUEUETASK_TIMES = 2,
        THREAD_RUNNABLE_MAX    = 64,
        THREAD_WAIT_TIME       = 100,
    };
    typedef CTList<CRunnableThread*>        LST_RUNNABLE_THREAD;
    typedef CTList<CQueueTaskThread*>       LST_QUEUETASK_THREAD;

    typedef CTQueue<CQueueTask*>            QUE_QUEUE_TASK;
public:
    static TRet THREAD_FUNC StaticRoutine(TParam);
    static void DefRoutine(TParam);
public:
    UInt   Init(void);
    void   Exit(void);

    bool   StartRunnable(CRunnable* pRunnable, Int nPriority, Int nPolicy);
    void   StopRunnable(CRunnableThread* pThread);

    bool   RunTask(CQueueTask* pQueueTask);
    void   FinishTask(CQueueTaskThread* pThread);

    void   SetTaskThread(UInt uThreadCount);
protected:
    CThreadPool(void);
    ~CThreadPool(void);
private:
    CThreadPool(const CThreadPool&);
    CThreadPool& operator=(const CThreadPool&);

    void   RemoveRunnableThreads(void);
    void   RemoveQueueTaskThreads(LST_QUEUETASK_THREAD& LstThreads);
    void   RemoveQueueTasks(void);

    bool   WakeQueueTaskThread(CQueueTaskThread*& pThread);
    bool   IdleQueueTaskThread(CQueueTaskThread* pThread);
    bool   CheckQueueTaskThread(CQueueTaskThread* pThread);

    bool   GetQueueTask(CQueueTask*& pTask);

    void   DecrQueueTaskThread(UInt uThreadCount);
    void   IncrQueueTaskThread(UInt uThreadCount);
private:
    UInt                   m_uEnabled;
    UInt                   m_uMaxQueueTaskThread;

    LST_RUNNABLE_THREAD    m_RunnableThreads;
    LST_QUEUETASK_THREAD   m_TaskIdleThreads;
    LST_QUEUETASK_THREAD   m_TaskWakeThreads;

    QUE_QUEUE_TASK         m_QueueTasks;

    CSyncLock              m_TPLockTRunnable;
    CSyncLock              m_TPLockTQueue;
    CSyncLock              m_TPLockTask;
public:
    static UInt            ms_uStartCount;
    static UInt            ms_uRunningCount;
    static UInt            ms_uStopCount;
    static UInt            ms_uKillCount;
};

typedef CTSingleton<CThreadPool> SThreadPool;

///////////////////////////////////////////////////////////////////
// CRunnableThread
class CRunnableThread : public CThread
{
    friend class CThreadPool;
public:
    CRunnableThread(void);
    virtual ~CRunnableThread(void);

    PINDEX  GetIndex(void);
    void    SetIndex(PINDEX index);

    bool    Add(CRunnable* pRunnable);
    void    Finish(void);
protected:
    virtual bool OnStart(void) OVERRIDE;
    virtual void OnStop(void) OVERRIDE;
    virtual void OnKill(void) OVERRIDE;
    virtual bool Run(void) OVERRIDE;
private:
    CRunnableThread(const CRunnableThread&);
    CRunnableThread& operator=(const CRunnableThread&);

    void    Routine(void);
private:
    PINDEX       m_Index;
    CRunnable*   m_pRunning;
};

///////////////////////////////////////////////////////////////////
// CQueueTaskThread
class CQueueTaskThread : public CThread
{
    friend class CThreadPool;
public:
    CQueueTaskThread(void);
    virtual ~CQueueTaskThread(void);

    virtual bool Stop(UInt uWait = (UInt)TIMET_INFINITE) OVERRIDE;

    PINDEX  GetIndex(void);
    void    SetIndex(PINDEX index);

    bool    Add(CQueueTask* pQueueTask);
    void    Finish(void);
protected:
    virtual bool OnStart(void) OVERRIDE;
    virtual void OnKill(void) OVERRIDE;
    virtual bool Run(void) OVERRIDE;
private:
    CQueueTaskThread(const CQueueTaskThread&);
    CQueueTaskThread& operator=(const CQueueTaskThread&);

    void    Routine(void);
private:
    PINDEX        m_Index;
    CQueueTask*   m_pQueueTask;
    CSyncEvent    m_SyncEvent; // auto-event
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __THREAD_HXX__
