// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __RTTI_HXX__
#define __RTTI_HXX__

#pragma once

#include "sync.h"
#include "object.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CRTTILinker
class CRTTILinker : public MObject
{
public:
    CRTTILinker(void);
    ~CRTTILinker(void);

    bool   Init(void);
    void   Exit(void);

    void   Link(CRTTI* pRTTI);
    void   Unlink(CRTTI* pRTTI);

    CRTTI* Find(PCXStr pszName);

    bool            InitType(void);
    void            ExitType(void);

    bool            RemoveType(const CRTTI& rtti);

    Int             GetEnumSize(const CRTTI& rtti);
    Int             GetVarSize(const CRTTI& rtti);
    Int             GetFuncSize(const CRTTI& rtti);

    const CRTTIEnum*      GetEnum(const CRTTI& rtti, PCXStr pszName);
    const CRTTIType*      GetVar(const CRTTI& rtti, PCXStr pszName);
    const CRTTIType*      GetFunc(const CRTTI& rtti, PCXStr pszName);

    const MAP_RTTI_ENUM&  GetEnumMap(const CRTTI& rtti);
    const MAP_RTTI_VAR&   GetVarMap(const CRTTI& rtti);
    const MAP_RTTI_FUNC&  GetFuncMap(const CRTTI& rtti);

    bool            SetEnumMap(const CRTTI& rtti, MAP_RTTI_ENUM& EnumMap);
    bool            SetVarMap(const CRTTI& rtti, MAP_RTTI_VAR& VarMap);
    bool            SetFuncMap(const CRTTI& rtti, MAP_RTTI_FUNC& FuncMap);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool            SetEnumMap(const CRTTI& rtti, MAP_RTTI_ENUM&& EnumMap);
    bool            SetVarMap(const CRTTI& rtti, MAP_RTTI_VAR&& VarMap);
    bool            SetFuncMap(const CRTTI& rtti, MAP_RTTI_FUNC&& FuncMap);
#endif
private:
    CRTTILinker(const CRTTILinker&);
    CRTTILinker& operator=(const CRTTILinker&);
private:
    typedef CTMap<CString, CRTTI*>                MAP_RTTI, *PMAP_RTTI;
    typedef CTMap<CString, CRTTI*>::PAIR          PAIR_RTTI;

    typedef CTMap<CString, MAP_RTTI_TYPE>         MAP_REGTYPE, *PMAP_REGTYPE;
    typedef CTMap<CString, MAP_RTTI_TYPE>::PAIR   PAIR_MAP_REGTYPE;
private:
    const MAP_RTTI_TYPE   mc_TypeNone;
private:
    MAP_RTTI      m_RTTIs;
    CSyncLock     m_RTTILock;

    MAP_REGTYPE   m_TypeEnums;
    MAP_REGTYPE   m_TypeVars;
    MAP_REGTYPE   m_TypeFuncs;
    CSyncLock     m_TypeLock;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __RTTI_HXX__