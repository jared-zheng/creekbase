// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CHashCRCImp
// http://en.wikipedia.org/wiki/Cyclic_redundancy_check
CHashCRCImp::CHashCRCImp(void)
: m_puTable32(nullptr)
, m_pullTable64(nullptr)
//// CRC-32-IEEE 802.3
//, m_uSeed32((UInt)0x04C11DB7) // normal
, m_uSeed32((UInt)0xEDB88320) // reversed
//, m_uSeed32((UInt)0x82608EDB) // reverse of reciprocal
//// CRC-64-ECMA-182
//, m_ullSeed64((ULLong)0x42F0E1EBA9EA3693) // normal
, m_ullSeed64((ULLong)0xC96C5795D7870F42) // reversed
//, m_ullSeed64((ULLong)0xA17870F5D4F51B49) // reverse of reciprocal
{
}

CHashCRCImp::~CHashCRCImp(void)
{
}

CHashCRCImp::CHashCRCImp(const CHashCRCImp&)
: m_puTable32(nullptr)
, m_pullTable64(nullptr)
//// CRC-32-IEEE 802.3
//, m_uSeed32((UInt)0x04C11DB7) // normal
, m_uSeed32((UInt)0xEDB88320) // reversed
//, m_uSeed32((UInt)0x82608EDB) // reverse of reciprocal
//// CRC-64-ECMA-182
//, m_ullSeed64((ULLong)0x42F0E1EBA9EA3693) // normal
, m_ullSeed64((ULLong)0xC96C5795D7870F42) // reversed
//, m_ullSeed64((ULLong)0xA17870F5D4F51B49) // reverse of reciprocal
{
}

CHashCRCImp& CHashCRCImp::operator=(const CHashCRCImp&)
{
    return (*this);
}

bool CHashCRCImp::Init(void)
{
    assert(m_puTable32 == nullptr);
    assert(m_pullTable64 == nullptr);
    size_t stPage  = (size_t)GPlatform.GetPageSize();
    size_t stAlloc = DEF::Align<size_t>(CRCT_TABLESIZE * sizeof(UInt), stPage);
    m_puTable32    = reinterpret_cast<PUInt>(GPlatform.Alloc(stAlloc));
    stAlloc        = DEF::Align<size_t>(CRCT_TABLESIZE * sizeof(ULLong), stPage);
    m_pullTable64  = reinterpret_cast<PULLong>(GPlatform.Alloc(stAlloc));
    if ((m_puTable32 != nullptr) && (m_pullTable64 != nullptr))
    {
        UInt   i   = 0;
        UInt   j   = 0;
        UInt   u   = 0;
        ULLong ull = 0;
        for (i = 0; i < CRCT_TABLECOL; ++i)
        {
            u = i;
            // generate a crc for every 8-bit value
            for (j = 0; j < CRCT_SEEDBITS; ++j)
            {
                u = (u & 1) ? (m_uSeed32 ^ (u >> 1)) : (u >> 1);
            }
            m_puTable32[i] = u;

            ull = (ULLong)i;
            // generate a crc for every 8-bit value
            for (j = 0; j < CRCT_SEEDBITS; ++j)
            {
                ull = (ull & 1) ? (m_ullSeed64 ^ (ull >> 1)) : (ull >> 1);
            }
            m_pullTable64[i] = ull;
        }
        // generate crc for each value followed by one, two, and three zeros,
        // and then the byte reversal of those as well as the first table
        for (i = 0; i < CRCT_TABLECOL; ++i)
        {
            u = m_puTable32[i];
            m_puTable32[CRCT_SEEDBITSHALF * CRCT_TABLECOL + i] = Reverse32(u);
            for (j = 1; j < CRCT_SEEDBITSHALF; ++j)
            {
                u = m_puTable32[u & CRCT_TABLECOLMASK] ^ (u >> CRCT_SEEDBITS);
                m_puTable32[j * CRCT_TABLECOL + i] = u;
                m_puTable32[(j + CRCT_SEEDBITSHALF) * CRCT_TABLECOL + i] = Reverse32(u);
            }

            ull = m_pullTable64[i];
            m_pullTable64[CRCT_SEEDBITSHALF * CRCT_TABLECOL + i] = Reverse64(ull);
            for (j = 1; j < CRCT_SEEDBITSHALF; ++j)
            {
                ull = m_pullTable64[ull & CRCT_TABLECOLMASK] ^ (ull >> CRCT_SEEDBITS);
                m_pullTable64[j * CRCT_TABLECOL + i] = ull;
                m_pullTable64[(j + CRCT_SEEDBITSHALF) * CRCT_TABLECOL + i] = Reverse64(ull);
            }
        }
        /////////////////////////////////////////////////////////////////////
        //for (i = 0; i < CRCT_SEEDBITS; i++)
        //{
        //    for (j = 0; j < CRCT_TABLECOL; j += CRCT_SEEDBITS)
        //    {
        //        FOUT(TF("%08X, %08X, %08X, %08X, %08X, %08X, %08X, %08X\n"),
        //             m_puTable32[i * CRCT_TABLECOL + j + 0],
        //             m_puTable32[i * CRCT_TABLECOL + j + 1],
        //             m_puTable32[i * CRCT_TABLECOL + j + 2],
        //             m_puTable32[i * CRCT_TABLECOL + j + 3],
        //             m_puTable32[i * CRCT_TABLECOL + j + 4],
        //             m_puTable32[i * CRCT_TABLECOL + j + 5],
        //             m_puTable32[i * CRCT_TABLECOL + j + 6],
        //             m_puTable32[i * CRCT_TABLECOL + j + 7]);
        //    }
        //}
        //for (i = 0; i < CRCT_SEEDBITS; i++)
        //{
        //    for (j = 0; j < CRCT_TABLECOL; j += CRCT_SEEDBITS)
        //    {
        //        FOUT(TF("%16llX, %16llX, %16llX, %16llX, ")
        //             TF("%16llX, %16llX, %16llX, %16llX\n")
        //             m_pullTable64[i * CRCT_TABLECOL + j + 0],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 1],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 2],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 3],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 4],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 5],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 6],
        //             m_pullTable64[i * CRCT_TABLECOL + j + 7]);
        //    }
        //}
        /////////////////////////////////////////////////////////////////////
        DEV_DUMP(TF("#---HASH CRC32&64 Table[%d] init okay---"), CRCT_TABLESIZE);
        return true;
    }
    DEV_WARN(TF("#---HASH CRC32&64 Table init failed---"));
    return false;
}

void CHashCRCImp::Exit(void)
{
    size_t stPage  = (size_t)GPlatform.GetPageSize();
    size_t stAlloc = DEF::Align<size_t>(CRCT_TABLESIZE * sizeof(UInt), stPage);
    if (m_puTable32 != nullptr)
    {
        GPlatform.Free(m_puTable32, stAlloc);
        m_puTable32 = nullptr;
    }
    stAlloc        = DEF::Align<size_t>(CRCT_TABLESIZE * sizeof(ULLong), stPage);
    if (m_pullTable64 != nullptr)
    {
        GPlatform.Free(m_pullTable64, stAlloc);
        m_pullTable64 = nullptr;
    }
    StatsReport();
    DEV_DUMP(TF("#---HASH CRC32&64 Table exit okay---"));
}

UInt CHashCRCImp::String32(PCStr pszString, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_puTable32 != nullptr) && (pszString != nullptr))
    {
        Char C      = 0;
        UInt uType  = 0;
        UInt uSeed1 = (UInt)CRCT_SEED1;
        UInt uSeed2 = (UInt)CRCT_SEED2;

        size_t stSize = CChar::Length(pszString);
        if (stSize < stLen)
        {
            stLen = stSize;
        }
        for (size_t i = 0; i < stLen; ++i)
        {
            C = pszString[i];

            uType  = (UInt)(C << CRCT_MOV1);
            uSeed1 = m_puTable32[(uType + C) & CRCT_TABLESIZEMASK] ^ (uSeed1 + uSeed2);
            uSeed2 = C + uSeed1 + uSeed2 + (uSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        return uSeed1;
    }
    return 0;
}

UInt CHashCRCImp::String32(PCWStr pszString, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_puTable32 != nullptr) && (pszString != nullptr))
    {
        WChar C      = 0;
        UInt  uType  = 0;
        UInt  uSeed1 = (UInt)CRCT_SEED1;
        UInt  uSeed2 = (UInt)CRCT_SEED2;

        size_t stSize = CWChar::Length(pszString);
        if (stSize < stLen)
        {
            stLen = stSize;
        }
        for (size_t i = 0; i < stLen; ++i)
        {
            C = pszString[i];

            uType  = (UInt)(C << CRCT_MOV1);
            uSeed1 = m_puTable32[(uType + C) & CRCT_TABLESIZEMASK] ^ (uSeed1 + uSeed2);
            uSeed2 = C + uSeed1 + uSeed2 + (uSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        return uSeed1;
    }
    return 0;
}

UInt CHashCRCImp::Buffer32(PByte pBuffer, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_puTable32 != nullptr) && (pBuffer != nullptr))
    {
        size_t stQuot   = (stLen / sizeof(UInt));
        size_t stRemain = (stLen - stQuot * sizeof(UInt));
        PUInt  pBuf32   = (PUInt)pBuffer;

        UInt   B        = 0;
        UInt   uType    = 0;
        UInt   uSeed1   = (UInt)CRCT_SEED1;
        UInt   uSeed2   = (UInt)CRCT_SEED2;

        for (size_t i = 0; i < stQuot; ++i)
        {
            B = pBuf32[i];

            uSeed1 = m_puTable32[B & CRCT_TABLESIZEMASK] ^ (B);
            uSeed2 = B + uSeed1 + uSeed2 + (uSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        pBuffer = (PByte)(pBuf32 + stQuot);
        for (size_t i = 0; i < stRemain; ++i)
        {
            B = pBuffer[i];

            uType  = (UInt)(B << CRCT_MOV1);
            uSeed1 = m_puTable32[(uType + B) & CRCT_TABLESIZEMASK] ^ (B + uSeed1 + uSeed2);
            uSeed2 = B + uSeed1 + uSeed2 + (uSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        return uSeed1;
    }
    return 0;
}

ULLong CHashCRCImp::String64(PCStr pszString, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_pullTable64 != nullptr) && (pszString != nullptr))
    {
        Char   C        = 0;
        ULLong ullType  = 0;
        ULLong ullSeed1 = (ULLong)CRCT_SEED1;
        ULLong ullSeed2 = (ULLong)CRCT_SEED2;

        size_t stSize = CChar::Length(pszString);
        if (stSize < stLen)
        {
            stLen = stSize;
        }
        for (size_t i = 0; i < stLen; ++i)
        {
            C = pszString[i];

            ullType  = (ULLong)((ULLong)C << CRCT_MOV1);
            ullSeed1 = m_pullTable64[(ullType + C) & CRCT_TABLESIZEMASK] ^ (ullSeed1 + ullSeed2);
            ullSeed2 = C + ullSeed1 + ullSeed2 + (ullSeed2 << CRCT_MOV2) + (ULLong)CRCT_ADDVAL;
        }
        return ullSeed1;
    }
    return 0;
}

ULLong CHashCRCImp::String64(PCWStr pszString, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_pullTable64 != nullptr) && (pszString != nullptr))
    {
        WChar  C        = 0;
        ULLong ullType  = 0;
        ULLong ullSeed1 = (ULLong)CRCT_SEED1;
        ULLong ullSeed2 = (ULLong)CRCT_SEED2;

        size_t stSize = CWChar::Length(pszString);
        if (stSize < stLen)
        {
            stLen = stSize;
        }
        for (size_t i = 0; i < stLen; ++i)
        {
            C = pszString[i];

            ullType  = (ULLong)((ULLong)C << CRCT_MOV1);
            ullSeed1 = m_pullTable64[(ullType + C) & CRCT_TABLESIZEMASK] ^ (ullSeed1 + ullSeed2);
            ullSeed2 = C + ullSeed1 + ullSeed2 + (ullSeed2 << CRCT_MOV2) + (ULLong)CRCT_ADDVAL;
        }
        return ullSeed1;
    }
    return 0;
}

ULLong CHashCRCImp::Buffer64(PByte pBuffer, size_t stLen)
{
    HASH_STATS_SCOPE StatsScope(m_HashStats);
    if ((m_pullTable64 != nullptr) && (pBuffer != nullptr))
    {
        size_t  stQuot   = (stLen / sizeof(ULLong));
        size_t  stRemain = (stLen - stQuot * sizeof(ULLong));
        PULLong pBuf64   = (PULLong)pBuffer;

        ULLong  B        = 0;
        ULLong  ullType  = 0;
        ULLong  ullSeed1 = (ULLong)CRCT_SEED1;
        ULLong  ullSeed2 = (ULLong)CRCT_SEED2;

        for (size_t i = 0; i < stQuot; ++i)
        {
            B = pBuf64[i];

            ullSeed1 = m_pullTable64[B & CRCT_TABLESIZEMASK] ^ (B);
            ullSeed2 = B + ullSeed1 + ullSeed2 + (ullSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        pBuffer = (PByte)(pBuf64 + stQuot);
        for (size_t i = 0; i < stRemain; ++i)
        {
            B = pBuffer[i];

            ullType  = (UInt)(B << CRCT_MOV1);
            ullSeed1 = m_pullTable64[(ullType + B) & CRCT_TABLESIZEMASK] ^ (B + ullSeed1 + ullSeed2);
            ullSeed2 = B + ullSeed1 + ullSeed2 + (ullSeed2 << CRCT_MOV2) + (UInt)CRCT_ADDVAL;
        }
        return ullSeed1;
    }
    return 0;
}

void CHashCRCImp::Dump(HASH_DUMP& Dump)
{
    if (((UInt)Dump.llUsedCount == (UInt)CRCT_SEED1) &&
        ((UInt)Dump.llUsedTick == (UInt)sizeof(UInt)))
    {
        Dump.llUsedCount = (LLong)m_puTable32;
        Dump.llUsedTick  = (LLong)(CRCT_TABLESIZE * sizeof(UInt));
    }
    else if (((UInt)Dump.llUsedCount == (UInt)CRCT_SEED2) &&
             ((UInt)Dump.llUsedTick == (UInt)sizeof(ULLong)))
    {
        Dump.llUsedCount = (LLong)m_pullTable64;
        Dump.llUsedTick  = (LLong)(CRCT_TABLESIZE * sizeof(ULLong));
    }
    else
    {
        HASH_STATS Stats  = m_HashStats;
        Stats.Tick2Time(GPlatform.GetBaseTick());
        Dump.llUsedCount = Stats.llUsedCount;
        Dump.llUsedTick  = Stats.llUsedTick;
    }
}

UInt CHashCRCImp::Reverse32(UInt u)
{
    return CPlatform::ByteSwap(u);
}

ULLong CHashCRCImp::Reverse64(ULLong ull)
{
    return CPlatform::ByteSwap(ull);
}

void CHashCRCImp::StatsReport(void)
{
    m_HashStats.Tick2Time(GPlatform.GetBaseTick());
    DEV_DUMP(TF("#---CRC Table Stats Report---"));
    DEV_DUMP(TF("| Used Count : %lld"),       m_HashStats.llUsedCount);
    DEV_DUMP(TF("| Used Time  : %d.%03d[S]"), (Int)(m_HashStats.llUsedTick / TIMET_S2MS), (Int)(m_HashStats.llUsedTick % TIMET_S2MS));
}

///////////////////////////////////////////////////////////////////
// CHash
CHash::CHash(void)
{
}

CHash::~CHash(void)
{
}

CHash::CHash(const CHash&)
{
}

CHash& CHash::operator=(const CHash&)
{
    return (*this);
}

size_t CHash::Hash(Char c)
{
    return (size_t)c;
}

size_t CHash::Hash(UChar uc)
{
    return (size_t)uc;
}

size_t CHash::Hash(Short s)
{
    return (size_t)s;
}

size_t CHash::Hash(UShort us)
{
    return (size_t)us;
}

size_t CHash::Hash(WChar wc)
{
    return (size_t)wc;
}

size_t CHash::Hash(Int n)
{
    return (size_t)n;
}

size_t CHash::Hash(UInt u)
{
    return (size_t)u;
}

size_t CHash::Hash(Long l)
{
    return (size_t)l;
}

size_t CHash::Hash(ULong ul)
{
    return (size_t)ul;
}

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
size_t CHash::Hash(LLong ll)
{
    return (size_t)Hash64(ll);
}

size_t CHash::Hash(ULLong ull)
{
    return (size_t)Hash64(ull);
}

size_t CHash::Hash(void* p)
{
    return (size_t)Hash64(p);
}

size_t CHash::Hash(PCStr pszString, size_t stLen)
{
    return (size_t)Hash64(pszString, stLen);
}

size_t CHash::Hash(PCWStr pszString, size_t stLen)
{
    return (size_t)Hash64(pszString, stLen);
}

size_t CHash::Hash(PByte pBuffer, size_t stLen)
{
    return (size_t)Hash64(pBuffer, stLen);
}
#else
size_t CHash::Hash(LLong ll)
{
    return (size_t)Hash32(ll);
}

size_t CHash::Hash(ULLong ull)
{
    return (size_t)Hash32(ull);
}

size_t CHash::Hash(void* p)
{
    return (size_t)Hash32(p);
}

size_t CHash::Hash(PCStr pszString, size_t stLen)
{
    return (size_t)Hash32(pszString, stLen);
}

size_t CHash::Hash(PCWStr pszString, size_t stLen)
{
    return (size_t)Hash32(pszString, stLen);
}

size_t CHash::Hash(PByte pBuffer, size_t stLen)
{
    return (size_t)Hash32(pBuffer, stLen);
}
#endif

UInt CHash::Hash32(LLong ll)
{
    return (UInt)((UInt)(ll) + (UInt)(ll >> CHashCRCImp::CRCT_BITS) * CHashCRCImp::CRCT_MULVAL);
}

UInt CHash::Hash32(ULLong ull)
{
    return (UInt)((UInt)(ull) + (UInt)(ull >> CHashCRCImp::CRCT_BITS) * CHashCRCImp::CRCT_MULVAL);
}

UInt CHash::Hash32(void* p)
{
    return (UInt)(uintptr_t)p;
}

UInt CHash::Hash32(PCStr pszString, size_t stLen)
{
    return GHash.String32(pszString, stLen);
}

UInt CHash::Hash32(PCWStr pszString, size_t stLen)
{
    return GHash.String32(pszString, stLen);
}

UInt CHash::Hash32(PByte pBuffer, size_t stLen)
{
    return GHash.Buffer32(pBuffer, stLen);
}

ULLong CHash::Hash64(LLong ll)
{
    return (ULLong)ll;
}

ULLong CHash::Hash64(ULLong ull)
{
    return (ULLong)ull;
}

ULLong CHash::Hash64(void* p)
{
    return (ULLong)p;
}

ULLong CHash::Hash64(PCStr pszString, size_t stLen)
{
    return GHash.String64(pszString, stLen);
}

ULLong CHash::Hash64(PCWStr pszString, size_t stLen)
{
    return GHash.String64(pszString, stLen);
}

ULLong CHash::Hash64(PByte pBuffer, size_t stLen)
{
    return GHash.Buffer64(pBuffer, stLen);
}

void CHash::Dump(HASH_DUMP& Dump)
{
    GHash.Dump(Dump);
}
