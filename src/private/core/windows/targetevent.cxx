// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "event.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTickEventImp : thread for CWaitableTimer
CTickEventImp::CTickEventImp(void)
: m_pHandler(nullptr)
, m_inxId(nullptr)
, m_utTick(0)
, m_uCount(0)
, m_uInterval(0)
, m_hTimer(false)
{
}

CTickEventImp::~CTickEventImp(void)
{
    assert(m_pHandler == 0);
}

bool CTickEventImp::Init(CEventHandler& EventHandlerRef, PINDEX index, uintptr_t utTick, UInt uCount, UInt uInterval)
{
    assert(m_utTick == 0);
    //Exit();

    if (m_hTimer.CreateTimer())
    {
        m_pHandler  = &EventHandlerRef;
        m_inxId     = index;
        m_utTick    = utTick;
        m_uCount    = DEF::Max<UInt>((UInt)1, uCount);
        m_uInterval = DEF::Max<UInt>((UInt)TICK_TIME_MIN, uInterval);
        return Start();
    }
    DEV_DEBUG(TF("  !!!Tick Event(%lld) Init Timer handle invalid"), utTick);
    return false;
}

bool CTickEventImp::Exit(bool bKill)
{
    if (IsRunning())
    {
        m_uCount = 0;
        LLong llDue = (0 - TIMET_100NS2MS);
        m_hTimer.SetTimer(llDue, 0, &CTickEventImp::StaticTickRoutine, this);
        DEV_DEBUG(TF("  !!!Tick Event set 1 MS timer for wait return!"));

        DEV_DEBUG(TF("  !!!Tick Event Exit stop thread"));
        if (Stop((UInt)(UInt)CEventManager::THREAD_WAIT_TIME) == false)
        {
            if (bKill)
            {
                DEV_DEBUG(TF("  !!!Tick Event Exit kill thread"));
                Kill();
            }
            else
            {
                return false;
            }
        }
    }
    if (CAtomics::Exchange<uintptr_t>(&m_utTick, 0) != 0)
    {
        m_hTimer.CloseTimer();
        Reset();
        return true;
    }
    return false;
}

bool CTickEventImp::Check(Int nMult)
{
    if (m_utTick > 0)
    {
        nMult = DEF::Max<Int>(nMult, CEventQueue::EVENT_COUNT_CHECK);
        UInt uTimeout = (m_uInterval * nMult);
        if (uTimeout == 0)
        {
            ++uTimeout;
        }
        return CheckStatus(uTimeout);
    }
    return false;
}

bool CTickEventImp::OnStart(void)
{
    m_pHandler->AddRef();

    Long  lPeriod = (Long)m_uInterval;
    LLong llDue   = lPeriod;
    llDue         = 0 - llDue * TIMET_100NS2MS; // nanosecond to millisecond *10000
    if (m_hTimer.SetTimer(llDue, lPeriod, &CTickEventImp::StaticTickRoutine, this))
    {
        DEV_DEBUG(TF("  !!!Tick Event(%lld) Init okay"), m_utTick);
        return true;
    }
    Reset();
    DEV_DEBUG(TF("  !!!Tick Event(%lld) SetTimer fail or start thread fail"), m_utTick);
    return false;
}

void CTickEventImp::OnStop(void)
{
}

void CTickEventImp::OnKill(void)
{
}

bool CTickEventImp::Run(void)
{
    UpdateWaitStatus();
    Int nRet = m_hTimer.Wait();
    if ((nRet == WAIT_IO_COMPLETION) || (nRet == WAIT_OBJECT_0))
    {
        return (m_uCount != 0);
    }
    return false;
}

void CTickEventImp::Cache(void)
{
    if (CAtomics::Exchange<uintptr_t>(&m_utTick, 0) != 0)
    {
        DEV_DEBUG(TF("  !!!Tick Event(%lld) return to cache"), m_utTick);
        m_hTimer.CloseTimer();
        Reset();
        if (SEventManager::GetInstance() != nullptr)
        {
            SEventManager::GetInstance()->CacheTickEvent(m_inxId);
        }
    }
}

void CTickEventImp::Reset(void)
{
    CEventHandler* pHandler = (CEventHandler*)CAtomics::ExchangePtr(reinterpret_cast<void* volatile*>(&m_pHandler), nullptr);
    if (pHandler != nullptr)
    {
        TRY_EXCEPTION
            pHandler->Release();
        CATCH_EXCEPTION
        END_EXCEPTION
    }
}

bool CTickEventImp::TickRoutine(void)
{
    UpdateWaitStatus(false);
    if (m_uCount > 0)
    {
        if (m_uCount != (UInt)TIMET_INFINITE)
        {
            --m_uCount;
        }
        // DEV_TRACE(TF("  !!!Tick Event(%lld) TickRoutine Count=%#X"), m_utTick, m_uCount);
        if (m_pHandler->OnHandle(m_utTick, m_uCount) != (UInt)RET_OKAY)
        {
            m_uCount = 0;
        }
    }
    return (m_uCount > 0);
}

void WINAPI CTickEventImp::StaticTickRoutine(void* pThis, ULong, ULong)
{
    CTickEventImp* pTickEventImp = reinterpret_cast<CTickEventImp*>(pThis);
//    TRY_EXCEPTION
//       // DEV_TRACE(TF("  !!!Tick Event StaticTickRoutine"));
        if (pTickEventImp->TickRoutine() == false)
        {
            pTickEventImp->Cache();
        }
//    CATCH_EXCEPTION
//        DEV_DEBUG(TF("  !!!Tick Event StaticTickRoutine invalid ptr"));
//        pTickEventImp->m_uCount = 0;
//    END_EXCEPTION
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
