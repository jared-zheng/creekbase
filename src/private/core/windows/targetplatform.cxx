// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "windows/reg.h"
#include "atomics.h"
#include "subsystem.h"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CPlatformImp
CPCXStr CPlatformImp::STR_LOG_LEVELNAME  = TF("-DevLevel:");
CPCXStr CPlatformImp::STR_LOG_FILENAME   = TF("DEV%ld_%04d%02d%02d_%02d%02d%02d_%03d.log");

Int CPlatformImp::DefExceptHandler(TId, Int, Int, void*, struct _EXCEPTION_POINTERS*)
{
    return EXCEPTION_CONTINUE_SEARCH;
}

EXCEPT_HANDLER CPlatformImp::ms_pExceptHandler = &CPlatformImp::DefExceptHandler;

CPlatformImp::CPlatformImp(void)
: mc_ullRuntime
(
      __ARCH_TARGET__
#ifdef __ARCH_TARGET_BIGENDIAN__
    | ARCH_TARGET_BIGENDIAN
#endif
    | __COMPILER_TYPE__
    | __PLATFORM_TARGET__
    | __RUNTIME_CHARSET__
#ifdef __RUNTIME_DEBUG__
    | RUNTIME_CONFIG_DEBUG
#endif
#ifdef __RUNTIME_STATIC__
    | RUNTIME_CONFIG_STATIC
#endif
)
, m_uPageSize(0)
#ifdef __RUNTIME_DEBUG__
, m_uLogLevel(LOGL_INFO|LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#else
, m_uLogLevel(LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#endif
, m_uComLevel(0)
, m_uLogMaxSize(0)
, m_hLogFile(HANDLE_INVALID)
, m_llBaseTime(0)
, m_llBaseTick(0)
, m_pComponent(nullptr)
{
}

CPlatformImp::~CPlatformImp(void)
{
    assert(m_hLogFile == HANDLE_INVALID);
}

CPlatformImp::CPlatformImp(const CPlatformImp&)
: mc_ullRuntime
(
      __ARCH_TARGET__
#ifdef __ARCH_TARGET_BIGENDIAN__
    | ARCH_TARGET_BIGENDIAN
#endif
    | __PLATFORM_TARGET__
    | __RUNTIME_CHARSET__
#ifdef __RUNTIME_DEBUG__
    | RUNTIME_CONFIG_DEBUG
#endif
#ifdef __RUNTIME_STATIC__
    | RUNTIME_CONFIG_STATIC
#endif
)
, m_uPageSize(0)
#ifdef __RUNTIME_DEBUG__
, m_uLogLevel(LOGL_INFO|LOGL_DUMP|LOGL_WARN|LOGL_ERROR)
#else
, m_uLogLevel(LOGL_ERROR)
#endif
, m_uComLevel(0)
, m_uLogMaxSize(0)
, m_hLogFile(HANDLE_INVALID)
, m_llBaseTime(0)
, m_llBaseTick(0)
, m_pComponent(nullptr)
{
}

CPlatformImp& CPlatformImp::operator=(const CPlatformImp&)
{
    return (*this);
}

bool CPlatformImp::Init(void)
{
    InitCMD();
    InitTick();

    InitLogFile();
    InitMemTrace();

    GetPageSize();
    CPUInfo();
    return true;
}

void CPlatformImp::Exit(void)
{
    ExitMemTrace();
    ExitLogFile();
}

Int CPlatformImp::SetExceptHandler(EXCEPT_HANDLER ExceptHandler)
{
    if (ExceptHandler != nullptr)
    {
        ms_pExceptHandler = ExceptHandler;
    }
    else
    {
        ms_pExceptHandler = &CPlatformImp::DefExceptHandler;
    }
    return RET_OKAY;
}

ULLong CPlatformImp::GetRuntime(void) const
{
    return (ULLong)mc_ullRuntime;
}

UInt CPlatformImp::GetPageSize(void)
{
    if (m_uPageSize == 0)
    {
        SYSTEM_INFO sys;
        ::GetSystemInfo(&sys);
        m_uPageSize = (UInt)((sys.dwPageSize > CPlatform::OSP_DEFAULT) ? sys.dwPageSize : CPlatform::OSP_DEFAULT);
    }
    return m_uPageSize;
}

UInt CPlatformImp::CheckThread(UInt uCount, UInt uTimes, UInt uMin) const
{
    return DEF::Maxmin<UInt>(uMin, uCount, (m_CPUInfo.uCores * uTimes));
}

LLong CPlatformImp::GetBaseTime(void) const
{
    return m_llBaseTime;
}

LLong CPlatformImp::GetBaseTick(void) const
{
    return m_llBaseTick;
}

void CPlatformImp::GetCPUInfo(CPlatform::CPUINFO& cpui) const
{
    cpui = m_CPUInfo;
}

bool CPlatformImp::DevelopPrint(CComponent* pComponent, UInt uLevel)
{
    if (pComponent != nullptr)
    {
        if (CAtomics::CompareExchangePtr((void* volatile*)&m_pComponent, pComponent, nullptr) == nullptr)
        {
            ComponentAddRef();
            m_uComLevel = (uLevel & LOGL_ALL);
            DEV_DUMP(TF("#---Windows Platform Develop Print addref CComponent %p---"), pComponent);
            return true;
        }
    }
    else if (m_pComponent != nullptr)
    {
        DEV_DUMP(TF("#---Windows Platform Develop Print release CComponent ---"));
        m_uComLevel = 0;
        ComponentRelease();
        return true;
    }
    return false;
}

void CPlatformImp::DevelopPrint(UInt uLevel, PCXStr pszFormat, va_list args)
{
    if (uLevel & (m_uLogLevel|m_uComLevel))
    {
        CPlatform::TIMEINFO ti = { 0 };
        CPlatform::GetTimeInfo(ti);

        Int   nLen = 0;
        XChar szLogBuf[TRANSC_LOG_BUF];
        switch (uLevel)
        {
#ifdef __RUNTIME_DEBUG__
        case LOGL_MEMMG:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[MEMMG][%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_TRACE:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[TRACE][%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_DEBUG:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DEBUG][%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
#endif  // __RUNTIME_DEBUG__
        case LOGL_INFO:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[INFO] [%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_DUMP:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DUMP] [%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_WARN:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[WARN] [%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        case LOGL_ERROR:
            {
                nLen = CXChar::Format(szLogBuf, TRANSC_LOG_BUF, TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[ERROR][%04X]"),
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId());
            }
            break;
        default:
            {
                return;
            }
        }
        if (nLen > 0)
        {
            nLen += CXChar::FormatV((szLogBuf + nLen), (size_t)((size_t)TRANSC_LOG_BUF - (size_t)nLen), pszFormat, args);
            //
            szLogBuf[nLen] = 0;
            if ((m_uLogLevel & LOGL_DEVPRINT) && (m_uLogLevel & uLevel))
            {
                Handle h = ::GetStdHandle(STD_OUTPUT_HANDLE);
                switch (uLevel)
                {
#ifdef __RUNTIME_DEBUG__
                case LOGL_MEMMG:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_MEMMG);
                    }
                    break;
                case LOGL_TRACE:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_TRACE);
                    }
                    break;
                case LOGL_DEBUG:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_DEBUG);
                    }
                    break;
#endif  // __RUNTIME_DEBUG__
                case LOGL_INFO:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_INFO);
                    }
                    break;
                case LOGL_DUMP:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_DUMP);
                    }
                    break;
                case LOGL_WARN:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_WARN);
                    }
                    break;
                case LOGL_ERROR:
                    {
                        ::SetConsoleTextAttribute(h, (UShort)CONSOLE_CLR_ERROR);
                    }
                    break;
                default: {}
                }
                _putts(szLogBuf);
            }
            szLogBuf[nLen] = TF('\r');
            ++nLen;
            szLogBuf[nLen] = TF('\n');
            ++nLen;
            szLogBuf[nLen] = 0;
            if ((m_pComponent != nullptr) && (m_uComLevel & uLevel))
            {
                m_pComponent->Command(szLogBuf, uLevel);
            }
            //
            if (m_uLogLevel & uLevel)
            {
                ::OutputDebugString(szLogBuf);
                PrintLogFile(szLogBuf, nLen);
            }
        }
    }
}

void* CPlatformImp::Alloc(size_t stSize, UInt uType)
{
    return (::VirtualAlloc(nullptr, stSize, MEM_RESERVE | MEM_COMMIT, (ULong)uType));
}

void  CPlatformImp::Free(void* pPtr, size_t stSize)
{
    UNREFERENCED_PARAMETER( stSize );
    ::VirtualFree(pPtr, 0, MEM_RELEASE);
}

void CPlatformImp::InitCMD(void)
{
    PCXStr pszCMD    = ::GetCommandLine();
    PCXStr pszLog    = CXChar::Str(pszCMD, STR_LOG_LEVELNAME);
    if (pszLog != nullptr)
    {
        pszLog      += TRANSC_LOG_LEVEL;
        m_uLogLevel  = (UInt)CXChar::ToULong(pszLog, nullptr, RADIXT_HEX);
        m_uLogLevel &= (LOGL_DEVOUT|LOGL_DEVPRINT|LOGL_DEVFILE|LOGL_MEMMG|LOGL_ALL);
    }
}

void CPlatformImp::InitTick(void)
{
    Handle hProcess = ::GetCurrentProcess();
    // Get the main processor affinity mask
    uintptr_t utProcessAffinityMask = 0;
    uintptr_t utSystemAffinityMask  = 0;
    if (::GetProcessAffinityMask(hProcess, (PDWORD_PTR)&utProcessAffinityMask, (PDWORD_PTR)&utSystemAffinityMask) && utProcessAffinityMask)
    {
        // Find the lowest processor that our process is allows to run against
        uintptr_t utAffinityMask = (utProcessAffinityMask & ((~utProcessAffinityMask) + 1));
        // Set this as the processor that our thread must always run against
        // This must be a subset of the process affinity mask
        Handle hThread  = ::GetCurrentThread();
        if (hThread != HANDLE_INVALID)
        {
            ::SetThreadAffinityMask(hThread, utAffinityMask);
            ::CloseHandle(hThread);
        }
    }
    ::CloseHandle(hProcess);

    LARGE_INTEGER Time = { 0 };
    ::QueryPerformanceCounter(&Time);
    m_llBaseTime = Time.QuadPart;

    ::QueryPerformanceFrequency(&Time);
    m_llBaseTick = Time.QuadPart;
}

bool CPlatformImp::InitLogFile(void)
{
    if (m_hLogFile == HANDLE_INVALID)
    {
        if ((m_uLogLevel & LOGL_DEVFILE) && (m_uLogLevel & LOGL_ALL))
        {
            SYSTEMTIME sm;
            ::GetLocalTime(&sm);
            XChar szLogFile[LMT_MAX_PATH] = { 0 };
            CXChar::Format(szLogFile, (size_t)LMT_MAX_PATH, STR_LOG_FILENAME,
                           CPlatform::GetCurrentPId(), sm.wYear, sm.wMonth, sm.wDay,
                           sm.wHour, sm.wMinute, sm.wSecond, sm.wMilliseconds);
            m_hLogFile = ::CreateFile(szLogFile, GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
            if (m_hLogFile != HANDLE_INVALID)
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                Byte  bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                ULong ulRet = 0;
                ::WriteFile(m_hLogFile, bHead, BOML_UTF16, &ulRet, nullptr);
#endif
            }
        }
    }
    return (m_hLogFile != HANDLE_INVALID);
}

void CPlatformImp::ExitLogFile(void)
{
    if (m_hLogFile != HANDLE_INVALID)
    {
        ::CloseHandle(m_hLogFile);
        m_hLogFile = HANDLE_INVALID;
    }
}

void CPlatformImp::PrintLogFile(PCXStr pszLog, Int nLen)
{
    if (m_hLogFile != HANDLE_INVALID)
    {
        UInt uRet = 0;
        ::WriteFile(m_hLogFile, pszLog, (ULong)nLen * sizeof(XChar), (PULong)&uRet, nullptr);

        uRet += CAtomics::Add<UInt>(&m_uLogMaxSize, uRet);
        if ((uRet >= TRANSC_LOG_MAXSIZE) &&
            (CAtomics::CompareExchange<UInt>(&m_uLogMaxSize, 0, uRet) == uRet))
        {
            ExitLogFile();
            InitLogFile();
        }
    }
}

bool CPlatformImp::InitMemTrace(void)
{
#ifdef __RUNTIME_DEBUG__
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
    _CrtSetReportMode(_CRT_WARN,   _CRTDBG_MODE_DEBUG);
    _CrtSetReportMode(_CRT_ERROR,  _CRTDBG_MODE_DEBUG);
    _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_DEBUG);
#endif

#if (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
#ifdef __RUNTIME_DEBUG__
    DEV_DUMP(TF("#---Windows Platform [%#X] - MSVC[%d] Build[%s, %s] Debug Version Memory Leak Trace Ready---"), _WIN32_WINNT, _MSC_VER, TF(__DATE__), TF(__TIME__));
#else
    DEV_DUMP(TF("#---Windows Platform [%#X] - MSVC[%d] Build[%s, %s] Release Version---"), _WIN32_WINNT, _MSC_VER, TF(__DATE__), TF(__TIME__));
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_CLANG)
#ifdef __RUNTIME_DEBUG__
    DEV_DUMP(TF("#---Windows Platform [%#X] - LLVM Clang[%s] Build[%s, %s] Debug Version Memory Leak Trace Ready---"), _WIN32_WINNT, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#else
    DEV_DUMP(TF("#---Windows Platform [%#X] - LLVM Clang[%s] Build[%s, %s] Release Version---"), _WIN32_WINNT, TF(__VERSION__), TF(__DATE__), TF(__TIME__));
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_GCC)
    #error "Add Compile Info!!!"
#else
    #error "Add Compile Info!!!"
#endif
    return true;
}

void CPlatformImp::ExitMemTrace(void)
{
#ifdef __RUNTIME_DEBUG__
    DEV_DUMP(TF("#---Windows Platform Debug Memory Leak Trace Exit---"));
#endif
}

void CPlatformImp::CPUInfo(void)
{
    MM_SAFE::Set(&m_CPUInfo, 0, sizeof(CPlatform::CPUINFO));
    // CPU Logical processors
    m_CPUInfo.uCores = 1;
    SYSTEM_INFO info;
    ::GetSystemInfo(&info);
    m_CPUInfo.uCores = (UInt)(info.dwNumberOfProcessors ? info.dwNumberOfProcessors : 1);
    //
    CReg reg;
    if (reg.Open(HKEY_LOCAL_MACHINE, TF("HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0"), KEY_READ) == ERROR_SUCCESS)
    {
        ULong ulTemp = LMT_MIN;
        reg.QueryStringValue(TF("Identifier"), m_CPUInfo.szInfo, &ulTemp);

        XChar szValue[LMT_MIN] = { 0 };
        szValue[0] = TF('-');
        ulTemp = LMT_MIN - 1;
        reg.QueryStringValue(TF("ProcessorNameString"), szValue + 1, &ulTemp);
        CXChar::Concat(m_CPUInfo.szInfo, LMT_MIN, szValue);
    }
}

void CPlatformImp::ComponentAddRef(void)
{
    m_pComponent->AddRef();
}

void CPlatformImp::ComponentRelease(void)
{
    TRY_EXCEPTION
        m_pComponent->Release();
        CAtomics::ExchangePtr((void* volatile*)&m_pComponent, nullptr);
    CATCH_EXCEPTION
    END_EXCEPTION
}

///////////////////////////////////////////////////////////////////
// CPlatform
CPlatform::CPlatform(void)
{
}

CPlatform::~CPlatform(void)
{
}

CPlatform::CPlatform(const CPlatform&)
{
}

CPlatform& CPlatform::operator=(const CPlatform&)
{
    return (*this);
}

ULLong CPlatform::GetRuntimeConfig(void)
{
    return (GPlatform.GetRuntime());
}

UInt CPlatform::GetPageSize(void)
{
    return (GPlatform.GetPageSize());
}

PId CPlatform::GetCurrentPId(void)
{
    return ::GetCurrentProcessId();
}

TId CPlatform::GetCurrentTId(void)
{
    return ::GetCurrentThreadId();
}

Int CPlatform::SleepEx(UInt uSleep, bool bAlert)
{
    return (Int)::SleepEx(uSleep, bAlert ? TRUE : FALSE);
}

void CPlatform::YieldEx(void)
{
    ::SwitchToThread();
}

UInt CPlatform::CheckThread(UInt uCount, UInt uTimes, UInt uMin)
{
    return GPlatform.CheckThread(uCount, uTimes, uMin);
}

UInt CPlatform::GetCPURate(CPUSTAT& cpus1, CPUSTAT& cpus2)
{
    UInt uRate = 0;
    if (cpus2.ullTotal != cpus1.ullTotal)
    {
        if (cpus2.ullTotal > cpus1.ullTotal)
        {
            ULLong ullUsed = (cpus2.ullUsed > cpus1.ullUsed) ? (cpus2.ullUsed - cpus1.ullUsed) : 0;
            ullUsed *= CPlatformImp::CPU_OCCUPY_RATE;
            
            uRate = (UInt)(ullUsed / (cpus2.ullTotal - cpus1.ullTotal));
        }
        else
        {
            ULLong ullUsed = (cpus1.ullUsed > cpus2.ullUsed) ? (cpus1.ullUsed - cpus2.ullUsed) : 0;
            ullUsed *= CPlatformImp::CPU_OCCUPY_RATE;

            uRate = (UInt)(ullUsed / (cpus1.ullTotal - cpus2.ullTotal));
        }
    }
    return uRate;
}

void CPlatform::GetCPUStat(CPUSTAT& cpus)
{
    FILETIME tIdle   = { 0 };
    FILETIME tSystem = { 0 };
    FILETIME tUser   = { 0 };
    ::GetSystemTimes(&tIdle, &tSystem, &tUser);

    ULARGE_INTEGER uiIdle;
    uiIdle.HighPart = tIdle.dwHighDateTime;
    uiIdle.LowPart  = tIdle.dwLowDateTime;
    ULARGE_INTEGER uiSystem;
    uiSystem.HighPart = tSystem.dwHighDateTime;
    uiSystem.LowPart  = tSystem.dwLowDateTime;
    ULARGE_INTEGER uiUser;
    uiUser.HighPart = tUser.dwHighDateTime;
    uiUser.LowPart  = tUser.dwLowDateTime;

    cpus.ullTotal = (uiSystem.QuadPart + uiUser.QuadPart);
    cpus.ullUsed  = (cpus.ullTotal - uiIdle.QuadPart);
}

void CPlatform::GetCPUInfo(CPUINFO& cpui)
{
    GPlatform.GetCPUInfo(cpui);
}

void CPlatform::GetMemInfo(MEMINFO& mem)
{
    mem.uPageSize = (GPlatform.GetPageSize() >> CAPAP_10); // KB

    MEMORYSTATUSEX mmx;
    mmx.dwLength = sizeof(MEMORYSTATUSEX);
    ::GlobalMemoryStatusEx(&mmx);
    mem.uUsedPercent = (UInt)(mmx.dwMemoryLoad);
    mem.uTotalPhys   = (UInt)(mmx.ullTotalPhys >> CAPAP_20); // MB
    mem.uAvailPhys   = (UInt)(mmx.ullAvailPhys >> CAPAP_20);
}

void CPlatform::GetOSInfo(OSINFO& os)
{
    XChar szValue[LMT_MAX_PATH] = { 0 };

    os.uOSType = OST_WINDOWS;

    CReg reg;
    if (reg.Open(HKEY_LOCAL_MACHINE, TF("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), KEY_READ) == ERROR_SUCCESS)
    {
        ULong ulTemp = 0;
        if (reg.QueryULongValue(TF("CurrentMajorVersionNumber"), ulTemp) == ERROR_SUCCESS) // win10
        {
            os.uMajor = (UInt)ulTemp;
            reg.QueryULongValue(TF("CurrentMinorVersionNumber"), ulTemp);
            os.uMinor = (UInt)ulTemp;
        }
        else // others
        {
            ulTemp = LMT_MAX_PATH;
            reg.QueryStringValue(TF("CurrentVersion"), szValue, &ulTemp);

            PXStr pszTemp = nullptr;
            os.uMajor = (UInt)CXChar::ToULong(szValue, &pszTemp);
            if (pszTemp != nullptr) // X.X
            {
                os.uMinor = (UInt)CXChar::ToULong((pszTemp + 1));
            }
        }
        ulTemp = LMT_MAX_PATH;
        reg.QueryStringValue(TF("CurrentBuildNumber"), szValue, &ulTemp);
        os.uBuild = (UInt)CXChar::ToULong(szValue);

        ulTemp = LMT_MAX_NAME;
        reg.QueryStringValue(TF("ProductName"), os.szInfo, &ulTemp);

        szValue[0] = TF('-');
        ulTemp = LMT_MAX_PATH - 1;
        if (reg.QueryStringValue(TF("BuildLabEx"), szValue + 1, &ulTemp) != ERROR_SUCCESS)
        {
            ulTemp = LMT_MAX_PATH - 1;
            reg.QueryStringValue(TF("BuildLab"), szValue + 1, &ulTemp);
        }
        CXChar::Concat(os.szInfo, LMT_MAX_NAME, szValue);
    }
}

void CPlatform::GetTimeInfo(TIMEINFO& ti, bool bLocal)
{
    SYSTEMTIME sm;
    if (bLocal)
    {
        ::GetLocalTime(&sm);
    }
    else
    {
        ::GetSystemTime(&sm);
    }
    ti.usYear    = sm.wYear;
    ti.usMonth   = sm.wMonth;
    ti.usWeekDay = sm.wDayOfWeek;
    ti.usDay     = sm.wDay;
    ti.usHour    = sm.wHour;
    ti.usMinute  = sm.wMinute;
    ti.usSecond  = sm.wSecond;
    ti.usMSecond = sm.wMilliseconds;
}

ULLong CPlatform::GetTimeStamp(void)
{
    SYSTEMTIME sm;
    ::GetLocalTime(&sm);

    struct tm atm;
    atm.tm_sec   = (Int)sm.wSecond;
    atm.tm_min   = (Int)sm.wMinute;
    atm.tm_hour  = (Int)sm.wHour;
    atm.tm_mday  = (Int)sm.wDay;
    atm.tm_mon   = (Int)(sm.wMonth - TIMET_TM_MONTH);  // tm_mon  is 0 based
    atm.tm_year  = (Int)(sm.wYear - TIMET_TM_YEAR);    // tm_year is 1900 based

    ULLong ullTime = (ULLong)mktime(&atm);
    ullTime = (ullTime * TIMET_S2MS) + (ULLong)sm.wMilliseconds;
    return ullTime;
}

size_t CPlatform::GetFullCmd(PXStr pszBuf, size_t stBufLen)
{
    if ((pszBuf == nullptr) || (stBufLen == 0))
    {
        return 0;
    }
    CXChar::Copy(pszBuf, stBufLen, ::GetCommandLine());
    return CXChar::Length(pszBuf);
}

size_t CPlatform::GetPathInfo(PXStr pszBuf, size_t stBufLen, bool bFull)
{
    if ((pszBuf == nullptr) || (stBufLen == 0))
    {
        return 0;
    }
    stBufLen = (size_t)::GetModuleFileName(nullptr, pszBuf, (ULong)stBufLen);
    if (stBufLen > 0)
    {
        if (bFull == false)
        {
            PXStr p = (PXStr)CXChar::RevChr(pszBuf, TF('\\'));
            assert(p > pszBuf);
            ++p;
            *p = 0;

            stBufLen = (p - pszBuf);
        }
    }
    return stBufLen;
}

PXStr CPlatform::SetLocal(int nLocal, PCXStr pszLanguage)
{
    return _tsetlocale(nLocal, pszLanguage);
}

Int CPlatform::SetExceptHandler(EXCEPT_HANDLER ExceptHandler)
{
    return GPlatform.SetExceptHandler(ExceptHandler);
}

LLong CPlatform::GetRunningTime(void)
{
    LARGE_INTEGER Time = { 0 };
    ::QueryPerformanceCounter(&Time);
    Time.QuadPart -= GPlatform.GetBaseTime();
    return ((Time.QuadPart * TIMET_S2MS) / GPlatform.GetBaseTick());
}

LLong CPlatform::GetOSRunningTime(void)
{
    LARGE_INTEGER Time = { 0 };
    ::QueryPerformanceCounter(&Time);
    return ((Time.QuadPart * TIMET_S2MS) / GPlatform.GetBaseTick());
}

LLong CPlatform::GetOSRunningTick(void)
{
    LARGE_INTEGER Time = { 0 };
    ::QueryPerformanceCounter(&Time);
    return (Time.QuadPart);
}

LLong CPlatform::GetOSBaseTick(void)
{
    return GPlatform.GetBaseTick();
}

LLong CPlatform::Tick2MilliSecond(LLong& llTick)
{
    return ((llTick * TIMET_S2MS) / GPlatform.GetBaseTick());
}

UShort CPlatform::ByteSwap(UShort usValue)
{
    return _byteswap_ushort(usValue);
}

UInt CPlatform::ByteSwap(UInt uValue)
{
    return (UInt)_byteswap_ulong((ULong)uValue);
}

ULong CPlatform::ByteSwap(ULong ulValue)
{
    return _byteswap_ulong(ulValue);
}

ULLong CPlatform::ByteSwap(ULLong ullValue)
{
    return _byteswap_uint64(ullValue);
}

bool CPlatform::DevelopPrint(CComponent* pComponent, UInt uLevel)
{
    return GPlatform.DevelopPrint(pComponent, uLevel);
}

void CPlatform::DevelopPrint(UInt uLevel, PCXStr pszFormat, ...)
{
    if (uLevel & (LOGL_ALL|LOGL_MEMMG))
    {
        va_list vl;
        va_start(vl, pszFormat);
        GPlatform.DevelopPrint(uLevel, pszFormat, vl);
        va_end(vl);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

