// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "core.hxx"
#include "thread.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CThreadPool
TRet THREAD_FUNC CThreadPool::StaticRoutine(TParam pParam)
{
    DefRoutine(pParam);
    return 0;
}

///////////////////////////////////////////////////////////////////
// CThread
UInt CThread::TlsCreateIndex(void* pParam)
{
    UNREFERENCED_PARAMETER( pParam );
    return (UInt)::TlsAlloc();
}

bool CThread::TlsDestroyIndex(UInt uIndex)
{
    return (::TlsFree(uIndex) != FALSE);
}

bool CThread::TlsSetIndexValue(UInt uIndex, void* pValue)
{
    return (::TlsSetValue(uIndex, pValue) != FALSE);
}

void* CThread::TlsGetIndexValue(UInt uIndex)
{
    return ::TlsGetValue(uIndex);
}
///////////////////////////////////////////////////////////////////
CThread::~CThread(void)
{
    if (IsRunning())
    {
        ::TerminateThread(m_hThread, EXIT_FAILURE);
        CAtomics::Decrement<UInt>(&(CThreadPool::ms_uRunningCount));
        CAtomics::Increment<UInt>(&(CThreadPool::ms_uKillCount));
    }
}

bool CThread::Start(const CThreadAttr* pAttr)
{
    if (IsRunning())
    {
        DEV_DEBUG(TF("  Thread[%p---%#X] Start return false by thread already running"), this, m_hThread.m_TId);
        return false;
    }
    if (m_hThread.IsValid())
    {
        m_hThread.Reset();
    }

    CAtomics::Increment<UInt>(&(CThreadPool::ms_uStartCount));
    if (pAttr == nullptr)
    {
        m_hThread = ::CreateThread(nullptr, 0, &CThreadPool::StaticRoutine, this, 0, &(m_hThread.m_TId));
    }
    else
    {
        m_uStatus = (pAttr->m_ulFlags & CREATE_SUSPENDED) ? TRUE : FALSE;
        m_hThread = ::CreateThread(pAttr->m_pSA, pAttr->m_stStack, &CThreadPool::StaticRoutine, this, pAttr->m_ulFlags, &(m_hThread.m_TId));
    }
    UpdateStatus();
    DEV_DEBUG(TF("  Thread[%p---%#X] Start create thread"), this, m_hThread.m_TId);
    return (m_hThread.IsValid());
}

bool CThread::GetPriority(Int& nPriority, Int& nPolicy) const
{
    nPolicy   = -1;
    nPriority = (Int)::GetThreadPriority(::GetCurrentThread());
    return true;
}

bool CThread::SetPriority(Int nPriority, Int nPolicy)
{
    ULong ulCurTP = ::GetThreadPriority(::GetCurrentThread());
    ULong ulNewTP = 0;
    if (nPolicy != -1)
    {
        ulNewTP = (ULong)THREAD_PRIORITY_NORMAL;
    }
    else
    {
        ulNewTP = (ULong)nPriority;
    }
    if (ulNewTP != ulCurTP)
    {
        ::SetThreadPriority(::GetCurrentThread(), ulNewTP);
        return true;
    }
    return false;
}

bool CThread::IsRunning(void) const
{
    if (m_hThread.IsValid())
    {
        if (::WaitForSingleObject(m_hThread, TIMET_IGNORE) == WAIT_TIMEOUT)
        {
            return true;
        }
    }
    return false;
}


Int CThread::Suspend(bool bSuspend)
{
    if (IsRunning())
    {
        if (bSuspend)
        {
            Int nCount = (Int)::SuspendThread(m_hThread);
            if (nCount != RET_ERROR)
            {
                if (nCount == 0)
                {
                    m_uStatus = TRUE;
                }
                return (nCount + 1);
            }
        }
        else
        {
            Int nCount = (Int)::ResumeThread(m_hThread);
            if (nCount != RET_ERROR)
            {
                if (nCount == 1)
                {
                    m_uStatus = FALSE;
                }
                return (nCount - 1);
            }
        }
    }
    return RET_ERROR;
}

Int CThread::Kill(UInt uCode)
{
    BOOL bRet = FALSE;
    if (IsRunning())
    {
        bRet = ::TerminateThread(m_hThread, (ULong)uCode);
        if (bRet != FALSE)
        {
            DEV_DEBUG(TF("  Thread Kill Thread[%p---%#X] succeeded"), this, m_hThread.m_TId);
            CAtomics::Decrement<UInt>(&(CThreadPool::ms_uRunningCount));
            CAtomics::Increment<UInt>(&(CThreadPool::ms_uKillCount));
            m_hThread.Detach(); // terminated thread handle invalid
            m_hThread.Reset();
        }
    }
    if (m_hThread.IsValid())
    {
        m_hThread.Reset();
    }
    if (bRet != FALSE)
    {
        OnKill();
    }
    return RET_OKAY;
}

Int CThread::Wait(UInt uWait, bool bAlert)
{
    if (m_hThread.IsValid())
    {
        Int nRet = (Int)::WaitForSingleObjectEx(m_hThread, uWait, bAlert ? TRUE : FALSE);
        if ((nRet != WAIT_OBJECT_0) && (nRet != WAIT_IO_COMPLETION))
        {
            DEV_DEBUG(TF("  Thread[%p---%#X] Wait ERROR ID:%#X"), this, m_hThread.m_TId, nRet);
        }
        return nRet;
    }
    return (Int)RET_FAIL;
}

Int CThread::Detach(void)
{
    return RET_OKAY;
}

void CThread::Routine(void)
{
    m_hThread.m_TId = ::GetCurrentThreadId();
#if (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
    bool bRet = false;
    __try
    {
        bRet = OnStart();
    }
    __except (CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_THREAD, THREAD_ROUTINE_ONSTART, this, GetExceptionInformation()))
    {
        bRet = true;
        DEV_DUMP(TF("  Thread[%p---%#X] Routine-OnStart Exception Handled"), this, m_hThread.m_TId);
    }

    while (bRet)
    {
        bRet = false;
        __try
        {
            for(;;)
            {
                if (Run() == false)
                {
                    break;
                }
            }
        }
        __except (CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_THREAD, THREAD_ROUTINE_RUN, this, GetExceptionInformation()))
        {
            bRet = true;
            DEV_DUMP(TF("  Thread[%p---%#X] Routine-Run Exception Handled"), this, m_hThread.m_TId);
        }
    }

    __try
    {
        OnStop();
    }
    __except (CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_THREAD, THREAD_ROUTINE_ONSTOP, this, GetExceptionInformation()))
    {
        DEV_DUMP(TF("  Thread[%p---%#X] Routine-OnStop Exception Handled"), this, m_hThread.m_TId);
    }
#else
    #pragma message("Windows only MSVC support SEH handler!")
    if (OnStart())
    {
        for(;;)
        {
            if (Run() == false)
            {
                break;
            }
        }
    }
    OnStop();
#endif
    m_hThread.Reset();
}

///////////////////////////////////////////////////////////////////
// CRunnableThread
void CRunnableThread::Routine(void)
{
    m_hThread.m_TId = ::GetCurrentThreadId();
    Detach();

#if (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
    Int  nRet = EXCEPTION_EXECUTE_HANDLER;
    bool bRet = false;
    __try
    {
        bRet = OnStart();
    }
    __except (nRet = CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_RUNNABLE, THREAD_ROUTINE_ONSTART, m_pRunning, GetExceptionInformation()), EXCEPTION_EXECUTE_HANDLER)
    {
        bRet = (nRet != EXCEPTION_CONTINUE_SEARCH);
        DEV_DUMP(TF("  RunnableThread[%p---%#X] Routine-OnStart Exception Handled"), this, m_hThread.m_TId);
    }

    while (bRet)
    {
        bRet = false;
        __try
        {
            for(;;)
            {
                if (Run() == false)
                {
                    break;
                }
            }
        }
        __except (nRet = CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_RUNNABLE, THREAD_ROUTINE_RUN, m_pRunning, GetExceptionInformation()), EXCEPTION_EXECUTE_HANDLER)
        {
            bRet = (nRet != EXCEPTION_CONTINUE_SEARCH);
            DEV_DUMP(TF("  RunnableThread[%p---%#X] Routine-Run Exception Handled"), this, m_hThread.m_TId);
        }
    }

    __try
    {
        OnStop();
    }
    __except (CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_RUNNABLE, THREAD_ROUTINE_ONSTOP, m_pRunning, GetExceptionInformation()), EXCEPTION_EXECUTE_HANDLER)
    {
        DEV_DUMP(TF("  RunnableThread[%p---%#X] Routine-OnStop Exception Handled"), this, m_hThread.m_TId);
    }
#else
    #pragma message("Windows only MSVC support SEH handler!")
    if (OnStart())
    {
        for(;;)
        {
            if (Run() == false)
            {
                break;
            }
        }
    }
    OnStop();
#endif
    Finish();
}

///////////////////////////////////////////////////////////////////
// CQueueTaskThread
void CQueueTaskThread::Routine(void)
{
    m_hThread.m_TId = ::GetCurrentThreadId();
#if (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
    bool bRet = OnStart();
    while (bRet)
    {
        bRet = false;
        __try
        {
            for(;;)
            {
                DEV_TRACE(TF("  QueueTaskThread[%p---%#X] wait"), this, m_hThread.m_TId);
                if ((m_SyncEvent.Wait() == RET_OKAY) && (m_pQueueTask != nullptr))
                {
                    Run();
                    // return to thread pool
                    DEV_DEBUG(TF("  QueueTaskThread[%p---%#X] Finish"), this, m_hThread.m_TId);
                    Finish();
                }
                else
                {
                    DEV_TRACE(TF("  QueueTaskThread[%p---%#X] quit"), this, m_hThread.m_TId);
                    break;
                }
            }
        }
        __except (CPlatformImp::ms_pExceptHandler(m_hThread.m_TId, THREAD_TYPE_QUEUETASK, THREAD_ROUTINE_RUN, m_pQueueTask, GetExceptionInformation()), EXCEPTION_EXECUTE_HANDLER)
        {
            bRet = true;
            Finish();
            DEV_DUMP(TF("  QueueTaskThread[%p---%#X] Routine-Run Exception Handled"), this, m_hThread.m_TId);
        }
    }
#else
    #pragma message("Windows only MSVC support SEH handler!")
    if (OnStart())
    {
        for (;;)
        {
            DEV_DUMP(TF("  QueueTaskThread[%p---%#X] wait"), this, m_hThread.m_TId);
            if ((m_SyncEvent.Wait() == RET_OKAY) && (m_pQueueTask != nullptr))
            {
                Run();
                // return to thread pool
                DEV_DUMP(TF("  QueueTaskThread[%p---%#X] Finish"), this, m_hThread.m_TId);
                Finish();
            }
            else
            {
                DEV_DUMP(TF("  QueueTaskThread[%p---%#X] quit"), this, m_hThread.m_TId);
                break;
            }
        }
    }
#endif
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

