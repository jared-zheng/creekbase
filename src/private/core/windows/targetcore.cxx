// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

#if defined(CORE_EXPORT)
///////////////////////////////////////////////////////////////////
#ifdef __RUNTIME_DEBUG__
    #define CORE_MODULE_ATTACH TF("$ Load coreDebug.dll --- DLL_PROCESS_ATTACH")
    #define CORE_MODULE_DETACH TF("$ Unload coreDebug.dll --- DLL_PROCESS_DETACH")
#else
    #define CORE_MODULE_ATTACH TF("$ Load core.dll --- DLL_PROCESS_ATTACH")
    #define CORE_MODULE_DETACH TF("$ Unload core.dll --- DLL_PROCESS_DETACH")
#endif

BOOL WINAPI DllMain(HMODULE hModule, ULong ulReason, void* lpReserved)
{
    UNREFERENCED_PARAMETER( lpReserved );

    if (ulReason == DLL_PROCESS_ATTACH)
    {
        ::DisableThreadLibraryCalls(hModule);
        DEV_DEBUG(CORE_MODULE_ATTACH);
    }
    else if (ulReason == DLL_PROCESS_DETACH)
    {
        DEV_DEBUG(CORE_MODULE_DETACH);
    }
    return TRUE;
}
///////////////////////////////////////////////////////////////////
#endif  // CORE_EXPORT

///////////////////////////////////////////////////////////////////
#if defined(_MSC_VER) // _MSC_VER
    #pragma warning(push)
    #pragma warning(disable:4073) // initializers put in library initialization area

    #pragma init_seg(lib)

    static CInitializer GInitializer;

    #pragma warning(pop)
#else
    #error "Compiler No Implement"
#endif  // _MSC_VER

CInitializer* CInitializer::Get(void)
{
    return (&GInitializer);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

