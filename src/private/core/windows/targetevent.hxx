// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_EVENT_HXX__
#define __TARGET_EVENT_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "event.h"
#include "thread.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTickEventImp : thread for CWaitableTimer
class CTickEventImp : public CThread
{
public:
    enum TICK_TIME
    {
        TICK_TIME_MIN   = 1, // 1 MS
    };
public:
    CTickEventImp(void);
    virtual ~CTickEventImp(void);

    bool    Init(CEventHandler& EventHandlerRef, PINDEX index, uintptr_t utTick, UInt uCount, UInt uInterval);
    bool    Exit(bool bKill = false);

    bool    Check(Int nMult);
protected:    // CThread
    virtual bool   OnStart(void) OVERRIDE;
    virtual void   OnStop(void) OVERRIDE;
    virtual void   OnKill(void) OVERRIDE;
    virtual bool   Run(void) OVERRIDE;
private:
    void   Cache(void);
    void   Reset(void);

    bool   TickRoutine(void);
    static void WINAPI StaticTickRoutine(void*, ULong, ULong);
private:
    CEventHandler*   m_pHandler;
    PINDEX           m_inxId;
    uintptr_t        m_utTick;
    UInt             m_uCount;
    UInt             m_uInterval;
    CWaitableTimer   m_hTimer;
};

///////////////////////////////////////////////////////////////////
// CEventQueueHandle
class CEventQueueHandle : public MObject
{
public:
    CEventQueueHandle(void);
    ~CEventQueueHandle(void);

    operator Handle(void) const;

    bool     IsValid(void) const;

    bool     Open(void);
    void     Close(void);

    bool     Signal(Int nFlag = 0);
    bool     Wait(void);
private:
    CEventQueueHandle(const CEventQueueHandle& aSrc);
    CEventQueueHandle& operator=(const CEventQueueHandle& aSrc);
public:
    CHandle   m_hIOCP;
};

INLINE CEventQueueHandle::CEventQueueHandle(void)
{
}

INLINE CEventQueueHandle::~CEventQueueHandle(void)
{
}

INLINE CEventQueueHandle::CEventQueueHandle(const CEventQueueHandle&)
{
}

INLINE CEventQueueHandle& CEventQueueHandle::operator=(const CEventQueueHandle&)
{
    return (*this);
}

INLINE CEventQueueHandle::operator Handle(void) const
{
    return (Handle)(m_hIOCP);
}

INLINE bool CEventQueueHandle::IsValid(void) const
{
    return m_hIOCP.IsValid();
}

INLINE bool CEventQueueHandle::Open(void)
{
    Close();
    m_hIOCP = ::CreateIoCompletionPort(HANDLE_INVALID, nullptr, 0, 0);
    return m_hIOCP.IsValid();
}

INLINE void CEventQueueHandle::Close(void)
{
    m_hIOCP.Close();
}

INLINE bool CEventQueueHandle::Signal(Int nFlag)
{
    return (::PostQueuedCompletionStatus(m_hIOCP, (ULong)(nFlag), 0, nullptr) != FALSE);
}

INLINE bool CEventQueueHandle::Wait(void)
{
    ULong        ulFlag  = 0L;
    uintptr_t    utEvent = 0L;
    LPOVERLAPPED pOL = nullptr;
    if (::GetQueuedCompletionStatus(m_hIOCP, &ulFlag, (PULONG_PTR)&utEvent, &pOL, (ULong)TIMET_INFINITE))
    {
        if (ulFlag != 0)
        {
            return true;
        }
        DEV_TRACE(TF("  IOCP get quit message, thread[%#X] quit"), CPlatform::GetCurrentTId());
    }
    return false;
}

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_EVENT_HXX__
