// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_PLATFORM_HXX__
#define __TARGET_PLATFORM_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "platform.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CPlatformImp
class CPlatformImp
{
public:
    enum TRANS_CONST
    {
        TRANSC_LOG_LEVEL   = 10,     // length of "-DevLevel:"
        TRANSC_LOG_BUF     = 8192,
        TRANSC_LOG_MAXSIZE = 4 * 1024 * 1024,
    };

    enum CONSOLE_CLR
    {
        CONSOLE_CLR_MEMMG  = 8,  // memmg : gray=8
        CONSOLE_CLR_TRACE  = 6,  // trace : dark yellow=6
        CONSOLE_CLR_DEBUG  = 3,  // debug : dark aqua=3
        CONSOLE_CLR_INFO   = 15, // info  : white=15
        CONSOLE_CLR_DUMP   = 11, // dump  : aqua=11
        CONSOLE_CLR_WARN   = 14, // warn  : yellow=14
        CONSOLE_CLR_ERROR  = 12, // error : red=12
    };

    enum CPU_OCCUPY_CONST
    {
        CPU_OCCUPY_RATE = 10000,
    };
    
    static CPCXStr STR_LOG_LEVELNAME;
    static CPCXStr STR_LOG_FILENAME;
public:
    static Int DefExceptHandler(TId tThreadId, Int nThreadType, Int nThreadRoutine, void* pRoutine, struct _EXCEPTION_POINTERS* pExceptInfo);
public:
    CPlatformImp(void);
    ~CPlatformImp(void);

    bool    Init(void);
    void    Exit(void);

    Int     SetExceptHandler(EXCEPT_HANDLER ExceptHandler);

    ULLong  GetRuntime(void) const;
    UInt    GetPageSize(void);
    UInt    CheckThread(UInt uCount, UInt uTimes, UInt uMin = 1) const;

    LLong   GetBaseTime(void) const;
    LLong   GetBaseTick(void) const;

    void    GetCPUInfo(CPlatform::CPUINFO& cpui) const;

    bool    DevelopPrint(CComponent* pComponent, UInt uLevel);
    void    DevelopPrint(UInt uLevel, PCXStr pszFormat, va_list args);

    void*   Alloc(size_t stSize, UInt uType = ALLOC_TYPE_NORMAL);
    void    Free(void* pPtr, size_t stSize);

private:
    CPlatformImp(const CPlatformImp&);
    CPlatformImp& operator=(const CPlatformImp&);

    void    InitCMD(void);
    void    InitTick(void);

    bool    InitLogFile(void);
    void    ExitLogFile(void);
    void    PrintLogFile(PCXStr pszLog, Int nLen);

    bool    InitMemTrace(void);
    void    ExitMemTrace(void);

    void    CPUInfo(void);

    void    ComponentAddRef(void);
    void    ComponentRelease(void);
public:
    static EXCEPT_HANDLER   ms_pExceptHandler;
private:
    const ULLong            mc_ullRuntime;
    UInt                    m_uPageSize;
    UInt                    m_uLogLevel;
    UInt                    m_uComLevel;
    UInt                    m_uLogMaxSize;
    Handle                  m_hLogFile;
    LLong                   m_llBaseTime; // core init base time
    LLong                   m_llBaseTick; // core init base tick
    CComponent*             m_pComponent; // develop print to component
    CPlatform::CPUINFO      m_CPUInfo;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_PLATFORM_HXX__
