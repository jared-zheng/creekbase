// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MEM_MANAGER_HXX__
#define __MEM_MANAGER_HXX__

#pragma once

#include "sync.h"
#include "memtraits.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// MM_STATS
struct tagMM_STATS : public tagMM_DUMP
{
public:
    tagMM_STATS(void)
    : llBeginTick(0)
    , llEndTick(0)
    {
    }

    ~tagMM_STATS(void)
    {
    }

    void IncreUsedSize(size_t stSize)
    {
        llUsedSize += (LLong)stSize;
        if (llUsedSize > llMaxUsedSize)
        {
            llMaxUsedSize = llUsedSize;
        }
    }

    void DecreUsedSize(size_t stSize)
    {
        if (llUsedSize > (LLong)stSize)
        {
            llUsedSize -= (LLong)stSize;
        }
        else
        {
            llUsedSize  = 0;
        }
    }

    void IncreCacheSize(size_t stSize)
    {
        llCacheSize += (LLong)stSize;
        if (llCacheSize > llMaxCacheSize)
        {
            llMaxCacheSize = llCacheSize;
        }
    }

    void DecreCacheSize(size_t stSize)
    {
        if (llCacheSize > (LLong)stSize)
        {
            llCacheSize -= (LLong)stSize;
        }
        else
        {
            llCacheSize  = 0;
        }
    }

    void Tick2Time(LLong llBaseTick)
    {
        llUsedTick   += (llAllocTick + llReallocTick + llFreeTick);

        llEndTick     = (llUsedTick * TIMET_S2MS);
        llUsedTick    = (llEndTick / llBaseTick);

        llEndTick     = (llAllocTick * TIMET_S2MS);
        llAllocTick   = (llEndTick / llBaseTick);

        llEndTick     = (llReallocTick * TIMET_S2MS);
        llReallocTick = (llEndTick / llBaseTick);

        llEndTick     = (llFreeTick * TIMET_S2MS);
        llFreeTick    = (llEndTick / llBaseTick);
    }
public:
    LLong   llBeginTick;
    LLong   llEndTick;
};
typedef tagMM_STATS MM_STATS, *PMM_STATS;

///////////////////////////////////////////////////////////////////
// MM_STATS_SCOPE
typedef struct tagMM_STATS_SCOPE
{
public:
    enum SCOPE_TYPE
    {
        SCOPET_ALLOC = 0,
        SCOPET_REALLOC,
        SCOPET_FREE,
        SCOPET_USED,
    };
public:
    tagMM_STATS_SCOPE(MM_STATS& stats, SCOPE_TYPE eType = SCOPET_ALLOC)
    : m_eType(eType)
    , m_pStats(&stats)
    {
        stats.llBeginTick = CPlatform::GetOSRunningTick();
    }


    ~tagMM_STATS_SCOPE(void)
    {
        m_pStats->llEndTick = CPlatform::GetOSRunningTick();
        if (m_pStats->llEndTick > m_pStats->llBeginTick)
        {
            switch (m_eType)
            {
            case SCOPET_ALLOC:
                {
                    m_pStats->llAllocTick   += (m_pStats->llEndTick - m_pStats->llBeginTick);
                }
                break;
            case SCOPET_REALLOC:
                {
                    m_pStats->llReallocTick += (m_pStats->llEndTick - m_pStats->llBeginTick);
                }
                break;
            case SCOPET_FREE:
                {
                    m_pStats->llFreeTick    += (m_pStats->llEndTick - m_pStats->llBeginTick);
                }
                break;
            case SCOPET_USED:
            default:
                {
                    m_pStats->llUsedTick    += (m_pStats->llEndTick - m_pStats->llBeginTick);
                }
            }
        }
    }
public:
    SCOPE_TYPE   m_eType;
    PMM_STATS    m_pStats;
}MM_STATS_SCOPE, *PMM_STATS_SCOPE;

///////////////////////////////////////////////////////////////////
// CMemCache
class CMemCache : public MObject
{
public:
    enum CACHE_DEF_CONST
    {
        CACHE_DEF_RATIO  = 2,
        CACHE_DEF_ALIGN  = 16, // default boundary to align memory allocations
        CACHE_DEF_80     = 820 * 1024,
        CACHE_DEF_50     = 512 * 1024,
    };

    typedef struct tagCACHE_BLOCK
    {
        tagCACHE_BLOCK*   pNext;
        size_t            stMagic;
    }CACHE_BLOCK, *PCACHE_BLOCK;

    typedef struct tagCACHE_CHUNK
    {
        tagCACHE_CHUNK*   pPrev;
        tagCACHE_CHUNK*   pNext;
        tagCACHE_CHUNK*   pFPrev;
        tagCACHE_CHUNK*   pFNext;
        tagCACHE_BLOCK*   pBlock;
        size_t            stCount;
        size_t            stMagic;
        size_t            stState;
    }CACHE_CHUNK, *PCACHE_CHUNK;
public:
    CMemCache(void);
    ~CMemCache(void);

    bool   Init(size_t stSize, size_t stGrow, size_t stMaxSize, size_t stType);
    void   Exit(MM_STATS& MMStatus, bool bDump = false);

    PByte  Alloc(MM_STATS& MMStatus);
    bool   Free(PByte pData, MM_STATS& MMStatus);
    bool   Check(PByte pData);
    PByte  Dump(PByte pData, size_t& stSize);

    size_t Size(void) const;  // block align size
    size_t Used(void) const;  // block used  size
    size_t Cache(void) const; // cache chunk size

    PINDEX AddCache(CMemCache*& pHead);
    void   RemoveCache(CMemCache*& pHead);
    void   PrevCache(CMemCache*& pPrev);
private:
    CMemCache(const CMemCache&);
    CMemCache& operator=(const CMemCache&);

    bool   CheckBlock(PCACHE_BLOCK pBlock);

    bool   IncrChunk(MM_STATS& MMStatus);
    bool   DecrChunk(PCACHE_CHUNK& pChunk, MM_STATS& MMStatus);

    bool   AllocChunk(PCACHE_CHUNK& pChunk);
private:
    CMemCache*     m_pMCPrev;
    CMemCache*     m_pMCNext;
    //
    CACHE_CHUNK*   m_pChunkHead;      // chunk list
    CACHE_CHUNK*   m_pChunkFree;      // free chunk
    //
    size_t         m_stBlockSize;
    size_t         m_stBlockCount;
    size_t         m_stBlockUsed;
    size_t         m_stChunkSize;
    size_t         m_stChunkCount;
    size_t         m_stChunkLimit;
    //
    CSyncLock      m_CacheLock;
};

///////////////////////////////////////////////////////////////////
// CMemManager
class CMemManager : public CMemTraits
{
public:
    CMemManager(void);
    ~CMemManager(void);

    bool   Init(void);
    void   Exit(void);

#ifdef __RUNTIME_DEBUG__
    void*  Alloc(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType);
    void*  ReAlloc(void* pOld, size_t stNewSize, PCXStr szFile, PCXStr szFunc, size_t stLine);
#else
    void*  Alloc(size_t stSize, size_t stType);
    void*  ReAlloc(void* pOld, size_t stNewSize);
#endif
    void   Free(void* p, size_t stSize);
    void   Dump(MM_DUMP& Dump);
    void   Dump(void* p);
    bool   Check(void* p);
    // cache
    PINDEX CacheCreate(size_t stSize, size_t stGrow, size_t stMaxSize, size_t stType);
    bool   CacheDestroy(PINDEX index);

    size_t CacheAlign(PINDEX index);
    size_t CacheUsed(PINDEX index);

    PByte  CacheAlloc(PINDEX index);
    bool   CacheFree(PINDEX index, PByte pData);
    bool   CacheCheck(PINDEX index, PByte pData);
    PByte  CacheDump(PINDEX index, PByte pData, size_t& stSize);
    void   CacheDump(PINDEX index);
private:
    CMemManager(const CMemManager&);
    CMemManager& operator=(const CMemManager&);

#ifdef __RUNTIME_DEBUG__
    void*  ChunkBlockAlloc(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine);
    void*  ChunkDirectAlloc(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType);
#else
    void*  ChunkBlockAlloc(size_t stSize);
    void*  ChunkDirectAlloc(size_t stSize, size_t stType);
#endif
    void   ChunkBlockFree(PMM_CHUNK pChunk, PMM_BLOCK pBlock);
    void   ChunkDirectFree(PMM_CHUNK pChunk, PMM_BLOCK pBlock);

    bool   CheckAndDump(void* p, bool bDump = false);
    bool   CheckBlock(void* p, PMM_CHUNK& pChunk, PMM_BLOCK& pBlock);

    size_t CalcOffset(size_t stChunk, size_t stBlock);

    void   LeakReport(void);
    size_t LeakDump(PMM_CHUNK pChunk, size_t stIndex);

    void   ErrorDump(void);
    void   StatsReport(void);

    void   CacheDump(void);
private:
    size_t          m_stPageSize;
    size_t          m_stAllChunk;
    CMemCache*      m_pMemCache;
    MM_STATS        m_MMStats;
    MM_CHUNK_LIST   m_Chunk[MM_LIST_COUNT];
    CSyncLock       m_MemLock;
    CSyncLock       m_CacheLock;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __MEM_MANAGER_HXX__
