// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MEM_TRAITS_INL__
#define __MEM_TRAITS_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CMemTraits
INLINE CMemTraits::CMemTraits(void)
{
}

INLINE CMemTraits::~CMemTraits(void)
{
}

INLINE CMemTraits::CMemTraits(const CMemTraits&)
{
}

INLINE CMemTraits& CMemTraits::operator=(const CMemTraits&)
{
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CMemTraits::tagMM_BLOCK
INLINE CMemTraits::tagMM_BLOCK::tagMM_BLOCK(void)
: stMagic(0)
#ifdef __RUNTIME_DEBUG__
, pszFunc(nullptr)
, pszFile(nullptr)
#endif
{
}

INLINE CMemTraits::tagMM_BLOCK::~tagMM_BLOCK(void)
{
}

#ifdef __RUNTIME_DEBUG__
INLINE void* CMemTraits::tagMM_BLOCK::Init(size_t stOffset, size_t stAlloc, PCXStr szFile, PCXStr szFunc, size_t stLine)
{
    stMagic = (stOffset << (size_t)MM_MAGIC_BLOCK_OFFSET) + (stLine & (size_t)MM_MAGIC_BLOCK_MASK);
    pszFunc = szFunc;
    pszFile = szFile;

    PByte pData = (PByte)(this);
    *(size_t*)(pData + (size_t)MM_BLOCK_HEAD) = (size_t)MM_MAGIC_BLOCK_PREFIX;
    *(size_t*)(pData + stAlloc - (size_t)MM_BLOCK_XXXFIX) = (size_t)MM_MAGIC_BLOCK_SUFFIX;
    return (pData + (size_t)MM_BLOCK_TODATA);
}

INLINE void CMemTraits::tagMM_BLOCK::Exit(size_t stAlloc)
{
    stMagic = 0;
    pszFunc = nullptr;
    pszFile = nullptr;

    PByte pData = (PByte)(this);
    *(size_t*)(pData + (size_t)MM_BLOCK_HEAD) = 0;
    *(size_t*)(pData + stAlloc - (size_t)MM_BLOCK_XXXFIX) = 0;
}

INLINE size_t CMemTraits::tagMM_BLOCK::Offset(void) const
{
    return ((stMagic >> (size_t)MM_MAGIC_BLOCK_OFFSET) & (size_t)MM_MAGIC_BLOCK_MASK);
}

FORCEINLINE bool CMemTraits::tagMM_BLOCK::InUsed(void) const
{
    return (*(size_t*)((PByte)(this) + (size_t)MM_BLOCK_HEAD) == (size_t)MM_MAGIC_BLOCK_PREFIX);
}

FORCEINLINE bool CMemTraits::tagMM_BLOCK::Check(size_t stAlloc) const
{
    return (*(size_t*)((PByte)(this) + stAlloc - (size_t)MM_BLOCK_XXXFIX) == (size_t)MM_MAGIC_BLOCK_SUFFIX);
}

#else   // __RUNTIME_DEBUG__
INLINE void* CMemTraits::tagMM_BLOCK::Init(size_t stOffset)
{
    stMagic = (size_t)MM_MAGIC_BLOCK_INUSED + (stOffset & (size_t)MM_MAGIC_BLOCK_OFFSET);
    return ((PByte)this + (size_t)MM_BLOCK_TODATA);
}

INLINE void CMemTraits::tagMM_BLOCK::Exit(void)
{
    stMagic = 0;
}

INLINE size_t CMemTraits::tagMM_BLOCK::Offset(void) const
{
    return (stMagic & (size_t)MM_MAGIC_BLOCK_OFFSET);
}

FORCEINLINE bool CMemTraits::tagMM_BLOCK::InUsed(void) const
{
    return ((stMagic & (size_t)MM_MAGIC_BLOCK_MASK) == (size_t)MM_MAGIC_BLOCK_INUSED);
}

FORCEINLINE bool CMemTraits::tagMM_BLOCK::Check(size_t) const
{
    return ((stMagic & (size_t)MM_MAGIC_BLOCK_MASK) == (size_t)MM_MAGIC_BLOCK_INUSED);
}
#endif  // __RUNTIME_DEBUG__

///////////////////////////////////////////////////////////////////
// CMemTraits::tagMM_CHUNK
INLINE CMemTraits::tagMM_CHUNK::tagMM_CHUNK(void)
: pPrev(nullptr)
, pNext(nullptr)
{
}

INLINE CMemTraits::tagMM_CHUNK::~tagMM_CHUNK(void)
{
}

INLINE void CMemTraits::tagMM_CHUNK::Init(size_t stData, size_t stIndex)
{
    //ChunkList init list ptr, do nothing here
    //pPrev  = nullptr;
    //pNext  = nullptr;
    if (stIndex != (size_t)MM_LIST_DIRECT)
    {
        mChunk.X.HighValue = (MSIZE_T)stIndex; // alloc size array index
        mChunk.X.LowValue  = (MSIZE_T)stData;  // chunk block count
        mBlock.ALLValue    = 0;
    }
    else
    {
        mChunk.ALLValue    = stData; // direct alloc size
        mBlock.ALLValue    = (size_t)MM_MAGIC_CHUNK_DIRECT;
    }
}

INLINE void CMemTraits::tagMM_CHUNK::Exit(void)
{
}

INLINE size_t CMemTraits::tagMM_CHUNK::AllocBlock(PMM_BLOCK& pAlloc, size_t stAlloc)
{
    assert(mBlock.ALLValue != (size_t)MM_MAGIC_CHUNK_FULL);
    size_t stRet = 0;
    // 1. free block alloc first
    if (mBlock.ALLValue == 0)
    {
        stRet = (size_t)mChunk.X.LowValue;
        assert(stRet > 0);
        --(mChunk.X.LowValue);
        pAlloc = (PMM_BLOCK)((PByte)this + (size_t)MM_CHUNK_HEAD + mChunk.X.LowValue * stAlloc);
    }
    else if (mBlock.ALLValue != (size_t)MM_MAGIC_CHUNK_DIRECT)
    {    // high : ptr offset, low : count
        assert(mBlock.X.LowValue > 0); // free count
        --(mBlock.X.LowValue);
        pAlloc = (PMM_BLOCK)((PByte)this + mBlock.X.HighValue);
        if (pAlloc->stMagic != 0)
        {
            assert(mBlock.X.LowValue > 0);
            mBlock.X.HighValue = (MSIZE_T)(pAlloc->stMagic & (size_t)MM_CHUNK_OFFSET);
            assert((pAlloc->stMagic & (size_t)MM_MAGIC_BLOCK_FREE) == (size_t)MM_MAGIC_BLOCK_FREE);
        }
        else // only one free block, set to none
        {
            assert(mBlock.X.LowValue == 0);
            mBlock.ALLValue = 0;
        }
    }
    else // mBlock.ALLValue == (size_t)MM_MAGIC_CHUNK_DIRECT
    {
        pAlloc = (PMM_BLOCK)(this + 1);
    }
    return stRet;
}

#ifdef __RUNTIME_DEBUG__
INLINE size_t CMemTraits::tagMM_CHUNK::FreeBlock(PMM_BLOCK pFree, size_t stAlloc)
#else
INLINE size_t CMemTraits::tagMM_CHUNK::FreeBlock(PMM_BLOCK pFree)
#endif
{
    assert(mBlock.ALLValue != (size_t)MM_MAGIC_CHUNK_FULL);
    assert(mBlock.ALLValue != (size_t)MM_MAGIC_CHUNK_DIRECT);
#ifdef __RUNTIME_DEBUG__
    size_t stOffset = (size_t)(pFree);
    size_t stChunk  = (size_t)(this);
    assert(stOffset > stChunk);
    stOffset -= stChunk;
    pFree->Exit(stAlloc);
#else
    pFree->Exit();
    size_t stOffset = ((size_t)(pFree) - (size_t)(this));
#endif

    // high : ptr offset, low : count
    if (mBlock.ALLValue != 0)
    {
        pFree->stMagic = ((size_t)mBlock.X.HighValue | (size_t)MM_MAGIC_BLOCK_FREE);
        mBlock.X.HighValue = (MSIZE_T)stOffset;

        ++(mBlock.X.LowValue);
        assert(mBlock.X.LowValue > 1);
    }
    else // none
    {
        mBlock.X.HighValue = (MSIZE_T)stOffset;
        mBlock.X.LowValue  = 1;
    }
    return (mBlock.X.LowValue);
}

FORCEINLINE size_t CMemTraits::tagMM_CHUNK::GetIndex(void) const
{
    return (size_t)(mChunk.X.HighValue);
}

FORCEINLINE size_t CMemTraits::tagMM_CHUNK::GetCount(void) const
{
    return (size_t)(mChunk.X.LowValue);
}

FORCEINLINE size_t CMemTraits::tagMM_CHUNK::GetAlloc(void) const
{
    assert(IsDirect());
    return mChunk.ALLValue;
}

FORCEINLINE size_t CMemTraits::tagMM_CHUNK::GetFree(void) const
{
    assert(IsDirect() == false);
    assert(IsFull() == false);
    return (size_t)(mBlock.X.LowValue);
}

FORCEINLINE size_t CMemTraits::tagMM_CHUNK::GetData(void) const
{
    return mBlock.ALLValue;
}

FORCEINLINE bool CMemTraits::tagMM_CHUNK::IsAllAlloc(void) const
{
    return ((mBlock.ALLValue == 0) && (mChunk.X.LowValue == 0));
}

FORCEINLINE bool CMemTraits::tagMM_CHUNK::IsDirect(void) const
{
    return (mBlock.ALLValue == (size_t)MM_MAGIC_CHUNK_DIRECT);
}

FORCEINLINE bool CMemTraits::tagMM_CHUNK::IsFull(void) const
{
    return (mBlock.ALLValue == (size_t)MM_MAGIC_CHUNK_FULL);
}

FORCEINLINE bool CMemTraits::tagMM_CHUNK::IsNull(void) const
{
    return (mBlock.ALLValue == 0);
}

FORCEINLINE void CMemTraits::tagMM_CHUNK::SetFull(void)
{
    mBlock.ALLValue = (size_t)MM_MAGIC_CHUNK_FULL;
}

FORCEINLINE void CMemTraits::tagMM_CHUNK::CleanFull(void)
{
    mBlock.ALLValue = 0;
}

FORCEINLINE CMemTraits::tagMM_CHUNK* CMemTraits::tagMM_CHUNK::GetPrev(void)
{
    return (pPrev);
}

FORCEINLINE CMemTraits::tagMM_CHUNK* CMemTraits::tagMM_CHUNK::GetNext(void)
{
    return (pNext);
}

FORCEINLINE CMemTraits::tagMM_BLOCK* CMemTraits::tagMM_CHUNK::GetBlock(CMemTraits::tagMM_BLOCK* pBlock)
{
    if (pBlock == nullptr)
    {
        return (PMM_BLOCK)((PByte)this + mBlock.X.HighValue);
    }
    else
    {
        return (PMM_BLOCK)((PByte)this + (pBlock->stMagic & (size_t)MM_CHUNK_OFFSET));
    }
}

///////////////////////////////////////////////////////////////////
// CMemTraits::tagMM_CHUNK_LIST
INLINE CMemTraits::tagMM_CHUNK_LIST::tagMM_CHUNK_LIST(void)
: pHead(nullptr)
, pFull(nullptr)
{
}

INLINE CMemTraits::tagMM_CHUNK_LIST::~tagMM_CHUNK_LIST(void)
{
}

INLINE void CMemTraits::tagMM_CHUNK_LIST::IncreChunk(PMM_CHUNK pChunk)
{
    if (pHead != nullptr)
    {
        pHead->pNext  = pChunk;
        pChunk->pPrev = pHead;
    }
    pHead = pChunk;
}

INLINE void CMemTraits::tagMM_CHUNK_LIST::DecreChunk(PMM_CHUNK pChunk)
{
    if (pHead == pChunk)
    {
        pHead = pHead->pPrev;
    }
    if (pChunk->pPrev != nullptr)
    {
        pChunk->pPrev->pNext = pChunk->pNext;
    }
    if (pChunk->pNext != nullptr)
    {
        pChunk->pNext->pPrev = pChunk->pPrev;
    }
}

INLINE void CMemTraits::tagMM_CHUNK_LIST::Full2Chunk(PMM_CHUNK pChunk)
{
    if (pFull == pChunk)
    {
        pFull = pFull->pPrev;
    }
    if (pChunk->pPrev != nullptr)
    {
        pChunk->pPrev->pNext = pChunk->pNext;
    }
    if (pChunk->pNext != nullptr)
    {
        pChunk->pNext->pPrev = pChunk->pPrev;
    }
    pChunk->pPrev = nullptr;
    pChunk->pNext = nullptr;

    pChunk->pPrev = pHead;
    if (pHead != nullptr)
    {
        pHead->pNext = pChunk;
    }
    pHead = pChunk;
}

INLINE void CMemTraits::tagMM_CHUNK_LIST::Chunk2Full(void)
{
    if (pHead != nullptr)
    {
        PMM_CHUNK pChunk = pHead;
        pHead = pHead->pPrev;
        if (pHead != nullptr)
        {
            pHead->pNext = nullptr;
        }

        pChunk->pPrev = nullptr;
        pChunk->pNext = nullptr;
        if (pFull != nullptr)
        {
            pFull->pNext  = pChunk;
            pChunk->pPrev = pFull;
        }
        pFull = pChunk;
    }
}

INLINE CMemTraits::PMM_CHUNK CMemTraits::tagMM_CHUNK_LIST::GetHead(void)
{
    return pHead;
}

INLINE CMemTraits::PMM_CHUNK CMemTraits::tagMM_CHUNK_LIST::GetFull(void)
{
    return pFull;
}

///////////////////////////////////////////////////////////////////
// CMemTraits
INLINE size_t CMemTraits::SELECT(size_t stSize, size_t& stAlloc)
{
    size_t stIndex = 0;
    if (stSize <= MM_ALIGN_SIZE[MM_SELECT_REGION_MID]) // [0, 21]
    {
        if (stSize <= MM_ALIGN_SIZE[MM_SELECT_REGION_LMIN])
        {
            stIndex = 0; // [0, 7]
        }
        else if (stSize <= MM_ALIGN_SIZE[MM_SELECT_REGION_LMAX])
        {
            stIndex = MM_SELECT_REGION_LMIN; // [7, 14]
        }
        else
        {
            stIndex = MM_SELECT_REGION_LMAX; // [14, 21]
        }
    }
    else // [21, 42]
    {
        if (stSize <= MM_ALIGN_SIZE[MM_SELECT_REGION_HMIN])
        {
            stIndex = MM_SELECT_REGION_MID; // [21, 28]
        }
        else if (stSize <= MM_ALIGN_SIZE[MM_SELECT_REGION_HMAX])
        {
            stIndex = MM_SELECT_REGION_HMIN; // [28, 35]
        }
        else
        {
            stIndex = MM_SELECT_REGION_HMAX; // [35, 42]
        }
    }
    return REGION(stSize, stIndex, stAlloc);
}

INLINE size_t CMemTraits::REGION(size_t stSize, size_t stIndex, size_t& stAlloc)
{
    if (stSize >= MM_ALIGN_SIZE[MM_SELECT_REGION_STEP + stIndex])
    {
        stIndex += MM_SELECT_REGION_STEP;
    }
    if (stSize <= MM_ALIGN_SIZE[0 + stIndex])
    {
        stAlloc = MM_ALIGN_SIZE[0 + stIndex];
        return (0 + stIndex);
    }
    else if (stSize <= MM_ALIGN_SIZE[1 + stIndex])
    {
        stAlloc = MM_ALIGN_SIZE[1 + stIndex];
        return (1 + stIndex);
    }
    else if (stSize <= MM_ALIGN_SIZE[2 + stIndex])
    {
        stAlloc = MM_ALIGN_SIZE[2 + stIndex];
        return (2 + stIndex);
    }
    else if (stSize <= MM_ALIGN_SIZE[3 + stIndex])
    {
        stAlloc = MM_ALIGN_SIZE[3 + stIndex];
        return (3 + stIndex);
    }
    else if (stSize <= MM_ALIGN_SIZE[4 + stIndex])
    {
        stAlloc = MM_ALIGN_SIZE[4 + stIndex];
        return (4 + stIndex);
    }
    assert(0);
    return (size_t)(MM_LIST_COUNT);
}

///////////////////////////////////////////////////////////////////////
//// last size 0X8000[32768] - [(MM_CHUNK_MAXSIZE - MM_CHUNK_HEAD == MM_CHUNK_DATASIZE) / 2 == MM_BLOCK_MAXSIZE]-> (32bit)0X7FF8[32760], (64bit)0X7FF0[32752]
//size_t i = 0;
//size_t j[8];
//for (i = 1; i < 49;)
//{
//    if (i == 1)
//    {
//        j[0] = (8<<(i>>2)) + (2<<(i-1)); ++i;
//        j[1] = (8<<(i>>2)) + (2<<(i-1)); ++i;
//        j[2] = (8<<(i>>2)) + (2<<(i-1)); ++i;
//        j[3] = (8<<(i>>2)) + (2<<(i-1)); ++i;
//        j[4] = (8<<(i>>2)) + (2<<(i-1)); ++i;
//    }
//    else
//    {
//        j[0] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//        j[1] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//        j[2] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//        j[3] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//        j[4] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//
//    }
//    j[5] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//    j[6] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//    j[7] = (4+((i+6)&3)) << (1+((i+6)>>2)); ++i;
//
//    OUT(TF("\t//  %8d,   %8d,   %8d,   %8d,\n")
//        TF("\t%#08X, %#08X, %#08X, %#08X,\n")
//        TF("\t//  %8d,   %8d,   %8d,   %8d,\n")
//        TF("\t%#08X, %#08X, %#08X, %#08X,\n"),
//        j[0], j[1], j[2], j[3],
//        j[0], j[1], j[2], j[3],
//        j[4], j[5], j[6], j[7],
//        j[4], j[5], j[6], j[7]);
//}
///////////////////////////////////////////////////////////////////
SELECTANY const size_t CMemTraits::MM_ALIGN_SIZE[] =
{
    //      10,         12,         16,         32,
    0X0000000A, 0X0000000C, 0X00000010, 0X00000020,
    //      48,         64,         80,         96,
    0X00000030, 0X00000040, 0X00000050, 0X00000060,
    //     112,        128,        160,        192,
    0X00000070, 0X00000080, 0X000000A0, 0X000000C0,
    //     224,        256,        320,        384,
    0X000000E0, 0X00000100, 0X00000140, 0X00000180,
    //     448,        512,        640,        768,
    0X000001C0, 0X00000200, 0X00000280, 0X00000300,
    //     896,       1024,       1280,       1536,
    0X00000380, 0X00000400, 0X00000500, 0X00000600,
    //    1792,       2048,       2560,       3072,
    0X00000700, 0X00000800, 0X00000A00, 0X00000C00,
    //    3584,       4096,       5120,       6144,
    0X00000E00, 0X00001000, 0X00001400, 0X00001800,
    //    7168,       8192,      10240,      12288,
    0X00001C00, 0X00002000, 0X00002800, 0X00003000,
    //   14336,      16384,      20480,      24576,
    0X00003800, 0X00004000, 0X00005000, 0X00006000,
    //   28672, //     32768,      40960,
    0X00007000, //0X00008000, 0X0000A000,
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    //   32752,      65536, // 0x00010000 - to 43[align only]
    0x00007FF0, 0x00010000,
#else
    //   32760,      65536, // 0x00010000 - to 43[align only]
    0x00007FF8, 0x00010000,
#endif
};

/////////////////////////////////////////////////////////////////////
//// MM_CHUNK_DATASIZE include each align size count
//size_t k = 0;
//size_t i = 0;
//size_t j[8];
//for (i = 0; i < 48;)
//{
//
//        (i = (i >= 48 ? 47 : i));j[0] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[1] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[2] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[3] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[4] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[5] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[6] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//        (i = (i >= 48 ? 47 : i));j[7] =
//        (MM_CHUNK_MAXSIZE - sizeof(MM_CHUNK)) / MM_ALIGN_SIZE[i]; ++i;
//
//    OUT(TF("\t//%d * %d = %d, %d * %d = %d, %d * %d = %d, %d * %d = %d,")
//        TF("\t%#08X, %#08X, %#08X, %#08X,\n\n")
//        TF("\t//%d * %d = %d, %d * %d = %d, %d * %d = %d, %d * %d = %d,")
//        TF("\t%#08X, %#08X, %#08X, %#08X,\n\n"),
//        j[0], MM_ALIGN_SIZE[k + 0], j[0] * MM_ALIGN_SIZE[k + 0],
//        j[1], MM_ALIGN_SIZE[k + 1], j[1] * MM_ALIGN_SIZE[k + 1],
//        j[2], MM_ALIGN_SIZE[k + 2], j[2] * MM_ALIGN_SIZE[k + 2],
//        j[3], MM_ALIGN_SIZE[k + 3], j[3] * MM_ALIGN_SIZE[k + 3],
//        j[0], j[1], j[2], j[3],
//        j[4], MM_ALIGN_SIZE[k + 4], j[4] * MM_ALIGN_SIZE[k + 4],
//        j[5], MM_ALIGN_SIZE[k + 5], j[5] * MM_ALIGN_SIZE[k + 5],
//        j[6], MM_ALIGN_SIZE[k + 6], j[6] * MM_ALIGN_SIZE[k + 6],
//        j[7], MM_ALIGN_SIZE[k + 7], j[7] * MM_ALIGN_SIZE[k + 7],
//        j[4], j[5], j[6], j[7]);
//    k += 8;
//}
/////////////////////////////////////////////////////////////////////
SELECTANY const size_t CMemTraits::MM_SIZE_COUNT[] =
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    //6550 * 10 = 65500, 5458 * 12 = 65496,
    //4094 * 16 = 65504, 2047 * 32 = 65504,
    0X00001996, 0X00001552, 0X00000FFE, 0X000007FF,

    //1364 * 48 = 65472, 1023 * 64 = 65472,
    // 818 * 80 = 65440, 682 * 96  = 65472,
    0X00000554, 0X000003FF, 0X00000332, 0X000002AA,

    //584 * 112 = 65408, 511 * 128 = 65408,
    //409 * 160 = 65440, 341 * 192 = 65472,
    0X00000248, 0X000001FF, 0X00000199, 0X00000155,

    //292 * 224 = 65408, 255 * 256 = 65280,
    //204 * 320 = 65280, 170 * 384 = 65280,
    0X00000124, 0X000000FF, 0X000000CC, 0X000000AA,

    //146 * 448 = 65408, 127 * 512 = 65024,
    //102 * 640 = 65280, 85 * 768  = 65280,
    0X00000092, 0X0000007F, 0X00000066, 0X00000055,

    //73 * 896  = 65408, 63 * 1024 = 64512,
    //51 * 1280 = 65280, 42 * 1536 = 64512,
    0X00000049, 0X0000003F, 0X00000033, 0X0000002A,

    //36 * 1792 = 64512, 31 * 2048 = 63488,
    //25 * 2560 = 64000, 21 * 3072 = 64512,
    0X00000024, 0X0000001F, 0X00000019, 0X00000015,

    //18 * 3584 = 64512, 15 * 4096 = 61440,
    //12 * 5120 = 61440, 10 * 6144 = 61440,
    0X00000012, 0X0000000F, 0X0000000C, 0X0000000A,

    //9 * 7168  = 64512, 7 * 8192  = 57344,
    //6 * 10240 = 61440, 5 * 12288 = 61440,
    0X00000009, 0X00000007, 0X00000006, 0X00000005,

    //4 * 14336 = 57344, 3 * 16384 = 49152,
    //3 * 20480 = 61440, 2 * 24576 = 49152,
    0X00000004, 0X00000003, 0X00000003, 0X00000002,

    //2 * 28672 = 57344, 2 * 32752 = 65504, 0 * 65536 = 0,
    0X00000002, 0X00000002, 0X00000000,
#else
    //6552 * 10 = 65520, 5460 * 12 = 65520,
    //4095 * 16 = 65520, 2047 * 32 = 65504,
    0X00001998, 0X00001554, 0X00000FFF, 0X000007FF,

    //1365 * 48 = 65520, 1023 * 64 = 65472,
    //819 * 80  = 65520, 682 * 96  = 65472,
    0X00000555, 0X000003FF, 0X00000333, 0X000002AA,

    //585 * 112 = 65520, 511 * 128 = 65408,
    //409 * 160 = 65440, 341 * 192 = 65472,
    0X00000249, 0X000001FF, 0X00000199, 0X00000155,

    //292 * 224 = 65408, 255 * 256 = 65280,
    //204 * 320 = 65280, 170 * 384 = 65280,
    0X00000124, 0X000000FF, 0X000000CC, 0X000000AA,

    //146 * 448 = 65408, 127 * 512 = 65024,
    //102 * 640 = 65280, 85 * 768  = 65280,
    0X00000092, 0X0000007F, 0X00000066, 0X00000055,

    //73 * 896  = 65408, 63 * 1024 = 64512,
    //51 * 1280 = 65280, 42 * 1536 = 64512,
    0X00000049, 0X0000003F, 0X00000033, 0X0000002A,

    //36 * 1792 = 64512, 31 * 2048 = 63488,
    //25 * 2560 = 64000, 21 * 3072 = 64512,
    0X00000024, 0X0000001F, 0X00000019, 0X00000015,

    //18 * 3584 = 64512, 15 * 4096 = 61440,
    //12 * 5120 = 61440, 10 * 6144 = 61440,
    0X00000012, 0X0000000F, 0X0000000C, 0X0000000A,

    //9 * 7168  = 64512, 7 * 8192  = 57344,
    //6 * 10240 = 61440, 5 * 12288 = 61440,
    0X00000009, 0X00000007, 0X00000006, 0X00000005,

    //4 * 14336 = 57344, 3 * 16384 = 49152,
    //3 * 20480 = 61440, 2 * 24576 = 49152,
    0X00000004, 0X00000003, 0X00000003, 0X00000002,

    // 2 * 28672 = 57344, 2 * 32760 = 65520, 0 * 65536 = 0,
    0X00000002, 0X00000002, 0X00000000,
#endif
};

#endif // __MEM_TRAITS_INL__
