// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __EVENT_HXX__
#define __EVENT_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetevent.hxx"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetevent.hxx"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CEventQueueImp
class CEventQueueImp : public CEventQueue
{
private:
    typedef struct tagQUEUE_DATA
    {
        uintptr_t     utEvent;
        size_t        stType;
        size_t        stSize;
        uintptr_t     utData;
        ULLong        ullParam;
        CSyncBase*    pSync;
    }QUEUE_DATA, *PQUEUE_DATA;

    typedef CTList<QUEUE_DATA>   LST_QUEUE_DATA;

    ///////////////////////////////////////////////////////////////////
    // CEventQueueThread
    class CEventQueueThread : public CThread
    {
    public:
        explicit CEventQueueThread(CEventQueueImp& EventQueueRef);
        virtual ~CEventQueueThread(void);
    protected:    // CThread
        virtual bool  OnStart(void) OVERRIDE;
        virtual void  OnStop(void) OVERRIDE;
        virtual bool  Run(void) OVERRIDE;
    private:
        CEventQueueImp*   m_pEventQueue;
    };
    typedef CTList<CEventQueueThread*>   LST_EVENTQUEUE_THREAD;
public:
    explicit CEventQueueImp(CEventHandler& EventHandlerRef, size_t stCacheSize = 0, UInt uThreads = 0);
    virtual ~CEventQueueImp(void);

    void    SetIndex(PINDEX index);
    // CEventQueue
    virtual bool   Init(void) OVERRIDE;
    virtual void   Exit(void) OVERRIDE;

    virtual bool   Add(uintptr_t utEvent, uintptr_t utData = 0, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_NONE) OVERRIDE;
    virtual bool   Add(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_REFCOUNT) OVERRIDE;
    virtual bool   Add(uintptr_t utEvent, CStream& Stream, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_CACHE_QUEUE) OVERRIDE;

    virtual bool   MaxCount(UInt uMax) OVERRIDE;//
    virtual UInt   GetCount(void) const OVERRIDE;//
    virtual UInt   GetThreads(void) const OVERRIDE;//
    virtual bool   SetThreads(UInt uThreadCount) OVERRIDE;
    virtual bool   SetThreadEvent(UInt uStart, UInt uStop) OVERRIDE;
    virtual bool   CheckThreads(UInt uTimeout) OVERRIDE;
private:
    bool    QueueHandle(QUEUE_DATA&);

    bool    InitEventQueue(void);
    bool    InitCache(void);

    void    ExitEventQueue(void);
    void    ExitCache(void);

    bool    QueueData(QUEUE_DATA& qd);

    void    ReleaseEventBase(CEventBase* pEventBase);
    void    SignalSyncBase(CSyncBase* pSync);

    void    HandlerAddRef(void);
    void    HandlerRelease(void);
private:
    CEventHandler*          m_pHandler;
    PINDEX                  m_inxId;
    PINDEX                  m_inxCache;
    size_t                  m_stCacheSize;
    UInt                    m_uMaxCount;
    UInt                    m_uThreads;
    UInt                    m_uStartEvent;
    UInt                    m_uStopEvent;
    CEventQueueHandle       m_hEventQueue;
    CSyncLock               m_EQLock;
    LST_QUEUE_DATA          m_QueueData;
    LST_EVENTQUEUE_THREAD   m_WorkThreads;
};

///////////////////////////////////////////////////////////////////
// CEventManager
class CEventManager : public MObject
{
public:
    enum THREAD_WAIT
    {
        THREAD_WAIT_TIME = 100,
    };

    enum TICK_CACHE
    {
        TICK_CACHE_MAX = 16,
    };
public:
    UInt   Init(void);
    void   Exit(void);
    // stCacheSize --> max cache size the queue used, 0 -- none cache used
    // nThreads    --> max threads the queue used, 0 -- only one
    bool   CreateEventQueue(CEventQueuePtr& EventQueuePtrRef, CEventHandler& EventHandlerRef, size_t stCacheSize = 0, UInt uThreads = 0);
    bool   DestroyEventQueue(PINDEX index);
    // tick min time 1ms
    bool   CreateTickEvent(uintptr_t utTick, UInt uInterval, CEventHandler& EventHandlerRef, UInt uCount = (UInt)TIMET_INFINITE);
    bool   DestroyTickEvent(uintptr_t utTick);
    bool   CheckTickEvent(uintptr_t utTick, Int nMultInterval);
    bool   CacheTickEvent(PINDEX index);
protected:
    CEventManager(void);
    ~CEventManager(void);
private:
    CEventManager(const CEventManager&);
    CEventManager& operator=(const CEventManager&);
private:
    typedef CTList<CEventQueueImp*>                   LST_QUEUE;
    typedef CTList<CTickEventImp*>                    LST_TICK;

    typedef CTMap<uintptr_t, CTickEventImp*>          MAP_TICK, *PMAP_TICK;
    typedef CTMap<uintptr_t, CTickEventImp*>::PAIR    PAIR_TICK;
private:
    LST_QUEUE    m_EventQueue;
    LST_TICK     m_TickCache;
    MAP_TICK     m_EventTick;
    CSyncLock    m_EMLock;
};

typedef CTSingleton<CEventManager> SEventManager;

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __EVENT_HXX__
