// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __OBJECT_HXX__
#define __OBJECT_HXX__

#pragma once

#include "object.h"
#include "singleton.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CObjectLinker
class CObjectLinker : public MObject
{
public:
    UInt   Init(void);
    void   Exit(void);

    PINDEX Link(CObject* pObj, const CUUID& uuid, size_t stFlag = CObject::FLAG_NONE);
    PINDEX Link(CObject* pObj, PCXStr pszName, size_t stFlag = CObject::FLAG_NONE);

    void   Unlink(PINDEX index);

    PINDEX Load(PINDEX& inxModule, CObject* pObj, const CUUID& uuid, size_t stFlag, PCXStr pszModule, const CModuleHandle& hModule);
    PINDEX Load(PINDEX& inxModule, CObject* pObj, PCXStr pszName, size_t stFlag, PCXStr pszModule, const CModuleHandle& hModule);

    void   Unload(PCXStr pszClass);

    Int    Find(CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti, bool bExactClass);
    Int    Find(CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName);

    bool   Find(CObjectPtr& ObjectPtr, size_t stKey, const CRTTI& rtti, bool bExactClass);
    bool   Find(CObjectPtr& ObjectPtr, size_t stKey, PCXStr pszClassName);

    bool   GetName(PINDEX index,  CString& strName);
    bool   GetName(PINDEX index,  CStringKey& strNameFix);
    bool   GetName(PINDEX index,  PXStr pszName, size_t stLen);
    size_t SetName(PINDEX& index, PCXStr pszNewName);

    bool   GetModule(PINDEX inxModule, CString& strPath);
protected:
    CObjectLinker(void);
    ~CObjectLinker(void);
private:
    CObjectLinker(const CObjectLinker&);
    CObjectLinker& operator=(const CObjectLinker&);

#ifdef __RUNTIME_DEBUG__
    void   ExitDump(void);
#endif
private:
    typedef struct tagOBJECT
    {
    public:
        tagOBJECT(void)
        : pObj(nullptr)
        {
        }

        ~tagOBJECT(void)
        {
        }
    public:
        CObject*   pObj;
        size_t     stFlag;
        XChar      szName[LMT_KEY];
    }OBJECT, *POBJECT;
    typedef CTMap<size_t, OBJECT>         MAP_OBJECT;
    typedef CTMap<size_t, OBJECT>::PAIR   MAP_OBJECT_PAIR;

    typedef struct tagMODULE
    {
    public:
        tagMODULE(void)
        : stRefCount(1)
        {
        }

        ~tagMODULE(void)
        {
        }
    public:
        size_t          stRefCount;
        CModuleHandle   hModule;
        CString         strPath;
    }MODULE, *PMODULE;
    typedef CTMap<size_t, MODULE>         MAP_MODULE;
    typedef CTMap<size_t, MODULE>::PAIR   MAP_MODULE_PAIR;
private:
    MAP_OBJECT   m_Objects;
    MAP_MODULE   m_Modules;
    CSyncLock    m_SyncLock;
};

typedef CTSingleton<CObjectLinker> SObjectLinker;

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __OBJECT_HXX__
