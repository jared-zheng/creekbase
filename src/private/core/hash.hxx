// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __HASH_HXX__
#define __HASH_HXX__

#pragma once

#include "hash.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// HASH_STATS
struct tagHASH_STATS : public tagHASH_DUMP
{
public:
    tagHASH_STATS(void)
    : llBeginTick(0)
    , llEndTick(0)
    {
    }

    ~tagHASH_STATS(void)
    {
    }

    void Tick2Time(LLong llBaseTick)
    {
        llUsedTick    = (LLong)(llUsedTick * TIMET_S2MS);
        llUsedTick    = (LLong)(llUsedTick / llBaseTick);
    }
public:
    LLong   llBeginTick;
    LLong   llEndTick;
};
typedef tagHASH_STATS HASH_STATS, *PHASH_STATS;

///////////////////////////////////////////////////////////////////
// HASH_STATS_SCOPE
typedef struct tagHASH_STATS_SCOPE
{
public:
    tagHASH_STATS_SCOPE(HASH_STATS& stats)
    : m_pStats(&stats)
    {
        stats.llBeginTick = CPlatform::GetOSRunningTick();
    }


    ~tagHASH_STATS_SCOPE(void)
    {
        m_pStats->llEndTick = CPlatform::GetOSRunningTick();
        if (m_pStats->llEndTick > m_pStats->llBeginTick)
        {
            m_pStats->llUsedTick += (m_pStats->llEndTick - m_pStats->llBeginTick);
        }
        ++(m_pStats->llUsedCount);
    }
public:
    PHASH_STATS   m_pStats;
}HASH_STATS_SCOPE, *PHASH_STATS_SCOPE;

///////////////////////////////////////////////////////////////////
// CHashCRCImp
class CHashCRCImp
{
public:
    enum CRC_TABLE
    {
        // 32bit hash LLong & ULLong
        CRCT_BITS          = 32,
        CRCT_MULVAL        = 23,
        // bit offset & add value
        CRCT_MOV1          = 8,
        CRCT_MOV2          = 5,
        CRCT_ADDVAL        = 3,

        CRCT_SEED1         = 0x7FED7FED,
        CRCT_SEED2         = 0xEEEEEEEE,
        // seeds
        CRCT_SEEDBITSHALF  = 4,
        CRCT_SEEDBITS      = 8,
        // table
        CRCT_TABLECOLMASK  = 255,
        CRCT_TABLECOL      = 256,
        CRCT_TABLESIZEMASK = 2047,
        CRCT_TABLESIZE     = 2048,
    };
public:
    CHashCRCImp(void);
    ~CHashCRCImp(void);

    bool    Init(void);
    void    Exit(void);

    UInt    String32(PCStr  pszString, size_t stLen);
    UInt    String32(PCWStr pszString, size_t stLen);
    UInt    Buffer32(PByte pBuffer, size_t stLen);

    ULLong  String64(PCStr  pszString, size_t stLen);
    ULLong  String64(PCWStr pszString, size_t stLen);
    ULLong  Buffer64(PByte pBuffer, size_t stLen);

    void    Dump(HASH_DUMP& Dump);
private:
    CHashCRCImp(const CHashCRCImp&);
    CHashCRCImp& operator=(const CHashCRCImp&);

    UInt    Reverse32(UInt u);
    ULLong  Reverse64(ULLong ull);

    void    StatsReport(void);
private:
    PUInt          m_puTable32;
    PULLong        m_pullTable64;
    const UInt     m_uSeed32;
    const ULLong   m_ullSeed64;
    HASH_STATS     m_HashStats;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __HASH_HXX__
