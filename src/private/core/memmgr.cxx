// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "core.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CMemCache
CMemCache::CMemCache(void)
: m_pMCPrev(nullptr)
, m_pMCNext(nullptr)
, m_pChunkHead(nullptr)
, m_pChunkFree(nullptr)
, m_stBlockSize(0)
, m_stBlockCount(0)
, m_stBlockUsed(0)
, m_stChunkSize(0)
, m_stChunkCount(0)
, m_stChunkLimit(0)
{
}

CMemCache::~CMemCache(void)
{
}

CMemCache::CMemCache(const CMemCache&)
: m_pMCPrev(nullptr)
, m_pMCNext(nullptr)
, m_pChunkHead(nullptr)
, m_pChunkFree(nullptr)
, m_stBlockSize(0)
, m_stBlockCount(0)
, m_stBlockUsed(0)
, m_stChunkSize(0)
, m_stChunkCount(0)
, m_stChunkLimit(0)
{
}

CMemCache& CMemCache::operator=(const CMemCache&)
{
    return (*this);
}

bool CMemCache::Init(size_t stSize, size_t stGrow, size_t stMaxSize, size_t stType)
{
    CPlatform::MEMINFO mi;
    CPlatform::GetMemInfo(mi);
    size_t stLimitTotal = (size_t)mi.uTotalPhys; // MB
    stLimitTotal *= CACHE_DEF_80; // 80%

    if (stSize <= (size_t)(stLimitTotal / CACHE_DEF_ALIGN))
    {
        stSize = DEF::Maxmin<size_t>((stSize + sizeof(PCACHE_BLOCK) + sizeof(PCACHE_BLOCK)), (size_t)CACHE_DEF_ALIGN, (size_t)(stLimitTotal / CACHE_DEF_ALIGN));
        stSize = DEF::Align<size_t>(stSize, (size_t)CACHE_DEF_ALIGN);
        m_stBlockSize = stSize;

        if (stGrow < stSize)
        {
            if (stSize >= (size_t)CMemTraits::MM_BLOCK_MAXSIZE)
            {
                stGrow = (size_t)CMemTraits::MM_CHUNK_MAXPAGESIZE / stSize;
            }
            else
            {
                stGrow = (size_t)CMemTraits::MM_CHUNK_MAXSIZE / stSize;
            }
            stGrow = (stGrow * stSize + sizeof(CACHE_CHUNK) + CMemTraits::MM_CHUNK_WITHBLOCK);
        }
        else
        {
            stGrow += (sizeof(CACHE_CHUNK) + CMemTraits::MM_CHUNK_WITHBLOCK);
        }
        stGrow = DEF::Maxmin<size_t>(stGrow, (size_t)CMemTraits::MM_CHUNK_MAXSIZE, stLimitTotal);
        stGrow = DEF::Align<size_t>(stGrow, (size_t)CMemTraits::MM_CHUNK_PAGESIZE); // boundary to page size
        m_stChunkSize  = stGrow;

        m_stBlockCount  = (stGrow - CMemTraits::MM_CHUNK_WITHBLOCK - sizeof(CACHE_CHUNK)) / stSize;
        m_stBlockCount &= (size_t)CMemTraits::MM_MAGIC_BLOCK_MAX;
        if (stType != 0)
        {
            m_stBlockCount |= (size_t)CMemTraits::MM_MAGIC_BLOCK_TYPE;
        }

        if (stMaxSize < stGrow)
        {
            stMaxSize = stGrow * CACHE_DEF_RATIO;
        }
        stMaxSize = DEF::Maxmin<size_t>(stMaxSize, (size_t)CMemTraits::MM_CHUNK_MAXSIZE, stLimitTotal);
        stMaxSize = DEF::Align<size_t>(stMaxSize, (size_t)CMemTraits::MM_CHUNK_PAGESIZE); // boundary to page size
        m_stChunkLimit = stMaxSize;

        DEV_MEMMG(TF(" MemCache[%p] init as %d"), this, m_stBlockSize);
        return true;
    }
    DEV_MEMMG(TF(" MemCache[%p] init failed as %lld > limit %lld"), this, stSize, (stLimitTotal / CACHE_DEF_ALIGN));
    return false;
}

void CMemCache::Exit(MM_STATS& MMStatus, bool bDump)
{
    CSyncLockScope scope(m_CacheLock);
    if (m_pChunkHead != nullptr)
    {
        assert(m_pChunkHead->pNext == nullptr);
        assert(m_stChunkCount > 0);
        PCACHE_CHUNK pPrev = nullptr;
        for (PCACHE_CHUNK pChunk = m_pChunkHead; pChunk != nullptr; --m_stChunkCount)
        {
            DEV_MEMMG(TF(" MemCache[%p] free chunk %p"), this, pChunk);
            pPrev  = pChunk->pPrev;
            FREE( pChunk );
            pChunk = pPrev;

            if (bDump == false)
            {
                MMStatus.DecreCacheSize(m_stChunkSize);
            }
        }
        assert(m_stChunkCount == 0);
    }
    m_pChunkHead = nullptr;
    m_pChunkFree = nullptr;

    m_pMCPrev = nullptr;
    m_pMCNext = nullptr;
    DEV_MEMMG(TF(" MemCache[%p] exit as %d"), this, m_stBlockSize);
}

PByte CMemCache::Alloc(MM_STATS& MMStatus)
{
    CSyncLockScope scope(m_CacheLock);
    MM_STATS_SCOPE StatsScope(MMStatus);

    if (m_pChunkFree == nullptr)
    {
        IncrChunk(MMStatus);
    }
    assert(m_pChunkFree != nullptr);
    if (m_pChunkFree != nullptr)
    {
        assert(m_pChunkFree->pFNext == nullptr);
        PCACHE_BLOCK pBlock = nullptr;
        if (m_pChunkFree->stCount > 0)
        {
            --(m_pChunkFree->stCount);
            pBlock = (PCACHE_BLOCK)((PByte)m_pChunkFree + sizeof(CACHE_CHUNK) + (m_pChunkFree->stCount * m_stBlockSize));
            DEV_MEMMG(TF(" MemCache[%p][%p] alloc block at %d---%p"), this, m_pChunkFree, m_pChunkFree->stCount, pBlock);
        }
        else if (m_pChunkFree->pBlock != nullptr)
        {
            assert(m_pChunkFree->pBlock->stMagic <= (m_stBlockCount & (size_t)CMemTraits::MM_MAGIC_BLOCK_MAX));
            DEV_MEMMG(TF(" MemCache[%p][%p]alloc block %p-%d, next block=%p"), this, m_pChunkFree, m_pChunkFree->pBlock, m_pChunkFree->pBlock->stMagic, m_pChunkFree->pBlock->pNext);
            pBlock = m_pChunkFree->pBlock;
            m_pChunkFree->pBlock = pBlock->pNext;
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_ERROR(TF(" MemCache[%p] alloc %p[%p---%p, %d]"), this, m_pChunkFree, m_pChunkFree->pFPrev, m_pChunkFree->pFNext, m_pChunkFree->stCount);
            assert(0);
        }
#endif
        if (pBlock != nullptr)
        {
            pBlock->pNext   = (PCACHE_BLOCK)(m_pChunkFree);
            pBlock->stMagic = (size_t)(CMemTraits::MM_MAGIC_BLOCK_INUSED);

            m_stBlockUsed += m_stBlockSize;
            DEV_MEMMG(TF(" MemCache[%p] alloc %p from %p, used size:%u"), this, pBlock, m_pChunkFree, m_stBlockUsed);
            if ((m_pChunkFree->pBlock == nullptr) &&
                (m_pChunkFree->stCount == 0))
            {
                DEV_MEMMG(TF(" MemCache[%p] chunk[%p] full, prev chunk[%p], next chunk[%p]"), this, m_pChunkFree, m_pChunkFree->pFPrev, m_pChunkFree->pFNext);
                assert(m_pChunkFree->stMagic == (size_t)CMemTraits::MM_MAGIC_CHUNK_DIRECT);
                PCACHE_CHUNK pChunk   = m_pChunkFree;
                m_pChunkFree->stState = (size_t)CMemTraits::MM_MAGIC_CHUNK_FULL;
                m_pChunkFree          = m_pChunkFree->pFPrev;
                if (m_pChunkFree != nullptr)
                {
                    m_pChunkFree->pFNext = nullptr;
                }
                pChunk->pFPrev = nullptr;
            }
            ++pBlock; // move to data
            return (PByte)(pBlock);
        }
    } // (m_pChunkFree != nullptr)
    return nullptr;
}

bool CMemCache::Free(PByte pData, MM_STATS& MMStatus)
{
    CSyncLockScope scope(m_CacheLock);
    MM_STATS_SCOPE StatsScope(MMStatus, MM_STATS_SCOPE::SCOPET_FREE);
    PCACHE_BLOCK pBlock = (PCACHE_BLOCK)(pData - sizeof(PCACHE_BLOCK) - sizeof(PCACHE_BLOCK));
    if (CheckBlock(pBlock))
    {
        PCACHE_CHUNK pChunk = (PCACHE_CHUNK)(pBlock->pNext);
        if (pChunk->pBlock != nullptr)
        {
            pBlock->pNext   = pChunk->pBlock;
            pBlock->stMagic = pChunk->pBlock->stMagic + 1;
            DEV_MEMMG(TF(" MemCache[%p][%p] free block %p-%d, prev block %p"), this, pChunk, pBlock, pBlock->stMagic, pChunk->pBlock);
        }
        else // none
        {
            pBlock->pNext   = nullptr;
            pBlock->stMagic = 1;
            DEV_MEMMG(TF(" MemCache[%p][%p] free block %p"), this, pChunk, pBlock);
        }
        pChunk->pBlock = pBlock;

        assert(m_stBlockUsed >= m_stBlockSize);
        m_stBlockUsed -= m_stBlockSize;
        DEV_MEMMG(TF(" MemCache[%p] free %p return %p"), this, pData, pChunk);

        if ((m_stBlockUsed > m_stChunkLimit) &&
            (pBlock->stMagic == m_stBlockCount))
        {
            DecrChunk(pChunk, MMStatus);
        }
        else if (pChunk->stState == (size_t)CMemTraits::MM_MAGIC_CHUNK_FULL) // ...
        {
            DEV_MEMMG(TF(" MemCache[%p] chunk[%p] free block, free chunk[%p]"), this, pChunk, m_pChunkFree);
            assert(m_pChunkFree != pChunk);
            pChunk->stState = (size_t)CMemTraits::MM_MAGIC_BLOCK_FREE;
            pChunk->pFPrev  = m_pChunkFree;
            pChunk->pFNext  = nullptr;
            if (m_pChunkFree != nullptr)
            {
                m_pChunkFree->pFNext = pChunk;
            }
            m_pChunkFree = pChunk;
        }
        return true;
    }
    return false;
}

bool CMemCache::Check(PByte pData)
{
    CSyncLockScope scope(m_CacheLock);
    return CheckBlock((PCACHE_BLOCK)(pData - sizeof(PCACHE_BLOCK) - sizeof(PCACHE_BLOCK)));
}

PByte CMemCache::Dump(PByte pData, size_t& stSize)
{
    CSyncLockScope scope(m_CacheLock);
    PCACHE_BLOCK pBlock = (PCACHE_BLOCK)(pData - sizeof(PCACHE_BLOCK) - sizeof(PCACHE_BLOCK));
    if (CheckBlock(pBlock))
    {
        stSize = m_stChunkSize;
        return ((PByte)(pBlock->pNext) - CMemTraits::MM_CHUNK_TODATA);
    }
    return nullptr;
}

size_t CMemCache::Size(void) const
{
    return m_stBlockSize;
}

size_t CMemCache::Used(void) const
{
    return m_stBlockUsed;
}

size_t CMemCache::Cache(void) const
{
    return (m_stChunkSize * m_stChunkCount);
}

PINDEX CMemCache::AddCache(CMemCache*& pHead)
{
    if (pHead != nullptr)
    {
        m_pMCPrev = pHead;
        pHead->m_pMCNext = this;

        pHead = this;
    }
    else
    {
        pHead = this;
    }
    return (PINDEX)this;
}

void CMemCache::RemoveCache(CMemCache*& pHead)
{
    if (pHead == this)
    {
        pHead = m_pMCPrev;
    }
    if (m_pMCPrev != nullptr)
    {
        m_pMCPrev->m_pMCNext = m_pMCNext;
    }
    if (m_pMCNext != nullptr)
    {
        m_pMCNext->m_pMCPrev = m_pMCPrev;
    }
}

void CMemCache::PrevCache(CMemCache*& pPrev)
{
    pPrev = m_pMCPrev;
}

bool CMemCache::CheckBlock(PCACHE_BLOCK pBlock)
{
    bool bValid = false;
    TRY_EXCEPTION
        if (pBlock->stMagic == (size_t)CMemTraits::MM_MAGIC_BLOCK_INUSED)
        {
            size_t stMagic = (PCACHE_CHUNK(pBlock->pNext))->stMagic;
            bValid = (stMagic == (size_t)CMemTraits::MM_MAGIC_CHUNK_DIRECT);
        }
    CATCH_EXCEPTION
        DEV_MEMMG(TF(" MemCache[%p] check find invalid ptr %p"), this, ((PByte)pBlock + sizeof(PCACHE_BLOCK) + sizeof(PCACHE_BLOCK)));
    END_EXCEPTION
    return bValid;
}

bool CMemCache::IncrChunk(MM_STATS& MMStatus)
{
    PCACHE_CHUNK pChunk = nullptr;
    if (AllocChunk(pChunk))
    {
        pChunk->pPrev   = m_pChunkHead;
        pChunk->pNext   = nullptr;
        pChunk->pFPrev  = nullptr;
        pChunk->pFNext  = nullptr;
        pChunk->pBlock  = nullptr;
        pChunk->stCount = (size_t)(m_stBlockCount & (size_t)CMemTraits::MM_MAGIC_BLOCK_MAX);
        pChunk->stMagic = (size_t)CMemTraits::MM_MAGIC_CHUNK_DIRECT;
        pChunk->stState = (size_t)CMemTraits::MM_MAGIC_BLOCK_FREE;

        if (m_pChunkHead != nullptr)
        {
            m_pChunkHead->pNext = pChunk;
        }
        m_pChunkHead = pChunk;
        m_pChunkFree = pChunk;

        ++m_stChunkCount;
        DEV_MEMMG(TF(" MemCache[%p] alloc chunk %p[%d]"), this, pChunk, m_stChunkCount);

        MMStatus.IncreCacheSize(m_stChunkSize);
        return true;
    }
    return false;
}

bool CMemCache::DecrChunk(PCACHE_CHUNK& pChunk, MM_STATS& MMStatus)
{
    if (m_pChunkHead == pChunk)
    {
        assert(m_pChunkHead->pNext == nullptr);
        m_pChunkHead = pChunk->pPrev;
    }
    if (pChunk->pPrev != nullptr)
    {
        pChunk->pPrev->pNext = pChunk->pNext;
    }
    if (pChunk->pNext != nullptr)
    {
        pChunk->pNext->pPrev = pChunk->pPrev;
    }

    if (m_pChunkFree == pChunk)
    {
        m_pChunkFree = pChunk->pFPrev;
    }
    if (pChunk->pFPrev != nullptr)
    {
        pChunk->pFPrev->pFNext = pChunk->pFNext;
    }
    if (pChunk->pFNext != nullptr)
    {
        pChunk->pFNext->pFPrev = pChunk->pFPrev;
    }
    FREE( pChunk );
    assert(m_stChunkCount > 0);
    --m_stChunkCount;
    DEV_MEMMG(TF(" MemCache[%p] free chunk %p as overflow"), this, pChunk);

    MMStatus.DecreCacheSize(m_stChunkSize);
    return true;
}

bool CMemCache::AllocChunk(PCACHE_CHUNK& pChunk)
{
    // reserved MM_CHUNK_WITHBLOCK for mem-manager
    if ((m_stBlockCount & (size_t)CMemTraits::MM_MAGIC_BLOCK_TYPE) == 0)
    {
        pChunk = reinterpret_cast<PCACHE_CHUNK>( ALLOC( (m_stChunkSize - (size_t)CMemTraits::MM_CHUNK_WITHBLOCK) ) );
    }
    else
    {
        pChunk = reinterpret_cast<PCACHE_CHUNK>( ALLOCEX( (m_stChunkSize - (size_t)CMemTraits::MM_CHUNK_WITHBLOCK), ALLOC_TYPE_EXECUTE ) );
    }
    return (pChunk != nullptr);
}

///////////////////////////////////////////////////////////////////
// CMemManager
CMemManager::CMemManager(void)
: m_stPageSize(0)
, m_stAllChunk(0)
, m_pMemCache(nullptr)
{
}

CMemManager::~CMemManager(void)
{
}

CMemManager::CMemManager(const CMemManager&)
: m_stPageSize(0)
, m_stAllChunk(0)
, m_pMemCache(nullptr)
{
}

CMemManager& CMemManager::operator=(const CMemManager&)
{
    return (*this);
}

bool CMemManager::Init(void)
{
    m_stPageSize = (size_t)GPlatform.GetPageSize();
    DEV_DUMP(TF("#---Memory manager init okay---"));
    return true;
}

void CMemManager::Exit(void)
{
    CacheDump();
    // leak  report
    LeakReport();
    // stats report
    StatsReport();
    DEV_DUMP(TF("#---Memory manager exit okay---"));
}

void* CMemManager::Alloc(
#ifdef __RUNTIME_DEBUG__
                         size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType)
#else
                         size_t stSize, size_t stType)
#endif
{
    CSyncLockScope scope(m_MemLock);
    MM_STATS_SCOPE StatsScope(m_MMStats);
    void* p = nullptr;
    if (stSize < (size_t)MM_BLOCK_DATALIMIT)
    {
#ifdef __RUNTIME_DEBUG__
        p = ChunkBlockAlloc(stSize, szFile, szFunc, stLine);
    }
    else
    {
        p = ChunkDirectAlloc(stSize, szFile, szFunc, stLine, stType);
#else
        p = ChunkBlockAlloc(stSize);
    }
    else
    {
        p = ChunkDirectAlloc(stSize, stType);
#endif
    }
    return p;
}

void* CMemManager::ReAlloc(
#ifdef __RUNTIME_DEBUG__
                           void* pOld, size_t stNewSize, PCXStr szFile, PCXStr szFunc, size_t stLine)
#else
                           void* pOld, size_t stNewSize)
#endif
{
    void* pNew = nullptr;
    assert(pOld != nullptr);
    if (pOld != nullptr)
    {
        CSyncLockScope scope(m_MemLock);
        MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_REALLOC);

        PMM_CHUNK pChunk = nullptr;
        PMM_BLOCK pBlock = nullptr;
        if (CheckBlock(pOld, pChunk, pBlock))
        {
            if (pChunk->IsDirect() == false)
            {
                size_t stIndex = pChunk->GetIndex();
                size_t stAlloc = MM_ALIGN_SIZE[stIndex] - (size_t)MM_BLOCK_WITHFIX;

                if (stNewSize > (size_t)MM_BLOCK_DATASIZE)
                {
#ifdef __RUNTIME_DEBUG__
                    pNew = ChunkDirectAlloc(stNewSize, szFile, szFunc, stLine, 0);
#else
                    pNew = ChunkDirectAlloc(stNewSize, 0);
#endif
                }
                else if (stNewSize > stAlloc)
                {
#ifdef __RUNTIME_DEBUG__
                    pNew = ChunkBlockAlloc(stNewSize, szFile, szFunc, stLine);
#else
                    pNew = ChunkBlockAlloc(stNewSize);
#endif
                }
                else
                {
                    pNew = pOld;
                }
                if ((pNew != nullptr) && (pNew != pOld))
                {
                    MM_SAFE::Cpy(pNew, stNewSize, pOld, stAlloc);
                    ChunkBlockFree(pChunk, pBlock);
                    DEV_MEMMG(TF("  %p[%d] realloc %p[%d]"), pOld, stAlloc, pNew, stNewSize);
                }
            }
            else
            {
                size_t stAlloc = pChunk->GetAlloc() - (size_t)MM_BLOCK_WITHFIX; // copy data
                if (stNewSize > stAlloc)
                {
#ifdef __RUNTIME_DEBUG__
                    pNew = ChunkDirectAlloc(stNewSize, szFile, szFunc, stLine, 0);
#else
                    pNew = ChunkDirectAlloc(stNewSize, 0);
#endif
                    if (pNew != nullptr)
                    {
                        MM_SAFE::Cpy(pNew, stNewSize, pOld, stAlloc);
                        ChunkDirectFree(pChunk, pBlock);
                        DEV_MEMMG(TF("  %p[%d] realloc %p[%d]"), pOld, stAlloc, pNew, stNewSize);
                    }
                }
                else
                {
                    pNew = pOld;
                }
            } // chunk type
        } // CheckBlock
    }
    return pNew;
}

void  CMemManager::Free(void* p, size_t)
{
    assert(p != nullptr);
    if (p != nullptr)
    {
        CSyncLockScope scope(m_MemLock);
        MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_FREE);

        PMM_CHUNK pChunk = nullptr;
        PMM_BLOCK pBlock = nullptr;
        if (CheckBlock(p, pChunk, pBlock))
        {
            if (pChunk->IsDirect() == false)
            {
                ChunkBlockFree(pChunk, pBlock);
            }
            else
            {
                ChunkDirectFree(pChunk, pBlock);
            }
        }
    }
}

void  CMemManager::Dump(MM_DUMP& Dump)
{
    MM_STATS Stats = m_MMStats;
    Stats.Tick2Time(GPlatform.GetBaseTick());
    Dump = Stats;
}

void  CMemManager::Dump(void* p)
{
    if (p != nullptr)
    {
        CSyncLockScope scope(m_MemLock);
        MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_USED);
        CheckAndDump(p, true);
    }
    DEV_DUMP(TF("  Total Chunk %d"), m_stAllChunk);
}

bool  CMemManager::Check(void* p)
{
    assert(p != nullptr);
    if (p != nullptr)
    {
        CSyncLockScope scope(m_MemLock);
        MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_USED);
        return CheckAndDump(p);
    }
    return false;
}

void* CMemManager::ChunkBlockAlloc(
#ifdef __RUNTIME_DEBUG__
                                   size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine)
#else
                                   size_t stSize)
#endif
{
    size_t stAlloc = stSize + (size_t)MM_BLOCK_WITHFIX;
    size_t stIndex = SELECT(stAlloc, stAlloc);
    assert(stAlloc == MM_ALIGN_SIZE[stIndex]);

    PMM_BLOCK pBlock = nullptr;
    PMM_CHUNK pChunk = m_Chunk[stIndex].GetHead();

    if (pChunk != nullptr)
    {
#ifdef __RUNTIME_DEBUG__
        assert(pChunk->GetIndex() == stIndex);
        assert((pChunk->GetCount() > 0) || (pChunk->IsNull() == false));

        size_t stRet = pChunk->AllocBlock(pBlock, stAlloc);
        if (stRet > 0)
        {
            DEV_MEMMG(TF("  Alloc1 from cached ChunkList[%d](%p)'s unused block[%p, remian %d blocks], Size(%d)[Alloc(%d)]"), stIndex, pChunk, pBlock, (stRet - 1), stSize, stAlloc);
        }
        else
        {
            DEV_MEMMG(TF("  Alloc1 from cached ChunkList[%d](%p)'s free block[%p], Size(%d)[Alloc(%d)]"), stIndex, pChunk, pBlock, stSize, stAlloc);
        }
#else
        pChunk->AllocBlock(pBlock, stAlloc);
#endif
        if (pChunk->IsAllAlloc())
        {
            DEV_MEMMG(TF("  Cached ChunkList[%d](%p)'s block is all alloc, it's prev chunk is %p"), stIndex, pChunk, pChunk->GetPrev());
            m_Chunk[stIndex].Chunk2Full();
            pChunk->SetFull();
        }
    }
    else
    {
        pChunk = reinterpret_cast<PMM_CHUNK>(GPlatform.Alloc(MM_CHUNK_MAXSIZE));
        if (pChunk != nullptr)
        {
            m_MMStats.IncreUsedSize((size_t)MM_CHUNK_MAXSIZE);

            ++m_stAllChunk;
            m_Chunk[stIndex].IncreChunk(pChunk);

            pChunk->Init(MM_SIZE_COUNT[stIndex], stIndex);
#ifdef __RUNTIME_DEBUG__
            size_t stRet = pChunk->AllocBlock(pBlock, stAlloc);
            assert(stRet > 1);
            assert(stRet == MM_SIZE_COUNT[stIndex]);
            DEV_MEMMG(TF("  Alloc1 from new ChunkList[%d](%p)'s unused block[%p, remian %d blocks], Size(%d)[Alloc(%d)]"), stIndex, pChunk, pBlock, (stRet - 1), stSize, stAlloc);
#else
            pChunk->AllocBlock(pBlock, stAlloc);
#endif
        }
        else
        {
#ifdef __RUNTIME_DEBUG__
            DEV_ERROR(TF("  Alloc block size=%d at %s--->%s--->%d failed!"), stSize, szFile, szFunc, stLine);
#endif
            ErrorDump();
        }
    }

    if (pBlock != nullptr)
    {
        size_t stOffset = CalcOffset((size_t)pChunk, (size_t)pBlock);
#ifdef __RUNTIME_DEBUG__
        DEV_MEMMG(TF("  Alloc2 block by FILE :%s(%p), FUNCTION:%s(%p), LINE:%d"), szFile, szFile, szFunc, szFunc, stLine);
        return pBlock->Init(stOffset, stAlloc, szFile, szFunc, stLine);
#else
        return pBlock->Init(stOffset);
#endif
    }
    return nullptr;
}

void* CMemManager::ChunkDirectAlloc(
#ifdef __RUNTIME_DEBUG__
                                    size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType)
#else
                                    size_t stSize, size_t stType)
#endif
{
    size_t stAlign = DEF::Align<size_t>((stSize + (size_t)MM_CHUNK_WITHBLOCK), m_stPageSize);
    size_t stAlloc = stAlign - (size_t)MM_CHUNK_HEAD; // exclude MM_BLOCK_HEAD
    stType         = (stType == 0) ? ALLOC_TYPE_NORMAL : stType;

    PMM_BLOCK pBlock = nullptr;
    PMM_CHUNK pChunk = reinterpret_cast<PMM_CHUNK>(GPlatform.Alloc(stAlign, (UInt)stType));
    if (pChunk != nullptr)
    {
        m_MMStats.IncreUsedSize(stAlign);

        ++m_stAllChunk;
        m_Chunk[MM_LIST_DIRECT].IncreChunk(pChunk);

        pChunk->Init(stAlloc);
        pChunk->AllocBlock(pBlock, stAlloc);

        DEV_MEMMG(TF("  Alloc3 from new ChunkList[%d](%p)'s single block[%p], Size(%d)[Alloc(%d)]"), MM_LIST_DIRECT, pChunk, pBlock, stSize, stAlloc);
    }
    else
    {
#ifdef __RUNTIME_DEBUG__
        DEV_ERROR(TF("  Alloc chunk size=%d at %s--->%s--->%d failed!"), stSize, szFile, szFunc, stLine);
#endif
        ErrorDump();
    }

    if (pBlock != nullptr)
    {
        size_t stOffset = CalcOffset((size_t)pChunk, (size_t)pBlock);
#ifdef __RUNTIME_DEBUG__
        DEV_MEMMG(TF("  Alloc4 block by FILE :%s(%p), FUNCTION:%s(%p), LINE:%d"), szFile, szFile, szFunc, szFunc, stLine);
        return pBlock->Init(stOffset, stAlloc, szFile, szFunc, stLine);
#else
        return pBlock->Init(stOffset);
#endif
    }
    return nullptr;
}

void  CMemManager::ChunkBlockFree(PMM_CHUNK pChunk, PMM_BLOCK pBlock)
{
    size_t stIndex = pChunk->GetIndex();
#ifdef __RUNTIME_DEBUG__
    size_t stAlloc = MM_ALIGN_SIZE[stIndex];
#else
#endif
    if (pChunk->IsFull())
    {
        DEV_MEMMG(TF("  Chunk(%p) return to cached ChunkList[%d], it's prev Chunk(%p)"), pChunk, stIndex, m_Chunk[stIndex].GetHead());
        m_Chunk[stIndex].Full2Chunk(pChunk);
        pChunk->CleanFull();
    }
#ifdef __RUNTIME_DEBUG__
    DEV_MEMMG(TF("  Free1 block by FILE :%s, FUNCTION:%s, LINE:%d"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK));
    size_t stFree = pChunk->FreeBlock(pBlock, stAlloc);
    DEV_MEMMG(TF("  Free2 ChunkList[%d](%p)'s block[%p, free %d blocks], Alloc Size(%d)"), stIndex, pChunk, pBlock, stFree, stAlloc);
#else
    size_t stFree = pChunk->FreeBlock(pBlock);
#endif
    if (stFree == MM_SIZE_COUNT[stIndex])
    {
        DEV_MEMMG(TF("  Chunk(%p) all blocks free, remove from ChunkList[%d](%p) to release[PREV(%p), NEXT(%p)]"), pChunk, stIndex, m_Chunk[stIndex].GetHead(), pChunk->GetPrev(), pChunk->GetNext());

        m_MMStats.DecreUsedSize((size_t)MM_CHUNK_MAXSIZE);

        assert(m_stAllChunk > 0);
        --m_stAllChunk;
        m_Chunk[stIndex].DecreChunk(pChunk);

        GPlatform.Free(pChunk, MM_CHUNK_MAXSIZE);
    }
}

void  CMemManager::ChunkDirectFree(PMM_CHUNK pChunk,
#ifdef __RUNTIME_DEBUG__
                                   PMM_BLOCK pBlock)
#else
                                   PMM_BLOCK)
#endif
{
#ifdef __RUNTIME_DEBUG__
    assert(m_Chunk[MM_LIST_DIRECT].GetHead() != nullptr);
    DEV_MEMMG(TF("  Free3 block by FILE :%s, FUNCTION:%s, LINE:%d"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK));
    DEV_MEMMG(TF("  Free4 Chunk(%p)[%d] single block, remove from ChunkList[%d](%p) to release[PREV(%p), NEXT(%p)]"), pChunk, pChunk->GetAlloc(), MM_LIST_DIRECT, m_Chunk[MM_LIST_DIRECT].GetHead(), pChunk->GetPrev(), pChunk->GetNext());
#else
    // UNREFERENCED_PARAMETER( pBlock );
#endif
    m_MMStats.DecreUsedSize((pChunk->GetAlloc() + (size_t)MM_CHUNK_HEAD));

    m_Chunk[MM_LIST_DIRECT].DecreChunk(pChunk);
    assert(m_stAllChunk > 0);
    --m_stAllChunk;

    GPlatform.Free(pChunk, (pChunk->GetAlloc() + (size_t)MM_CHUNK_HEAD));
}

bool  CMemManager::CheckAndDump(void* p, bool bDump)
{
    PMM_CHUNK pChunk = nullptr;
    PMM_BLOCK pBlock = nullptr;
    if (CheckBlock(p, pChunk, pBlock) == false)
    {
        return false;
    }
    bool   bRet    = false;
    size_t stAlloc = 0;
    if (pChunk->IsDirect() == false)
    {
        size_t stIndex = pChunk->GetIndex();
        size_t stCount = pChunk->GetCount();
#ifdef __RUNTIME_DEBUG__
        if ((stIndex < (size_t)MM_LIST_DIRECT) && (stCount <= MM_SIZE_COUNT[stIndex]))
        {
            stAlloc = MM_ALIGN_SIZE[stIndex];
            bRet    = pBlock->Check(stAlloc);
        }
#else
        bRet = ((stIndex < (size_t)MM_LIST_DIRECT) && (stCount <= MM_SIZE_COUNT[stIndex]));
        stAlloc = MM_ALIGN_SIZE[stIndex];
#endif
    }
    else
    {
        stAlloc = pChunk->GetAlloc();
#ifdef __RUNTIME_DEBUG__
        bRet    = pBlock->Check(stAlloc);
#else
        bRet    = true;
#endif
    }
    if (bRet && bDump)
    {
#ifdef __RUNTIME_DEBUG__
        DEV_DUMP(TF("  %s->%s(%d), block at %p, size=%u"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK), pBlock, stAlloc);
#else
        DEV_DUMP(TF("  block at %p, size=%u"), pBlock, stAlloc);
#endif
    }
    return bRet;
}

bool CMemManager::CheckBlock(void* p, PMM_CHUNK& pChunk, PMM_BLOCK& pBlock)
{
    pBlock = (PMM_BLOCK)((PByte)p - (size_t)MM_BLOCK_TODATA);
    bool bValid = false;
    TRY_EXCEPTION
        if (pBlock->InUsed())
        {
            size_t stPtr = (size_t)pBlock;
            assert(stPtr > pBlock->Offset() * m_stPageSize);
            stPtr = DEF::Align<size_t>(stPtr, m_stPageSize);
            stPtr -= (pBlock->Offset() * m_stPageSize);

            pChunk = (PMM_CHUNK)stPtr;
            bValid = true;
        }
    CATCH_EXCEPTION
        DEV_MEMMG(TF("  Memory manager check find invalid ptr %p"), p);
    END_EXCEPTION
    return bValid;
}

size_t CMemManager::CalcOffset(size_t stChunk, size_t stBlock)
{
    assert(stBlock > stChunk);
    stBlock = DEF::Align<size_t>(stBlock, m_stPageSize);
    return ((stBlock - stChunk) / m_stPageSize);
}

void  CMemManager::LeakReport(void)
{
    size_t   stCount  = 0;
    PMM_CHUNK pChunk  = nullptr;
    for (size_t i = 0; i < (size_t)MM_LIST_DIRECT; ++i)
    {
        pChunk = m_Chunk[i].GetHead();
        if (pChunk != nullptr)
        {
            stCount = MM_SIZE_COUNT[i];
            do
            {
                //DEV_MEMMG(TF("#---Memory manager Leak Report [Check un-release Chunk[%d]%p, %d + %d ?= %d]---"), i, pChunk, pChunk->GetCount(), pChunk->GetFree(), stCount);
                if ((pChunk->GetCount() + pChunk->GetFree()) == stCount)
                {
                    DEV_MEMMG(TF("#---Memory manager Leak Report [Check Chunk[%d]%p release]---"), i, pChunk);
                    m_MMStats.DecreUsedSize((size_t)MM_CHUNK_MAXSIZE);

                    assert(m_stAllChunk > 0);
                    --m_stAllChunk;
                    m_Chunk[i].DecreChunk(pChunk);
                    GPlatform.Free(pChunk, MM_CHUNK_MAXSIZE);

                    pChunk = m_Chunk[i].GetHead();
                }
                else
                {
                    DEV_MEMMG(TF("#---Memory manager Leak Report [Check Chunk[%d]%p Leak]---"), i, pChunk);
                    pChunk = pChunk->GetPrev();
                }
            } while (pChunk != nullptr);
        }
        else
        {
            DEV_MEMMG(TF("#---Memory manager Leak Report [Check Chunk[%d] is null]---"), i);
        }
    }

    DEV_DUMP(TF("#---Memory manager Leak Report [Total Leak Chunk %d]---"), m_stAllChunk);

    size_t stLeak  = 0;
    size_t stBlock = 0;
    for (size_t i = 0; i < (size_t)MM_LIST_DIRECT; ++i)
    {
        pChunk = m_Chunk[i].GetHead();
        if (pChunk != nullptr)
        {
            stCount = MM_SIZE_COUNT[i];
            do
            {
                stCount -= pChunk->GetCount();
                if ((pChunk->IsNull()) || (stCount > pChunk->GetFree()))
                {
                    stBlock += LeakDump(pChunk, i);
                }
                else
                {
                    DEV_MEMMG(TF("   ??? Chunk(%p) -- %d[%d]"), pChunk, pChunk->GetBlock(), pChunk->GetFree(), stCount);
                }
                ++stLeak;
                pChunk = pChunk->GetPrev();
            } while (pChunk != nullptr);
        }

        pChunk = m_Chunk[i].GetFull();
        if (pChunk != nullptr)
        {
            do
            {
                if (pChunk->IsFull())
                {
                    stBlock += LeakDump(pChunk, i);
                }
                else
                {
                    DEV_MEMMG(TF("   Full Chunk(%p) %d - %d"), pChunk, pChunk->GetCount(), pChunk->GetFree());
                }
                ++stLeak;
                pChunk = pChunk->GetPrev();
            } while (pChunk != nullptr);
        }
    }
    assert(m_Chunk[MM_LIST_DIRECT].GetFull() == nullptr);
    pChunk = m_Chunk[MM_LIST_DIRECT].GetHead();
    while (pChunk != nullptr)
    {
        if (pChunk->IsDirect())
        {
            stBlock += LeakDump(pChunk, (size_t)MM_LIST_DIRECT);
        }
        else
        {
            DEV_MEMMG(TF("  Direct Chunk(%p) %#X"), pChunk, pChunk->GetData());
        }
        ++stLeak;
        pChunk = pChunk->GetPrev();
    }
    DEV_DUMP(TF("# Report Chunk %d, Block %d"), stLeak, stBlock);
}

size_t CMemManager::LeakDump(PMM_CHUNK pChunk, size_t stIndex)
{
    if (stIndex < (size_t)MM_LIST_DIRECT)
    {
        size_t stAlloc = MM_ALIGN_SIZE[stIndex];
        size_t stCount = MM_SIZE_COUNT[stIndex];

        size_t stLeak  = (stCount - pChunk->GetCount());
        if ((pChunk->IsNull() == false) && (pChunk->IsFull() == false))
        {
            stLeak -= pChunk->GetFree();
        }
        DEV_DUMP(TF("  Chunk(%p) Leak block count %d"), pChunk, stLeak);

        PByte pData = ((PByte)pChunk + (size_t)MM_CHUNK_HEAD);
        pData += (stCount - 1) * stAlloc;
        PMM_BLOCK pBlock = (PMM_BLOCK)pData;
        for (size_t i = 0; i < stCount; ++i)
        {
#ifdef __RUNTIME_DEBUG__
            if (pBlock->InUsed())
            {
                TRY_EXCEPTION
                    DEV_DUMP(TF("  %s->%s(%d), block at %p, size=%u"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK), pBlock, stAlloc);
                CATCH_EXCEPTION
                    DEV_DUMP(TF("  %p->%p(%d), block at %p, size=%u"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK), pBlock, stAlloc);
                END_EXCEPTION
            }
#else
            if (pBlock->InUsed())
            {
                DEV_DUMP(TF("  block at %p, size=%u"), pBlock, stAlloc);
            }
#endif
            pData  -= stAlloc;
            pBlock  = (PMM_BLOCK)pData;
        }
        return stLeak;
    }
    else
    {
        assert(stIndex == (size_t)MM_LIST_DIRECT);

        DEV_DUMP(TF("  Direct Chunk(%p) Leak block count 1"), pChunk);

        size_t stAlloc = pChunk->GetAlloc();
        PMM_BLOCK pBlock = (PMM_BLOCK)((PByte)pChunk + (size_t)MM_CHUNK_HEAD);
#ifdef __RUNTIME_DEBUG__
        if (pBlock->InUsed())
        {
            TRY_EXCEPTION
                DEV_DUMP(TF("  %s->%s(%d), block at %p, size=%u"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK), pBlock, stAlloc);
            CATCH_EXCEPTION
                DEV_DUMP(TF("  %p->%p(%d), block at %p, size=%u"), pBlock->pszFile, pBlock->pszFunc, (pBlock->stMagic & (size_t)MM_MAGIC_BLOCK_MASK), pBlock, stAlloc);
            END_EXCEPTION
        }
#else
        if (pBlock->InUsed())
        {
            DEV_DUMP(TF("  block at %p, size=%u"), pBlock, stAlloc);
        }
#endif
        return 1;
    }
}

void  CMemManager::ErrorDump(void)
{
    DEV_ERROR(TF("  Memory manager alloc out of memory, cur used size=%d.%dMB(max used size=%d.%dMB), cur cached size=%d.%dMB(max cache size=%d.%dMB)"),
              (Int)(m_MMStats.llUsedSize     >> CAPAP_20), (Int)((m_MMStats.llUsedSize     >> CAPAP_10) & CAPAP_MASK),
              (Int)(m_MMStats.llMaxUsedSize  >> CAPAP_20), (Int)((m_MMStats.llMaxUsedSize  >> CAPAP_10) & CAPAP_MASK),
              (Int)(m_MMStats.llCacheSize    >> CAPAP_20), (Int)((m_MMStats.llCacheSize    >> CAPAP_10) & CAPAP_MASK),
              (Int)(m_MMStats.llMaxCacheSize >> CAPAP_20), (Int)((m_MMStats.llMaxCacheSize >> CAPAP_10) & CAPAP_MASK));
    assert(0);
}

void  CMemManager::StatsReport(void)
{
    m_MMStats.Tick2Time(GPlatform.GetBaseTick());
    DEV_DUMP(TF("#---Memory manager Stats Report---"));
    DEV_DUMP(TF("| Cur Used  Size    : %d.%04d[MB]"), (Int)(m_MMStats.llUsedSize     >> CAPAP_20), (Int)((m_MMStats.llUsedSize     >> CAPAP_10) & CAPAP_MASK));
    DEV_DUMP(TF("| Max Used  Size    : %d.%04d[MB]"), (Int)(m_MMStats.llMaxUsedSize  >> CAPAP_20), (Int)((m_MMStats.llMaxUsedSize  >> CAPAP_10) & CAPAP_MASK));
    DEV_DUMP(TF("| Cur Cache Size    : %d.%04d[MB]"), (Int)(m_MMStats.llCacheSize    >> CAPAP_20), (Int)((m_MMStats.llCacheSize    >> CAPAP_10) & CAPAP_MASK));
    DEV_DUMP(TF("| Max Cache Size    : %d.%04d[MB]"), (Int)(m_MMStats.llMaxCacheSize >> CAPAP_20), (Int)((m_MMStats.llMaxCacheSize >> CAPAP_10) & CAPAP_MASK));
    DEV_DUMP(TF("| Total Used Time   : %d.%03d[S]"),  (Int)(m_MMStats.llUsedTick     / TIMET_S2MS), (Int)(m_MMStats.llUsedTick      %  TIMET_S2MS));
    DEV_DUMP(TF("| Alloc Used Time   : %d.%03d[S]"),  (Int)(m_MMStats.llAllocTick    / TIMET_S2MS), (Int)(m_MMStats.llAllocTick     %  TIMET_S2MS));
    DEV_DUMP(TF("| Realloc Used Time : %d.%03d[S]"),  (Int)(m_MMStats.llReallocTick  / TIMET_S2MS), (Int)(m_MMStats.llReallocTick   %  TIMET_S2MS));
    DEV_DUMP(TF("| Free Used Time    : %d.%03d[S]"),  (Int)(m_MMStats.llFreeTick     / TIMET_S2MS), (Int)(m_MMStats.llFreeTick      %  TIMET_S2MS));
}

///////////////////////////////////////////////////////////////////
// CMemManager
PINDEX CMemManager::CacheCreate(size_t stSize, size_t stGrow, size_t stMaxSize, size_t stType)
{
    MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_USED);
    CMemCache* pCache = MNEW CMemCache;
    if (pCache != nullptr)
    {
        if (pCache->Init(stSize, stGrow, stMaxSize, stType))
        {
            CSyncLockScope scope(m_CacheLock);
            return pCache->AddCache(m_pMemCache);
        }
        MDELETE pCache;
    }
    DEV_WARN(TF("#---Memory manager : create or init CMemCache failed!!!"));
    return nullptr;
}

bool CMemManager::CacheDestroy(PINDEX index)
{
    MM_STATS_SCOPE StatsScope(m_MMStats, MM_STATS_SCOPE::SCOPET_USED);
    CSyncLockScope scope(m_CacheLock);
    if (Check(index))
    {
        CMemCache* pCache = (CMemCache*)index;
        pCache->RemoveCache(m_pMemCache);
        pCache->Exit(m_MMStats);
        MDELETE pCache;
        return true;
    }
    return false;
}

size_t CMemManager::CacheAlign(PINDEX index)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Size();
    }
    return 0;
}

size_t CMemManager::CacheUsed(PINDEX index)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Used();
    }
    return 0;
}

PByte CMemManager::CacheAlloc(PINDEX index)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Alloc(m_MMStats);
    }
    return nullptr;
}

bool CMemManager::CacheFree(PINDEX index, PByte pData)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Free(pData, m_MMStats);
    }
    return false;
}

bool CMemManager::CacheCheck(PINDEX index, PByte pData)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Check(pData);
    }
    return false;
}

PByte CMemManager::CacheDump(PINDEX index, PByte pData, size_t& stSize)
{
    if (Check(index))
    {
        return ((CMemCache*)index)->Dump(pData, stSize);
    }
    return nullptr;
}

void CMemManager::CacheDump(PINDEX index)
{
    if (Check(index))
    {
        size_t stBlockSize = ((CMemCache*)index)->Size();
        size_t stBlockUsed = ((CMemCache*)index)->Used();
        size_t stChunkUsed = ((CMemCache*)index)->Cache();
        DEV_DUMP(TF("    Memory manager : MemCache[%p], Cache Size = %d, Used Size = %d, Block Size = %d"), index, stChunkUsed, stBlockUsed, stBlockSize);
    }
}

void CMemManager::CacheDump(void)
{
#ifdef __RUNTIME_DEBUG__
    size_t stCacheSize = 0;
    size_t stCacheUsed = 0;

    CMemCache* pPrev = nullptr;
    CMemCache* pCurr = m_pMemCache;
    while (pCurr != nullptr)
    {
        pCurr->PrevCache(pPrev);

        if (pCurr->Used() > 0)
        {
            DEV_DUMP(TF("#---Memory manager : Cache at %p, Cache Size = %d, Used Size = %d---"), pCurr, pCurr->Cache(), pCurr->Used());
            stCacheSize += pCurr->Cache();
            stCacheUsed += pCurr->Used();
        }

        pCurr->Exit(m_MMStats, true);
        MDELETE pCurr;

        pCurr = pPrev;
    }
    DEV_DUMP(TF("#---Memory manager dump : Cache All Size = %#X, Cache Used Size = %#X---"), stCacheSize, stCacheUsed);
#else
    CMemCache* pPrev = nullptr;
    CMemCache* pCurr = m_pMemCache;
    while (pCurr != nullptr)
    {
        pCurr->PrevCache(pPrev);

        pCurr->Exit(m_MMStats, true);
        MDELETE pCurr;

        pCurr = pPrev;
    }
#endif
    m_pMemCache = nullptr;
}

///////////////////////////////////////////////////////////////////
// MObject
#ifdef __RUNTIME_DEBUG__
    void* MObject::operator new(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine)
    {
        return GMemory.Alloc(stSize, szFile, szFunc, stLine, 0);
    }

    void* MObject::operator new[](size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine)
    {
        return GMemory.Alloc(stSize, szFile, szFunc, stLine, 0);
    }

    void  MObject::operator delete(void*, PCXStr, PCXStr, size_t)
    {
    }

    void  MObject::operator delete[](void*, PCXStr, PCXStr, size_t)
    {
    }
#else   // __RUNTIME_DEBUG__
    void* MObject::operator new(size_t stSize)
    {
        return GMemory.Alloc(stSize, 0);
    }

    void* MObject::operator new[](size_t stSize)
    {
        return GMemory.Alloc(stSize, 0);
    }
#endif  // __RUNTIME_DEBUG__

void MObject::operator delete(void* p, size_t stSize)
{
    GMemory.Free(p, stSize);
}

void MObject::operator delete[](void* p, size_t stSize)
{
    GMemory.Free(p, stSize);
}

#ifdef __RUNTIME_DEBUG__
    void* MObject::MAlloc(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType)
    {
        return GMemory.Alloc(stSize, szFile, szFunc, stLine, stType);
    }

    void* MObject::MReAlloc(void* pOld, size_t stNewSize, PCXStr szFile, PCXStr szFunc, size_t stLine)
    {
        return GMemory.ReAlloc(pOld, stNewSize, szFile, szFunc, stLine);
    }
#else   // __RUNTIME_DEBUG__
    void* MObject::MAlloc(size_t stSize, size_t stType)
    {
        return GMemory.Alloc(stSize, stType);
    }

    void* MObject::MReAlloc(void* pOld, size_t stNewSize)
    {
        return GMemory.ReAlloc(pOld, stNewSize);
    }
#endif  // __RUNTIME_DEBUG__

void  MObject::MFree(void* p, size_t stSize)
{
    GMemory.Free(p, stSize);
}

void  MObject::MDump(MM_DUMP& Dump)
{
    GMemory.Dump(Dump);
}

void  MObject::MDump(void* p)
{
    GMemory.Dump(p);
}

bool  MObject::MCheck(void* p)
{
    return GMemory.Check(p);
}
// cache : boundary to align pagesize
PINDEX MObject::MCCreate(size_t stSize, size_t stGrow, size_t stMaxSize, size_t stType)
{
    return GMemory.CacheCreate(stSize, stGrow, stMaxSize, stType);
}

bool   MObject::MCDestroy(PINDEX index)
{
    return GMemory.CacheDestroy(index);
}

size_t MObject::MCAlignSize(PINDEX index)
{
    return GMemory.CacheAlign(index);
}

size_t MObject::MCUsedSize(PINDEX index)
{
    return GMemory.CacheUsed(index);
}

PByte  MObject::MCAlloc(PINDEX index)
{
    return GMemory.CacheAlloc(index);
}

bool   MObject::MCFree(PINDEX index, PByte pData)
{
    return GMemory.CacheFree(index, pData);
}

bool   MObject::MCCheck(PINDEX index, PByte pData)
{
    return GMemory.CacheCheck(index, pData);
}

PByte MObject::MCDump(PINDEX index, PByte pData, size_t& stSize)
{
    return GMemory.CacheDump(index, pData, stSize);
}

void   MObject::MCDump(PINDEX index)
{
    return GMemory.CacheDump(index);
}
