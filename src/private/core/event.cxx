// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "core.hxx"
#include "event.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CEventQueueImp : CEventQueueThread
CEventQueueImp::CEventQueueThread::CEventQueueThread(CEventQueueImp& EventQueueRef)
: m_pEventQueue(&EventQueueRef)
{
}

CEventQueueImp::CEventQueueThread::~CEventQueueThread(void)
{
}

bool CEventQueueImp::CEventQueueThread::OnStart(void)
{
    if (m_pEventQueue->m_uStartEvent != 0)
    {
        m_pEventQueue->m_pHandler->OnHandle(m_pEventQueue->m_uStartEvent, (uintptr_t)GetId(), 0);
    }
    return true;
}

void CEventQueueImp::CEventQueueThread::OnStop(void)
{
    if (m_pEventQueue->m_uStopEvent != 0)
    {
        m_pEventQueue->m_pHandler->OnHandle(m_pEventQueue->m_uStopEvent, (uintptr_t)GetId(), 0);
    }
}

bool CEventQueueImp::CEventQueueThread::Run(void)
{
    UpdateWaitStatus();
    if (m_pEventQueue->m_hEventQueue.Wait())
    {
        UpdateWaitStatus(false);
        if (m_pEventQueue->m_QueueData.GetSize() > 0)
        {
            QUEUE_DATA qd = { 0 };
            {
                m_pEventQueue->m_EQLock.Signal();
                qd = m_pEventQueue->m_QueueData.GetHead();
                m_pEventQueue->m_QueueData.RemoveHeadDirect();
                m_pEventQueue->m_EQLock.Reset();
            }
            return m_pEventQueue->QueueHandle(qd);
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CEventQueueImp
CEventQueueImp::CEventQueueImp(CEventHandler& EventHandlerRef, size_t stCacheSize, UInt uThreads)
: m_pHandler(&EventHandlerRef)
, m_inxId(nullptr)
, m_inxCache(nullptr)
, m_stCacheSize(stCacheSize)
, m_uMaxCount(EVENT_COUNT_MAX)
, m_uThreads(uThreads)
, m_uStartEvent(0)
, m_uStopEvent(0)
{
}

CEventQueueImp::~CEventQueueImp(void)
{
    Exit();
    assert(m_QueueData.GetSize() == 0);
    assert(m_WorkThreads.GetSize() == 0);
    if (m_inxId != nullptr)
    {
        if (SEventManager::GetInstance() != nullptr)
        {
            SEventManager::GetInstance()->DestroyEventQueue(m_inxId);
        }
        m_inxId = nullptr;
    }
}

void CEventQueueImp::SetIndex(PINDEX index)
{
    m_inxId = index;
}

bool CEventQueueImp::Init(void)
{
    if (m_hEventQueue.IsValid() == false)
    {
        CSyncLockScope scope(m_EQLock);
        if (InitEventQueue() && InitCache())
        {
            HandlerAddRef();
            UInt uThreadCount = m_uThreads;
            m_uThreads = 0;
            SetThreads(uThreadCount);
            DEV_TRACE(TF("  Event queue[%p, %d] init okay"), this, m_uThreads);
            return true;
        }
    }
    DEV_TRACE(TF("  Event queue[%p] init already or failed"), this);
    return false;
}

void CEventQueueImp::Exit(void)
{
    if (m_hEventQueue.IsValid())
    {
        m_EQLock.Signal();
        ExitEventQueue();
        ExitCache();
        m_EQLock.Reset();
    }
    HandlerRelease();
}

bool CEventQueueImp::Add(uintptr_t utEvent, uintptr_t utData, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    if (utEvent > 0)
    {
        QUEUE_DATA qd = { 0 };
        qd.pSync = pSync;
        switch (nType)
        {
        case EVENT_TYPE_NONE:
            {
                qd.utEvent  = utEvent;
                qd.stType   = EVENT_TYPE_NONE;
                qd.utData   = utData;
                qd.ullParam = ullParam;
            }
            break;
        case EVENT_TYPE_REFCOUNT:
            {
                if (MCheck(reinterpret_cast<void*>(utData)))
                {
                    (reinterpret_cast<CEventBase*>(utData))->AddRef();

                    qd.utEvent  = utEvent;
                    qd.stType   = EVENT_TYPE_REFCOUNT;
                    qd.utData   = utData;
                    qd.ullParam = ullParam;
                }
            }
            break;
        case EVENT_TYPE_CACHE_REF:
        case EVENT_TYPE_CACHE_MANAGED:
            {
                if (MCCheck(reinterpret_cast<PINDEX>(ullParam), reinterpret_cast<PByte>(utData)))
                {
                    qd.utEvent  = utEvent;
                    qd.stType   = (size_t)nType;
                    qd.utData   = utData;
                    qd.ullParam = ullParam;
                }
            }
            break;
        case EVENT_TYPE_CACHE_QUEUE:
            {
                qd.stSize = (size_t)ullParam;
                if ((m_stCacheSize > 0) && (qd.stSize <= m_stCacheSize))
                {
                    PByte pCache = (PByte)MCAlloc(m_inxCache);
                    if (pCache != nullptr)
                    {
                        MM_SAFE::Cpy(pCache, m_stCacheSize, reinterpret_cast<PByte>(utData), qd.stSize);
                        qd.utEvent   = utEvent;
                        qd.stType    = EVENT_TYPE_CACHE_QUEUE;
                        qd.utData   = reinterpret_cast<uintptr_t>(pCache);
                        qd.ullParam = ullParam;
                    }
                }
            }
            break;
        default:
            {
            }
        }
        if (qd.utEvent > 0)
        {
            DEV_TRACE(TF("  Event queue[%p] add event okay, event=%lld(type=%d, utData=%p, ullParam=%llx)"), this, utEvent, nType, utData, ullParam);
            return QueueData(qd);
        }
        DEV_TRACE(TF("  Event queue[%p] add event failed, event=%lld(type=%d, utData=%p, ullParam=%llx)"), this, utEvent, nType, utData, ullParam);
    }
    return false;
}

bool CEventQueueImp::Add(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    if (utEvent > 0)
    {
        QUEUE_DATA qd = { 0 };
        qd.pSync = pSync;
        switch (nType)
        {
        case EVENT_TYPE_REFCOUNT:
            {
                if (MCheck(&EventRef))
                {
                    EventRef.AddRef();

                    qd.utEvent  = utEvent;
                    qd.stType   = EVENT_TYPE_REFCOUNT;
                    qd.utData   = reinterpret_cast<uintptr_t>(&EventRef);
                    qd.ullParam = ullParam;
                }
            }
            break;
        case EVENT_TYPE_CACHE_QUEUE:
            {
                qd.stSize = EventRef.Length();
                if ((m_stCacheSize > 0) && (qd.stSize <= m_stCacheSize))
                {
                    PByte pCache = (PByte)MCAlloc(m_inxCache);
                    if (pCache != nullptr)
                    {
                        CBufWriteStream BufWriteStream(qd.stSize, pCache);
                        EventRef.Serialize(BufWriteStream);

                        qd.utEvent  = utEvent;
                        qd.stType   = EVENT_TYPE_CACHE_QUEUE;
                        qd.utData   = reinterpret_cast<uintptr_t>(pCache);
                        qd.ullParam = ullParam;
                    }
                }
            }
            break;
        default:
            {
            }
        }
        if (qd.utEvent > 0)
        {
            DEV_TRACE(TF("  Event queue[%p] add event okay, event=%lld(type=%d, CEventBase's Length=%p, ullParam=%llx)"), this, utEvent, nType, qd.stSize, ullParam);
            return QueueData(qd);
        }
        DEV_TRACE(TF("  Event queue[%p] add event failed, event=%lld(type=%d, CEventBase's Length=%p, ullParam=%llx)"), this, utEvent, nType, qd.stSize, ullParam);
    }
    return false;
}

bool CEventQueueImp::Add(uintptr_t utEvent, CStream& Stream, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    if ((utEvent > 0) && (m_stCacheSize > 0) && Stream.IsRead())
    {
        QUEUE_DATA qd = { 0 };
        qd.pSync      = pSync;
        qd.stSize     = Stream.Rest();
        PByte pCache  = (PByte)MCAlloc(m_inxCache);
        if (pCache != nullptr)
        {
            Stream.Read((void*)pCache, m_stCacheSize);

            qd.utEvent   = utEvent;
            qd.stType    = EVENT_TYPE_CACHE_QUEUE;
            qd.utData   = reinterpret_cast<uintptr_t>(pCache);
            qd.ullParam = ullParam;
        }
        if (qd.utEvent > 0)
        {
            DEV_TRACE(TF("  Event queue[%p] add event okay, event=%lld(Stream's Size=%p, ullParam=%llx)"), this, utEvent, qd.stSize, ullParam);
            return QueueData(qd);
        }
        DEV_TRACE(TF("  Event queue[%p] add event failed, event=%lld(Stream's Size=%p, ullParam=%llx)"), this, utEvent, qd.stSize, ullParam);
    }
    return false;
}

bool CEventQueueImp::MaxCount(UInt uMax)
{
    m_uMaxCount = uMax;
    return (uMax > 0);
}

UInt CEventQueueImp::GetCount(void) const
{
    return (UInt)(m_QueueData.GetSize());
}

UInt CEventQueueImp::GetThreads(void) const
{
    return m_uThreads;
}

bool CEventQueueImp::SetThreads(UInt uThreadCount)
{
    if (m_hEventQueue.IsValid() == false)
    {
        return false;
    }
    uThreadCount = GPlatform.CheckThread(uThreadCount, EVENT_THREAD_MAX);

    if (uThreadCount > m_uThreads)
    {
        for (UInt i = m_uThreads; i < uThreadCount; ++i)
        {
            CEventQueueThread* pThread = MNEW CEventQueueThread(*this);
            if (pThread != nullptr)
            {
                pThread->Start();
                ++m_uThreads;
                m_WorkThreads.AddTail(pThread);
            }
        }
    }
    else if (uThreadCount < m_uThreads)
    {
        for (UInt i = uThreadCount; i < m_uThreads; ++i)
        {
            m_hEventQueue.Signal();
        }
        PINDEX index = m_WorkThreads.GetHeadIndex();
        while (index != nullptr)
        {
            CEventQueueThread* pThread = m_WorkThreads.GetNext(index);
            if (pThread->IsRunning() == false)
            {
                m_WorkThreads.Remove(pThread);
                MDELETE pThread;
                --m_uThreads;
                if (uThreadCount >= m_uThreads)
                {
                    break;
                }
            }
        }
    }
    DEV_DEBUG(TF("  Event queue[%p] set work threads=%d"), this, m_uThreads);
    return true;
}

bool CEventQueueImp::SetThreadEvent(UInt uStart, UInt uStop)
{
    m_uStartEvent = uStart;
    m_uStopEvent  = uStop;
    DEV_DEBUG(TF("  Event queue[%p] set threads start event=%d, stop event=%d"), this, uStart, uStop);
    return true;
}

bool CEventQueueImp::CheckThreads(UInt uTimeout)
{
    if (m_hEventQueue.IsValid() == false)
    {
        return false;
    }
    for (PINDEX index = m_WorkThreads.GetHeadIndex(); index != nullptr;)
    {
        CEventQueueThread* pThread = m_WorkThreads.GetNext(index);
        if (pThread->IsRunning())
        {
            if (pThread->CheckStatus(uTimeout))
            {
                return true; // work thread is timeout
            }
        }
        else
        {
            return true; // work thread is stoped or except terminated
        }
    }
    return false;
}

bool CEventQueueImp::QueueHandle(QUEUE_DATA& qd)
{
    switch (qd.stType)
    {
    case EVENT_TYPE_CACHE_QUEUE:
        {
            CBufReadStream BufReadStream(qd.stSize, reinterpret_cast<PByte>(qd.utData));
            m_pHandler->OnHandle(qd.utEvent, BufReadStream, qd.ullParam);
            MCFree(m_inxCache, reinterpret_cast<PByte>(qd.utData));
        }
        break;
    case EVENT_TYPE_CACHE_MANAGED:
        {
            CEventBase* pEventBase = reinterpret_cast<CEventBase*>(qd.utData);
            m_pHandler->OnHandle(qd.utEvent, *pEventBase, qd.ullParam);
            MCFree(reinterpret_cast<PINDEX>(qd.ullParam), reinterpret_cast<PByte>(qd.utData));
        }
        break;
    case EVENT_TYPE_REFCOUNT:
        {
            CEventBase* pEventBase = reinterpret_cast<CEventBase*>(qd.utData);
            m_pHandler->OnHandle(qd.utEvent, *pEventBase, qd.ullParam);
            ReleaseEventBase(pEventBase);
        }
        break;
    case EVENT_TYPE_CACHE_REF:
    case EVENT_TYPE_NONE:
    default:
        {
            m_pHandler->OnHandle(qd.utEvent, qd.utData, qd.ullParam);
        }
    }
    if (qd.pSync != nullptr)
    {
        SignalSyncBase(qd.pSync);
    }
    return true;
}

bool CEventQueueImp::InitEventQueue(void)
{
    if (m_hEventQueue.IsValid() == false)
    {
        return m_hEventQueue.Open();
    }
    return true;
}

bool CEventQueueImp::InitCache(void)
{
    if (m_stCacheSize > 0)
    {
        size_t stMaxCache = (m_uMaxCount >> EVENT_COUNT_CACHE) * m_stCacheSize;
        m_inxCache = MCCreate(m_stCacheSize, 0, stMaxCache);
        if (m_inxCache == nullptr)
        {
            DEV_TRACE(TF("  Event queue[%p] init failed by init cache failed"), this);
            return false;
        }
    }
    return true;
}

void CEventQueueImp::ExitEventQueue(void)
{
    if (m_hEventQueue.IsValid())
    {
        for (Int i = 0; i < m_WorkThreads.GetSize(); ++i)
        {
            m_hEventQueue.Signal();
        }
        PINDEX index = m_WorkThreads.GetHeadIndex();
        while (index != nullptr)
        {
            CEventQueueThread* pThread = m_WorkThreads.GetNext(index);
            if (pThread->IsRunning())
            {
                if (pThread->Stop((UInt)CEventManager::THREAD_WAIT_TIME) == false)
                {
                    pThread->Kill();
                }
            }
            MDELETE pThread;
        }
        m_WorkThreads.RemoveAll();
        m_hEventQueue.Close();
        DEV_TRACE(TF("  Event queue[%p] stop queue thread and close event object"), this);
    }
}

void CEventQueueImp::ExitCache(void)
{
    PINDEX index = m_QueueData.GetHeadIndex();
    while (index != nullptr)
    {
        QUEUE_DATA qd = m_QueueData.GetNext(index);
        switch (qd.stType)
        {
        case EVENT_TYPE_CACHE_QUEUE:
            {
                DEV_TRACE(TF("  Event queue[%p] Unhandled event:%lld[queue managed cache[%llx] %p, ullParam=%llx]"), this, qd.utEvent, m_inxCache, qd.utData, qd.ullParam);
                MCFree(m_inxCache, reinterpret_cast<PByte>(qd.utData));
            }
            break;
        case EVENT_TYPE_CACHE_MANAGED:
            {
                DEV_TRACE(TF("  Event queue[%p] Unhandled event:%lld[managed cache[%llx] %p]"), this, qd.utEvent, m_inxCache, qd.ullParam, qd.utData);
                MCFree(reinterpret_cast<PINDEX>(qd.ullParam), reinterpret_cast<PByte>(qd.utData));
            }
            break;
        case EVENT_TYPE_REFCOUNT:
            {
                DEV_TRACE(TF("  Event queue[%p] Unhandled event:%lld[CEventBase at %p, ullParam=%llx]"), this, qd.utEvent, qd.utData, qd.ullParam);
                ReleaseEventBase(reinterpret_cast<CEventBase*>(qd.utData));
            }
            break;
        case EVENT_TYPE_CACHE_REF:
            {
                DEV_TRACE(TF("  Event queue[%p] Unhandled event:%lld[user cache[%llx] %p]"), this, qd.utEvent, qd.ullParam, qd.utData);
            }
            break;
        default:
            {
                DEV_TRACE(TF("  Event queue[%p] Unhandled event:%lld"), this, qd.utEvent);
            }
        }
    }
    m_QueueData.RemoveAll();
    if (m_inxCache != nullptr)
    {
        MCDestroy(m_inxCache);
        m_inxCache = nullptr;
    }
    DEV_DEBUG(TF("  Event queue[%p] exit okay"), this);
}

bool CEventQueueImp::QueueData(QUEUE_DATA& qd)
{
    PINDEX index = nullptr;
    {
        CSyncLockScope scope(m_EQLock);
        if (m_uMaxCount > (UInt)m_QueueData.GetSize())
        {
            index = m_QueueData.AddTail(qd);
        }
        else
        {
            DEV_TRACE(TF("  Event queue[%p] add event %lld failed by queue is full"), this, qd.utEvent);
        }
    }
    if (index != nullptr)
    {
        DEV_TRACE(TF("  Event queue[%p] add event %lld"), this, qd.utEvent);
        return m_hEventQueue.Signal(m_QueueData.GetSize());
    }
    return false;
}

void CEventQueueImp::ReleaseEventBase(CEventBase* pEventBase)
{
    TRY_EXCEPTION
        pEventBase->Release();
    CATCH_EXCEPTION
    END_EXCEPTION
}

void CEventQueueImp::SignalSyncBase(CSyncBase* pSync)
{
    TRY_EXCEPTION
        pSync->Signal();
    CATCH_EXCEPTION
    END_EXCEPTION
}

void CEventQueueImp::HandlerAddRef(void)
{
    m_pHandler->AddRef();
}

void CEventQueueImp::HandlerRelease(void)
{
    TRY_EXCEPTION
        if (m_pHandler != nullptr)
        {
            m_pHandler->Release();
            m_pHandler = nullptr;
        }
    CATCH_EXCEPTION
    END_EXCEPTION
}

///////////////////////////////////////////////////////////////////
// CEventManager
CEventManager::CEventManager(void)
{
}

CEventManager::~CEventManager(void)
{
    assert(m_EventQueue.GetSize() == 0);
    assert(m_TickCache.GetSize() == 0);
    assert(m_EventTick.GetSize() == 0);
}

CEventManager::CEventManager(const CEventManager&)
{
}

CEventManager& CEventManager::operator=(const CEventManager&)
{
    return (*this);
}

UInt CEventManager::Init(void)
{
    m_EventTick.SetHash(CContainerTraits::HASHD_SIZE);
    DEV_DUMP(TF("*-Event-manager init okay-"));
    return (UInt)RET_OKAY;
}

void CEventManager::Exit(void)
{
    DEV_DUMP(TF("*-Event-manager dump, Un-release Event-queue : %d-"), m_EventQueue.GetSize());
    PINDEX index = m_EventQueue.GetHeadIndex();
    while (index != nullptr)
    {
        CEventQueueImp* pEQ = m_EventQueue.GetNext(index);
        pEQ->SetIndex(nullptr);
        pEQ->Exit();
        MDELETE pEQ;
        DEV_DUMP(TF("| Event-manager : Un-release Event-queue %p"), pEQ);
    }
    m_EventQueue.RemoveAll();

    DEV_DUMP(TF("*-Event-manager dump, Un-release Event-tick : %d-"), m_EventTick.GetSize());
    index = m_EventTick.GetFirstIndex();
    while (index != nullptr)
    {
        PAIR_TICK* pPair = m_EventTick.GetNext(index);
        DEV_DUMP(TF("| Event-manager : Un-release Event-tick %p id=%d"), pPair->m_V, pPair->m_K);
        pPair->m_V->Exit(true);
        MDELETE (pPair->m_V);
    }
    m_EventTick.RemoveAll();

    index = m_TickCache.GetHeadIndex();
    while (index != nullptr)
    {
        CTickEventImp* pTick = m_TickCache.GetNext(index);
        DEV_DUMP(TF("| Event-manager : Release Cached Event-tick %p"), pTick);
        pTick->Exit();
        MDELETE pTick;
    }
    m_TickCache.RemoveAll();
    DEV_DUMP(TF("*-Event-manager exit okay-"));
}

bool CEventManager::CreateEventQueue(CEventQueuePtr& EventQueuePtrRef, CEventHandler& EventHandlerRef, size_t stCacheSize, UInt uThreads)
{
    CSyncLockScope scope(m_EMLock);
    CEventQueueImp* pEventQueue = MNEW CEventQueueImp(EventHandlerRef, stCacheSize, uThreads);
    if (pEventQueue != nullptr)
    {
        PINDEX index = m_EventQueue.AddTail(pEventQueue);
        pEventQueue->SetIndex(index);
        EventQueuePtrRef = pEventQueue;
        DEV_DEBUG(TF("  Event-manager create Event queue(%p)"), pEventQueue);
        return true;
    }
    return false;
}

bool CEventManager::DestroyEventQueue(PINDEX index)
{
    CSyncLockScope scope(m_EMLock);
    DEV_DEBUG(TF("  Event-manager destroy Event queue(%p)"), m_EventQueue.GetAt(index));
    return m_EventQueue.RemoveAt(index);
}

bool CEventManager::CreateTickEvent(uintptr_t utTick, UInt uInterval, CEventHandler& EventHandlerRef, UInt uCount)
{
    if (utTick > 0)
    {
        CSyncLockScope scope(m_EMLock);
        const PAIR_TICK* pPair = m_EventTick.Find(utTick);
        if (pPair != nullptr)
        {
            DEV_DEBUG(TF("  Tick-event tick-event id[%lld] exist!"), utTick);
            return false;
        }

        CTickEventImp* pEventTick = nullptr;
        if (m_TickCache.GetSize() > 0)
        {
            pEventTick = m_TickCache.RemoveHead();
            DEV_DEBUG(TF("  Event-manager get Event Tick(%p) from cache list for id=%lld"), pEventTick, utTick);
        }
        else
        {
            pEventTick = MNEW CTickEventImp;
            DEV_DEBUG(TF("  Event-manager create new Event Tick(%p) for id=%lld"), pEventTick, utTick);
        }
        if (pEventTick != nullptr)
        {
            PINDEX index = m_EventTick.Add(utTick, pEventTick);
            if (pEventTick->Init(EventHandlerRef, index, utTick, uCount, uInterval))
            {
                DEV_DEBUG(TF("  Event-manager init Event Tick(%p) for id=%lld"), pEventTick, utTick);
                return true;
            }
            else
            {
                DEV_DEBUG(TF("  Event-manager init Event Tick(%p) for id=%lld fail"), pEventTick, utTick);
                m_EventTick.RemoveAt(index);
                MDELETE pEventTick;
            }
        }
    }
    return false;
}

bool CEventManager::DestroyTickEvent(uintptr_t utTick)
{
    if (utTick > 0)
    {
        CSyncLockScope scope(m_EMLock);
        PAIR_TICK* pPair = m_EventTick.Find(utTick);
        if (pPair != nullptr)
        {
            if (pPair->m_V->Exit())
            {
                DEV_DEBUG(TF("  Event-manager uninit Event Tick(%p) for id=%lld"), pPair->m_V, utTick);
                if (m_TickCache.GetSize() < TICK_CACHE_MAX)
                {
                    m_TickCache.AddTail(pPair->m_V);
                }
                else
                {
                    MDELETE pPair->m_V;
                }
                return m_EventTick.RemoveAt(reinterpret_cast<PINDEX>(pPair));
            }
            else
            {
                DEV_DEBUG(TF("  Event-manager uninit Event Tick(%p) for id=%lld failed"), pPair->m_V, utTick);
            }
            return true;
        }
    }
    return false;
}

bool CEventManager::CheckTickEvent(uintptr_t utTick, Int nMultInterval)
{
    if (utTick > 0)
    {
        CSyncLockScope scope(m_EMLock);
        PAIR_TICK* pPair = m_EventTick.Find(utTick);
        if (pPair != nullptr)
        {
            return pPair->m_V->Check(nMultInterval);
        }
        return true;
    }
    return false;
}

bool CEventManager::CacheTickEvent(PINDEX index)
{
    CSyncLockScope scope(m_EMLock);
    if (m_EventTick.Check(index))
    {
        DEV_DEBUG(TF("  Event-manager cache Event Tick id=%lld"), m_EventTick[index]);
        if (m_TickCache.GetSize() < TICK_CACHE_MAX)
        {
            m_TickCache.AddTail(m_EventTick[index]);
        }
        else
        {
            MDELETE m_EventTick[index];
        }
        return m_EventTick.RemoveAt(index);
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CEventQueue
CEventQueue::CEventQueue(void)
{
}

CEventQueue::~CEventQueue(void)
{
}

CEventQueue::CEventQueue(const CEventQueue& aSrc)
: CTRefCount<CEventQueue>(aSrc)
{
}

CEventQueue& CEventQueue::operator=(const CEventQueue& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CEventQueue>::operator=(aSrc);
    }
    return (*this);
}

bool CEventQueue::EventQueue(CEventQueuePtr& EventQueuePtrRef, CEventHandler& EventHandlerRef, size_t stCacheSize, UInt uThreads)
{
    if (SEventManager::GetInstance() == nullptr)
    {
        return false;
    }
    return SEventManager::GetInstance()->CreateEventQueue(EventQueuePtrRef, EventHandlerRef, stCacheSize, uThreads);
}

bool CEventQueue::CreateTickEvent(uintptr_t utTick, UInt uInterval, CEventHandler& EventHandlerRef, UInt uCount)
{
    if (SEventManager::GetInstance() == nullptr)
    {
        return false;
    }
    return SEventManager::GetInstance()->CreateTickEvent(utTick, uInterval, EventHandlerRef, uCount);
}

bool CEventQueue::DestroyTickEvent(uintptr_t utTick)
{
    if (SEventManager::GetInstance() == nullptr)
    {
        return false;
    }
    return SEventManager::GetInstance()->DestroyTickEvent(utTick);
}

bool CEventQueue::CheckTickEvent(uintptr_t utTick, Int nMultInterval)
{
    if (SEventManager::GetInstance() == nullptr)
    {
        return false;
    }
    return SEventManager::GetInstance()->CheckTickEvent(utTick, nMultInterval);
}
