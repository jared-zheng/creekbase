// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MEM_TRAITS_HXX__
#define __MEM_TRAITS_HXX__

#pragma once

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CMemTraits
class CMemTraits
{
public:
    enum MM_LIST_CONST
    {
        MM_LIST_DIRECT         = 0x0000002A, // 42
        MM_LIST_COUNT          = 0x0000002B, // 43
    };

    enum MM_SELECT_CONST
    {
        MM_SELECT_REGION_STEP  = 0x00000003, // 3

        MM_SELECT_REGION_LMIN  = 0x00000007, // 7
        MM_SELECT_REGION_LMAX  = 0x0000000E, // 14
        MM_SELECT_REGION_MID   = 0x00000015, // 21
        MM_SELECT_REGION_HMIN  = 0x0000001C, // 28
        MM_SELECT_REGION_HMAX  = 0x00000023, // 35
    };

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    typedef UInt MSIZE_T;

    enum MM_CHUNK_CONST
    {
        MM_CHUNK_HEAD          = 0x00000020, // chunk struct size
#ifdef __RUNTIME_DEBUG__
        MM_CHUNK_TOBLOCK       = 0x00000038, // MM_CHUNK_HEAD + MM_BLOCK_HEAD
        MM_CHUNK_TODATA        = 0x00000040, // MM_CHUNK_HEAD + MM_BLOCK_TODATA
        MM_CHUNK_WITHBLOCK     = 0x00000048, // MM_CHUNK_HEAD + MM_BLOCK_WITHFIX
#else
        MM_CHUNK_TOBLOCK       = 0x00000028, // MM_CHUNK_HEAD + MM_BLOCK_HEAD
        MM_CHUNK_TODATA        = 0x00000028, // MM_CHUNK_HEAD + MM_BLOCK_TODATA
        MM_CHUNK_WITHBLOCK     = 0x00000028, // MM_CHUNK_HEAD + MM_BLOCK_WITHFIX
#endif
        MM_CHUNK_PAGESIZE      = 0x00001000, // 4K
        MM_CHUNK_OFFSET        = 0x0000FFFF, //
        MM_CHUNK_DATASIZE      = 0x0000FFE0, // MM_CHUNK_MAXSIZE - MM_CHUNK_HEAD
        MM_CHUNK_MAXSIZE       = 0x00010000, // 64K
        MM_CHUNK_MAXPAGESIZE   = 0x00400000, // 4M
    };

    enum MM_BLOCK_CONST
    {
#ifdef __RUNTIME_DEBUG__
        MM_BLOCK_XXXFIX        = 0x00000008, // prefix & suffix size
        MM_BLOCK_HEAD          = 0x00000018, // block struct size
        MM_BLOCK_TODATA        = 0x00000020, // block struct size + prefix
        MM_BLOCK_WITHFIX       = 0x00000028, // block + prefix + suffix
        MM_BLOCK_DATASIZE      = 0x00007FC8, // MM_BLOCK_MAXSIZE - MM_BLOCK_WITHFIX
        MM_BLOCK_DATALIMIT     = 0x00007FC9, // MM_BLOCK_DATASIZE + 1
#else
        MM_BLOCK_XXXFIX        = 0x00000000, // no prefix & suffix size
        MM_BLOCK_HEAD          = 0x00000008, // block struct size(stMagic only)
        MM_BLOCK_TODATA        = 0x00000008, // block struct size + no prefix
        MM_BLOCK_WITHFIX       = 0x00000008, // block + no prefix & suffix
        MM_BLOCK_DATASIZE      = 0x00007FE8, // MM_BLOCK_MAXSIZE - MM_BLOCK_WITHFIX
        MM_BLOCK_DATALIMIT     = 0x00007FE9, // MM_BLOCK_DATASIZE + 1
#endif
        MM_BLOCK_MAXSIZE       = 0x00007FF0, // MM_CHUNK_DATASIZE / 2
    };

#ifndef __MODERN_CXX_NOT_SUPPORTED
    enum MM_MAGIC_CONST : ULLong
    {
        MM_MAGIC_CHUNK_FULL    = 0x46554C4C00000000, // FULL-ASCII
        MM_MAGIC_CHUNK_DIRECT  = 0x4452435400000000, // DRCT-ASCII
#ifdef __RUNTIME_DEBUG__
        MM_MAGIC_BLOCK_OFFSET  = 0x0000000000000020,
        MM_MAGIC_BLOCK_MASK    = 0x00000000FFFFFFFF,
#else
        MM_MAGIC_BLOCK_OFFSET  = 0x00000000000000FF,
        MM_MAGIC_BLOCK_MASK    = 0xFFFFFFFFFFFFFF00,
#endif
        MM_MAGIC_BLOCK_FREE    = 0x424C434BF4EE0000, // BLCK-ASCII+
        MM_MAGIC_BLOCK_INUSED  = 0x424C434B5553ED00, // BLCK-USED-ASCII
        MM_MAGIC_BLOCK_PREFIX  = 0x004990D167CF5E86, // UNICODE - NAME
        MM_MAGIC_BLOCK_SUFFIX  = 0x5E8667CF90D10049, // UNICODE - NAME
        MM_MAGIC_BLOCK_MAX     = 0x0FFFFFFFFFFFFFFF,
        MM_MAGIC_BLOCK_TYPE    = 0x1000000000000000,
    };
#else
    static const ULLong MM_MAGIC_CHUNK_FULL   = 0x46554C4C00000000ULL;
    static const ULLong MM_MAGIC_CHUNK_DIRECT = 0x4452435400000000ULL;
#ifdef __RUNTIME_DEBUG__
    static const ULLong MM_MAGIC_BLOCK_OFFSET = 0x0000000000000020ULL;
    static const ULLong MM_MAGIC_BLOCK_MASK   = 0x00000000FFFFFFFFULL;
#else
    static const ULLong MM_MAGIC_BLOCK_OFFSET = 0x00000000000000FFULL;
    static const ULLong MM_MAGIC_BLOCK_MASK   = 0xFFFFFFFFFFFFFF00ULL;
#endif
    static const ULLong MM_MAGIC_BLOCK_FREE   = 0x424C434BF4EE0000ULL;
    static const ULLong MM_MAGIC_BLOCK_INUSED = 0x424C434B5553ED00ULL;
    static const ULLong MM_MAGIC_BLOCK_PREFIX = 0x004990D167CF5E86ULL;
    static const ULLong MM_MAGIC_BLOCK_SUFFIX = 0x5E8667CF90D10049ULL;
    static const ULLong MM_MAGIC_BLOCK_MAX    = 0x0FFFFFFFFFFFFFFFULL;
    static const ULLong MM_MAGIC_BLOCK_TYPE   = 0x1000000000000000ULL;
#endif

#else  // (__ARCH_TARGET__ == ARCH_TARGET_64)

    typedef UShort MSIZE_T;

    enum MM_CHUNK_CONST
    {
        MM_CHUNK_HEAD          = 0x00000010, // chunk struct size
#ifdef __RUNTIME_DEBUG__
        MM_CHUNK_TOBLOCK       = 0x0000001C, // MM_CHUNK_HEAD + MM_BLOCK_HEAD
        MM_CHUNK_TODATA        = 0x00000020, // MM_CHUNK_HEAD + MM_BLOCK_TODATA
        MM_CHUNK_WITHBLOCK     = 0x00000024, // MM_CHUNK_HEAD + MM_BLOCK_WITHFIX
#else
        MM_CHUNK_TOBLOCK       = 0x00000014, // MM_CHUNK_HEAD + MM_BLOCK_HEAD
        MM_CHUNK_TODATA        = 0x00000014, // MM_CHUNK_HEAD + MM_BLOCK_TODATA
        MM_CHUNK_WITHBLOCK     = 0x00000014, // MM_CHUNK_HEAD + MM_BLOCK_WITHFIX
#endif
        MM_CHUNK_PAGESIZE      = 0x00001000, // 4K
        MM_CHUNK_OFFSET        = 0x0000FFFF, //
        MM_CHUNK_DATASIZE      = 0x0000FFF0, // MM_CHUNK_MAXSIZE - MM_CHUNK_HEAD
        MM_CHUNK_MAXSIZE       = 0x00010000, // 64K
        MM_CHUNK_MAXPAGESIZE   = 0x00400000, // 4M
    };

    enum MM_BLOCK_CONST
    {
#ifdef __RUNTIME_DEBUG__
        MM_BLOCK_XXXFIX        = 0x00000004, // prefix & suffix size
        MM_BLOCK_HEAD          = 0x0000000C, // block struct size
        MM_BLOCK_TODATA        = 0x00000010, // block struct size + prefix
        MM_BLOCK_WITHFIX       = 0x00000014, // block + prefix + suffix
        MM_BLOCK_DATASIZE      = 0x00007FE4, // MM_BLOCK_MAXSIZE - MM_BLOCK_WITHFIX
        MM_BLOCK_DATALIMIT     = 0x00007FE5, // MM_BLOCK_DATASIZE + 1
#else
        MM_BLOCK_XXXFIX        = 0x00000000, // no prefix & suffix size
        MM_BLOCK_HEAD          = 0x00000004, // block struct size(stMagic only)
        MM_BLOCK_TODATA        = 0x00000004, // block struct size + no prefix
        MM_BLOCK_WITHFIX       = 0x00000004, // block + no prefix & suffix
        MM_BLOCK_DATASIZE      = 0x00007FF4, // MM_BLOCK_MAXSIZE - MM_BLOCK_WITHFIX
        MM_BLOCK_DATALIMIT     = 0x00007FF5, // MM_BLOCK_DATASIZE + 1
#endif
        MM_BLOCK_MAXSIZE       = 0x00007FF8, // MM_CHUNK_DATASIZE / 2
    };

    enum MM_MAGIC_CONST
    {
        MM_MAGIC_CHUNK_FULL    = 0xBEEF0000, //
        MM_MAGIC_CHUNK_DIRECT  = 0xDEAD0000, //
#ifdef __RUNTIME_DEBUG__
        MM_MAGIC_BLOCK_OFFSET  = 0x00000010,
        MM_MAGIC_BLOCK_MASK    = 0x0000FFFF,
#else
        MM_MAGIC_BLOCK_OFFSET  = 0x000000FF,
        MM_MAGIC_BLOCK_MASK    = 0xFFFFFF00,
#endif
        MM_MAGIC_BLOCK_FREE    = 0xF4EE0000, //
        MM_MAGIC_BLOCK_INUSED  = 0x5553ED00, // USED - ASCII
        MM_MAGIC_BLOCK_PREFIX  = 0x67CF5E86, // UNICODE - NAME
        MM_MAGIC_BLOCK_SUFFIX  = 0x5E8667CF, // UNICODE - NAME
        MM_MAGIC_BLOCK_MAX     = 0x0FFFFFFF,
        MM_MAGIC_BLOCK_TYPE    = 0x10000000,
    };

#endif  // (__ARCH_TARGET__ == ARCH_TARGET_64)

    typedef struct tagMM_X
    {
        MSIZE_T LowValue;
        MSIZE_T HighValue;
    }MM_X;

    union MM_MAGIC
    {
        MM_X   X;
        size_t ALLValue;
    };

    // block data alloc from chunk bottom --> top
    typedef struct tagMM_BLOCK
    {
    public:
        tagMM_BLOCK(void);
        ~tagMM_BLOCK(void);
#ifdef __RUNTIME_DEBUG__
        void*    Init(size_t stOffset, size_t stAlloc, PCXStr szFile, PCXStr szFunc, size_t stLine);
        void     Exit(size_t stAlloc);
#else
        void*    Init(size_t stOffset);
        void     Exit(void);
#endif
        size_t   Offset(void) const;
        bool     InUsed(void) const;
        bool     Check(size_t stAlloc) const;
    public:
        // USED : [Debug]alloc func line(low), offset(high); [Release]MM_MAGIC_BLOCK_INUSED(high), offset(low)
        // FREE : next free block ptr
        size_t   stMagic;
#ifdef __RUNTIME_DEBUG__
        PCXStr   pszFunc;
        PCXStr   pszFile;
        // PREFIX
        // Data
        // SUFFIX
#endif
    }MM_BLOCK, *PMM_BLOCK;

    typedef struct tagMM_CHUNK
    {
    public:
        tagMM_CHUNK(void);
        ~tagMM_CHUNK(void);

        void Init(size_t stData, size_t stIndex = (size_t)MM_LIST_DIRECT);
        void Exit(void);

        size_t AllocBlock(PMM_BLOCK& pAlloc, size_t stAlloc);
#ifdef __RUNTIME_DEBUG__
        size_t FreeBlock(PMM_BLOCK pFree, size_t stAlloc);
#else
        size_t FreeBlock(PMM_BLOCK pFree);
#endif

        size_t GetIndex(void) const;
        size_t GetCount(void) const;
        size_t GetAlloc(void) const; // direct alloc
        size_t GetFree(void) const;
        size_t GetData(void) const;

        bool   IsAllAlloc(void) const;
        bool   IsDirect(void) const;
        bool   IsFull(void) const;
        bool   IsNull(void) const;

        void   SetFull(void);
        void   CleanFull(void);

        tagMM_CHUNK*   GetPrev(void);
        tagMM_CHUNK*   GetNext(void);
        tagMM_BLOCK*   GetBlock(tagMM_BLOCK* pBlock = nullptr);
    public:
        tagMM_CHUNK*   pPrev;
        tagMM_CHUNK*   pNext;
        // Direct Alloc : [ALLValue]alloc size, exclude MM_CHUNK_HEAD
        // Block  Alloc : [X.HighValue]alloc size array index; [X.LowValue]unused block count
        MM_MAGIC       mChunk;
        // Direct Alloc : [ALLValue]MM_MAGIC_CHUNK_DIRECT
        // Block  Alloc : [ALLValue]0---no free block; [ALLValue]MM_MAGIC_CHUNK_FULL
        //                [X.HighValue]ptr offset of free block; [X.LowValue]free block count
        MM_MAGIC       mBlock;
    }MM_CHUNK, *PMM_CHUNK;

    typedef struct tagMM_CHUNK_LIST
    {
    public:
        tagMM_CHUNK_LIST(void);
        ~tagMM_CHUNK_LIST(void);

        void IncreChunk(PMM_CHUNK pChunk);
        void DecreChunk(PMM_CHUNK pChunk);

        void Full2Chunk(PMM_CHUNK pChunk);
        void Chunk2Full(void);

        PMM_CHUNK GetHead(void);
        PMM_CHUNK GetFull(void);
    public:
        PMM_CHUNK          pHead;
        PMM_CHUNK          pFull;
    }MM_CHUNK_LIST, *PMM_CHUNK_LIST;

    static const size_t MM_ALIGN_SIZE[MM_LIST_COUNT];
    static const size_t MM_SIZE_COUNT[MM_LIST_COUNT];
public:
    static size_t SELECT(size_t stSize, size_t& stAlloc);
    static size_t REGION(size_t stSize, size_t stIndex, size_t& stAlloc);
protected:
    CMemTraits(void);
    ~CMemTraits(void);
private:
    CMemTraits(const CMemTraits&);
    CMemTraits& operator=(const CMemTraits&);
};
///////////////////////////////////////////////////////////////////
#include "memtraits.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __MEM_TRAITS_HXX__
