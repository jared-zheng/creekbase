// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_TRAITS_HXX__
#define __NETWORK_TRAITS_HXX__

#pragma once

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
#pragma push_macro("new")
#undef new
#pragma push_macro("delete")
#undef delete

///////////////////////////////////////////////////////////////////
// CTNETBufferTraits : alloc from cache
template <typename T>
class CTNETBufferTraits ABSTRACT
{
public:
    enum BUFFER_STYLE
    {
        BUFFERS_NONE        = 0x00000000,
        BUFFERS_SEND        = 0x00000001,
        BUFFERS_RECV        = 0x00000002,
        BUFFERS_ACCEPT      = 0x00000004,
        BUFFERS_CONNECT     = 0x00000008,
        BUFFERS_BROADCAST   = 0x00000010,
        BUFFERS_JUMBO       = 0x00000020,
        BUFFERS_RIO         = 0x00000040,
        BUFFERS_CLOSE       = 0x40000000,
    };
public:
    static void* operator new(size_t, PByte p);
    static void* operator new[](size_t, PByte p);
    static void  operator delete(void* p, PByte);
    static void  operator delete[](void* p, PByte);
    static void  operator delete(void* p, size_t);
    static void  operator delete[](void* p, size_t);
};

template <typename T>
INLINE void* CTNETBufferTraits<T>::operator new(size_t, PByte p)
{
    return p;
}

template <typename T>
INLINE void* CTNETBufferTraits<T>::operator new[](size_t, PByte p)
{
    return p;
}

template <typename T>
INLINE void CTNETBufferTraits<T>::operator delete(void*, PByte)
{
}

template <typename T>
INLINE void CTNETBufferTraits<T>::operator delete[](void*, PByte)
{
}

template <typename T>
INLINE void CTNETBufferTraits<T>::operator delete(void*, size_t)
{
}

template <typename T>
INLINE void CTNETBufferTraits<T>::operator delete[](void*, size_t)
{
}

///////////////////////////////////////////////////////////////////
// CTNETSocketTraits :  alloc from cache
template <typename T>
class CTNETSocketTraits ABSTRACT
{
public:
    enum SOCKET_TRAITS
    {
        SOCKETT_NONE,
        SOCKETT_PACKET,
        SOCKETT_RAW,
        SOCKETT_UDP,
        SOCKETT_TCP,
        SOCKETT_TCP_LISTEN,
        SOCKETT_TCP_CONNECT,
        SOCKETT_TCP_ACCEPT,

        SOCKETT_MASK        = 0x0000000F,

        SOCKETT_EX_UNICAST  = 0x00000010,
        SOCKETT_EX_JUMBO    = 0x00000020,
        SOCKETT_EX_SEND     = 0x00000040,
        SOCKETT_EX_RECV     = 0x00000080,
        SOCKETT_EX_NOHEAD   = 0x00000100,
        SOCKETT_EX_HASHEAD  = 0x00000200,
        SOCKETT_EX_ALIVE    = 0x00000400,
        SOCKETT_EX_SETTIME  = 0x00000800,

        SOCKETT_EX_DERIVE   = (SOCKETT_EX_SEND|SOCKETT_EX_RECV|SOCKETT_EX_NOHEAD|SOCKETT_EX_HASHEAD),
        SOCKETT_EX_MASK     = 0x00000FF0,
    };

    enum SOCKET_TYPE
    {
        SOCKETT_INIT,
        SOCKETT_READY,
        SOCKETT_ACK,
        SOCKETT_LIVE,
    };

    enum SOCKET_STATUS
    {
        SOCKETS_NONE       = 0x00000000,
        SOCKETS_HANDLEMASK = 0x0000FFFF,

        SOCKETS_CLOSE      = 0x00010000,
        SOCKETS_CLOSEMASK  = 0x000F0000,

        SOCKETS_QUEUE      = 0x00100000,
        SOCKETS_PEND       = 0x00200000,
        SOCKETS_CHECK      = 0x00400000,
        SOCKETS_ASYNCMASK  = 0x00700000,

        SOCKETS_EXIT       = 0x00800000,

        SOCKETS_QUEUE_NEG  = 0x00F00000,
        SOCKETS_PEND_NEG   = 0x00E00000,
        SOCKETS_CHECK_NEG  = 0x00C00000,
    };
public:
    static void* operator new(size_t, PByte p);
    static void* operator new[](size_t, PByte p);
    static void  operator delete(void* p, PByte);
    static void  operator delete[](void* p, PByte);
    static void  operator delete(void* p, size_t);
    static void  operator delete[](void* p, size_t);
};

template <typename T>
INLINE void* CTNETSocketTraits<T>::operator new(size_t, PByte p)
{
    return p;
}

template <typename T>
INLINE void* CTNETSocketTraits<T>::operator new[](size_t, PByte p)
{
    return p;
}

template <typename T>
INLINE void CTNETSocketTraits<T>::operator delete(void*, PByte)
{
}

template <typename T>
INLINE void CTNETSocketTraits<T>::operator delete[](void*, PByte)
{
}

template <typename T>
INLINE void CTNETSocketTraits<T>::operator delete(void*, size_t)
{
}

template <typename T>
INLINE void CTNETSocketTraits<T>::operator delete[](void*, size_t)
{
}

///////////////////////////////////////////////////////////////////
//
#pragma pop_macro("delete")
#pragma pop_macro("new")

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_TRAITS_HXX__
