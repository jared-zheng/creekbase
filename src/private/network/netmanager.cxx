// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "rand.h"
#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetManager
UInt CNetManager::Init(const NET_ATTR& Attr, CEventHandler* pEventHandler, CNETPackBuild* pPackBuild)
{
    if (CAtomics::CompareExchange<uintptr_t>(&m_utStatus, MANAGERS_INIT, MANAGERS_NONE) == MANAGERS_NONE)
    {
        m_PackBuildPtr = pPackBuild;
        if (m_PackBuildPtr == nullptr)
        {
            m_PackBuildPtr = MNEW CNETPackBuildDef;
            if (m_PackBuildPtr == nullptr)
            {
                return (UInt)RET_FAIL;
            }
        }
        AdjustDump();
        AdjustAttr(Attr);

        if (m_hNetEvent.Open())
        {
            assert(m_inxSocket == nullptr);
            assert(m_inxBuffer == nullptr);
            assert(m_inxJumbo  == nullptr);
            assert(m_inxAddr   == nullptr);
            assert(m_TickThread.IsRunning() == false);
            assert(m_WorkThread.GetSize() == 0);
            assert(m_NetSocket.GetSize() == 0);
            // 1. cache init
            if (CacheInit())
            {
                CLCGRand Rand;
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
                m_utRadix       = (uintptr_t)Rand.Rand() << 16;
                m_utRadix      += (uintptr_t)Rand.Rand();
#else
                m_utRadix       = (uintptr_t)Rand.Rand();
#endif
                m_pEventHandler = pEventHandler;
                HandlerAddRef();

                CAtomics::Exchange<uintptr_t>(&m_utStatus, MANAGERS_OKAY);
                // 2. create work threads
                m_WorkThread.SetGrow(m_Attr.nThread);
                m_WorkThread.Add(m_Attr.nThread, true);
                for (Int i = 0; i < m_Attr.nThread; ++i)
                {
                    m_WorkThread[i] = MNEW CNETWorkThread(this);
                    m_WorkThread[i]->Start();
                }
                // 3. start tick thread if needed
                if ((m_Attr.nAttrs & (ATTR_ACK_DETECT|ATTR_TIMEOUT_DETECT)) != 0)
                {
                    m_TickThread.SetParam(this, m_Attr.nAckTime, m_Attr.nTimeout);
                    m_TickThread.Start();
                }
                assert(m_NetSocket.GetSize() == 0);
                DEV_DEBUG(TF(" CNetwork init okay[%p]"), m_utRadix);
                return (UInt)RET_OKAY;
            }
            m_hNetEvent.Close();
        }
        m_PackBuildPtr = nullptr;
        DEV_WARN(TF(" CNetwork init failed"));
    }
    return (UInt)RET_FAIL;
}

void CNetManager::Exit(void)
{
    if (CAtomics::CompareExchange<uintptr_t>(&m_utStatus, MANAGERS_EXIT, MANAGERS_OKAY) == MANAGERS_OKAY)
    {
        // 3. stop tick thread
        if (m_TickThread.IsRunning())
        {
            if (m_TickThread.Stop((UInt)(TIMET_S2MS + TIMET_S2MS)) == false)
            {
                m_TickThread.Kill();
            }
        }
        // 2.1 post quit status
        m_hNetEvent.SignalAll(m_WorkThread.GetSize());
        CPlatform::SleepEx((UInt)CNETWorkThread::THREAD_WAIT_TIME);
        // 2.2 destroy work threads
        for (Int i = 0; i < m_WorkThread.GetSize(); ++i)
        {
            if (m_WorkThread[i]->Stop((UInt)CNETWorkThread::THREAD_WAIT_TIME) == false)
            {
                m_WorkThread[i]->Kill();
            }
            MDELETE m_WorkThread[i];
        }
        m_WorkThread.RemoveAll();
        // 3. sockets
        RemoveAllSocket();
        m_hNetEvent.Close();
        // 4. cache exit
        CacheExit();
        HandlerRelease();

        m_PackBuildPtr = nullptr;

        CAtomics::Exchange<uintptr_t>(&m_utStatus, MANAGERS_NONE);
        DEV_DEBUG(TF(" CNetwork exit okay"));
    }
}

void CNetManager::HandlerAddRef(void)
{
    if (m_pEventHandler != nullptr)
    {
        m_pEventHandler->AddRef();
    }
}

void CNetManager::HandlerRelease(void)
{
    TRY_EXCEPTION
        if (m_pEventHandler != nullptr)
        {
            m_pEventHandler->Release();
            m_pEventHandler = nullptr;
        }
    CATCH_EXCEPTION
    END_EXCEPTION
}

bool CNetManager::Check(UInt uTimeout)
{
    if (CheckStatus())
    {
        for (Int i = 0; i < m_WorkThread.GetSize(); ++i)
        {
            if (m_WorkThread[i]->IsRunning() == false)
            {
                return true; // work thread is stoped or except terminated
            }
            if (m_WorkThread[i]->CheckStatus(uTimeout))
            {
                return true; // work thread is timeout
            }
        }
        if ((m_Attr.nAttrs & (ATTR_ACK_DETECT|ATTR_TIMEOUT_DETECT)) != 0)
        {
            if (m_TickThread.IsRunning() == false)
            {
                return true; // tick thread is stoped or except terminated
            }
            if (m_TickThread.CheckStatus((TIMET_S2MS + TIMET_S2MS) / TIMET_XS2XS))
            {
                return true; // tick thread is timeout
            }
        }
    }
    return false;
}

CNETSocket* CNetManager::CreateSocket(UInt uFlag, CEventHandler* pEventHandler, PSOCKADDR_INET pSockAddr)
{
    assert(CheckStatus());
    CNETSocket* pSocket = nullptr;
    if (uFlag < SOCKET_UDP)
    {
        pSocket = AllocSocket<CTCPSocket>(pEventHandler);
    }
    else if (GetCreateTraits(uFlag) < SOCKET_OPTION)
    {
        pSocket = AllocSocket<CUDPSocket>(pEventHandler);
    }
    if (pSocket != nullptr)
    {
        if (pSocket->Init((Int)uFlag, pSockAddr))
        {
            assert(CheckStatus());
            return pSocket;
        }
        if (pSocket->Exit())
        {
            FreeSocket(pSocket);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" CreateSocket AllocSocket failed"));
    }
#endif
    return nullptr;
}

bool CNetManager::DestroySocket(CNETSocket* pSocket, bool bAsync)
{
    if (CheckStatus())
    {
        if (pSocket->Exit(bAsync))
        {
            DEV_DEBUG(TF(" FreeSocket Socket %p"), pSocket);
            FreeSocket(pSocket);
        }
    }
    return true;
}

bool CNetManager::Listen(CTCPSocket* pSocket, Int nAccept)
{
    assert(CheckStatus());
    bool bRet = false;
    if (pSocket->IsTCP())
    {
        bRet = pSocket->Listen(nAccept);
    }
    else
    {
        DEV_DEBUG(TF(" Listen socket[%p] invalid type : %d"), pSocket, pSocket->GetTraits());
    }
    return bRet;
}

bool CNetManager::Connect(CTCPSocket* pSocket, const SOCKADDR_INET& SockAddr)
{
    assert(CheckStatus());
    bool bRet = false;
    if (pSocket->IsTCP())
    {
        bRet = pSocket->Connect(SockAddr);
    }
    else
    {
        DEV_DEBUG(TF(" Connect socket[%p] invalid type : %d"), pSocket, pSocket->GetTraits());
    }
    return bRet;
}

bool CNetManager::Send(CTCPSocket* pSocket, class CNETWriteStream* pStream, Int nFlag, ULLong ullParam)
{
    assert(CheckStatus());
    size_t      stSize  = (size_t)pStream->Tell();
    CNETBuffer* pBuffer = nullptr;
    size_t      stStyle = CNETBuffer::BUFFERS_SEND;
    if (pStream->GetIndex() != m_inxBuffer)
    {
        stStyle |= CNETBuffer::BUFFERS_JUMBO;
    }
    switch (nFlag)
    {
    case SEND_NORMAL:
    case SEND_WAIT:
        {
            CreateBuffer(stStyle, pBuffer, pStream->GetCache());
        }
        break;
    case SEND_LIVE:
        {
            if (pSocket->GetLiveData() != 0)
            {
                CreateBuffer(stStyle, pBuffer, pStream->GetCache());
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                DEV_DEBUG(TF(" Send socket[%p] Check Live-Data null"), pSocket);
            }
#endif
        }
        break;
    case SEND_CLOSE:
        {
            stStyle |= CNETBuffer::BUFFERS_CLOSE;
            CreateBuffer(stStyle, pBuffer, pStream->GetCache());
        }
        break;
    case SEND_BROADCAST:
    case SEND_BROADCAST_ALL:
    case SEND_BROADCAST_AS:
    case SEND_BROADCAST_NOT:
    case SEND_BROADCAST_OR:
    case SEND_BROADCAST_AND:
    case SEND_BROADCAST_XOR:
    case SEND_BROADCAST_GT:
    case SEND_BROADCAST_GTOE:
    case SEND_BROADCAST_LT:
    case SEND_BROADCAST_LTOE:
        {
            return BroadCastSend(pSocket, pStream, nFlag, ullParam);
        }
        break;
    default:
        {
#ifdef __RUNTIME_DEBUG__
            DEV_DEBUG(TF(" Send socket[%p] Check invalid send event"), pSocket);
#endif
        }
    }
    if ((stSize > 0) && (pBuffer != nullptr))
    {
        pBuffer->Pack(stSize, pStream->GetDataOffset(), m_PackBuildPtr);

        bool bRet = false;
        if (pSocket->IsTCPTraffic())
        {
            bRet = pSocket->Send(pBuffer);
        }
        if (bRet)
        {
            return true;
        }
        if (pBuffer != nullptr)
        {
            DestroyBuffer(pBuffer);
        }
    }
    return false;
}

bool CNetManager::Send(const CTArray<Socket>& aSocket, class CNETWriteStream* pStream, Int nFlag, ULLong ullParam)
{
    assert(CheckStatus());
    size_t               stSize = (size_t)pStream->Tell();
    CNETBufferBroadCast* pBuffer = nullptr;
    size_t               stStyle = CNETBuffer::BUFFERS_SEND;
    if (pStream->GetIndex() != m_inxBuffer)
    {
        stStyle |= CNETBuffer::BUFFERS_JUMBO;
    }
    if (nFlag == SEND_CLOSE)
    {
        stStyle |= CNETBuffer::BUFFERS_CLOSE;
    }
    if (stSize > 0)
    {
        if (CreateBuffer(stStyle, (CNETBuffer*&)pBuffer, pStream->GetCache(), true) == false)
        {
            return false;
        }
        pBuffer->Pack(stSize, pStream->GetDataOffset(), m_PackBuildPtr);

        ARY_NETSOCKET MultiCastSockets;
        {
            DEV_DEBUG(TF(" #MultiCastSend in !"));
            CSyncLockScope scope(m_NMLock);
            DEV_DEBUG(TF(" #MultiCastSend ready !"));
            switch (nFlag)
            {
            case SEND_NORMAL:
            case SEND_WAIT:
            case SEND_BROADCAST:
            case SEND_BROADCAST_ALL:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_LIVE:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() != 0))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_AS:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() == ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_NOT:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() != ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_OR:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        ((pMultiCast->m_V->GetLiveData() | ullParam) != 0))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_AND:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        ((pMultiCast->m_V->GetLiveData() & ullParam) != 0))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_XOR:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        ((pMultiCast->m_V->GetLiveData() ^ ullParam) != 0))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_GT:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() > ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_GTOE:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() >= ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_LT:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() < ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            case SEND_BROADCAST_LTOE:
            {
                for (Int i = 0; i < aSocket.GetSize(); ++i)
                {
                    PAIR_NETSOCKET* pMultiCast = m_NetSocket.Find(aSocket[i]);
                    if ((pMultiCast != nullptr) &&
                        (pMultiCast->m_V->Check()) &&
                        (pMultiCast->m_V->IsTCPTraffic()) &&
                        (pMultiCast->m_V->GetLiveData() <= ullParam))
                    {
                        pMultiCast->m_V->SetCheckStatus(true);
                        MultiCastSockets.Add(pMultiCast->m_V);
                    }
                }
            }
            break;
            default:
            {
            }
            }
        }
        UInt uSend = 0;
        UInt uCount = (UInt)MultiCastSockets.GetSize();
        if (uCount > 0)
        {
            DEV_DEBUG(TF(" #MultiCastSend start !"));
            CNETBuffer* pBufferRef = nullptr;
            for (UInt i = 0; i < uCount; ++i)
            {
                pBufferRef = pBuffer;

                CTCPSocket* pMultiCast = static_cast<CTCPSocket*>(MultiCastSockets[i]);
                pMultiCast->Send(pBufferRef);
                if (pBufferRef == nullptr)
                {
                    ++uSend;
                }
                if (pMultiCast->SetCheckStatus(false))
                {
                    DEV_DEBUG(TF(" !Send destroy Socket %p---%X"), pMultiCast, uSend);
                    DestroySocket(pMultiCast, false);
                }
            }
            DEV_DEBUG(TF(" #MultiCastSend match sockets[%d---ullParam=%llX---%d] out !"), nFlag, ullParam, uSend);
        }
        if (pBuffer->Finish(uSend))
        {
            DEV_DEBUG(TF(" #MultiCastSend Finished[%d---ullParam=%llX---%d] ---destroy broadcast buffer %p!"), nFlag, ullParam, uSend, pBuffer);
            DestroyBuffer((CNETBuffer*&)pBuffer, true);
        }
        return (uSend > 0);
    }
    return false;
}

bool CNetManager::SendTo(CUDPSocket* pSocket, class CNETWriteStream* pStream, const SOCKADDR_INET& SockAddr, Int nFlag)
{
    assert(CheckStatus());
    size_t      stSize  = (size_t)pStream->Tell();
    CNETBuffer* pBuffer = nullptr;
    Int         stStyle = CNETBuffer::BUFFERS_SEND;
    if (pStream->GetIndex() != m_inxBuffer)
    {
        stStyle |= CNETBuffer::BUFFERS_JUMBO;
    }
    switch (nFlag)
    {
    case SEND_NORMAL:
    case SEND_WAIT:
        {
            CreateBuffer(stStyle, pBuffer, pStream->GetCache());
        }
        break;
    case SEND_LIVE:
        {
            if (pSocket->GetLiveData() != 0)
            {
                CreateBuffer(stStyle, pBuffer, pStream->GetCache());
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                DEV_DEBUG(TF(" SendTo socket[%p] Check Live-Data null"), pSocket);
            }
#endif
        }
        break;
    case SEND_CLOSE:
        {
            stStyle |= CNETBuffer::BUFFERS_CLOSE;
            CreateBuffer(stStyle, pBuffer, pStream->GetCache());
        }
        break;
    case SEND_BROADCAST:
        {
            CreateBuffer(stStyle, pBuffer, pStream->GetCache());
        }
        break;
    default:
        {
#ifdef __RUNTIME_DEBUG__
            DEV_DEBUG(TF(" SendTo socket[%p] Check invalid send event"), pSocket);
#endif
        }
    }
    if ((stSize > 0) && (pBuffer != nullptr))
    {
        pBuffer->Pack(stSize, pStream->GetDataOffset(), m_PackBuildPtr);

        bool bRet = false;
        if (pSocket->IsUDP())
        {
            bRet = pSocket->SendTo(pBuffer, SockAddr);
        }
        if (bRet)
        {
            return true;
        }
        if (pBuffer != nullptr)
        {
            DestroyBuffer(pBuffer);
        }
    }

    return false;
}

size_t CNetManager::AdjustSize(ADJUST_SIZE eAdjustSize) const
{
    switch (eAdjustSize)
    {
    case ADJUSTS_BUFFER_ALL:
        {
            return (sizeof(CNETBufferBroadCast) + m_PackBuildPtr->OffsetSize() + (size_t)m_Attr.nMaxBuffer);
        }
        break;
    case ADJUSTS_JUMBO_ALL:
        {
            return (sizeof(CNETBufferBroadCast) + m_PackBuildPtr->OffsetSize() + (size_t)m_Attr.nMaxJumbo);
        }
        break;
    case ADJUSTS_BUFFER_OFFSET:
    case ADJUSTS_JUMBO_OFFSET:
        {
            return (sizeof(CNETBufferBroadCast) + m_PackBuildPtr->OffsetSize());
        }
        break;
    case ADJUSTS_BUFFER_DATA:
        {
            return (m_PackBuildPtr->OffsetSize() + (size_t)m_Attr.nMaxBuffer);
        }
        break;
    case ADJUSTS_JUMBO_DATA:
        {
            return (m_PackBuildPtr->OffsetSize() + (size_t)m_Attr.nMaxJumbo);
        }
        break;
    default:
        {
            return 0;
        }
    }
}

void CNetManager::LiveAck(LLong llCurrent, Int nAck)
{
    if (CheckStatus())
    {
        ARY_NETSOCKET AckSockets;
        {
            DEV_TRACE(TF(" #LiveAck process in !"));
            CSyncLockScope scope(m_NMLock);
            DEV_TRACE(TF(" #LiveAck process ready !"));
            PINDEX index = m_NetSocket.GetFirstIndex();
            while (index != nullptr)
            {
                CNETSocket* pSocket = (m_NetSocket.GetNext(index)->m_V);
                if (pSocket->Check() && pSocket->IsAckType())
                {
                    if (pSocket->CheckLiveTime(llCurrent, nAck))
                    {
                        pSocket->SetCheckStatus(true);
                        pSocket->SetCloseStatus();
                        AckSockets.Add(pSocket);
                    }
                }
            }
            DEV_TRACE(TF(" #LiveAck process start"));
        }
        for (Int i = 0; i < AckSockets.GetSize(); ++i)
        {
            AckSockets[i]->SetCheckStatus(false);
            DestroySocket(AckSockets[i], false);
            DEV_DEBUG(TF(" #LiveAck destroy Socket %p"), AckSockets[i]);
        }
        DEV_TRACE(TF(" #LiveAck process out !"));
    }
}

void CNetManager::Timeout(LLong llCurrent, Int nTimeout)
{
    if (CheckStatus())
    {
        ARY_NETSOCKET TimeoutSockets;
        {
            DEV_TRACE(TF(" #Timeout process in !"));
            CSyncLockScope scope(m_NMLock);
            DEV_TRACE(TF(" #Timeout process ready !"));
            PINDEX index = m_NetSocket.GetFirstIndex();
            while (index != nullptr)
            {
                CNETSocket* pSocket = (m_NetSocket.GetNext(index)->m_V);
                if (pSocket->Check())
                {
                    if ((pSocket->IsTCPAccept()  && pSocket->IsTrafficType()) ||
                        (pSocket->IsTCPConnect() && pSocket->IsReadyType()))
                    {
                        if (pSocket->CheckLiveTime(llCurrent, nTimeout))
                        {
                            pSocket->SetCheckStatus(true);
                            pSocket->SetCloseStatus();
                            TimeoutSockets.Add(pSocket);
                        }
                    }
                }
            }
            DEV_TRACE(TF(" #Timeout process start"));
        }
        for (Int i = 0; i < TimeoutSockets.GetSize(); ++i)
        {
            TimeoutSockets[i]->SetCheckStatus(false);
            DestroySocket(TimeoutSockets[i], false);
            DEV_DEBUG(TF(" #Timeout destroy Socket %p"), TimeoutSockets[i]);
        }
        DEV_TRACE(TF(" #Timeout process out"));
    }
}

bool CNetManager::CacheInit(void)
{
    size_t stGrow = CACHE_DEF_NORMAL_COUNT;
    if (m_Attr.nAttrs & ATTR_MAX_LARGE)
    {
        stGrow = CACHE_DEF_LARGE_COUNT;
        m_NetSocket.SetHash(CACHE_DEF_LARGE_HASH);
    }
    else if (m_Attr.nAttrs & ATTR_MAX_MASSIVE)
    {
        stGrow = CACHE_DEF_MASSIVE_COUNT;
        m_NetSocket.SetHash(CACHE_DEF_MASSIVE_HASH);
    }
    else
    {
        m_NetSocket.SetHash(CACHE_DEF_NORMAL_HASH);
    }
    size_t stMax = stGrow * CACHE_DEF_RATIO;

    if (m_inxSocket == nullptr)
    {
        size_t stSocket = sizeof(CTCPSocket);
        if (stSocket < sizeof(CUDPSocket))
        {
            stSocket = sizeof(CUDPSocket);
        }
        m_inxSocket = MCCreate(stSocket, stGrow * stSocket, stMax * stSocket);
        DEV_DEBUG(TF("+ CNetwork component socket cache=[%p, %d, %d]"), m_inxSocket, stGrow * stSocket, stMax * stSocket);
    }
    if (m_inxBuffer == nullptr)
    {
        size_t stGrowSize = stGrow * AdjustSize(ADJUSTS_BUFFER_ALL);
        stGrowSize = DEF::Maxmin<size_t>(stGrowSize, CACHE_DEF_MIN_GROW, CACHE_DEF_MAX_GROW);

        m_inxBuffer = MCCreate(AdjustSize(ADJUSTS_BUFFER_ALL), stGrowSize, stMax * AdjustSize(ADJUSTS_BUFFER_ALL));
        DEV_DEBUG(TF("+ CNetwork component buffer cache=[%p, %d, %d]"), m_inxBuffer, stGrowSize, stMax * AdjustSize(ADJUSTS_BUFFER_ALL));
    }
    if (m_inxJumbo == nullptr)
    {
        size_t stGrowSize = CACHE_DEF_NORMAL_COUNT * AdjustSize(ADJUSTS_JUMBO_ALL);
        stGrowSize = DEF::Maxmin<size_t>(stGrowSize, CACHE_DEF_MIN_GROW, CACHE_DEF_MAX_GROW);

        m_inxJumbo = MCCreate(AdjustSize(ADJUSTS_JUMBO_ALL), stGrowSize, CACHE_DEF_JUMBO_MAX * AdjustSize(ADJUSTS_JUMBO_ALL));
        DEV_DEBUG(TF("+ CNetwork component jumbo cache=[%p, %d, %d]"), m_inxJumbo, stGrowSize, CACHE_DEF_JUMBO_MAX * AdjustSize(ADJUSTS_JUMBO_ALL));
    }
    if (m_inxAddr == nullptr)
    {
        m_inxAddr = MCCreate(sizeof(SOCKADDR_INET), stGrow * sizeof(SOCKADDR_INET), stMax * sizeof(SOCKADDR_INET));
        DEV_DEBUG(TF("+ CNetwork component addr cache=[%p, %d, %d]"), m_inxAddr, stGrow * sizeof(SOCKADDR_INET), stMax * sizeof(SOCKADDR_INET));
    }
    DEV_DEBUG(TF("+ CNetwork component socket size=[%d, %d], buffer class size =[%d, %d], buffer[all size=%d, offset=%d, data=%d], jumbo buffer[all size=%d, offset=%d, data=%d], event = %d"),
              sizeof(CTCPSocket), sizeof(CUDPSocket), sizeof(CNETBuffer), sizeof(CNETBufferBroadCast),
              AdjustSize(ADJUSTS_BUFFER_ALL), AdjustSize(ADJUSTS_BUFFER_OFFSET), AdjustSize(ADJUSTS_BUFFER_DATA),
              AdjustSize(ADJUSTS_JUMBO_ALL),  AdjustSize(ADJUSTS_JUMBO_OFFSET),  AdjustSize(ADJUSTS_JUMBO_DATA),
              m_Attr.nMaxEvent);

    m_Attr.nBufferSize    = m_Attr.nMaxBuffer;
    m_Attr.nJumboSize     = m_Attr.nMaxJumbo;
    m_Attr.nBufferOffset  = (Int)AdjustSize(ADJUSTS_BUFFER_OFFSET);
    m_Attr.nJumboOffset   = (Int)AdjustSize(ADJUSTS_JUMBO_OFFSET);
    m_Attr.inxBuffer      = m_inxBuffer;
    m_Attr.inxJumbo       = m_inxJumbo;
    return ((m_inxSocket != nullptr) && (m_inxBuffer != nullptr) && (m_inxJumbo != nullptr) && (m_inxAddr != nullptr));
}

void CNetManager::CacheExit(void)
{
    if (m_inxSocket != nullptr)
    {
        DEV_DEBUG(TF(" Network Socket memaid cache[%p] used size %p"), m_inxSocket, MCUsedSize(m_inxSocket));
        MCDestroy(m_inxSocket);
        m_inxSocket = nullptr;
    }
    if (m_inxBuffer != nullptr)
    {
        DEV_DEBUG(TF(" Network Buffer memaid cache[%p] used size %p"), m_inxBuffer, MCUsedSize(m_inxBuffer));
        MCDestroy(m_inxBuffer);
        m_inxBuffer = nullptr;
    }
    if (m_inxJumbo != nullptr)
    {
        DEV_DEBUG(TF(" Network Jumbo Buffer memaid cache[%p] used size %p"), m_inxJumbo, MCUsedSize(m_inxJumbo));
        MCDestroy(m_inxJumbo);
        m_inxJumbo = nullptr;
    }
    if (m_inxAddr != nullptr)
    {
        DEV_DEBUG(TF(" Network Addr Buffer memaid cache[%p] used size %p"), m_inxAddr, MCUsedSize(m_inxAddr));
        MCDestroy(m_inxAddr);
        m_inxAddr = nullptr;
    }
    m_Attr.nBufferSize    = 0;
    m_Attr.nJumboSize     = 0;
    m_Attr.nBufferOffset  = 0;
    m_Attr.nJumboOffset   = 0;
    m_Attr.inxBuffer      = nullptr;
    m_Attr.inxJumbo       = nullptr;
}

bool CNetManager::BroadCastSend(CTCPSocket* pSocket, CNETWriteStream* pStream, Int nFlag, ULLong ullParam)
{
    size_t               stSize  = (size_t)pStream->Tell();
    CNETBufferBroadCast* pBuffer = nullptr;
    Int                  nStyle  = CNETBuffer::BUFFERS_SEND;
    if (pStream->GetIndex() != m_inxBuffer)
    {
        nStyle |= CNETBuffer::BUFFERS_JUMBO;
    }
    if (CreateBuffer(nStyle, (CNETBuffer*&)pBuffer, pStream->GetCache(), true) == false)
    {
        return false;
    }
    pBuffer->Pack(stSize, pStream->GetDataOffset(), m_PackBuildPtr);

    ARY_NETSOCKET BroadCastSockets;
    {
        DEV_DEBUG(TF(" #BroadCastSend in !"));
        CSyncLockScope scope(m_NMLock);
        DEV_DEBUG(TF(" #BroadCastSend ready !"));
        PINDEX index = m_NetSocket.GetFirstIndex();

        switch (nFlag)
        {
        case SEND_BROADCAST:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast != pSocket) &&
                        (pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_ALL:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_AS:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() == ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_NOT:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() != ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_OR:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        ((pBroadCast->GetLiveData() | ullParam) != 0))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_AND:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        ((pBroadCast->GetLiveData() & ullParam) != 0))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_XOR:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        ((pBroadCast->GetLiveData() ^ ullParam) != 0))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_GT:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() > ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_GTOE:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() >= ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_LT:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() < ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        case SEND_BROADCAST_LTOE:
            {
                while (index != nullptr)
                {
                    CNETSocket* pBroadCast = (m_NetSocket.GetNext(index)->m_V);
                    if ((pBroadCast->Check() == true) && 
                        (pBroadCast->IsBroadCastEnable(CNETSocket::SOCKETT_TCP_ACCEPT)) &&
                        (pBroadCast->GetLiveData() <= ullParam))
                    {
                        pBroadCast->SetCheckStatus(true);
                        BroadCastSockets.Add(pBroadCast);
                    }
                }
            }
            break;
        default:
            {
            }
        }
    }
    UInt uSend  = 0;
    UInt uCount = (UInt)BroadCastSockets.GetSize();
    if (uCount > 0)
    {
        DEV_DEBUG(TF(" #BroadCastSend start !"));
        CNETBuffer* pBufferRef = nullptr;
        for (UInt i = 0; i < uCount; ++i)
        {
            pBufferRef = pBuffer;

            CTCPSocket* pBroadCast = static_cast<CTCPSocket*>(BroadCastSockets[i]);
            pBroadCast->Send(pBufferRef);
            if (pBufferRef == nullptr)
            {
                ++uSend;
            }
            if (pBroadCast->SetCheckStatus(false))
            {
                DestroySocket(pBroadCast, false);
            }
        }
        DEV_DEBUG(TF(" #BroadCastSend match sockets[%d---ullParam=%llX---%d] out !"), nFlag, ullParam, uSend);
    }
    if (pBuffer->Finish(uSend))
    {
        DEV_DEBUG(TF(" #BroadCastSend Finished[%d---ullParam=%llX---%d] ---destroy broadcast buffer %p!"), nFlag, ullParam, uSend, pBuffer);
        DestroyBuffer((CNETBuffer*&)pBuffer, true);
    }
    return (uSend > 0);
}

