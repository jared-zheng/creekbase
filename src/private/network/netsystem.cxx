// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "netmanager.hxx"
#include "netsystem.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetworkSystem : CSubSystem
INLINE CNetworkSystem::CNetworkSystem(void)
{
    DEV_DEBUG(TF("* Network SubSystem singleton Inst created"));
}

INLINE CNetworkSystem::~CNetworkSystem(void)
{
    DEV_DEBUG(TF("* Network SubSystem singleton Inst destroyed"));
}
// CTRefCount
INLINE Int CNetworkSystem::Release(void)
{
    return m_Counter.Reset();
}
// CComponent
INLINE UInt CNetworkSystem::Command(PCXStr, uintptr_t)
{
    return 0;
}

INLINE UInt CNetworkSystem::Update(void)
{
    return 0;
}

// CSubSystem
INLINE Int CNetworkSystem::GetComponentInfo(MAP_COMPONENT_INFO& Components) const
{
    // CNetwork
    Components.Add(UUID_OF( CNetwork ), CStringKey(NAME_OF( CNetwork )));

    return Components.GetSize();
}

INLINE bool CNetworkSystem::FindComponent(const CUUID& uuId) const
{
    // CNetwork
    if (uuId == UUID_OF( CNetwork ))
    {
        return true;
    }
    return false;
}

INLINE bool CNetworkSystem::CreateComponent(const CUUID& uuId, CComponentPtr& CComponentPtrRef)
{
    CComponentPtrRef = nullptr;
    // CNetwork
    if (uuId == UUID_OF( CNetwork ))
    {
        DEV_DUMP(TF("+ CNetwork component created"));
        CNetManager* pNetwork = MNEW CNetManager;
        if (pNetwork != nullptr)
        {
            CComponentPtrRef = pNetwork;
            return true;
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CreateSubSystem
NETWORKAPI IMPLEMENT_CREATE_SUBSYSTEM( NetworkStatic )
{
    if (uuId == UUID_OF( CNetworkSystem ))
    {
        CNetworkSystemSingleton::GetInstance()->AddRef();
        return CNetworkSystemSingleton::GetInstance();
    }
    return nullptr;
}


