// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_THREAD_HXX__
#define __NETWORK_THREAD_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetnetthread.hxx"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetnetthread.hxx"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETTickThread
class CNETTickThread : public CThread
{
public:
    CNETTickThread(void);
    virtual ~CNETTickThread(void);

    void    SetParam(CNetManager* pNetwork, Int nAck = 0, Int nTimeout = 0);
protected:
    virtual bool Run(void) OVERRIDE;
private:
    CNetManager*   m_pNetwork;
    LLong          m_llAckTime;
    LLong          m_llTimeout;
    Int            m_nAckTime;
    Int            m_nAckTimeTick;
    Int            m_nTimeout;
    Int            m_nTimeoutTick;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_THREAD_HXX__
