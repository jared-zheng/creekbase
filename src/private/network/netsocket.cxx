// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETSocket
bool CNETSocket::Add(void)
{
    m_Index = m_pNetwork->AddSocket(m_utId, this);
    return (m_Index != nullptr);
}

bool CNETSocket::Check(void)
{
    return (CAtomics::CompareExchangePtr((void* volatile*)&m_Index, nullptr, nullptr) != nullptr);
}

bool CNETSocket::Remove(void)
{
    PINDEX index = (PINDEX)CAtomics::ExchangePtr((void* volatile*)&m_Index, nullptr);
    if (index != nullptr)
    {
        return m_pNetwork->RemoveSocket(index);
    }
    return false;
}

void CNETSocket::HandlerAddRef(void)
{
    m_pEventHandler->AddRef();
}

void CNETSocket::HandlerRelease(void)
{
    TRY_EXCEPTION
        if (m_pEventHandler != nullptr)
        {
            m_pEventHandler->Release();
            m_pEventHandler = nullptr;
        }
    CATCH_EXCEPTION
    END_EXCEPTION
}

bool CNETSocket::RecvBufferHandle(CNETBuffer*& pBuffer, size_t stBytes)
{
    bool  bRet = true;
    if (IsExcludeHead() == false)
    {
        size_t stBuf = pBuffer->GetBufUsed() + stBytes;      // cached data size + recv data size
        // !!!special pack: pack only has head size for ack or live
        if (stBuf >= m_pNetwork->GetPackBuild()->HeadSize()) // data size must >= pack head size
        {
            bRet = DetectPackComplete(pBuffer, stBuf);
        }
        else // recv data size < head
        {
            pBuffer->SetBuf(0, stBuf);
            DEV_DEBUG(TF(" NETSocket[%p] recv cached data size[%d] <= pack head size[%d]"), this, stBuf, m_pNetwork->GetPackBuild()->HeadSize());
        }
    }
    else
    {
        PByte pData = pBuffer->GetData(m_pNetwork->GetPackBuild()->OffsetSize());
        bRet = OnPackComplete(pData, stBytes, pBuffer, true);
        if (bRet)
        {
            bRet = CheckBuffer(pBuffer, stBytes + 1, (pData == nullptr));
            if ((stBytes >= m_pNetwork->AdjustSize(CNetManager::ADJUSTS_BUFFER_DATA)) &&
                (IsJumboRecv() == false) &&
                CreateJumboBuffer(CNETBuffer::BUFFERS_RECV))
            {
                DEV_DEBUG(TF(" NETSocket[%p] ExcludeHead CreateJumboBuffer and add jumbo recv status!"), this);
                pBuffer = m_pJumboBuffer;
                SetTraitsEx(SOCKETT_EX_JUMBO, true);
            }
        }
        else
        {
            DEV_DEBUG(TF(" NETSocket[%p] RecvEvent return false"), this);
            CheckBuffer(pBuffer, stBytes, (pData == nullptr));
            bRet = IsUDP(); // udp is not 1 <--> 1, just discard buffer's pack data, can not close
        }
        if (bRet)
        {
            pBuffer->SetBuf(0, 0);
        }
    }
    return bRet;
}

bool CNETSocket::DetectPackComplete(CNETBuffer*& pBuffer, size_t stBuf)
{
    size_t stHead  = m_pNetwork->GetPackBuild()->HeadSize();
    size_t stCount = 0; // buffer recv complete pack count
    size_t stMax   = 0; // buffer recv max complete pack size

    size_t stStart = 0;
    size_t stSize  = stBuf;
    PByte  pHead   = nullptr;
    bool   bRet    = pBuffer->Unpack(pHead, stStart, stSize, m_pNetwork->GetPackBuild());
    // !!!special pack: pack only has head size for ack or live
    while (stSize >= stHead)
    {
        if (stSize <= stBuf)
        {
            ++stCount;
            stMax    = (stSize > stMax) ? stSize : stMax;
            stBuf   -= stSize;
            stStart += stSize;
            // 
            if (IsIncludeHead() == false)
            {
                stSize -= stHead;
                pHead  += stHead;
            }
            if (stSize > 0)
            {
                if ((stCount == 1) && (stBuf == 0)) // one recv get all data
                {
                    bRet = OnPackComplete(pHead, stSize, pBuffer, true);
                }
                else
                {
                    bRet = OnPackComplete(pHead, stSize, pBuffer, false);
                }
            }
            else
            {
                if (IsAckType())
                {
                    SetType(SOCKETT_LIVE);
                }
                SetLiveTime();
            }
            if (bRet)
            {
                stSize = stBuf;
                if (stSize == 0)
                {
                    bRet = CheckBuffer(pBuffer, stMax, ((stCount == 1) && (pHead == nullptr)));
                }
                else if (stSize >= stHead)
                {
                    DEV_DEBUG(TF(" NETSocket[%p] DetectPackComplete next pack begin at %d, size is %d"), this, stStart, stSize);
                    bRet = pBuffer->Unpack(pHead, stStart, stSize, m_pNetwork->GetPackBuild());
                }
            }
            else
            {
                DEV_DEBUG(TF(" NETSocket[%p] RecvEvent return false"), this);
                CheckBuffer(pBuffer, stMax, ((stCount == 1) && (pHead == nullptr)));
                stBuf = 0;
                bRet  = IsUDP(); // udp is not 1 <--> 1, just discard buffer's pack data, can not close
                break;
            }
        }
        else if (stSize <= m_pNetwork->AdjustSize(CNetManager::ADJUSTS_JUMBO_DATA)) // jumbo recving
        {
            DEV_DEBUG(TF(" NETSocket[%p] DetectPackComplete recv complete pack count=%d, max complete pack size=%d, ready next jumbo buffer recv"), this, stCount, stMax);
            return JumboBufferRecv(pBuffer, stStart, stBuf); // common buffer swap to jumbo buffer special
        }
    }
    if (bRet)
    {
        //DEV_DEBUG(TF(" NETSocket[%p] DetectPackComplete recv complete pack count=%d, max complete pack size=%d, ready next buffer recv"), this, stCount, stMax);
        return pBuffer->SetBuf(stStart, stBuf);
    }
    DEV_DEBUG(TF(" NETSocket[%p] DetectPackComplete recv complete pack count=%d, max complete pack size=%d, recv return false"), this, stCount, stMax);
    return false;
}

bool CNETSocket::CheckBuffer(CNETBuffer*& pBuffer, size_t stMax, bool bHandOver)
{
    bool bRet = true;
    if (bHandOver)
    {
        if (pBuffer == m_pCommonBuffer)
        {
            //DEV_DEBUG(TF(" NETSocket[%p] discard common recv-buffer, create new buffer"), this);
            DiscardCommonBuffer();
            bRet = CreateCommonBuffer(CNETBuffer::BUFFERS_RECV);
            pBuffer = m_pCommonBuffer;
        }
        else
        {
            assert(pBuffer == m_pJumboBuffer);
            //DEV_DEBUG(TF(" NETSocket[%p] discard jumbo recv-buffer, remove jumbo recv status"), this);
            DiscardJumboBuffer();
            if (stMax <= m_pNetwork->AdjustSize(CNetManager::ADJUSTS_BUFFER_DATA))
            {
                SetTraitsEx(SOCKETT_EX_JUMBO, false);
                pBuffer = m_pCommonBuffer;
            }
            else
            {
                SetTraitsEx(SOCKETT_EX_JUMBO, true);
                bRet = CreateJumboBuffer(CNETBuffer::BUFFERS_RECV);
                pBuffer = m_pJumboBuffer;
            }
        }
    }
    else if (pBuffer->IsJumbo())
    {
        if (stMax <= m_pNetwork->AdjustSize(CNetManager::ADJUSTS_BUFFER_DATA))
        {
            DEV_DEBUG(TF(" NETSocket[%p] jumbo buffer recv finish, destroy jumbo buffer & remove jumbo recv status!"), this);
            SetTraitsEx(SOCKETT_EX_JUMBO, false);
            DestroyJumboBuffer();
            pBuffer = m_pCommonBuffer;
        }
        else
        {
            DEV_DEBUG(TF(" NETSocket[%p] jumbo buffer recv finish, recv max complete pack size > buffer size, jumbo recv again!"), this);
        }
    }
    return bRet;
}

bool CNETSocket::JumboBufferRecv(CNETBuffer*& pBuffer, size_t stStart, size_t stBuf)
{
    bool bRet = false;
    if (pBuffer == m_pJumboBuffer)
    {
        DEV_DEBUG(TF(" NETSocket[%p] jumbo recv again, start=%d, remain=%d!"), this, stStart, stBuf);
        assert(IsJumboRecv());
        bRet = pBuffer->SetBuf(stStart, stBuf);
    }
    else if (CreateJumboBuffer(CNETBuffer::BUFFERS_RECV))
    {
        DEV_DEBUG(TF(" NETSocket[%p] CreateJumboBuffer and add jumbo recv status!"), this);
        assert(IsJumboRecv() == false);
        bRet    = m_pJumboBuffer->CopyBuf(pBuffer->GetData(stStart), stBuf);
        pBuffer = m_pJumboBuffer;
        SetTraitsEx(SOCKETT_EX_JUMBO, true);
    }
    else
    {
        DEV_DEBUG(TF(" NETSocket[%p] CreateJumboBuffer failed!"), this);
    }
    return bRet; // common buffer swap to jumbo buffer special
}

bool CNETSocket::CreateCommonBuffer(CNETBuffer::BUFFER_STYLE eBufferStyle)
{
    if (m_pCommonBuffer == nullptr)
    {
        return m_pNetwork->CreateBuffer(eBufferStyle, m_pCommonBuffer);
    }
    return true;
}

bool CNETSocket::DestroyCommonBuffer(void)
{
    if (m_pCommonBuffer != nullptr)
    {
        m_pNetwork->DestroyBuffer(m_pCommonBuffer);
        m_pCommonBuffer = nullptr;
    }
    return true;
}

void CNETSocket::DiscardCommonBuffer(void)
{
    m_pCommonBuffer = nullptr;
}

bool CNETSocket::CreateJumboBuffer(CNETBuffer::BUFFER_STYLE eBufferStyle)
{
    if (m_pJumboBuffer == nullptr)
    {
        return m_pNetwork->CreateBuffer(eBufferStyle|CNETBuffer::BUFFERS_JUMBO, m_pJumboBuffer);
    }
    return true;
}

bool CNETSocket::DestroyJumboBuffer(void)
{
    if (m_pJumboBuffer != nullptr)
    {
        m_pNetwork->DestroyBuffer(m_pJumboBuffer);
        m_pJumboBuffer = nullptr;
    }
    return true;
}

void CNETSocket::DiscardJumboBuffer(void)
{
    m_pJumboBuffer = nullptr;
}

bool CNETSocket::CreateQueueBuffer(Int nLimit)
{
    UNREFERENCED_PARAMETER( nLimit );
    assert(m_nQueueSize == 0);
    DEV_DEBUG(TF(" NETSocket[%p] create queue buffer limit %d!"), this, nLimit);
    return true;
}

bool CNETSocket::DestroyQueueBuffer(void)
{
    CSyncLockScope scope(m_NSLock);
    if (m_QueueBuffer.GetSize() > 0)
    {
        for (PINDEX index = m_QueueBuffer.GetHeadIndex(); index != nullptr; )
        {
            SOCKET_BUFFER& Buffer = m_QueueBuffer.GetNext(index);
            m_pNetwork->DestroyBuffer(Buffer.pBuffer);

            if (Buffer.pSockAddr != nullptr)
            {
                m_pNetwork->DestroyAddr((PByte)Buffer.pSockAddr);
                Buffer.pSockAddr = nullptr;
            }
        }
        DEV_DEBUG(TF(" NETSocket[%p] destroy queue buffer size %d!"), this, m_QueueBuffer.GetSize());
        m_QueueBuffer.RemoveAll();
    }
    CAtomics::Exchange<UInt>((PUInt)&m_nQueueSize, 0);
    return true;
}

Int CNETSocket::AddBuffer(CNETBuffer*& pBuffer, const SOCKADDR_INET* pSockAddr)
{
    CSyncLockScope scope(m_NSLock);
    Int nIndex = m_QueueBuffer.GetSize();
    if (nIndex < m_pNetwork->GetAttr().nMaxSend)
    {
        PINDEX index = m_QueueBuffer.AddTail();
        if (index != nullptr)
        {
            SOCKET_BUFFER& Buffer = m_QueueBuffer.GetAt(index);
            Buffer.pBuffer = pBuffer;
            pBuffer = nullptr;
            if (pSockAddr != nullptr)
            {
                Buffer.pSockAddr = (PSOCKADDR_INET)m_pNetwork->CreateAddr();
                assert(Buffer.pSockAddr != nullptr);
                MM_SAFE::Cpy(Buffer.pSockAddr, sizeof(SOCKADDR_INET), pSockAddr, sizeof(SOCKADDR_INET));
            }
            CAtomics::Increment<UInt>((PUInt)&m_nQueueSize);
            return nIndex;
        }
    }
    DEV_DEBUG(TF(" NETSocket[%p] AddBuffer failed by limit %d <= %d or alloc memory failed"), this, nIndex, m_pNetwork->GetAttr().nMaxSend);
    return -1;
}

Int CNETSocket::RemoveBuffer(void)
{
    CSyncLockScope scope(m_NSLock);
    if (m_QueueBuffer.GetSize() > 0)
    {
        assert(m_nQueueSize > 0);
        CAtomics::Decrement<UInt>((PUInt)&m_nQueueSize);
        SOCKET_BUFFER& Buffer = m_QueueBuffer.GetHead();

        m_pNetwork->DestroyBuffer(Buffer.pBuffer);
        Buffer.pBuffer = nullptr;

        if (Buffer.pSockAddr != nullptr)
        {
            m_pNetwork->DestroyAddr((PByte)Buffer.pSockAddr);
        }
        m_QueueBuffer.RemoveHeadDirect();
        return m_QueueBuffer.GetSize();
    }
    return 0;
}
