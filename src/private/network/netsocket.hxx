// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_SOCKET_HXX__
#define __NETWORK_SOCKET_HXX__

#pragma once

#include "netbuffer.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETSocket
class CNetManager;
class NOVTABLE CNETSocket ABSTRACT : public CNETSocketBase
{
public:
    CNETSocket(CNetManager* pNetwork, CEventHandler* pEventHandler);
    virtual ~CNETSocket(void);

    virtual bool   Init(Int nFlag, PSOCKADDR_INET pSockAddr = nullptr) PURE;
    // 1. other thread invoke destroy
    // 2. worker thread handle return false
    // 3. return false if send list is not null when async close
    virtual bool   Exit(bool bAsync = false) PURE;
    //
    virtual bool   OnEventHandle(UInt uParam, CNETBuffer* pBuffer) PURE;
    virtual bool   OnEventFail(UInt uParam, CNETBuffer* pBuffer) PURE;
    //
    bool           Add(void);
    bool           Check(void);
    bool           Remove(void);

    bool           SetSendTimeout(Int nTime);
    bool           SetRecvTimeout(Int nTime);

    bool           SetKeepAlive(bool bEnable = true);
    bool           SetKeepAliveTime(UInt uTime);
private:
protected:
    bool           Create(Int nType, Int nProtocol);
    bool           Close(void);
    bool           Destroy(void);

    void           HandlerAddRef(void);
    void           HandlerRelease(void);
    //
    bool           SetOption(Int nFlag);
    bool           BindAddr(PSOCKADDR_INET pSockAddr);
    //
    bool           BindEvent(bool bAdd = true, UInt uEvent = NET_DEF_EVENT_DEFAULT);
    bool           UpdateEvent(UInt uEvent = NET_DEF_EVENT_DEFAULT);
    //
    bool           RecvBufferHandle(CNETBuffer*& pBuffer, size_t stBytes);
    bool           DetectPackComplete(CNETBuffer*& pBuffer, size_t stBuf);
    bool           CheckBuffer(CNETBuffer*& pBuffer, size_t stMax, bool bHandOver);
    bool           JumboBufferRecv(CNETBuffer*& pBuffer, size_t stStart, size_t stBuf);
    virtual bool   OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly) PURE;
    //
    bool           CreateCommonBuffer(CNETBuffer::BUFFER_STYLE eBufferStyle);
    bool           DestroyCommonBuffer(void);
    void           DiscardCommonBuffer(void);

    bool           CreateJumboBuffer(CNETBuffer::BUFFER_STYLE eBufferStyle);
    bool           DestroyJumboBuffer(void);
    void           DiscardJumboBuffer(void);
    //
    bool           CreateQueueBuffer(Int nLimit);
    bool           DestroyQueueBuffer(void);
    // return -1 --- error/full, return 0 --- only buffer in queue, return other --- queueing
    Int            AddBuffer(CNETBuffer*& pBuffer, const SOCKADDR_INET* pSockAddr = nullptr);
    // return 0 --- send queue is empty, return > 0 --- queueing
    Int            RemoveBuffer(void);
    //
    void           UpdateAddr(void);
protected:
    struct tagSOCKET_BUFFER : public MObject
    {
    public:
        tagSOCKET_BUFFER(void)
        : pBuffer(nullptr)
        , pSockAddr(nullptr)
        {
        }

        ~tagSOCKET_BUFFER(void)
        {
        }
    public:
        CNETBuffer*      pBuffer;
        PSOCKADDR_INET   pSockAddr;
    };
    typedef struct tagSOCKET_BUFFER  SOCKET_BUFFER, *PSOCKET_BUFFER;
    typedef CTList<SOCKET_BUFFER>    SOCKET_BUFFER_LIST;
protected:
    CNetManager*         m_pNetwork;      // network manager
    CEventHandler*       m_pEventHandler; // handler
    // recv buffer, accept buffer(tcp) or connect buffer(tcp)
    CNETBuffer*          m_pCommonBuffer;
    CNETBuffer*          m_pJumboBuffer;
    // send buffers or accept buffers(tcp)
    SOCKET_BUFFER_LIST   m_QueueBuffer;
    // sync
    CSyncLock            m_NSLock;
};

///////////////////////////////////////////////////////////////////
#include "netsocket.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_SOCKET_HXX__
