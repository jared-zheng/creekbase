// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"
#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETTickThread
CNETTickThread::CNETTickThread(void)
: m_pNetwork(nullptr)
, m_llAckTime(0)
, m_llTimeout(0)
, m_nAckTime(0)
, m_nAckTimeTick(0)
, m_nTimeout(0)
, m_nTimeoutTick(0)
{
}

CNETTickThread::~CNETTickThread(void)
{
}

void CNETTickThread::SetParam(CNetManager* pNetwork, Int nAck, Int nTimeout)
{
    m_pNetwork      = pNetwork;
    m_nAckTime      = nAck;
    m_nAckTimeTick  = ((nAck * NET_DEF_ONE_HALFH) >> 10) ;
    m_nAckTimeTick  = DEF::Maxmin<Int>(CNETTraits::ATTR_LMT_MIN_TIME, m_nAckTimeTick, CNETTraits::ATTR_DEF_TICK_TIME);
    m_nTimeout      = nTimeout;
    m_nTimeoutTick  = ((nTimeout * NET_DEF_ONE_HALFH) >> 10);
    m_nTimeoutTick  = DEF::Maxmin<Int>(CNETTraits::ATTR_LMT_MIN_TIME, m_nTimeoutTick, CNETTraits::ATTR_DEF_TICK_TIME);
    assert(m_pNetwork != nullptr);
    DEV_DEBUG(TF(" tick thread set param ack time %d[MS]---%d, timeout time %d[MS]---%d"), nAck, m_nAckTimeTick, nTimeout, m_nTimeoutTick);
    m_llAckTime = CPlatform::GetRunningTime();
    m_llTimeout = m_llAckTime;
}

bool CNETTickThread::Run(void)
{
    UpdateWaitStatus(false);
    if (m_pNetwork->CheckStatus() == false)
    {
        DEV_DEBUG(TF(" tick thread get network manager exit, stop thread"));
        return false;
    }

    LLong llCurrent = CPlatform::GetRunningTime() + CNETTraits::ATTR_DEF_BOUND;
    if (m_nAckTime > 0)
    {
        if (llCurrent >= (m_llAckTime + m_nAckTimeTick))
        {
            m_pNetwork->LiveAck(llCurrent, m_nAckTime);
            m_llAckTime = CPlatform::GetRunningTime();
        }
        llCurrent = CPlatform::GetRunningTime() + CNETTraits::ATTR_DEF_BOUND;
    }
    if (m_nTimeout > 0)
    {
        if (llCurrent >= (m_llTimeout + m_nTimeoutTick))
        {
            m_pNetwork->Timeout(llCurrent, m_nTimeout);
            m_llTimeout = CPlatform::GetRunningTime();
        }
    }
    UpdateWaitStatus();
    CPlatform::SleepEx(TIMET_S2MS, true); // ***windows only : apc queue handle
    return true;
}
