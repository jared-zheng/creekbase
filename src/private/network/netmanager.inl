// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_MANAGER_INL__
#define __NETWORK_MANAGER_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CNetManager
INLINE CNetManager::CNetManager(void)
: m_utStatus(MANAGERS_NONE)
, m_utRadix(0)
, m_pEventHandler(nullptr)
, m_inxSocket(nullptr)
, m_inxBuffer(nullptr)
, m_inxJumbo(nullptr)
, m_inxAddr(nullptr)
{
}

INLINE CNetManager::~CNetManager(void)
{
    Exit();
}

INLINE CNetManager::CNetManager(const CNetManager&)
: m_utStatus(MANAGERS_NONE)
, m_utRadix(0)
, m_pEventHandler(nullptr)
, m_inxSocket(nullptr)
, m_inxBuffer(nullptr)
, m_inxJumbo(nullptr)
, m_inxAddr(nullptr)
{
}

INLINE CNetManager& CNetManager::operator=(const CNetManager&)
{
    return (*this);
}

INLINE Int CNetManager::Release(void)
{
    Int nRef = m_Counter.Reset();
    if (nRef == 0)
    {
        MDELETE this;
        DEV_DUMP(TF("+ CNetwork component destroyed"));
    }
    return nRef;
}

INLINE UInt CNetManager::Command(PCXStr, uintptr_t)
{
    return (UInt)RET_OKAY;
}

INLINE UInt CNetManager::Update(void)
{
    return (UInt)RET_OKAY;
}

INLINE CNetManager::Socket CNetManager::Create(UShort usLocalPort, PCXStr pszLocalAddr, UInt uFlag)
{
    if (CheckStatus() && (m_pEventHandler != nullptr))
    {
        SOCKADDR_INET SockAddr;
        if (SetSocketAddr(usLocalPort, pszLocalAddr, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
        {
            CNETSocket* pSocket = CreateSocket(uFlag, m_pEventHandler, &SockAddr);
            if (pSocket != nullptr)
            {
                return (Socket)(pSocket->GetId());
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Create1 translate addr [%s]:%d failed"), pszLocalAddr, usLocalPort);
        }
    }
    else
    {
        DEV_DEBUG(TF(" Create1 status[%d] or eventhandler ptr[%p] failed"), m_utStatus, m_pEventHandler);
#endif
    }
    return 0;
}

INLINE CNetManager::Socket CNetManager::Create(CEventHandler& EventHandlerRef, UShort usLocalPort, PCXStr pszLocalAddr, UInt uFlag)
{
    if (CheckStatus())
    {
        SOCKADDR_INET SockAddr;
        if (SetSocketAddr(usLocalPort, pszLocalAddr, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
        {
            CNETSocket* pSocket = CreateSocket(uFlag, &EventHandlerRef, &SockAddr);
            if (pSocket != nullptr)
            {
                return (Socket)(pSocket->GetId());
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Create2 translate addr [%s]:%d failed"), pszLocalAddr, usLocalPort);
        }
    }
    else
    {
        DEV_DEBUG(TF(" Create2 status[%d] failed"), m_utStatus);
#endif
    }
    return 0;
}

INLINE CNetManager::Socket CNetManager::Create(const NET_ADDR& NetAddrRef, UInt uFlag)
{
    if (CheckStatus() && (m_pEventHandler != nullptr))
    {
        SOCKADDR_INET SockAddr;
        if (SetSocketAddr(NetAddrRef, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
        {
            CNETSocket* pSocket = CreateSocket(uFlag, m_pEventHandler, &SockAddr);
            if (pSocket != nullptr)
            {
                return (Socket)(pSocket->GetId());
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            if (NetAddrRef.usAttr == 0)
            {
                DEV_DEBUG(TF(" Create3 NET_ADDR is empty!"));
            }
            else if (NetAddrRef.usAttr & ATTR_IPV6)
            {
                const UShort* pIP = NetAddrRef.Addr.usAddr;
                DEV_DEBUG(TF(" Create3 translate NET_ADDR-[%X:%X:%X:%X:%X:%X:%X:%X]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], NetAddrRef.usPort);
            }
            else // ATTR_IPV4
            {
                const Byte* pIP = NetAddrRef.Addr.bAddr;
                DEV_DEBUG(TF(" Create3 translate NET_ADDR-[%d.%d.%d.%d]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], NetAddrRef.usPort);
            }
        }
    }
    else
    {
        DEV_DEBUG(TF(" Create3 status[%d] or eventhandler ptr[%p] failed"), m_utStatus, m_pEventHandler);
#endif
    }
    return 0;
}

INLINE CNetManager::Socket CNetManager::Create(CEventHandler& EventHandlerRef, const NET_ADDR& NetAddrRef, UInt uFlag)
{
    if (CheckStatus())
    {
        SOCKADDR_INET SockAddr;
        if (SetSocketAddr(NetAddrRef, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
        {
            CNETSocket* pSocket = CreateSocket(uFlag, &EventHandlerRef, &SockAddr);
            if (pSocket != nullptr)
            {
                return (Socket)(pSocket->GetId());
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            if (NetAddrRef.usAttr == 0)
            {
                DEV_DEBUG(TF(" Create4 NET_ADDR is empty!"));
            }
            else if (NetAddrRef.usAttr & ATTR_IPV6)
            {
                const UShort* pIP = NetAddrRef.Addr.usAddr;
                DEV_DEBUG(TF(" Create4 translate NET_ADDR-[%X:%X:%X:%X:%X:%X:%X:%X]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], NetAddrRef.usPort);
            }
            else // ATTR_IPV4
            {
                const Byte* pIP = NetAddrRef.Addr.bAddr;
                DEV_DEBUG(TF(" Create4 translate NET_ADDR-[%d.%d.%d.%d]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], NetAddrRef.usPort);
            }
        }
    }
    else
    {
        DEV_DEBUG(TF(" Create4 status[%d] failed"), m_utStatus);
#endif
    }
    return 0;
}

INLINE bool CNetManager::Destroy(Socket sSocket)
{
    if (CheckStatus() && (sSocket != 0))
    {
        CTNETSocketCheck<CNETSocket> Check(this);
        if (CheckSocket<CNETSocket>(sSocket, Check))
        {
            Check->SetCloseStatus();
            return true;
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" Destroy status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::Listen(Socket sSocket, Int nAccept)
{
    if (CheckStatus() && (sSocket != 0))
    {
        nAccept = DEF::Maxmin<Int>(nAccept, ATTR_LMT_MIN_BACKLOG, ATTR_LMT_MAX_BACKLOG);
        CTNETSocketCheck<CTCPSocket> Check(this);
        if (CheckSocket<CTCPSocket>(sSocket, Check))
        {
            return Listen(Check, nAccept);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" Listen status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::Connect(Socket sSocket, UShort usRemotePort, PCXStr pszRemoteAddr)
{
    if (CheckStatus() && (sSocket != 0))
    {
        CTNETSocketCheck<CTCPSocket> Check(this);
        if (CheckSocket<CTCPSocket>(sSocket, Check))
        {
            SOCKADDR_INET SockAddr;
            if (SetSocketAddr(usRemotePort, pszRemoteAddr, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
            {
                return Connect(Check, SockAddr);
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                DEV_DEBUG(TF(" Connect1 translate addr [%s]:%d failed"), pszRemoteAddr, usRemotePort);
            }
#endif
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" Connect1 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::Connect(Socket sSocket, const NET_ADDR& RemoteNetAddrRef)
{
    if (CheckStatus() && (sSocket != 0))
    {
        CTNETSocketCheck<CTCPSocket> Check(this);
        if (CheckSocket<CTCPSocket>(sSocket, Check))
        {
            SOCKADDR_INET SockAddr;
            if (SetSocketAddr(RemoteNetAddrRef, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
            {
                return Connect(Check, SockAddr);
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                if (RemoteNetAddrRef.usAttr == 0)
                {
                    DEV_DEBUG(TF(" Connect2 NET_ADDR is empty!"));
                }
                else if (RemoteNetAddrRef.usAttr & ATTR_IPV6)
                {
                    const UShort* pIP = RemoteNetAddrRef.Addr.usAddr;
                    DEV_DEBUG(TF(" Connect2 translate NET_ADDR-[%X:%X:%X:%X:%X:%X:%X:%X]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], RemoteNetAddrRef.usPort);
                }
                else // ATTR_IPV4
                {
                    const Byte* pIP = RemoteNetAddrRef.Addr.bAddr;
                    DEV_DEBUG(TF(" Connect2 translate NET_ADDR-[%d.%d.%d.%d]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], RemoteNetAddrRef.usPort);
                }
            }
#endif
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" Connect2 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::Send(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag, ULLong ullParam)
{
    if (CheckStatus())
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());

            if (sSocket != 0)
            {
                CTNETSocketCheck<CTCPSocket> Check(this);
                if (CheckSocket<CTCPSocket>(sSocket, Check))
                {
                    return Send(Check, pStream, nFlag, ullParam);
                }
            }
            else if (nFlag > SEND_BROADCAST)
            {
                return Send(nullptr, pStream, nFlag, ullParam);
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Send1 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" Send1 status[%d] failed"), m_utStatus);
#endif
    }
    return false;
}

INLINE bool CNetManager::Send(const CTArray<Socket>& aSocket, CStreamScopePtr& StreamPtr, Int nFlag, ULLong ullParam)
{
    if (CheckStatus())
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());
            return Send(aSocket, pStream, nFlag, ullParam);
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Send3 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" Send3 status[%d] failed"), m_utStatus);
#endif
    }
    return false;
}

INLINE bool CNetManager::SendCopy(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag, ULLong ullParam)
{
    if (CheckStatus())
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());
            CNETWriteStream  Stream;
            if (pStream->Tell() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxJumbo);
            }
            Stream.Write(pStream->GetBuf(), pStream->Tell());
            assert(Stream.Tell() == pStream->Tell());

            if (sSocket != 0)
            {
                CTNETSocketCheck<CTCPSocket> Check(this);
                if (CheckSocket<CTCPSocket>(sSocket, Check))
                {
                    return Send(Check, &Stream, nFlag, ullParam);
                }
            }
            else if (nFlag > SEND_BROADCAST)
            {
                return Send(nullptr, &Stream, nFlag, ullParam);
            }
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" SendCopy Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendCopy status[%d] failed"), m_utStatus);
#endif
    }
    return false;
}

INLINE bool CNetManager::Send(Socket sSocket, CEventBase& EventRef, Int nFlag, ULLong ullParam)
{
    if (CheckStatus())
    {
        if (EventRef.Length() <= (size_t)m_Attr.nMaxJumbo)
        {
            CNETWriteStream Stream;
            if (EventRef.Length() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, 0, (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, 0, (size_t)m_Attr.nMaxJumbo);
            }
            EventRef.Serialize(Stream);

            CStreamScopePtr StreamPtr(&Stream, false);
            return Send(sSocket, StreamPtr, nFlag, ullParam);
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Send2 EventRef data length failed[%d >? %d(or %d)]"), EventRef.Length(), m_Attr.nMaxBuffer, m_Attr.nMaxJumbo);
        }
    }
    else
    {
        DEV_DEBUG(TF(" Send2 status[%d] failed"), m_utStatus);
#endif
    }
    return false;
}

INLINE bool CNetManager::Send(const CTArray<Socket>& aSocket, CEventBase& EventRef, Int nFlag, ULLong ullParam)
{
    if (CheckStatus())
    {
        if (EventRef.Length() <= (size_t)m_Attr.nMaxJumbo)
        {
            CNETWriteStream Stream;
            if (EventRef.Length() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, 0, (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, 0, (size_t)m_Attr.nMaxJumbo);
            }
            EventRef.Serialize(Stream);

            CStreamScopePtr StreamPtr(&Stream, false);
            return Send(aSocket, StreamPtr, nFlag, ullParam);
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" Send4 EventRef data length failed[%d >? %d(or %d)]"), EventRef.Length(), m_Attr.nMaxBuffer, m_Attr.nMaxJumbo);
        }
    }
    else
    {
        DEV_DEBUG(TF(" Send4 status[%d] failed"), m_utStatus);
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());

            CTNETSocketCheck<CUDPSocket> Check(this);
            if (CheckSocket<CUDPSocket>(sSocket, Check))
            {
                SOCKADDR_INET SockAddr;
                if (SetSocketAddr(usRemotePort, pszRemoteAddr, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
                {
                    return SendTo(Check, pStream, SockAddr, nFlag);
                }
#ifdef __RUNTIME_DEBUG__
                else
                {
                    DEV_DEBUG(TF(" SendTo1 translate addr [%s]:%d failed"), pszRemoteAddr, usRemotePort);
                }
            }
            else
            {
                DEV_DEBUG(TF(" SendTo1 socket ptr[%d] is null or is not UDP"), Check.Get());
            }
        }
        else
        {
            DEV_DEBUG(TF(" SendTo1 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendTo1 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#else
            }
        }
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket, CStreamScopePtr&, const ARY_STRADDR&, Int)
{
    // udp multi-cast used special addr as[192.168.255.255]
    return false;
}

INLINE bool CNetManager::SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());
            CNETWriteStream  Stream;
            if (pStream->Tell() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxJumbo);
            }
            Stream.Write(pStream->GetBuf(), pStream->Tell());
            assert(Stream.Tell() == pStream->Tell());

            CTNETSocketCheck<CUDPSocket> Check(this);
            if (CheckSocket<CUDPSocket>(sSocket, Check))
            {
                SOCKADDR_INET SockAddr;
                if (SetSocketAddr(usRemotePort, pszRemoteAddr, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
                {
                    return SendTo(Check, &Stream, SockAddr, nFlag);
                }
#ifdef __RUNTIME_DEBUG__
                else
                {
                    DEV_DEBUG(TF(" SendToCopy1 translate addr [%s]:%d failed"), pszRemoteAddr, usRemotePort);
                }
            }
            else
            {
                DEV_DEBUG(TF(" SendToCopy1 socket ptr[%d] is null or is not UDP"), Check.Get());
            }
        }
        else
        {
            DEV_DEBUG(TF(" SendToCopy1 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendToCopy1 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#else
            }
        }
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());

            CTNETSocketCheck<CUDPSocket> Check(this);
            if (CheckSocket<CUDPSocket>(sSocket, Check))
            {
                SOCKADDR_INET SockAddr;
                if (SetSocketAddr(RemoteNetAddrRef, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
                {
                    return SendTo(Check, pStream, SockAddr, nFlag);
                }
#ifdef __RUNTIME_DEBUG__
                else
                {
                    if (RemoteNetAddrRef.usAttr == 0)
                    {
                        DEV_DEBUG(TF(" SendTo2 NET_ADDR is empty!"));
                    }
                    else if (RemoteNetAddrRef.usAttr & ATTR_IPV6)
                    {
                        const UShort* pIP = RemoteNetAddrRef.Addr.usAddr;
                        DEV_DEBUG(TF(" SendTo2 translate NET_ADDR-[%X:%X:%X:%X:%X:%X:%X:%X]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], RemoteNetAddrRef.usPort);
                    }
                    else // ATTR_IPV4
                    {
                        const Byte* pIP = RemoteNetAddrRef.Addr.bAddr;
                        DEV_DEBUG(TF(" SendTo2 translate NET_ADDR-[%d.%d.%d.%d]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], RemoteNetAddrRef.usPort);
                    }
                }
            }
            else
            {
                DEV_DEBUG(TF(" SendTo2 socket ptr[%d] is null or is not UDP"), Check.Get());
            }
        }
        else
        {
            DEV_DEBUG(TF(" SendTo2 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendTo2 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#else
            }
        }
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket, CStreamScopePtr&, const ARY_NETADDR&, Int)
{
    // udp multi-cast used special addr as[192.168.255.255]
    return false;
}

INLINE bool CNetManager::SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET) && StreamPtr->IsWrite())
        {
            CNETWriteStream* pStream = static_cast<CNETWriteStream*>(StreamPtr.Get());
            CNETWriteStream  Stream;
            if (pStream->Tell() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, pStream->GetDataOffset(), (size_t)m_Attr.nMaxJumbo);
            }
            Stream.Write(pStream->GetBuf(), pStream->Tell());
            assert(Stream.Tell() == pStream->Tell());

            CTNETSocketCheck<CUDPSocket> Check(this);
            if (CheckSocket<CUDPSocket>(sSocket, Check))
            {
                SOCKADDR_INET SockAddr;
                if (SetSocketAddr(RemoteNetAddrRef, SockAddr, (m_Attr.nAttrs & ATTR_IPV6)))
                {
                    return SendTo(Check, &Stream, SockAddr, nFlag);
                }
#ifdef __RUNTIME_DEBUG__
                else
                {
                    if (RemoteNetAddrRef.usAttr == 0)
                    {
                        DEV_DEBUG(TF(" SendToCopy2 NET_ADDR is empty!"));
                    }
                    else if (RemoteNetAddrRef.usAttr & ATTR_IPV6)
                    {
                        const UShort* pIP = RemoteNetAddrRef.Addr.usAddr;
                        DEV_DEBUG(TF(" SendToCopy2 translate NET_ADDR-[%X:%X:%X:%X:%X:%X:%X:%X]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], RemoteNetAddrRef.usPort);
                    }
                    else // ATTR_IPV4
                    {
                        const Byte* pIP = RemoteNetAddrRef.Addr.bAddr;
                        DEV_DEBUG(TF(" SendToCopy2 translate NET_ADDR-[%d.%d.%d.%d]:%d to addr failed"), pIP[0], pIP[1], pIP[2], pIP[3], RemoteNetAddrRef.usPort);
                    }
                }
            }
            else
            {
                DEV_DEBUG(TF(" SendToCopy2 socket ptr[%d] is null or is not UDP"), Check.Get());
            }
        }
        else
        {
            DEV_DEBUG(TF(" SendToCopy2 Stream mode error"));
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendToCopy2 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#else
            }
        }
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket sSocket, CEventBase& EventRef, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if (EventRef.Length() <= (size_t)m_Attr.nMaxJumbo)
        {
            CNETWriteStream Stream;
            if (EventRef.Length() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, 0, (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, 0, (size_t)m_Attr.nMaxJumbo);
            }
            EventRef.Serialize(Stream);
            CStreamScopePtr StreamPtr(&Stream, false);
            return SendTo(sSocket, StreamPtr, pszRemoteAddr, usRemotePort, nFlag);
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" SendTo3 EventRef data length failed[%d >? %d(or %d)]"), EventRef.Length(), m_Attr.nMaxBuffer, m_Attr.nMaxJumbo);
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendTo3 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket, CEventBase&, const ARY_STRADDR&, Int)
{
    // udp multi-cast used special addr as[192.168.255.255]
    return false;
}

INLINE bool CNetManager::SendTo(Socket sSocket, CEventBase& EventRef, const NET_ADDR& RemoteNetAddrRef, Int nFlag)
{
    if (CheckStatus() && (sSocket != 0))
    {
        if (EventRef.Length() <= (size_t)m_Attr.nMaxJumbo)
        {
            CNETWriteStream Stream;
            if (EventRef.Length() <= (size_t)m_Attr.nMaxBuffer)
            {
                Stream.Create(m_inxBuffer, (size_t)m_Attr.nBufferOffset, 0, (size_t)m_Attr.nMaxBuffer);
            }
            else
            {
                Stream.Create(m_inxJumbo, (size_t)m_Attr.nJumboOffset, 0, (size_t)m_Attr.nMaxJumbo);
            }
            EventRef.Serialize(Stream);
            CStreamScopePtr StreamPtr(&Stream, false);
            return SendTo(sSocket, StreamPtr, RemoteNetAddrRef, nFlag);
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_DEBUG(TF(" SendTo4 EventRef data length failed[%d >? %d(or %d)]"), EventRef.Length(), m_Attr.nMaxBuffer, m_Attr.nMaxJumbo);
        }
    }
    else
    {
        DEV_DEBUG(TF(" SendTo4 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
#endif
    }
    return false;
}

INLINE bool CNetManager::SendTo(Socket, CEventBase&, const ARY_NETADDR&, Int)
{
    // udp multi-cast used special addr as[192.168.255.255]
    return false;
}

INLINE bool CNetManager::GetAddr(Socket sSocket, CString& strAddr, UShort& usPort, bool bRemote)
{
    if (CheckStatus() && (sSocket != 0))
    {
        bool bRet = false;
        CTNETSocketCheck<CNETSocket> Check(this);
        if (CheckSocket<CNETSocket>(sSocket, Check))
        {
            bRet = GetSocketAddr(strAddr, usPort, Check->GetAddr(bRemote));
        }
        return bRet;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" GetAddr1 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::GetAddr(Socket sSocket, NET_ADDR& NetAddrRef, bool bRemote)
{
    if (CheckStatus() && (sSocket != 0))
    {
        bool bRet = false;
        CTNETSocketCheck<CNETSocket> Check(this);
        if (CheckSocket<CNETSocket>(sSocket, Check))
        {
            bRet = GetSocketAddr(NetAddrRef, Check->GetAddr(bRemote));
        }
        return bRet;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" GetAddr2 status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return false;
}

INLINE bool CNetManager::TranslateAddr(CString& strAddr, UShort& usPort, NET_ADDR& NetAddrRef, bool bString2Addr) const
{
    SOCKADDR_INET SockAddr;
    if (bString2Addr)
    {
        if (SetSocketAddr(usPort, *strAddr, SockAddr, (NetAddrRef.usAttr & ATTR_IPV6)))
        {
            return GetSocketAddr(NetAddrRef, SockAddr);
        }
    }
    else
    {
        if (SetSocketAddr(NetAddrRef, SockAddr, (NetAddrRef.usAttr & ATTR_IPV6)))
        {
            return GetSocketAddr(strAddr, usPort, SockAddr);
        }
    }
    return false;
}

INLINE bool CNetManager::TranslateAddr(PCXStr pszAddr, UShort usPort, NET_ADDR& NetAddrRef) const
{
    SOCKADDR_INET SockAddr;
    if (SetSocketAddr(usPort, pszAddr, SockAddr, (NetAddrRef.usAttr & ATTR_IPV6)))
    {
        return GetSocketAddr(NetAddrRef, SockAddr);
    }
    return false;
}

INLINE ULLong CNetManager::GetAttr(Socket sSocket, UInt uAttr)
{
    ULLong ullRet = 0;
    if (CheckStatus() && (sSocket != 0))
    {
        CTNETSocketCheck<CNETSocket> Check(this);
        if (CheckSocket<CNETSocket>(sSocket, Check))
        {
            switch (uAttr)
            {
            case SOCKET_LIVEDATA:
                {
                    ullRet = Check->GetLiveData();
                }
                break;
            case SOCKET_TCP:
                {
                    ullRet = Check->IsTCPGroup() ? TRUE : FALSE;
                    if (ullRet == TRUE)
                    {
                        ullRet = Check->IsBroadCast() ? SOCKET_TCP_BROADCAST : SOCKET_TCP;
                    }
                }
                break;
            case SOCKET_UDP:
                {
                    ullRet = Check->IsUDP() ? TRUE : FALSE;
                    if (ullRet == TRUE)
                    {
                        ullRet = Check->IsBroadCast() ? SOCKET_UDP_BROADCAST : SOCKET_UDP;
                    }
                }
                break;
            case SOCKET_SEND_EVENT:
                {
                    ullRet = Check->IsSendEvent() ? TRUE : FALSE;
                }
                break;
            case SOCKET_RECV_EVENT:
                {
                    ullRet = Check->IsRecvEvent() ? TRUE : FALSE;
                }
                break;
            case SOCKET_EXCLUDE_HEAD:
                {
                    ullRet = Check->IsExcludeHead() ? TRUE : FALSE;
                }
                break;
            case SOCKET_INCLUDE_HEAD:
                {
                    ullRet = Check->IsIncludeHead() ? TRUE : FALSE;
                }
                break;
            default:
                {
                }
            }
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" GetAttr status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return ullRet;
}

INLINE bool CNetManager::SetAttr(Socket sSocket, ULLong ullData, UInt uAttr)
{
    bool bRet = false;
    if (CheckStatus() && (sSocket != 0))
    {
        CTNETSocketCheck<CNETSocket> Check(this);
        if (CheckSocket<CNETSocket>(sSocket, Check))
        {
            switch (uAttr)
            {
            case SOCKET_LIVEDATA:
                {
                    Check->SetLiveData(ullData);
                    bRet = true;
                }
                break;
            case SOCKET_TCP_BROADCAST:
                {
                    bRet = Check->IsTCPGroup();
                    if (bRet)
                    {
                        Check->SetTraitsEx(CNETSocket::SOCKETT_EX_UNICAST, (ullData == FALSE));
                    }
                }
                break;
            case SOCKET_SEND_EVENT:
                {
                    bRet = Check->IsSendEvent();
                    Check->SetTraitsEx(CNETSocket::SOCKETT_EX_SEND, (ullData == TRUE));
                }
                break;
            case SOCKET_RECV_EVENT:
                {
                    bRet = Check->IsRecvEvent();
                    Check->SetTraitsEx(CNETSocket::SOCKETT_EX_RECV, (ullData == TRUE));
                }
                break;
            case SOCKET_EXCLUDE_HEAD:
                {
                    bRet = Check->IsExcludeHead();
                    Check->SetTraitsEx(CNETSocket::SOCKETT_EX_NOHEAD, (ullData == TRUE));
                }
                break;
            case SOCKET_INCLUDE_HEAD:
                {
                    bRet = Check->IsIncludeHead();
                    Check->SetTraitsEx(CNETSocket::SOCKETT_EX_HASHEAD, (ullData == TRUE));
                }
                break;
            case SOCKET_SEND_TIMEOUT:
                {
                    bRet = Check->SetSendTimeout((Int)ullData);
                }
                break;
            case SOCKET_RECV_TIMEOUT:
                {
                    bRet = Check->SetRecvTimeout((Int)ullData);
                }
                break;
            case SOCKET_TCP_KEEPALIVE:
                {
                    bRet = Check->IsTCPTraffic();
                    if (bRet)
                    {
                        bRet = Check->SetKeepAlive((ullData == TRUE));
                    }
                }
                break;
            case SOCKET_TCP_ALIVETIME:
                {
                    bRet = Check->IsTCPTraffic();
                    if (bRet)
                    {
                        bRet = Check->SetKeepAliveTime((UInt)ullData);
                    }
                }
                break;
            case SOCKET_MANUAL_TIMEOUT:
                {
                    bRet = Check->SetLiveTime((LLong)ullData);
                }
                break;
            default:
                {
                }
            }
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" SetAttr status[%d] or sSocket ptr[%p] failed"), m_utStatus, sSocket);
    }
#endif
    return bRet;
}

INLINE bool CNetManager::AllocBuffer(NET_PARAM& NetParam, bool bJumbo) const
{
    if (CheckStatus())
    {
        if (bJumbo == false)
        {
            NetParam.pCache = MCAlloc(m_inxBuffer);
            if (NetParam.pCache != nullptr)
            {
                NetParam.index   = m_inxBuffer;
                NetParam.pData   = NetParam.pCache + m_Attr.nBufferOffset;
                NetParam.nCache  = m_Attr.nMaxBuffer;
                NetParam.nSize   = m_Attr.nMaxBuffer;
                NetParam.nOffset = m_Attr.nBufferOffset;
                NetParam.nAttr   = (Int)ATTR_MAX_BUFFER;
            }
        }
        else
        {
            NetParam.pCache = MCAlloc(m_inxJumbo);
            if (NetParam.pCache != nullptr)
            {
                NetParam.index   = m_inxJumbo;
                NetParam.pData   = NetParam.pCache + m_Attr.nJumboOffset;
                NetParam.nCache  = m_Attr.nMaxJumbo;
                NetParam.nSize   = m_Attr.nMaxJumbo;
                NetParam.nOffset = m_Attr.nJumboOffset;
                NetParam.nAttr   = (Int)ATTR_MAX_JUMBOBUF;
            }
        }
        return (NetParam.pCache != nullptr);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" AllocBuffer NET_PARAM status[%d] failed"), m_utStatus);
    }
#endif
    return false;
}

INLINE bool CNetManager::AllocBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache, size_t stDataOffset) const
{
    if (CheckStatus())
    {
        if (pCache == nullptr)
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxBuffer, (size_t)m_Attr.nBufferOffset, stDataOffset, (size_t)m_Attr.nMaxBuffer);
        }
        else if (MCCheck(m_inxBuffer, pCache))
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxBuffer, (size_t)m_Attr.nBufferOffset, stDataOffset, (size_t)m_Attr.nMaxBuffer, pCache);
        }
        return (StreamScopePtrRef.Get() != nullptr);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" AllocBuffer status[%d] failed"), m_utStatus);
    }
#endif
    return false;
}

INLINE bool CNetManager::AllocJumboBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache, size_t stDataOffset) const
{
    if (CheckStatus())
    {
        if (pCache == nullptr)
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxJumbo, (size_t)m_Attr.nJumboOffset, stDataOffset, (size_t)m_Attr.nMaxJumbo);
        }
        else if (MCCheck(m_inxJumbo, pCache))
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxJumbo, (size_t)m_Attr.nJumboOffset, stDataOffset, (size_t)m_Attr.nMaxJumbo, pCache);
        }
        return (StreamScopePtrRef.Get() != nullptr);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" AllocJumboBuffer status[%d] failed"), m_utStatus);
    }
#endif
    return false;
}

INLINE bool CNetManager::ReuseBuffer(CStreamScopePtr& StreamScopePtrRef, PINDEX index, PByte pCache, size_t stDataOffset) const
{
    if (CheckStatus())
    {
        if ((index == m_inxBuffer) && MCCheck(m_inxBuffer, pCache))
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxBuffer, (size_t)m_Attr.nBufferOffset, stDataOffset, (size_t)m_Attr.nMaxBuffer, pCache);
        }
        else if ((index == m_inxJumbo) && MCCheck(m_inxJumbo, pCache))
        {
            StreamScopePtrRef = MNEW CNETWriteStream(m_inxJumbo, (size_t)m_Attr.nJumboOffset, stDataOffset, (size_t)m_Attr.nMaxJumbo, pCache);
        }
        return (StreamScopePtrRef.Get() != nullptr);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" ReuseBuffer status[%d] failed"), m_utStatus);
    }
#endif
    return false;
}

INLINE bool CNetManager::ReferBuffer(CStreamScopePtr& StreamScopePtrRef, CStreamScopePtr& StreamPtr) const
{
    if (CheckStatus())
    {
        if ((StreamPtr != nullptr) && (StreamPtr->GetMode() & CNETStream::STREAMM_EXT_NET))
        {
            CNETReadStream* pStream = MNEW CNETReadStream;
            if (pStream != nullptr)
            {
                pStream->Refer(*StreamPtr);
            }
            StreamScopePtrRef = pStream;
        }
        return (StreamScopePtrRef.Get() != nullptr);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" ReferBuffer status[%d] failed"), m_utStatus);
    }
#endif
    return false;
}

INLINE void CNetManager::Dump(NET_DUMP& Dump) const
{
    Dump = m_Dump;
}

INLINE void CNetManager::Attr(NET_ATTR& Attr) const
{
    Attr = m_Attr;
}

INLINE PByte CNetManager::CreateAddr(void) const
{
    return MCAlloc(m_inxAddr);
}

INLINE bool CNetManager::DestroyAddr(PByte pAddr) const
{
    return MCFree(m_inxAddr, pAddr);
}

INLINE PINDEX CNetManager::AddSocket(uintptr_t& utId, CNETSocket* pSocket)
{
    if (CheckStatus())
    {
        CSyncLockScope scope(m_NMLock);

        m_utRadix += RADIXC_ADD;
        if ((m_utRadix & RADIXC_HIGH) == RADIXC_HIGH)
        {
            m_utRadix &= RADIXC_LOW;
        }
        utId += (m_utRadix & RADIXC_LOW);
        utId &= RADIXC_LOW;
        utId += (m_utRadix & RADIXC_HIGH);

        DEV_DEBUG(TF(" #Add socket[%p - %p] to map"), pSocket, utId);
        return m_NetSocket.Add((Socket)utId, pSocket);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" AddSocket status[%d] failed"), m_utStatus);
    }
#endif
    return nullptr;
}

template <typename T>
INLINE bool CNetManager::CheckSocket(Socket sSocket, CTNETSocketCheck<T>& SocketCheckRef)
{
    CSyncLockScope scope(m_NMLock);
    MAP_NETSOCKET::PAIR* pPair = m_NetSocket.Find(sSocket);
    if ((pPair != nullptr) && (pPair->m_V->Check()))
    {
        //DEV_DEBUG(TF(" #Check socket[%p] in map is[%p]"), sSocket, pPair->m_V);
        SocketCheckRef = pPair->m_V;
        return true;
    }
    //DEV_DEBUG(TF(" #Check socket[%p] in map failed"), sSocket);
    return false;
}

INLINE bool CNetManager::RemoveSocket(PINDEX index)
{
    if (CheckStatus() && (index != nullptr))
    {
        CSyncLockScope scope(m_NMLock);
        if (m_NetSocket.Check(index))
        {
#ifdef __RUNTIME_DEBUG__
            MAP_NETSOCKET::PAIR* pPair = m_NetSocket.GetAt(index);
            //assert(pPair->m_K == (Socket)pPair->m_V->GetId());
            DEV_DEBUG(TF(" #Remove socket[%p - %p] from map"), pPair->m_V, pPair->m_K);
            return m_NetSocket.RemoveAt(index);
        }
        else
        {
            DEV_DEBUG(TF(" #Remove socket from map index invalid[%p-%d]"), index, m_utStatus);
        }
    }
    else
    {
        DEV_DEBUG(TF(" RemoveSocket status[%d] or PINDEX is null fail"), m_utStatus);
#else
            return m_NetSocket.RemoveAt(index);
        }
#endif
    }
    return (m_utStatus == MANAGERS_EXIT);
}

INLINE void CNetManager::RemoveAllSocket(void)
{
    assert(m_utStatus == MANAGERS_EXIT);
    DEV_DEBUG(TF(" #RemoveAll socket in map !"));
    CSyncLockScope scope(m_NMLock);
    DEV_DEBUG(TF(" #RemoveAll socket in map start"));
    PINDEX index = m_NetSocket.GetFirstIndex();
    while (index != nullptr)
    {
        CNETSocket* pSocket = (m_NetSocket.GetNext(index)->m_V);
#ifdef __RUNTIME_DEBUG__
        DEV_DUMP(TF(" CNetwork exit stop socket %p-%p"), pSocket, pSocket->GetId());
#endif
        pSocket->SetStatus(CNETSocket::SOCKETS_EXIT); // no close event
        pSocket->Exit();
        FreeSocket(pSocket);
    }
    m_NetSocket.RemoveAll();
}

INLINE bool CNetManager::CheckStatus(void) const
{
    return (m_utStatus == MANAGERS_OKAY);
}

INLINE CNetEventHandle& CNetManager::GetEvent(void)
{
    return (m_hNetEvent);
}

INLINE CNETTraits::CNETPackBuildPtr& CNetManager::GetPackBuild(void)
{
    return (m_PackBuildPtr);
}

INLINE PINDEX CNetManager::GetSocketIndex(void) const
{
    return (m_inxSocket);
}

INLINE PINDEX CNetManager::GetBufferIndex(void) const
{
    return (m_inxBuffer);
}

INLINE PINDEX CNetManager::GetJumboIndex(void) const
{
    return (m_inxJumbo);
}

INLINE PINDEX CNetManager::GetAddrIndex(void) const
{
    return (m_inxAddr);
}

INLINE CNETTraits::NET_DUMP& CNetManager::GetDump(void)
{
    return m_Dump;
}

INLINE CNETTraits::NET_ATTR& CNetManager::GetAttr(void)
{
    return m_Attr;
}

template <typename T>
INLINE T* CNetManager::AllocSocket(CEventHandler* pEventHandler)
{
    return (new(MCAlloc(m_inxSocket)) T(this, pEventHandler)); // from cache
}

INLINE void CNetManager::FreeSocket(CNETSocket* pSocket)
{
    delete pSocket;
    MCFree(m_inxSocket, (PByte)pSocket); // from cache
}

INLINE void CNetManager::AdjustDump(void)
{
    m_Dump.Reset();
}

INLINE void CNetManager::AdjustAttr(const NET_ATTR& Attr)
{
    m_Attr = Attr;
    // 1. ipv4/ipv6, work thread
    if (m_Attr.nAttrs & ATTR_THREAD)
    {
        m_Attr.nThread = (Int)CPlatform::CheckThread((UInt)m_Attr.nThread, ATTR_DEF_TIMES);
    }
    else
    {
        m_Attr.nThread = (Int)CPlatform::CheckThread((UInt)UINT_MAX);
    }
    // 2. ack/time
    if (m_Attr.nAttrs & (ATTR_ACK_DETECT|ATTR_TIMEOUT_DETECT|ATTR_ACK_TIME|ATTR_TIMEOUT_TIME))
    {
        m_Attr.nAckTime = 0;
        if ((m_Attr.nAttrs & ATTR_ACK_TIME) == ATTR_ACK_TIME)
        {
            m_Attr.nAttrs  |= ATTR_ACK_DETECT;
            m_Attr.nAckTime = DEF::Maxmin<Int>(ATTR_LMT_MIN_TIME, Attr.nAckTime, ATTR_LMT_MAX_TIME);
        }
        else if (m_Attr.nAttrs & ATTR_ACK_DETECT)
        {
            m_Attr.nAckTime = ATTR_DEF_ACK_TIME;
        }

        m_Attr.nTimeout = 0;
        if ((m_Attr.nAttrs & ATTR_TIMEOUT_TIME) == ATTR_TIMEOUT_TIME)
        {
            m_Attr.nAttrs  |= ATTR_TIMEOUT_DETECT;
            m_Attr.nTimeout = DEF::Maxmin<Int>(ATTR_LMT_MIN_TIME, Attr.nTimeout, ATTR_LMT_MAX_TIME);
        }
        else if (m_Attr.nAttrs & ATTR_TIMEOUT_DETECT)
        {
            m_Attr.nTimeout = ATTR_DEF_TIMEOUT_TIME;
        }
    }
    // 3. buffer
    if (m_Attr.nAttrs & ATTR_MAX_BUFFER)
    {
        m_Attr.nMaxBuffer  = DEF::Maxmin<Int>(ATTR_LMT_MIN_BUFFER, Attr.nMaxBuffer, ATTR_LMT_MAX_BUFFER);
        m_Attr.nMaxBuffer  = DEF::Align<Int>(m_Attr.nMaxBuffer, ATTR_LMT_MIN_BUFFER);
    }
    else
    {
        m_Attr.nMaxBuffer  = ATTR_DEF_MAX_BUFFER;
    }
    // 4. jumbo buffer
    if (m_Attr.nAttrs & ATTR_MAX_JUMBOBUF)
    {
        m_Attr.nMaxJumbo  = DEF::Maxmin<Int>(ATTR_LMT_MIN_JUMBOBUF, Attr.nMaxJumbo, ATTR_LMT_MAX_JUMBOBUF);
        m_Attr.nMaxJumbo  = DEF::Max<Int>(m_Attr.nMaxJumbo, (m_Attr.nMaxBuffer * ATTR_DEF_TIMES));
        m_Attr.nMaxJumbo  = DEF::Align<Int>(m_Attr.nMaxJumbo, ATTR_LMT_MIN_JUMBOBUF);
    }
    else
    {
        m_Attr.nMaxJumbo  = DEF::Max<Int>(ATTR_DEF_MAX_JUMBOBUF, (m_Attr.nMaxBuffer * ATTR_DEF_TIMES));
    }
    m_Attr.nMaxJumbo  -= (Int)m_PackBuildPtr->OffsetSize();
    m_Attr.nMaxBuffer -= (Int)m_PackBuildPtr->OffsetSize();
    // send
    if (m_Attr.nAttrs & ATTR_MAX_SEND)
    {
        m_Attr.nMaxSend = DEF::Maxmin<Int>(ATTR_LMT_MIN_QUEUE, Attr.nMaxSend, ATTR_LMT_MAX_QUEUE);
        m_Attr.nMaxSend = DEF::Align<Int>(m_Attr.nMaxSend, ATTR_LMT_MIN_QUEUE);
    }
    else
    {
        m_Attr.nMaxSend = ATTR_DEF_MAX_SEND;
    }
    // event
    if (m_Attr.nAttrs & ATTR_MAX_EVENT)
    {
        m_Attr.nMaxEvent = DEF::Maxmin<Int>(ATTR_LMT_MIN_EVENT, Attr.nMaxEvent, ATTR_LMT_MAX_EVENT);
        m_Attr.nMaxEvent = DEF::Align<Int>(m_Attr.nMaxEvent, ATTR_LMT_MIN_EVENT);
    }
    else
    {
        m_Attr.nMaxEvent = ATTR_DEF_MAX_EVENT;
    }
}

///////////////////////////////////////////////////////////////////
// CTNETSocketCheck
template <typename T>
INLINE CTNETSocketCheck<T>::CTNETSocketCheck(CNetManager* pNetwork, T* pNETSocket)
: m_pNetwork(pNetwork)
, m_pNETSocket(pNETSocket)
, m_bAsync(false)
{
    if (m_pNETSocket != nullptr)
    {
        m_pNETSocket->SetCheckStatus(true);
    }
}

template <typename T>
INLINE CTNETSocketCheck<T>::~CTNETSocketCheck(void)
{
    if (m_pNETSocket != nullptr)
    {
        if (m_pNETSocket->SetCheckStatus(false))
        {
            m_pNetwork->DestroySocket(m_pNETSocket, m_bAsync);
        }
    }
    m_pNETSocket = nullptr;
    m_pNetwork   = nullptr;
}

template <typename T>
INLINE CTNETSocketCheck<T>& CTNETSocketCheck<T>::operator=(T* pNETSocket)
{
    if (m_pNETSocket == nullptr)
    {
        m_pNETSocket = pNETSocket;
        if (m_pNETSocket != nullptr)
        {
            m_pNETSocket->SetCheckStatus(true);
        }
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTNETSocketCheck<T>& CTNETSocketCheck<T>::operator=(X* pNETSocket)
{
    if (m_pNETSocket == nullptr)
    {
        m_pNETSocket = static_cast<T*>(pNETSocket);
        if (m_pNETSocket != nullptr)
        {
            m_pNETSocket->SetCheckStatus(true);
        }
    }
    return (*this);
}

template <typename T>
INLINE CTNETSocketCheck<T>::operator T*(void) const
{
    return m_pNETSocket;
}

template <typename T>
INLINE T& CTNETSocketCheck<T>::operator*(void) const
{
    return (*m_pNETSocket);
}

template <typename T>
INLINE T* CTNETSocketCheck<T>::operator->(void) const
{
    return m_pNETSocket;
}

template <typename T>
INLINE T* CTNETSocketCheck<T>::Get(void) const
{
    return m_pNETSocket;
}

template <typename T>
INLINE void CTNETSocketCheck<T>::SetAsync(bool bAsync)
{
    m_bAsync = bAsync;
}

///////////////////////////////////////////////////////////////////
// CNETStream
INLINE CNETStream::CNETStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache, Int nMode)
: CBufStreamBase(nMode|STREAMM_EXT_NET)
, m_index(nullptr)
, m_stSize(0)
, m_stPos(0)
, m_stOffset(0)
, m_pData(nullptr)
, m_pCache(nullptr)
{
    if (index != nullptr)
    {
        Create(index, stOffset, stDataOffset, stSize, pCache);
    }
}

INLINE CNETStream::~CNETStream(void)
{
    Close();
}

INLINE size_t CNETStream::Tell(void) const
{
    return (m_stPos);
}

INLINE size_t CNETStream::Size(void) const
{
    return (m_stSize);
}

INLINE size_t CNETStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CNETStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE void CNETStream::Close(void)
{
    if (m_pCache != nullptr)
    {
        MCFree(m_index, m_pCache);
        m_pCache = nullptr;
    }
    m_pData    = nullptr;
    m_stOffset = 0;
    m_stPos    = 0;
    m_stSize   = 0;
    m_index    = nullptr;
}

INLINE PByte CNETStream::GetBuf(size_t stPos)
{
    if (stPos < m_stSize)
    {
        return (PByte)(m_pData + stPos);
    }
    return nullptr;
}

INLINE const PByte CNETStream::GetBuf(size_t stPos) const
{
    if (stPos < m_stSize)
    {
        return (const PByte)(m_pData + stPos);
    }
    return nullptr;
}

INLINE PByte CNETStream::GetBufPos(void)
{
    return (PByte)(m_pData + m_stPos);
}

INLINE const PByte CNETStream::GetBufPos(void) const
{
    return (PByte)(m_pData + m_stPos);
}

INLINE bool CNETStream::Create(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache)
{
    if (m_pCache == nullptr)
    {
        assert(m_index    == nullptr);
        assert(m_stSize   == 0);
        assert(m_stPos    == 0);
        assert(m_stOffset == 0);
        assert(m_pData    == nullptr);
        assert(stSize > stDataOffset);

        if (pCache == nullptr)
        {
            m_pCache = MCAlloc(index);
        }
        else
        {
            m_pCache = pCache;
        }
        if (m_pCache != nullptr)
        {
            m_pData    = (m_pCache + stOffset + stDataOffset);
            m_stSize   = (stSize - stDataOffset);
            m_stOffset = stDataOffset;
            m_index    = index;
            return true;
        }
    }
    return false;
}

INLINE PByte CNETStream::GetCache(void)
{
    SetError();
    PByte pCache = m_pCache;
    m_pCache   = nullptr;
    m_pData    = nullptr;
    //m_stOffset = 0;
    m_stPos    = 0;
    m_stSize   = 0;
    m_index    = nullptr;
    return (pCache);
}

INLINE PINDEX CNETStream::GetIndex(void) const
{
    return (m_index);
}

INLINE size_t CNETStream::GetDataOffset(void) const
{
    return (m_stOffset);
}

///////////////////////////////////////////////////////////////////
// CNETReadStream
INLINE CNETReadStream::CNETReadStream(void)
: CNETStream(nullptr, 0, 0, 0, nullptr, STREAMM_READ)
{
}

INLINE CNETReadStream::CNETReadStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache)
: CNETStream(index, stOffset, stDataOffset, stSize, pCache, STREAMM_READ)
{
}

INLINE CNETReadStream::~CNETReadStream(void)
{
}

INLINE size_t CNETReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    if (IsEnd() || (m_pData == nullptr))
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy(pV, (size_t)stLenBytes, (m_pData + m_stPos), (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE bool CNETReadStream::Refer(CStream& aSrc)
{
    if (aSrc.GetMode() & CNETStream::STREAMM_EXT_NET)
    {
        Close();

        CNETStream* pStream = static_cast<CNETStream*>(&aSrc);
        m_index = pStream->GetIndex();
        m_pData = pStream->GetBuf();
        if (aSrc.IsRead())
        {
            m_stSize = pStream->Size();
        }
        else
        {
            assert(aSrc.IsWrite());
            m_stSize = pStream->Tell();
        }
        if (pStream->IsByteSwap())
        {
            SetByteSwap(true);
        }
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CNETWriteStream
INLINE CNETWriteStream::CNETWriteStream(void)
: CNETStream(nullptr, 0, 0, 0, nullptr, STREAMM_WRITE)
{
}

INLINE CNETWriteStream::CNETWriteStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pData)
: CNETStream(index, stOffset, stDataOffset, stSize, pData, STREAMM_WRITE)
{
}

INLINE CNETWriteStream::~CNETWriteStream(void)
{
}

INLINE size_t CNETWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    if ((m_stSize == 0) || (m_pData == nullptr))
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy((m_pData + m_stPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

#endif // __NETWORK_MANAGER_INL__
