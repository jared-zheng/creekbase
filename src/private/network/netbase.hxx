// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_BASE_HXX__
#define __NETWORK_BASE_HXX__

#pragma once

#include "nettraits.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETPackBuildDef
class CNETPackBuildDef : public CNETTraits::CNETPackBuild
{
public:
    enum PACK_HEAD_CONST
    {
        PACKH_POWER = 0x0000000C,
        PACKH_SHIFT = 0x00000014,
        PACKH_RADIX = 0x00000FFF, //
        PACKH_FLAG  = 0x00025243, // ASCII RC
        PACKH_MASKL = 0x000FFFFF,
        PACKH_MASKH = 0xFFF00000,
    };

    typedef struct tagPACK_HEAD
    {
    public:
        void    Build(size_t stSize);
        bool    Check(void);
    public:
        UInt    uFlag;
        UInt    uSize;
    }PACK_HEAD, *PPACK_HEAD;
    // sizeof(PACK_HEAD) == ATTR_DEF_PACK_HEAD
public:
    CNETPackBuildDef(void);
    virtual ~CNETPackBuildDef(void);

    // send package head ptr, package data size
    // return package all size, include package head
    virtual size_t Build(PByte pPack, size_t stSize) OVERRIDE;
    // check recv package valid check
    // return package all size, include package head
    // return 0 when check fail
    virtual size_t Check(PByte pPack, size_t stSize) OVERRIDE;
    // package head size
    virtual size_t HeadSize(void) const OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CNETBufferBase
class CNETBufferBase : public CTNETBufferTraits<CNETBufferBase>
{
public:
    bool     IsValid(void) const;
    bool     IsSend(void) const;
    bool     IsRecv(void) const;
    bool     IsAccept(void) const;
    bool     IsConnect(void) const;
    bool     IsBroadCast(void) const;
    bool     IsJumbo(void) const;
    bool     IsRIO(void) const;
    bool     IsClose(void) const;
    size_t   GetStyle(void) const;

    size_t   GetSize(void) const;
    PByte    GetData(size_t stStart = 0);

    size_t   GetBufSize(size_t stStart = 0);
    size_t   GetBufUsed(void);
    PByte    GetBufData(size_t stStart = 0);
    PNETBUF  GetBuf(void);

    bool     BlockBufData(void);

    bool     SetBufData(size_t stStart);
    bool     SetBuf(size_t stStart, size_t stSize);
    bool     CopyBuf(PByte pData, size_t stSize);

    bool     Pack(size_t stSize, size_t stOffset, CNETTraits::CNETPackBuildPtr& BuildPtr);
    bool     Unpack(PByte& pData, size_t stStart, size_t& stSize, CNETTraits::CNETPackBuildPtr& BuildPtr);
protected:
    CNETBufferBase(size_t stSize, size_t stStyle = BUFFERS_SEND);
    ~CNETBufferBase(void);
private:
    CNETBufferBase(const CNETBufferBase&);
    CNETBufferBase& operator=(const CNETBufferBase&);
protected:
    size_t   m_stStyle;    // send/recv, broadcast + shared, Memory alignment REQUIREMENTS
    size_t   m_stSize;
    PByte    m_pData;
    NETBUF   m_Buf;
};

///////////////////////////////////////////////////////////////////
// CTNETBufferBroadCast : delete buf ptr verify by IsBroadCast
template <typename T>
class CTNETBufferBroadCast : public T
{
    typedef CTNETBufferBroadCast<T> TBroadCast;
public:
    bool Continue(void);
    bool Finish(UInt uBroadCast);
protected:
    explicit CTNETBufferBroadCast(size_t stSize, size_t stStyle); // broadcast type is auto add
    ~CTNETBufferBroadCast(void);
private:
    UInt   m_uBroadCast;   // broadcast buffer broadcast count
    UInt   m_uFinished;    // broadcast buffer finished  count
};

///////////////////////////////////////////////////////////////////
// CNETSocketBase
class NOVTABLE CNETSocketBase ABSTRACT : public CTNETSocketTraits<CNETSocketBase>
{
public:
    CNETSocketBase(void);
    virtual ~CNETSocketBase(void);

    uintptr_t   GetId(void) const;
    SOCKET      GetSocket(void) const;
    // traits
    bool        IsNone(void) const;
    bool        IsUDP(void) const;
    bool        IsTCP(void) const;
    bool        IsTCPListen(void) const;
    bool        IsTCPConnect(void) const;
    bool        IsTCPAccept(void) const;
    bool        IsTCPGroup(void) const;
    bool        IsTCPValid(void) const;
    bool        IsTCPTraffic(void) const;

    Int         GetTraits(void) const;
    void        SetTraits(Int nTraits);

    bool        IsBroadCast(void) const;
    bool        IsJumboRecv(void) const;
    bool        IsSendEvent(void) const;
    bool        IsRecvEvent(void) const;
    bool        IsExcludeHead(void) const;
    bool        IsIncludeHead(void) const;
    bool        IsKeepAlive(void) const;
    bool        IsSetTime(void) const;

    Int         GetTraitsEx(bool bDerive = false) const;
    void        SetTraitsEx(Int nTraitsEx, bool bAdd);

    bool        IsInitType(void) const;
    bool        IsReadyType(void) const;
    bool        IsAckType(void) const;
    bool        IsLiveType(void) const;
    bool        IsTrafficType(void) const;
    Int         GetType(void) const;
    void        SetType(Int nType);

    bool        IsHandleStatus(void);
    bool        IsCloseStatus(void);
    void        SetHandleStatus(bool bHandle);
    void        SetCloseStatus(void);

    bool        IsQueueStatus(void);
    bool        IsPendStatus(void);
    bool        IsCheckStatus(void);
    bool        IsAsyncStatus(void);
    bool        IsExitStatus(void);

    bool        SetQueueStatus(bool bQueue);
    bool        SetPendStatus(bool bPend);
    bool        SetCheckStatus(bool bCheck);

    bool        IsCloseEnable(void);
    Int         GetStatus(void);
    void        SetStatus(Int nStatus);

    bool        IsBroadCastEnable(Int nTraits);

    ULLong      GetLiveData(void);
    void        SetLiveData(ULLong ullLiveData);
    LLong       GetLiveTime(void) const;
    void        SetLiveTime(void);
    bool        SetLiveTime(LLong llSetTime);
    bool        CheckLiveTime(LLong llCurrent, Int nTime) const;
    //
    const SOCKADDR_INET&  GetAddr(bool bRemote) const;
private:
    CNETSocketBase(const CNETSocketBase&);
    CNETSocketBase& operator=(const CNETSocketBase&);
protected:
    PINDEX          m_Index;
    uintptr_t       m_utId;
    SOCKET          m_hSocket;
    //
    Int             m_nTraits;
    Int             m_nType;
    Int             m_nStatus;
    // send buffers or accept buffers(tcp) queue
    Int             m_nQueueSize;
    // live data & last update time, running time
    ULLong          m_ullLiveData;
    LLong           m_llLiveTime;
    // remote/local addr
    SOCKADDR_INET   m_adrRemote;
    SOCKADDR_INET   m_adrLocal;
};

///////////////////////////////////////////////////////////////////
#include "netbase.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_BASE_HXX__
