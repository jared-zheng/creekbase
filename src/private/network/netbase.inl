// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_BASE_INL__
#define __NETWORK_BASE_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CNETPackBuildDef::tagPACK_HEAD
INLINE void CNETPackBuildDef::tagPACK_HEAD::Build(size_t stSize)
{
    uSize  = (UInt)(stSize + CNETTraits::ATTR_DEF_PACK_HEAD);
    uFlag  = PACKH_FLAG + ((UInt)stSize >> PACKH_POWER);
    uFlag += (((UInt)stSize & PACKH_RADIX) << PACKH_SHIFT);
}

INLINE bool CNETPackBuildDef::tagPACK_HEAD::Check(void)
{
    return ( ( ((uFlag & PACKH_MASKH) >> PACKH_SHIFT) + (((uFlag & PACKH_MASKL) - PACKH_FLAG) << PACKH_POWER) ) == (uSize - CNETTraits::ATTR_DEF_PACK_HEAD) );
}

///////////////////////////////////////////////////////////////////
// CNETPackBuildDef
INLINE CNETPackBuildDef::CNETPackBuildDef(void)
{
    DEV_DEBUG(TF(" Default NETPackBuild Created"));
}

INLINE CNETPackBuildDef::~CNETPackBuildDef(void)
{
    DEV_DEBUG(TF(" Default NETPackBuild Destroyed"));
}

INLINE size_t CNETPackBuildDef::Build(PByte pPack, size_t stSize)
{
    PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
    pHead->Build(stSize);
    return (size_t)(pHead->uSize);
}

INLINE size_t CNETPackBuildDef::Check(PByte pPack, size_t)
{
    PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
    if (pHead->Check())
    {
        return (size_t)(pHead->uSize);
    }
    return 0;
}

INLINE size_t CNETPackBuildDef::HeadSize(void) const
{
    return (size_t)(CNETTraits::ATTR_DEF_PACK_HEAD);
}

///////////////////////////////////////////////////////////////////
// CNETBufferBase
INLINE CNETBufferBase::CNETBufferBase(size_t stSize, size_t stStyle)
: m_stStyle(stStyle)
, m_stSize(stSize)
, m_pData(nullptr)
{
}

INLINE CNETBufferBase::~CNETBufferBase(void)
{
}

INLINE CNETBufferBase::CNETBufferBase(const CNETBufferBase&)
: m_stStyle(BUFFERS_NONE)
, m_stSize(0)
, m_pData(nullptr)
{
}

INLINE CNETBufferBase& CNETBufferBase::operator=(const CNETBufferBase&)
{
    return (*this);
}

INLINE bool CNETBufferBase::IsValid(void) const
{
    return (m_pData != nullptr);
}

INLINE bool CNETBufferBase::IsSend(void) const
{
    return ((m_stStyle & BUFFERS_SEND) != 0);
}

INLINE bool CNETBufferBase::IsRecv(void) const
{
    return ((m_stStyle & BUFFERS_RECV) != 0);
}

INLINE bool CNETBufferBase::IsAccept(void) const
{
    return ((m_stStyle & BUFFERS_ACCEPT) != 0);
}

INLINE bool CNETBufferBase::IsConnect(void) const
{
    return ((m_stStyle & BUFFERS_CONNECT) != 0);
}

INLINE bool CNETBufferBase::IsBroadCast(void) const
{
    return ((m_stStyle & BUFFERS_BROADCAST) != 0);
}

INLINE bool CNETBufferBase::IsJumbo(void) const
{
    return ((m_stStyle & BUFFERS_JUMBO) != 0);
}

INLINE bool CNETBufferBase::IsRIO(void) const
{
    return ((m_stStyle & BUFFERS_RIO) != 0);
}

INLINE bool CNETBufferBase::IsClose(void) const
{
    return ((m_stStyle & BUFFERS_CLOSE) != 0);
}

INLINE size_t CNETBufferBase::GetStyle(void) const
{
    return m_stStyle;
}

INLINE size_t CNETBufferBase::GetSize(void) const
{
    return m_stSize;
}

INLINE PByte CNETBufferBase::GetData(size_t stStart)
{
    assert(stStart <= m_stSize);
    if (stStart < m_stSize)
    {
        return (m_pData + stStart);
    }
    return nullptr;
}

INLINE size_t CNETBufferBase::GetBufSize(size_t stStart)
{
    assert((size_t)m_Buf.len <= m_stSize);
    assert(stStart <= (size_t)m_Buf.len);
    if (stStart < (size_t)m_Buf.len)
    {
        return ((size_t)m_Buf.len - stStart);
    }
    return 0;
}

INLINE size_t CNETBufferBase::GetBufUsed(void)
{
    assert((size_t)m_Buf.len <= m_stSize);
    if ((size_t)m_Buf.len <= m_stSize)
    {
        return (m_stSize - (size_t)m_Buf.len);
    }
    return 0;
}

INLINE PByte CNETBufferBase::GetBufData(size_t stStart)
{
    assert((size_t)m_Buf.len <= m_stSize);
    assert(stStart <= (size_t)m_Buf.len);
    if (stStart < (size_t)m_Buf.len)
    {
        return (PByte)(m_Buf.buf + stStart);
    }
    return nullptr;
}

INLINE PNETBUF CNETBufferBase::GetBuf(void)
{
    return (&m_Buf);
}

INLINE bool CNETBufferBase::BlockBufData(void)
{
    if (IsValid())
    {
        return ((PByte)m_Buf.buf > m_pData);
    }
    return false;
}

INLINE bool CNETBufferBase::SetBufData(size_t stStart)
{
    if (IsValid())
    {
        if (stStart <= (size_t)m_Buf.len)
        {
            m_Buf.len -= (BufSize)(stStart);
            m_Buf.buf += stStart;
        }
        return true;
    }
    return false;
}

INLINE bool CNETBufferBase::SetBuf(size_t stStart, size_t stSize)
{
    if (IsValid())
    {
        if (stSize == 0)
        {
            m_Buf.len = (BufSize)m_stSize;
            m_Buf.buf = (BufAddr)m_pData;
        }
        else if ((stStart + stSize) <= m_stSize)
        {
            if (stStart > 0)
            {
                DEV_DEBUG(TF("CNETBufferBase SetBuf move start at %d, size is %d,  buffer-size=%d"), stStart, stSize, m_stSize);
                MM_SAFE::Mov(m_pData, m_stSize, (m_pData + stStart), stSize);
            }
#ifdef __RUNTIME_DEBUG__
            else if (stSize > 0)
            {
                DEV_DEBUG(TF("CNETBufferBase SetBuf remain size is %d,  buffer-size=%d"), stSize, (m_stSize - stSize));
            }
#endif
            m_Buf.len = (BufSize)(m_stSize - stSize);
            m_Buf.buf = (BufAddr)(m_pData  + stSize);
        }
        return true;
    }
    return false;
}

INLINE bool CNETBufferBase::CopyBuf(PByte pData, size_t stSize)
{
    assert(pData != nullptr);
    if (IsValid())
    {
        DEV_DEBUG(TF("CNETBufferBase CopyBuf copy size is %d,  buffer-size=%d"), stSize, m_stSize);
        assert(stSize < m_stSize);
        MM_SAFE::Cpy(m_pData, m_stSize, pData, stSize);

        m_Buf.len = (BufSize)(m_stSize - stSize);
        m_Buf.buf = (BufAddr)(m_pData  + stSize);
        return true;
    }
    return false;
}

INLINE bool CNETBufferBase::Pack(size_t stSize, size_t stOffset, CNETTraits::CNETPackBuildPtr& BuildPtr)
{
    assert(IsSend());
    assert(stSize > 0);
    assert(stSize <= (m_stSize - BuildPtr->OffsetSize() - stOffset));

    if (stOffset == 0)
    {
        m_Buf.len = (BufSize)(BuildPtr->Build(m_pData, stSize));
    }
    else
    {
        m_Buf.len = (BufSize)(stSize);
        m_Buf.buf = (BufAddr)(m_pData + BuildPtr->OffsetSize() + stOffset);
        DEV_DEBUG(TF("CNETBufferBase Pack offset %d + %d,  buffer-size=%d"), BuildPtr->OffsetSize(), stOffset, stSize);
    }
    return true;
}

INLINE bool CNETBufferBase::Unpack(PByte& pData, size_t stStart, size_t& stSize, CNETTraits::CNETPackBuildPtr& BuildPtr)
{
    assert(IsRecv());
    assert(stSize > 0);
    assert((stStart + stSize) <= m_stSize);
    if ((IsRecv() == false) || ((stStart + stSize) > m_stSize))
    {
        DEV_DEBUG(TF("CNETBufferBase[%d] Unpack IsRecv=%d, Size=%d, stStart + stSize=%d --- %d"), IsJumbo(), IsRecv(), stSize, stStart + stSize, m_stSize);
        stSize = 0;
        return false;
    }

    bool bRet = true;
    pData = m_pData + stStart;

    size_t stAllSize = BuildPtr->Check(pData, stSize);
    if (stAllSize > 0)
    {
        if (stAllSize <= stSize)
        {
            stSize = stAllSize; // recv data complete
        }
        else if (stAllSize <= m_stSize)
        {
            DEV_DEBUG(TF("CNETBufferBase[%d] Unpack data size=%d > recv size=%d, pack not complete"), IsJumbo(), stAllSize, stSize);
            stSize = 0;
        }
        else // jumbo data ?
        {
            bRet = false;
            if (IsJumbo() == false)
            {
                stSize = stAllSize;
                DEV_DEBUG(TF("CNETBufferBase Unpack data size=%d is out of buffer size=%d, jumbo buffer recving ?"), stAllSize, m_stSize);
            }
            else
            {
                stSize = 0;
                DEV_DEBUG(TF("CNETBufferBase Unpack data size=%d is out of jumbo buffer size=%d"), stAllSize, m_stSize);
            }
        }
    }
    else
    {
        stSize = 0;
        bRet   = false;
        DEV_DEBUG(TF("CNETBufferBase Unpack data head check failed"));
    }
    return bRet;
}

///////////////////////////////////////////////////////////////////
// CTNETBufferBroadCast : delete buf ptr vertify by IsBroadCast
template <typename T>
INLINE CTNETBufferBroadCast<T>::CTNETBufferBroadCast(size_t stSize, size_t stStyle)
: T(stSize, stStyle)
, m_uBroadCast(0)
, m_uFinished(0)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<T, CNETBufferBase>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
}

template <typename T>
INLINE CTNETBufferBroadCast<T>::~CTNETBufferBroadCast(void)
{
}

template <typename T>
INLINE bool CTNETBufferBroadCast<T>::Continue(void)
{
    if (TBroadCast::IsBroadCast())
    {
        CAtomics::Increment<UInt>(&m_uFinished);
        if (CAtomics::CompareExchange<UInt>(&m_uBroadCast, (m_uFinished + 1), m_uFinished) == m_uFinished)
        {
            DEV_DEBUG(TF(" %p BroadCastBuffer Continue finished %d == broadcast %d, broadcast finished"), this, m_uFinished, (m_uBroadCast - 1));
            return false;
        }
        else
        {
            DEV_DEBUG(TF(" %p BroadCastBuffer set Continue finished = %d, broadcast = %d"), this, m_uFinished, m_uBroadCast);
        }
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTNETBufferBroadCast<T>::Finish(UInt uBroadCast)
{
    if (TBroadCast::IsBroadCast())
    {
        CAtomics::Exchange<UInt>(&m_uBroadCast, uBroadCast);
        if (CAtomics::CompareExchange<UInt>(&m_uBroadCast, (m_uFinished + 1), m_uFinished) == m_uFinished)
        {
            DEV_DEBUG(TF(" %p BroadCastBuffer Finish finished %d == broadcast %d, broadcast finished"), this, m_uFinished, (m_uBroadCast - 1));
            return true;
        }
        else
        {
            DEV_DEBUG(TF(" %p BroadCastBuffer set Finish broadcast = %d, finished = %d"), this, m_uBroadCast, m_uFinished);
        }
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////
// CNETSocketBase
INLINE CNETSocketBase::CNETSocketBase(void)
: m_Index(nullptr)
, m_utId(0)
, m_hSocket(INVALID_SOCKET)
//
, m_nTraits(SOCKETT_NONE)
, m_nType(SOCKETT_INIT)
, m_nStatus(SOCKETS_NONE)
// send buffers or accept buffers(tcp) queue
, m_nQueueSize(0)
// live data & last update time
, m_ullLiveData(0)
, m_llLiveTime(0)
{
    MM_SAFE::Set(&m_adrRemote, 0, sizeof(SOCKADDR_INET));
    MM_SAFE::Set(&m_adrLocal,  0, sizeof(SOCKADDR_INET));
}

INLINE CNETSocketBase::~CNETSocketBase(void)
{
}

INLINE CNETSocketBase::CNETSocketBase(const CNETSocketBase&)
: m_Index(nullptr)
, m_utId(0)
, m_hSocket(INVALID_SOCKET)
//
, m_nTraits(SOCKETT_NONE)
, m_nType(SOCKETT_INIT)
, m_nStatus(SOCKETS_NONE)
// send buffers or accept buffers(tcp) queue
, m_nQueueSize(0)
// live data & last update time
, m_ullLiveData(0)
, m_llLiveTime(0)
{
}

INLINE CNETSocketBase& CNETSocketBase::operator=(const CNETSocketBase&)
{
    return (*this);
}

INLINE uintptr_t CNETSocketBase::GetId(void) const
{
    return m_utId;
}

INLINE SOCKET CNETSocketBase::GetSocket(void) const
{
    return m_hSocket;
}
// traits
INLINE bool CNETSocketBase::IsNone(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_NONE);
}

INLINE bool CNETSocketBase::IsUDP(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_UDP);
}

INLINE bool CNETSocketBase::IsTCP(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_TCP);
}

INLINE bool CNETSocketBase::IsTCPListen(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_TCP_LISTEN);
}

INLINE bool CNETSocketBase::IsTCPConnect(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_TCP_CONNECT);
}

INLINE bool CNETSocketBase::IsTCPAccept(void) const
{
    return ((m_nTraits & SOCKETT_MASK) == SOCKETT_TCP_ACCEPT);
}

INLINE bool CNETSocketBase::IsTCPGroup(void) const
{
    return ((m_nTraits & SOCKETT_MASK) >= SOCKETT_TCP);
}

INLINE bool CNETSocketBase::IsTCPValid(void) const
{
    return ((m_nTraits & SOCKETT_MASK) > SOCKETT_TCP);
}

INLINE bool CNETSocketBase::IsTCPTraffic(void) const
{
    return ((m_nTraits & SOCKETT_MASK) >= SOCKETT_TCP_CONNECT);
}

INLINE Int CNETSocketBase::GetTraits(void) const
{
    return (m_nTraits & SOCKETT_MASK);
}

INLINE void CNETSocketBase::SetTraits(Int nTraits)
{
    m_nTraits = (m_nTraits & SOCKETT_EX_MASK) + (nTraits & SOCKETT_MASK);
}

INLINE bool CNETSocketBase::IsBroadCast(void) const
{
    return ((m_nTraits & SOCKETT_EX_UNICAST) == 0);
}

INLINE bool CNETSocketBase::IsJumboRecv(void) const
{
    return ((m_nTraits & SOCKETT_EX_JUMBO) != 0);
}

INLINE bool CNETSocketBase::IsSendEvent(void) const
{
    return ((m_nTraits & SOCKETT_EX_SEND) != 0);
}

INLINE bool CNETSocketBase::IsRecvEvent(void) const
{
    return ((m_nTraits & SOCKETT_EX_RECV) != 0);
}

INLINE bool CNETSocketBase::IsExcludeHead(void) const
{
    return ((m_nTraits & SOCKETT_EX_NOHEAD) != 0);
}

INLINE bool CNETSocketBase::IsIncludeHead(void) const
{
    return ((m_nTraits & SOCKETT_EX_HASHEAD) != 0);
}

INLINE bool CNETSocketBase::IsKeepAlive(void) const
{
    return ((m_nTraits & SOCKETT_EX_ALIVE) != 0);
}

INLINE bool CNETSocketBase::IsSetTime(void) const
{
    return ((m_nTraits & SOCKETT_EX_SETTIME) != 0);
}

INLINE Int CNETSocketBase::GetTraitsEx(bool bDerive) const
{
    if (bDerive == false)
    {
        return (m_nTraits & SOCKETT_EX_MASK);
    }
    else
    {
        return (m_nTraits & SOCKETT_EX_DERIVE);
    }
}

INLINE void CNETSocketBase::SetTraitsEx(Int nTraitsEx, bool bAdd)
{
    if (bAdd)
    {
        m_nTraits |= (nTraitsEx & SOCKETT_EX_MASK);
    }
    else
    {
        m_nTraits &= (~(nTraitsEx & SOCKETT_EX_MASK));
    }
}

INLINE bool CNETSocketBase::IsInitType(void) const
{
    return (m_nType == SOCKETT_INIT);
}

INLINE bool CNETSocketBase::IsReadyType(void) const
{
    return (m_nType == SOCKETT_READY);
}

INLINE bool CNETSocketBase::IsAckType(void) const
{
    return (m_nType == SOCKETT_ACK);
}

INLINE bool CNETSocketBase::IsLiveType(void) const
{
    return (m_nType == SOCKETT_LIVE);
}

INLINE bool CNETSocketBase::IsTrafficType(void) const
{
    return (IsLiveType() || IsAckType());
}

INLINE Int CNETSocketBase::GetType(void) const
{
    return m_nType;
}

INLINE void CNETSocketBase::SetType(Int nType)
{
    m_nType = nType;
}

INLINE bool CNETSocketBase::IsHandleStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_HANDLEMASK) != 0);
}

INLINE bool CNETSocketBase::IsCloseStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_CLOSEMASK) != 0);
}

INLINE void CNETSocketBase::SetHandleStatus(bool bHandle)
{
    if (bHandle)
    {
        CAtomics::Increment<UInt>((PUInt)&m_nStatus);
    }
    else
    {
        CAtomics::Decrement<UInt>((PUInt)&m_nStatus);
    }
}

INLINE void CNETSocketBase::SetCloseStatus(void)
{
    if (IsCloseStatus() == false)
    {
        CAtomics::Add<UInt>((PUInt)&m_nStatus, SOCKETS_CLOSE);
    }
}

INLINE bool CNETSocketBase::IsQueueStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_QUEUE) != 0);
}

INLINE bool CNETSocketBase::IsPendStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_PEND) != 0);
}

INLINE bool CNETSocketBase::IsCheckStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_CHECK) != 0);
}

INLINE bool CNETSocketBase::IsAsyncStatus(void)
{
    return ((CAtomics::CompareExchange<UInt>((PUInt)&m_nStatus, SOCKETS_NONE, SOCKETS_NONE) & SOCKETS_ASYNCMASK) != 0);
}

INLINE bool CNETSocketBase::IsExitStatus(void)
{
    return ((m_nStatus & SOCKETS_EXIT) != 0);
}

INLINE bool CNETSocketBase::SetQueueStatus(bool bQueue)
{
    if (bQueue)
    {
        if (IsQueueStatus() == false)
        {
            CAtomics::Add<UInt>((PUInt)&m_nStatus, SOCKETS_QUEUE);
        }
        return true;
    }
    else if (IsQueueStatus())
    {
        CAtomics::Add<UInt>((PUInt)&m_nStatus, (UInt)SOCKETS_QUEUE_NEG);
    }
    return IsCloseEnable();
}

INLINE bool CNETSocketBase::SetPendStatus(bool bPend)
{
    if (bPend)
    {
        if (IsPendStatus() == false)
        {
            CAtomics::Add<UInt>((PUInt)&m_nStatus, SOCKETS_PEND);
        }
        return true;
    }
    else if (IsPendStatus())
    {
        CAtomics::Add<UInt>((PUInt)&m_nStatus, (UInt)SOCKETS_PEND_NEG);
    }
    return IsCloseEnable();
}

INLINE bool CNETSocketBase::SetCheckStatus(bool bCheck)
{
    if (bCheck)
    {
        if (IsCheckStatus() == false)
        {
            CAtomics::Add<UInt>((PUInt)&m_nStatus, SOCKETS_CHECK);
            //DEV_DEBUG(TF(" SetCheckStatus[%p] status on"), this);
        }
        return true;
    }
    else if (IsCheckStatus())
    {
        CAtomics::Add<UInt>((PUInt)&m_nStatus, (UInt)SOCKETS_CHECK_NEG);
        //DEV_DEBUG(TF(" SetCheckStatus[%p] status off"), this);
    }
    return IsCloseEnable();
}

INLINE bool CNETSocketBase::IsCloseEnable(void)
{
    if (IsAsyncStatus() == false)
    {
        return IsCloseStatus();
    }
    return false;
}

INLINE Int CNETSocketBase::GetStatus(void)
{
    return m_nStatus;
}

INLINE void CNETSocketBase::SetStatus(Int nStatus)
{
    m_nStatus = nStatus;
}

INLINE bool CNETSocketBase::IsBroadCastEnable(Int nTraits)
{
    if (IsBroadCast() && IsLiveType())
    {
        return (GetTraits() == nTraits);
    }
    return false;
}

INLINE ULLong CNETSocketBase::GetLiveData(void)
{
    return m_ullLiveData;
}

INLINE void CNETSocketBase::SetLiveData(ULLong ullLiveData)
{
    if (IsAckType())
    {
        SetType(SOCKETT_LIVE);
        SetLiveTime();
        DEV_DEBUG(TF(" NETSocket[%p] SetLiveData Ack-->Live"), this);
    }
    m_ullLiveData = ullLiveData;
}

INLINE LLong CNETSocketBase::GetLiveTime(void) const
{
    return m_llLiveTime;
}

INLINE void CNETSocketBase::SetLiveTime(void)
{
    if (IsSetTime() == false)
    {
        m_llLiveTime = CPlatform::GetRunningTime();
    }
}

INLINE bool CNETSocketBase::SetLiveTime(LLong llSetTime)
{
    if (llSetTime > 0)
    {
        SetTraitsEx(SOCKETT_EX_SETTIME, true);
        m_llLiveTime = CPlatform::GetRunningTime() + llSetTime;
    }
    else
    {
        SetTraitsEx(SOCKETT_EX_SETTIME, false);
        m_llLiveTime = CPlatform::GetRunningTime();
    }
    return true;
}

INLINE bool CNETSocketBase::CheckLiveTime(LLong llCurrent, Int nTime) const
{
    DEV_DEBUG(TF(" NETSocket[%p] CheckLiveTime current %lld >= %lld + %d(%lld)"), this, llCurrent, m_llLiveTime, nTime, m_llLiveTime + nTime);
    return (llCurrent >= (m_llLiveTime + nTime));
}

INLINE const SOCKADDR_INET& CNETSocketBase::GetAddr(bool bRemote) const
{
    if (bRemote)
    {
        return m_adrRemote;
    }
    else
    {
        return m_adrLocal;
    }
}

#endif // __NETWORK_BASE_INL__
