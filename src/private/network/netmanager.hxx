// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_MANAGER_HXX__
#define __NETWORK_MANAGER_HXX__

#pragma once

#include "netsockettcp.hxx"
#include "netsocketudp.hxx"
#include "netthread.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
template <typename T>
class CTNETSocketCheck;

class CNETStream;
class CNETReadStream;
class CNETWriteStream;

///////////////////////////////////////////////////////////////////
// CNetManager
class CNetManager : public CNetwork
{
public:
    enum MANAGER_STATUS
    {
        MANAGERS_NONE = 0,
        MANAGERS_INIT,
        MANAGERS_OKAY,
        MANAGERS_EXIT,
    };

    enum ADJUST_SIZE
    {
        ADJUSTS_BUFFER_ALL = 0, // buffer + pack head size + m_Attr.nMaxBuffer
        ADJUSTS_JUMBO_ALL,      // buffer + pack head size + m_Attr.nMaxJumbo
        ADJUSTS_BUFFER_OFFSET,  // buffer + pack head size
        ADJUSTS_JUMBO_OFFSET,   // buffer + pack head size
        ADJUSTS_BUFFER_DATA,    // pack head size + m_Attr.nMaxBuffer
        ADJUSTS_JUMBO_DATA,     // pack head size + m_Attr.nMaxJumbo
    };

    enum CACHE_DEF
    {
        CACHE_DEF_RATIO         = 8,
        CACHE_DEF_JUMBO_MAX     = 8192,

        CACHE_DEF_NORMAL_COUNT  = 32,
        CACHE_DEF_LARGE_COUNT   = 1024,
        CACHE_DEF_MASSIVE_COUNT = 8192,

        CACHE_DEF_NORMAL_HASH   = 0x00000400, // 1024
        CACHE_DEF_LARGE_HASH    = 0x00002000, // 8192
        CACHE_DEF_MASSIVE_HASH  = 0x00008000, // 32768

        CACHE_DEF_MIN_GROW      = 0x00010000, // 64K
        CACHE_DEF_MAX_GROW      = 0x08000000, // 128M
    };

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
#ifndef __MODERN_CXX_NOT_SUPPORTED
    enum RADIX_CONST : ULLong
    {
        RADIXC_ADD  = 0x0000000100000000,
        RADIXC_LOW  = 0x00000000FFFFFFFF,
        RADIXC_HIGH = 0xFFFFFFFF00000000,
    };
#else
    static const ULLong RADIXC_ADD  = 0x0000000100000000ULL;
    static const ULLong RADIXC_LOW  = 0x00000000FFFFFFFFULL;
    static const ULLong RADIXC_HIGH = 0xFFFFFFFF00000000ULL;
#endif
#else
    enum RADIX_CONST
    {
        RADIXC_ADD  = 0x00010000,
        RADIXC_LOW  = 0x0000FFFF,
        RADIXC_HIGH = 0xFFFF0000,
    };
#endif

    typedef CTMap<Socket, CNETSocket*>         MAP_NETSOCKET, *PMAP_NETSOCKET;
    typedef CTMap<Socket, CNETSocket*>::PAIR   PAIR_NETSOCKET;
    typedef CTArray<CNETSocket*>               ARY_NETSOCKET;
    typedef CTArray<CNETWorkThread*>           ARY_WORKTHREAD;
public:
    CNetManager(void);//
    virtual ~CNetManager(void);//
    // CTRefCount
    //virtual Int        AddRef(void) OVERRIDE;
    virtual Int        Release(void) OVERRIDE;//
    // CComponent
    virtual UInt       Command(PCXStr pszCMD, uintptr_t utParam) OVERRIDE;//
    virtual UInt       Update(void) OVERRIDE;//
    // CNetwork
    virtual UInt       Init(const NET_ATTR& Attr, CEventHandler* pEventHandler = nullptr, CNETPackBuild* pPackBuild = nullptr) OVERRIDE;
    virtual void       Exit(void) OVERRIDE;

    virtual Socket     Create(UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr, UInt uFlag = SOCKET_TCP_BROADCAST) OVERRIDE;//
    virtual Socket     Create(CEventHandler& EventHandlerRef, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr, UInt uFlag = SOCKET_TCP_BROADCAST) OVERRIDE;//
    virtual Socket     Create(const NET_ADDR& NetAddrRef, UInt uFlag = SOCKET_TCP_BROADCAST) OVERRIDE;//
    virtual Socket     Create(CEventHandler& EventHandlerRef, const NET_ADDR& NetAddrRef, UInt uFlag = SOCKET_TCP_BROADCAST) OVERRIDE;//
    virtual bool       Destroy(Socket sSocket) OVERRIDE;//

    virtual bool       Listen(Socket sSocket, Int nAccept = ATTR_LMT_MIN_BACKLOG) OVERRIDE;//
    virtual bool       Connect(Socket sSocket, UShort usRemotePort, PCXStr pszRemoteAddr) OVERRIDE;//
    virtual bool       Connect(Socket sSocket, const NET_ADDR& RemoteNetAddrRef) OVERRIDE;//

    virtual bool       Send(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) OVERRIDE;//
    virtual bool       Send(const CTArray<Socket>& aSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) OVERRIDE;
    virtual bool       SendCopy(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) OVERRIDE;//

    virtual bool       Send(Socket sSocket, CEventBase& EventRef, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) OVERRIDE;//
    virtual bool       Send(const CTArray<Socket>& aSocket, CEventBase& EventRef, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) OVERRIDE;

    virtual bool       SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) OVERRIDE;//
    virtual bool       SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const ARY_STRADDR& aRemoteStrAddr, Int nFlag = SEND_NORMAL) OVERRIDE;
    virtual bool       SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) OVERRIDE;//

    virtual bool       SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) OVERRIDE;//
    virtual bool       SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const ARY_NETADDR& aRemoteNetAddr, Int nFlag = SEND_NORMAL) OVERRIDE;
    virtual bool       SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) OVERRIDE;//

    virtual bool       SendTo(Socket sSocket, CEventBase& EventRef, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) OVERRIDE;//
    virtual bool       SendTo(Socket sSocket, CEventBase& EventRef, const ARY_STRADDR& aRemoteStrAddr, Int nFlag = SEND_NORMAL) OVERRIDE;

    virtual bool       SendTo(Socket sSocket, CEventBase& EventRef, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) OVERRIDE;//
    virtual bool       SendTo(Socket sSocket, CEventBase& EventRef, const ARY_NETADDR& aRemoteNetAddr, Int nFlag = SEND_NORMAL) OVERRIDE;

    virtual bool       GetAddr(Socket sSocket, CString& strAddr, UShort& usPort, bool bRemote = true) OVERRIDE;//
    virtual bool       GetAddr(Socket sSocket, NET_ADDR& NetAddrRef, bool bRemote = true) OVERRIDE;//
    virtual bool       GetLocalAddr(ARY_STRING& strAddrs, Int nAttr = ATTR_NONE) const OVERRIDE;
    virtual bool       GetLocalAddr(ARY_NETADDR& NetAddrs, Int nAttr = ATTR_NONE) const OVERRIDE;
    virtual bool       GetRemoteAddr(PCXStr pszRemoteAddr, ARY_STRING& strAddrs, Int nAttr = ATTR_IPV6) const OVERRIDE;
    virtual bool       GetRemoteAddr(PCXStr pszRemoteAddr, ARY_NETADDR& NetAddrs, Int nAttr = ATTR_IPV6) const OVERRIDE;
    virtual bool       TranslateAddr(CString& strAddr, UShort& usPort, NET_ADDR& NetAddrRef, bool bString2Addr = true) const OVERRIDE;//
    virtual bool       TranslateAddr(PCXStr pszAddr, UShort usPort, NET_ADDR& NetAddrRef) const OVERRIDE;//

    virtual ULLong     GetAttr(Socket sSocket, UInt uAttr = SOCKET_LIVEDATA) OVERRIDE;//
    virtual bool       SetAttr(Socket sSocket, ULLong ullData, UInt uAttr = SOCKET_LIVEDATA) OVERRIDE;//

    virtual bool       AllocBuffer(NET_PARAM& NetParam, bool bJumbo = false) const OVERRIDE;//
    virtual bool       AllocBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache = nullptr, size_t stDataOffset = 0) const OVERRIDE;//
    virtual bool       AllocJumboBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache = nullptr, size_t stDataOffset = 0) const OVERRIDE;//
    virtual bool       ReuseBuffer(CStreamScopePtr& StreamScopePtrRef, PINDEX index, PByte pCache, size_t stDataOffset = 0) const OVERRIDE;//

    virtual bool       ReferBuffer(CStreamScopePtr& StreamScopePtrRef, CStreamScopePtr& StreamPtr) const OVERRIDE;//

    virtual bool       Check(UInt uTimeout) OVERRIDE;

    virtual void       Dump(NET_DUMP& Dump) const OVERRIDE;//
    virtual void       Attr(NET_ATTR& Attr) const OVERRIDE;//
    // inner methods
    // socket
    CNETSocket*        CreateSocket(UInt uFlag, CEventHandler* pEventHandler, PSOCKADDR_INET pSockAddr);
    bool               DestroySocket(CNETSocket* pSocket, bool bAsync);
    // tcp socket
    bool               Listen(CTCPSocket* pSocket, Int nAccept);
    bool               Connect(CTCPSocket* pSocket, const SOCKADDR_INET& SockAddr);
    bool               Send(CTCPSocket* pSocket, class CNETWriteStream* pStream, Int nFlag, ULLong ullParam);
    bool               Send(const CTArray<Socket>& aSocket, class CNETWriteStream* pStream, Int nFlag, ULLong ullParam);
    // udp socket
    bool               SendTo(CUDPSocket* pSocket, class CNETWriteStream* pStream, const SOCKADDR_INET& SockAddr, Int nFlag);

    bool               GetSocketAddr(CString& strAddr, UShort& usPort, const SOCKADDR_INET& SockAddr) const;
    bool               GetSocketAddr(NET_ADDR& NetAddrRef, const SOCKADDR_INET& SockAddr) const;
    bool               SetSocketAddr(UShort usPort, PCXStr pszAddr, SOCKADDR_INET& SockAddr, Int nAttr = ATTR_IPV6) const;
    bool               SetSocketAddr(const NET_ADDR& NetAddrRef, SOCKADDR_INET& SockAddr, Int nAttr = ATTR_IPV6) const;
    // buffer & jumbo buffer
    size_t             AdjustSize(ADJUST_SIZE eAdjustSize) const;
    bool               CreateBuffer(size_t stStyle, CNETBuffer*& pBuffer, PByte pCache = nullptr, bool bBroadCast = false) const;
    bool               DestroyBuffer(CNETBuffer*& pBuffer, bool bFinished = false) const;

    PByte              CreateAddr(void) const;//
    bool               DestroyAddr(PByte pAddr) const;//
    // tick
    void               LiveAck(LLong llCurrent, Int nAck);
    void               Timeout(LLong llCurrent, Int nTimeout);

    PINDEX             AddSocket(uintptr_t& utId, CNETSocket* pSocket);//
    template <typename T>
    bool               CheckSocket(Socket sSocket, CTNETSocketCheck<T>& SocketCheckRef);//
    bool               RemoveSocket(PINDEX index);//
    void               RemoveAllSocket(void);//
    //
    bool               CheckStatus(void) const;//
    CNetEventHandle&   GetEvent(void);//
    CNETPackBuildPtr&  GetPackBuild(void);//

    PINDEX             GetSocketIndex(void) const;//
    PINDEX             GetBufferIndex(void) const;//
    PINDEX             GetJumboIndex(void) const;//
    PINDEX             GetAddrIndex(void) const;//

    NET_DUMP&          GetDump(void);//
    NET_ATTR&          GetAttr(void);//
private:
    CNetManager(const CNetManager&);//
    CNetManager& operator=(const CNetManager&);//

    bool               CacheInit(void);
    void               CacheExit(void);

    void               HandlerAddRef(void);
    void               HandlerRelease(void);

    template <typename T>
    T*                 AllocSocket(CEventHandler* pEventHandler);//
    void               FreeSocket(CNETSocket* pSocket);//

    bool               BroadCastSend(CTCPSocket* pSocket, class CNETWriteStream* pStream, Int nFlag, ULLong ullParam);

    void               AdjustDump(void);//
    void               AdjustAttr(const NET_ATTR& Attr);//

private:
    uintptr_t          m_utStatus;
    uintptr_t          m_utRadix;

    CEventHandler*     m_pEventHandler;
    PINDEX             m_inxSocket;
    PINDEX             m_inxBuffer;
    PINDEX             m_inxJumbo;
    PINDEX             m_inxAddr;

    NET_DUMP           m_Dump;
    NET_ATTR           m_Attr;

    CNETPackBuildPtr   m_PackBuildPtr;
    CNetEventHandle    m_hNetEvent;
    CSyncLock          m_NMLock;
    CNETTickThread     m_TickThread;
    ARY_WORKTHREAD     m_WorkThread;
    MAP_NETSOCKET      m_NetSocket;
};

///////////////////////////////////////////////////////////////////
// CNETSocketCheck
template <typename T>
class CTNETSocketCheck : public MObject
{
public:
    CTNETSocketCheck(CNetManager* pNetwork, T* pNETSocket = nullptr);
    ~CTNETSocketCheck(void);

    CTNETSocketCheck& operator=(T* pNETSocket);
    template <typename X> CTNETSocketCheck& operator=(X* pNETSocket);

    operator T*(void)   const;
    T& operator*(void)  const;
    T* operator->(void) const;
    T* Get(void)        const;

    void SetAsync(bool bAsync);
private:
    CTNETSocketCheck(const CTNETSocketCheck& aSrc);
    CTNETSocketCheck& operator=(const CTNETSocketCheck& aSrc);
private:
    CNetManager*   m_pNetwork;
    T*             m_pNETSocket;
    bool           m_bAsync;
};

///////////////////////////////////////////////////////////////////
// CNETStream
class CNETStream : public CBufStreamBase
{
public:
    enum STREAM_MODE_EXTEND
    {
        STREAMM_EXT_NET = 0x00000100,
    };
public:
    CNETStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache, Int nMode);
    virtual ~CNETStream(void);

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;

    virtual void   Close(void) OVERRIDE;

    virtual PByte  GetBuf(size_t stPos = 0) OVERRIDE;
    virtual const  PByte GetBuf(size_t stPos = 0) const OVERRIDE;
    virtual PByte  GetBufPos(void) OVERRIDE;
    virtual const  PByte GetBufPos(void) const OVERRIDE;

    bool    Create(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache = nullptr);
    PByte   GetCache(void);
    PINDEX  GetIndex(void) const;
    size_t  GetDataOffset(void) const;
protected:
    PINDEX   m_index;
    size_t   m_stSize;
    size_t   m_stPos;
    size_t   m_stOffset;
    PByte    m_pData;
    PByte    m_pCache;
};

///////////////////////////////////////////////////////////////////
// CNETReadStream
class CNETReadStream : public CNETStream
{
public:
    CNETReadStream(void);
    CNETReadStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache);
    virtual ~CNETReadStream(void);

    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;

    virtual bool   Refer(CStream& aSrc) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CNETWriteStream
class CNETWriteStream : public CNETStream
{
public:
    CNETWriteStream(void);
    CNETWriteStream(PINDEX index, size_t stOffset, size_t stDataOffset, size_t stSize, PByte pCache = nullptr);
    virtual ~CNETWriteStream(void);

    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
#include "netmanager.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_MANAGER_HXX__
