// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTCPSocket
CTCPSocket::CTCPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler)
: CNETSocket(pNetwork, pEventHandler)
, m_ulByte(0)
, m_ulFlag(0)
{
}

CTCPSocket::~CTCPSocket(void)
{
}

bool CTCPSocket::Init(Int nFlag, PSOCKADDR_INET pSockAddr)
{
    if (Create(SOCK_STREAM, IPPROTO_TCP)) // create socket
    {
        // add  map
        if (Add() == false)
        {
            DEV_DEBUG(TF(" TCPSocket[%p] add to socket map failed"), this);
            return false;
        }
        HandlerAddRef();
        SetTraits(SOCKETT_TCP);
        if (pSockAddr == nullptr) // ...
        {
            DEV_DEBUG(TF(" TCPSocket[%p - %p] ready for accept"), this, m_utId);
            return true;
        }
        SetOption(nFlag);
        // bind addr & event
        if (BindAddr(pSockAddr))
        {
            return BindEvent();
        }
    }
    return false;
}

bool CTCPSocket::Exit(bool bAsync)
{
    bool bRet = false;
    SetCloseStatus();
    if ((bAsync == false) || (CAtomics::CompareExchange<UInt>((PUInt)&m_nQueueSize, 0, 0) == 0))
    {
        if (Close())
        {
            CloseEvent();
            HandlerRelease();
        }
        if (IsHandleStatus() == false)
        {
            if (Remove()) // remove from map
            {
                // windows listen socket special!!!
                if (IsTCPListen())
                {
                    CloseReadyAccept();
                }
                DEV_DEBUG(TF(" TCPSocket[%p] exit remove okay!"), this);
            }
            else
            {
                DEV_DEBUG(TF(" TCPSocket[%p] exit detect index is null already!"), this);
            }
            bRet = IsCloseEnable();
        }
    }
    if (bRet)
    {
        return Destroy();
    }
    DEV_DEBUG(TF(" TCPSocket[%p] exit detect lock status[%X] or sending buffer[%d] is not empty, destroy by async work thread"), this, GetStatus(), m_nQueueSize);
    return false;
}

bool CTCPSocket::OnEventHandle(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifdef __RUNTIME_DEBUG__
    size_t stStyle = pBuffer->GetStyle();
#endif
    if (pBuffer->IsSend())
    {
        assert(IsTCPTraffic() && IsTrafficType());
        bRet = SendHandled(uBytes, pBuffer);
    }
    else if (pBuffer->IsRecv())
    {
        assert(IsTCPTraffic() && IsTrafficType());
        bRet = RecvHandled(uBytes, pBuffer);
    }
    else if (pBuffer->IsAccept())
    {
        assert(IsTCPListen() && IsLiveType());
        bRet = AcceptHandled(uBytes, pBuffer);
    }
    else if (pBuffer->IsConnect())
    {
        assert(IsTCPConnect() && IsReadyType());
        bRet = ConnectHandled(uBytes, pBuffer);
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle failed by traits[%d]-type[%d]-status[%X], data size %d, buffer=%p-%#X"), this, GetTraits(), GetType(), GetStatus(), uBytes, pBuffer, stStyle);
    return false;
}

bool CTCPSocket::OnEventFail(UInt uError, CNETBuffer* pBuffer)
{
    bool bRet = false;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifdef __RUNTIME_DEBUG__
    size_t stStyle = pBuffer->GetStyle();
#endif
    if (pBuffer->IsSend())
    {
        assert(IsTCPTraffic() && IsTrafficType());
        SetQueueStatus(false); // send queue IOCP status off
        //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail send queue IOCP status off"), this);
    }
    else if (pBuffer->IsRecv())
    {
        assert(IsTCPTraffic() && IsTrafficType());
        SetPendStatus(false);  // recv pend IOCP status off
        //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail recv pend IOCP status off"), this);
    }
    else if (pBuffer->IsAccept())
    {
        assert(IsTCPListen() && IsLiveType());
        SetQueueStatus(false); // listen queue IOCP status off
        //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail listen queue IOCP status off"), this);
    }
    else if (pBuffer->IsConnect())
    {
        assert(IsTCPConnect() && IsReadyType());
        //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail connect failed[error=%d] by traits[%d]-type[%d]-status[%X], buffer=%p-%#X"), this, uError, GetTraits(), GetType(), GetStatus(), pBuffer, stStyle);
        SetPendStatus(false);  // connect pend IOCP status off
        //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail connect pend IOCP status off---%d"), this, uError);
        assert(m_pCommonBuffer == pBuffer);
        DestroyCommonBuffer();

        bRet = UpdateReadyConnect(uError);
    }
    // ERROR_NETNAME_DELETED(64) peer close socket gracelessly
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail[error=%d] failed by traits[%d]-type[%d]-status[%X], buffer=%p-%#X"), this, uError, GetTraits(), GetType(), GetStatus(), pBuffer, stStyle);
    return false;
}

bool CTCPSocket::Listen(Int nAccept)
{
    if (IsCloseStatus())
    {
        return false;
    }
    if (IsReadyType())
    {
        assert(IsTCP());
        SetTraits(SOCKETT_TCP_LISTEN);
        if ((::listen(m_hSocket, nAccept) == SOCKET_ERROR) || (CNetExtension::Init(m_hSocket) == false))
        {
            DEV_DEBUG(TF(" TCPSocket[%p] listen failed[%#X]"), this, ::WSAGetLastError());
            return false;
        }
        if (CreateQueueBuffer(nAccept))
        {
            return IssueListen(nAccept);
        }
    }
    DEV_DEBUG(TF(" TCPSocket[%p] listen failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::Connect(const SOCKADDR_INET& SockAddr)
{
    if (IsCloseStatus())
    {
        return false;
    }
    if (IsReadyType())
    {
        assert(IsTCP());
        SetTraits(SOCKETT_TCP_CONNECT);
        if (CNetExtension::Init(m_hSocket) == false)
        {
            DEV_DEBUG(TF(" TCPSocket[%p] connect failed[%#X]"), this, ::WSAGetLastError());
            return false;
        }
        m_adrRemote = SockAddr;
        return IssueConnect();
    }
    DEV_DEBUG(TF(" TCPSocket[%p] connect failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::Send(CNETBuffer*& pBuffer)
{
    if (IsCloseStatus())
    {
        return false;
    }
    bool bRet = false;
    assert(IsTCPTraffic());
    if (IsTrafficType())
    {
        LLong llSendTick = CPlatform::GetOSRunningTick();
        Int   nIndex     = AddBuffer(pBuffer);
        if (nIndex > -1)
        {
            bRet = true;
            if ((nIndex == 0) && (IssueSend() == false))
            {
                bRet = false;
                SetCloseStatus();
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
    }
    else
    {
        DEV_DEBUG(TF(" TCPSocket[%p] send failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    }
    return bRet;
}

bool CTCPSocket::AcceptHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    UNREFERENCED_PARAMETER( uBytes );

    PACCEPT_ENTRY p = reinterpret_cast<PACCEPT_ENTRY>(pBuffer->GetData());
    assert(p->pListen == this);
    assert((size_t)uBytes == p->stBytes);
    assert(p->index != nullptr);
    assert(m_QueueBuffer.GetAt(p->index).pBuffer == pBuffer);

#if (NTDDI_VERSION >= NTDDI_VISTA)
    if (p->pAccept->UpdateReadyAccept(m_hSocket))
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    if (p->pAccept->UpdateReadyAccept(pBuffer))
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
    {
        bool bRet = AcceptEvent(p->pAccept->GetId()) ? p->pAccept->IssueAccept(GetTraitsEx(true)) : false;
        if (bRet == false)
        {
            DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandled[accept socket %p] AcceptEvent return false to close accept socket"), this, p->pAccept);
            m_pNetwork->DestroySocket(p->pAccept, false);
        }
    }
    SetLiveTime();

    if (AsyncAccept(p->index, pBuffer) == false)
    {
        SetQueueStatus(false); // listen queue IOCP status off
        //DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandled-AsyncAccept listen queue IOCP status off"), this);
        return false;
    }
    return true;
}

bool CTCPSocket::ConnectHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    assert(IsTCPConnect() && IsReadyType());
    UNREFERENCED_PARAMETER( pBuffer );
    assert(m_pCommonBuffer == pBuffer);
    assert(uBytes == 0);

    //DEV_DEBUG(TF(" TCPSocket[%p] ConnectHandled connect pend IOCP status off"), this);
    SetPendStatus(false);  // connect pend IOCP status off
    DestroyCommonBuffer(); // destroy connect buffer
    // update conext
    ::setsockopt(m_hSocket, SOL_SOCKET, SO_UPDATE_CONNECT_CONTEXT, nullptr, 0);
    UpdateAddr();

    SetLiveTime();
    SetType(SOCKETT_LIVE);
    if (CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend) && IssueRecv())
    {
        return ConnectEvent((uintptr_t)uBytes);
    }
    return false;
}

bool CTCPSocket::SendHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    if (IsCloseStatus() == false)
    {
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendSize), (LLong)uBytes);
        SetLiveTime();

        if (pBuffer->IsClose() == false)
        {
            bRet = true;
            Int nQueue = 0;
            {
                CSyncLockScope scope(m_NSLock);
                nQueue = RemoveBuffer();
                if (nQueue == 0)
                {
                    // send queue is empty, clean send queue status
                    //DEV_DEBUG(TF(" TCPSocket[%p] SendHandled-SendQueue empty send queue IOCP status off"), this);
                    SetQueueStatus(false); // send queue IOCP status off
                }
            }
            if (nQueue > 0)
            {
                DEV_DEBUG(TF(" TCPSocket[%p] SendHandled okay, send next in queue"), this);
                bRet = AsyncSend();
            }
            else if (IsSendEvent())
            {
                bRet = SendEvent(); // send list buffer is null
            }
        }
        else
        {
            DEV_DEBUG(TF(" TCPSocket[%p] SendHandled, close socket by request"), this);
        }
    }
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" TCPSocket[%p] SendHandled send queue IOCP status off"), this);
        SetQueueStatus(false); // send queue IOCP status off
        return false;
    }
    return true;
}

bool CTCPSocket::RecvHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    if ((IsCloseStatus() == false) && (uBytes > 0))
    {
        bRet = true;
        if (uBytes != (UInt)NET_DEF_FIREASYNC)
        {
            LLong llRecvTick = CPlatform::GetOSRunningTick();

            bRet = RecvBufferHandle(pBuffer, (size_t)uBytes);

            llRecvTick = (CPlatform::GetOSRunningTick() - llRecvTick);
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvTick), llRecvTick);
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvSize), (LLong)uBytes);
        }
        if (bRet == true)
        {
            if (IsCloseStatus() == false)
            {
                SetLiveTime();
                bRet = AsyncRecv(pBuffer);
            }
            else
            {
                bRet = false;
            }
        }
    }
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" TCPSocket[%p] RecvHandled recv pend IOCP status off"), this);
        SetPendStatus(false);  // recv pend IOCP status off
        return false;
    }
    return true;
}

bool CTCPSocket::IssueListen(Int nAccept)
{
    assert(m_nQueueSize == 0);
    //DEV_DEBUG(TF(" TCPSocket[%p] IssueListen listen queue IOCP status on"), this);
    SetQueueStatus(true); // listen queue IOCP status on
    CNETBuffer* pBuffer = nullptr;
    Int  nCount = 0;
    for (nCount = 0; nCount < nAccept; ++nCount)
    {
        if (m_pNetwork->CreateBuffer(CNETBuffer::BUFFERS_ACCEPT, pBuffer))
        {
            PINDEX index = AddListenBuffer(pBuffer);
            assert(index != nullptr);
            if (AsyncAccept(index, pBuffer) == false)
            {
                m_pNetwork->DestroyBuffer(pBuffer);
                break;
            }
        }
    }
    if (nCount == nAccept)
    {
        SetType(SOCKETT_LIVE);
        DEV_DEBUG(TF(" TCPSocket[%p] issue listen[%d] okay"), this, nAccept);
        return true;
    }
    //DEV_DEBUG(TF(" TCPSocket[%p] IssueListen listen queue IOCP status off"), this);
    SetQueueStatus(false); // listen queue IOCP status off
    SetCloseStatus();
    DEV_DEBUG(TF(" TCPSocket[%p] issue listen[%d] failed, set close status"), this, nAccept);
    return false;
}

bool CTCPSocket::IssueAccept(Int nDerive)
{
    //if (IsTCP())
    //{
        assert(IsInitType());
        if (BindEvent())
        {
            SetLiveTime();
            if (m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_ACK_DETECT)
            {
                SetType(SOCKETT_ACK);
            }
            else
            {
                SetType(SOCKETT_LIVE);
            }
            SetTraits(SOCKETT_TCP_ACCEPT);
            if (nDerive != 0)
            {
                DEV_DEBUG(TF(" TCPSocket[%p] derive TraitsEx=%#X"), this, nDerive);
                SetTraitsEx(nDerive, true);
            }
            if (CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend))
            {
                return IssueRecv();
            }
        }
    //}
    DEV_DEBUG(TF(" TCPSocket[%p] issue accept failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::IssueConnect(void)
{
    if (CreateCommonBuffer(CNETBuffer::BUFFERS_CONNECT))
    {
        //DEV_DEBUG(TF(" TCPSocket[%p] IssueConnect connect pend IOCP status on"), this);
        SetPendStatus(true);  // connect pend IOCP status on
        if (AsyncConnect())
        {
            return true;
        }
        //DEV_DEBUG(TF(" TCPSocket[%p] IssueConnect connect pend IOCP status off"), this);
        SetPendStatus(false);  // connect pend IOCP status off
        DestroyCommonBuffer();
    }
    return false;
}

bool CTCPSocket::IssueSend(void)
{
    {
        CSyncLockScope scope(m_NSLock);
        //DEV_DEBUG(TF(" TCPSocket[%p] IssueSend send queue IOCP status on"), this);
        SetQueueStatus(true); // send queue IOCP status on
    }
    if (AsyncSend())
    {
        return true;
    }
    //DEV_DEBUG(TF(" TCPSocket[%p] IssueSend send queue IOCP status off"), this);
    SetQueueStatus(false); // send queue IOCP status off
    return false;
}

bool CTCPSocket::IssueRecv(void)
{
    if (CreateCommonBuffer(CNETBuffer::BUFFERS_RECV))
    {
        m_PendEntry.pBuffer = m_pCommonBuffer;
        m_PendEntry.Reset();

        //DEV_DEBUG(TF(" TCPSocket[%p] IssueRecv recv pend IOCP status on"), this);
        SetPendStatus(true);  // recv pend IOCP status on
        if (m_pNetwork->GetEvent().Signal((ULong)NET_DEF_FIREASYNC, reinterpret_cast<uintptr_t>(this), &m_PendEntry))
        {
            DEV_DEBUG(TF(" TCPSocket[%p - %p] ready to recv data"), this, m_utId);
            return true;
        }
        //DEV_DEBUG(TF(" TCPSocket[%p] IssueRecv recv pend IOCP status off"), this);
        SetPendStatus(false);  // recv pend IOCP status off
        DestroyCommonBuffer();
    }
    return false;
}

bool CTCPSocket::AsyncAccept(PINDEX index, CNETBuffer* pBuffer)
{
    PACCEPT_ENTRY p = reinterpret_cast<PACCEPT_ENTRY>(pBuffer->GetData());
    p->Reset();
    p->pBuffer      = pBuffer;
    p->stBytes      = 0;
    p->index        = index;
    p->pListen      = this;
    p->pAccept      = static_cast<CTCPSocket*>(m_pNetwork->CreateSocket(CNETTraits::SOCKET_TCP_BROADCAST, m_pEventHandler, nullptr));
    if (p->pAccept != nullptr)
    {
        pBuffer->SetBuf(0, sizeof(ACCEPT_ENTRY));
        if (CNetExtension::ms_pAcceptEx(m_hSocket, p->pAccept->GetSocket(),
                                        pBuffer->GetBufData(),
                                        0,
                                        sizeof(SOCKADDR_INET) + NET_DEF_SOCKADDR,
                                        sizeof(SOCKADDR_INET) + NET_DEF_SOCKADDR,
                                        (PULong)&(p->stBytes),
                                        p) == TRUE)
        {
            DEV_DEBUG(TF(" TCPSocket[%p] async accept with socket[%p]"), this, p->pAccept);
            return true;
        }
        if (::WSAGetLastError() == WSA_IO_PENDING)
        {
            ::WSASetLastError(0);
            return true;
        }
        DEV_DEBUG(TF(" TCPSocket[%p] async accept failed[%#X]"), this, ::WSAGetLastError());
        m_pNetwork->DestroySocket(p->pAccept, false);
    }
    else
    {
        DEV_DEBUG(TF(" TCPSocket[%p] async accept failed by create socket"), this);
    }
    return false;
}

bool CTCPSocket::AsyncConnect(void)
{
    m_PendEntry.pBuffer = m_pCommonBuffer;
    m_PendEntry.Reset();

    Int        n = 0;
    LPSOCKADDR p = nullptr;
    if (m_adrRemote.si_family == AF_INET6)
    {
        n = sizeof(SOCKADDR_IN6);
        p = (LPSOCKADDR)&(m_adrRemote.Ipv6);
    }
    else
    {
        n = sizeof(SOCKADDR_IN);
        p = (LPSOCKADDR)&(m_adrRemote.Ipv4);
    }

    if (CNetExtension::ms_pConnectEx(m_hSocket, p, n, nullptr, 0, nullptr, &m_PendEntry) == TRUE)
    {
        return true;
    }
    if (::WSAGetLastError() == WSA_IO_PENDING)
    {
        ::WSASetLastError(0);
        return true;
    }
    DEV_DEBUG(TF(" TCPSocket[%p] async connect failed[%#X]"), this, ::WSAGetLastError());
    return false;
}

bool CTCPSocket::AsyncSend(void)
{
    CNETBuffer* pBuffer = m_QueueBuffer.GetHead().pBuffer;
    m_QueueEntry.NetBuf = *(pBuffer->GetBuf());
    if (pBuffer->BlockBufData())
    {
        // exclude head igore
    }
    else if (IsExcludeHead())
    {
        m_QueueEntry.NetBuf.len -= (BufSize)m_pNetwork->GetPackBuild()->OffsetSize();
        m_QueueEntry.NetBuf.buf += m_pNetwork->GetPackBuild()->OffsetSize();
        DEV_DEBUG(TF(" TCPSocket[%p] AsyncSend ExcludeHead Traits"), this);
    }
    m_QueueEntry.pBuffer = pBuffer;
    m_QueueEntry.Reset();
    if (::WSASend(m_hSocket, &(m_QueueEntry.NetBuf), 1, &m_ulByte, 0, &m_QueueEntry, nullptr) == 0)
    {
        return true;
    }
    if (::WSAGetLastError() == WSA_IO_PENDING)
    {
        ::WSASetLastError(0);
        return true;
    }
    DEV_DEBUG(TF(" TCPSocket[%p] async send queue[%d] failed[%#X]"), this, m_nQueueSize, ::WSAGetLastError());
    return false;
}

bool CTCPSocket::AsyncRecv(CNETBuffer* pBuffer)
{
    NETBUF NetBuf = *(pBuffer->GetBuf());
    if (pBuffer->BlockBufData())
    {
        // exclude head igore
    }
    else if (IsExcludeHead())
    {
        NetBuf.len -= (BufSize)m_pNetwork->GetPackBuild()->OffsetSize();
        NetBuf.buf += m_pNetwork->GetPackBuild()->OffsetSize();
        DEV_DEBUG(TF(" TCPSocket[%p] AsyncRecv ExcludeHead Traits"), this);
    }
    m_PendEntry.pBuffer = pBuffer;
    m_PendEntry.Reset();

    if (::WSARecv(m_hSocket, &NetBuf, 1, &m_ulByte, &m_ulFlag, &m_PendEntry, nullptr) == 0)
    {
        return true;
    }
    if (::WSAGetLastError() == WSA_IO_PENDING)
    {
        ::WSASetLastError(0);
        return true;
    }
    DEV_DEBUG(TF(" TCPSocket[%p] async recv failed[%#X]"), this, ::WSAGetLastError());
    return false;
}

bool CTCPSocket::AcceptEvent(uintptr_t utAccept)
{
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPConnectCount), 1);
    DEV_DEBUG(TF(" TCPSocket[%p - %p] EVENT_TCP_ACCEPT[accept socket %p]"), this, m_utId, utAccept);
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_ACCEPT, utAccept, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::ConnectEvent(uintptr_t utError)
{
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPConnectCount), 1);
    DEV_DEBUG(TF(" TCPSocket[%p - %p] EVENT_TCP_CONNECT[%#X]"), this, m_utId, utError);
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_CONNECT, utError, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::SendEvent(void)
{
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_SEND, (uintptr_t)m_pNetwork->GetAttr().nMaxSend, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::RecvEvent(CNETTraits::TCP_PARAM& Param)
{
    //////
    // work thread <--> logic thread
    if (IsRecvEvent())
    {
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_RECV, (uintptr_t)Param.nSize, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    else
    {
        CBufReadStream BufReadStream((size_t)Param.nSize, Param.pData);
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_RECV, BufReadStream, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    //////
}

void CTCPSocket::CloseEvent(void)
{
    if (m_pNetwork->CheckStatus())
    {
        DEV_DEBUG(TF(" TCPSocket[%p - %p] CloseEvent traits[%d]-state[%d]-status[%X] %p"), this, m_utId, GetTraits(), GetType(), GetStatus(), m_pEventHandler);
        if (IsTCPValid())
        {
            //////
            // work thread <--> logic thread
            m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_CLOSE, m_utId, m_ullLiveData);
            //////
        }
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPCloseCount), 1);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF(" TCPSocket[%p - %p] CloseEvent exit status"), this, m_utId);
    }
#endif
}

PINDEX CTCPSocket::AddListenBuffer(CNETBuffer* pBuffer)
{
    SOCKET_BUFFER Buffer;
    Buffer.pBuffer = pBuffer;

    return m_QueueBuffer.AddTail(Buffer);
}

void CTCPSocket::CloseReadyAccept(void)
{
    CSyncLockScope scope(m_NSLock);
    if (m_QueueBuffer.GetSize() > 0)
    {
        for (PINDEX index = m_QueueBuffer.GetHeadIndex(); index != nullptr; )
        {
            SOCKET_BUFFER& Buffer = m_QueueBuffer.GetNext(index);
            if (m_pNetwork->CheckStatus())
            {
                PACCEPT_ENTRY p = reinterpret_cast<PACCEPT_ENTRY>(Buffer.pBuffer->GetData());
                m_pNetwork->DestroySocket(p->pAccept, false);
            }
            m_pNetwork->DestroyBuffer(Buffer.pBuffer);
        }
        m_QueueBuffer.RemoveAll();
        m_nQueueSize = 0;
    }
}

#if (NTDDI_VERSION >= NTDDI_VISTA)
bool CTCPSocket::UpdateReadyAccept(SOCKET hListen)
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
bool CTCPSocket::UpdateReadyAccept(CNETBuffer* pBuffer)
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
{
    //if (IsTCP())
    //{
        assert(IsInitType());
#if (NTDDI_VERSION >= NTDDI_VISTA)
        ::setsockopt(m_hSocket, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*)&hListen, sizeof(SOCKET));
        if (m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_IPV6)
        {
            Int nLen = sizeof(SOCKADDR_IN6);
            ::getsockname(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv6), &nLen);
            ::getpeername(m_hSocket, (LPSOCKADDR)&(m_adrRemote.Ipv6), &nLen);
        }
        else // ATTR_IPV4
        {
            Int nLen = sizeof(SOCKADDR_IN);
            ::getsockname(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv4), &nLen);
            ::getpeername(m_hSocket, (LPSOCKADDR)&(m_adrRemote.Ipv4), &nLen);
        }
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
        #pragma message("Windows XP No Implement")
        PACCEPT_ENTRY p = reinterpret_cast<PACCEPT_ENTRY>(pBuffer->GetData());
        SOCKET hListen  = p->pListen->GetSocket();
        ::setsockopt(m_hSocket, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*)&hListen, sizeof(SOCKET));
        SOCKADDR* padrLocal = nullptr;
        SOCKADDR* padrRemote = nullptr;
        Int       nLenLocal = sizeof(SOCKADDR_INET);
        Int       nLenRemote = sizeof(SOCKADDR_INET);
        CNetExtension::ms_pGetAcceptExSockAddrs(pBuffer->GetBufData(), 0,
                                                sizeof(SOCKADDR_INET) + NET_DEF_SOCKADDR,
                                                sizeof(SOCKADDR_INET) + NET_DEF_SOCKADDR,
                                                &padrLocal, &nLenLocal,
                                                &padrRemote, &nLenRemote);
        MM_SAFE::Cpy(&m_adrRemote, sizeof(SOCKADDR_INET), padrRemote, sizeof(SOCKADDR_INET));
        MM_SAFE::Cpy(&m_adrLocal, sizeof(SOCKADDR_INET), padrLocal, sizeof(SOCKADDR_INET));
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
        return true;
    //}
    //DEV_DEBUG(TF(" TCPSocket[%p] update ready accept failed by traits[%d]"), this, GetTraits());
    //return false;
}

bool CTCPSocket::UpdateReadyConnect(UInt uError)
{
    switch (uError)
    {
    case ERROR_SEM_TIMEOUT:
    case ERROR_CONNECTION_REFUSED:
    case WSAENOTCONN:
    case WSAEALREADY:
    case WSAEADDRNOTAVAIL:
    case WSAEAFNOSUPPORT:
    case WSAECONNREFUSED:
    case WSAEISCONN:
    case WSAENETUNREACH:
    case WSAEHOSTUNREACH:
    case WSAETIMEDOUT:
        {
            ::WSASetLastError(0);
            if (ConnectEvent((uintptr_t)uError))
            {
                SetTraits(SOCKETT_TCP);
                return true;
            }
        }
        break;
    default: {}
    }
    return false;
}

bool CTCPSocket::OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly)
{
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = (CNETTraits::Socket)m_utId;
    Param.pData   = pData;
    Param.nSize   = (Int)stSize;
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_TCP;
    if (pBuffer->IsJumbo() == false)
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_BUFFER;
        Param.index   = m_pNetwork->GetBufferIndex();
        Param.nOffset = m_pNetwork->GetAttr().nBufferOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxBuffer;
    }
    else
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_JUMBOBUF;
        Param.index   = m_pNetwork->GetJumboIndex();
        Param.nOffset = m_pNetwork->GetAttr().nJumboOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxJumbo;
    }
    if (bOnly)
    {
        Param.pCache = (PByte)pBuffer;
    }
    bool bRet = RecvEvent(Param);
    if (bOnly)
    {
        pData = Param.pCache;
    }
    return bRet;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
