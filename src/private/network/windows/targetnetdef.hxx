// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_DEF_HXX__
#define __TARGET_NETWORK_DEF_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "sync.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
enum NET_DEF
{
    NET_DEF_EVENT_DEFAULT = 0,
    NET_DEF_SOCKADDR      = 16,         // AcceptEx :  at least 16 bytes more than the maximum address length for the transport protocol in use[MSDN]
    NET_DEF_ONE_HALFL     = 460,        // n * 460 >> 10 --> 0.45
    NET_DEF_ONE_HALFH     = 522,        // n * 564 >> 10 --> 0.51
    NET_DEF_FIREASYNC     = 0x67CF5E86, // UNICODE - NAME
};

typedef Long   AtomVal, *PAtomVal;
typedef ULong  BufSize, *PBufSize;
typedef PChar  BufAddr, *PBufAddr;
typedef WSABUF NETBUF,  *PNETBUF;

struct tagSOCKET_ENTRY : public WSAOVERLAPPED
{
public:
    tagSOCKET_ENTRY(void)
    {
        MM_SAFE::Set(this, 0, sizeof(tagSOCKET_ENTRY));
    }

    ~tagSOCKET_ENTRY(void)
    {
    }

    void Reset(void)
    {
        MM_SAFE::Set(this, 0, sizeof(WSAOVERLAPPED));
    }
public:
    class CNETBuffer*   pBuffer;
};
typedef tagSOCKET_ENTRY   SOCKET_ENTRY, *PSOCKET_ENTRY;

struct tagQUEUE_ENTRY : public tagSOCKET_ENTRY
{
public:
    tagQUEUE_ENTRY(void)
    {
        MM_SAFE::Set(this, 0, sizeof(tagQUEUE_ENTRY));
    }

    ~tagQUEUE_ENTRY(void)
    {
    }
public:
    NETBUF   NetBuf;
};
typedef tagQUEUE_ENTRY   QUEUE_ENTRY, *PQUEUE_ENTRY;

///////////////////////////////////////////////////////////////////
// CNetExtension
class CNetExtension : public MObject
{
public:
    static bool  Init(SOCKET hSocket);
    static void  Exit(void);
private:
    CNetExtension(void);
    ~CNetExtension(void);
    CNetExtension(const CNetExtension&);
    CNetExtension& operator=(const CNetExtension&);
public:
    static LPFN_ACCEPTEX                  ms_pAcceptEx;
    static LPFN_CONNECTEX                 ms_pConnectEx;
#if (NTDDI_VERSION < NTDDI_VISTA)
    static LPFN_GETACCEPTEXSOCKADDRS      ms_pGetAcceptExSockAddrs;
#endif
};

SELECTANY LPFN_ACCEPTEX                   CNetExtension::ms_pAcceptEx             = nullptr;
SELECTANY LPFN_CONNECTEX                  CNetExtension::ms_pConnectEx            = nullptr;
#if (NTDDI_VERSION < NTDDI_VISTA)
SELECTANY LPFN_GETACCEPTEXSOCKADDRS       CNetExtension::ms_pGetAcceptExSockAddrs = nullptr;
#endif

INLINE CNetExtension::CNetExtension(void)
{
}

INLINE CNetExtension::~CNetExtension(void)
{
}

INLINE CNetExtension::CNetExtension(const CNetExtension&)
{
}

INLINE CNetExtension& CNetExtension::operator=(const CNetExtension&)
{
    return (*this);
}

INLINE bool CNetExtension::Init(SOCKET hSocket)
{
    // Load the AcceptEx function into memory using WSAIoctl.
    // The WSAIoctl function is an extension of the ioctlsocket()
    // function that can use overlapped I/O. The function's 3rd
    // through 6th parameters are input and output buffers where
    // we pass the pointer to our AcceptEx function. This is used
    // so that we can call the AcceptEx function directly, rather
    // than refer to the Mswsock.lib library.
    ULong ulBytes = 0;
    Int   nRet    = 0;
    if (ms_pAcceptEx == nullptr)
    {
        GUID  guid = WSAID_ACCEPTEX;
        nRet       = ::WSAIoctl(hSocket,
                                SIO_GET_EXTENSION_FUNCTION_POINTER,
                                &guid,
                                sizeof(GUID),
                                &ms_pAcceptEx,
                                sizeof(LPFN_ACCEPTEX),
                                &ulBytes,
                                nullptr,
                                nullptr);
        if (nRet == SOCKET_ERROR)
        {
            DEV_DEBUG(TF(" CNetExtension::Init get AcceptEx ptr failed[%#X]"), ::WSAGetLastError());
        }
    }
    if (ms_pConnectEx == nullptr)
    {
        GUID  guid = WSAID_CONNECTEX;
        nRet       = ::WSAIoctl(hSocket,
                                SIO_GET_EXTENSION_FUNCTION_POINTER,
                                &guid,
                                sizeof(GUID),
                                &ms_pConnectEx,
                                sizeof(LPFN_CONNECTEX),
                                &ulBytes,
                                nullptr,
                                nullptr);
        if (nRet == SOCKET_ERROR)
        {
            DEV_DEBUG(TF(" CNetExtension::Init get ConnectEx ptr failed[%#X]"), ::WSAGetLastError());
        }
    }
#if (NTDDI_VERSION < NTDDI_VISTA)
    if (ms_pGetAcceptExSockAddrs == nullptr)
    {
        GUID  guid = WSAID_GETACCEPTEXSOCKADDRS;
        nRet       = ::WSAIoctl(hSocket,
                                SIO_GET_EXTENSION_FUNCTION_POINTER,
                                &guid,
                                sizeof(GUID),
                                &ms_pGetAcceptExSockAddrs,
                                sizeof(LPFN_GETACCEPTEXSOCKADDRS),
                                &ulBytes,
                                nullptr,
                                nullptr);
        if (nRet == SOCKET_ERROR)
        {
            DEV_DEBUG(TF(" CNetExtension::Init get GetAcceptExSockAddrs ptr failed[%#X]"), ::WSAGetLastError());
        }
    }
    return ((ms_pAcceptEx != nullptr)  &&
            (ms_pConnectEx != nullptr) &&
            (ms_pGetAcceptExSockAddrs != nullptr));
#else
    return ((ms_pAcceptEx != nullptr) && (ms_pConnectEx != nullptr));
#endif
}

INLINE void CNetExtension::Exit(void)
{
}

///////////////////////////////////////////////////////////////////
// CNetEventHandle
class CNetEventHandle : public MObject
{
public:
    CNetEventHandle(void);
    ~CNetEventHandle(void);

    operator Handle(void) const;

    bool     IsValid(void);

    bool     Open(void);
    void     Close(void);

    bool     Ctl(Handle hSocket, void* pArg);
    bool     Signal(ULong uSignal, ULONG_PTR upKey, LPWSAOVERLAPPED pOvl);
    bool     SignalAll(Int nCount);
    bool     Wait(UInt& uBytes, PULONG_PTR ppSocket, LPWSAOVERLAPPED* ppOvl);
private:
    CNetEventHandle(const CNetEventHandle& aSrc);
    CNetEventHandle& operator=(const CNetEventHandle& aSrc);
public:
    Handle   m_hNetEvent;
};

INLINE CNetEventHandle::CNetEventHandle(void)
: m_hNetEvent(HANDLE_INVALID)
{
}

INLINE CNetEventHandle::~CNetEventHandle(void)
{
    Close();
}

INLINE CNetEventHandle::CNetEventHandle(const CNetEventHandle&)
: m_hNetEvent(HANDLE_INVALID)
{
}

INLINE CNetEventHandle& CNetEventHandle::operator=(const CNetEventHandle&)
{
    return (*this);
}

INLINE CNetEventHandle::operator Handle(void) const
{
    return m_hNetEvent;
}

INLINE bool CNetEventHandle::IsValid(void)
{
    return (m_hNetEvent != HANDLE_INVALID);
}

INLINE bool CNetEventHandle::Open(void)
{
    Close();
    m_hNetEvent = ::CreateIoCompletionPort(HANDLE_INVALID, nullptr, 0, 0);
    if (m_hNetEvent != nullptr)
    {
        return true;
    }
    m_hNetEvent = HANDLE_INVALID;
    DEV_DEBUG(TF("NetEvent completion-port create failed[%#X]"), ::WSAGetLastError());
    return false;
}

INLINE void CNetEventHandle::Close(void)
{
    if (IsValid())
    {
        ::CloseHandle(m_hNetEvent);
        m_hNetEvent = HANDLE_INVALID;
    }
}

INLINE bool CNetEventHandle::Ctl(Handle hSocket, void* pArg)
{
    assert(IsValid());
    if (::CreateIoCompletionPort(hSocket, m_hNetEvent, (uintptr_t)pArg, 0) == m_hNetEvent)
    {
        return true;
    }
    DEV_DEBUG(TF("NetEvent completion-port ctl failed[%#X]"), ::WSAGetLastError());
    return false;
}

INLINE bool CNetEventHandle::Signal(ULong uSignal, ULONG_PTR upKey, LPWSAOVERLAPPED pOvl)
{
    return (::PostQueuedCompletionStatus(m_hNetEvent, uSignal, upKey, pOvl) != FALSE);
}

INLINE bool CNetEventHandle::SignalAll(Int nCount)
{
    assert(IsValid());
    for (Int i = 0; i < nCount; ++i)
    {
        ::PostQueuedCompletionStatus(m_hNetEvent, 0, 0, nullptr);
    }
    return (nCount > 0);
}

INLINE bool CNetEventHandle::Wait(UInt& uBytes, PULONG_PTR ppSocket, LPWSAOVERLAPPED* ppOvl)
{
    assert(IsValid());
    return (::GetQueuedCompletionStatus(m_hNetEvent, (PULong)&uBytes, ppSocket, ppOvl, INFINITE) == TRUE);
}

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_DEF_HXX__
