// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_SOCKET_TCP_HXX__
#define __TARGET_NETWORK_SOCKET_TCP_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netsocket.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTCPSocket
class CTCPSocket : public CNETSocket
{
public:
    CTCPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler);//
    virtual  ~CTCPSocket(void);//

    virtual  bool Init(Int nFlag, PSOCKADDR_INET pSockAddr = nullptr) OVERRIDE;
    virtual  bool Exit(bool bAsync = false) OVERRIDE; // return false if send list is not null when async close
    //
    virtual  bool OnEventHandle(UInt uBytes, CNETBuffer* pBuffer) OVERRIDE;
    virtual  bool OnEventFail(UInt uBytes, CNETBuffer* pBuffer) OVERRIDE;
    //
    bool     Listen(Int nAccept);
    bool     Connect(const SOCKADDR_INET& SockAddr);
    bool     Send(CNETBuffer*& pBuffer);
private:
    bool     AcceptHandled(UInt uBytes, CNETBuffer* pBuffer);
    bool     ConnectHandled(UInt uBytes, CNETBuffer* pBuffer);
    bool     SendHandled(UInt uBytes, CNETBuffer* pBuffer);
    bool     RecvHandled(UInt uBytes, CNETBuffer* pBuffer);
    //
    bool     IssueListen(Int nAccept);
    bool     IssueAccept(Int nDerive);
    bool     IssueConnect(void);
    bool     IssueSend(void);
    bool     IssueRecv(void);
    //
    bool     AsyncAccept(PINDEX index, CNETBuffer* pBuffer);
    bool     AsyncConnect(void);
    bool     AsyncSend(void);
    bool     AsyncRecv(CNETBuffer* pBuffer);
    //
    bool     AcceptEvent(uintptr_t utAccept);
    bool     ConnectEvent(uintptr_t utError);
    bool     SendEvent(void);
    bool     RecvEvent(CNETTraits::TCP_PARAM& Param);
    void     CloseEvent(void);
    //
    PINDEX   AddListenBuffer(CNETBuffer* pBuffer);
    void     CloseReadyAccept(void);
#if (NTDDI_VERSION >= NTDDI_VISTA)
    bool     UpdateReadyAccept(SOCKET hListen);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    bool     UpdateReadyAccept(CNETBuffer* pBuffer);
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
    bool     UpdateReadyConnect(UInt uError);
    //
    virtual  bool OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly) OVERRIDE;
private:
    struct tagACCEPT_ENTRY : public tagSOCKET_ENTRY
    {
    public:
        tagACCEPT_ENTRY(void)
        {
            MM_SAFE::Set(this, 0, sizeof(tagACCEPT_ENTRY));
        }

        ~tagACCEPT_ENTRY(void)
        {
        }
    public:
        CTCPSocket*   pAccept;
        CTCPSocket*   pListen;
        PINDEX        index;
        size_t        stBytes;
    };
    typedef tagACCEPT_ENTRY ACCEPT_ENTRY, *PACCEPT_ENTRY;
private:
    ULong            m_ulByte;
    ULong            m_ulFlag;
    QUEUE_ENTRY      m_QueueEntry;
    SOCKET_ENTRY     m_PendEntry;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_SOCKET_TCP_HXX__
