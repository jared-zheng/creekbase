// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETWorkThread
CNETWorkThread::CNETWorkThread(CNetManager* pNetwork)
: m_pNetwork(pNetwork)
{
}

CNETWorkThread::~CNETWorkThread(void)
{
}


bool CNETWorkThread::Run(void)
{
    if (m_pNetwork->CheckStatus() == false)
    {
        DEV_DEBUG(TF(" work thread get network manager exit, stop thread"));
        return false;
    }

    UpdateWaitStatus();
    return OnSocketHandle();
}

bool CNETWorkThread::OnSocketHandle(void)
{
    UInt          uBytes  = 0;
    CNETSocket*   pSocket = nullptr;
    PSOCKET_ENTRY pEntry  = nullptr;
    bool          bRet    = m_pNetwork->GetEvent().Wait(uBytes, (PULONG_PTR)&pSocket, (LPWSAOVERLAPPED*)&pEntry);
    UpdateWaitStatus(false);
    if (bRet)
    {
        if ((pSocket == nullptr) && (pEntry == nullptr))
        {
            DEV_DEBUG(TF(" work thread get exit flag, stop thread"));
            return false;
        }
        //if (pSocket->Check())
        //{
            //DEV_DEBUG(TF(" work thread get status okay, socket[%p] with buffer[%p]"), pSocket, pEntry->pBuffer);
            if (pSocket->OnEventHandle(uBytes, pEntry->pBuffer) == false)
            {
                m_pNetwork->DestroySocket(pSocket, false);
            }
        //}
        //else
        //{
        //    DEV_DEBUG(TF(" work thread get status okay, socket[%p] is invalid with buffer[%p]"), pSocket, pEntry->pBuffer);
        //}
    }
    else // ConnectEx failed GetQueuedCompletionStatus return FALSE
    {
        assert(pSocket != nullptr);
        assert(pEntry  != nullptr);
        //if (pSocket->Check())
        //{
            uBytes = (UInt)::WSAGetLastError();
            if (pSocket->OnEventFail(uBytes, pEntry->pBuffer) == false)
            {
                m_pNetwork->DestroySocket(pSocket, false);
            }
        //}
        //else
        //{
        //    DEV_DEBUG(TF(" work thread get status fail[%#X], socket[%p] is invalid with buffer[%p]"), ::WSAGetLastError(), pSocket, pEntry->pBuffer);
        //}
    }
    return true;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
