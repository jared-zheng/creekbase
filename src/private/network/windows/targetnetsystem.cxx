// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netsystem.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetworkSystem : CSubSystem
UInt CNetworkSystem::Init(void)
{
    if (CAtomics::Increment<UInt>(&(CNetworkSystemSingleton::ms_uInitRef)) == 1)
    {
        WSADATA wsaData = { 0 };
        Int nRet = ::WSAStartup(WINSOCK_VERSION, &wsaData);
        if (nRet != 0)
        {
            DEV_WARN(TF("* Network SubSystem singleton init failed[%d]"), nRet);
            return (UInt)RET_FAIL;
        }
        if ((LOBYTE(wsaData.wVersion) != 2) || (HIBYTE(wsaData.wVersion) != 2))
        {
            DEV_WARN(TF("* Network SubSystem singleton init failed by version[%#X]"), wsaData.wVersion);
            return (UInt)RET_FAIL;
        }
        DEV_DUMP(TF("* Network SubSystem singleton init okay"));
    }
    return (UInt)RET_OKAY;
}

void CNetworkSystem::Exit(void)
{
    if (CAtomics::Decrement<UInt>(&(CNetworkSystemSingleton::ms_uInitRef)) == 0)
    {
        ::WSACleanup();
        DEV_DUMP(TF("* Network SubSystem singleton exit okay"));
    }
}

#if defined(NETWORK_EXPORT)
///////////////////////////////////////////////////////////////////
//
#ifdef __RUNTIME_DEBUG__
    #define NETWORK_MODULE_ATTACH TF("$ Load networkDebug.dll --- DLL_PROCESS_ATTACH")
    #define NETWORK_MODULE_DETACH TF("$ Unload networkDebug.dll --- DLL_PROCESS_DETACH")
#else
    #define NETWORK_MODULE_ATTACH TF("$ Load network.dll --- DLL_PROCESS_ATTACH")
    #define NETWORK_MODULE_DETACH TF("$ Unload network.dll --- DLL_PROCESS_DETACH")
#endif

BOOL WINAPI DllMain(HMODULE hModule, ULong ulReason, void* lpReserved)
{
    UNREFERENCED_PARAMETER( lpReserved );

    if (ulReason == DLL_PROCESS_ATTACH)
    {
        ::DisableThreadLibraryCalls(hModule);
        DEV_DEBUG(NETWORK_MODULE_ATTACH);
    }
    else if (ulReason == DLL_PROCESS_DETACH)
    {
        DEV_DEBUG(NETWORK_MODULE_DETACH);
    }
    return TRUE;
}
///////////////////////////////////////////////////////////////////
#endif  // NETWORK_EXPORT

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)


