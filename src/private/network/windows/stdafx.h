#ifndef __STDAFX_H__
#define __STDAFX_H__

#pragma once

#include "windows/targetconfig.h"
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <mstcpip.h>
#include <mswsock.h>

//#if (NTDDI_VERSION >= NTDDI_WINBLUE)
//#define  __RUNTIME_RIO_SUPPORT__
//#endif

#include "targetnetdef.hxx"

#include "network.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#
//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __STDAFX_H__
