// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUDPSocket
CUDPSocket::CUDPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler)
: CNETSocket(pNetwork, pEventHandler)
, m_ulByte(0)
, m_ulFlag(0)
, m_nLen(0)
{
}

CUDPSocket::~CUDPSocket(void)
{
}

bool CUDPSocket::Init(Int nFlag, PSOCKADDR_INET pSockAddr)
{
    if (Create(SOCK_DGRAM, IPPROTO_UDP)) // create socket
    {
        // add  map
        if (Add() == false)
        {
            DEV_DEBUG(TF(" UDPSocket[%p] add to socket map failed"), this);
            return false;
        }
        HandlerAddRef();
        SetTraits(SOCKETT_UDP);
        SetOption(nFlag);
        assert(pSockAddr != nullptr);
        if (BindAddr(pSockAddr) && BindEvent())
        {
            // bind pSockAddr maybe any addr, here get exact addr
            UpdateAddr();
            return IssueRecvFrom();
        }
    }
    return false;
}

bool CUDPSocket::Exit(bool bAsync)
{
    bool bRet = false;
    SetCloseStatus();
    if ((bAsync == false) || (CAtomics::CompareExchange<UInt>((PUInt)&m_nQueueSize, 0, 0) == 0))
    {
        if (Close())
        {
            CloseEvent();
            HandlerRelease();
        }
        if (IsHandleStatus() == false)
        {
            if (Remove()) // remove from map
            {
                DEV_DEBUG(TF(" UDPSocket[%p] exit remove okay!"), this);
            }
            else
            {
                DEV_DEBUG(TF(" UDPSocket[%p] exit detect index is null already!"), this);
            }
        }
        if (Close())
        {
            bRet = IsCloseEnable();
        }
    }
    if (bRet)
    {
        return Destroy();
    }
    DEV_DEBUG(TF(" UDPSocket[%p] exit detect lock status[%X] or sending buffer[%d] is not empty, destroy by async work thread"), this, GetStatus(), m_nQueueSize);
    return false;
}

bool CUDPSocket::OnEventHandle(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifdef __RUNTIME_DEBUG__
    size_t stStyle = pBuffer->GetStyle();
#endif
    if (pBuffer->IsSend())
    {
        assert(IsLiveType());
        bRet = SendHandled(uBytes, pBuffer);
    }
    else if (pBuffer->IsRecv())
    {
        assert(IsReadyType() || IsLiveType());
        bRet = RecvHandled(uBytes, pBuffer);
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" UDPSocket[%p] OnEventHandle failed by traits[%d]-type[%d]-status[%X], data size %d, buffer=%p-%#X"), this, GetTraits(), GetType(), GetStatus(), uBytes, pBuffer, stStyle);
    return false;
}

bool CUDPSocket::OnEventFail(UInt uError, CNETBuffer* pBuffer)
{
    bool bRet = false;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifdef __RUNTIME_DEBUG__
    size_t stStyle = pBuffer->GetStyle();
#endif
    if (pBuffer->IsSend())
    {
        assert(IsLiveType());
        SetQueueStatus(false); // sendto queue IOCP status off
        //DEV_DEBUG(TF(" UDPSocket[%p] OnEventFail sendto queue IOCP status off"), this);
    }
    else if (pBuffer->IsRecv())
    {
        assert(IsReadyType() || IsLiveType());
        SetPendStatus(false);  // recvfrom pend IOCP status off
        //DEV_DEBUG(TF(" UDPSocket[%p] OnEventFail recvfrom pend IOCP status off"), this);
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" UDPSocket[%p] OnEventFail[error=%d] failed by traits[%d]-type[%d]-status[%X], buffer=%p-%#X"), this, uError, GetTraits(), GetType(), GetStatus(), pBuffer, stStyle);
    return false;
}

bool CUDPSocket::SendTo(CNETBuffer*& pBuffer, const SOCKADDR_INET& SockAddr)
{
    if (IsCloseStatus())
    {
        return false;
    }
    bool bRet = false;
    assert(IsUDP());
    if (IsLiveType())
    {
        LLong llSendTick = CPlatform::GetOSRunningTick();
        Int   nIndex     = AddBuffer(pBuffer, &SockAddr);
        if (nIndex > -1)
        {
            bRet = true;
            if ((nIndex == 0) && (IssueSendTo() == false))
            {
                bRet = false;
                SetCloseStatus();
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
    }
    else
    {
        DEV_DEBUG(TF(" UDPSocket[%p] sendto failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    }
    return bRet;
}

bool CUDPSocket::SendHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    if (IsCloseStatus() == false)
    {
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendSize), (LLong)uBytes);
        SetLiveTime();

        if (pBuffer->IsClose() == false)
        {
            bRet = true;
            Int nQueue = 0;
            {
                CSyncLockScope scope(m_NSLock);
                nQueue = RemoveBuffer();
                if (nQueue == 0)
                {
                    // send queue is empty, clean send queue status
                    //DEV_DEBUG(TF(" UDPSocket[%p] SendHandled-SendQueue empty sendto queue IOCP status off"), this);
                    SetQueueStatus(false); // send queue IOCP status off
                }
            }
            if (nQueue > 0)
            {
                DEV_DEBUG(TF(" UDPSocket[%p] SendHandled okay, send next in queue"), this);
                bRet = AsyncSendTo();
            }
            else if (IsSendEvent())
            {
                bRet = SendEvent(); // send list buffer is null
            }
        }
        else
        {
            DEV_DEBUG(TF(" UDPSocket[%p] SendHandled, close socket by request"), this);
        }
    }
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" UDPSocket[%p] SendHandled sendto queue IOCP status off"), this);
        SetQueueStatus(false); // sendto queue IOCP status off
        return false;
    }
    return true;
}

bool CUDPSocket::RecvHandled(UInt uBytes, CNETBuffer* pBuffer)
{
    bool bRet = false;
    if ((IsCloseStatus() == false) && (uBytes > 0))
    {
        bRet = true;
        if (uBytes != NET_DEF_FIREASYNC)
        {
            LLong llRecvTick = CPlatform::GetOSRunningTick();
            bRet = RecvBufferHandle(pBuffer, (size_t)uBytes);

            llRecvTick = (CPlatform::GetOSRunningTick() - llRecvTick);
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvTick), llRecvTick);
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvSize), (LLong)uBytes);
        }
        else
        {
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llUDPCreateCount), 1);
        }
        if (bRet == true)
        {
            if (IsCloseStatus() == false)
            {
                SetLiveTime();
                bRet = AsyncRecvFrom(pBuffer);
            }
            else
            {
                bRet = false;
            }
        }
    }
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" UDPSocket[%p] RecvHandled recvfrom pend IOCP status off"), this);
        SetPendStatus(false);  // recvfrom pend IOCP status off
        return false;
    }
    return true;
}

bool CUDPSocket::IssueSendTo(void)
{
    CSyncLockScope scope(m_NSLock);
    //DEV_DEBUG(TF(" UDPSocket[%p] IssueSendTo sendto queue IOCP status on"), this);
    SetQueueStatus(true); // sendto queue IOCP status on
    if (AsyncSendTo())
    {
        return true;
    }
    //DEV_DEBUG(TF(" UDPSocket[%p] IssueSendTo sendto queue IOCP status off"), this);
    SetQueueStatus(false); // sendto queue IOCP status off
    DEV_DEBUG(TF(" UDPSocket[%p] send queue buffer[%d] failed"), this, m_nQueueSize);
    return false;
}

bool CUDPSocket::IssueRecvFrom(void)
{
    if (CreateCommonBuffer(CNETBuffer::BUFFERS_RECV) && CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend))
    {
        SetType(SOCKETT_LIVE);

        m_PendEntry.pBuffer = m_pCommonBuffer;
        m_PendEntry.Reset();

        //DEV_DEBUG(TF(" UDPSocket[%p] IssueRecvFrom recvfrom pend IOCP status on"), this);
        SetPendStatus(true);  // recvfrom pend IOCP status on
        if (m_pNetwork->GetEvent().Signal((ULong)NET_DEF_FIREASYNC, reinterpret_cast<uintptr_t>(this), &m_PendEntry))
        {
            DEV_DEBUG(TF(" UDPSocket[%p - %p] ready to recv data"), this, m_utId);
            return true;
        }
        //DEV_DEBUG(TF(" UDPSocket[%p] IssueRecvFrom recvfrom pend IOCP status off"), this);
        SetPendStatus(false);  // recvfrom pend IOCP status off
        DestroyCommonBuffer();
    }
    return false;
}

bool CUDPSocket::AsyncSendTo(void)
{
    SOCKET_BUFFER& Buffer = m_QueueBuffer.GetHead();
    m_QueueEntry.pBuffer  = Buffer.pBuffer;
    m_QueueEntry.NetBuf   = *(Buffer.pBuffer->GetBuf());
    if (Buffer.pBuffer->BlockBufData())
    {
        // exclude head igore
    }
    else if (IsExcludeHead())
    {
        m_QueueEntry.NetBuf.len -= (BufSize)m_pNetwork->GetPackBuild()->OffsetSize();
        m_QueueEntry.NetBuf.buf += m_pNetwork->GetPackBuild()->OffsetSize();
        DEV_DEBUG(TF(" UDPSocket[%p] ExcludeHead Traits"), this);
    }
    m_QueueEntry.Reset();

    Int        n = 0;
    LPSOCKADDR p = nullptr;
    if (m_adrLocal.si_family == AF_INET6)
    {
        n = sizeof(SOCKADDR_IN6);
        p = (LPSOCKADDR)&(Buffer.pSockAddr->Ipv6);
    }
    else
    {
        n = sizeof(SOCKADDR_IN);
        p = (LPSOCKADDR)&(Buffer.pSockAddr->Ipv4);
    }
    if (::WSASendTo(m_hSocket, &(m_QueueEntry.NetBuf), 1, &m_ulByte, 0, p, n, &m_QueueEntry, nullptr) == 0)
    {
        return true;
    }
    if (::WSAGetLastError() == WSA_IO_PENDING)
    {
        ::WSASetLastError(0);
        return true;
    }
    DEV_DEBUG(TF(" UDPSocket[%p] async sendto queue[%d] failed[%#X]"), this, m_nQueueSize, ::WSAGetLastError());
    return false;
}

bool CUDPSocket::AsyncRecvFrom(CNETBuffer* pBuffer)
{
    NETBUF NetBuf = *(pBuffer->GetBuf());
    if (pBuffer->BlockBufData())
    {
        // exclude head igore
    }
    else if (IsExcludeHead())
    {
        NetBuf.len -= (BufSize)m_pNetwork->GetPackBuild()->OffsetSize();
        NetBuf.buf += m_pNetwork->GetPackBuild()->OffsetSize();
        DEV_DEBUG(TF(" UDPSocket[%p] AsyncRecvFrom ExcludeHead Traits"), this);
    }
    m_PendEntry.pBuffer = pBuffer;
    m_PendEntry.Reset();

    LPSOCKADDR pSockAddr = nullptr;
    if (m_adrLocal.si_family == AF_INET6)
    {
        m_nLen    = (Int)sizeof(SOCKADDR_IN6);
        pSockAddr = (LPSOCKADDR)&(m_adrRemote.Ipv6);
    }
    else
    {
        m_nLen    = (Int)sizeof(SOCKADDR_IN);
        pSockAddr = (LPSOCKADDR)&(m_adrRemote.Ipv4);
    }

    if (::WSARecvFrom(m_hSocket, &NetBuf, 1, &m_ulByte, &m_ulFlag, pSockAddr, &m_nLen, &m_PendEntry, nullptr) == 0)
    {
        return true;
    }
    if (::WSAGetLastError() == WSA_IO_PENDING)
    {
        ::WSASetLastError(0);
        return true;
    }
    DEV_DEBUG(TF(" UDPSocket[%p] async recvfrom failed[%#X]"), this, ::WSAGetLastError());
    return false;
}

bool CUDPSocket::SendEvent(void)
{
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_SEND, (uintptr_t)m_pNetwork->GetAttr().nMaxSend, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CUDPSocket::RecvEvent(CNETTraits::UDP_PARAM& Param)
{
    //////
    // work thread <--> logic thread
    if (IsRecvEvent())
    {
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_RECV, (uintptr_t)Param.nSize, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    else
    {
        CBufReadStream BufReadStream((size_t)Param.nSize, Param.pData);
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_RECV, BufReadStream, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    //////
}

void CUDPSocket::CloseEvent(void)
{
    if (m_pNetwork->CheckStatus())
    {
        DEV_DEBUG(TF(" UDPSocket[%p - %p] CloseEvent traits[%d]-state[%d]-status[%X] %p"), this, m_utId, GetTraits(), GetType(), GetStatus(), m_pEventHandler);
        //////
        // work thread <--> logic thread
        m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_CLOSE, m_utId, m_ullLiveData);
        //////
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llUDPCloseCount), 1);
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DUMP(TF(" UDPSocket[%p - %p] CloseEvent exit status"), this, m_utId);
    }
#endif
}

bool CUDPSocket::OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly)
{
    CNETTraits::UDP_PARAM Param;
    m_pNetwork->GetSocketAddr(Param.NetAddr, GetAddr(true));
    Param.sSocket = (CNETTraits::Socket)m_utId;
    Param.pData   = pData;
    Param.nSize   = (Int)stSize;
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_UDP;
    if (pBuffer->IsJumbo() == false)
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_BUFFER;
        Param.index   = m_pNetwork->GetBufferIndex();
        Param.nOffset = m_pNetwork->GetAttr().nBufferOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxBuffer;
    }
    else
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_JUMBOBUF;
        Param.index   = m_pNetwork->GetJumboIndex();
        Param.nOffset = m_pNetwork->GetAttr().nJumboOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxJumbo;
    }
    if (bOnly)
    {
        Param.pCache = (PByte)pBuffer;
    }
    bool bRet = RecvEvent(Param);
    if (bOnly)
    {
        pData = Param.pCache;
    }
    return bRet;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
