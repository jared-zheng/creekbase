// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_BUFFER_HXX__
#define __TARGET_NETWORK_BUFFER_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netbase.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETBuffer
class CNETBuffer : public CNETBufferBase
{
public:
    CNETBuffer(size_t stSize, size_t stStyle = BUFFERS_SEND);
    ~CNETBuffer(void);
private:
protected:
};

///////////////////////////////////////////////////////////////////
// CNETBufferBroadCast : delete buf ptr verify by IsBroadCast
// RIO : [RIO BUF special---not supported]
//       The buffer associated with a send operation
//       should not be used concurrently with another send operation.
//       The buffer, and buffer registration, must remain valid for the duration of a send operation.
//       This means that you should not pass the same PRIO_BUF to a RIOSend(Ex) request
//       when one is already pending. Only after an in-flight RIOSend(Ex) request is complete
//       should you re-use the same PRIO_BUF
//       (either with the same offset or with a different offset and length).
class CNETBufferBroadCast : public CTNETBufferBroadCast<CNETBuffer>
{
public:
    explicit CNETBufferBroadCast(size_t stSize, size_t stStyle); // broadcast type is auto add
    ~CNETBufferBroadCast(void);
};

///////////////////////////////////////////////////////////////////
#include "targetnetbuffer.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_BUFFER_HXX__
