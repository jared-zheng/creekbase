// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_SOCKET_UDP_HXX__
#define __TARGET_NETWORK_SOCKET_UDP_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netsocket.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUDPSocket
class CUDPSocket : public CNETSocket
{
public:
    CUDPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler);//
    virtual  ~CUDPSocket(void);//

    virtual  bool Init(Int nFlag, PSOCKADDR_INET pSockAddr = nullptr) OVERRIDE;
    virtual  bool Exit(bool bAsync = false) OVERRIDE; // return false if send list is not null when async close
    //
    virtual  bool OnEventHandle(UInt uBytes, CNETBuffer* pBuffer) OVERRIDE;
    virtual  bool OnEventFail(UInt uBytes, CNETBuffer* pBuffer) OVERRIDE;
    //
    bool     SendTo(CNETBuffer*& pBuffer, const SOCKADDR_INET& SockAddr);
private:
    //
    bool     SendHandled(UInt uBytes, CNETBuffer* pBuffer);
    bool     RecvHandled(UInt uBytes, CNETBuffer* pBuffer);
    //
    bool     IssueSendTo(void);
    bool     IssueRecvFrom(void);
    //
    bool     AsyncSendTo(void);
    bool     AsyncRecvFrom(CNETBuffer* pBuffer);
    //
    bool     SendEvent(void);
    bool     RecvEvent(CNETTraits::UDP_PARAM& Param);
    void     CloseEvent(void);
    //
    virtual  bool OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly) OVERRIDE;
private:
    ULong                m_ulByte;
    ULong                m_ulFlag;
    Int                  m_nLen;
    QUEUE_ENTRY          m_QueueEntry;
    SOCKET_ENTRY         m_PendEntry;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_SOCKET_UDP_HXX__
