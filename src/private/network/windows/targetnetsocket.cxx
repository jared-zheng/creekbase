// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETSocket
bool CNETSocket::SetSendTimeout(Int nTime)
{
    if (::setsockopt(m_hSocket, SOL_SOCKET, SO_SNDTIMEO, (PCStr)&nTime, (Int)sizeof(Int)) == SOCKET_ERROR)
    {
        DEV_DEBUG(TF(" NETSocket[%p] set send timeout failed[%#X]"), this, ::WSAGetLastError());
        return false;
    }
    return true;
}

bool CNETSocket::SetRecvTimeout(Int nTime)
{
    if (::setsockopt(m_hSocket, SOL_SOCKET, SO_RCVTIMEO, (PCStr)&nTime, (Int)sizeof(Int)) == SOCKET_ERROR)
    {
        DEV_DEBUG(TF(" NETSocket[%p] set recv timeout failed[%#X]"), this, ::WSAGetLastError());
        return false;
    }
    return true;
}

bool CNETSocket::SetKeepAlive(bool bEnable)
{
    if (bEnable)
    {
        if (IsKeepAlive() == false)
        {
            Int nKeepAlive = TRUE;
            if (::setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, (PCStr)&nKeepAlive, (Int)sizeof(Int)) == SOCKET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set keepalive true option failed[%#X]"), this, ::WSAGetLastError());
                return false;
            }
            SetTraitsEx(SOCKETT_EX_ALIVE, true);
        }
    }
    else
    {
        if (IsKeepAlive())
        {
            Int nKeepAlive = FALSE;
            if (::setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, (PCStr)&nKeepAlive, (Int)sizeof(Int)) == SOCKET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set keepalive false option failed[%#X]"), this, ::WSAGetLastError());
                return false;
            }
            SetTraitsEx(SOCKETT_EX_ALIVE, false);
        }
    }
    return true;
}

bool CNETSocket::SetKeepAliveTime(UInt uTime)
{
    if (SetKeepAlive())
    {
        ULLong ullInterval = (ULLong)uTime * NET_DEF_ONE_HALFL;
        ullInterval >>= 10;

        tcp_keepalive KeepAlive;
        KeepAlive.onoff             = (BufSize)TRUE;
        KeepAlive.keepalivetime     = (BufSize)uTime;
        KeepAlive.keepaliveinterval = (BufSize)ullInterval;
        if (::WSAIoctl(m_hSocket, SIO_KEEPALIVE_VALS, (void*)&KeepAlive, sizeof(tcp_keepalive), nullptr, 0, (PBufSize)&ullInterval, nullptr, nullptr) == SOCKET_ERROR)
        {
            DEV_DEBUG(TF(" NETSocket[%p] set keepalivetime option failed[%#X]"), this, ::WSAGetLastError());
            return false;
        }
        return true;
    }
    return false;
}

bool CNETSocket::Create(Int nType, Int nProtocol)
{
    assert(m_pNetwork      != nullptr);
    assert(m_pEventHandler != nullptr);
    assert(m_hSocket       == INVALID_SOCKET);

    Int nPF   = ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_IPV6) ? PF_INET6 : PF_INET);
    m_hSocket = ::WSASocket(nPF, nType, nProtocol, nullptr, 0, WSA_FLAG_OVERLAPPED);
    if (m_hSocket == INVALID_SOCKET)
    {
        DEV_DEBUG(TF(" NETSocket[%p] create socket[PF : %s---Type : %d---Proto : %d] failed[%#X]"), this, ((nPF == PF_INET6) ? TF("PF_INET6") : TF("PF_INET")), nType, nProtocol, ::WSAGetLastError());
        return SOCKETT_NONE;
    }
    m_utId = (uintptr_t)m_hSocket;
    DEV_DEBUG(TF(" NETSocket[%p] create socket[PF : %s---Type : %d---Proto : %d] successed"), this, ((nPF == PF_INET6) ? TF("PF_INET6") : TF("PF_INET")), nType, nProtocol);
    return true;
}

bool CNETSocket::Close(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    SOCKET hSocket = (SOCKET)CAtomics::Exchange64<LLong>((PLLong)&m_hSocket, (LLong)INVALID_SOCKET);
#else
    SOCKET hSocket = (SOCKET)CAtomics::Exchange<Long>((PLong)&m_hSocket, (Long)INVALID_SOCKET);
#endif
    if (hSocket != INVALID_SOCKET)
    {
        DEV_DEBUG(TF(" NETSocket[%p] Shutdown socket[%p] with buffer[%p - %p]"), this, m_utId, m_pCommonBuffer, m_pJumboBuffer);
        ::shutdown(hSocket, SD_BOTH);
        ::closesocket(hSocket);
        return (m_pEventHandler != nullptr);
    }
    else
    {
        DEV_DEBUG(TF(" NETSocket[%p] Close socket already"), this);
    }
    return false;
}

bool CNETSocket::Destroy(void)
{
    uintptr_t utId = CAtomics::Exchange<uintptr_t>(&m_utId, 0);
    if (utId != 0)
    {
        DEV_DEBUG(TF(" NETSocket[%p] %p Destroyed"), this, utId);
        // buffer
        DestroyQueueBuffer();
        DestroyCommonBuffer();
        DestroyJumboBuffer();
        return true;
    }
    else
    {
        DEV_DEBUG(TF(" NETSocket[%p] %p Destroyed already"), this, utId);
    }
    return false;
}

bool CNETSocket::SetOption(Int nFlag)
{
    if ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_REUSEADDR) != 0)
    {
        Int nData = TRUE;
        ::setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEADDR, (PCStr)&nData, (Int)sizeof(Int));
    }

    Int nBufSize = (Int)m_pNetwork->AdjustSize(CNetManager::ADJUSTS_JUMBO_DATA);
    Int nDefault = 0;

    Int nLen = (Int)sizeof(Int);
    ::getsockopt(m_hSocket, SOL_SOCKET, SO_RCVBUF, (PStr)&nDefault, &nLen);
    if (nDefault < nBufSize)
    {
        ::setsockopt(m_hSocket, SOL_SOCKET, SO_RCVBUF, (PCStr)&nBufSize, (Int)sizeof(Int));
    }

    nLen = (Int)sizeof(Int);
    ::getsockopt(m_hSocket, SOL_SOCKET, SO_SNDBUF, (PStr)&nDefault, &nLen);
    if (nDefault < nBufSize)
    {
        ::setsockopt(m_hSocket, SOL_SOCKET, SO_SNDBUF, (PCStr)&nBufSize, (Int)sizeof(Int));
    }
    if (IsTCP())
    {
        if ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_NAGLE) == 0)
        {
            Int nNoDelay = TRUE;
            ::setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, (PCStr)&nNoDelay, (Int)sizeof(Int));
        }
        if (nFlag == CNETTraits::SOCKET_TCP)
        {
            SetTraitsEx(SOCKETT_EX_UNICAST, true);
        }
    }
    else
    {    assert(IsUDP());
        if (nFlag == CNETTraits::SOCKET_UDP_BROADCAST)
        {
            Int nBroadCast = TRUE;
            if (::setsockopt(m_hSocket, SOL_SOCKET, SO_BROADCAST, (PCStr)&nBroadCast, (Int)sizeof(Int)) == SOCKET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set broadcast option failed[%#X]"), this, ::WSAGetLastError());
                return false;
            }
        }
        else
        {
            SetTraitsEx(SOCKETT_EX_UNICAST, true);
        }
        //// WSAECONNRESET
        //// The virtual circuit was reset by the remote side executing a hard or abortive close.
        //// The application should close the socket as it is no longer usable.
        //// For a UPD datagram socket, this error would indicate that a previous send operation resulted in an ICMP "Port Unreachable" message.
        ULong ulRet  = 0;
        BOOL  bReset = FALSE;
        ::WSAIoctl(m_hSocket, SIO_UDP_CONNRESET, &bReset, sizeof(BOOL), nullptr, 0, &ulRet, nullptr, nullptr);
    }
    return true;
}

bool CNETSocket::BindAddr(PSOCKADDR_INET pSockAddr)
{
    m_adrLocal = *pSockAddr;
    if (m_adrLocal.si_family == AF_INET6)
    {
        if (::bind(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv6), (Int)sizeof(SOCKADDR_IN6)) == 0)
        {
            return true;
        }
    }
    else if (m_adrLocal.si_family == AF_INET)
    {
        if (::bind(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv4), (Int)sizeof(SOCKADDR_IN)) == 0)
        {
            return true;
        }
    }
    DEV_DEBUG(TF(" NETSocket[%p] bind local address failed[%#X]"), this, ::WSAGetLastError());
    return false;
}

bool CNETSocket::BindEvent(bool, UInt)
{
    if (m_pNetwork->GetEvent().Ctl((Handle)m_hSocket, this))
    {
        SetType(SOCKETT_READY);
        DEV_DEBUG(TF(" NETSocket[%p - %p] bind event okay"), this, m_utId);
        return true;
    }
    return false;
}

bool CNETSocket::UpdateEvent(UInt)
{
    return false;
}

void CNETSocket::UpdateAddr(void)
{
    if (m_adrLocal.si_family == AF_INET6)
    {
        Int nLen = (Int)sizeof(SOCKADDR_IN6);
        ::getsockname(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv6), &nLen);
    }
    else
    {
        Int nLen = (Int)sizeof(SOCKADDR_IN);
        ::getsockname(m_hSocket, (LPSOCKADDR)&(m_adrLocal.Ipv4), &nLen);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
