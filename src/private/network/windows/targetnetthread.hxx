// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_THREAD_HXX__
#define __TARGET_NETWORK_THREAD_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "thread.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

class CNetManager;
///////////////////////////////////////////////////////////////////
// CNETWorkThread
class CNETWorkThread : public CThread
{
public:
    enum THREAD_WAIT
    {
        THREAD_WAIT_TIME = 100, // MS
    };
public:
    explicit CNETWorkThread(CNetManager* pNetwork);
    virtual ~CNETWorkThread(void);
protected:
    virtual bool Run(void) OVERRIDE;
private:
    bool OnSocketHandle(void);
private:
    CNetManager*         m_pNetwork;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_THREAD_HXX__
