//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by network.rc

#define PRODUCT_MAJOR_VERSION       COMMON_MAJOR_VERSION
#define PRODUCT_MINOR_VERSION       COMMON_MINOR_VERSION
#define PRODUCT_BUILD_VERSION       1
#define PRODUCT_REVISION_VERSION    296
#define PRODUCT_STRING_VER          COMMON_STRING_VERSION ", 1, 296"

#define FILE_YEAR_VERSION           2022
#define FILE_MONTH_VERSION          05
#define FILE_MDAY_VERSION           11
#define FILE_BUILD_VERSION          1 // day the X-th build
#define FILE_STRING_VER             "2022, 05, 11, 1"

#if defined(_UNICODE) || defined(UNICODE)
#define DESC_NAME                   "creek network unicode"
#else
#define DESC_NAME                   "creek network"
#endif // _UNICODE & UNICODE
#define FILE_NAME                   "network.dll"
#define FILE_COMMENTS               "network.dll"

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
