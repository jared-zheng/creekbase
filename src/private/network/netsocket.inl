// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_SOCKET_INL__
#define __NETWORK_SOCKET_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CNETSocket
INLINE CNETSocket::CNETSocket(CNetManager* pNetwork, CEventHandler* pEventHandler)
: m_pNetwork(pNetwork)
, m_pEventHandler(pEventHandler)
// recv buffer, accept buffer(tcp) or connect buffer(tcp)
, m_pCommonBuffer(nullptr)
, m_pJumboBuffer(nullptr)
// send buffers or accept buffers(tcp)
{
}

INLINE CNETSocket::~CNETSocket(void)
{
}

#endif // __NETWORK_SOCKET_INL__
