// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_BUFFER_HXX__
#define __TARGET_NETWORK_BUFFER_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netbase.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETBuffer
class CNETBuffer : public CNETBufferBase
{
public:
    CNETBuffer(size_t stSize, size_t stStyle = BUFFERS_SEND);
    ~CNETBuffer(void);
};

///////////////////////////////////////////////////////////////////
// CNETBufferBroadCast : delete buf ptr verify by IsBroadCast
class CNETBufferBroadCast : public CTNETBufferBroadCast<CNETBuffer>
{
public:
    explicit CNETBufferBroadCast(size_t stSize, size_t stStyle); // broadcast type is auto add
    ~CNETBufferBroadCast(void);
};

///////////////////////////////////////////////////////////////////
#include "targetnetbuffer.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_NETWORK_BUFFER_HXX__
