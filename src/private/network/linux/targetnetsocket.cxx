// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETSocket
bool CNETSocket::SetSendTimeout(Int nTime)
{
    struct timeval tTimeo;
    tTimeo.tv_sec  = (time_t)(nTime / TIMET_S2MS);
    tTimeo.tv_usec = (__suseconds_t)(nTime % TIMET_S2MS) * TIMET_XS2XS; // MS - US
    if (tTimeo.tv_usec >= (__suseconds_t)TIMET_US2SEC)
    {
        tTimeo.tv_usec -= (__suseconds_t)TIMET_US2SEC;
        tTimeo.tv_sec  += 1;
    }
    if (setsockopt(m_hSocket, SOL_SOCKET, SO_SNDTIMEO, (const void*)&tTimeo, (socklen_t)sizeof(timeval)) == RET_ERROR)
    {
        DEV_DEBUG(TF(" NETSocket[%p] set send timeout failed[%d]"), this, errno);
        return false;
    }
    return true;
}

bool CNETSocket::SetRecvTimeout(Int nTime)
{
    struct timeval tTimeo;
    tTimeo.tv_sec  = (time_t)(nTime / TIMET_S2MS);
    tTimeo.tv_usec = (__suseconds_t)(nTime % TIMET_S2MS) * TIMET_XS2XS; // MS - US
    if (tTimeo.tv_usec >= (__suseconds_t)TIMET_US2SEC)
    {
        tTimeo.tv_usec -= (__suseconds_t)TIMET_US2SEC;
        tTimeo.tv_sec  += 1;
    }
    if (::setsockopt(m_hSocket, SOL_SOCKET, SO_RCVTIMEO, (const void*)&tTimeo, (socklen_t)sizeof(timeval)) == RET_ERROR)
    {
        DEV_DEBUG(TF(" NETSocket[%p] set recv timeout failed[%d]"), this, errno);
        return false;
    }
    return true;
}

bool CNETSocket::SetKeepAlive(bool bEnable)
{
    if (bEnable)
    {
        if (IsKeepAlive() == false)
        {
            Int nKeepAlive = TRUE;
            if (setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, (const void*)&nKeepAlive, (socklen_t)sizeof(Int)) == RET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set keepalive true option failed[%d]"), this, errno);
                return false;
            }
            SetTraitsEx(SOCKETT_EX_ALIVE, true);
        }
    }
    else
    {
        if (IsKeepAlive())
        {
            Int nKeepAlive = FALSE;
            if (setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, (const void*)&nKeepAlive, (socklen_t)sizeof(Int)) == RET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set keepalive false option failed[%d]"), this, errno);
                return false;
            }
            SetTraitsEx(SOCKETT_EX_ALIVE, false);
        }
    }
    return true;
}

bool CNETSocket::SetKeepAliveTime(UInt uTime)
{
    if (SetKeepAlive())
    {
        ULLong ullInterval = uTime * NET_DEF_ONE_HALFL;
        ullInterval >>= 10;

        Int nKeepAlive = (Int)uTime / TIMET_XS2XS;
        if (setsockopt(m_hSocket, IPPROTO_TCP, TCP_KEEPIDLE, (const void*)&nKeepAlive, (socklen_t)sizeof(Int)) == RET_ERROR)
        {
            DEV_DEBUG(TF(" NETSocket[%p] set keepalive idle option failed[%d]"), this, errno);
            return false;
        }
        nKeepAlive = (Int)ullInterval / TIMET_XS2XS;
        if (setsockopt(m_hSocket, IPPROTO_TCP, TCP_KEEPINTVL, (const void*)&nKeepAlive, (socklen_t)sizeof(Int)) == RET_ERROR)
        {
            DEV_DEBUG(TF(" NETSocket[%p] set keepalive intvl option failed[%d]"), this, errno);
            return false;
        }
        nKeepAlive = NET_DEF_CNT;
        if (setsockopt(m_hSocket, IPPROTO_TCP, TCP_KEEPCNT, (const void*)&nKeepAlive, (socklen_t)sizeof(Int)) == RET_ERROR)
        {
            DEV_DEBUG(TF(" NETSocket[%p] set keepalive cnt option failed[%d]"), this, errno);
            return false;
        }
        return true;
    }
    return false;
}

bool CNETSocket::Create(Int nType, Int nProtocol)
{
    assert(m_pNetwork      != nullptr);
    assert(m_pEventHandler != nullptr);
    assert(m_hSocket       == INVALID_SOCKET);

    Int nPF   = ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_IPV6) ? PF_INET6 : PF_INET);
    m_hSocket = socket(nPF, nType|SOCK_NONBLOCK, nProtocol);
    if (m_hSocket == INVALID_SOCKET)
    {
        DEV_DEBUG(TF(" NETSocket[%p] create socket[PF : %s---Type : %d---Proto : %d] failed[%d]"), this, ((nPF == PF_INET) ? TF("PF_INET") : TF("PF_INET6")), nType, nProtocol, errno);
        return false;
    }
    m_utId  = (uintptr_t)m_hSocket;
    DEV_DEBUG(TF(" NETSocket[%p] create socket[PF : %s---Type : %d---Proto : %d] successed"), this, ((nPF == PF_INET) ? TF("PF_INET") : TF("PF_INET6")), nType, nProtocol);
    return true;
}

bool CNETSocket::Close(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    SOCKET hSocket = (SOCKET)CAtomics::Exchange64<LLong>((PLLong)&m_hSocket, (LLong)INVALID_SOCKET);
#else
    SOCKET hSocket = (SOCKET)CAtomics::Exchange<Long>((PLong)&m_hSocket, (Long)INVALID_SOCKET);
#endif
    if (hSocket != INVALID_SOCKET)
    {
        DEV_DEBUG(TF(" NETSocket[%p] Shutdown socket[%p] with buffer[%p - %p]"), this, m_utId, m_pCommonBuffer, m_pJumboBuffer);
        shutdown(hSocket, SHUT_RDWR);
        close(hSocket);
        return (m_pEventHandler != nullptr);
    }
    else
    {
        DEV_DEBUG(TF(" NETSocket[%p] Close socket already"), this);
    }
    return false;
}

bool CNETSocket::Destroy(void)
{
    uintptr_t utId = CAtomics::Exchange<uintptr_t>(&m_utId, 0);
    if (utId != 0)
    {
        DEV_DEBUG(TF(" NETSocket[%p] %p Destroyed"), this, utId);
        // buffer
        DestroyQueueBuffer();
        DestroyCommonBuffer();
        DestroyJumboBuffer();
        return true;
    }
    else
    {
        DEV_DEBUG(TF(" NETSocket[%p] %p Destroyed already"), this, utId);
    }
    return false;
}

bool CNETSocket::SetOption(Int nFlag)
{
    if ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_REUSEADDR) != 0)
    {
        Int nData = TRUE;
        setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEADDR, (const void*)&nData, (socklen_t)sizeof(Int));
    }

    Int nBufSize = (Int)m_pNetwork->AdjustSize(CNetManager::ADJUSTS_JUMBO_DATA);
    Int nDefault = 0;

    socklen_t tLen = (socklen_t)sizeof(Int);
    getsockopt(m_hSocket, SOL_SOCKET, SO_RCVBUF, (void*)&nDefault, &tLen);
    if (nDefault < nBufSize)
    {
        setsockopt(m_hSocket, SOL_SOCKET, SO_RCVBUF, (const void*)&nBufSize, (socklen_t)sizeof(Int));
    }

    tLen = (socklen_t)sizeof(Int);
    getsockopt(m_hSocket, SOL_SOCKET, SO_SNDBUF, (void*)&nDefault, &tLen);
    if (nDefault < nBufSize)
    {
        setsockopt(m_hSocket, SOL_SOCKET, SO_SNDBUF, (const void*)&nBufSize, (socklen_t)sizeof(Int));
    }
    if (IsTCP())
    {
        if ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_NAGLE) == 0)
        {
            Int nNoDelay = TRUE;
            setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, (const void*)&nNoDelay, (socklen_t)sizeof(Int));
        }
        if (nFlag == CNETTraits::SOCKET_TCP)
        {
            SetTraitsEx(SOCKETT_EX_UNICAST, true);
        }
    }
    else
    {    assert(IsUDP());
        if (nFlag == CNETTraits::SOCKET_UDP_BROADCAST)
        {
            Int nBroadCast = TRUE;
            if (setsockopt(m_hSocket, SOL_SOCKET, SO_BROADCAST, (const void*)&nBroadCast, (socklen_t)sizeof(Int)) == RET_ERROR)
            {
                DEV_DEBUG(TF(" NETSocket[%p] set broadcast option1 failed[%d]"), this, errno);
                return false;
            }
        }
        else
        {
            SetTraitsEx(SOCKETT_EX_UNICAST, true);
        }
        // ECONNREFUSED
        // A remote host refused to allow the network connection (typically because it is not running the requested service).
        // setsockopt(fd, IPPROTO_IP, IP_RECVERR , &val,sizeof(int));
    }

    //#ifdef TCP_KEEPIDLE
    //            optval = curlx_sltosi(data->set.tcp_keepidle);
    //            KEEPALIVE_FACTOR(optval);
    //            if(setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPIDLE,
    //                (void *)&optval, sizeof(optval)) < 0) {
    //                infof(data, "Failed to set TCP_KEEPIDLE on fd %d\n", sockfd);
    //            }
    //#endif
    //#ifdef TCP_KEEPINTVL
    //            optval = curlx_sltosi(data->set.tcp_keepintvl);
    //            KEEPALIVE_FACTOR(optval);
    //            if(setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPINTVL,
    //                (void *)&optval, sizeof(optval)) < 0) {
    //                infof(data, "Failed to set TCP_KEEPINTVL on fd %d\n", sockfd);
    //            }
    //#endif
    //#ifdef TCP_KEEPALIVE
    //            /* Mac OS X style */
    //            optval = curlx_sltosi(data->set.tcp_keepidle);
    //            KEEPALIVE_FACTOR(optval);
    //            if(setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPALIVE,
    //                (void *)&optval, sizeof(optval)) < 0) {
    //                infof(data, "Failed to set TCP_KEEPALIVE on fd %d\n", sockfd);
    //            }
    //#endif
    return true;
}

bool CNETSocket::BindAddr(PSOCKADDR_INET pSockAddr)
{
    bool bRet = false;
    m_adrLocal = *pSockAddr;
    if (m_adrLocal.si_family == AF_INET)
    {
        bRet = (bind(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv4), (socklen_t)sizeof(sockaddr_in)) == 0);
    }
    else if (m_adrLocal.si_family == AF_INET6)
    {
        bRet = (bind(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv6), (socklen_t)sizeof(sockaddr_in6)) == 0);
    }
    if (bRet)
    {
        SetType(SOCKETT_READY);
        return true;
    }
    DEV_DEBUG(TF(" NETSocket[%p] bind local address failed[%d]"), this, errno);
    return false;
}

bool CNETSocket::BindEvent(bool bAdd, UInt uEvent)
{
    if (bAdd)
    {
        if (m_pNetwork->GetEvent().Ctl(EPOLL_CTL_ADD, uEvent, (Handle)m_hSocket, this))
        {
            DEV_DEBUG(TF(" NETSocket[%p - %p] bind event okay"), this, m_utId);
            return true;
        }
        return false;
    }
    else
    {
        return m_pNetwork->GetEvent().Ctl(EPOLL_CTL_DEL, 0, (Handle)m_hSocket, this);
    }
}

bool CNETSocket::UpdateEvent(UInt uEvent)
{
    return m_pNetwork->GetEvent().Ctl(EPOLL_CTL_MOD, uEvent, (Handle)m_hSocket, this);
}

void CNETSocket::UpdateAddr(void)
{
    if (m_adrLocal.si_family == AF_INET)
    {
        UInt uLen = (UInt)sizeof(sockaddr_in);
        getsockname(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv4), (socklen_t*)&uLen);
    }
    else
    {
        UInt uLen = (UInt)sizeof(sockaddr_in6);
        getsockname(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv6), (socklen_t*)&uLen);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
