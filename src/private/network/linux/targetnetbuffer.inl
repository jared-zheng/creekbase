// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_BUFFER_INL__
#define __TARGET_NETWORK_BUFFER_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CNETBuffer
INLINE CNETBuffer::CNETBuffer(size_t stSize, size_t stStyle)
: CNETBufferBase(stSize, stStyle)
{
    m_pData   = ((PByte)this + sizeof(CNETBufferBroadCast));
    m_Buf.len = (BufSize)m_stSize;
    m_Buf.buf = (BufAddr)m_pData;
}

INLINE CNETBuffer::~CNETBuffer(void)
{
}

///////////////////////////////////////////////////////////////////
// CNETBufferBroadCast : delete buf ptr vertify by IsBroadCast
INLINE CNETBufferBroadCast::CNETBufferBroadCast(size_t stSize, size_t stStyle)
: CTNETBufferBroadCast<CNETBuffer>(stSize, stStyle|BUFFERS_BROADCAST)
{
}

INLINE CNETBufferBroadCast::~CNETBufferBroadCast(void)
{
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_NETWORK_BUFFER_INL__
