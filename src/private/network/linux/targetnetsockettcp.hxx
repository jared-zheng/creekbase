// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_SOCKET_TCP_HXX__
#define __TARGET_NETWORK_SOCKET_TCP_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netsocket.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTCPSocket
class CTCPSocket : public CNETSocket
{
public:
    CTCPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler);//
    virtual  ~CTCPSocket(void);//

    virtual  bool Init(Int nFlag, PSOCKADDR_INET pSockAddr = nullptr) OVERRIDE;
    virtual  bool Exit(bool bAsync = false) OVERRIDE; // return false if send list is not null when async close
    //
    virtual  bool OnEventHandle(UInt uEvents, CNETBuffer*) OVERRIDE;
    virtual  bool OnEventFail(UInt uEvents, CNETBuffer* pBuffer) OVERRIDE;
    //
    bool     Listen(Int nAccept);
    bool     Connect(const SOCKADDR_INET& SockAddr);
    bool     Send(CNETBuffer*& pBuffer);
private:
    bool     AcceptHandle(void);
    bool     ConnectHandle(void);
    bool     SendHandle(void);
    bool     RecvHandle(void);
    //
    bool     GetErrResult(bool& bRet) const;
    //
    bool     SendBufferHandle(CNETBuffer*& pBuffer, ssize_t& sstCur, bool& bRet);
    //
    bool     AsyncListen(void);
    bool     AsyncAccept(Int nDerive);
    bool     AsyncConnect(void);
    bool     AsyncSend(bool bAdd = true);
    bool     AsyncRecv(void);
    //
    bool     AcceptEvent(uintptr_t utAccept);
    bool     ConnectEvent(uintptr_t utError);
    bool     SendEvent(void);
    bool     RecvEvent(CNETTraits::TCP_PARAM& Param);
    void     CloseEvent(void);
    //
    bool     UpdateReadyAccept(SOCKET hAccept);
    bool     UpdateReadyConnect(UInt uError);
    //
    virtual  bool OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly) OVERRIDE;
private:
    TId      m_tidSend;
    TId      m_tidRecv;
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_NETWORK_SOCKET_TCP_HXX__
