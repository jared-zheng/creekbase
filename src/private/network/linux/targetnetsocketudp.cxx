// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUDPSocket
CUDPSocket::CUDPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler)
: CNETSocket(pNetwork, pEventHandler)
, m_tidSendTo(0)
, m_tidRecvFrom(0)
{
}

CUDPSocket::~CUDPSocket(void)
{
}

bool CUDPSocket::Init(Int nFlag, PSOCKADDR_INET pSockAddr)
{
    if (Create(SOCK_DGRAM, IPPROTO_UDP))
    {
        // add  map
        if (Add() == false)
        {
            DEV_DEBUG(TF(" UDPSocket[%p] add to socket map failed"), this);
            return false;
        }
        HandlerAddRef();
        SetTraits(SOCKETT_UDP);
        SetOption(nFlag);
        assert(pSockAddr != nullptr);
        if (BindAddr(pSockAddr) && AsyncRecvFrom())
        {
            // bind pSockAddr maybe any addr, here get exact addr
            UpdateAddr();
            return true;
        }
    }
    return false;
}

bool CUDPSocket::Exit(bool bAsync)
{
    bool bRet = false;
    SetCloseStatus();
    if ((bAsync == false) || (CAtomics::CompareExchange<UInt>((PUInt)&m_nQueueSize, 0, 0) == 0))
    {
        if (Close())
        {
            CloseEvent();
            HandlerRelease();
        }
        if (IsHandleStatus() == false)
        {
            if (Remove()) // remove from map
            {
                DEV_DEBUG(TF(" UDPSocket[%p] exit remove okay!"), this);
            }
            else
            {
                DEV_DEBUG(TF(" UDPSocket[%p] exit detect index is null already!"), this);
            }
            bRet = IsCloseEnable();
        }
    }
    if (bRet)
    {
        return Destroy();
    }
    DEV_DEBUG(TF(" UDPSocket[%p] exit detect lock status[%X] or sending buffer[%d] is not empty, destroy by async work thread"), this, GetStatus(), m_nQueueSize);
    return false;
}

bool CUDPSocket::OnEventHandle(UInt uEvents, CNETBuffer*)
{
    assert(uEvents & (EPOLLIN|EPOLLOUT));
    bool bRet = true;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    if (uEvents & EPOLLIN)
    {
        //assert(IsTrafficType());
        bRet = RecvHandle();
    }
    if (bRet && (uEvents & EPOLLOUT))
    {
        //assert(IsLiveType());
        bRet = SendHandle();
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" UDPSocket[%p] OnEventHandle failed by traits[%d]-type[%d]-status[%X], events=%#X"), this, GetTraits(), GetType(), GetStatus(), uEvents);
    return false;
}

bool CUDPSocket::OnEventFail(UInt, CNETBuffer*)
{
    // bool bRet = false;
    // SetHandleStatus(true);
    // //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    // if (uEvents & EPOLLOUT)
    // {
    //     //assert(IsLiveType());
    //     SetQueueStatus(false); // sendto queue EPOLL status off
    //     //DEV_DEBUG(TF(" UDPSocket[%p] OnEventFail sendto queue EPOLL status off"), this);
    // }
    // uEvents = errno;
    // //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    // SetHandleStatus(false);
    // if (bRet)
    // {
    //     return (IsCloseStatus() == false); // close flag
    // }
    SetPendStatus(false); // connect pend EPOLL status off
    SetQueueStatus(false); // send queue EPOLL status off
    DEV_DEBUG(TF(" UDPSocket[%p] OnEventFail[error=%d] failed by traits[%d]-type[%d]-status[%X]"), this, errno, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CUDPSocket::SendTo(CNETBuffer* pBuffer, const SOCKADDR_INET& SockAddr)
{
    if (IsCloseStatus())
    {
        return false;
    }
    bool bRet = false;
    assert(IsUDP());
    if (IsLiveType())
    {
        LLong llSendTick = CPlatform::GetOSRunningTick();
        Int   nIndex     = AddBuffer(pBuffer, &SockAddr);
        if (nIndex > -1)
        {
            bRet = true;
            if ((nIndex == 0) && (AsyncSendTo() == false))
            {
                bRet = false;
                SetCloseStatus();
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
    }
    else
    {
        DEV_DEBUG(TF(" UDPSocket[%p] sendto failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    }
    return bRet;
}

bool CUDPSocket::SendHandle(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    if (CAtomics::CompareExchange64<TId>(&m_tidSendTo, CPlatform::GetCurrentTId(), 0) != 0)
#else
    if (CAtomics::CompareExchange<TId>(&m_tidSendTo, CPlatform::GetCurrentTId(), 0) != 0)
#endif
    {
        return true;
    }

    bool bRet = false;
    if (IsCloseStatus() == false)
    {
        socklen_t        slSockAddr = 0;
        struct sockaddr* pSockAddr  = nullptr;
        CNETBuffer*      pBuffer    = nullptr;
        GetSendParam(slSockAddr, pSockAddr, pBuffer);

        LLong  llSendTick = CPlatform::GetOSRunningTick();
        ssize_t sstRet = 0;
        ssize_t sstCur = 0;
        ssize_t sstAll = 0;
        if (pBuffer->BlockBufData())
        {
            // exclude head igore
        }
        if (IsExcludeHead())
        {
            DEV_DEBUG(TF(" UDPSocket[%p] SendHandle ExcludeHead Traits"), this);
            sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
        }
        for (;;)
        {
            sstRet = sendto(m_hSocket, pBuffer->GetBufData((size_t)sstCur), pBuffer->GetBufSize((size_t)sstCur), MSG_DONTWAIT, pSockAddr, slSockAddr);
            if (sstRet > 0)
            {
                sstAll += sstRet;
                sstCur += sstRet;
                if (SendBufferHandle(pBuffer, sstCur, bRet, slSockAddr, pSockAddr) == false)
                {
                    break;
                }
            }
            else
            {
                bRet = false;
                if (GetErrResult(bRet))
                {
                    continue;
                }
                if (bRet)
                {
                    pBuffer->SetBufData((size_t)sstCur);
                    //SetQueueStatus(false); // send queue EPOLL status off
                }
                break;
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendSize), (LLong)sstAll);
    }
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    CAtomics::Exchange64<TId>(&m_tidSendTo, 0);
#else
    CAtomics::Exchange<TId>(&m_tidSendTo, 0);
#endif
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" UDPSocket[%p] SendHandle send queue EPOLL status off"), this);
        SetQueueStatus(false); // send queue EPOLL status off
        return false;
    }
    return true;
}

bool CUDPSocket::RecvHandle(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    if (CAtomics::CompareExchange64<TId>(&m_tidRecvFrom, CPlatform::GetCurrentTId(), 0) != 0)
#else
    if (CAtomics::CompareExchange<TId>(&m_tidRecvFrom, CPlatform::GetCurrentTId(), 0) != 0)
#endif
    {
        return true;
    }

    //DEV_DEBUG(TF(" UDPSocket[%p] RecvHandle recv pend EPOLL status on"), this);
    SetPendStatus(true); // recv pend EPOLL status on

    socklen_t        slSockAddr = 0;
    struct sockaddr* pSockAddr  = nullptr;
    CNETBuffer*      pBuffer    = nullptr;
    GetRecvParam(slSockAddr, pSockAddr, pBuffer);

    LLong   llRecvTick = CPlatform::GetOSRunningTick();
    bool    bRet   = false;
    ssize_t sstAll = 0;
    ssize_t sstRet = 0;
    ssize_t sstCur = 0;
    for (;;)
    {
        sstCur = 0;
        if (pBuffer->BlockBufData())
        {
            // exclude head igore
        }
        else if (IsExcludeHead())
        {
            //DEV_DEBUG(TF(" UDPSocket[%p] RecvHandle ExcludeHead Traits"), this);
            sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
        }
        socklen_t slLen = slSockAddr;
        sstRet = recvfrom(m_hSocket, pBuffer->GetBufData((size_t)sstCur), pBuffer->GetBufSize((size_t)sstCur), MSG_DONTWAIT, pSockAddr, &slLen);
        if (sstRet != 0)
        {
            if (sstRet != RET_ERROR)
            {
                sstAll += sstRet;
                if (RecvBufferHandle(pBuffer, (size_t)sstRet))
                {
                    SetLiveTime();
                    continue;
                }
            }
            else if (GetErrResult(bRet))
            {
                continue;
            }
        }
        break;
    }
    llRecvTick = (CPlatform::GetOSRunningTick() - llRecvTick);
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvTick), llRecvTick);
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvSize), (LLong)sstAll);

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    CAtomics::Exchange64<TId>(&m_tidRecvFrom, 0);
#else
    CAtomics::Exchange<TId>(&m_tidRecvFrom, 0);
#endif
    //DEV_DEBUG(TF(" UDPSocket[%p] RecvHandle recv pend EPOLL status off"), this);
    SetPendStatus(false); // recv pend EPOLL status off
    return bRet;
}

bool CUDPSocket::GetSendParam(socklen_t& slSockAddr, struct sockaddr*& pSockAddr, CNETBuffer*& pBuffer) const
{
    const SOCKET_BUFFER& Buffer = m_QueueBuffer.GetHead();
    if (m_adrLocal.si_family == AF_INET6)
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in6);
        pSockAddr  = (struct sockaddr*)&(Buffer.pSockAddr->Ipv6);
    }
    else
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in);
        pSockAddr  = (struct sockaddr*)&(Buffer.pSockAddr->Ipv4);
    }
    pBuffer = Buffer.pBuffer;
    return true;
}

bool CUDPSocket::GetRecvParam(socklen_t& slSockAddr, struct sockaddr*& pSockAddr, CNETBuffer*& pBuffer) const
{
    if (m_adrLocal.si_family == AF_INET6)
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in6);
        pSockAddr  = (struct sockaddr*)&(m_adrRemote.Ipv6);
    }
    else
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in);
        pSockAddr  = (struct sockaddr*)&(m_adrRemote.Ipv4);
    }
    pBuffer = (m_pJumboBuffer == nullptr) ? m_pCommonBuffer : m_pJumboBuffer;
    return true;
}

bool CUDPSocket::GetErrResult(bool& bRet) const
{
    Int nRet = errno;
    if (nRet == EAGAIN) // EWOULDBLOCK
    {
        bRet = true;
    }
    else if (nRet == EINTR)
    {
        return true;
    }
    return false;
}

bool CUDPSocket::SendBufferHandle(CNETBuffer*& pBuffer, ssize_t& sstCur, bool& bRet, socklen_t& slSockAddr, struct sockaddr*& pSockAddr)
{
    if ((size_t)sstCur == pBuffer->GetBufSize()) // current buffer data send complete
    {
        SetLiveTime();
        if (pBuffer->IsClose() == false)
        {
            bRet = true;
            Int nQueue = 0;
            {
                CSyncLockScope scope(m_NSLock);
                nQueue = RemoveBuffer();
                if (nQueue == 0)
                {
                    // send queue is empty, clean send queue status
                    AsyncSendTo(false);
                }
            }
            if (nQueue > 0)
            {
                GetSendParam(slSockAddr, pSockAddr, pBuffer);

                sstCur = 0;
                if (IsExcludeHead())
                {
                    DEV_DEBUG(TF(" UDPSocket[%p] SendHandle ExcludeHead Traits"), this);
                    sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
                }
            }
            else if (IsSendEvent())
            {
                bRet = SendEvent(); // send list buffer is null;
            }
        }
        else
        {
            bRet = false;
            DEV_DEBUG(TF(" UDPSocket[%p] SendBufferHandle, close socket by request"), this);
        }
        return false;
    }
    return true;
}

bool CUDPSocket::AsyncSendTo(bool bAdd)
{
    bool bRet = false;
    if (bAdd)
    {
        CSyncLockScope scope(m_NSLock);
        //DEV_DEBUG(TF(" UDPSocket[%p] AsyncSendTo send queue EPOLL status on"), this);
        SetQueueStatus(true); // send queue EPOLL status on

        //DEV_DEBUG(TF(" UDPSocket[%p] async send add EPOLLOUT(ET mode)"), this);
        bRet = UpdateEvent(NET_DEF_EVENT_IOMODE);
        if (bRet == false)
        {
            //DEV_DEBUG(TF(" UDPSocket[%p] AsyncSendTo send queue EPOLL off"), this);
            SetQueueStatus(false); // send queue EPOLL status off
        }
    }
    else
    {
        assert(m_nQueueSize == 0);
        //DEV_DEBUG(TF(" UDPSocket[%p] AsyncSendTo empty send queue EPOLL status off"), this);
        SetQueueStatus(false); // send queue EPOLL status off

        //DEV_DEBUG(TF(" UDPSocket[%p] async send remove EPOLLOUT(ET mode)"), this);
        bRet = UpdateEvent();
    }
    return bRet;
}

bool CUDPSocket::AsyncRecvFrom(void)
{
    if (CreateCommonBuffer(CNETBuffer::BUFFERS_RECV) && CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend))
    {
        if (BindEvent())
        {
            DEV_DEBUG(TF(" UDPSocket[%p - %p] ready to recv data"), this, m_utId);
            CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llUDPCreateCount), 1);
            SetType(SOCKETT_LIVE);
            return true;
        }
        DestroyCommonBuffer();
    }
    return false;
}

bool CUDPSocket::SendEvent(void)
{
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_SEND, (uintptr_t)m_pNetwork->GetAttr().nMaxSend, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CUDPSocket::RecvEvent(CNETTraits::UDP_PARAM& Param)
{
    //////
    // work thread <--> logic thread
    if (IsRecvEvent())
    {
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_RECV, (uintptr_t)Param.nSize, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    else
    {
        CBufReadStream BufReadStream((size_t)Param.nSize, Param.pData);
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_RECV, BufReadStream, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    //////
}

void CUDPSocket::CloseEvent(void)
{
    if (IsExitStatus() == false)
    {
        DEV_DEBUG(TF(" UDPSocket[%p - %p] CloseEvent traits[%d]-state[%d]-status[%X] %p"), this, m_utId, GetTraits(), GetType(), GetStatus(), m_pEventHandler);
        //////
        // work thread <--> logic thread
        m_pEventHandler->OnHandle(CNETTraits::EVENT_UDP_CLOSE, m_utId, m_ullLiveData);
        //////
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llUDPCloseCount), 1);
    }
}

bool CUDPSocket::OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly)
{
    CNETTraits::UDP_PARAM Param;
    m_pNetwork->GetSocketAddr(Param.NetAddr, GetAddr(true));
    Param.sSocket = (CNETTraits::Socket)m_utId;
    Param.pData   = pData;
    Param.nSize   = (Int)stSize;
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_UDP;
    if (pBuffer->IsJumbo() == false)
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_BUFFER;
        Param.index   = m_pNetwork->GetBufferIndex();
        Param.nOffset = m_pNetwork->GetAttr().nBufferOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxBuffer;
    }
    else
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_JUMBOBUF;
        Param.index   = m_pNetwork->GetJumboIndex();
        Param.nOffset = m_pNetwork->GetAttr().nJumboOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxJumbo;
    }
    if (bOnly)
    {
        Param.pCache = (PByte)pBuffer;
    }
    bool bRet = RecvEvent(Param);
    if (bOnly)
    {
        pData = Param.pCache;
    }
    return bRet;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
