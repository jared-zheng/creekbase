// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetManager
bool CNetManager::GetLocalAddr(ARY_STRING& strAddrs, Int nAttr) const
{
    struct ifaddrs* pIPAddr = nullptr;
    if (getifaddrs(&pIPAddr) == RET_ERROR)
    {
        DEV_DEBUG(TF(" GetLocalAddr1 get ifaddrs failed[%d]"), errno);
        return false;
    }

    sa_family_t  si_family = ((nAttr & ATTR_IPV6) != 0) ? AF_INET6 : AF_INET;

    CString strAddr;
    UShort  usPort = 0;

    for (struct ifaddrs* pIP = pIPAddr; pIP != nullptr; pIP = pIP->ifa_next)
    {
        if (pIP->ifa_addr == nullptr)
        {
            continue;
        }
        if (pIP->ifa_addr->sa_family == si_family)
        {
            if (GetSocketAddr(strAddr, usPort, *((PSOCKADDR_INET)(pIP->ifa_addr))))
            {
                strAddrs.Add(strAddr);
            }
        }
    }
    freeifaddrs(pIPAddr);
    return (strAddrs.GetSize() > 0);
}

bool CNetManager::GetLocalAddr(ARY_NETADDR& NetAddrs, Int nAttr) const
{
    struct ifaddrs* pIPAddr = nullptr;
    if (getifaddrs(&pIPAddr) == RET_ERROR)
    {
        DEV_DEBUG(TF(" GetLocalAddr2 get ifaddrs failed[%d]"), errno);
        return false;
    }

    sa_family_t  si_family = ((nAttr & ATTR_IPV6) != 0) ? AF_INET6 : AF_INET;

    NET_ADDR NetAddr;
    NetAddr.usAttr = (UShort)nAttr;

    for (struct ifaddrs* pIP = pIPAddr; pIP != nullptr; pIP = pIP->ifa_next)
    {
        if (pIP->ifa_addr == nullptr)
        {
            continue;
        }
        if (pIP->ifa_addr->sa_family == si_family)
        {
            if (GetSocketAddr(NetAddr, *((PSOCKADDR_INET)(pIP->ifa_addr))))
            {
                NetAddrs.Add(NetAddr);
            }
        }
    }
    freeifaddrs(pIPAddr);
    return (NetAddrs.GetSize() > 0);
}

bool CNetManager::GetRemoteAddr(PCXStr pszRemoteAddr, ARY_STRING& strAddrs, Int nAttr) const
{
    struct addrinfo* pResult = nullptr;
    struct addrinfo  aiHints = { 0 };
    aiHints.ai_family  = ((nAttr & ATTR_IPV6) != 0) ? AF_INET6 : AF_INET;
    //aiHints.ai_flags   = AI_PASSIVE;
    Int nRet = getaddrinfo(pszRemoteAddr, nullptr, &aiHints, &pResult);
    if (nRet != RET_OKAY)
    {
        DEV_DEBUG(TF(" GetRemoteAddr1 get address[%s] failed[%d]"), pszRemoteAddr, nRet);
        return false;
    }
    CString strAddr;
    UShort  usPort = 0;
    for (struct addrinfo* p = pResult; p != nullptr; p = p->ai_next)
    {
        if (GetSocketAddr(strAddr, usPort, *((SOCKADDR_INET*)(p->ai_addr))))
        {
            strAddrs.Add(strAddr);
        }
    }
    freeaddrinfo(pResult);
    return true;
}

bool CNetManager::GetRemoteAddr(PCXStr pszRemoteAddr, ARY_NETADDR& NetAddrs, Int nAttr) const
{
    struct addrinfo* pResult = nullptr;
    struct addrinfo  aiHints = { 0 };
    aiHints.ai_family  = ((nAttr & ATTR_IPV6) != 0) ? AF_INET6 : AF_INET;
    //aiHints.ai_flags   = AI_PASSIVE;
    Int nRet = getaddrinfo(pszRemoteAddr, nullptr, &aiHints, &pResult);
    if (nRet != RET_OKAY)
    {
        DEV_DEBUG(TF(" GetRemoteAddr2 get address[%s] failed[%d]"), pszRemoteAddr, nRet);
        return false;
    }
    NET_ADDR NetAddr;
    NetAddr.usAttr = (UShort)nAttr;
    for (struct addrinfo* p = pResult; p != nullptr; p = p->ai_next)
    {
        if (GetSocketAddr(NetAddr, *((SOCKADDR_INET*)(p->ai_addr))))
        {
            NetAddrs.Add(NetAddr);
        }
    }
    freeaddrinfo(pResult);
    return true;
}

bool CNetManager::GetSocketAddr(CString& strAddr, UShort& usPort, const SOCKADDR_INET& SockAddr) const
{
    if (SockAddr.si_family != 0)
    {
        XChar szAddr[LMT_MIN] = { 0 };
        if (SockAddr.si_family == AF_INET6)
        {
            usPort = ntohs(SockAddr.Ipv6.sin6_port);
            if (_tinet_ntop(AF_INET6, &(SockAddr.Ipv6.sin6_addr), szAddr, LMT_MIN) != nullptr)
            {
                strAddr = szAddr;
                return true;
            }
        }
        else // AF_INET
        {
            usPort = ntohs(SockAddr.Ipv4.sin_port);
            if (_tinet_ntop(AF_INET, &(SockAddr.Ipv4.sin_addr), szAddr, LMT_MIN) != nullptr)
            {
                strAddr = szAddr;
                return true;
            }
        }
        DEV_DEBUG(TF(" GetSocketAddr1 translate address failed[%d]"), errno);
    }
    else
    {
        DEV_DEBUG(TF(" GetSocketAddr1 SOCKADDR_INET invalid"));
    }
    return false;
}

bool CNetManager::GetSocketAddr(NET_ADDR& NetAddrRef, const SOCKADDR_INET& SockAddr) const
{
    if (SockAddr.si_family != 0)
    {
        if (SockAddr.si_family == AF_INET6)
        {
            if (NetAddrRef.usAttr & ATTR_NETORDER)
            {
                NetAddrRef.usAttr = ATTR_IPV6 | ATTR_NETORDER;
                NetAddrRef.usPort = SockAddr.Ipv6.sin6_port;
            }
            else
            {
                NetAddrRef.usAttr = ATTR_IPV6;
                NetAddrRef.usPort = ntohs(SockAddr.Ipv6.sin6_port);
            }
            MM_SAFE::Cpy(NetAddrRef.Addr.bAddr, NET_ADDR::LEN_BYTE, &(SockAddr.Ipv6.sin6_addr), sizeof(in6_addr));
        }
        else // AF_INET
        {
            if (NetAddrRef.usAttr & ATTR_NETORDER)
            {
                NetAddrRef.usAttr = ATTR_IPV4 | ATTR_NETORDER;
                NetAddrRef.usPort = SockAddr.Ipv4.sin_port;
            }
            else
            {
                NetAddrRef.usAttr = ATTR_IPV4;
                NetAddrRef.usPort = ntohs(SockAddr.Ipv4.sin_port);
            }
            MM_SAFE::Cpy(NetAddrRef.Addr.bAddr, NET_ADDR::LEN_BYTE, &(SockAddr.Ipv4.sin_addr), sizeof(in_addr));
        }
        return true;
    }
    else
    {
        DEV_DEBUG(TF(" GetSocketAddr2 SOCKADDR_INET invalid"));
    }
    return false;
}

bool CNetManager::SetSocketAddr(UShort usPort, PCXStr pszAddr, SOCKADDR_INET& SockAddr, Int nAttr) const
{
    MM_SAFE::Set(&SockAddr, 0, sizeof(SOCKADDR_INET));
    if (nAttr & ATTR_IPV6)
    {
        SockAddr.si_family = AF_INET6;
        if ((pszAddr == nullptr) || (*pszAddr == 0))
        {
            SockAddr.Ipv6.sin6_addr = in6addr_any;
        }
        else
        {
            struct addrinfo* pResult = nullptr;
            struct addrinfo  aiHints = { 0 };
            aiHints.ai_family  = AF_INET6;
            aiHints.ai_flags   = AI_PASSIVE;

            Int nRet = getaddrinfo(pszAddr, nullptr, &aiHints, &pResult);
            if (nRet != RET_OKAY)
            {
                DEV_DEBUG(TF(" SetSocketAddr1 translate address[%s:%d] failed[%d]"), pszAddr, usPort, nRet);
                return false;
            }
            SockAddr.Ipv6 = *((sockaddr_in6*)(pResult->ai_addr));
            //SockAddr.Ipv6.sin6_addr = ((sockaddr_in6*)(pResult->ai_addr))->sin6_addr;
            freeaddrinfo(pResult);
        }
        SockAddr.Ipv6.sin6_port = htons(usPort);
    }
    else
    {
        SockAddr.si_family = AF_INET;
        if ((pszAddr == nullptr) || (*pszAddr == 0))
        {
            SockAddr.Ipv4.sin_addr.s_addr = INADDR_ANY;
        }
        else
        {
            struct addrinfo* pResult = nullptr;
            struct addrinfo  aiHints = { 0 };
            aiHints.ai_family  = AF_INET;
            aiHints.ai_flags   = AI_PASSIVE;

            Int nRet = getaddrinfo(pszAddr, nullptr, &aiHints, &pResult);
            if (nRet != RET_OKAY)
            {
                DEV_DEBUG(TF(" SetSocketAddr1 translate address[%s:%d] failed[%d]"), pszAddr, usPort, nRet);
                return false;
            }
            SockAddr.Ipv4 = *((sockaddr_in*)(pResult->ai_addr));
            //SockAddr.Ipv4.sin_addr = ((sockaddr_in*)(pResult->ai_addr))->sin_addr;
            freeaddrinfo(pResult);
        }
        SockAddr.Ipv4.sin_port = htons(usPort);
    }
    return true;
}

bool CNetManager::SetSocketAddr(const NET_ADDR& NetAddrRef, SOCKADDR_INET& SockAddr, Int nAttr) const
{
    if (NetAddrRef.IsValid())
    {
        MM_SAFE::Set(&SockAddr, 0, sizeof(SOCKADDR_INET));
        if ((nAttr & ATTR_IPV6) && (NetAddrRef.usAttr & ATTR_IPV6))
        {
            SockAddr.si_family = AF_INET6;
            MM_SAFE::Cpy(&(SockAddr.Ipv6.sin6_addr), sizeof(in6_addr), NetAddrRef.Addr.bAddr, sizeof(in6_addr));
            if (NetAddrRef.usAttr & ATTR_NETORDER)
            {
                SockAddr.Ipv6.sin6_port = NetAddrRef.usPort;
            }
            else
            {
                SockAddr.Ipv6.sin6_port = htons(NetAddrRef.usPort);
            }
            return true;
        }
        else if (NetAddrRef.usAttr & ATTR_IPV4)
        {
            SockAddr.si_family = AF_INET;
            MM_SAFE::Cpy(&(SockAddr.Ipv4.sin_addr), sizeof(in_addr), NetAddrRef.Addr.bAddr, sizeof(in_addr));
            if (NetAddrRef.usAttr & ATTR_NETORDER)
            {
                SockAddr.Ipv4.sin_port = NetAddrRef.usPort;
            }
            else
            {
                SockAddr.Ipv4.sin_port = htons(NetAddrRef.usPort);
            }
            return true;
        }
    }
    return false;
}

bool CNetManager::CreateBuffer(size_t stStyle, CNETBuffer*& pBuffer, PByte pCache, bool bBroadCast) const
{
    PINDEX      index       = m_inxBuffer;
    ADJUST_SIZE eAdjustSize = ADJUSTS_BUFFER_DATA;
    if (stStyle & CNETBuffer::BUFFERS_JUMBO)
    {
        index       = m_inxJumbo;
        eAdjustSize = ADJUSTS_JUMBO_DATA;
    }
    if (pCache == nullptr)
    {
        pCache = MCAlloc(index);
    }
    if (pCache != nullptr)
    {
        if (bBroadCast == false)
        {
            pBuffer = new(pCache) CNETBuffer(AdjustSize(eAdjustSize), stStyle); // from cache
        }
        else
        {
            pBuffer = new(pCache) CNETBufferBroadCast(AdjustSize(eAdjustSize), stStyle); // from cache
        }
    }
    else
    {
        pBuffer = nullptr;
        DEV_DEBUG(TF(" CreateBuffer Alloc failed"));
    }
    return (pBuffer != nullptr);
}

bool CNetManager::DestroyBuffer(CNETBuffer*& pBuffer, bool bFinished) const
{
    PINDEX index = m_inxBuffer;
    if (pBuffer->IsJumbo())
    {
        index = m_inxJumbo;
    }
    if (pBuffer->IsBroadCast() == false)
    {
        delete pBuffer;
    }
    else
    {
        CNETBufferBroadCast* pBufferBroadCast = static_cast<CNETBufferBroadCast*>(pBuffer);
        if ((bFinished == false) && (pBufferBroadCast->Continue() == true))
        {
            return false;
        }
        //DEV_DEBUG(TF(" %p BroadCastBuffer destroy %p --- %p"), pBufferBroadCast, pBufferBroadCast->GetEntry(), pBufferBroadCast->Index(1));
        delete pBufferBroadCast;
    }
    MCFree(index, (PByte)pBuffer); // from cache
    pBuffer = nullptr;
    return true;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
