// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETWorkThread
CNETWorkThread::CNETWorkThread(CNetManager* pNetwork)
: m_pNetwork(pNetwork)
, m_pEventEntry(nullptr)
, m_nEventCount(0)
{
    m_nEventCount = m_pNetwork->GetAttr().nMaxEvent;
    m_pEventEntry = (struct epoll_event*)ALLOC( m_nEventCount * sizeof(struct epoll_event) );
    DEV_DEBUG(TF(" work thread create epoll_event count=%d"), m_nEventCount);
}

CNETWorkThread::~CNETWorkThread(void)
{
    if (m_pEventEntry != nullptr)
    {
        FREE( m_pEventEntry );
        m_pEventEntry = nullptr;
    }
    m_nEventCount = 0;
}

CNETWorkThread::CNETWorkThread(const CNETWorkThread&)
: m_pNetwork(nullptr)
, m_pEventEntry(nullptr)
, m_nEventCount(0)
{
}

CNETWorkThread& CNETWorkThread::operator=(const CNETWorkThread&)
{
    return (*this);
}

bool CNETWorkThread::OnStart(void)
{
    return (m_pEventEntry != nullptr);
}

bool CNETWorkThread::Run(void)
{
    if (m_pNetwork->CheckStatus() == false)
    {
        DEV_DEBUG(TF(" work thread get network manager exit, stop thread"));
        return false;
    }

    UpdateWaitStatus();
    Int nCount = m_pNetwork->GetEvent().Wait(m_pEventEntry, m_nEventCount);
    if (nCount == RET_ERROR)
    {
        nCount = errno;
        DEV_DEBUG(TF(" work thread get status fail[%#d]"), nCount);
        UpdateWaitStatus(false);
        return (nCount == EINTR);
    }
    UpdateWaitStatus(false);
    return OnSocketHandleEpoll(nCount);
}

bool CNETWorkThread::OnSocketHandleEpoll(Int nCount)
{
    UInt        uEvents = 0;
    CNETSocket* pSocket = nullptr;
    for (Int i = 0; i < nCount; ++i)
    {
        if (m_pEventEntry[i].data.ptr == nullptr)
        {
            DEV_DEBUG(TF(" work thread get exit flag[%d---%d], stop thread"), i, nCount);
            return false;
        }

        uEvents = m_pEventEntry[i].events;
        pSocket = reinterpret_cast<CNETSocket*>(m_pEventEntry[i].data.ptr);

        if (pSocket->Check())
        {
            if ((uEvents & (EPOLLERR|EPOLLHUP)) == 0)
            {
                //DEV_DEBUG(TF(" work thread get status okay, socket[%p] with event[%#X]"), pSocket, uEvents);
                if (pSocket->OnEventHandle(uEvents, nullptr) == false)
                {
                    m_pNetwork->DestroySocket(pSocket, false);
                }
            }
            else if (pSocket->OnEventFail(uEvents, nullptr) == false)
            {
                m_pNetwork->DestroySocket(pSocket, false);
            }
        }
        else
        {
            DEV_DEBUG(TF(" work thread get status, socket[%p] is invalid"), pSocket);
        }
    }
    return true;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
