// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netmanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTCPSocket
CTCPSocket::CTCPSocket(CNetManager* pNetwork, CEventHandler* pEventHandler)
: CNETSocket(pNetwork, pEventHandler)
, m_tidSend(0)
, m_tidRecv(0)
{
}

CTCPSocket::~CTCPSocket(void)
{
}

bool CTCPSocket::Init(Int nFlag, PSOCKADDR_INET pSockAddr)
{
    if (nFlag > (Int)CNETTraits::SOCKET_LIVEDATA)
    {
        if (Create(SOCK_STREAM, IPPROTO_TCP))
        {
            // add  map
            if (Add() == false)
            {
                DEV_DEBUG(TF(" TCPSocket[%p] add to socket map failed"), this);
                return false;
            }
            HandlerAddRef();
            SetTraits(SOCKETT_TCP);
            SetOption(nFlag);
            assert(pSockAddr != nullptr);
            return BindAddr(pSockAddr);
        }
    }
    else
    {
        assert(pSockAddr != nullptr);
        assert(pSockAddr->si_family == AF_UNSPEC);
        return UpdateReadyAccept(pSockAddr->Accept.hAccept);
    }
    return false;
}

bool CTCPSocket::Exit(bool bAsync)
{
    bool bRet = false;
    SetCloseStatus();
    if ((bAsync == false) || (CAtomics::CompareExchange<UInt>((PUInt)&m_nQueueSize, 0, 0) == 0))
    {
        if (Close())
        {
            CloseEvent();
            HandlerRelease();
        }
        if (IsHandleStatus() == false)
        {
            if (Remove()) // remove from map
            {
                DEV_DEBUG(TF(" TCPSocket[%p] exit remove okay!"), this);
            }
            else
            {
                DEV_DEBUG(TF(" TCPSocket[%p] exit detect index is null already!"), this);
            }
            bRet = IsCloseEnable();
        }
    }
    if (bRet)
    {
        return Destroy();
    }
    DEV_DEBUG(TF(" TCPSocket[%p] exit detect lock status[%X] or sending buffer[%d] is not empty, destroy by async work thread"), this, GetStatus(), m_nQueueSize);
    return false;
}

bool CTCPSocket::OnEventHandle(UInt uEvents, CNETBuffer*)
{
    //DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle traits[%d]-type[%d]-status[%X], events=%#X"), this, GetTraits(), GetType(), GetStatus(), uEvents);
    bool bRet = true;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    if (uEvents & EPOLLIN)
    {
        bRet = false;
        if (IsTCPListen())
        {
            assert(IsLiveType());
            bRet = AcceptHandle();
#ifdef __RUNTIME_DEBUG__
            if (bRet == false) DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle EPOLLIN========= AcceptHandle return false"), this);
#endif
        }
        else if (IsTrafficType())
        {
            assert(IsTCPTraffic());
            bRet = RecvHandle();
#ifdef __RUNTIME_DEBUG__
            if (bRet == false) DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle EPOLLIN========= RecvHandle return false"), this);
#endif
        }
    }
    if (bRet && (uEvents & EPOLLOUT))
    {
        bRet = false;
        if (IsTrafficType())
        {
            assert(IsTCPTraffic());
            bRet = SendHandle();
#ifdef __RUNTIME_DEBUG__
            if (bRet == false) DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle EPOLLOUT========= SendHandle return false"), this);
#endif
        }
        else if (IsReadyType())
        {
            assert(IsTCPConnect());
            bRet = ConnectHandle();
#ifdef __RUNTIME_DEBUG__
            if (bRet == false) DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle EPOLLOUT========= ConnectHandle return false"), this);
#endif
        }
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" TCPSocket[%p] OnEventHandle failed by traits[%d]-type[%d]-status[%X], events=%#X"), this, GetTraits(), GetType(), GetStatus(), uEvents);
    return false;
}

bool CTCPSocket::OnEventFail(UInt uEvents, CNETBuffer*)
{
    bool bRet = false;
    SetHandleStatus(true);
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail epoll events=%X"), this, uEvents);
    if (uEvents & EPOLLOUT)
    {
        uEvents = errno;
        if (IsReadyType() && IsTCPConnect())
        {
            //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail connect pend EPOLL status off---%d"), this, uEvents);
            SetPendStatus(false); // connect pend EPOLL status off

            socklen_t tLen = (socklen_t)sizeof(UInt);
            getsockopt(m_hSocket, SOL_SOCKET, SO_ERROR, &uEvents, &tLen);
            DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail connect failed[error=%d] by traits[%d]-type[%d]-status[%X]"), this, uEvents, GetTraits(), GetType(), GetStatus());

            bRet = UpdateReadyConnect(uEvents);
        }
        else
        {
            assert(IsTrafficType());
            assert(IsTCPTraffic());
            SetQueueStatus(false); // send queue EPOLL status off
            //DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail send queue EPOLL status off"), this);
        }
    }
    else
    {
        SetPendStatus(false); // connect pend EPOLL status off
        SetQueueStatus(false); // send queue EPOLL status off
        uEvents = errno;
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    SetHandleStatus(false);
    if (bRet)
    {
        return (IsCloseStatus() == false); // close flag
    }
    DEV_DEBUG(TF(" TCPSocket[%p] OnEventFail[error=%d] failed by traits[%d]-type[%d]-status[%X]"), this, uEvents, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::Listen(Int nAccept)
{
    if (IsCloseStatus())
    {
        return false;
    }
    if (IsReadyType())
    {
        assert(IsTCP());
        SetTraits(SOCKETT_TCP_LISTEN);
        if (listen(m_hSocket, nAccept) == RET_ERROR)
        {
            DEV_DEBUG(TF(" TCPSocket[%p] listen failed[%d]"), this, errno);
            return false;
        }
        if (CreateQueueBuffer(nAccept))
        {
            return AsyncListen();
        }
    }
    DEV_DEBUG(TF(" TCPSocket[%p] listen failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::Connect(const SOCKADDR_INET& SockAddr)
{
    if (IsCloseStatus())
    {
        return false;
    }
    if (IsReadyType())
    {
        assert(IsTCP());
        SetTraits(SOCKETT_TCP_CONNECT);
        m_adrRemote = SockAddr;
        return AsyncConnect();
    }
    DEV_DEBUG(TF(" TCPSocket[%p] connect failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    return false;
}

bool CTCPSocket::Send(CNETBuffer*& pBuffer)
{
    if (IsCloseStatus())
    {
        return false;
    }
    bool bRet = false;
    assert(IsTCPTraffic());
    if (IsTrafficType())
    {
        LLong llSendTick = CPlatform::GetOSRunningTick();
        Int   nIndex     = AddBuffer(pBuffer);
        if (nIndex > -1)
        {
            bRet = true;
            if ((nIndex == 0) && (AsyncSend() == false))
            {
                bRet = false;
                SetCloseStatus();
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
    }
    else
    {
        DEV_DEBUG(TF(" TCPSocket[%p] send failed by traits[%d]-type[%d]-status[%X]"), this, GetTraits(), GetType(), GetStatus());
    }
    return bRet;
}

bool CTCPSocket::AcceptHandle(void)
{
    //DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandle queue EPOLL status on"), this);
    SetQueueStatus(true); // listen queue EPOLL status on

    SOCKADDR_INET SockAddr;
    SockAddr.si_family = AF_UNSPEC;

    bool bRet = false;
    for (;;)
    {
        SetLiveTime();
        SockAddr.Accept.hAccept = accept4(m_hSocket, nullptr, nullptr, SOCK_NONBLOCK);
        if (SockAddr.Accept.hAccept != RET_ERROR)
        {
            CTCPSocket* pAccept = static_cast<CTCPSocket*>(m_pNetwork->CreateSocket(CNETTraits::SOCKET_LIVEDATA, m_pEventHandler, &SockAddr));
            if (pAccept != nullptr)
            {
                bool bRet = AcceptEvent(pAccept->GetId()) ? pAccept->AsyncAccept(GetTraitsEx(true)) : false;
                if (bRet == false)
                {
                    DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandle[accept socket %p] AcceptEvent return false to close accept socket"), this, pAccept);
                    m_pNetwork->DestroySocket(pAccept, false);
                }
                continue;
            }
        }
        else
        {
            switch (errno)
            {
                case EAGAIN: // EWOULDBLOCK
                case ECONNABORTED:
                case ENETDOWN:
                case EPROTO:
                case ENOPROTOOPT:
                case EHOSTDOWN:
                case ENONET:
                case EHOSTUNREACH:
                case EOPNOTSUPP:
                case ENETUNREACH:
                {
                    bRet = true;
                }
                break;
                case EINTR:
                {
                    continue;
                }
                break;
                default:
                {
                }
            }
            DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandle accept4 return[%d]"), this, errno);
        }
        break;
    }
    //DEV_DEBUG(TF(" TCPSocket[%p] AcceptHandle queue EPOLL status off"), this);
    SetQueueStatus(false); // listen queue EPOLL status off
    return bRet;
}

bool CTCPSocket::ConnectHandle(void)
{
    //DEV_DEBUG(TF(" TCPSocket[%p] ConnectHandle connect pend EPOLL status off"), this);
    SetPendStatus(false);  // connect pend EPOLL status off
    // 0.
    Int       nErr = RET_ERROR;
    socklen_t tLen = (socklen_t)sizeof(Int);
    getsockopt(m_hSocket, SOL_SOCKET, SO_ERROR, &nErr, &tLen);
    if (nErr == RET_OKAY)
    {
        // 1. update conext
        UpdateAddr();

        //SetPendStatus(true);  // connect pend EPOLL status off
        UpdateEvent();
        DEV_DEBUG(TF(" TCPSocket[%p] async connect remove EPOLLOUT(ET mode)"), this);
        // 2. issue recv
        SetLiveTime();
        SetType(SOCKETT_LIVE);
        // 3.
        if (CreateCommonBuffer(CNETBuffer::BUFFERS_RECV) && CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend))
        {
            return ConnectEvent(0);
        }
    }
    else
    {
        if (UpdateReadyConnect(nErr))
        {
            return true;
        }
    }
    return false;
}

bool CTCPSocket::SendHandle(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    if (CAtomics::CompareExchange64<TId>(&m_tidSend, CPlatform::GetCurrentTId(), 0) != 0)
#else
    if (CAtomics::CompareExchange<TId>(&m_tidSend, CPlatform::GetCurrentTId(), 0) != 0)
#endif
    {
        return true;
    }

    bool bRet = false;
    if (IsCloseStatus() == false)
    {
        CNETBuffer* pBuffer = m_QueueBuffer.GetHead().pBuffer;

        LLong  llSendTick = CPlatform::GetOSRunningTick();
        ssize_t sstRet = 0;
        ssize_t sstCur = 0;
        ssize_t sstAll = 0;
        if (pBuffer->BlockBufData())
        {
            // exclude head igore
        }
        else if (IsExcludeHead())
        {
            //DEV_DEBUG(TF(" TCPSocket[%p] SendHandle ExcludeHead Traits"), this);
            sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
        }
        for (;;)
        {
            sstRet = send(m_hSocket, pBuffer->GetBufData((size_t)sstCur), pBuffer->GetBufSize((size_t)sstCur), MSG_DONTWAIT);
            if (sstRet > 0)
            {
                sstAll += sstRet;
                sstCur += sstRet;
                if (SendBufferHandle(pBuffer, sstCur, bRet) == false)
                {
                    break;
                }
            }
            else
            {
                bRet = false;
                if (GetErrResult(bRet))
                {
                    continue;
                }
                if (bRet)
                {
                    pBuffer->SetBufData((size_t)sstCur);
                    //SetQueueStatus(false); // send queue EPOLL status off
                }
                break;
            }
        }
        llSendTick = (CPlatform::GetOSRunningTick() - llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendTick), llSendTick);
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llSendSize), (LLong)sstAll);
    }
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    CAtomics::Exchange64<TId>(&m_tidSend, 0);
#else
    CAtomics::Exchange<TId>(&m_tidSend, 0);
#endif
    if (bRet == false)
    {
        //DEV_DEBUG(TF(" TCPSocket[%p] SendHandle send queue EPOLL status off"), this);
        SetQueueStatus(false); // send queue EPOLL status off
        return false;
    }
    return true;
}

bool CTCPSocket::RecvHandle(void)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    if (CAtomics::CompareExchange64<TId>(&m_tidRecv, CPlatform::GetCurrentTId(), 0) != 0)
#else
    if (CAtomics::CompareExchange<TId>(&m_tidRecv, CPlatform::GetCurrentTId(), 0) != 0)
#endif
    {
        return true;
    }

    //DEV_DEBUG(TF(" TCPSocket[%p] RecvHandle recv pend EPOLL status on"), this);
    SetPendStatus(true); // recv pend EPOLL status on

    CNETBuffer* pBuffer = (m_pJumboBuffer == nullptr) ? m_pCommonBuffer : m_pJumboBuffer;
    LLong   llRecvTick  = CPlatform::GetOSRunningTick();
    bool    bRet   = false;
    ssize_t sstAll = 0;
    ssize_t sstRet = 0;
    ssize_t sstCur = 0;
    for (;;)
    {
        sstCur = 0;
        if (pBuffer->BlockBufData())
        {
            // exclude head igore
        }
        else if (IsExcludeHead())
        {
            //DEV_DEBUG(TF(" TCPSocket[%p] RecvHandle ExcludeHead Traits"), this);
            sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
        }
        sstRet = recv(m_hSocket, pBuffer->GetBufData((size_t)sstCur), pBuffer->GetBufSize((size_t)sstCur), MSG_DONTWAIT);
        if (sstRet != 0)
        {
            if (sstRet != RET_ERROR)
            {
                sstAll += sstRet;
                if (RecvBufferHandle(pBuffer, (size_t)sstRet))
                {
                    SetLiveTime();
                    continue;
                }
            }
            else if (GetErrResult(bRet))
            {
                continue;
            }
        }
        break;
    }
    llRecvTick = (CPlatform::GetOSRunningTick() - llRecvTick);
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvTick), llRecvTick);
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llRecvSize), (LLong)sstAll);

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    CAtomics::Exchange64<TId>(&m_tidRecv, 0);
#else
    CAtomics::Exchange<TId>(&m_tidRecv, 0);
#endif
    //DEV_DEBUG(TF(" TCPSocket[%p] RecvHandle recv pend EPOLL status off"), this);
    SetPendStatus(false); // recv pend EPOLL status off
    return bRet;
}

bool CTCPSocket::GetErrResult(bool& bRet) const
{
    Int nRet = errno;
    if (nRet == EAGAIN) // EWOULDBLOCK
    {
        bRet = true;
    }
    else if (nRet == EINTR)
    {
        return true;
    }
    return false;
}

bool CTCPSocket::SendBufferHandle(CNETBuffer*& pBuffer, ssize_t& sstCur, bool& bRet)
{
    if ((size_t)sstCur == pBuffer->GetBufSize()) // current buffer data send complete
    {
        SetLiveTime();
        if (pBuffer->IsClose() == false)
        {
            bRet = true;
            Int nQueue = 0;
            {
                CSyncLockScope scope(m_NSLock);
                nQueue = RemoveBuffer();
                if (nQueue == 0)
                {
                    // send queue is empty, clean send queue status
                    AsyncSend(false);
                }
            }
            if (nQueue > 0)
            {
                DEV_DEBUG(TF(" TCPSocket[%p] SendBufferHandle okay, send next in queue[%d]"), this, nQueue);
                pBuffer = m_QueueBuffer.GetHead().pBuffer;

                sstCur = 0;
                if (IsExcludeHead())
                {
                    //DEV_DEBUG(TF(" TCPSocket[%p] SendBufferHandle ExcludeHead Traits"), this);
                    sstCur = (ssize_t)(m_pNetwork->GetPackBuild()->OffsetSize());
                }
                return true;
            }
            else if (IsSendEvent())
            {
                bRet = SendEvent(); // send list buffer is null
            }
        }
        else
        {
            bRet = false;
            DEV_DEBUG(TF(" TCPSocket[%p] SendBufferHandle, close socket by request"), this);
        }
        return false;
    }
    return true;
}

bool CTCPSocket::AsyncListen(void)
{
    assert(m_nQueueSize == 0);
    if (BindEvent())
    {
        DEV_DEBUG(TF(" TCPSocket[%p - %p] ready to accept"), this, m_utId);
        SetType(SOCKETT_LIVE);
        return true;
    }
    SetCloseStatus();
    DEV_DEBUG(TF(" TCPSocket[%p] async listen failed, set close status"), this);
    return false;
}

bool CTCPSocket::AsyncAccept(Int nDerive)
{
    assert(IsInitType());
    SetLiveTime();
    if (m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_ACK_DETECT)
    {
        SetType(SOCKETT_ACK);
    }
    else
    {
        SetType(SOCKETT_LIVE);
    }
    SetTraits(SOCKETT_TCP_ACCEPT);
    if (nDerive != 0)
    {
        DEV_DEBUG(TF(" TCPSocket[%p] derive TraitsEx=%#X"), this, nDerive);
        SetTraitsEx(nDerive, true);
    }
    return AsyncRecv();
}

bool CTCPSocket::AsyncConnect(void)
{
    //DEV_DEBUG(TF(" TCPSocket[%p] AsyncConnect connect pend EPOLL status on"), this);
    SetPendStatus(true);  // connect pend EPOLL status on

    socklen_t        slSockAddr = 0;
    struct sockaddr* pSockAddr  = nullptr;
    if (m_adrRemote.si_family == AF_INET6)
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in6);
        pSockAddr  = (struct sockaddr*)&(m_adrRemote.Ipv6);
    }
    else
    {
        slSockAddr = (socklen_t)sizeof(sockaddr_in);
        pSockAddr  = (struct sockaddr*)&(m_adrRemote.Ipv4);
    }
    Int nRet = connect(m_hSocket, pSockAddr, slSockAddr);
    if (nRet == RET_ERROR)
    {
        if (errno != EINPROGRESS)
        {
            //DEV_DEBUG(TF(" TCPSocket[%p] AsyncConnect1 connect pend EPOLL status off"), this);
            SetPendStatus(false);  // connect pend EPOLL status off
            DEV_DEBUG(TF(" TCPSocket[%p] async connect failed[%d]"), this, errno);
            return false;
        }
    }
    if (BindEvent(true, NET_DEF_EVENT_IOMODE))
    {
        //DEV_DEBUG(TF(" TCPSocket[%p] async connect add EPOLLOUT(ET mode)"), this);
        // non-block connect okay epoll wait return EPOLLOUT(ET mode)
        return true;
    }
    //DEV_DEBUG(TF(" TCPSocket[%p] AsyncConnect2 connect pend EPOLL status off"), this);
    SetPendStatus(false);  // connect pend EPOLL status off
    return false;
}

bool CTCPSocket::AsyncSend(bool bAdd)
{
    bool bRet = false;
    if (bAdd)
    {
        CSyncLockScope scope(m_NSLock);
        //DEV_DEBUG(TF(" TCPSocket[%p] AsyncSend send queue EPOLL status on"), this);
        SetQueueStatus(true); // send queue EPOLL status on

        //DEV_DEBUG(TF(" TCPSocket[%p] async send add EPOLLOUT(ET mode)"), this);
        bRet = UpdateEvent(NET_DEF_EVENT_IOMODE);
        if (bRet == false)
        {
            //DEV_DEBUG(TF(" TCPSocket[%p] AsyncSend send queue EPOLL off"), this);
            SetQueueStatus(false); // send queue EPOLL status off
        }
    }
    else
    {
        //assert(m_nQueueSize == 0);
        //DEV_DEBUG(TF(" TCPSocket[%p] AsyncSend empty send queue EPOLL status off"), this);
        SetQueueStatus(false); // send queue EPOLL status off

        //DEV_DEBUG(TF(" TCPSocket[%p] async send remove EPOLLOUT(ET mode)"), this);
        bRet = UpdateEvent();
    }
    return bRet;
}

bool CTCPSocket::AsyncRecv(void)
{
    if (CreateCommonBuffer(CNETBuffer::BUFFERS_RECV))
    {
        if (BindEvent())
        {
            DEV_DEBUG(TF(" TCPSocket[%p - %p] ready to recv data"), this, m_utId);
            return CreateQueueBuffer(m_pNetwork->GetAttr().nMaxSend);
        }
        DestroyCommonBuffer();
    }
    return false;
}

bool CTCPSocket::AcceptEvent(uintptr_t utAccept)
{
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPConnectCount), 1);
    DEV_DEBUG(TF(" TCPSocket[%p - %p] EVENT_TCP_ACCEPT[accept socket %p]"), this, m_utId, utAccept);
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_ACCEPT, utAccept, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::ConnectEvent(uintptr_t utError)
{
    CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPConnectCount), 1);
    DEV_DEBUG(TF(" TCPSocket[%p - %p] EVENT_TCP_CONNECT[%d]"), this, m_utId, utError);
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_CONNECT, utError, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::SendEvent(void)
{
    //////
    // work thread <--> logic thread
    return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_SEND, (uintptr_t)m_pNetwork->GetAttr().nMaxSend, (ULLong)(m_utId)) == RET_OKAY);
    //////
}

bool CTCPSocket::RecvEvent(CNETTraits::TCP_PARAM& Param)
{
    //////
    // work thread <--> logic thread
    if (IsRecvEvent())
    {
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_RECV, (uintptr_t)Param.nSize, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    else
    {
        CBufReadStream BufReadStream((size_t)Param.nSize, Param.pData);
        return (m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_RECV, BufReadStream, reinterpret_cast<ULLong>(&Param)) == RET_OKAY);
    }
    //////
}

void CTCPSocket::CloseEvent(void)
{
    if (IsExitStatus() == false)
    {
        DEV_DEBUG(TF(" TCPSocket[%p - %p] CloseEvent traits[%d]-state[%d]-status[%X] %p"), this, m_utId, GetTraits(), GetType(), GetStatus(), m_pEventHandler);
        if (IsTCPValid())
        {
            //////
            // work thread <--> logic thread
            m_pEventHandler->OnHandle(CNETTraits::EVENT_TCP_CLOSE, m_utId, m_ullLiveData);
            //////
        }
        CAtomics::Add64<LLong>(&(m_pNetwork->GetDump().llTCPCloseCount), 1);
    }
}

bool CTCPSocket::UpdateReadyAccept(SOCKET hAccept)
{
    assert(m_pNetwork      != nullptr);
    assert(m_pEventHandler != nullptr);
    assert(m_hSocket       == INVALID_SOCKET);
    m_hSocket = hAccept;
    m_utId    = (uintptr_t)m_hSocket;
    DEV_DEBUG(TF(" NETSocket[%p] update ready accept socket[type : %s] successed"), this, ((m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_IPV6) ? TF("AF_INET6") : TF("AF_INET")));
    // add  map
    if (Add() == false)
    {
        DEV_DEBUG(TF(" TCPSocket[%p] accept add to socket map failed"), this);
        return false;
    }
    HandlerAddRef();

    if (m_pNetwork->GetAttr().nAttrs & CNETTraits::ATTR_IPV6)
    {
        UInt uLen = (UInt)sizeof(sockaddr_in6);
        getsockname(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv6),  (socklen_t*)&uLen);
        getpeername(m_hSocket, (struct sockaddr*)&(m_adrRemote.Ipv6), (socklen_t*)&uLen);
    }
    else
    {
        UInt uLen = (UInt)sizeof(sockaddr_in);
        getsockname(m_hSocket, (struct sockaddr*)&(m_adrLocal.Ipv4),  (socklen_t*)&uLen);
        getpeername(m_hSocket, (struct sockaddr*)&(m_adrRemote.Ipv4), (socklen_t*)&uLen);
    }
    return true;
}

bool CTCPSocket::UpdateReadyConnect(UInt uError)
{
    switch (uError)
    {
    case EACCES:
    case EPERM:
    case EAFNOSUPPORT:
    case EALREADY:
    case ECONNREFUSED:
    case EINPROGRESS:
    case EINTR:
    case EISCONN:
    case ENETUNREACH:
    case EPROTOTYPE:
    case ETIMEDOUT:
        {
            if (ConnectEvent((uintptr_t)uError))
            {
                SetTraits(SOCKETT_TCP);
                return true;
            }
        }
        break;
    default: {}
    }
    return false;
}

bool CTCPSocket::OnPackComplete(PByte& pData, size_t stSize, CNETBuffer* pBuffer, bool bOnly)
{
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = (CNETTraits::Socket)m_utId;
    Param.pData   = pData;
    Param.nSize   = (Int)stSize;
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_TCP;
    if (pBuffer->IsJumbo() == false)
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_BUFFER;
        Param.index   = m_pNetwork->GetBufferIndex();
        Param.nOffset = m_pNetwork->GetAttr().nBufferOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxBuffer;
    }
    else
    {
        Param.nAttr  |= (Int)CNETTraits::ATTR_MAX_JUMBOBUF;
        Param.index   = m_pNetwork->GetJumboIndex();
        Param.nOffset = m_pNetwork->GetAttr().nJumboOffset;
        Param.nCache  = m_pNetwork->GetAttr().nMaxJumbo;
    }
    if (bOnly)
    {
        Param.pCache = (PByte)pBuffer;
    }
    bool bRet = RecvEvent(Param);
    if (bOnly)
    {
        pData = Param.pCache;
    }
    return bRet;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
