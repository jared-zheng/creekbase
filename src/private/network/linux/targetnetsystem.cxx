// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "netsystem.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetworkSystem : CSubSystem
UInt CNetworkSystem::Init(void)
{
    if (CAtomics::Increment<UInt>(&(CNetworkSystemSingleton::ms_uInitRef)) == 1)
    {
        DEV_DUMP(TF("* Network SubSystem singleton init okay"));
    }
    return (UInt)RET_OKAY;
}

void CNetworkSystem::Exit(void)
{
    if (CAtomics::Decrement<UInt>(&(CNetworkSystemSingleton::ms_uInitRef)) == 0)
    {
        DEV_DUMP(TF("* Network SubSystem singleton exit okay"));
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)


