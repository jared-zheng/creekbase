#include "stdafx.h"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

#if (__COMPILER_TYPE__ == COMPILER_TYPE_GCC)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    #ifdef __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("64 Debug   Static  Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("64 Debug   Dynamic Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #else   // __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("64 Release Static  Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("64 Release Dynamic Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #endif  // __RUNTIME_DEBUG__
#else
    #ifdef __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("32 Debug   Static  Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("32 Debug   Dynamic Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #else   // __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("32 Release Static  Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("32 Release Dynamic Library Version Compiled with GNU G++ = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #endif  // __RUNTIME_DEBUG__
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_CLANG)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    #ifdef __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("64 Debug   Static  Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("64 Debug   Dynamic Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #else   // __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("64 Release Static  Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("64 Release Dynamic Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #endif  // __RUNTIME_DEBUG__
#else
    #ifdef __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("32 Debug   Static  Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("32 Debug   Dynamic Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #else   // __RUNTIME_DEBUG__
        #ifdef __RUNTIME_STATIC__
            #pragma message("32 Release Static  Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #else
            #pragma message("32 Release Dynamic Library Version Compiled with LLVM Clang = " __VERSION__ ", __ARCH_TARGET__ = " __ARCH_TARGET_STR__ ", __PLATFORM_TARGET__ = " __PLATFORM_TARGET_STR__ ", __RUNTIME_CHARSET__ = " __RUNTIME_CHARSET_STR__)
        #endif
    #endif  // __RUNTIME_DEBUG__
#endif
#elif (__COMPILER_TYPE__ == COMPILER_TYPE_MSVC)
    #error "Add Compile Info!!!"
#else
    #error "Add Compile Info!!!"
#endif
