// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_DEF_HXX__
#define __TARGET_NETWORK_DEF_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "mobject.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
enum NET_DEF
{
    NET_DEF_CNT           = 5,
    NET_DEF_MAX_NETBUF    = 64,
    NET_DEF_ONE_HALFL     = 460,        // n * 460 >> 10 --> 0.45
    NET_DEF_ONE_HALFH     = 522,        // n * 564 >> 10 --> 0.51
    NET_DEF_EVENT_DEFAULT = (EPOLLIN|EPOLLET),
    NET_DEF_EVENT_IOMODE  = (EPOLLIN|EPOLLOUT|EPOLLET),
    INVALID_SOCKET        = HANDLE_INVALID,
};

typedef Handle SOCKET,  *PSOCKET;
typedef size_t BufSize, *PBufSize;
typedef PByte  BufAddr, *PBufAddr;

typedef struct tagNETBUF
{
    BufSize   len;
    BufAddr   buf;
}NETBUF, *PNETBUF;

typedef struct tagACCEPT_SOCKET
{
    sa_family_t  si_family; // AF_UNSPEC
    SOCKET       hAccept;
}ACCEPT_SOCKET, *PACCEPT_SOCKET;

typedef union tagSOCKADDR_INET
{
    sockaddr_in   Ipv4;
    sockaddr_in6  Ipv6;
    ACCEPT_SOCKET Accept;
    sa_family_t   si_family;
}SOCKADDR_INET, *PSOCKADDR_INET;

#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    #define _tinet_ntop               inet_ntop
    #define _tdlerror                 dlerror
    #define _tdlsym                   dlsym
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    #warning "make sure linux native API support wchar_t string!"
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    #error "make sure linux native API support utf-16 string!"
#else
    #error "unknown runtime charset"
#endif

///////////////////////////////////////////////////////////////////
// CNetEventHandle
class CNetEventHandle : public MObject
{
public:
    CNetEventHandle(void);
    ~CNetEventHandle(void);

    bool     IsValid(void) const;

    bool     Open(void);
    void     Close(void);

    bool     Ctl(Int nOpt, UInt uFlag, SOCKET hSocket, void* pArg);
    bool     SignalAll(Int nCount);
    Int      Wait(struct epoll_event* pEventData, Int nCount);
private:
    CNetEventHandle(const CNetEventHandle& aSrc);
    CNetEventHandle& operator=(const CNetEventHandle& aSrc);
public:
    Handle   m_hNetEvent;
};

INLINE CNetEventHandle::CNetEventHandle(void)
: m_hNetEvent(HANDLE_INVALID)
{
}

INLINE CNetEventHandle::~CNetEventHandle(void)
{
    Close();
}

INLINE CNetEventHandle::CNetEventHandle(const CNetEventHandle&)
: m_hNetEvent(HANDLE_INVALID)
{
}

INLINE CNetEventHandle& CNetEventHandle::operator=(const CNetEventHandle&)
{
    return (*this);
}

INLINE bool CNetEventHandle::IsValid(void) const
{
    return (m_hNetEvent != HANDLE_INVALID);
}

INLINE bool CNetEventHandle::Open(void)
{
    Close();
    m_hNetEvent = epoll_create(LMT_KEY);
    if (IsValid())
    {
        return true;
    }
    DEV_DEBUG(TF("NetEvent epoll create failed[%d]"), errno);
    return false;
}

INLINE void CNetEventHandle::Close(void)
{
    if (IsValid())
    {
        close(m_hNetEvent);
        m_hNetEvent = HANDLE_INVALID;
    }
}

INLINE bool CNetEventHandle::Ctl(Int nOpt, UInt uFlag, SOCKET hSocket, void* pArg)
{
    assert(IsValid());
    struct epoll_event eEvent;
    eEvent.events   = uFlag;
    eEvent.data.ptr = pArg;
    if (epoll_ctl(m_hNetEvent, nOpt, hSocket, &eEvent) == RET_ERROR)
    {
        DEV_DEBUG(TF("NetEvent epoll ctl op=%d, flag=%d, arg=%p failed[%d]"), nOpt, uFlag, pArg, errno);
        return false;
    }
    return true;
}

INLINE bool CNetEventHandle::SignalAll(Int nCount)
{
    assert(IsValid());
    struct epoll_event eEvent;
    eEvent.events   = EPOLLOUT;
    eEvent.data.ptr = nullptr;
    epoll_ctl(m_hNetEvent, EPOLL_CTL_ADD, STDOUT_FILENO, &eEvent);
    for (Int i = 1; i < nCount; ++i)
    {
        epoll_ctl(m_hNetEvent, EPOLL_CTL_MOD, STDOUT_FILENO, &eEvent);
    }
    return (nCount > 0);
}

INLINE Int CNetEventHandle::Wait(struct epoll_event* pEventData, Int nCount)
{
    assert(IsValid());
    return epoll_wait(m_hNetEvent, pEventData, nCount, -1);
}

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_NETWORK_DEF_HXX__
