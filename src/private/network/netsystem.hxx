// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_SYSTEM_HXX__
#define __NETWORK_SYSTEM_HXX__

#pragma once

#include "singleton.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetworkSystem : CSubSystem
class CNetworkSystem : public CSubSystem
{
public:
    // CTRefCount
    //virtual Int   AddRef(void) OVERRIDE;
    virtual Int   Release(void) OVERRIDE;
    // CComponent
    virtual UInt  Command(PCXStr pszCMD, uintptr_t utParam) OVERRIDE;
    virtual UInt  Update(void) OVERRIDE;
    // CSubSystem
    virtual UInt  Init(void) OVERRIDE;
    virtual void  Exit(void) OVERRIDE;

    virtual Int   GetComponentInfo(MAP_COMPONENT_INFO& Components) const OVERRIDE;
    virtual bool  FindComponent(const CUUID& uuId) const OVERRIDE;
    virtual bool  CreateComponent(const CUUID& uuId, CComponentPtr& CComponentPtrRef) OVERRIDE;
public:
    CNetworkSystem(void);
    virtual ~CNetworkSystem(void);
};

///////////////////////////////////////////////////////////////////
// CNetworkSystemSingleton
class CNetworkSystemSingleton : public CTSingletonInst<CNetworkSystem>
{
public:
    static UInt   ms_uInitRef;
public:
    CNetworkSystemSingleton(void);
    ~CNetworkSystemSingleton(void);
    CNetworkSystemSingleton(const CNetworkSystemSingleton&);
    CNetworkSystemSingleton& operator=(const CNetworkSystemSingleton&);
};
typedef CNetworkSystemSingleton GNetworkSystem;
#define GNetworkSystemInst      CNetworkSystemSingleton::GetInstance()

///////////////////////////////////////////////////////////////////
#include "netsystem.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_SYSTEM_HXX__
