// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __XUI_MANAGER_HXX__
#define __XUI_MANAGER_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "xuisystem.hxx"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CXUIManagerWindows
class CXUIManagerWindows : public CXUIManager
{
public:
    // CTRefCount
    //virtual Int      AddRef(void) OVERRIDE;
    virtual Int      Release(void) OVERRIDE;
    // CComponent
    virtual UInt     Command(PCXStr pszCMD, uintptr_t utParam) OVERRIDE;
    virtual UInt     Update(void) OVERRIDE;
    // CXUIManager
    virtual UInt     Init(void) OVERRIDE;
    virtual void     Exit(void) OVERRIDE;

    virtual void     MsgLoop(void) OVERRIDE;

    virtual intptr_t Create(IXWnd& xWndRef, CREATE_PARAM& cp) OVERRIDE;
    virtual intptr_t Create(IXWnd& xWndRef, ULong ulExStyle, PCXStr pszName, ULong ulStyle,
                            Int nx = CW_USEDEFAULT, Int ny = CW_USEDEFAULT, Int ncx = CW_USEDEFAULT, Int ncy = CW_USEDEFAULT,
                            IXWnd* pParent = nullptr, intptr_t itMenuID = 0, BOOL bFlag = TRUE, UInt uClassStyle = CS_VREDRAW|CS_HREDRAW|CS_DBLCLKS,
                            CEventHandler* pEventHandler = nullptr) OVERRIDE;
    virtual bool     Attach(IXWnd& xWndRef, HWND hWnd, IXWnd* pParent = nullptr) OVERRIDE;
    virtual bool     Detach(IXWnd& xWndRef) OVERRIDE;

    virtual IXWnd*   Find(PCXStr pszName) OVERRIDE;
    virtual IXWnd*   Find(HWND hWnd) OVERRIDE;
    virtual IXWnd*   GetFrame(void) OVERRIDE;

    virtual bool     LoadAccel(Int nAccelID) OVERRIDE;
    virtual bool     LoadResource(PCXStr pszFile) OVERRIDE;
    virtual void     SetResHandle(Module mResInst, bool bUnload = false) OVERRIDE;
    virtual Module   GetResHandle(void) OVERRIDE;
public:
    CXUIManagerWindows(void);
    virtual ~CXUIManagerWindows(void);
private:
    bool    OnIdle(void);
    bool    PreTranslateMessage(LPMSG pMsg);
    bool    IsIdleMessage(LPMSG pMsg);
    void    Dump(void);

    bool    RegisterWndClass(PCXStr pszClass, Int nResID, UInt uClassStyle);
private:
    static LRESULT   CALLBACK XWndProc(HWND hWnd, UInt uMsg, WPARAM wParam, LPARAM lParam);
    static intptr_t  CALLBACK XDlgProc(HWND hDlg, UInt uMsg, WPARAM wParam, LPARAM lParam);
    static LRESULT   CALLBACK SWndProc(HWND hWnd, UInt uMsg, WPARAM wParam, LPARAM lParam);
    static intptr_t  CALLBACK SDlgProc(HWND hDlg, UInt uMsg, WPARAM wParam, LPARAM lParam);

    bool    Inthunk(IXWnd* pWnd);
    bool    Unthunk(IXWnd* pWnd);

    IXWnd*  FindXWnd(HWND hWnd);
private:
    typedef struct tagXUI_WND
    {
        IXWnd* pWnd;
        PByte  pThunk;
    }XUI_WND, *PXUI_WND;
    typedef CTMap<HWND, XUI_WND>         MAP_XWND;
    typedef CTMap<HWND, XUI_WND>::PAIR   MAP_XWND_PAIR;
private:
    IXWnd*      m_pFrame;
    bool        m_bUnload;
    HMODULE     m_hResInst;
    HACCEL      m_hAccel;
    MAP_XWND    m_XWnd;
};

///////////////////////////////////////////////////////////////////
#include "xuimanager.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __XUI_MANAGER_HXX__
