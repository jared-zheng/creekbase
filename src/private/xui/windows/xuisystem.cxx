// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "xuimanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CXUISystem : CSubSystem
CXUISystem::CXUISystem(void)
: m_inxThunk(nullptr)
, m_uThunkSize(0)
{
    DEV_DEBUG(TF("* XUI System singleton Inst created"));
}

CXUISystem::~CXUISystem(void)
{
    DEV_DEBUG(TF("* XUI System singleton Inst destroyed"));
}
// CTRefCount
Int CXUISystem::Release(void)
{
    return m_Counter.Reset();
}
// CComponent
UInt CXUISystem::Command(PCXStr pszCMD, uintptr_t utParam)
{
    UNREFERENCED_PARAMETER( pszCMD );
    UNREFERENCED_PARAMETER( utParam );
    return 0;
}

UInt CXUISystem::Update(void)
{
    return 0;
}

UInt CXUISystem::Init(void)
{
    if (CAtomics::CompareExchange<UInt>(&m_uThunkSize, (size_t)XWND_THUNK_SIZE, 0) == 0)
    {
        m_inxThunk = MCCreate((size_t)XWND_THUNK_SIZE, 0, 0, ALLOC_TYPE_EXECUTE);
        if (m_inxThunk != nullptr)
        {
            DEV_DUMP(TF("* XUI System singleton init okay"));
            return (UInt)RET_OKAY;
        }
    }
    return (UInt)RET_FAIL;
}

void CXUISystem::Exit(void)
{
    if (CAtomics::Exchange<UInt>(&m_uThunkSize, 0) > 0)
    {
        MCDestroy(m_inxThunk);
        m_inxThunk = nullptr;
        DEV_DUMP(TF("* XUI System singleton exit okay"));
    }
}

// CSubSystem
Int CXUISystem::GetComponentInfo(MAP_COMPONENT_INFO& Components) const
{
    // CXUIManager
    Components.Add(UUID_OF(CXUIManager), CStringKey(NAME_OF(CXUIManager)));

    return Components.GetSize();
}

bool CXUISystem::FindComponent(const CUUID& uuId) const
{
    // CXUIManager
    if (uuId == UUID_OF( CXUIManager ))
    {
        return true;
    }
    return false;
}

bool CXUISystem::CreateComponent(const CUUID& uuId, CComponentPtr& CComponentPtrRef)
{
    CComponentPtrRef = nullptr;
    // CXUIManager
    if (uuId == UUID_OF( CXUIManager ))
    {
        CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
        if (CXUISystemSingleton::ms_pXUIManager == nullptr)
        {
            DEV_DUMP(TF("+ CXUIManager component created"));
            CXUISystemSingleton::ms_pXUIManager = MNEW CXUIManagerWindows;
        }
        if (CXUISystemSingleton::ms_pXUIManager != nullptr)
        {
            CComponentPtrRef = CXUISystemSingleton::ms_pXUIManager;
            return true;
        }
    }
    return false;
}

PByte CXUISystem::AllocThunk(uintptr_t utThis, uintptr_t utProc)
{
    if (m_inxThunk != nullptr)
    {
        PXTHUNK pThunk = reinterpret_cast<PXTHUNK>(MCAlloc(m_inxThunk));
        if (pThunk != nullptr)
        {
            pThunk->Init(utThis, utProc);
        }
        return (PByte)pThunk;
    }
    return nullptr;
}

void CXUISystem::FreeThunk(PByte pThunk)
{
    if (m_inxThunk != nullptr)
    {
        MCFree(m_inxThunk, pThunk);
    }
}

///////////////////////////////////////////////////////////////////
// CreateSubSystem
XUIAPI IMPLEMENT_CREATE_SUBSYSTEM( XUIStatic )
{
    if (uuId == UUID_OF( CXUISystem ))
    {
        CXUISystemSingleton::GetInstance()->AddRef();
        return CXUISystemSingleton::GetInstance();
    }
    return nullptr;
}

#if defined(XUI_EXPORT)
///////////////////////////////////////////////////////////////////
//
#ifdef __RUNTIME_DEBUG__
    #define XUI_MODULE_ATTACH TF("$ Load xuiDebug.dll --- DLL_PROCESS_ATTACH")
    #define XUI_MODULE_DETACH TF("$ Unload xuiDebug.dll --- DLL_PROCESS_DETACH")
#else
    #define XUI_MODULE_ATTACH TF("$ Load xui.dll --- DLL_PROCESS_ATTACH")
    #define XUI_MODULE_DETACH TF("$ Unload xui.dll --- DLL_PROCESS_DETACH")
#endif

BOOL WINAPI DllMain(HMODULE hModule, ULong ulReason, void* lpReserved)
{
    UNREFERENCED_PARAMETER( lpReserved );

    if (ulReason == DLL_PROCESS_ATTACH)
    {
        ::DisableThreadLibraryCalls(hModule);
        DEV_DEBUG(XUI_MODULE_ATTACH);
    }
    else if (ulReason == DLL_PROCESS_DETACH)
    {
        DEV_DEBUG(XUI_MODULE_DETACH);
    }
    return TRUE;
}
///////////////////////////////////////////////////////////////////
#endif  // XUI_EXPORT

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

