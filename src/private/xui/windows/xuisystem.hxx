// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __XUI_SYSTEM_HXX__
#define __XUI_SYSTEM_HXX__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "xui.h"
#include "singleton.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CXUISystem : CSubSystem
class CXUISystem : public CSubSystem
{
public:
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    enum XWND_THUNK
    {
        XWND_THUNK_SIZE = 48,
    };

    #pragma pack(push, 2)
    typedef struct tagXTHUNK : public MObject
    {
    public:
        void Init(uintptr_t utThis, uintptr_t utProc)
        {
            usMOVRCX = 0xB948;
            ullTHIS  = utThis;
            usMOVRAX = 0xB848;
            ullPROC  = utProc;
            usRAXJMP = 0xe0FF;
            ::FlushInstructionCache(::GetCurrentProcess(), this, sizeof(tagXTHUNK));
        }
    public:
        UShort  usMOVRCX;   // mov rcx, pThis
        ULLong  ullTHIS;    //
        UShort  usMOVRAX;   // mov rax, WindowProc
        ULLong  ullPROC;    //
        UShort  usRAXJMP;   // jmp WindowProc
    }XTHUNK, *PXTHUNK;
    #pragma pack(pop)
#else
    enum XWND_THUNK
    {
        XWND_THUNK_SIZE = 32,
    };

    #pragma pack(push, 1)
    typedef struct tagXTHUNK : public MObject
    {
    public:
        void Init(uintptr_t utThis, uintptr_t utProc)
        {
            uMOV  = 0x042444C7;  //C7 44 24 0C
            uTHIS = (UInt)utThis;
            bJMP  = 0xE9;
            uPROC = (UInt)(utProc - (reinterpret_cast<UInt>(this) + sizeof(tagXTHUNK)));
            ::FlushInstructionCache(::GetCurrentProcess(), this, sizeof(tagXTHUNK));
        }
    public:
        UInt   uMOV;  // mov dword ptr [esp+0x4], pThis (esp+0x4 is m_hWnd)
        UInt   uTHIS; //
        Byte   bJMP;  // jmp WindowProc
        UInt   uPROC; // relative jmp
    }XTHUNK, *PXTHUNK;
    #pragma pack(pop)
#endif
public:
    // CTRefCount
    //virtual Int  AddRef(void) OVERRIDE;
    virtual Int  Release(void) OVERRIDE;
    // CComponent
    virtual UInt Command(PCXStr pszCMD, uintptr_t utParam) OVERRIDE;
    virtual UInt Update(void) OVERRIDE;
    // CSubSystem
    virtual UInt Init(void) OVERRIDE;
    virtual void Exit(void) OVERRIDE;

    virtual Int  GetComponentInfo(MAP_COMPONENT_INFO& Components) const OVERRIDE;
    virtual bool FindComponent(const CUUID& uuId) const OVERRIDE;
    virtual bool CreateComponent(const CUUID& uuId, CComponentPtr& CComponentPtrRef) OVERRIDE;

    PByte   AllocThunk(uintptr_t utThis, uintptr_t utProc);
    void    FreeThunk(PByte pThunk);
public:
    CXUISystem(void);
    virtual ~CXUISystem(void);
private:
    PINDEX   m_inxThunk;
    UInt     m_uThunkSize;
};

///////////////////////////////////////////////////////////////////
// CXUISystemSingleton
class CXUISystemSingleton : public CTSingletonInst<CXUISystem>
{
public:
    static CSyncLock                   ms_SyncLock;
    static class CXUIManagerWindows*   ms_pXUIManager;
public:
    CXUISystemSingleton(void);
    ~CXUISystemSingleton(void);
    CXUISystemSingleton(const CXUISystemSingleton&);
    CXUISystemSingleton& operator=(const CXUISystemSingleton&);
};
typedef CXUISystemSingleton GXUI;
#define GXUIInst            CXUISystemSingleton::GetInstance()

///////////////////////////////////////////////////////////////////
#include "xuisystem.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __XUI_SYSTEM_HXX__
