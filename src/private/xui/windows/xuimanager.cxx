// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#include "stdafx.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "xuikit.h"
#include "xuimanager.hxx"

//////////////////////////#
using namespace CREEK;   //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CXUIManagerWindows
UInt CXUIManagerWindows::Init(void)
{
    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    if (m_hResInst == nullptr)
    {
        m_hResInst = ::GetModuleHandle(nullptr);
    }
    DEV_DEBUG(TF("  CXUIManager init okay[%p]"), m_hResInst);
    return (UInt)RET_OKAY;
}

void CXUIManagerWindows::Exit(void)
{
    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    if (m_pFrame != nullptr)
    {
        if (m_pFrame->IsWindow())
        {
            if (m_pFrame->IsModal())
            {
                m_pFrame->EndDialog(0);
            }
            else
            {
                m_pFrame->DestroyWindow();
            }
            ::PostQuitMessage(0); // make sure let message loop end
        }
        m_pFrame = nullptr;
    }
    SetResHandle(nullptr);
    Dump();
    DEV_DEBUG(TF("  CXUIManager exit okay"));
}

void CXUIManagerWindows::MsgLoop(void)
{
    bool bDoIdle = true;
    MSG  msg;

    for(;;)
    {
        if (bDoIdle && (::PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE) == FALSE))
        {
            bDoIdle = OnIdle();
        }
        else
        {
            if (::GetMessage(&msg, nullptr, 0, 0) > 0)
            {
                if (PreTranslateMessage(&msg) == false)
                {
                    ::TranslateMessage(&msg);
                    ::DispatchMessage(&msg);
                }
                bDoIdle = IsIdleMessage(&msg);
            }
            else // WM_QUIT
            {
                DEV_DEBUG(TF("  CXUIManager request quit"));
                break;
            }
        }
    }

    m_pFrame = nullptr;
    SetResHandle(nullptr);
}

intptr_t CXUIManagerWindows::Create(IXWnd& xWndRef, CREATE_PARAM& cp)
{
    return Create(xWndRef, cp.ulExStyle, cp.pszName, cp.ulStyle, cp.nx, cp.ny, cp.ncx, cp.ncy, cp.pParent, cp.itMenuID, cp.bFlag, cp.uClassStyle, cp.pEventHandler);
}

intptr_t CXUIManagerWindows::Create(IXWnd& xWndRef, ULong ulExStyle, PCXStr pszName, ULong ulStyle,
                                    Int nx, Int ny, Int ncx, Int ncy, IXWnd* pParent, intptr_t itMenuID, BOOL bFlag, UInt uClassStyle, CEventHandler* pEventHandler)
{
    assert(m_hResInst != nullptr);
    assert(xWndRef.m_hWnd == nullptr);
    if (xWndRef.m_hWnd != nullptr)
    {
        return RET_ERROR;
    }

    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    intptr_t itRet = RET_ERROR;
    xWndRef.m_pOwner = pParent;
    xWndRef.SetEventHandler(pEventHandler);
    if (xWndRef.IsDialogType() == false)
    {
        assert(ulStyle != 0);

        bool bRet = RegisterWndClass(xWndRef.GetXWndClass(), (Int)itMenuID, uClassStyle);
        xWndRef.m_hWnd  = ::CreateWindowEx(ulExStyle, xWndRef.GetXWndClass(), pszName, ulStyle,
                                           nx, ny, ncx, ncy,
                                           (pParent != nullptr) ? pParent->m_hWnd : nullptr,
                                           bRet ? nullptr : (HMENU)itMenuID, nullptr, &xWndRef);
        assert(xWndRef.m_hWnd != nullptr);
        if (xWndRef.m_hWnd == nullptr)
        {
            DEV_DEBUG(TF("  CreateWindow %s Failed : %d"), xWndRef.GetXWndClass(), ::GetLastError());
            xWndRef.m_pOwner = nullptr;
            return RET_ERROR;
        }
        itRet = reinterpret_cast<intptr_t>(xWndRef.m_hWnd);

        // controls
        if (bFlag == TRUE)
        {
            if (xWndRef.m_pProc == nullptr)
            {
                assert(xWndRef.IsControlType() == true);
                xWndRef.PreSubClass();
                Inthunk(&xWndRef);
            }
            assert(xWndRef.m_pProc != nullptr);
        }
    }
    else if (bFlag == FALSE) // modeless
    {
        xWndRef.m_hWnd = ::CreateDialogParam(m_hResInst, MAKEINTRESOURCE(itMenuID),
                                             (pParent != nullptr) ? pParent->m_hWnd : (::GetActiveWindow()),
                                             &CXUIManagerWindows::SDlgProc,
                                             (LPARAM)&xWndRef);
        assert(xWndRef.m_hWnd != nullptr);
        if (xWndRef.m_hWnd == nullptr)
        {
            xWndRef.m_pOwner = nullptr;
            return RET_ERROR;
        }
        itRet = reinterpret_cast<intptr_t>(xWndRef.m_hWnd);
    }
    else
    {
        xWndRef.SetModal();
        itRet = ::DialogBoxParam(m_hResInst, MAKEINTRESOURCE(itMenuID),
                                 (pParent != nullptr) ? pParent->m_hWnd : (::GetActiveWindow()),
                                 &CXUIManagerWindows::SDlgProc,
                                 (LPARAM)&xWndRef);
        xWndRef.SetModal(false);
        if (itRet < 0)
        {
            DEV_DEBUG(TF("  DoModal error=%d for IXWnd[PTR=%p]"), ::GetLastError(), &xWndRef);
        }
    }
    return itRet;
}

bool CXUIManagerWindows::Attach(IXWnd& xWndRef, HWND hWnd, IXWnd* pParent)
{
    assert(xWndRef.m_hWnd == nullptr);
    if (xWndRef.m_hWnd != nullptr)
    {
        return false;
    }

    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    xWndRef.Attach(hWnd, pParent);
    assert(xWndRef.IsSubClass() == false);

    xWndRef.PreSubClass();
    if (Inthunk(&xWndRef))
    {
        xWndRef.SubClass();
        return true;
    }
    return false;
}

bool CXUIManagerWindows::Detach(IXWnd& xWndRef)
{
    assert(xWndRef.m_hWnd == nullptr);
    if ((xWndRef.m_hWnd == nullptr) || (xWndRef.m_pProc == nullptr))
    {
        return false;
    }

    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    if (xWndRef.IsSubClass())
    {
        Unthunk(&xWndRef);
        if (xWndRef.IsDialogType())
        {
            ::SetWindowLongPtr(xWndRef.m_hWnd, DWLP_DLGPROC, (intptr_t)xWndRef.m_pProc);
        }
        else
        {
            ::SetWindowLongPtr(xWndRef.m_hWnd, GWLP_WNDPROC, (intptr_t)xWndRef.m_pProc);
        }
        xWndRef.Detach();
        xWndRef.m_pProc = nullptr;
        xWndRef.SubClass(false);
        return true;
    }
    return false;
}

IXWnd* CXUIManagerWindows::Find(PCXStr pszName)
{
    HWND hWnd = ::FindWindow(pszName, nullptr);
    if (hWnd != nullptr)
    {
        return Find(hWnd);
    }
    return nullptr;
}

IXWnd* CXUIManagerWindows::Find(HWND hWnd)
{
    CSyncLockScope scope(CXUISystemSingleton::ms_SyncLock);
    const MAP_XWND_PAIR* pPair = m_XWnd.Find(hWnd);
    if (pPair != nullptr)
    {
        return (pPair->m_V.pWnd);
    }
    return nullptr;
}

bool CXUIManagerWindows::OnIdle(void)
{
    if (m_pFrame != nullptr)
    {
        return (m_pFrame->OnIdle());
    }
    return true;
}

bool CXUIManagerWindows::PreTranslateMessage(LPMSG pMsg)
{
    for (HWND hWnd = pMsg->hwnd; hWnd != nullptr; hWnd = ::GetParent(hWnd))
    {
        IXWnd* pWnd = FindXWnd(hWnd);
        if (pWnd != nullptr)
        {
            if (pWnd->PreTranslateMessage(pMsg))
            {
                return true;
            }
        }
        if (pWnd == m_pFrame)
        {
            break;
        }
    }
    if ((m_hAccel != nullptr) && (m_pFrame != nullptr))
    {
        if (::TranslateAccelerator(m_pFrame->m_hWnd, m_hAccel, pMsg))
        {
            return true;
        }
    }
    return false;
}

bool CXUIManagerWindows::IsIdleMessage(LPMSG pMsg)
{
    // These messages should NOT cause idle processing
    switch (pMsg->message)
    {
    case WM_PAINT:
    case WM_NCPAINT:
    case WM_NCMOUSEMOVE:
    case WM_TIMER:
    case 0x0118:    // WM_SYSTIMER (caret blink)
    case WM_MOUSEMOVE:
    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_KEYDOWN:
    case WM_KEYUP:
        {
            return false;
        }
        break;
    default:
        {
            return true;
        }
    }
}

void CXUIManagerWindows::Dump(void)
{
    if (m_XWnd.GetSize() > 0)
    {
        DEV_DEBUG(TF("* Total %d Undestroyed IXWnd Detect"), m_XWnd.GetSize());
#ifdef __RUNTIME_DEBUG__
        PINDEX index = m_XWnd.GetFirst();
        while (index != nullptr)
        {
            const MAP_XWND_PAIR* pPair = m_XWnd.GetNext(index);
            DEV_DUMP(TF("! Undestroyed IXWnd %p[HWND is %p, %s]"), pPair->m_V.pWnd, pPair->m_V.pWnd->m_hWnd, pPair->m_V.pWnd->GetXWndClass());
        }
#endif
        m_XWnd.RemoveAll();
    }
    else
    {
        DEV_DEBUG(TF("* None Undestroyed IXWnd Detect"));
    }
}

bool CXUIManagerWindows::RegisterWndClass(PCXStr pszClass, Int nResID, UInt uClassStyle)
{
    WNDCLASSEX wcx = { 0 };
    wcx.hInstance = ::GetModuleHandle(nullptr);
    if (::GetClassInfoEx(wcx.hInstance, pszClass, &wcx) == FALSE)
    {
        wcx.cbSize          = sizeof(WNDCLASSEX);
        wcx.style          = CS_HREDRAW | CS_VREDRAW | uClassStyle;
        wcx.lpfnWndProc      = &CXUIManagerWindows::SWndProc;
        wcx.cbClsExtra      = 0;
        wcx.cbWndExtra      = 0;
        wcx.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wcx.lpszClassName = pszClass;

        if (nResID != 0)
        {
            wcx.hIcon          = ::LoadIcon(m_hResInst, MAKEINTRESOURCE(nResID));
            //wcx.hIcon          = // (HICON)::LoadImage(m_hResInst, MAKEINTRESOURCE(nResID), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
            wcx.hIconSm          = wcx.hIcon;
            wcx.hCursor          = ::LoadCursor(m_hResInst, MAKEINTRESOURCE(nResID));
            //wcx.hCursor      = // (HCURSOR)::LoadImage(m_hResInst, MAKEINTRESOURCE(nResID), IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE);
            wcx.lpszMenuName  = MAKEINTRESOURCE(nResID);
        }
        if (wcx.hIcon == nullptr)
        {
            wcx.hIcon          = ::LoadIcon(nullptr, IDI_APPLICATION);
            wcx.hIconSm          = wcx.hIcon;
        }
        if (wcx.hCursor == nullptr)
        {
            wcx.hCursor          = ::LoadCursor(nullptr, IDC_ARROW);
        }
        return (::RegisterClassEx(&wcx) != 0);
    }
    return false;
}

LRESULT CALLBACK CXUIManagerWindows::XWndProc(HWND hWnd, UInt uMsg, WPARAM wParam, LPARAM lParam)
{
    LRESULT lRet = 0;
    IXWnd*  pWnd = reinterpret_cast<IXWnd*>(hWnd);
    if (pWnd != nullptr)
    {
        lRet = pWnd->WindowProc(uMsg, wParam, lParam);
        if (uMsg == WM_NCDESTROY)
        {
            CXUISystemSingleton::ms_pXUIManager->Unthunk(pWnd);
            pWnd->m_hWnd   = nullptr;
            pWnd->m_pOwner = nullptr;
            pWnd->m_pProc  = nullptr;
        }
    }
    else
    {
        lRet = ::DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
    return lRet;
}

intptr_t CALLBACK CXUIManagerWindows::XDlgProc(HWND hDlg, UInt uMsg, WPARAM wParam, LPARAM lParam)
{
    intptr_t itRet = 0;
    IXWnd*  pWnd = reinterpret_cast<IXWnd*>(hDlg);
    if (pWnd != nullptr)
    {
        itRet = (intptr_t)(pWnd->WindowProc(uMsg, wParam, lParam));
        if (uMsg == WM_NCDESTROY)
        {
            CXUISystemSingleton::ms_pXUIManager->Unthunk(pWnd);
            pWnd->m_hWnd   = nullptr;
            pWnd->m_pOwner = nullptr;
            pWnd->m_pProc  = nullptr;
        }
    }
    return itRet;
}

LRESULT CALLBACK CXUIManagerWindows::SWndProc(HWND hWnd, UInt uMsg, WPARAM wParam, LPARAM lParam)
{
    if (uMsg == WM_NCCREATE)
    {
        LPCREATESTRUCT pCS = reinterpret_cast<LPCREATESTRUCT>(lParam);
        IXWnd* pWnd        = reinterpret_cast<IXWnd*>(pCS->lpCreateParams);
        assert(pWnd != nullptr);

        DEV_DEBUG(TF("  Create Window handle %p for IXWnd[PTR=%p]"), hWnd, pWnd);

        pWnd->m_hWnd = hWnd;
        CXUISystemSingleton::ms_pXUIManager->Inthunk(pWnd);
        return pWnd->WindowProc(uMsg, wParam, lParam);
    }
    return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
}

intptr_t CALLBACK CXUIManagerWindows::SDlgProc(HWND hDlg, UInt uMsg, WPARAM wParam, LPARAM lParam)
{
    if (uMsg == WM_INITDIALOG)
    {
        IXWnd* pWnd = reinterpret_cast<IXWnd*>(lParam);
        assert(pWnd != nullptr);

        if (pWnd->IsModal())
        {
            DEV_DEBUG(TF("  Create Modal Dialog handle %p for IXWnd[PTR=%p]"), hDlg, pWnd);
        }
        else
        {
            DEV_DEBUG(TF("  Create Modeless Dialog handle %p for IXWnd[PTR=%p]"), hDlg, pWnd);
        }

        pWnd->m_hWnd = hDlg;
        CXUISystemSingleton::ms_pXUIManager->Inthunk(pWnd);

        IXWnd* pChild = nullptr;
        HWND hWnd = ::GetWindow(hDlg, GW_CHILD);
        while (hWnd != nullptr)
        {
            if (((CXDlg*)pWnd)->OnDDWExchange((Int)::GetWindowLongPtr(hWnd, GWLP_ID), pChild))
            {
                pChild->Attach(hWnd, pWnd);
                // Current do not add dlg's controls into map
                // if add to map, these controls IXWnd should sepcial handle when exit
                //CXUISystemSingleton::ms_pXUIManager->m_XWnd.Add(pWnd->m_hWnd, pWnd);
                //DEV_DEBUG(TF("  Add Control IXWnd[%p, %#X, %s] to MAP"), pWnd, pWnd->m_hWnd, pWnd->GetXWndClass());
                //CXUISystemSingleton::ms_pXUIManager->AttachXWnd(*pChild, hWnd, pWnd);
            }
            hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
        }
        return (intptr_t)(pWnd->WindowProc(uMsg, wParam, lParam));
    }
    return FALSE;
}

bool CXUIManagerWindows::Inthunk(IXWnd* pWnd)
{
    assert(pWnd != nullptr);
    assert(FindXWnd(pWnd->m_hWnd) == nullptr);

    XUI_WND xWnd = { pWnd, nullptr };
    if (pWnd->IsFrameMode())
    {
        assert(m_pFrame == nullptr);
        m_pFrame = pWnd;
    }

    if (pWnd->IsDialogType())
    {
        pWnd->m_pProc = (WNDPROC)::GetWindowLongPtr(pWnd->m_hWnd, DWLP_DLGPROC);
        if (pWnd->m_pProc == (WNDPROC)&CXUIManagerWindows::SDlgProc)
        {
            pWnd->m_pProc = nullptr;
        }
        xWnd.pThunk = CXUISystemSingleton::GetInstance()->AllocThunk((uintptr_t)pWnd, (uintptr_t)(&CXUIManagerWindows::XDlgProc));
        ::SetWindowLongPtr(pWnd->m_hWnd, DWLP_DLGPROC, reinterpret_cast<intptr_t>(xWnd.pThunk));
    }
    else
    {
        pWnd->m_pProc = (WNDPROC)::GetWindowLongPtr(pWnd->m_hWnd, GWLP_WNDPROC);
        if (pWnd->m_pProc == &CXUIManagerWindows::SWndProc)
        {
            pWnd->m_pProc = DefWindowProc;
        }
        xWnd.pThunk = CXUISystemSingleton::GetInstance()->AllocThunk((uintptr_t)pWnd, (uintptr_t)(&CXUIManagerWindows::XWndProc));
        ::SetWindowLongPtr(pWnd->m_hWnd, GWLP_WNDPROC, reinterpret_cast<intptr_t>(xWnd.pThunk));
    }
    m_XWnd.Add(pWnd->m_hWnd, xWnd);
    DEV_DEBUG(TF("  Add IXWnd[%p, %#X, %s] to MAP"), pWnd, pWnd->m_hWnd, pWnd->GetXWndClass());
    return true;
}

bool CXUIManagerWindows::Unthunk(IXWnd* pWnd)
{
    assert(pWnd != nullptr);
    assert(FindXWnd(pWnd->m_hWnd) != nullptr);

    bool bRet = false;
    PINDEX index = m_XWnd.FindIndex(pWnd->m_hWnd);
    assert(index != nullptr);
    assert(m_XWnd[index].pThunk != nullptr);
    CXUISystemSingleton::GetInstance()->FreeThunk(m_XWnd[index].pThunk);
    if (m_XWnd.RemoveAt(index))
    {
        DEV_DEBUG(TF("  Remove IXWnd[%p, %p, %s] from MAP"), pWnd, pWnd->m_hWnd, pWnd->GetXWndClass());
        if (pWnd->IsFrameMode())
        {
            assert(m_pFrame == pWnd);
            m_pFrame = nullptr;
            DEV_DEBUG(TF("  Frame IXWnd Removed, WM_QUIT message will post!"));
        }
        bRet = true;
    }
    return bRet;
}

IXWnd* CXUIManagerWindows::FindXWnd(HWND hWnd)
{
    __try
    {
        const MAP_XWND_PAIR* pPair = m_XWnd.Find(hWnd);
        if (pPair != nullptr)
        {
            return (pPair->m_V.pWnd);
        }
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
    }
    return nullptr;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

