// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __XUI_MANAGER_INL__
#define __XUI_MANAGER_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
///////////////////////////////////////////////////////////////////
// CXUIManagerWindows
INLINE CXUIManagerWindows::CXUIManagerWindows(void)
: m_pFrame(nullptr)
, m_bUnload(false)
, m_hResInst(nullptr)
, m_hAccel(nullptr)
{
    m_XWnd.SetHash(CContainerTraits::HASHD_SIZE);
}

INLINE CXUIManagerWindows::~CXUIManagerWindows(void)
{
    Exit();
}

INLINE Int CXUIManagerWindows::Release(void)
{
    Int nRef = m_Counter.Reset();
    if (nRef == 0)
    {
        MDELETE this;
        DEV_DUMP(TF("+ CXUIManager component destroyed"));
    }
    return nRef;
}

INLINE UInt CXUIManagerWindows::Command(PCXStr pszCMD, uintptr_t utParam)
{
    UNREFERENCED_PARAMETER( pszCMD );
    UNREFERENCED_PARAMETER( utParam );
    return (UInt)RET_OKAY;
}

INLINE UInt CXUIManagerWindows::Update(void)
{
    return (UInt)RET_OKAY;
}

INLINE IXWnd* CXUIManagerWindows::GetFrame(void)
{
    return m_pFrame;
}

INLINE bool CXUIManagerWindows::LoadAccel(Int nAccelID)
{
    m_hAccel = ::LoadAccelerators(m_hResInst, MAKEINTRESOURCE(nAccelID));
    return (m_hAccel != nullptr);
}

INLINE bool CXUIManagerWindows::LoadResource(PCXStr pszFile)
{
    m_hResInst = ::LoadLibraryEx(pszFile, nullptr, DONT_RESOLVE_DLL_REFERENCES);
    assert(m_hResInst);
    if (m_hResInst != nullptr)
    {
        m_bUnload = true;
    }
    return (m_hResInst != nullptr);
}

INLINE void CXUIManagerWindows::SetResHandle(Module mResInst, bool bUnload)
{
    if (m_hResInst != nullptr)
    {
        if (m_bUnload)
        {
            ::FreeLibrary(m_hResInst);
        }
        m_hResInst = nullptr;
    }
    if (mResInst != nullptr)
    {
        m_hResInst = mResInst;
        m_bUnload  = bUnload;
    }
}

INLINE Module CXUIManagerWindows::GetResHandle(void)
{
    return m_hResInst;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __XUI_MANAGER_INL__
