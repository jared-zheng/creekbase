// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __XUI_SYSTEM_INL__
#define __XUI_SYSTEM_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CXUISystem : CSubSystem

/////////////////////////////////////////////////////////////////////
// CXUISystemSingleton
SELECTANY CSyncLock            CXUISystemSingleton::ms_SyncLock;
SELECTANY CXUIManagerWindows*  CXUISystemSingleton::ms_pXUIManager = nullptr;

INLINE CXUISystemSingleton::CXUISystemSingleton(void)
{
}

INLINE CXUISystemSingleton::~CXUISystemSingleton(void)
{
}

INLINE CXUISystemSingleton::CXUISystemSingleton(const CXUISystemSingleton&)
{
}

INLINE CXUISystemSingleton& CXUISystemSingleton::operator=(const CXUISystemSingleton&)
{
    return (*this);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __XUI_SYSTEM_INL__
