#include "stdafx.h"
#include "testweb.h"

//#define NETTLS_TEST

#ifdef NETTLS_TEST
#include "networkhttptls.h"

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
#ifdef __RUNTIME_DEBUG__
#ifdef LIB_RUNTIME
#pragma comment(lib, "networktlslibDebug_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlslibDebug_x64_v143_MultiByte.lib")
#else
#pragma comment(lib, "networktlsDebug_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlsDebug_x64_v143_MultiByte.lib")
#endif
#else
#ifdef LIB_RUNTIME
#pragma comment(lib, "networktlslib_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlslib_x64_v143_MultiByte.lib")
#else
#pragma comment(lib, "networktlsRelease_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlsRelease_x64_v143_MultiByte.lib")
#endif
#endif
#endif

#else
#include "networkhttp.h"
#endif

using namespace CREEK;

DECLARE_GLOBAL_LOADER( NetworkSystem )

const CCString sTestGet = "GET /test.html?arg1=v1&arg2=v2 HTTP/1.1\r\nHost: gcc.gnu.org\r\nConnection: keep-alive\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\r\n"
"Accept: image/webp,image/apng,image/*,*/*;q=0.8\r\nReferer: https://gcc.gnu.org/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: zh-CN,zh;q=0.9\r\nCookie: test1=value1;test2=value2\r\n\r\n";

const CCString sTestLogin = "{\"rname\":\"111\",\"rpsw\":\"\",\"id\":\"6c60af31\",\"name\":\"WebSocket-Test\",\"act\":\"sign\",\"msg\":\"\"}";
const CCString sTestHello = "{\"rname\":\"111\",\"rpsw\":\"\",\"id\":\"6c60af31\",\"name\":\"WebSocket-Test\",\"act\":\"toall\",\"msg\":\"websocket-client test hello!!!\"}";
const CCString sTestHeart = "{\"rname\":\"111\",\"rpsw\":\"\",\"id\":\"6c60af31\",\"name\":\"WebSocket-Test\",\"act\":\"heart\",\"msg\":\"\"}";

class CTestClient : public CNulPackNetworkEventHandler
{
    //DECLARE_CLASS_LOADER( CTestClient, NetworkSystem )

    enum CLIENT_LIVE
    {
        CLIENT_LIVE_TIMER_ID  = 10001,
        CLIENT_LIVE_TIMER_VAL = 6000,
    };
public:
    CTestClient(void) : m_sTest(0), m_bConnect(false), m_bWebSocket(false), m_nTestCount(0)
    {
    }

    virtual ~CTestClient(void)
    {
    }

    bool Init(void)
    {
        CNetworkPtr NetworkPtr;
        LOADER_GLOBAL_CREEATE( NetworkSystem, UUID_OF( CNetwork ), NetworkPtr.Cast<CComponent>());
        if (NetworkPtr != nullptr)
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs = CNETTraits::ATTR_THREAD;
#ifdef TEST_IPV6
            attr.nAttrs |= CNETTraits::ATTR_IPV6;
#endif
            attr.nThread  = 2;
            if (NetworkPtr->Init(attr, this) != RET_OKAY)
            {
                return false;
            }
#ifdef NETTLS_TEST
            CNetworkTLS::CONFIG_PARAM Param;
            Param.bClientMode = true;
            if (m_Client.Init(NetworkPtr, Param, *this) == false)
#else
            if (m_Client.Init(NetworkPtr, *this) == false)
#endif
            {
                return false;
            }
            Connect2Server();
            CEventQueue::CreateTickEvent(CLIENT_LIVE_TIMER_ID, CLIENT_LIVE_TIMER_VAL, *this);
            return true;
        }
        return false;
    }

    void Exit(void)
    {
        CEventQueue::DestroyTickEvent(CLIENT_LIVE_TIMER_ID);
        m_Client.Exit();
    }

    void Wait(void)
    {
        m_Event.Wait();
    }

    virtual UInt OnHandle(uintptr_t utEvent, UInt)
    {
        if (utEvent == CLIENT_LIVE_TIMER_ID)
        {
            if (m_bConnect)
            {
                // if (m_nTestCount < 2)
                // {
                //     ++m_nTestCount;
                if (m_bWebSocket)
                {
                    CWSParser Parser;
                    Parser.From(sTestHeart, (CWSTraits::WS_FINAL|CWSTraits::WS_TEXT|CWSTraits::WS_MASK));
                    CBufReadStream brs(Parser.GetFrameSize(), Parser.GetCacheBuf());
                    m_Client.Send(m_sTest, brs);
                }
                else
                {
                    m_Client.Send(m_sTest, sTestGet);
                }
                // }
                // else
                // {
                //     m_Client.Check();
                // }
            }
            else
            {
                Connect2Server();
            }
        }
        return RET_OKAY;
    }
protected:
    virtual bool OnTcpConnect(UInt uError, Socket sConnect)
    {
        DEV_INFO(TF("###Connect return errid=%d, socket %p"), uError, sConnect);
#ifndef NETTLS_TEST
        if (uError == 0)
        {
            SendRequest(sConnect);
        }
#endif
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        m_sTest = 0;
        m_bConnect = false;
        //m_bWebSocket = !m_bWebSocket;
        m_nTestCount = 0;
        return true;
    }

    virtual bool OnTlsHandShake(UInt uError, Socket sSocket)
    {
        DEV_INFO(TF("###TLS Handshake return errid=%d, socket %p"), uError, sSocket);
        if (uError == 0)
        {
            SendRequest(sSocket);
        }
        return true;
    }

    virtual bool OnTlsError(UInt uError, Socket sSocket)
    {
        DEV_INFO(TF("###TLS Error errid=%d, socket %p"), uError, sSocket);
        return true;
    }

    virtual bool OnBeforeUpgrade(uintptr_t utSessionPtr, ULLong ullType)
    {
        DEV_INFO(TF("###OnBeforeUpgrade SessionPtr=%p Type[%llx]"), utSessionPtr, ullType);
        return true;
    }

    virtual bool OnAfterUpgrade(uintptr_t utSessionPtr, ULLong ullType)
    {
        DEV_INFO(TF("###OnAfterUpgrade SessionPtr=%p Type[%llx]"), utSessionPtr, ullType);
        CWSParser Parser;
        Parser.From(sTestLogin, (CWSTraits::WS_FINAL|CWSTraits::WS_TEXT|CWSTraits::WS_MASK));
        CBufReadStream brs(Parser.GetFrameSize(), Parser.GetCacheBuf());
        m_Client.Send(m_sTest, brs);
        return true;
    }

    virtual bool OnWebSession(uintptr_t utSessionPtr, ULLong ullType)
    {
#ifdef NETTLS_TEST
        if (ullType == CWEBSession::WEB_SESSION_HTTP_TLS)
#else
        if (ullType == CWEBSession::WEB_SESSION_HTTP)
#endif
        {
            CHTTPSession* pSession = reinterpret_cast<CHTTPSession*>(utSessionPtr);
            CHTTPResponse Response;
            pSession->GetParser().To(Response);

            DEV_INFO(TF("###OnWebSession http Version=%s, Status=%s, Message=%s"),
                     *(Response.GetVersion()), *(Response.GetStatus()), *(Response.GetMessage()));

            CHTTPTraits::MAP_HEADER& Header = Response.Header();
            DEV_INFO(TF("###OnWebSession http Header Count=%d"), Header.GetSize());
            Int i = 0;
            for (PINDEX index = Header.GetFirstIndex(); index != nullptr; )
            {
                CHTTPTraits::PAIR_HEADER* pPair = Header.GetNext(index);
                DEV_INFO(TF("###OnWebSession http %d-th Header key=%s, value=%s"),
                         i, pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
                ++i;
            }

            CHTTPTraits::ARY_COOKIE& aCookie = Response.GetCookies();
            DEV_INFO(TF("###OnWebSession http Cookie Array Count=%d"), aCookie.GetSize());
            for (Int c = 0; c < aCookie.GetSize(); ++c)
            {
                CHTTPTraits::MAP_COOKIE& Cookie = *(aCookie[c]);
                DEV_INFO(TF("###OnWebSession http Cookie Array-%d Count=%d"), c, Cookie.GetSize());
                i = 0;
                for (PINDEX index = Cookie.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_COOKIE* pPair = Cookie.GetNext(index);
                    DEV_INFO(TF("###OnWebSession http %d-th Cookie[%d] key=%s, value=%s"),
                             i, c, pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
                    ++i;
                }
            }

            const CHTTPTraits::ARY_BUF& Buf = Response.GetContent();

            DEV_INFO(TF("###OnWebSession http Content Count=%d"), Buf.GetSize());
            for (i = 0; i < Buf.GetSize(); ++i)
            {
                DEV_INFO(TF("###OnWebSession http %d-th Content Size=%d"), i, Buf[i].Size());
            }
        }
#ifdef NETTLS_TEST
        else if (ullType == CWEBSession::WEB_SESSION_WS_TLS)
#else
        else if (ullType == CWEBSession::WEB_SESSION_WS)
#endif
        {
            CWSSession* pSession = reinterpret_cast<CWSSession*>(utSessionPtr);
            CWSParser& Parser = pSession->GetParser();

            DEV_INFO(TF("###OnWebSession websocket Content=%s"), (PCXStr)Parser.GetCachePayload());
            if (m_nTestCount == 0)
            {
                ++m_nTestCount;
                Parser.From(sTestHello, (CWSTraits::WS_FINAL|CWSTraits::WS_TEXT|CWSTraits::WS_MASK));
                CBufReadStream brs(Parser.GetFrameSize(), Parser.GetCacheBuf());
                m_Client.Send(m_sTest, brs);
            }
        }
        return true;
    }

    bool Connect2Server(void)
    {
        bool bRet = false;
        if (m_sTest == 0)
        {
            bRet = m_Client.Create(m_sTest);
        }
        if (bRet)
        {
            bRet = m_Client.Connect(m_sTest, TF("localhost"), 3999);
#ifdef TEST_IPV6
            bRet = m_Client.Connect(m_sTest, TF("::1"), 3999);
#else
            //bRet = m_Client.Connect(m_sTest, TF("121.36.156.249"), 3999);
            //bRet = m_Client.Connect(m_sTest, TF("121.40.165.18"), 8801);
#endif
        }
        return bRet;
    }

    bool SendRequest(Socket sSocket)
    {
        m_bConnect = true;
        if (m_bWebSocket)
        {
            CHTTPRequest Request(CHTTPTraits::HTTP_CONTENT_NONE);
            CWSParser::UpgradeRequest(Request, "localhost:3999");
#ifdef TEST_IPV6
#else
            //CWSParser::UpgradeRequest(Request, "121.40.165.18:8801");//"121.36.156.249:3999");
#endif
            CCString strData;
            Request.Format(strData);
            DEV_DEBUG(TF("###Upgrade WebSocket Request=%s"), *strData);
            return m_Client.Send(sSocket, strData);
        }
        else
        {
            return m_Client.Send(sSocket, sTestGet);
        }
    }
private:
    Socket          m_sTest;
    bool            m_bConnect;
    bool            m_bWebSocket;
    Int             m_nTestCount;
#ifdef NETTLS_TEST
    CWEBTLSClient   m_Client;
#else
    CWEBClient      m_Client;
#endif
    CSyncEvent      m_Event;
};

IMPLEMENT_GLOBAL_LOADER( NetworkSystem)

void TestWeb(void)
{
    INIT_GLOBAL_LOADER( NetworkSystem, UUID_OF( CNetworkSystem ), NETWORK_MODULE_NAME);
    CTestClient Client;
    if (Client.Init())
    {
        Client.Wait();
        DEV_INFO(TF("wait okay"));
        CPlatform::SleepEx(2000);
    }
    Client.Exit();
    DUMP(nullptr);
    EXIT_GLOBAL_LOADER( NetworkSystem );
    DUMP(nullptr);

    getchar();
}
