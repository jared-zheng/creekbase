#include "stdafx.h"
#include "testweb.h"
#include "test.h"

using namespace CREEK;

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#define _tmain   main

#endif

Int _tmain(void)
{
    CoreInit();

    TestWeb();

    CoreExit();
    return 0;
}


