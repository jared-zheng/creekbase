ifdef BUILDDIR
THISDIR = $(SRCDIR)/public/webserver
TARGET_NAME = webserver

ifdef LIB_RUNTIME
OBJDIR_NAME = webserverlib
TARGET_SUFFIX = lib
DEBUG_SUFFIX = Debug
FILETYPE_SUFFIX = .a
else
OBJDIR_NAME = webserver
TARGET_SUFFIX = 
DEBUG_SUFFIX = Debug
FILETYPE_SUFFIX = .so
endif

TARGET_DEBUG = $(TARGET_NAME)$(TARGET_SUFFIX)d
TARGET_RELEASE = $(TARGET_NAME)$(TARGET_SUFFIX)

INC =  -I. -I./webserver -I../include
ifdef LIB_RUNTIME
ifdef CLANG_CC
CFLAGS =  -Wnon-virtual-dtor -Wall -std=c++11 -include "stdafx.h" -D_GNU_SOURCE -D_LIB -DLIB_RUNTIME
else
CFLAGS =  -Wnon-virtual-dtor -Winit-self -Wunreachable-code -Wzero-as-null-pointer-constant -Wall -std=c++11 -Winvalid-pch -include "stdafx.h" -D_GNU_SOURCE -D_LIB -DLIB_RUNTIME
endif
else
ifdef CLANG_CC
CFLAGS =  -Wnon-virtual-dtor -Wall -std=c++11 -include "stdafx.h" -D_GNU_SOURCE
else
CFLAGS =  -Wnon-virtual-dtor -Winit-self -Wunreachable-code -Wzero-as-null-pointer-constant -Wall -std=c++11 -Winvalid-pch -include "stdafx.h" -D_GNU_SOURCE
endif
endif
RESINC = 
LIBDIR = 
LIB =  -lpthread -lrt -ldl
ifdef LIB_RUNTIME
LDFLAGS = 
else
LDFLAGS =  -Wl,-rpath=.
endif

ifdef CLANG_CC
CFLAGS_DEBUG = $(CFLAGS)
CFLAGS_RELEASE = $(CFLAGS)
LDFLAGS_DEBUG = $(LDFLAGS)
LDFLAGS_RELEASE = $(LDFLAGS)
else
CFLAGS_DEBUG = $(CFLAGS) -pg
CFLAGS_RELEASE = $(CFLAGS)
LDFLAGS_DEBUG = $(LDFLAGS) -pg
LDFLAGS_RELEASE = $(LDFLAGS)
endif

INC_DEBUG_X64 =  $(INC)
CFLAGS_DEBUG_X64 =  $(CFLAGS_DEBUG) -m64 -g -D_DEBUG -DDEBUG
RESINC_DEBUG_X64 =  $(RESINC)
RCFLAGS_DEBUG_X64 =  $(RCFLAGS)
LIBDIR_DEBUG_X64 =  $(LIBDIR) -L$(BUILDDIR)/x64/bin/
ifdef LIB_RUNTIME
LIB_DEBUG_X64 = $(LIB) -lcore$(TARGET_SUFFIX)$(DEBUG_SUFFIX) -lnetwork$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
# -lmbedtls$(TARGET_SUFFIX)$(DEBUG_SUFFIX) -lnetworktls$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
else
LIB_DEBUG_X64 = $(LIB) -lcore$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
# -lmbedtls$(TARGET_SUFFIX)$(DEBUG_SUFFIX) -lnetworktls$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
endif
LDFLAGS_DEBUG_X64 =  $(LDFLAGS_DEBUG) -m64
OBJDIR_DEBUG_X64 = $(BUILDDIR)/x64/obj/debug/$(OBJDIR_NAME)
DEP_DEBUG_X64 = 
OUT_DEBUG_X64 = $(BUILDDIR)/x64/bin/$(TARGET_DEBUG)

INC_RELEASE_X64 =  $(INC)
CFLAGS_RELEASE_X64 =  $(CFLAGS_RELEASE) -O2 -m64 -DNDEBUG
RESINC_RELEASE_X64 =  $(RESINC)
RCFLAGS_RELEASE_X64 =  $(RCFLAGS)
LIBDIR_RELEASE_X64 =  $(LIBDIR) -L$(BUILDDIR)/x64/bin/
ifdef LIB_RUNTIME
LIB_RELEASE_X64 = $(LIB) -lcore$(TARGET_SUFFIX) -lnetwork$(TARGET_SUFFIX)
else
LIB_RELEASE_X64 = $(LIB) -lcore$(TARGET_SUFFIX)
endif
LDFLAGS_RELEASE_X64 =  $(LDFLAGS_RELEASE) -s -m64
OBJDIR_RELEASE_X64 = $(BUILDDIR)/x64/obj/release/$(OBJDIR_NAME)
DEP_RELEASE_X64 = 
OUT_RELEASE_X64 = $(BUILDDIR)/x64/bin/$(TARGET_RELEASE)

INC_DEBUG_X86 =  $(INC)
CFLAGS_DEBUG_X86 =  $(CFLAGS_DEBUG) -m32 -g -D_DEBUG -DDEBUG
RESINC_DEBUG_X86 =  $(RESINC)
RCFLAGS_DEBUG_X86 =  $(RCFLAGS)
LIBDIR_DEBUG_X86 =  $(LIBDIR) -L$(BUILDDIR)/x86/bin/
ifdef LIB_RUNTIME
LIB_DEBUG_X86 = $(LIB) -lcore$(TARGET_SUFFIX)$(DEBUG_SUFFIX) -lnetwork$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
else
LIB_DEBUG_X86 = $(LIB) -lcore$(TARGET_SUFFIX)$(DEBUG_SUFFIX)
endif
LDFLAGS_DEBUG_X86 =  $(LDFLAGS_DEBUG) -m32
OBJDIR_DEBUG_X86 = $(BUILDDIR)/x86/obj/debug/$(OBJDIR_NAME)
DEP_DEBUG_X86 = 
OUT_DEBUG_X86 = $(BUILDDIR)/x86/bin/$(TARGET_DEBUG)

INC_RELEASE_X86 =  $(INC)
CFLAGS_RELEASE_X86 =  $(CFLAGS_RELEASE) -O2 -m32 -DNDEBUG
RESINC_RELEASE_X86 =  $(RESINC)
RCFLAGS_RELEASE_X86 =  $(RCFLAGS)
LIBDIR_RELEASE_X86 =  $(LIBDIR) -L$(BUILDDIR)/x86/bin/
ifdef LIB_RUNTIME
LIB_RELEASE_X86 = $(LIB) -lcore$(TARGET_SUFFIX) -lnetwork$(TARGET_SUFFIX)
else
LIB_RELEASE_X86 = $(LIB) -lcore$(TARGET_SUFFIX)
endif
LDFLAGS_RELEASE_X86 =  $(LDFLAGS_RELEASE) -s -m32
OBJDIR_RELEASE_X86 = $(BUILDDIR)/x86/obj/release/$(OBJDIR_NAME)
DEP_RELEASE_X86 = 
OUT_RELEASE_X86 = $(BUILDDIR)/x86/bin/$(TARGET_RELEASE)

OBJ_DEBUG_X64 = $(OBJDIR_DEBUG_X64)/stdafx.o $(OBJDIR_DEBUG_X64)/test.o $(OBJDIR_DEBUG_X64)/testweb.o

OBJ_RELEASE_X64 = $(OBJDIR_RELEASE_X64)/stdafx.o $(OBJDIR_RELEASE_X64)/test.o $(OBJDIR_RELEASE_X64)/testweb.o

OBJ_DEBUG_X86 = $(OBJDIR_DEBUG_X86)/stdafx.o $(OBJDIR_DEBUG_X86)/test.o $(OBJDIR_DEBUG_X86)/testweb.o

OBJ_RELEASE_X86 = $(OBJDIR_RELEASE_X86)/stdafx.o $(OBJDIR_RELEASE_X86)/test.o $(OBJDIR_RELEASE_X86)/testweb.o

all: debug_x64 release_x64 debug_x86 release_x86

clean: clean_debug_x64 clean_release_x64 clean_debug_x86 clean_release_x86

before_debug_x64: 
	test -d $(BUILDDIR)/x64/bin || mkdir -p $(BUILDDIR)/x64/bin
	test -d $(OBJDIR_DEBUG_X64) || mkdir -p $(OBJDIR_DEBUG_X64)

after_debug_x64: 

debug_x64: before_debug_x64 out_debug_x64 after_debug_x64

out_debug_x64: before_debug_x64 $(OBJ_DEBUG_X64) $(DEP_DEBUG_X64)
	$(LD) $(LIBDIR_DEBUG_X64) -o $(OUT_DEBUG_X64) $(OBJ_DEBUG_X64)  $(LDFLAGS_DEBUG_X64) $(LIB_DEBUG_X64)

$(OBJDIR_DEBUG_X64)/stdafx.o: $(THISDIR)/stdafx.cpp
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/stdafx.cpp -o $(OBJDIR_DEBUG_X64)/stdafx.o

$(OBJDIR_DEBUG_X64)/test.o: $(THISDIR)/test.cpp
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/test.cpp -o $(OBJDIR_DEBUG_X64)/test.o

$(OBJDIR_DEBUG_X64)/testweb.o: $(THISDIR)/testweb.cpp
	$(CXX) $(CFLAGS_DEBUG_X64) $(INC_DEBUG_X64) -c $(THISDIR)/testweb.cpp -o $(OBJDIR_DEBUG_X64)/testweb.o

clean_debug_x64: 
	rm -rf $(OBJDIR_DEBUG_X64)
	rm -rf $(BUILDDIR)/x64/bin

before_release_x64: 
	test -d $(BUILDDIR)/x64/bin || mkdir -p $(BUILDDIR)/x64/bin
	test -d $(OBJDIR_RELEASE_X64) || mkdir -p $(OBJDIR_RELEASE_X64)

after_release_x64: 

release_x64: before_release_x64 out_release_x64 after_release_x64

out_release_x64: before_release_x64 $(OBJ_RELEASE_X64) $(DEP_RELEASE_X64)
	$(LD) $(LIBDIR_RELEASE_X64) -o $(OUT_RELEASE_X64) $(OBJ_RELEASE_X64)  $(LDFLAGS_RELEASE_X64) $(LIB_RELEASE_X64)

$(OBJDIR_RELEASE_X64)/stdafx.o: $(THISDIR)/stdafx.cpp
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/stdafx.cpp -o $(OBJDIR_RELEASE_X64)/stdafx.o

$(OBJDIR_RELEASE_X64)/test.o: $(THISDIR)/test.cpp
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/test.cpp -o $(OBJDIR_RELEASE_X64)/test.o

$(OBJDIR_RELEASE_X64)/testweb.o: $(THISDIR)/testweb.cpp
	$(CXX) $(CFLAGS_RELEASE_X64) $(INC_RELEASE_X64) -c $(THISDIR)/testweb.cpp -o $(OBJDIR_RELEASE_X64)/testweb.o

clean_release_x64: 
	rm -rf $(OBJDIR_RELEASE_X64)
	rm -rf $(BUILDDIR)/x64/bin

before_debug_x86: 
	test -d $(BUILDDIR)/x86/bin || mkdir -p $(BUILDDIR)/x86/bin
	test -d $(OBJDIR_DEBUG_X86) || mkdir -p $(OBJDIR_DEBUG_X86)

after_debug_x86: 

debug_x86: before_debug_x86 out_debug_x86 after_debug_x86

out_debug_x86: before_debug_x86 $(OBJ_DEBUG_X86) $(DEP_DEBUG_X86)
	$(LD) $(LIBDIR_DEBUG_X86) -o $(OUT_DEBUG_X86) $(OBJ_DEBUG_X86)  $(LDFLAGS_DEBUG_X86) $(LIB_DEBUG_X86)

$(OBJDIR_DEBUG_X86)/stdafx.o: $(THISDIR)/stdafx.cpp
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/stdafx.cpp -o $(OBJDIR_DEBUG_X86)/stdafx.o

$(OBJDIR_DEBUG_X86)/test.o: $(THISDIR)/test.cpp
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/test.cpp -o $(OBJDIR_DEBUG_X86)/test.o

$(OBJDIR_DEBUG_X86)/testweb.o: $(THISDIR)/testweb.cpp
	$(CXX) $(CFLAGS_DEBUG_X86) $(INC_DEBUG_X86) -c $(THISDIR)/testweb.cpp -o $(OBJDIR_DEBUG_X86)/testweb.o

clean_debug_x86: 
	rm -rf $(OBJDIR_DEBUG_X86)
	rm -rf $(BUILDDIR)/x86/bin

before_release_x86: 
	test -d $(BUILDDIR)/x86/bin || mkdir -p $(BUILDDIR)/x86/bin
	test -d $(OBJDIR_RELEASE_X86) || mkdir -p $(OBJDIR_RELEASE_X86)

after_release_x86: 

release_x86: before_release_x86 out_release_x86 after_release_x86

out_release_x86: before_release_x86 $(OBJ_RELEASE_X86) $(DEP_RELEASE_X86)
	$(LD) $(LIBDIR_RELEASE_X86) -o $(OUT_RELEASE_X86) $(OBJ_RELEASE_X86)  $(LDFLAGS_RELEASE_X86) $(LIB_RELEASE_X86)

$(OBJDIR_RELEASE_X86)/stdafx.o: $(THISDIR)/stdafx.cpp
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/stdafx.cpp -o $(OBJDIR_RELEASE_X86)/stdafx.o

$(OBJDIR_RELEASE_X86)/test.o: $(THISDIR)/test.cpp
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/test.cpp -o $(OBJDIR_RELEASE_X86)/test.o

$(OBJDIR_RELEASE_X86)/testweb.o: $(THISDIR)/testweb.cpp
	$(CXX) $(CFLAGS_RELEASE_X86) $(INC_RELEASE_X86) -c $(THISDIR)/testweb.cpp -o $(OBJDIR_RELEASE_X86)/testweb.o

clean_release_x86: 
	rm -rf $(OBJDIR_RELEASE_X86)
	rm -rf $(BUILDDIR)/x86/bin
else
error_build: 
	echo "!!!please use top Makefile to build!!!"
endif

.PHONY: before_debug_x64 after_debug_x64 clean_debug_x64 before_release_x64 after_release_x64 clean_release_x64 before_debug_x86 after_debug_x86 clean_debug_x86 before_release_x86 after_release_x86 clean_release_x86 error_build

