#pragma once

#ifdef _WIN32
#include "windows/targetconfig.h"
#include "windows/targetmodule.h"
#else
#include "linux/targetconfig.h"
#include "linux/targetmodule.h"
#endif
