#include "stdafx.h"
#include "testweb.h"

//#define NETTLS_TEST

#ifdef NETTLS_TEST
#include "networkhttptls.h"

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
#ifdef __RUNTIME_DEBUG__
#ifdef LIB_RUNTIME
#pragma comment(lib, "networktlslibDebug_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlslibDebug_x64_v143_MultiByte.lib")
#else
#pragma comment(lib, "networktlsDebug_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlsDebug_x64_v143_MultiByte.lib")
#endif
#else
#ifdef LIB_RUNTIME
#pragma comment(lib, "networktlslib_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlslib_x64_v143_MultiByte.lib")
#else
#pragma comment(lib, "networktlsRelease_x64_v143_MultiByte.lib")
#pragma comment(lib, "mbedtlsRelease_x64_v143_MultiByte.lib")
#endif
#endif
#endif

#else
#include "networkhttp.h"
#endif

using namespace CREEK;
class CTestServer : public CNulPackNetworkEventHandler
{
    DECLARE_CLASS_LOADER( CTestServer, NetworkSystem )
public:
    CTestServer(void) : m_bNetBuffer(true)
    {
    }

    virtual ~CTestServer(void)
    {
    }

    bool Init(void)
    {
        CNetworkPtr NetworkPtr;
        LOADER_CLASS_CREEATE( CTestServer, NetworkSystem, UUID_OF( CNetwork ), NetworkPtr.Cast<CComponent>());
        if (NetworkPtr != nullptr)
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs = CNETTraits::ATTR_THREAD | CNETTraits::ATTR_REUSEADDR;// | CNETTraits::ATTR_ACK_DETECT | CNETTraits::ATTR_TIMEOUT_DETECT;
#ifdef TEST_IPV6
            attr.nAttrs |= CNETTraits::ATTR_IPV6;
#endif
            attr.nThread  = 2;
            attr.nAckTime = 6000;
            attr.nTimeout = 80 * 1000;
            if (NetworkPtr->Init(attr, this) == RET_OKAY)
            {
                CNETTraits::ARY_STRING strAddrs;
#ifdef TEST_IPV6
                if (NetworkPtr->GetLocalAddr(strAddrs))
#else
                if (NetworkPtr->GetLocalAddr(strAddrs, CNETTraits::ATTR_IPV4))
#endif
                {
                    for (Int i = 0; i < strAddrs.GetSize(); ++i)
                    {
                        DEV_INFO(TF(" *IP:%s"), *(strAddrs[i]));
                    }
                }
                if (m_bNetBuffer)
                {
                    m_Server.SetNetBufferMode();
                }
#ifdef NETTLS_TEST
                CNetworkTLS::CONFIG_PARAM Param;
                Param.strCertFile = TF("test.crt");
                Param.strKeyFile  = TF("test.key");
                Param.strKeyPass  = TF("abc#123");
                if (m_Server.Init(NetworkPtr, Param, *this))
#else
                if (m_Server.Init(NetworkPtr, *this))
#endif
                {
                    return m_Server.Listen(nullptr, 3999);
                }
            }
        }
        return false;
    }

    void Exit(void)
    {
        m_Server.Exit();
    }

    void Wait(void)
    {
        m_Event.Wait();
    }
protected:
    virtual bool OnTcpAccept(Socket sAccept, Socket sListen)
    {
        DEV_INFO(TF("###Accept socket=%p, listen socket %p"), sAccept, sListen);
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        return true;
    }

    virtual bool OnTlsHandShake(UInt uError, Socket sSocket)
    {
        DEV_INFO(TF("###TLS Handshake return errid=%d, socket %p"), uError, sSocket);
        return true;
    }

    virtual bool OnTlsError(UInt uError, Socket sSocket)
    {
        DEV_INFO(TF("###TLS Error errid=%d, socket %p"), uError, sSocket);
        return true;
    }

    virtual bool OnBeforeUpgrade(uintptr_t utSessionPtr, ULLong ullType)
    {
        DEV_INFO(TF("###OnBeforeUpgrade SessionPtr=%p Type[%llx]"), utSessionPtr, ullType);
        return true;
    }

    virtual bool OnAfterUpgrade(uintptr_t utSessionPtr, ULLong ullType)
    {
        DEV_INFO(TF("###OnAfterUpgrade SessionPtr=%p Type[%llx]"), utSessionPtr, ullType);
        CWSSession* pSession = reinterpret_cast<CWSSession*>(utSessionPtr);
        m_Server.GetNetwork()->SetAttr(pSession->GetSocket(), 100);
        return true;
    }

    virtual bool OnWebSession(uintptr_t utSessionPtr, ULLong ullType)
    {
#ifdef NETTLS_TEST
        if (ullType == CWEBSession::WEB_SESSION_HTTP_TLS)
#else
        if (ullType == CWEBSession::WEB_SESSION_HTTP)
#endif
        {
            CHTTPSession* pSession = reinterpret_cast<CHTTPSession*>(utSessionPtr);
            if (m_bNetBuffer == false)
            {
                CHTTPRequest Request;
                pSession->GetParser().To(Request);

                DEV_INFO(TF("###OnWebSession http Method=%s, Path=%s, Version=%s"),
                    *(Request.GetMethod()), *(Request.GetPath()), *(Request.GetVersion()));

                CHTTPTraits::MAP_HEADER& Header = Request.Header();
                DEV_INFO(TF("###OnWebSession http Header Count=%d"), Header.GetSize());
                Int i = 0;
                for (PINDEX index = Header.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_HEADER* pPair = Header.GetNext(index);
                    DEV_INFO(TF("###OnWebSession http %d-th Header key=%s, value=%s"),
                        i, pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
                    ++i;
                }

                CHTTPTraits::MAP_COOKIE& Cookie = Request.Cookie();
                DEV_INFO(TF("###OnWebSession http Cookie Count=%d"), Cookie.GetSize());
                i = 0;
                for (PINDEX index = Cookie.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_COOKIE* pPair = Cookie.GetNext(index);
                    DEV_INFO(TF("###OnWebSession http %d-th Cookie key=%s, value=%s"),
                        i, pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
                    ++i;
                }

                CHTTPTraits::MAP_PARAM& Param = Request.Param();
                DEV_INFO(TF("###OnWebSession http Param Count=%d"), Param.GetSize());
                i = 0;
                for (PINDEX index = Param.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_PARAM* pPair = Param.GetNext(index);
                    DEV_INFO(TF("###OnWebSession http %d-th Param key=%s, value=%s"),
                        i, pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
                    ++i;
                }

                const CHTTPTraits::ARY_BUF& Buf = Request.GetContent();

                DEV_INFO(TF("###OnWebSession http Content Count=%d"), Buf.GetSize());
                for (i = 0; i < Buf.GetSize(); ++i)
                {
                    DEV_INFO(TF("###OnWebSession http %d-th Content Size=%d"), i, Buf[i].Size());
                }

                CCString strData = "HTTP/1.1 200 OK\r\nServer: nginx\r\nContent-Type: text/html;charset=UTF-8\r\nVary: Accept-Encoding\r\nCache-Control: no-store\r\nPragrma: no-cache\r\nCache-Control: no-cache\r\nContent-Length:87\r\n\r\n<HTML><HEAD><TITLE>CREEK REST API</TITLE></HEAD><BODY>CREEK REST API TEST</BODY></HTML>";

                m_Server.Send(pSession->GetSocket(), strData);
            }
            else
            {
                CHTTPRequestRef RequestRef;
                pSession->GetParser().Detach(RequestRef);

                CCString strMethod;
                RequestRef.GetMethod(strMethod);

                CCString strPath;
                RequestRef.GetPath(strPath);

                CCString strVersion;
                RequestRef.GetVersion(strVersion);

                DEV_INFO(TF("###OnWebSession http Method=%s, Path=%s, Version=%s"), *strMethod, *strPath, *strVersion);

                CHTTPTraits::MAP_HEADER_REF& Header = RequestRef.Header();
                DEV_INFO(TF("###OnWebSession http Header Count=%d"), Header.GetSize());
                Int i = 0;
                for (PINDEX index = Header.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_HEADER_REF* pPair = Header.GetNext(index);
                    CCString strKey = pPair->m_K;
                    CCString strValue = pPair->m_V;
                    DEV_INFO(TF("###OnWebSession http %d-th Header key=%s, value=%s"),
                        i, strKey.GetBuffer(), strValue.GetBuffer());
                    ++i;
                }

                CHTTPTraits::MAP_COOKIE_REF& Cookie = RequestRef.Cookie();
                DEV_INFO(TF("###OnWebSession http Cookie Count=%d"), Cookie.GetSize());
                i = 0;
                for (PINDEX index = Cookie.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_COOKIE_REF* pPair = Cookie.GetNext(index);
                    CCString strKey = pPair->m_K;
                    CCString strValue = pPair->m_V;
                    DEV_INFO(TF("###OnWebSession http %d-th Cookie key=%s, value=%s"),
                        i, strKey.GetBuffer(), strValue.GetBuffer());
                    ++i;
                }

                CHTTPTraits::MAP_PARAM_REF& Param = RequestRef.Param();
                DEV_INFO(TF("###OnWebSession http Param Count=%d"), Param.GetSize());
                i = 0;
                for (PINDEX index = Param.GetFirstIndex(); index != nullptr; )
                {
                    CHTTPTraits::PAIR_PARAM_REF* pPair = Param.GetNext(index);
                    CCString strKey = pPair->m_K;
                    CCString strValue = pPair->m_V;
                    DEV_INFO(TF("###OnWebSession http %d-th Param key=%s, value=%s"),
                        i, strKey.GetBuffer(), strValue.GetBuffer());
                    ++i;
                }

                const CHTTPTraits::ARY_BUF& Buf = RequestRef.GetContent();

                DEV_INFO(TF("###OnWebSession http Content Count=%d"), Buf.GetSize());
                for (i = 0; i < Buf.GetSize(); ++i)
                {
                    DEV_INFO(TF("###OnWebSession http %d-th Content Size=%d"), i, Buf[i].Size());
                }

                CStreamScopePtr StreamPtr;
                m_Server.GetNetwork()->AllocBuffer(StreamPtr, RequestRef.NetBuffer().pCache);
                StreamPtr->Write((PByte)"HTTP/1.1 200 OK\r\nServer: nginx\r\nContent-Type: text/html;charset=UTF-8\r\nVary: Accept-Encoding\r\nCache-Control: no-store\r\nPragrma: no-cache\r\nCache-Control: no-cache\r\nContent-Length:87\r\n\r\n<HTML><HEAD><TITLE>CREEK REST API</TITLE></HEAD><BODY>CREEK REST API TEST</BODY></HTML>", 271);
                m_Server.Send(pSession->GetSocket(), StreamPtr);
                RequestRef.Detach();
            }
        }
#ifdef NETTLS_TEST
        else if (ullType == CWEBSession::WEB_SESSION_WS_TLS)
#else
        else if (ullType == CWEBSession::WEB_SESSION_WS)
#endif
        {
            CWSSession* pSession = reinterpret_cast<CWSSession*>(utSessionPtr);
            CWSParser& Parser = pSession->GetParser();
            if (m_bNetBuffer == false)
            {
                size_t stBuf = (size_t)Parser.GetFrameSize();

                CBufWriteStream bws(stBuf + 1024);
                Parser.To(bws);

                Int nCode = Parser.GetFrameCode();
                nCode &= ~(CWSTraits::WS_MASK);

                CBufReadStream brs;
                brs.Refer(bws);

                Parser.From(brs, nCode);

                bws.Seek(0);
                Parser.Format(bws);

                m_Server.Send(pSession->GetSocket(), bws.GetBuf(), bws.Tell());
            }
            else
            {
                NET_BUFFER& NetBuffer = Parser.NetBuffer();
                CStreamScopePtr StreamPtr;
                m_Server.GetNetwork()->ReuseBuffer(StreamPtr, NetBuffer.index, NetBuffer.pCache, 4);

                Int nFrameSize = 0;
                Int nFrameCode = 0;
                Int nFrameData = 0;
                Parser.Detach(nFrameSize, nFrameCode, nFrameData);

                nFrameCode &= CWSTraits::WS_HEAD_MASK;
                nFrameSize -= nFrameData;

                nFrameSize = (Int)CWSParser::SetFrameHead(StreamPtr->GetBuf(), (UShort)nFrameCode, (size_t)nFrameSize);

                StreamPtr->Seek((SeekPos)nFrameSize);
                m_Server.Send(pSession->GetSocket(), StreamPtr);
            }
        }
        return true;
    }
private:
    bool            m_bNetBuffer;
#ifdef NETTLS_TEST
    CWEBTLSServer   m_Server;
#else
    CWEBServer      m_Server;
#endif
    CSyncEvent      m_Event;
};

IMPLEMENT_CLASS_LOADER( CTestServer, NetworkSystem )

void TestWeb(void)
{
    //INIT_CLASS_LOADER( CTestServer, NetworkSystem, UUID_OF( CNetworkSystem ), TF("network-rio-Debug.dll"));
    INIT_CLASS_LOADER( CTestServer, NetworkSystem, UUID_OF( CNetworkSystem ), NETWORK_MODULE_NAME);
    CTestServer Server;
    if (Server.Init())
    {
        Server.Wait();
        DEV_INFO(TF("wait okay"));
        CPlatform::SleepEx(2000);
    }
    Server.Exit();
    //DUMP(nullptr);
    EXIT_CLASS_LOADER( CTestServer, NetworkSystem );
    //DUMP(nullptr);

    getchar();
}
