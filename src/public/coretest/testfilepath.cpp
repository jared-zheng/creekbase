#include "stdafx.h"
#include "keyvalue.h"
#include "filepath.h"
#include "exception.h"
#include "filelog.h"
#include "testfilepath.h"

using namespace CREEK;

void TestFilePath(void)
{
  //  CFileLog _log;
  //  _log.SetMaxSize(1024 * 500);
  //  LOG_INFO(_log, TF("TestFilePath"));
    CVarType v1;
    v1 = TF("hello");
    DEV_DUMP(TF("v1 = %s"), v1.GetXString().GetBuffer());

    v1 = 1234567890;
    DEV_DUMP(TF("v1 = %d"), v1.GetInt());

    CTArray<Int> aInt;
    aInt.Add(100);
    aInt.Add(200);
    v1 = std::move(aInt);

    DEV_DUMP(TF("v1 = %d"), v1.AnyCast<CTArray<Int>>().GetSize());

    CVarType v2(std::move(v1));
    DEV_DUMP(TF("v1 type none = %d, v2 = %d"), v1.IsNone(), v2.AnyCast<CTArray<Int>>().GetSize());

    v1 = true;
    DEV_DUMP(TF("v1 = %d"), v1.GetBoolean());

    CVarType v3 = v1;
    DEV_DUMP(TF("v3 = %d"), v3.GetBoolean());

    CException ee(999999999, TF("werquykfkkhjkjk"));

    v3.SetValue(std::move(ee));

    CException* pee = v3.TryCast<CException>();
    DEV_INFO(TF("v3 val = %d-%s"), pee->GetCode(), pee->GetInfo().GetBuffer());

    try
    {
        CKeyValue kv;
        kv.SetValue(TF("testkey"), TF("testvalue"));
        kv.SetValue(TF("testkey2"), (Long)102345);

        Long lRet = 0;
        kv.GetValue(TF("testkey2"), lRet);

        DEV_INFO(TF("testkey2 in key-value val = %d"), lRet);

        kv.SetValue(TF("testkey2"), (Long)987654);
        kv.GetValue(TF("testkey2"), lRet);
        DEV_INFO(TF("testkey2 in key-value val = %d"), lRet);

        CException e1(88888888, TF("gerareahjlhhilgifkjdhg"));
        kv.SetAny<CException>(TF("testAny1"), e1);
        //kv.SetAny<CException>(TF("testAny1"), std::move(e1));
        CException* pe1 = kv.TryCast<CException>(TF("testAny1"));
        DEV_INFO(TF("testAny1 in key-value val = %d-%s"), pe1->GetCode(), pe1->GetInfo().GetBuffer());
        DEV_INFO(TF("e1 val = %d-%s"), e1.GetCode(), e1.GetInfo().GetBuffer());

        kv.SetAny<CException>(TF("testAny"));
        CException& except = kv.AnyCast<CException>(TF("testAny"));
        except.SetCode(10000);
        except.SetInfo(TF("ewopriqsidfdhlsadjf"));

        CException* pe = kv.TryCast<CException>(TF("testAny"));
        DEV_INFO(TF("testAny in key-value val = %d-%s"), pe->GetCode(), pe->GetInfo().GetBuffer());


        throw CException(12, TF("testthrow"));
    }
    catch (CException& e)
    {
        DEV_WARN(TF("Exception %d-%s"), e.GetCode(), e.GetInfo().GetBuffer());
    }

    CFilePath fp;
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    // TEST1�ַ�����Ӧ����·��, [F:\\]
    fp.SetPath(TF("TEST1"), TF("E:\\Temp\\"));

    // ��ȡTEST1��Ӧ·�����������ļ�(������Ŀ¼����Ŀ¼����ļ�)
    Int nCount = 0;
    CTList<CString> lf;
    fp.Find(lf, TF("*"), TF("TEST1"), 29);
    for (PINDEX index = lf.GetHeadIndex(); index != nullptr; )
    {
        CString& strPath = lf.GetNext(index);
        DEV_INFO(TF("%d file is %s"), nCount, *strPath);
        ++nCount;
    }
    bool b1 = fp.IsExist(TF("F:\\sddssdfsdf"));
    bool b2 = fp.IsExist(TF("F:\\sddssdfsdf2"));
    bool baa = fp.IsExist(TF("E:\\Temp\\11912901HWInfo.rar"));
    bool bab = fp.IsExist(TF("E:\\Temp\\IMG_20190422_090721.jpg"));
    bool b3 = fp.IsFolder(TF("F:\\sddssdfsdf"));
    bool b4 = fp.IsFolder(TF("F:\\sddssdfsdf2"));
    bool b31 = fp.IsFolder(TF("E:\\Temp\\11912901HWInfo.rar"));
    bool b32 = fp.IsFolder(TF("E:\\Temp\\IMG_20190422_090721.jpg"));
    bool b41 = fp.IsFile(TF("F:\\sddssdfsdf"));
    bool b42 = fp.IsFile(TF("F:\\sddssdfsdf2"));
    bool b5 = fp.IsFile(TF("E:\\Temp\\11912901HWInfo.rar"));
    bool b6 = fp.IsFile(TF("E:\\Temp\\IMG_20190422_090721.jpg"));

    bool b7 = fp.IsExist(TF("dev"), TF("TEST1"));
    bool b8 = fp.IsExist(TF("dev2"), TF("TEST1"));
    bool bba = fp.IsExist(TF("《传奇3》三大版本界面设计评测.pdf"), TF("TEST1"));
    bool bbb = fp.IsExist(TF("PCHunter_free2.zip"), TF("TEST1"));
    bool b9 = fp.IsFolder(TF("dev"), TF("TEST1"));
    bool ba = fp.IsFolder(TF("dev2"), TF("TEST1"));
    bool b51 = fp.IsFolder(TF("《传奇3》三大版本界面设计评测.pdf"), TF("TEST1"));
    bool b52 = fp.IsFolder(TF("PCHunter_free2.zip"), TF("TEST1"));
    bool b61 = fp.IsFile(TF("dev"), TF("TEST1"));
    bool b62 = fp.IsFile(TF("dev2"), TF("TEST1"));
    bool bb = fp.IsFile(TF("《传奇3》三大版本界面设计评测.pdf"), TF("TEST1"));
    bool bc = fp.IsFile(TF("PCHunter_free2.zip"), TF("TEST1"));
    //// TEST2��Ӧ·�����洴���ļ���
    fp.SetPath(TF("TEST2"), TF("D:\\haha\\test"));
    fp.CreateFolder(TF("TT\\tt1\\tt2\\tt3\\tt4\\tt5\\tt6\\tt7\\tt8\\tt9\\tt123\\"), TF("TEST2"));

    //// TEST1��Ӧ·������������������·��
    CString str1 = TF("TT\\tt1\\tt2\\tt3\\tt4\\tt5\\tt6\\tt7\\tt8\\tt9\\tt123\\");
    fp.GetFullPath(TF("TEST2"), str1);
    DEV_INFO(*str1);
    //// ɾ��TEST1��Ӧ·������������������·���µ������ļ�(������Ŀ¼����Ŀ¼����ļ�)
    CPlatform::SleepEx(6000);
    fp.DeleteFolder(nullptr, TF("TEST2"));
#else
    fp.SetPath(TF("TEST1"), TF("/home/jaredz/Documents"));

    CFileAttr fa;
    fp.FileAttr(fa, TF("Video Conference.pptx"), TF("TEST1"));
    // ��ȡTEST1��Ӧ·�����������ļ�(������Ŀ¼����Ŀ¼����ļ�)
    Int nCount = 0;
    CTList<CString> lf;
    fp.Find(lf, TF("*"), TF("TEST1"), 13);
    for (PINDEX index = lf.GetHeadIndex(); index != nullptr; )
    {
        CString& strPath = lf.GetNext(index);
        DEV_INFO(TF("%d file is %s"), nCount, *strPath);
        ++nCount;
    }
    // TEST2��Ӧ·�����洴���ļ���
    fp.SetPath(TF("TEST2"), TF("/home/jaredz/Downloads/test"));
    fp.CreateFolder(TF("TT/tt1/tt2/tt3/tt4/tt5/tt6/tt7/tt8/tt9/tt123/"), TF("TEST2"));

    // TEST1��Ӧ·������������������·��
    CString str1 = TF("TT/tt1/tt2/tt3/tt4/tt5/tt6/tt7/tt8/tt9/tt123/");
    fp.GetFullPath(TF("TEST2"), str1);
    DEV_INFO(*str1);
    // ɾ��TEST1��Ӧ·������������������·���µ������ļ�(������Ŀ¼����Ŀ¼����ļ�)
    fp.DeleteFolder(nullptr, TF("TEST2"));
#endif
}
