﻿#include "stdafx.h"
#include "networkhttp.h"
#include "testhttp.h"

using namespace CREEK;

struct HTTP_TEST
{
    bool   bComplete;
    CPCStr pszHttp;
};

HTTP_TEST gs_http[] = 
{

{true,
"GET /test HTTP/1.1\r\n"
"User-Agent: curl/7.18.0 (i486-pc-linux-gnu) libcurl/7.18.0 OpenSSL/0.9.8g zlib/1.2.3.3 libidn/1.1\r\n"
"Host: 0.0.0.0=5000\r\n"
"Accept: */*\r\n"
"\r\n"},

{true,
"GET /favicon.ico HTTP/1.1\r\n"
"Host: 0.0.0.0=5000\r\n"
"User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9) Gecko/2008061015 Firefox/3.0\r\n"
"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
"Accept-Language: en-us,en;q=0.5\r\n"
"Accept-Encoding: gzip,deflate\r\n"
"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n"
"Keep-Alive: 300\r\n"
"Connection: keep-alive\r\n"
"\r\n"},

{true,
"GET /dumbluck HTTP/1.1\r\n"
"aaaaaaaaaaaaa:++++++++++\r\n"
"\r\n"},

{true,
"GET /forums/1/topics/2375?page=1#posts-17408 HTTP/1.1\r\n"
"\r\n"},

{true,
"GET /get_no_headers_no_body/world HTTP/1.1\r\n"
"\r\n"},

{true,
"GET /get_one_header_no_body HTTP/1.1\r\n"
"Accept: */*\r\n"
"\r\n"},

{false,
"GET /get_funky_content_length_body_hello HTTP/1.0\r\n"
"conTENT-Length: 5\r\n"
"\r\n"
"HELLO"},

{false,
"GET /get_funky_content_length_body_hello HTTP/1.0\r\n"
"conTENT-Length: 5\r\n"
"\r\n"
"HELLO"},

{false,
"POST /post_identity_body_world?q=search#hey HTTP/1.1\r\n"
"Accept: */*\r\n"
"Transfer-Encoding: identity\r\n"
"Content-Length: 5\r\n"
"\r\n"
"World"},

{false,
"POST /post_chunked_all_your_base HTTP/1.1\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"1e\r\nall your base are belong to us\r\n"
"0\r\n"
"\r\n"},

{false,
"POST /chunked_w_nonsense_after_length HTTP/1.1\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"5; ilovew3;whattheluck=aretheseparametersfor\r\nhello\r\n"
"6; blahblah; blah\r\n world\r\n"
"0\r\n"
"\r\n"},

{true,
"GET /with_\"stupid\"_quotes?foo=\"bar\" HTTP/1.1\r\n\r\n"},

{true,
"GET /test HTTP/1.0\r\n"
"Host: 0.0.0.0:5000\r\n"
"User-Agent: ApacheBench/2.3\r\n"
"Accept: */*\r\n\r\n"},

{true,
"GET /test.cgi?foo=bar?baz HTTP/1.1\r\n\r\n"},

{true,
"GET /demo HTTP/1.1\r\n"
"Host: example.com\r\n"
"Connection: Upgrade\r\n"
"Sec-WebSocket-Key2: 12998 5 Y3 1  .P00\r\n"
"Sec-WebSocket-Protocol: sample\r\n"
"Upgrade: WebSocket\r\n"
"Sec-WebSocket-Key1: 4 @1  46546xW%0l 1 5\r\n"
"Origin: http://example.com\r\n"
"\r\n"
"Hot diggity dogg"},

{true,
"REPORT /test HTTP/1.1\r\n"
"\r\n"},

{true,
"GET http://hypnotoad.org?hail=all HTTP/1.1\r\n"
"\r\n"},

{false,
"POST / HTTP/1.1\r\n"
"Host: www.example.com\r\n"
"Content-Type: application/x-www-form-urlencoded\r\n"
"Content-Length: 4\r\n"
"\r\n"
"q=42\r\n"},

{true,
"GET /demo HTTP/1.1\r\n"
"Host: example.com\r\n"
"Connection: Something,\r\n"
" Upgrade, ,Keep-Alive\r\n"
"Sec-WebSocket-Key2: 12998 5 Y3 1  .P00\r\n"
"Sec-WebSocket-Protocol: sample\r\n"
"Upgrade: WebSocket\r\n"
"Sec-WebSocket-Key1: 4 @1  46546xW%0l 1 5\r\n"
"Origin: http://example.com\r\n"
"\r\n"
"Hot diggity dogg"},

{true,
"GET /demo HTTP/1.1\r\n"
"Connection: keep-alive, upgrade\r\n"
"Upgrade: WebSocket\r\n"
"\r\n"
"Hot diggity dogg"},

{false,
"HTTP/1.1 301 Moved Permanently\r\n"
"Location: http://www.google.com/\r\n"
"Content-Type: text/html; charset=UTF-8\r\n"
"Date: Sun, 26 Apr 2009 11:11:49 GMT\r\n"
"Expires: Tue, 26 May 2009 11:11:49 GMT\r\n"
"X-$PrototypeBI-Version: 1.6.0.3\r\n" /* $ char in header field */
"Cache-Control: public, max-age=2592000\r\n"
"Server: gws\r\n"
"Content-Length:  219  \r\n"
"\r\n"
"<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">\n"
"<TITLE>301 Moved</TITLE></HEAD><BODY>\n"
"<H1>301 Moved</H1>\n"
"The document has moved\n"
"<A HREF=\"http://www.google.com/\">here</A>.\r\n"
"</BODY></HTML>\r\n"},

{true,
"HTTP/1.1 200 OK\r\n"
"Date: Tue, 04 Aug 2009 07:59:32 GMT\r\n"
"Server: Apache\r\n"
"X-Powered-By: Servlet/2.5 JSP/2.1\r\n"
"Content-Type: text/xml; charset=utf-8\r\n"
"Connection: close\r\n"
"\r\n"
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
"  <SOAP-ENV:Body>\n"
"    <SOAP-ENV:Fault>\n"
"       <faultcode>SOAP-ENV:Client</faultcode>\n"
"       <faultstring>Client Error</faultstring>\n"
"    </SOAP-ENV:Fault>\n"
"  </SOAP-ENV:Body>\n"
"</SOAP-ENV:Envelope>"},

{true,
"HTTP/1.1 404 Not Found\r\n\r\n"},

{true,
"HTTP/1.1 301\r\n\r\n"},

{false,
"HTTP/1.1 200 OK\r\n"
"Content-Type: text/plain\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"25  \r\n"
"This is the data in the first chunk\r\n"
"\r\n"
"1C\r\n"
"and this is the second one\r\n"
"\r\n"
"0  \r\n"
"\r\n"},

{false,
"HTTP/1.1 200 OK\r\n"
"Server: DCLK-AdSvr\r\n"
"Content-Type: text/xml\r\n"
"Content-Length: 0\r\n"
"DCLK_imp: v7;x;114750856;0-0;0;17820020;0/0;21603567/21621457/1;;~okv=;dcmt=text/xml;;~cs=o\r\n\r\n"},

{false,
"HTTP/1.0 301 Moved Permanently\r\n"
"Date: Thu, 03 Jun 2010 09:56:32 GMT\r\n"
"Server: Apache/2.2.3 (Red Hat)\r\n"
"Cache-Control: public\r\n"
"Pragma: \r\n"
"Location: http://www.bonjourmadame.fr/\r\n"
"Vary: Accept-Encoding\r\n"
"Content-Length: 0\r\n"
"Content-Type: text/html; charset=UTF-8\r\n"
"Connection: keep-alive\r\n"
"\r\n"},

{false,
"HTTP/1.1 200 OK\r\n"
"Date: Tue, 28 Sep 2010 01:14:13 GMT\r\n"
"Server: Apache\r\n"
"Cache-Control: no-cache, must-revalidate\r\n"
"Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n"
".et-Cookie: PlaxoCS=1274804622353690521; path=/; domain=.plaxo.com\r\n"
"Vary: Accept-Encoding\r\n"
"_eep-Alive: timeout=45\r\n"
"_onnection: Keep-Alive\r\n"
"Transfer-Encoding: chunked\r\n"
"Content-Type: text/html\r\n"
"Connection: close\r\n"
"\r\n"
"0\r\n\r\n"},

{true,
"HTTP/1.1 200 OK\r\n"
"Content-Type: text/plain\r\n"
"\r\n"
"hello world"},

{true,
"HTTP/1.0 200 OK\r\n"
"Connection: keep-alive\r\n"
"\r\n"},

{true,
"HTTP/1.0 204 No content\r\n"
"Connection: keep-alive\r\n"
"\r\n"},

{true,
"HTTP/1.1 200 OK\r\n"
"\r\n"},

{false,
"HTTP/1.1 200 OK\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"0\r\n"
"\r\n"},

{false,
"HTTP/1.1 301 MovedPermanently\r\n"
"Date: Wed, 15 May 2013 17:06:33 GMT\r\n"
"Server: Server\r\n"
"x-amz-id-1: 0GPHKXSJQ826RK7GZEB2\r\n"
"p3p: policyref=\"http://www.amazon.com/w3c/p3p.xml\"},CP=\"CAO DSP LAW CUR ADM IVAo IVDo CONo OTPo OUR DELi PUBi OTRi BUS PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA HEA PRE LOC GOV OTC \"\r\n"
"x-amz-id-2: STN69VZxIFSz9YJLbz1GDbxpbjG6Qjmmq5E3DxRhOUw+Et0p4hr7c/Q8qNcx4oAD\r\n"
"Location: http://www.amazon.com/Dan-Brown/e/B000AP9DSU/ref=s9_pop_gw_al1?_encoding=UTF8&refinementId=618073011&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=center-2&pf_rd_r=0SHYY5BZXN3KR20BNFAY&pf_rd_t=101&pf_rd_p=1263340922&pf_rd_i=507846\r\n"
"Vary: Accept-Encoding,User-Agent\r\n"
"Content-Type: text/html; charset=ISO-8859-1\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"1\r\n"
"\n\r\n"
"0\r\n"
"\r\n"},

{ true,
"HTTP/1.1 200 \r\n"
"\r\n" },

{ false,
"HTTP/1.1 200 OK\r\n"
"Content-Length-X: 0\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"2\r\n"
"OK\r\n"
"0\r\n"
"\r\n" },

{ false,
"HTTP/1.1 200 OK\r\n"
"Connection: upgrade\r\n"
"Upgrade: h2c\r\n"
"Transfer-Encoding: chunked\r\n"
"\r\n"
"2\r\n"
"bo\r\n"
"2\r\n"
"dy\r\n"
"0\r\n"
"\r\n" },
};

#ifndef ARRAY_SIZE
# define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#endif

void TestHttp(void)
{
    CHTTPParser   Parser;
    CHTTPRequest  Request;
    CHTTPResponse Response;

    size_t stCount = ARRAY_SIZE(gs_http);
    for (size_t i = 0; i < stCount; ++i)
    {
        size_t stSize = CChar::Length(gs_http[i].pszHttp);
        Parser.Parse((PByte)gs_http[i].pszHttp, stSize, gs_http[i].bComplete);
        if (Parser.IsContentComplete())
        {
            if (Parser.IsResponseIdent() == false)
            {
                Parser.To(Request);
            }
            else
            {
                Parser.To(Response);
            }
            DEV_INFO(TF("!!!%dth http=%s parser complete!!!"), i, gs_http[i].pszHttp);
        }
        else
        {
            DEV_WARN(TF("!!!%dth http=%s parser failed!!!"), i, gs_http[i].pszHttp);
        }
        Parser.Close();
    }
}
