#include "stdafx.h"
#include "container.h"
#include "tstring.h"
#include "testhash.h"
#include <time.h>

using namespace CREEK;

CPCXStr STR_TEST[] =
{
    TF(""),
    TF("a"),
    TF("ab"),
    TF("abc"),
    TF("abcd"),
    TF("abcde"),
    TF("abcdef"),
    TF("abcdefg"),
    TF("A"),
    TF("aB"),
    TF("abC"),
    TF("abcD"),
    TF("abcdE"),
    TF("abcdeF"),
    TF("abcdefG"),
    TF("A"),
    TF("AB"),
    TF("ABC"),
    TF("ABCD"),
    TF("ABCDE"),
    TF("ABCDEF"),
    TF("ABCDEFG"),
};

void TestHash(void)
{
    for (Int i = 0; i < 21; ++i)
    {
        size_t stData  = CHash::Hash(STR_TEST[i]);
        DEV_INFO(TF("%7s hash   : %p"), STR_TEST[i], stData);

        UInt unData   = CHash::Hash32(STR_TEST[i]);
        DEV_INFO(TF("%7s hash32 : %p"), STR_TEST[i], unData);

        ULLong ullData = CHash::Hash64(STR_TEST[i]);
        DEV_INFO(TF("%7s hash64 : %llX"), STR_TEST[i], ullData);
    }
    size_t stData = CHash::Hash((PByte)STR_TEST[20], 7 * sizeof(XChar));
    DEV_INFO(TF("%s hash : %llX"), STR_TEST[20], stData);

    HASH_DUMP dump;
    dump.llUsedCount = 0x7FED7FED;
    dump.llUsedTick  = sizeof(UInt);
    CHash::Dump(dump);
    DEV_INFO(TF("hash : %llX-%llX"), dump.llUsedCount, dump.llUsedTick);

    dump.llUsedCount = 0xEDECEBEA;
    dump.llUsedTick  = sizeof(ULLong);
    CHash::Dump(dump);
    DEV_INFO(TF("hash : %llX-%llX"), dump.llUsedCount, dump.llUsedTick);

}