#include "stdafx.h"
#include "streambuf.h"
#include "streamfile.h"
#include "codec.h"
#include "crypto.h"
#include "module.h"
#include "testmodule.h"

#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21

/* F, G, H and I are basic MD5 functions.
*/
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))

/* ROTATE_LEFT rotates x left n bits.
*/
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

/* FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4.
Rotation is separate from addition to prevent recomputation.
*/
#define FF(a, b, c, d, x, s, ac) { \
    (a) += F ((b), (c), (d)) + (x) + (unsigned long int)(ac); \
    (a) = ROTATE_LEFT ((a), (s)); \
    (a) += (b); \
    }
#define GG(a, b, c, d, x, s, ac) { \
    (a) += G ((b), (c), (d)) + (x) + (unsigned long int)(ac); \
    (a) = ROTATE_LEFT ((a), (s)); \
    (a) += (b); \
    }
#define HH(a, b, c, d, x, s, ac) { \
    (a) += H ((b), (c), (d)) + (x) + (unsigned long int)(ac); \
    (a) = ROTATE_LEFT ((a), (s)); \
    (a) += (b); \
    }
#define II(a, b, c, d, x, s, ac) { \
    (a) += I ((b), (c), (d)) + (x) + (unsigned long int)(ac); \
    (a) = ROTATE_LEFT ((a), (s)); \
    (a) += (b); \
    }


/* MD5 initialization. Begins an MD5 operation, writing a new context.
*/

MD5_CTX::MD5_CTX()
{
    MD5Init ();
}

MD5_CTX::~MD5_CTX()
{
}

void MD5_CTX::MD5Init ()
{
    this->count[0] = this->count[1] = 0;
    /* Load magic initialization constants.*/
    this->state[0] = 0x67452301;
    this->state[1] = 0xefcdab89;
    this->state[2] = 0x98badcfe;
    this->state[3] = 0x10325476;
    /* Add by Liguangyi */
    MD5_memset(PADDING, 0, sizeof(PADDING));
    *PADDING=0x80;
    //PADDING = {
    //    0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    //    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    //    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0    };
}

/* MD5 block update operation. Continues an MD5 message-digest
operation, processing another message block, and updating the
context.
*/
void MD5_CTX::MD5Update (unsigned char *input,unsigned int inputLen)
{
    unsigned int i, index, partLen;

    /* Compute number of bytes mod 64 */
    index = (unsigned int)((this->count[0] >> 3) & 0x3F);

    /* Update number of bits */
    if ((this->count[0] += ((unsigned long int)inputLen << 3))
        < ((unsigned long int)inputLen << 3))
        this->count[1]++;
    this->count[1] += ((unsigned long int)inputLen >> 29);

    partLen = 64 - index;

    /* Transform as many times as possible.
    */
    if (inputLen >= partLen) {
        MD5_memcpy((unsigned char*)&this->buffer[index],
            (unsigned char*)input, partLen);
        MD5Transform (this->state, this->buffer);

        for (i = partLen; i + 63 < inputLen; i += 64)
            MD5Transform (this->state, &input[i]);

        index = 0;
    }
    else
        i = 0;

    /* Buffer remaining input */
    MD5_memcpy ((unsigned char*)&this->buffer[index], (unsigned char*)&input[i], inputLen-i);
}

/* MD5 finalization. Ends an MD5 message-digest operation, writing the
the message digest and zeroizing the context.
*/
void MD5_CTX::MD5Final (unsigned char digest[16])
{
    unsigned char bits[8];
    unsigned int index, padLen;

    /* Save number of bits */
    Encode (bits, this->count, 8);

    /* Pad out to 56 mod 64.
    */
    index = (unsigned int)((this->count[0] >> 3) & 0x3f);
    padLen = (index < 56) ? (56 - index) : (120 - index);
    MD5Update ( PADDING, padLen);

    /* Append length (before padding) */
    MD5Update (bits, 8);
    /* save state in digest */
    Encode (digest, this->state, 16);

    /* Zeroize sensitive information.
    */
    MD5_memset ((unsigned char*)this, 0, sizeof (*this));
    this->MD5Init();
}

/* MD5 basic transformation. Transforms state based on block.
*/
void MD5_CTX::MD5Transform (unsigned long int state[4], unsigned char block[64])
{
    unsigned long int a = state[0], b = state[1], c = state[2], d = state[3], x[16];

    Decode (x, block, 64);

    /* Round 1 */
    FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
    FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
    FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
    FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
    FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
    FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
    FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
    FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
    FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
    FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
    FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
    FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
    FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
    FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
    FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
    FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

    /* Round 2 */
    GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
    GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
    GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
    GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
    GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
    GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
    GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
    GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
    GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
    GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
    GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
    GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
    GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
    GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
    GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
    GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

    /* Round 3 */
    HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
    HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
    HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
    HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
    HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
    HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
    HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
    HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
    HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
    HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
    HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
    HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
    HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
    HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
    HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
    HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

    /* Round 4 */
    II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
    II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
    II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
    II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
    II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
    II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
    II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
    II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
    II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
    II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
    II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
    II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
    II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
    II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
    II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
    II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;

    /* Zeroize sensitive information.
    */
    MD5_memset ((unsigned char*)x, 0, sizeof (x));
}

/* Encodes input (unsigned long int) into output (unsigned char). Assumes len is
a multiple of 4.
*/
void MD5_CTX::Encode (unsigned char *output, unsigned long int *input,unsigned int len)
{
    unsigned int i, j;

    for (i = 0, j = 0; j < len; i++, j += 4) {
        output[j] = (unsigned char)(input[i] & 0xff);
        output[j+1] = (unsigned char)((input[i] >> 8) & 0xff);
        output[j+2] = (unsigned char)((input[i] >> 16) & 0xff);
        output[j+3] = (unsigned char)((input[i] >> 24) & 0xff);
    }
}

/* Decodes input (unsigned char) into output (unsigned long int). Assumes len is
a multiple of 4.
*/
void MD5_CTX::Decode (unsigned long int *output, unsigned char *input, unsigned int len)
{
    unsigned int i, j;

    for (i = 0, j = 0; j < len; i++, j += 4)
        output[i] = ((unsigned long int)input[j]) | (((unsigned long int)input[j+1]) << 8) |
        (((unsigned long int)input[j+2]) << 16) | (((unsigned long int)input[j+3]) << 24);
}

/* Note: Replace "for loop" with standard memcpy if possible.
*/

void MD5_CTX::MD5_memcpy (unsigned char* output, unsigned char* input,unsigned int len)
{
    unsigned int i;

    for (i = 0; i < len; i++)
        output[i] = input[i];
}

/* Note: Replace "for loop" with standard memset if possible.
*/
void MD5_CTX::MD5_memset (unsigned char* output,int value,unsigned int len)
{
    unsigned int i;

    for (i = 0; i < len; i++)
        ((char *)output)[i] = (char)value;
}

using namespace CREEK;

void TestModule(void)
{
    CModule module;
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    module.Attach(TF("C:\\Windows\\system32\\ntdll.dll"));
#else
    module.Attach(TF("/usr/lib/librt.so"));
#endif
    DEV_INFO(TF("%#llX"), (Module)module);

    //MD5 ("") = d41d8cd98f00b204e9800998ecf8427e
    //MD5 ("a") = 0cc175b9c0f1b6a831c399e269772661
    //MD5 ("abc") = 900150983cd24fb0d6963f7d28e17f72
    //MD5 ("message digest") = f96b697d7cb7938d525a2f31aaf161d0
    //MD5 ("abcdefghijklmnopqrstuvwxyz") = c3fcd3d76192e4007dfb496cca67e13b
    //MD5 ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789") = d174ab98d277d9f5a5611c2c9f419d9f
    //CCString strAESKey = "WangZuKeJi_ARCH";
    //CBufReadStream bAESKey(strAESKey.Length(), (PByte)*strAESKey);
    //CString strAESRet;

    MD5_CTX md5_;

    //Byte bOut[16] ={0};
    CMD5::NUM Num;
    CSHA1::NUM Num3;

    Char szNull[] = "";
    Char sz1[] = "a";
    Char sz3[] = "abc";
    Char szLittle[] = "message digest";
    Char szLong[] = "abcdefghijklmnopqrstuvwxyz";
    Char szBig[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    CBufReadStream brs(CChar::Length(szNull), (PByte)szNull);
    CString strKey;
    CMD5::Crypto(brs, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), szNull, *strKey);

    md5_.MD5Update((PByte)szNull, 0);
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), szNull, *strKey);

    brs.Seek(0);
    CMD5::Crypto(brs, Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5n : %s"), szNull, *strKey);

    brs.Seek(0);
    CSHA1::Crypto(brs, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), szNull, *strKey);

    brs.Seek(0);
    CSHA1::Crypto(brs, Num3);
    CSHA1::ToString(Num3, strKey);
    DEV_INFO(TF("[%s]sha1nn : %s"), szNull, *strKey);

    CBufReadStream brs1(CChar::Length(sz1), (PByte)sz1);
    CMD5::Crypto(brs1, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), sz1, *strKey);

    md5_.MD5Update((PByte)sz1, (UInt)CChar::Length(sz1));
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), sz1, *strKey);

    brs1.Seek(0);
    CSHA1::Crypto(brs1, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), sz1, *strKey);

    CBufReadStream brs3(CChar::Length(sz3), (PByte)sz3);
    CMD5::Crypto(brs3, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), sz3, *strKey);

    md5_.MD5Update((PByte)sz3, (UInt)CChar::Length(sz3));
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), sz3, *strKey);

    brs3.Seek(0);
    CSHA1::Crypto(brs3, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), sz3, *strKey);

    CBufReadStream brsl(CChar::Length(szLittle), (PByte)szLittle);
    CMD5::Crypto(brsl, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), szLittle, *strKey);

    md5_.MD5Update((PByte)szLittle, (UInt)CChar::Length(szLittle));
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), szLittle, *strKey);

    brsl.Seek(0);
    CSHA1::Crypto(brsl, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), szLittle, *strKey);

    CBufReadStream brso(CChar::Length(szLong), (PByte)szLong);
    CMD5::Crypto(brso, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), szLong, *strKey);

    md5_.MD5Update((PByte)szLong, (UInt)CChar::Length(szLong));
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), szLong, *strKey);

    brso.Seek(0);
    CSHA1::Crypto(brso, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), szLong, *strKey);

    CBufReadStream brsb(CChar::Length(szBig), (PByte)szBig);
    CMD5::Crypto(brsb, strKey);
    DEV_INFO(TF("[%s]md5  : %s"), szBig, *strKey);

    md5_.MD5Update((PByte)szBig, (UInt)CChar::Length(szBig));
    md5_.MD5Final(Num);
    CMD5::ToString(Num, strKey);
    DEV_INFO(TF("[%s]md5_ : %s"), szBig, *strKey);

    brsb.Seek(0);
    CSHA1::Crypto(brsb, strKey);
    DEV_INFO(TF("[%s]sha1 : %s"), szBig, *strKey);


    //CBufReadStream  brsAES1(CChar::Length(sz1), (PByte)sz1);
    //CBufWriteStream bwsAES1(DEF::Align<size_t>(CChar::Length(sz1), CAES::AES_LEN_BLOCK));
    //CAES::Encode(bAESKey, brsAES1, bwsAES1);
    //bAESKey.Seek(0);
    //brsAES1.Seek(0);
    //CAES::Encode(bAESKey, brsAES1, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), sz1, *strAESKey, *strAESRet);
    //bAESKey.Seek(0);
    //CBufReadStream  brsAES11(bwsAES1.Size(), bwsAES1.GetBuf());
    //CAES::Decode(bAESKey, brsAES11, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), sz1, *strAESKey, *strAESRet);

    //CBufReadStream  brsAES2(CChar::Length(sz3), (PByte)sz3);
    //CBufWriteStream bwsAES2(DEF::Align<size_t>(CChar::Length(sz3), CAES::AES_LEN_BLOCK));
    //bAESKey.Seek(0);
    //CAES::Encode(bAESKey, brsAES2, bwsAES2);
    //bAESKey.Seek(0);
    //brsAES2.Seek(0);
    //CAES::Encode(bAESKey, brsAES2, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), sz3, *strAESKey, *strAESRet);
    //bAESKey.Seek(0);
    //CBufReadStream  brsAES22(bwsAES2.Size(), bwsAES2.GetBuf());
    //CAES::Decode(bAESKey, brsAES22, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), sz3, *strAESKey, *strAESRet);

    //CBufReadStream  brsAES3(CChar::Length(szLittle), (PByte)szLittle);
    //CBufWriteStream bwsAES3(DEF::Align<size_t>(CChar::Length(szLittle), CAES::AES_LEN_BLOCK));
    //bAESKey.Seek(0);
    //CAES::Encode(bAESKey, brsAES3, bwsAES3);
    //bAESKey.Seek(0);
    //brsAES3.Seek(0);
    //CAES::Encode(bAESKey, brsAES3, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szLittle, *strAESKey, *strAESRet);
    //bAESKey.Seek(0);
    //CBufReadStream  brsAES33(bwsAES3.Size(), bwsAES3.GetBuf());
    //CAES::Decode(bAESKey, brsAES33, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szLittle, *strAESKey, *strAESRet);

    //CBufReadStream  brsAES4(CChar::Length(szLong), (PByte)szLong);
    //CBufWriteStream bwsAES4(DEF::Align<size_t>(CChar::Length(szLong), CAES::AES_LEN_BLOCK));
    //bAESKey.Seek(0);
    //CAES::Encode(bAESKey, brsAES4, bwsAES4);
    //bAESKey.Seek(0);
    //brsAES4.Seek(0);
    //CAES::Encode(bAESKey, brsAES4, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szLong, *strAESKey, *strAESRet);
    //bAESKey.Seek(0);
    //CBufReadStream  brsAES44(bwsAES4.Size(), bwsAES4.GetBuf());
    //CAES::Decode(bAESKey, brsAES44, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szLong, *strAESKey, *strAESRet);

    //CBufReadStream  brsAES5(CChar::Length(szBig), (PByte)szBig);
    //CBufWriteStream bwsAES5(DEF::Align<size_t>(CChar::Length(szBig), CAES::AES_LEN_BLOCK));
    //bAESKey.Seek(0);
    //CAES::Encode(bAESKey, brsAES5, bwsAES5);
    //bAESKey.Seek(0);
    //brsAES5.Seek(0);
    //CAES::Encode(bAESKey, brsAES5, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szBig, *strAESKey, *strAESRet);
    //bAESKey.Seek(0);
    //CBufReadStream  brsAES55(bwsAES5.Size(), bwsAES5.GetBuf());
    //CAES::Decode(bAESKey, brsAES55, strAESRet);
    //DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szBig, *strAESKey, *strAESRet);


    //CCString strcTest = "http://www.xiami.com/play?ids=/song/playlist/id/34382178/type/3#open";
    //CCString strcTest1 = "aHR0cDovL3d3dy54aWFtaS5jb20vcGxheT9pZHM9L3NvbmcvcGxheWxpc3QvaWQvMzQzODIxNzgvdHlwZS8zI29wZW4=";
    //CCString strcOut;
    //CCString strcOut2;
    //CBase64::Encode(strcOut, strcTest);
    //CBase64::Decode(strcOut2, strcTest1);

    //bool b1 = (strcOut == strcTest1);
    //bool b2 = (strcOut2 == strcTest);

    //CFileReadStream frs;
    //if (frs.Create(TF("F:\\WoptiSetup.zip")))
    //{
    //    CString strOut;

    //    CMD5::Crypto(frs, strOut);
    //    DEV_INFO(TF("md5 : %s"), *strOut);
    //}
    CProcessHandle proc;
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    if (proc.Open(TF("C:\\Windows\\notepad.exe")))
    {
        if (proc.Check())
        {
            DEV_INFO(TF("proc is running ppid = %x, gpid = %x"), CProcessHandle::GetParentId(), CProcessHandle::GetGroupId());
        }
        else
        {
            DEV_INFO(TF("proc ???"));
        }
        proc.Wait((UInt)TIMET_INFINITE);
    }
#else
    if (proc.Open(TF("/bin/top"))) // /bin/ls
    {
        if (proc.Check())
        {
            DEV_INFO(TF("proc is running ppid = %x, gpid = %x"), CProcessHandle::GetParentId(), CProcessHandle::GetGroupId());
        }
        else
        {
            DEV_INFO(TF("proc ???"));
        }
        Int nRet = proc.Wait();
        DEV_INFO(TF("proc wait ret = %x"), nRet);
    }
#endif
}
