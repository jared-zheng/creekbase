#include "stdafx.h"
#include "tstring.h"
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
#include "windows\combase.h"
#endif
#include "container.h"
#include "refcount.h"
#include "testtstring.h"

using namespace CREEK;

const XChar* const _tStr = TF("123456isf；加工费ijsjd才vxmcvv1.，木质线材");

class CTTT : public MObject
{
public:
    CTTT(Int nT = 0) : m_nT(nT) { DEV_INFO(TF("###########CTTT::CTTT")); }
    ~CTTT(void) { DEV_INFO(TF("###########CTTT::~CTTT")); }
    CTTT(const CTTT& aSrc) : m_nT(aSrc.m_nT) { DEV_INFO(TF("###########CTTT::CTTTCopy")); }
    CTTT& operator=(const CTTT& aSrc)
    {
        m_nT = aSrc.m_nT;
        DEV_INFO(TF("###########CTTT::CTTT Operator"));
        return *this;
    }
public:
    Int m_nT;
};

typedef CTScopePtr<CTTT>   CTTTPtr;

void TestTString(void)
{
    CStringRef sref(_tStr);
    DEV_INFO(TF("ref string [%s] length is %d"), *sref, sref.Length());

    size_t stIndex = 0;
    Long l   = sref.ToLong(stIndex);
    stIndex = 0;
    ULong ul = sref.ToULong(stIndex);
    stIndex = 0;
    LLong ll = sref.ToLLong(stIndex);
    stIndex = 0;
    ULLong ull = sref.ToULLong(stIndex);
    stIndex = 0;
    Double d = sref.ToDouble(stIndex);
    DEV_INFO(TF("ref string [%s] length is %d, %d-%d"), *sref, sref.Length(), l, stIndex);

    CStringFix sfix(sref);
    DEV_INFO(TF("1fix string [%s] length is %d"), *sfix, sfix.Length());

    typedef CTStringFix<CXChar, (size_t)360> CStringFix360;
    CStringFix360 str360 = sfix;

    sfix += TF("阿隆索爹无法超大附件qwe，。/");
    DEV_INFO(TF("2fix string [%s] length is %d"), *sfix, sfix.Length());

    CStringFix sfix2(sfix);
    sfix2 += sref;
    sfix2 += sfix;
    DEV_INFO(TF("3fix string [%s] length is %d"), *sfix2, sfix2.Length());

    bool bRet1 = (sfix2 <= _tStr);
    bool bRet2 = (sfix2 <  _tStr);
    bool bRet3 = (sfix2 >= _tStr);
    bool bRet4 = (sfix2 > _tStr);
    bool bRet5 = (sfix2 == _tStr);
    bool bRet6 = (sfix2 != _tStr);
    DEV_INFO(TF("4fix string [%s] compare %s : %d, %d, %d, %d, %d, %d"), *sfix2, _tStr, bRet1, bRet2, bRet3, bRet4, bRet5, bRet6);

    bRet1 = (sfix2 <= sfix);
    bRet2 = (sfix2 <  sfix);
    bRet3 = (sfix2 >= sfix);
    bRet4 = (sfix2 > sfix);
    bRet5 = (sfix2 == sfix);
    bRet6 = (sfix2 != sfix);
    DEV_INFO(TF("5fix string [%s] compare %s : %d, %d, %d, %d, %d, %d"), *sfix2, *sfix, bRet1, bRet2, bRet3, bRet4, bRet5, bRet6);

    sfix2.SetAt(3, '@');
    DEV_INFO(TF("6fix string [%s] length is %d"), *sfix2, sfix2.Length());

    Int nIndex1 = sfix2.Find('@');
    Int nIndex2 = sfix2.Find(TF("阿隆索"));
    DEV_INFO(TF("7fix string [%s] length is %d, @ at %d, 阿隆索 at %d"), *sfix2, sfix2.Length(), nIndex1, nIndex2);

    sfix2.Delete(7, 12);
    sfix2.Insert(5, '$');
    sfix2.Insert(78, TF("===<<<ABCGSDKLSDLSDSD:LS:LDKFDKJFLKJDKJF>>>==="));
    sfix2.Replace('s', '#');
    DEV_INFO(TF("8fix string [%s] length is %d"), *sfix2, sfix2.Length());

    sfix2.Replace(TF("工费"), TF("+++斯蒂芬森地方+++"));
    DEV_INFO(TF("9fix string [%s] length is %d"), *sfix2, sfix2.Length());

    sfix2.Remove('+');
    DEV_INFO(TF("10fix string [%s] length is %d"), *sfix2, sfix2.Length());

    sfix2.Upper();
    DEV_INFO(TF("11fix string [%s] length is %d"), *sfix2, sfix2.Length());

    sfix2.Lower();
    DEV_INFO(TF("12fix string [%s] length is %d"), *sfix2, sfix2.Length());

    sfix2.Reverse();
    DEV_INFO(TF("13fix string [%s] length is %d"), *sfix2, sfix2.Length());

    CStringFix sfix3(TF("======"));
    sfix3.AppendBuffer(_tStr);
    sfix3 += TF("======");

    CStringFix sfix4 = sfix3;

    DEV_INFO(TF("14fix string [%s] length is %d"), *sfix3, sfix3.Length());
    sfix3.TrimLeft('=');
    DEV_INFO(TF("15fix string [%s] length is %d"), *sfix3, sfix3.Length());
    sfix3.TrimRight('=');
    DEV_INFO(TF("16fix string [%s] length is %d"), *sfix3, sfix3.Length());

    sfix4.Trim('=');
    DEV_INFO(TF("17fix string [%s] length is %d"), *sfix4, sfix4.Length());

    CStringFix sfix5 = sfix4.Left(5);
    DEV_INFO(TF("18fix string [%s] length is %d"), *sfix5, sfix5.Length());

    CStringFix sfix6 = sfix4.Right(8);
    DEV_INFO(TF("19fix string [%s] length is %d"), *sfix6, sfix6.Length());

    CStringFix sfix7 = sfix4.Mid(6, 9);
    DEV_INFO(TF("20fix string [%s] length is %d"), *sfix7, sfix7.Length());

    CStringFix sfix8;
    sfix8.ToString(0x123456789LL, 16);
    DEV_INFO(TF("21fix string [%s] length is %d"), *sfix8, sfix8.Length());

    sfix8 = TF("hskdhf987654321FF");
    stIndex = 5;
    l = sfix8.ToLong(stIndex, 16);
    stIndex = 5;
    ul = sfix8.ToULong(stIndex, 16);
    stIndex = 5;
    ll = sfix8.ToLLong(stIndex, 16);
    stIndex = 5;
    ull = sfix8.ToULLong(stIndex, 16);
    stIndex = 5;
    d = sfix8.ToDouble(stIndex);
    DEV_INFO(TF("22fix string [%s] length is %d, %llX---%lld"), *sfix8, sfix8.Length(), ll, stIndex);

    CString sstr9;
    sstr9 = sfix8;
    sstr9 += TF('\n');
    sstr9.ToString(0x123456789LL, 16);
    DEV_INFO(TF("23 string [%s] length is %d"), *sstr9, sstr9.Length());

    CTArray<Int> AryInt;
    AryInt.Add(100, false);
    for (Int i = 0; i < AryInt.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%d"), i, AryInt[i]);
    }
    DEV_INFO(TF("&&&&&&&&&&&&&String Array alloc size=%d, size=%d, grow=%d"), AryInt.GetAllocSize(), AryInt.GetSize(), AryInt.GetGrow());
    //// StringFix container test
    CTArray<CStringFix> AryStr;
    //AryStr.Add(100, false);
    //for (Int i = 0; i < AryStr.GetSize(); ++i)
    //{
    //    DEV_INFO(TF("%d-->%s"), i, AryStr[i].GetBuffer());
    //}
    //DEV_INFO(TF("&&&&&&&&&&&&&String Array alloc size=%d, size=%d, grow=%d"), AryStr.GetAllocSize(), AryStr.GetSize(), AryStr.GetGrow());

    AryStr.Add(CStringFix(_tStr));
    AryStr.Add(CStringFix(TF("看到手机放到沙发的撒娇啊士大夫,.大发光火")));
    AryStr.Add(CStringFix(TF("ewteryry感觉共同的空间;.哦i;")));
    AryStr.Add(CStringFix(TF("vc宁波新闻太热鱼好极了空间ADS ")));

    DEV_INFO(TF("String Array alloc size=%d, size=%d, grow=%d"), AryStr.GetAllocSize(), AryStr.GetSize(), AryStr.GetGrow());

    for (Int i = 0; i < AryStr.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%s"), i, AryStr[i].GetBuffer());
    }

    Int nIndex = AryStr.Find(CStringFix(TF("ewteryry感觉共同的空间;.哦i;")));
    if (nIndex != -1)
    {
        DEV_INFO(TF("Find %d-->%s"), nIndex, AryStr[nIndex].GetBuffer());
        AryStr.RemoveAt(nIndex);
    }

    for (Int i = 0; i < AryStr.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%s"), i, AryStr[i].GetBuffer());
    }
#if _MSC_VER >= 1800
    CTArray<CStringFix> aasss(std::move(AryStr));
    DEV_INFO(TF("==========================%d"), AryStr.GetSize());
    for (Int i = 0; i < AryStr.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%s"), i, AryStr[i].GetBuffer());
    }
    DEV_INFO(TF("==========================%d"), aasss.GetSize());
    for (Int i = 0; i < aasss.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%s"), i, aasss[i].GetBuffer());
    }

    CTArray<CString> aaaXXX;
    aaaXXX.Add(std::move(sstr9));

    CString strMMMM444 = TF("看到手机放到沙发的撒娇啊士大夫,.大发光火");
    CTList<CString> llVVB;
    llVVB.AddHead(std::move(strMMMM444));
#endif
    CTMap<CStringFix, Long> MapStr;
    MapStr.Add(CStringFix(_tStr), 123);
    MapStr.Add(CStringFix(TF("看到手机放到沙发的撒娇啊士大夫,.大发光火")), 345);
    MapStr.Add(CStringFix(TF("ewteryry感觉共同的空间;.哦i;")), 567);
    MapStr.Add(CStringFix(TF("vc宁波新闻太热鱼好极了空间ADS ")), 789);

    DEV_INFO(TF("String Map alloc size=%d, size=%d, grow=%d"), MapStr.GetAllocSize(), MapStr.GetSize(), MapStr.GetGrow());

    PINDEX index = MapStr.GetFirstIndex();
    while (index != nullptr)
    {
        const CTMap<CStringFix, Long>::PAIR* pair = MapStr.GetNext(index);
        DEV_INFO(TF("%d-->%s"), pair->m_V, pair->m_K.GetBuffer());
    }

    index = MapStr.FindIndex(CStringFix(TF("看到手机放到沙发的撒娇啊士大夫,.大发光火")));
    if (index != nullptr)
    {
        const CTMap<CStringFix, Long>::PAIR* pair = MapStr.GetAt(index);
        DEV_INFO(TF("Find %d-->%s"), pair->m_V, pair->m_K.GetBuffer());
        MapStr.RemoveAt(index);
    }

    index = MapStr.GetFirstIndex();
    while (index != nullptr)
    {
        const CTMap<CStringFix, Long>::PAIR* pair = MapStr.GetNext(index);
        DEV_INFO(TF("%d-->%s"), pair->m_V, pair->m_K.GetBuffer());
    }

#if _MSC_VER >= 1800
    CTMap<CStringFix, Long> MapStr2 = std::move(MapStr);
    DEV_INFO(TF("*************String Map alloc size=%d, size=%d, grow=%d"), MapStr.GetAllocSize(), MapStr.GetSize(), MapStr.GetGrow());
    DEV_INFO(TF("(*************String Map alloc size=%d, size=%d, grow=%d"), MapStr2.GetAllocSize(), MapStr2.GetSize(), MapStr2.GetGrow());
    index = MapStr2.GetFirstIndex();
    while (index != nullptr)
    {
        const CTMap<CStringFix, Long>::PAIR* pair = MapStr2.GetNext(index);
        DEV_INFO(TF("%d-->%s"), pair->m_V, pair->m_K.GetBuffer());
    }

    CString strMMMM = TF("看到手机放到沙发的撒娇啊士大夫,.大发光火");
    CTMap<CString, Long> MapStr3;
    MapStr3.Add(std::move(strMMMM), 12);
#endif
    // String
    CString str0;
    DEV_INFO(TF("0 string [%s] length is %d"), *str0, str0.Length());

    CString str1(_tStr);
    DEV_INFO(TF("1 string [%s] length is %d"), *str1, str1.Length());

    CString str2(str1);
    DEV_INFO(TF("2 string [%s] length is %d"), *str2, str2.Length());

    CString str3(sref);
    DEV_INFO(TF("3 string [%s] length is %d"), *str3, str3.Length());

    str0 = _tStr;
    DEV_INFO(TF("4 string [%s] length is %d"), *str0, str0.Length());

    str1 = sfix2;
    DEV_INFO(TF("5 string [%s] length is %d"), *str1, str1.Length());

    str2 = str1;
    DEV_INFO(TF("6 string [%s] length is %d"), *str2, str2.Length());

    CStringRef refx1(*str2);
    str3 = refx1;
    DEV_INFO(TF("7 string [%s] length is %d"), *str3, str3.Length());

    str0 += _tStr;
    DEV_INFO(TF("8 string [%s] length is %d"), *str0, str0.Length());

    str1 += sfix2;
    DEV_INFO(TF("9 string [%s] length is %d"), *str1, str1.Length());

    str2 += str1;
    DEV_INFO(TF("10 string [%s] length is %d"), *str2, str2.Length());

    CStringRef refx2(*str2);
    str3 += refx2;
    DEV_INFO(TF("11 string [%s] length is %d"), *str3, str3.Length());

    CString str4 = str0 + _tStr;
    DEV_INFO(TF("12 string [%s] length is %d"), *str4, str4.Length());

    CString str5 = str1 + sfix2;
    DEV_INFO(TF("13 string [%s] length is %d"), *str5, str5.Length());

    CString str6 = str2 + str1;
    DEV_INFO(TF("14 string [%s] length is %d"), *str6, str6.Length());

    CStringRef refx3(*str2);
    CString str7 = str3 + refx3;
    DEV_INFO(TF("15 string [%s] length is %d"), *str7, str7.Length());


    bRet1 = (str7 <= _tStr);
    bRet2 = (str7 <  _tStr);
    bRet3 = (str7 >= _tStr);
    bRet4 = (str7 > _tStr);
    bRet5 = (str7 == _tStr);
    bRet6 = (str7 != _tStr);
    DEV_INFO(TF("16 string [%s] compare %s : %d, %d, %d, %d, %d, %d"), *str7, _tStr, bRet1, bRet2, bRet3, bRet4, bRet5, bRet6);

    bRet1 = (str7 <= str6);
    bRet2 = (str7 <  str6);
    bRet3 = (str7 >= str6);
    bRet4 = (str7 > str6);
    bRet5 = (str7 == str6);
    bRet6 = (str7 != str6);
    DEV_INFO(TF("17 string [%s] compare %s : %d, %d, %d, %d, %d, %d"), *str7, *str6, bRet1, bRet2, bRet3, bRet4, bRet5, bRet6);

    XChar xc = str7[5];
    XChar xx = str7.GetAt(5);
    DEV_INFO(TF("18 string [%s] 5th is %c[%c]"), *str7, xc, xx);

    str7.SetAt(5, '#');
    XChar* p1 = *str7;
    const XChar* p2 = *str7;
    const XChar* p3 = str7.GetBuffer();
    DEV_INFO(TF("19 string [%s][%s][%s][%s]"), *str7, p1, p2, p3);

    str7.FillBuffer(TF("看电视了立法的手机发就发啦上的纠纷 地方"));
    str7.AppendBuffer(TF("=====KDSJFLKDSF=========+++++++++++++++++++"));
    str7.AppendBuffer('$');
    DEV_INFO(TF("20 string [%s] length is %d"), *str7, str7.Length());

    str6.SetBufferLength(123);
    DEV_INFO(TF("21 string [%s] length is %d, buffer length %d"), *str6, str6.Length(), str6.BufferLength());

    bool bRet = str7.IsEmpty();
    //str7.Empty();
    DEV_INFO(TF("22 string [%s] length is %d[%d]"), *str7, str7.Length(), bRet);

    Int nIndex3 = str7.Find('L', 6);
    Int nIndex4 = str7.Find(TF("电视了立法的手机发"), 1);
    DEV_INFO(TF("23 string [%s] L at %d, 电视了立法的手机发 at %d"), *str7, nIndex3, nIndex4);

    Int nIndex5 = str7.Cmp(TF("看电视了立法的手机发就发啦上的纠纷 地方=====KdSJfLKDsF=========+++++++++++++++++++$"));
    Int nIndex6 = str7.Cmpi(TF("看电视了立法的手机发就发啦上的纠纷 地方=====KdSJfLKDsF=========+++++++++++++++++++$"));
    DEV_INFO(TF("24 string [%s] compare is %d, %d"), *str7, nIndex5, nIndex6);

    str7.Delete(5, 8);
    DEV_INFO(TF("25 string [%s] length is %d"), *str7, str7.Length());

    str7.Insert(8, '@');
    DEV_INFO(TF("26 string [%s] length is %d"), *str7, str7.Length());

    str7.Insert(3, TF("*****************************************"));
    DEV_INFO(TF("27 string [%s] length is %d"), *str7, str7.Length());

    str7.Replace('*', '^');
    DEV_INFO(TF("28 string [%s] length is %d"), *str7, str7.Length());

    str7.Replace(TF("纠@纷 地"), TF("数控刀具反抗都是浪费"));
    DEV_INFO(TF("29 string [%s] length is %d"), *str7, str7.Length());

    str7.Remove('^');
    str7.Remove('=');
    str7.Remove('+');
    DEV_INFO(TF("30 string [%s] length is %d"), *str7, str7.Length());

    str7.Shrink();

    str5.Format(TF("了;广发卡;领导反馈给第三方士大夫结果;了多少咖啡馆了多少风格是咖啡蛋糕%d肯定是减肥%s"), 23948923, TF("深刻的房价肯定舒服"));
    DEV_INFO(TF("31 string [%s] length is %d"), *str5, str5.Length());

    str5.AppendFormat(TF("KLJFDSLKDSJ%sKJSFJDSFJDSHF%d"), TF("ui儿哦iio"), 345435);
    DEV_INFO(TF("32 string [%s] length is %d"), *str5, str5.Length());

    str5.Upper();
    DEV_INFO(TF("33 string [%s] length is %d"), *str5, str5.Length());

    str5.Lower();
    DEV_INFO(TF("34 string [%s] length is %d"), *str5, str5.Length());

    str5.Reverse();
    DEV_INFO(TF("35 string [%s] length is %d"), *str5, str5.Length());

    str3 = TF("==========看电视了立法的手机发就发啦上的纠纷 地方$$$$$$$KdSJfLKDsF+++++++++++++++++++=========");
    str4 = str3;

    str3.TrimLeft('=');
    str3.TrimRight('=');
    DEV_INFO(TF("36 string [%s] length is %d"), *str3, str3.Length());

    str4.Trim('=');
    DEV_INFO(TF("37 string [%s] length is %d"), *str4, str4.Length());

    str2 = TF("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    CString strx1 = str2.Left(7);
    CString strx2 = str2.Right(7);
    CString strx3 = str2.Mid(7, 9);
    DEV_INFO(TF("38 string [%s][%s][%s]"), *strx1, *strx2, *strx3);
#if _MSC_VER >= 1800
    CString strxxx = std::move(strx3);
    DEV_INFO(TF("39 string [%s][%s]"), *strxxx, *strx3);

    strxxx = TF("");
    strx3  = nullptr;
    DEV_INFO(TF("40 string [%s][%s]"), *strxxx, *strx3);
#endif
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    CComStr olestr = *strx1;
    olestr += *strx2;
    olestr.Upper();
    olestr.Lower();
    olestr.Reverse();
    olestr.Trim('-');
#endif

    CTComPtr<IUnknown> comptr;

    CComVar   var;
#endif

    CTMap<Int, void*> mmppp;
    for (Int i = 0; i < 100; ++i)
    {
        mmppp.Add(i);
    }
    index = mmppp.GetFirstIndex();
    while (index != nullptr)
    {
        CTMap<Int, void*>::PAIR* ppp = mmppp.GetNext(index);
        DEV_INFO(TF("*******************%d-->%d"), ppp->m_K, ppp->m_V);
    }

    CTMap<Int, CTTTPtr> mmttt;
    for (Int i = 0; i < 100; ++i)
    {
        mmttt.Add(i);
    }
    index = mmttt.GetFirstIndex();
    while (index != nullptr)
    {
        CTMap<Int, CTTTPtr>::PAIR* ttt = mmttt.GetNext(index);
        ttt->m_V = MNEW CTTT;
        DEV_INFO(TF("@@@@@@@@@@@@@@@@@@%d-->%d"), ttt->m_K, ttt->m_V->m_nT);

        ttt->m_V->m_nT = ttt->m_K;
        DEV_INFO(TF("$$$$$$$$$$$$$$$$$%d-->%d"), ttt->m_K, ttt->m_V->m_nT);
    }
}
