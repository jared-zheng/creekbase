#include "stdafx.h"
#include "sync.h"
#include "testsync.h"

using namespace CREEK;

void TestSync(void)
{
    CSyncCounter Counter;
    CSyncCounterScope scope1(Counter);
    DEV_INFO(TF("counter = %d"), Counter.GetCount());

    CSyncLock  Lock;
    CSyncLockScope scope2(Lock);

    CSyncMutex Mutex;
    CSyncMutexScope scope3(Mutex);

    CSyncEvent Event;
    CSyncEventScope scope4(Event);

    CSyncSema SEmaphore;
    CSyncSemaScope scope5(SEmaphore);

    CRWLock RWLock;
    {
        CRWLockReadScope scope6(RWLock);
    }
    {
        CRWLockWriteScope scope7(RWLock);
    }
}
