#include "stdafx.h"
#include "testplatform.h"
#include "testhash.h"
#include "testmem.h"
#include "testsync.h"
#include "testcommon.h"
#include "testtstring.h"
#include "testexception.h"
#include "testrefcount.h"
#include "testmodule.h"
#include "testuuid.h"
#include "testfilepath.h"
#include "testthread.h"
#include "testevent.h"
#include "testrtti.h"
#include "testhttp.h"

#include "filelog.h"
#include "subsystem.h"
#include "rand.h"
#include "test.h"

#include "streamfile.h"
#include "timescope.h"

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

//#include "..\..\external\xar\xar.h"
//#ifdef __RUNTIME_DEBUG__
//    #if (__ARCH_TARGET__ == ARCH_TARGET_64)
//        #ifdef LIB_RUNTIME
//            #pragma comment(lib, "xarlibDebug_x64_v142_MultiByte.lib")
//        #else
//            #pragma comment(lib, "xarDebug_x64_v142_MultiByte.lib")
//        #endif
//    #endif
//#endif

#include "windows\ini.h"
#include "windows\fileversion.h"
#include "windows\reg.h"

#pragma comment(lib, "version.lib")

#pragma comment(lib, "rpcrt4.lib")

#else

#define _tmain   main

#endif

using namespace CREEK;

Int _tmain(void)
{
    //DEV_INFO(TF("%s"), "$(PlatformTarget)");
    //srand(::GetTickCount());

    //std::vector<int, CTMAlloc<int>> vi;
    //std::list<short, CTMAlloc<short>> ls;
    //std::map<int, double, std::less<int>, CTMAlloc<std::pair<int, double>>> md;

    //double d1 = 1.0;

    //for (int i = 0; i < 100; ++i)
    //{
    //    vi.push_back(i);
    //    ls.push_back((short)i);
    //    md.insert(std::make_pair(i, i * d1));
    //}

    //TestException();
    TestPlatform(); // hardware and platform information
    // TestHash();     // crc32 & crc64 hash support
//    TestMem();      // memory manager
//
//    TestSync();
    //TestCommon();   // container
//    TestTString();
//
//    TestRefCount();   // refcount & random
    //TestModule();
 //   TestUUID();
    //TestFilePath(); // file path

    //TestHttp();
    XChar szBuffer[1024];
    CPlatform::GetFullCmd(szBuffer, 1024);
    DEV_ERROR(szBuffer);

    CPlatform::GetPathInfo(szBuffer, 1024, true);
    DEV_ERROR(szBuffer);

    CPlatform::GetPathInfo(szBuffer, 1024);
    DEV_ERROR(szBuffer);

    //CPlatform::CPUSTAT cs1, cs2;
    //for (Int i = 0; i < 10; ++i)
    //{
    //    CPlatform::GetCPUStat(cs1);
    //    DEV_ERROR(TF("10. %llu,%llu"), cs1.ullUsed, cs1.ullTotal);
    //    CPlatform::SleepEx(1000);

    //    CPlatform::GetCPUStat(cs2);
    //    DEV_ERROR(TF("11. %llu,%llu"), cs2.ullUsed, cs2.ullTotal);

    //    UInt uRate = CPlatform::GetCPURate(cs1, cs2);
    //    UInt u1 = (uRate / 100);
    //    UInt u2 = (uRate % 100);

    //    uRate = 10000 - uRate;
    //    UInt u3 = (uRate / 100);
    //    UInt u4 = (uRate % 100);
    //    DEV_ERROR(TF("Rate = %d.%d   Idle = %d.%d"), u1, u2, u3, u4);
    //}

    CoreInit();
    ////TestHash();
    //TestThread(); // thread pool
    //TestEvent();  // event manager
    TestRTTI(); // object manager & rtti
    CoreExit();

    //CFileLogPipe pipeLog;

    //CTime time;
    //for (Int i = 0; i < 10; ++i)
    //{
    //    CString strTime = time.Format(TF("!!!%Y-%m-%d %H:%M:%S!!!"));
    //    pipeLog.Log(LOGL_INFO, *strTime);
    //    strTime += TF("\r\n");
    //    pipeLog.Put(LOGL_INFO, *strTime);
    //    CPlatform::SleepEx(5000);
    //}

    //getchar();

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    //CIni ini(TF("D:\\boot.ini"));

    //XChar szTmp[64] = { 0 };
    //CReg reg;
    //if (reg.Open(HKEY_LOCAL_MACHINE, TF("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), KEY_READ) == ERROR_SUCCESS)
    //{
    //    ULong ulLen = 64;
    //    if (reg.QueryStringValue(TF("ProductName"), szTmp, &ulLen) == ERROR_SUCCESS)
    //    {
    //        ini.CreateSetion(TF("Product"));
    //        ini.SetKeyValue(TF("Product"), TF("Name"), szTmp);
    //    }
    //}

    //CFileVersion fv;
    //fv.GetFileVersion(TF("x64\\Bin\\coreD.dll"));

    //ULong uM1, uM2;
    //fv.GetFileVersion(uM1, uM2);
    //DEV_INFO(TF(" file version %X---%X"), uM1, uM2);
    //fv.GetProductVersion(uM1, uM2);
    //DEV_INFO(TF(" product version %X---%X"), uM1, uM2);
#endif

   ////// LOG_DUMP(log_, TF("hello"));
   // PByte bBuf = (PByte)ALLOC( 8 * 1024 * 1024 );
   // PByte bOut = (PByte)ALLOC( 16 * 1024 * 1024 );
   // CFileReadStream frs;
   // if (frs.Create(TF("E:\\aaa.pdf")))
   // {
   //      CFileWriteStream fws;
   //     if (fws.Create(TF("E:\\aaa.zzz"), FILEF_NEW_ALWAYS))
   //     {
   //         void* p = EncodeInit();

   //         size_t tttlen = frs.Read(bBuf, 8 * 1024 * 1024);
   //         size_t ooolen = 16 * 1024 * 1024;
   //         while (tttlen > 0)
   //         {
   //             ooolen = 16 * 1024 * 1024;
   //             int nRet = EncodeData(p, bBuf, tttlen, bOut, ooolen, frs.IsEnd());
   //             if (ooolen != 16 * 1024 * 1024)
   //             {
   //                 fws.Write(bOut, (16 * 1024 * 1024 - ooolen));
   //             }
   //             if (nRet == 0)
   //             {
   //                 EncodeExit(p);
   //                 break;
   //             }
   //             tttlen = frs.Read(bBuf, 8 * 1024 * 1024);
   //         }
   //     }
   // }

   // CFileReadStream frs2;
   // if (frs2.Create(TF("E:\\aaa.zzz")))
   // {
   //      CFileWriteStream fws;
   //     if (fws.Create(TF("E:\\aaa.uncomp"), FILEF_NEW_ALWAYS))
   //     {
   //         void* p = DecodeInit();
   //         size_t tttlen = frs2.Read(bBuf, 8 * 1024 * 1024);
   //         size_t ooolen = 16 * 1024 * 1024;
   //         while (tttlen > 0)
   //         {
   //             ooolen = 16 * 1024 * 1024;
   //             int nRet = DecodeData(p, bBuf, tttlen, bOut, ooolen);
   //             fws.Write(bOut, 16 * 1024 * 1024 - ooolen);
   //             if (nRet != 2)
   //             {
   //                 DecodeExit(p);
   //                 break;
   //             }
   //         }
   //     }
   // }
   // FREE( bBuf );
   // FREE( bOut );

    return 0;
}
