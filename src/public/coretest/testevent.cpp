#include "stdafx.h"
#include "streambuf.h"
#include "thread.h"
#include "event.h"
#include "testevent.h"

using namespace CREEK;

class CTestEvent : public CEventBase
{
public:
    CTestEvent(void) {}
    virtual ~CTestEvent(void) {}
    virtual size_t Length(void) const { return 0;}
    virtual void  Serialize(CStream&) { }
    PCXStr Name(void)
    {
        return TF("I'm CTestEvent");
    }
};

class CTestEvent2 : public CEventBase
{
public:
    CTestEvent2(void) {}
    virtual ~CTestEvent2(void) {}
    virtual size_t Length(void) const { return 0;}
    virtual void  Serialize(CStream&) { }
    PCXStr Name(void)
    {
        return TF("I'm CTestEvent2");
    }
};

class CTestEventStream : public CEventBase
{
public:
    CTestEventStream(void)
    : m_nTest(100)
    {}
    virtual ~CTestEventStream(void) { }
    virtual size_t Length(void) const
    {
        return (sizeof(Int));
    }
    virtual void  Serialize(CStream& Stream)
    {
        if (Stream.IsRead())
        {
            Stream >> m_nTest;
        }
        else
        {
            Stream << m_nTest;
        }
    }
    Int GetValue(void)
    {
        return m_nTest;
    }
    void SetValue(Int nValue)
    {
        m_nTest = nValue;
    }
private:
    Int m_nTest;
};

class CTestEventHandler : public CEventHandler
{
public:
    CTestEventHandler(void) { }
    virtual ~CTestEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        if (utEvent < 2000)
        {
            DEV_INFO(TF("event tick %d, event name %s, %llx"), utEvent, ((CTestEvent*)(utData))->Name(), ullParam);
        }
        else if (utEvent == 2000)
        {
            DEV_INFO(TF("event tick 2000, cache data[%p] alloc from index %p"), utData, ullParam);
            MObject::MCFree((PINDEX)ullParam, (PByte)utData);
        }
        else
        {
            DEV_INFO(TF("event tick %d, thread id=%p, param=%p"), utEvent, utData, ullParam);
        }
        CPlatform::SleepEx(10000);
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
    {
        if (utEvent == 2001)
        {
            DEV_INFO(TF("event tick 2001, event name %s, %llx"), ((CTestEvent*)(&EventRef))->Name(), ullParam);
        }
        else
        {
            DEV_INFO(TF("event tick %d, event name %s, %llx"), utEvent, ((CTestEvent2*)(&EventRef))->Name(), ullParam);
        }
        CPlatform::SleepEx(10000);
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
    {
        if (utEvent == 2002)
        {
            XChar szTemp[16];
            Stream.Read(szTemp, 16 * sizeof(XChar));

            DEV_INFO(TF("event tick 2002, event name %s, %llx"), szTemp, ullParam);
        }
        else if (utEvent == 6666)
        {
            XChar szBuf[16] = { 0 };
            Stream.Read(szBuf, sizeof(szBuf));
            DEV_INFO(TF("event tick %d, event data str %s, %llx"), utEvent, szBuf, ullParam);
        }
        else
        {
            CTestEventStream TestEventStream;
            TestEventStream.Serialize(Stream);
            DEV_INFO(TF("event tick %d, event data %d, %llx"), utEvent, TestEventStream.GetValue(), ullParam);
        }
        CPlatform::SleepEx(10000);
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t, UInt)
    {
        return RET_OKAY;
    }
};

class CTestTickEventHandler : public CEventHandler
{
public:
    CTestTickEventHandler(void) { }
    virtual ~CTestTickEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t, uintptr_t, ULLong)
    {
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t, CEventBase&, ULLong)
    {
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t, CStream&, ULLong)
    {
        return RET_OKAY;
    }

    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount)
    {
        DEV_INFO(TF("Tick-event %d, remain %d"), utEvent, uCount);
        if (utEvent == 8888)
        {
            CEventQueue::CreateTickEvent(9999, 300 * 1000, *this);
            return RET_OKAY;
        }
        DEV_INFO(TF("Tick-event222222 %d, remain %d"), utEvent, uCount);
        return RET_OKAY;
    }
};

void TestEvent(void)
{
    //PINDEX index = MObject::MCCreate(64);

    //CTestEventHandler TestEventHandler; // �¼���������
    //CTestEvent TestEvent; // �¼������
    //CEventQueuePtr EventQueuePtr; // ���洴���¼����е�����ָ�����
    // //�����¼�����, �¼����к��¼����������
    // //�¼���������¼���ȫ�����¼���������������
    // //??????????���߳��¼����лᵼ���¼����ǰ�FIFO���д���??????????
    //if (CEventQueue::EventQueue(EventQueuePtr, TestEventHandler, 64, 2))
    //{
    //    EventQueuePtr->SetThreadEvent(221122, 332233);
    //    EventQueuePtr->Init(); // ������ʱ����г�ʼ��

    //    const XChar szBuf[] = TF("Hello Word");
    //    // 1. ���¼����м�����йܵ��¼������ַ�����¼�
    //    //    ���йܷ�ʽ, �����¼���������е�ָ����
    //    //    �¼��������������Ժ�, ֱ�Ӷ���
    //    Int nEventID = 1023;
    //    for (; nEventID < 1087; ++nEventID)
    //    {
    //        EventQueuePtr->Add(nEventID, (uintptr_t)&TestEvent, 0x1234567891234567); // OnHandle(Int, uintptr_t, ULLong)
    //    }

    //    PByte pData = MObject::MCAlloc(index, 13);
    //    EventQueuePtr->Add(2000, (uintptr_t)pData, (ULLong)index, CEventQueue::EVENT_TYPE_CACHE_REF); // OnHandle(Int, uintptr_t, ULLong)

    //    CTestEvent* pNew1 = MNEW CTestEvent;
    //    EventQueuePtr->Add(2001, (uintptr_t)pNew1, 0x1234567890000000, CEventQueue::EVENT_TYPE_REFCOUNT); // OnHandle(Int, CEventBase&, ULLong)

    //    EventQueuePtr->Add(2002, (uintptr_t)szBuf, (ULLong)(11 * sizeof(XChar)), CEventQueue::EVENT_TYPE_CACHE_QUEUE); // OnHandle(Int, CStream&, ULLong)
    //    // 2. ���¼����м����������, ������MNEW�������¼�����ʹ��EVENT_TYPE_REFCOUNTʧ��
    //    //    ���¼����м����йܵ��¼������ַ�����¼�
    //    //    �¼���������е�ָ�����¼��������������Ժ�,
    //    //    �Զ�Release�������
    //    CTestEventStream TestEventStream;
    //    EventQueuePtr->Add(2003, TestEventStream, 0x1234567891234567); // failed
    //    EventQueuePtr->Add(2004, TestEventStream, 0x7777777777777777, CEventQueue::EVENT_TYPE_CACHE_QUEUE); // OnHandle(Int, CStream&, ULLong)
    //    // 3. ���¼����м����������
    //    nEventID = 3001;
    //    CTestEvent2* pNew2 = MNEW CTestEvent2;
    //    pNew2->AddRef();
    //    EventQueuePtr->Add(nEventID, *pNew2, 0x1234567890000000 + nEventID); ++nEventID;
    //    EventQueuePtr->Add(nEventID, *pNew2, 0x1234567890000000 + nEventID); ++nEventID;
    //    EventQueuePtr->Add(nEventID, *pNew2, 0x1234567890000000 + nEventID); ++nEventID;
    //    EventQueuePtr->Add(nEventID, *pNew2, 0x1234567890000000 + nEventID); ++nEventID;
    //    pNew2->Release();

    //    // ֱ�����¼����д������л���������
    //    CBufReadStream BufReadStream(sizeof(szBuf), (PByte)szBuf);
    //    EventQueuePtr->Add(6666, BufReadStream, 0x1234567891234567);

    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }
    //    CPlatform::SleepEx(2000);
    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }
    //    CPlatform::SleepEx(2000);
    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }
    //    CPlatform::SleepEx(4000); // �ȴ�1��

    //    CPlatform::SleepEx(2000);
    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }
    //    CPlatform::SleepEx(2000);
    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }
    //    CPlatform::SleepEx(2000);
    //    if (EventQueuePtr->CheckThreads(1))
    //    {
    //        DEV_WARN(TF("EventQueuePtr Check Threads FAILED!!!"));
    //    }

    //    EventQueuePtr->Exit(); // �����¼�����
    //    // ��������, ����ָ�������Զ��ͷ�ʱ�����
    //}
    //MObject::MCDestroy(index);

    CTestTickEventHandler* pTTT = MNEW CTestTickEventHandler;
    // ע��һ����ʱ�¼�, ID=8888, �¼����1��, ��ʱ�ܹ�����3��
    // ��ʱʱ�䵽��TestTickEventHandler�����������ʱ�¼�
    CEventQueue::CreateTickEvent(8888, 1000, *pTTT, 1);
//    CEventQueue::CreateTickEvent(9999, 1000, *pTTT, 6);
//    CPlatform::SleepEx(2000);
//    CEventQueue::CheckTickEvent(8888);
//    CEventQueue::CheckTickEvent(9999);
//    CPlatform::SleepEx(2000);
//    CEventQueue::CheckTickEvent(8888);
//    CEventQueue::CheckTickEvent(9999);
//    CPlatform::SleepEx(2000);
//    CEventQueue::CheckTickEvent(8888);
//    CEventQueue::CheckTickEvent(9999);
    CPlatform::SleepEx(8000);
//#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
//    CWaitableTimer Timer;
//    Timer.SetTimer(-200 * 10000, 400);
//    for (Int i = 0; i < 4; ++i)
//    {
//        Timer.Wait();
//        DEV_INFO(TF("%dth Timer"), i);
//    }
//#endif
}
