#include "stdafx.h"
#include "thread.h"
#include "exception.h"
#include "testthread.h"

using namespace CREEK;

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
Int MyThreadExceptHandler(TId tThreadId, Int nThreadType, Int nThreadRoutine, void* pRoutine, struct _EXCEPTION_POINTERS* pExceptInfo)
{
    DEV_DUMP(TF("id=%p, type=%d, step=%d, ptr=%p"), tThreadId, nThreadType, nThreadRoutine, pRoutine);

    CString strName = TF("MyThreadExceptHandler");
    strName.AppendFormat(TF("type-%d---routine-%d"), nThreadType, nThreadRoutine);
    CSEHException::Dump(pExceptInfo, *strName);
    //if (nThreadRoutine != CThread::THREAD_ROUTINE_RUN)
    {
        return EXCEPTION_EXECUTE_HANDLER;
    }
    //return EXCEPTION_CONTINUE_SEARCH;
}
#endif

enum MY_THREAD
{
    MY_THREAD_NUM = 1,
};

class CMyThread : public CThread
{
public:
    CMyThread() : m_nCount(0) {}
protected:
    virtual bool OnStart(void)
    {
        DEV_INFO(TF("Thread %llX on start"), GetId());
        //Int *p = nullptr;
        //*p = 0;
        return true;
    }

    virtual void OnStop(void)
    {
        DEV_INFO(TF("Thread %d on stop"), GetId());
        //Int *p = nullptr;
        //*p = 0;

    }

    virtual void OnKill(void)
    {
        //DEV_INFO(TF("Thread %d on kill"), GetId());
    }

    virtual bool Run(void)
    {
        if (m_nCount == 0)
        {
            UpdateWaitStatus(false);
        }
        else
        {
            SetWaitStatus(false);
        }
        ++m_nCount;
        if (m_nCount > 4)
        {
            DEV_INFO(TF("Thread %d on OUT"), GetId());
            return false;
        }
        DEV_INFO(TF("Thread[%d] on Running"), GetId());
        //Int *p = nullptr;
        //*p = 0;
        if (m_nCount == 1)
        SetWaitStatus();
        CPlatform::SleepEx(2000);

        return true;
        //return false;
    }

    Int   m_nCount;
};

class CMyRunnable : public CRunnable
{
public:
    CMyRunnable() : m_nCount(0) {}
    ~CMyRunnable() { DEV_INFO(TF("Runnable[%p] dctr"), this);}
public:
    virtual bool OnStart(TId tid)
    {
        DEV_INFO(TF("Runnable[%p] on start"), this);
        //Int *p = nullptr;
        //*p = 0;
        //DEV_INFO(TF("Runnable[%p] on start1111"), this);
        return true;
    }

    virtual void OnStop(void)
    {
        DEV_INFO(TF("Runnable[%p] on stop"), this);
        //Int *p = nullptr;
        //*p = 0;
    }

    virtual bool Run(void)
    {
        ++m_nCount;
        if (m_nCount > 1)
        {
            DEV_DUMP(TF("Runnable[%p] on OUT"), this);
            return false;
        }
        DEV_INFO(TF("Runnable-Thread[%p] on Running"), this);
        return true;
    }

    Int   m_nCount;
};

class CMyQueueTask : public CQueueTask
{
public:
    ~CMyQueueTask() { DEV_INFO(TF("QueueTask[%p] dctr"), this);}

    virtual void Run(TId tid)
    {
        DEV_INFO(TF("QueueTask-Thread[tid:%p] run Task %p"), tid, this);
        //Int *p = nullptr;
        //*p = 0;
    }
};

class CORunnable : public CRunnable
{
public:
    virtual bool OnStart(TId tid)
    {
        return true;
    }

    virtual void OnStop(void)
    {
        DEV_INFO(TF("ORunnable[%p] on stop"), this);
        //Int *p = nullptr;
        //*p = 0;
    }

    virtual bool Run(void)
    {
        DEV_INFO(TF("ORunnable[%p] on Run"), this);
        return false;
    }
};

void TestThread(void)
{
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    CPlatform::SetExceptHandler(MyThreadExceptHandler);
#endif

    //CPlatform::SleepEx(10000);

    CMyThread MyThread;
    MyThread.Start();

    for (UInt uCheck = 1; uCheck < 9; ++uCheck)
    {
        CPlatform::SleepEx(500);
        MyThread.CheckStatus(1);
    }

//    CThread::QueueThread(2);
//    CMyQueueTask  MyQueueTask[MY_THREAD_NUM];
//    for (Int i = 0; i < MY_THREAD_NUM; ++i)
//    {
//        CThread::QueueTask(MyQueueTask[i]);
//    }

    for (int i = 0; i < 4; ++i)
    {
        CThread::QueueTask(MNEW CMyQueueTask);
        CThread::StartRunnable(MNEW CMyRunnable);
    }

    //CMyRunnable MyRunnable[MY_THREAD_NUM];
    //for (Int i = 0; i < MY_THREAD_NUM; ++i)
    //{
    //    CThread::StartRunnable(MyRunnable[i]);
    //}

//    CMyThread MyThread[MY_THREAD_NUM];
//    for (Int i = 0; i < MY_THREAD_NUM; ++i)
//    {
//        MyThread[i].Start();
//    }

    //DEV_INFO(TF("SleepEx 8000"));
    CPlatform::SleepEx(10000);
    //for (Int i = 0; i < MY_THREAD_NUM; ++i)
    //{
    //    MyThread[i].Wait();
    //}


    //CORunnable ORunnable;
    //CThread::StartRunnable(ORunnable);
    //CPlatform::SleepEx(10000);
}
