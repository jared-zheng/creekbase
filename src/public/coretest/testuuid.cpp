#include "stdafx.h"
#include "uidgen.h"
#include "streamfile.h"
#include "streambuf.h"
#include "markup.h"
#include "sync.h"
#include "codec.h"
#include "testuuid.h"

using namespace CREEK;

DECLARE_UUID( MyTest, {EFA24E61-B078-11d0-89E4-00C04FC9E26E} )
class MyTest
{
public:
    Int i;
};

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
size_t stAll  = 0;
size_t stOkay = 0;
BYTE  bBuf[1024 * 1024];

CAFileReadStream  afrs;
CAFileWriteStream afws;


VOID WINAPI AIO_READ(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
    if (dwErrorCode == 0)
    {
        DEV_INFO(TF("file aio read : %d + %d >= %d"), stOkay, dwNumberOfBytesTransfered, stAll);
        stOkay += dwNumberOfBytesTransfered;
        lpOverlapped->Offset += dwNumberOfBytesTransfered;
        afws.Write(bBuf, dwNumberOfBytesTransfered);
    }
    else
    {
        DEV_INFO(TF("file aio read err = %d, %d"), dwErrorCode, dwNumberOfBytesTransfered);
    }
}

VOID WINAPI AIO_WRITE(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
    if (dwErrorCode == 0)
    {
        DEV_INFO(TF("file aio write : %d"), dwNumberOfBytesTransfered);
        lpOverlapped->Offset += dwNumberOfBytesTransfered;
        if (stAll > stOkay)
        {
            afrs.Read(bBuf, 1024 * 1024);
        }
    }
}
#endif

void TestUUID(void)
{
    CUUID uuid12;
    CUUIDGenerator::Create(uuid12);

    CString strUUID12;
    uuid12.ToString(strUUID12);

    DEV_INFO(TF("UUID12=%s"), *strUUID12);


    CString strUUID13;
    CUUIDGenerator::Create(strUUID13);

    DEV_INFO(TF("UUID13=%s"), *strUUID13);


    PCXStr psz111 = TF("564DD780B1C9B0A44FAF0A85AC787717");

    CUUID uuid19;
    uuid19.ToUUID(psz111, CUUID::UUID_FROMAT_DIGITS_ONLY);

    CString strUUID19;
    uuid19.ToString(strUUID19);

    DEV_INFO(TF("UUID19=%s"), *strUUID19);

    Byte bbb[] = { 01, 02, 03, 04, 05, 06, 00, 00 };

    ULLong ull = CPlatform::ByteSwap(*(PULLong)bbb);
    ull >>= 16;
    DEV_INFO(TF("---------------%016llX"), ull);



   // const CUUID& uuid = UUID_OF( MyTest );

    //CUUID uuidx = uuid;
    //CString str123;
    //uuid.ToString(str123);
    //DEV_INFO(TF("UUID : %s"), *str123);
    //
    //
    //CUUID uuid1;
    //CUUID uuid2;
    //bool bbb1 = (uuid1 == uuid2);
    //bool bbb2 = (uuid1 != uuid2);
    //
    //uuid1.ToUUID(TF("{EFA24E61-B078-11d0-89E4-00C04FC9E26E}"));
    //uuid2.ToUUID(TF("{F935DC26-1CF0-11D0-ADB9-00C04FD58A0B}"));
    //
    //bbb1 = (uuid1 == uuid2);
    //bbb2 = (uuid1 != uuid2);
    //
    //UID uuid3 = { 0 };
    //UID uuid4 = { 0 };
    //uuid1.GetUUID(uuid3);
    //uuid2.GetUUID(uuid4);
    //
    //CStringFix strFix;
    //CString str;
    //uuid1.ToString(strFix);
    //uuid2.ToString(str);
    //
    //DEV_INFO(TF("UUID1 : %s"), *strFix);
    //DEV_INFO(TF("UUID2 : %s"), *str);
    //
    //CFileWriteStream fws;
    //if (fws.Create(TF("D:\\1.dat"), FILEF_NEW_ALWAYS|FILEF_EXT_NOBUF))
    //{
    //    uuid1.Serialize(fws);
    //    fws << TF('\r') << TF('\n');
    //    uuid2.Serialize(fws);
    //
    //    fws << strFix;
    //
    //    const ULong ulTTT = 0x12345678;
    //
    //    fws << ulTTT;
    //
    //    fws << TF("������������kjfoikids�۸��Ƿ�����vŶiJF1V23#$#*P");
    //
    //    void* p = (void*)0x12001300;
    //
    //    fws << p;
    //}
    //fws.Close();
    //
    //CUUID uuid5;
    //CUUID uuid6;
    //
    //CFileReadStream frs;
    //if (frs.Create(TF("D:\\1.dat"), FILEF_OPEN_EXIST))
    //{
    //    uuid5.Serialize(frs);
    //
    //    XChar c1, c2;
    //    frs >> c1 >> c2;
    //    uuid6.Serialize(frs);
    //
    //    CString str2;
    //    ULong ulTTT2 = 0;
    //    void* p2 = nullptr;
    //    frs >> str2 >> ulTTT2 >> str >> p2;
    //
    //    DEV_INFO(TF("%s, %#X, %s, %p"), *str2, ulTTT2, *str, p2);
    //}
    //uuid5.ToString(strFix);
    //uuid6.ToString(str);
    //DEV_INFO(TF("UUID5 : %s"), *strFix);
    //DEV_INFO(TF("UUID6 : %s"), *str);
    //
    //CPlatform::SetLocal(LC_ALL, TF(""));
    //DEV_INFO(CPlatform::SetLocal());
    //
    //CFileReadStream frsxml1;
    //if (frsxml1.Create(TF("D:\\ansitest.xml")))
    //{
    //    CXMLDocument doc;
    //    doc.Load(frsxml1, 0, ENT_LOCAL);

    //    bool bRet = doc.HasChild();
    //    DEV_INFO(TF("has child %d"), bRet);

    //    CFileWriteStream fwsx;
    //    if (fwsx.Create(TF("D:\\ansi2ansi.xml"), FILEF_NEW_ALWAYS|FILEF_EXT_NOBUF))
    //    {
    //        doc.Save(fwsx, ENT_LOCAL);
    //    }
    //    fwsx.Close();

    //    if (fwsx.Create(TF("D:\\ansi2utf8.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
    //    {
    //        doc.Save(fwsx, ENT_UTF8);
    //    }
    //    fwsx.Close();

    //    if (fwsx.Create(TF("D:\\ansi2utf16.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
    //    {
    //        doc.Save(fwsx, ENT_UTF16);
    //    }
    //    fwsx.Close();

    //    if (fwsx.Create(TF("D:\\ansi2utf32.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
    //    {
    //        doc.Save(fwsx, ENT_UTF32);
    //    }
    //}

//    frsxml1.Close();
//    if (frsxml1.Create(TF("D:\\utf8test.xml")))
//    {
//        CXMLDocument doc;
//        doc.Load(frsxml1, 0, ENT_LOCAL);
//
//        bool bRet = doc.HasChild();
//        DEV_INFO(TF("has child %d"), bRet);
//
//        CFileWriteStream fwsx;
//        if (fwsx.Create(TF("D:\\utf82ansi.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_LOCAL);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf82utf8.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF8);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf82utf16.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF16);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf82utf32.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF32);
//        }
//    }
//
//    frsxml1.Close();
//    if (frsxml1.Create(TF("D:\\utf16test.xml")))
//    {
//        CXMLDocument doc;
//        doc.Load(frsxml1, 0, ENT_LOCAL);
//
//        bool bRet = doc.HasChild();
//        DEV_INFO(TF("has child %d"), bRet);
//
//        CFileWriteStream fwsx;
//        if (fwsx.Create(TF("D:\\utf162ansi.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_LOCAL);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf162utf8.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF8);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf162utf16.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF16);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf162utf32.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF32);
//        }
//    }
//
//    frsxml1.Close();
//    if (frsxml1.Create(TF("D:\\utf32test.xml")))
//    {
//        CXMLDocument doc;
//        doc.Load(frsxml1, 0, ENT_LOCAL);
//
//        bool bRet = doc.HasChild();
//        DEV_INFO(TF("has child %d"), bRet);
//
//        CFileWriteStream fwsx;
//        if (fwsx.Create(TF("D:\\utf322ansi.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_LOCAL);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf322utf8.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF8);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf322utf16.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF16);
//        }
//        fwsx.Close();
//
//        if (fwsx.Create(TF("D:\\utf322utf32.xml"), FILEF_NEW_ALWAYS | FILEF_EXT_NOBUF))
//        {
//            doc.Save(fwsx, ENT_UTF32);
//        }
//    }
//
//#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
//
//    // 1. callback
//    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0, 0, AIO_READ))
//    {
//        size_t stRead = 0;
//        stAll = afrs.Size();
//        if (afws.Create(TF("D:\\Download\\Win81sdkbak1.rar"), 0, 0, AIO_WRITE))
//        {
//            afrs.Read(bBuf, 1024 * 1024);
//            //while (afrs.Wait() && (stAll > stOkay))
//            while ((afrs.Wait(stRead) == RET_OKAY) && (stAll > stOkay))
//            {
//                DEV_INFO(TF("file aio ID : %d, %x --- buf=%X"), ::GetLastError(), stRead, stOkay);
//            }
//        }
//    }
//
//    // 2. event
//    CSyncEvent ReadEvent, WriteEvent;
//    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0, 0, nullptr, ReadEvent.m_hEvent))
//    {
//        stAll = afrs.Size();
//        if (afws.Create(TF("D:\\Download\\Win81sdkbak2.rar"), 0, 0, nullptr, WriteEvent.m_hEvent))
//        {
//            Int    nRet   = 0;
//            size_t stRead = 0;
//            while (stAll > stOkay)
//            {
//                afrs.Read(bBuf, 1024 * 1024);
//                nRet = ReadEvent.Wait((UInt)TIMET_INFINITE);
//                if (nRet == RET_OKAY)
//                {
//                    afrs.Wait(stRead);
//                    afrs.GetAttr().Offset += (DWORD)stRead;
//                    stOkay += stRead;
//
//                    afws.Write(bBuf, stRead);
//                    nRet = WriteEvent.Wait((UInt)TIMET_INFINITE);
//                    if (nRet == RET_OKAY)
//                    {
//                        afws.Wait(stRead);
//                        afws.GetAttr().Offset += (DWORD)stRead;
//                        continue;
//                    }
//                }
//                break;
//            }
//        }
//    }
//
//    // 3. wait
//    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0))
//    {
//        stAll = afrs.Size();
//        if (afws.Create(TF("D:\\Download\\Win81sdkbak3.rar"), 0))
//        {
//            Int    nRet = 0;
//            size_t stRead = 0;
//            while (stAll > stOkay)
//            {
//                afrs.Read(bBuf, 1024 * 1024);
//                nRet = afrs.Result(stRead, true);
//
//                if (nRet == RET_OKAY)
//                {
//                    afrs.Seek((SeekPos)stRead);
//                    stOkay += stRead;
//                    afws.Write(bBuf, stRead);
//
//                    nRet = afws.Result(stRead, true);
//                    if (nRet == RET_OKAY)
//                    {
//                        afws.Seek((SeekPos)stRead);
//                        continue;
//                    }
//                }
//                break;
//            }
//        }
//    }
//#endif



CString strKey = TF("asdasdasd");
CKVNode KVNode;
KVNode[TF("tag")].SetValue("slkjfslkd");
KVNode[TF("serverFlag")].SetValue("slkjfslkd");
KVNode[TF("account")].SetValue("3rqwefaweslkjfslk");

CString strJson;
KVNode.Save(strJson);

////ENCODING_TYPE eType = ENT_LOCAL;
////CString strJson;
////CKVStore s3(ENT_LOCAL);
////s3.Save(KVNode, strJson, eType, false);
////
////CBufReadStream  brs(strJson.Length(), (PByte)*strJson);
////CBufWriteStream bws(CAES::AlignSize(brs.Size()));
////CAES::Encode(strKey, brs, bws);
////
////CFileWriteStream fws;
////if (fws.Create(TF("testKeyFile.ini")))
////{
////    fws.Write(bws.GetBuf(), bws.Tell());
////}
////fws.Close();
//
//CKVNode KVNode;
//
//CFileReadStream frs;
//if (frs.Create(TF("testKeyFile.ini")))
//{
//    CBufReadStream brs(frs.Size());
//    frs.Read(brs.GetBuf(), brs.Size());
//
//    CBufWriteStream bws(CAES::AlignSize(brs.Size()));
//    CAES::Decode(strKey, brs, bws);
//
//    CBufReadStream ref;
//    ref.Refer(bws);
//
//    CKVStore sw(ENT_LOCAL);
//    sw.Load(KVNode, ref);
//}


//#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
//    CKVNode kv3;
//    CFileReadStream frs;
//    if (frs.Create(TF("D:\\json_data.txt")))
//    {
//        CKVStore s2(ENT_UTF8);
//        s2.Load(kv3, frs);
//    }
//
//    CStreamSeekScope scope(frs, 10);
//
//    CFileWriteStream fws2;
//    if (fws2.Create(TF("D:\\kv38.txt")))
//    {
//        CKVStore s3(ENT_UTF16);
//        s3.Save(kv3, fws2, ENT_UTF8);
//    }
//#else
//    CKVNode kv;
//    CFileReadStream frs;
//    if (frs.Create(TF("./json_data.txt")))
//    {
//        CKVStore s2(ENT_UTF8);
//        s2.Load(kv, frs);
//    }
//
//    CFileWriteStream fws2;
//    if (fws2.Create(TF("./kv32.txt")))
//    {
//        CKVStore s3(ENT_UTF8);
//        s3.Save(kv, fws2, ENT_UTF32);
//    }
//#endif
}
