#include "stdafx.h"
#include "timescope.h"

using namespace CREEK;

PCXStr GetOSType(UInt uOSType)
{
    switch (uOSType)
    {
        CASE_RETSTR(CPlatform::OST_UNKNOWN, TF("Unknown OS"));
        CASE_RETSTR(CPlatform::OST_WINDOWS, TF("Windows"));
        CASE_RETSTR(CPlatform::OST_LINUX, TF("Linux"));
        CASE_RETSTR(CPlatform::OST_MAC, TF("Mac"));
        CASE_RETSTR(CPlatform::OST_IOS, TF("iOS"));
        CASE_RETSTR(CPlatform::OST_ANDROID, TF("Android"));
    default:
        {
            return TF("Unknown OS Type");
        }
    }
}

void TestPlatform(void)
{
    ULLong ullRuntime = CPlatform::GetRuntimeConfig();
    DEV_ERROR(TF("---Platform Information--- %llX"), ullRuntime);
    DEV_INFO(TF(" %s"), TF(__ARCH_TARGET_STR__));
    DEV_INFO(TF(" %s"), TF(__PLATFORM_TARGET_STR__));
    DEV_INFO(TF(" %s"), TF(__RUNTIME_CHARSET_STR__));
    DEV_INFO(TF(" %s"), (ullRuntime & RUNTIME_CONFIG_DEBUG)       ? TF("Debug Status")   : TF("Release Status"));
    DEV_INFO(TF(" %s"), (ullRuntime & RUNTIME_CONFIG_STATIC)      ? TF("Static Library") : TF("Dynamic Library"));


    CPlatform::CPUINFO cpui;
    CPlatform::GetCPUInfo(cpui);
    DEV_WARN(TF("---CPU Information---"));
    DEV_INFO(TF(" %s"), cpui.szInfo);
    DEV_INFO(TF(" Cores : %d"), cpui.uCores);

    CPlatform::MEMINFO mem;
    CPlatform::GetMemInfo(mem);
    DEV_ERROR(TF("---Memory Information---"));
    DEV_INFO(TF(" Memory Current Used       : %8d[%%]"), mem.uUsedPercent);
    DEV_INFO(TF(" Memory Page Size          : %8d[KB]"), mem.uPageSize);
    DEV_INFO(TF(" Memory Total Physical Size: %8d[MB]"), mem.uTotalPhys);
    DEV_INFO(TF(" Memory Avail Physical Size: %8d[MB]"), mem.uAvailPhys);

    CPlatform::OSINFO os;
    CPlatform::GetOSInfo(os);
    DEV_DEBUG(TF("---Operation System Information---"));
    DEV_INFO(TF(" OS Type    : %s"),             GetOSType(os.uOSType));
    DEV_INFO(TF(" OS Version : %d.%d Build %d"), os.uMajor, os.uMinor, os.uBuild);
    DEV_INFO(TF(" OS Info    : %s"),             os.szInfo);

    CPlatform::TIMEINFO time;
    CPlatform::GetTimeInfo(time);
    DEV_INFO(TF("Now : %d-%d-%d(%d), %d-%d-%d-%d"), time.usYear, time.usMonth, time.usDay, time.usWeekDay, time.usHour, time.usMinute, time.usSecond, time.usMSecond);

    CTime tt1(time);

    CPlatform::SleepEx(2000);
    CPlatform::GetTimeInfo(time);
    CTime tt2(time);

    CTimeScope ts = tt2 - tt1;
    CString    strF = ts.Format();
    DEV_INFO(TF("%s"), *strF);

    LLong llTick = CPlatform::GetOSRunningTick();
    DEV_INFO(TF("os tick %lld"), llTick);
    llTick = CPlatform::Tick2MilliSecond(llTick);
    DEV_INFO(TF("os ms %lld"), llTick);

    llTick = CPlatform::GetRunningTime();
    DEV_INFO(TF("running %lld"), llTick);

    DEV_INFO(TF("MS timestamp %lld"), CPlatform::GetTimeStamp());
}
