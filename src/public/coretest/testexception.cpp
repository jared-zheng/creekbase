#include "stdafx.h"
#include "exception.h"
#include "testexception.h"

using namespace CREEK;

void func(void)
{
    Int *p = nullptr;

    *p = 0;
}

void TestException(void)
{
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    CSEHExceptionFilter::Init();

    func();

    CSEHExceptionFilter::Exit();
#else
    CExceptionTerminater::Init();

    func();

    CExceptionTerminater::Exit();

#endif
}
