﻿#include "stdafx.h"
#include "container.h"
#include "testmem.h"

using namespace CREEK;

enum TEST_COUNT
{
    TESTC_MAX = 1024,
};

class CTestMenBase : public MObject
{
public:
    CTestMenBase(UInt uCopy)
    : m_uSizeCopy(uCopy)
    {
    }
    virtual ~CTestMenBase(void)
    {
    }
public:
    UInt   m_uSizeCopy;
};

template <UInt uSize>
class CTestMem : public CTestMenBase
{
public:
    CTestMem(UInt uCopy)
    : CTestMenBase(uCopy)
    {
    }

    void Print(void)
    {
        DEV_INFO(TF("%d-->%d"), m_uSizeCopy, uSize);
    }

private:
    void*  m_pT[uSize];
};

typedef CTestMem<10>  CTestMem10;
typedef CTestMem<20>  CTestMem20;
typedef CTestMem<30>  CTestMem30;
typedef CTestMem<40>  CTestMem40;
typedef CTestMem<50>  CTestMem50;
typedef CTestMem<60>  CTestMem60;
typedef CTestMem<70>  CTestMem70;
typedef CTestMem<80>  CTestMem80;
typedef CTestMem<90>  CTestMem90;
typedef CTestMem<100> CTestMem100;
typedef CTestMem<200> CTestMem200;
typedef CTestMem<300> CTestMem300;
typedef CTestMem<400> CTestMem400;
typedef CTestMem<500> CTestMem500;
typedef CTestMem<600> CTestMem600;
typedef CTestMem<700> CTestMem700;
typedef CTestMem<800> CTestMem800;
typedef CTestMem<900> CTestMem900;
CTestMenBase* GetObj(void)
{
    switch (rand() % 18)
    {
    case  0 : return MNEW CTestMem10(10);
    case  1 : return MNEW CTestMem20(20);
    case  2 : return MNEW CTestMem30(30);
    case  3 : return MNEW CTestMem40(40);
    case  4 : return MNEW CTestMem50(50);
    case  5 : return MNEW CTestMem60(60);
    case  6 : return MNEW CTestMem70(70);
    case  7 : return MNEW CTestMem80(80);
    case  8 : return MNEW CTestMem90(90);
    case  9 : return MNEW CTestMem100(100);
    case 10 : return MNEW CTestMem200(200);
    case 11 : return MNEW CTestMem300(300);
    case 12 : return MNEW CTestMem400(400);
    case 13 : return MNEW CTestMem500(500);
    case 14 : return MNEW CTestMem600(600);
    case 15 : return MNEW CTestMem700(700);
    case 16 : return MNEW CTestMem800(800);
    case 17 : return MNEW CTestMem900(900);
    }
    return nullptr;
}

void TestMem(void)
{
#if 0
    //srand(::GetTickCount());
    //size_t stAlloc = 16LL * 1024LL * 1024LL * 1024LL;
    //PInt pTest = (PInt)ALLOC( stAlloc );
    //PInt pTest1 = (PInt)ALLOC( stAlloc );
    //if ((pTest != nullptr) && (pTest1 != nullptr))
    //{
    //    DEV_INFO(TF("Alloc 32G okay"));
    //    for (Int i = 0; i < 20000; ++i)
    //    {
    //        Int j = i * rand();
    //        pTest[j] = 100;
    //        DEV_INFO(TF("Buffer %d set 100"), j);
    //    }
    //    MM_SAFE::Cpy(pTest1, stAlloc, pTest, stAlloc);
    //}
    //if (pTest != nullptr)
    //{
    //    FREE( pTest );
    //}
    //if (pTest1 != nullptr)
    //{
    //    FREE( pTest1 );
    //}
    // prt array to store random memory alloc ptr
    void* pV[32768];

    Int i = 0;
    // alloc small size memory
    Int j = rand() % 0x200000;
    void* p = ALLOC(j);

    for (i = 0; (i < 100) && p; ++i)
    {
        // realloc test
        j = rand() % 0x200000;
        p = REALLOC(p, j);
    }
    if (p)
    {
        // free memory
        FREE(p);
    }

    // memory depth alloc test
    DEV_INFO(TF("++++++BEGIN MEM Alloc test++++++"));
    for (i = 0; i < 0x00008000; ++i)
    {
        pV[i] = ALLOC( i);
    }
    // dump test
    i = rand() % 0x00008000;
    //if (pV[i] != nullptr)
    //{
        DUMP(pV[i]);
    //}

    // random free test
    DEV_INFO(TF("++++++BEGIN MEM random Free Test++++++"));
    for (i = 0; i < 0x200000; ++i)
    {
        j = (rand() & 0x7FFF);
        if (pV[j] != nullptr)
        {
            FREE( pV[j] );
            pV[j] = nullptr;
        }
    }
    i = rand() & 0x00008000;
    //if (pV[i] != nullptr)
    //{
        DUMP(pV[i]);
    //}

    // memory depth alloc test again
    DEV_INFO(TF("++++++BEGIN MEM random alloc Test++++++"));
    for (i = 0; i < 0x00008000; ++i)
    {
        if (pV[i] == nullptr)
        {
            pV[i] = ALLOC( i);
        }
    }
    i = rand() % 0x00008000;
//    if (pV[i] != nullptr)
    //{
        DUMP(pV[i]);
    //}

    // free
    DEV_INFO(TF("++++++BEGIN MEM free Test++++++"));
    for (i = 0; i < 0x00008000; ++i)
    {
        //j = rand() % 10;
        //if (j != 0)
        //{
            if (pV[i] != nullptr)
            {
                FREE( pV[i]);
            }
        //}
    }
    // dump memory info
    i = rand() % 0x00008000;
    //if (pV[i] != nullptr)
    //{
        DUMP(pV[i]);
    //}
    DEV_INFO(TF("++++++END MEM Free Test++++++"));

        // alloc small size memory
    j = rand() % 0x200000;
    p = ALLOC(j);
    if (p)
    {
        // free memory
        FREE(p);
    }

    // C方式申请内存:
    // 使用 ALLOC( size )申请内存
    // 使用 REALLOC( p, size ) 变动原来申请内存大小
    // 使用 FREE ( p ) 释放内存
    void** pVO = reinterpret_cast<void**>(ALLOC( TESTC_MAX * sizeof(void*) ));
    if (pVO != nullptr)
    {
        for (Int k = 0; k < 2; ++k)
        {
            // 1. 随机分配内存
            for (i = 0; i < TESTC_MAX; ++i)
            {
                if (pVO[i] == nullptr)
                {
                    pVO[i] = ALLOC( i * (rand() % 1024));
                }
                else
                {
                    pVO[i] = REALLOC( pVO[i], i * rand() );
                }
            }
            // 报告随机内存块的信息, DUMP传nullptr, 只报告内存管理器缓存的块区数量
            i = rand() % TESTC_MAX;
            DUMP(pVO[i]);

            // 2. 随机释放内存
            for (i = 0; i < TESTC_MAX; ++i)
            {
                j = (rand() % TESTC_MAX);
                if (pVO[j] != nullptr)
                {
                    FREE( pVO[j] );
                    pVO[j] = nullptr;
                }
            }
            // 报告随机内存块的信息, DUMP传nullptr, 只报告内存管理器缓存的块区数量
            i = rand() % TESTC_MAX;
            DUMP(pVO[i]);

            for (i = 0; i < TESTC_MAX; ++i)
            {
                if (pVO[i] != nullptr)
                {
                    FREE( pVO[i] );
                    pVO[i] = nullptr;
                }
            }
        }
        FREE( pVO );
    }

    // C++方式申请内存:
    // 使用 MNEW 创建类对象
    // 使用 MDELETE 释放类对象
    CTestMenBase** pO = reinterpret_cast<CTestMenBase**>(ALLOC( TESTC_MAX * sizeof(CTestMenBase*) ));
    if (pO != nullptr)
    {
        for (Int k = 0; k < 2; ++k)
        {
            // 1. 随机分配内存
            for (i = 0; i < TESTC_MAX; ++i)
            {
                if (pO[i] == nullptr)
                {
                    pO[i] = GetObj();
                }
            }
            // 报告随机内存块的信息, DUMP传nullptr, 只报告内存管理器缓存的块区数量
            i = rand() % TESTC_MAX;
            DUMP(pO[i]);

            // 2. 随机释放内存
            for (i = 0; i < TESTC_MAX; ++i)
            {
                j = (rand() % TESTC_MAX);
                if (pO[j] != nullptr)
                {
                    MDELETE pO[j];
                    pO[j] = nullptr;
                }
            }
            // 报告随机内存块的信息, DUMP传nullptr, 只报告内存管理器缓存的块区数量
            i = rand() % TESTC_MAX;
            DUMP(pO[i]);

            for (i = 0; i < TESTC_MAX; ++i)
            {
                if (pO[i] != nullptr)
                {
                    MDELETE pO[i];
                    pO[i] = nullptr;
                }
            }
        }
        FREE( pO );
    }



    // cache
        PByte pData0[TESTC_MAX] = { 0 };
        PByte pData1[TESTC_MAX] = { 0 };
        PByte pData2[TESTC_MAX] = { 0 };
        PByte pData3[TESTC_MAX] = { 0 };
        PByte pData4[TESTC_MAX] = { 0 };
        PByte pData5[TESTC_MAX] = { 0 };
        PByte pData6[TESTC_MAX] = { 0 };
        PByte pData7[TESTC_MAX] = { 0 };
        // 让MemAid提供固定缓存大小为4096, 每次增长频率为32(MemAid内部每次想内存管理器申请单元为 4096 * 32)
        // 上面的缓存大小MemAId默认是按系统页面内存大小对齐的, MemAid每次从内存管理器申请单元按64K对齐
        // 最大能缓存2G左右的固定缓存块总和大小, 大于2G固定缓存还是能申请, 但只要被释放就不会再缓存下来
        PINDEX index0 = MObject::MCCreate(4096, 30);
        PINDEX index1 = MObject::MCCreate(2000, 30);
        PINDEX index2 = MObject::MCCreate(9000, 30);
        PINDEX index3 = MObject::MCCreate(5000, 30);
        PINDEX index4 = MObject::MCCreate(1, 30);
        PINDEX index5 = MObject::MCCreate(15, 30);
        PINDEX index6 = MObject::MCCreate(179, 30);
        PINDEX index7 = MObject::MCCreate(300, 30);

        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            pData0[i] = MObject::MCAlloc(index0);
            pData1[i] = MObject::MCAlloc(index1);
            pData2[i] = MObject::MCAlloc(index2);
            pData3[i] = MObject::MCAlloc(index3);
            pData4[i] = MObject::MCAlloc(index4);
            pData5[i] = MObject::MCAlloc(index5);
            pData6[i] = MObject::MCAlloc(index6);
            pData7[i] = MObject::MCAlloc(index7);
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] != nullptr)
            {
                MObject::MCFree(index0, pData0[j]);
            }
            if (pData1[j] != nullptr)
            {
                MObject::MCFree(index1, pData1[j]);
            }
            if (pData2[j] != nullptr)
            {
                MObject::MCFree(index2, pData2[j]);
            }
            if (pData3[j] != nullptr)
            {
                MObject::MCFree(index3, pData3[j]);
            }
            if (pData4[j] != nullptr)
            {
                MObject::MCFree(index4, pData4[j]);
            }
            if (pData5[j] != nullptr)
            {
                MObject::MCFree(index5, pData5[j]);
            }
            if (pData6[j] != nullptr)
            {
                MObject::MCFree(index6, pData6[j]);
            }
            if (pData7[j] != nullptr)
            {
                MObject::MCFree(index7, pData7[j]);
            }
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] == nullptr)
            {
                pData0[j] = MObject::MCAlloc(index0);
            }
            if (pData1[j] == nullptr)
            {
                pData1[j] = MObject::MCAlloc(index1);
            }
            if (pData2[j] == nullptr)
            {
                pData2[j] = MObject::MCAlloc(index2);
            }
            if (pData3[j] == nullptr)
            {
                pData3[j] = MObject::MCAlloc(index3);
            }
            if (pData4[j] == nullptr)
            {
                pData4[j] = MObject::MCAlloc(index4);
            }
            if (pData5[j] == nullptr)
            {
                pData5[j] = MObject::MCAlloc(index5);
            }
            if (pData6[j] == nullptr)
            {
                pData6[j] = MObject::MCAlloc(index6);
            }
            if (pData7[j] == nullptr)
            {
                pData7[j] = MObject::MCAlloc(index7);
            }
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] != nullptr)
            {
                MObject::MCFree(index0, pData0[j]);
            }
            if (pData1[j] != nullptr)
            {
                MObject::MCFree(index1, pData1[j]);
            }
            if (pData2[j] != nullptr)
            {
                MObject::MCFree(index2, pData2[j]);
            }
            if (pData3[j] != nullptr)
            {
                MObject::MCFree(index3, pData3[j]);
            }
            if (pData4[j] != nullptr)
            {
                MObject::MCFree(index4, pData4[j]);
            }
            if (pData5[j] != nullptr)
            {
                MObject::MCFree(index5, pData5[j]);
            }
            if (pData6[j] != nullptr)
            {
                MObject::MCFree(index6, pData6[j]);
            }
            if (pData7[j] != nullptr)
            {
                MObject::MCFree(index7, pData7[j]);
            }
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] == nullptr)
            {
                pData0[j] = MObject::MCAlloc(index0);
            }
            if (pData1[j] == nullptr)
            {
                pData1[j] = MObject::MCAlloc(index1);
            }
            if (pData2[j] == nullptr)
            {
                pData2[j] = MObject::MCAlloc(index2);
            }
            if (pData3[j] == nullptr)
            {
                pData3[j] = MObject::MCAlloc(index3);
            }
            if (pData4[j] == nullptr)
            {
                pData4[j] = MObject::MCAlloc(index4);
            }
            if (pData5[j] == nullptr)
            {
                pData5[j] = MObject::MCAlloc(index5);
            }
            if (pData6[j] == nullptr)
            {
                pData6[j] = MObject::MCAlloc(index6);
            }
            if (pData7[j] == nullptr)
            {
                pData7[j] = MObject::MCAlloc(index7);
            }
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] != nullptr)
            {
                MObject::MCFree(index0, pData0[j]);
            }
            if (pData1[j] != nullptr)
            {
                MObject::MCFree(index1, pData1[j]);
            }
            if (pData2[j] != nullptr)
            {
                MObject::MCFree(index2, pData2[j]);
            }
            if (pData3[j] != nullptr)
            {
                MObject::MCFree(index3, pData3[j]);
            }
            if (pData4[j] != nullptr)
            {
                MObject::MCFree(index4, pData4[j]);
            }
            if (pData5[j] != nullptr)
            {
                MObject::MCFree(index5, pData5[j]);
            }
            if (pData6[j] != nullptr)
            {
                MObject::MCFree(index6, pData6[j]);
            }
            if (pData7[j] != nullptr)
            {
                MObject::MCFree(index7, pData7[j]);
            }
        }
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            Int j = rand() % TESTC_MAX;
            if (pData0[j] == nullptr)
            {
                pData0[j] = MObject::MCAlloc(index0);
            }
            if (pData1[j] == nullptr)
            {
                pData1[j] = MObject::MCAlloc(index1);
            }
            if (pData2[j] == nullptr)
            {
                pData2[j] = MObject::MCAlloc(index2);
            }
            if (pData3[j] == nullptr)
            {
                pData3[j] = MObject::MCAlloc(index3);
            }
            if (pData4[j] == nullptr)
            {
                pData4[j] = MObject::MCAlloc(index4);
            }
            if (pData5[j] == nullptr)
            {
                pData5[j] = MObject::MCAlloc(index5);
            }
            if (pData6[j] == nullptr)
            {
                pData6[j] = MObject::MCAlloc(index6);
            }
            if (pData7[j] == nullptr)
            {
                pData7[j] = MObject::MCAlloc(index7);
            }
        }
        MObject::MCDump(index0);
        MObject::MCDestroy(index0);
        MObject::MCDump(index1);
        MObject::MCDestroy(index1);
        MObject::MCDump(index2);
        MObject::MCDestroy(index2);
        MObject::MCDump(index3);
        MObject::MCDestroy(index3);
        MObject::MCDump(index4);
        MObject::MCDestroy(index4);
        MObject::MCDump(index5);
        MObject::MCDestroy(index5);
        MObject::MCDump(index6);
        MObject::MCDestroy(index6);
        MObject::MCDump(index7);
        MObject::MCDestroy(index7);
        for (Int i = 0; i < TESTC_MAX; ++i)
        {
            if (pData0[i] != nullptr)
            {
                MObject::MCFree(index0, pData0[i]);
            }
            if (pData1[i] != nullptr)
            {
                MObject::MCFree(index1, pData1[i]);
            }
            if (pData2[i] != nullptr)
            {
                MObject::MCFree(index2, pData2[i]);
            }
            if (pData3[i] != nullptr)
            {
                MObject::MCFree(index3, pData3[i]);
            }
            if (pData4[i] != nullptr)
            {
                MObject::MCFree(index4, pData4[i]);
            }
            if (pData5[i] != nullptr)
            {
                MObject::MCFree(index5, pData5[i]);
            }
            if (pData6[i] != nullptr)
            {
                MObject::MCFree(index6, pData6[i]);
            }
            if (pData7[i] != nullptr)
            {
                MObject::MCFree(index7, pData7[i]);
            }
        }
        MObject::MCDestroy(index0);
        MObject::MCDestroy(index1);
        MObject::MCDestroy(index2);
        MObject::MCDestroy(index3);
        MObject::MCDestroy(index4);
        MObject::MCDestroy(index5);
        MObject::MCDestroy(index6);
        MObject::MCDestroy(index7);

        PINDEX index8 = MObject::MCCreate(6000, 32);
        DEV_INFO(TF("new cache index %p"), index8);
        PINDEX index9 = MObject::MCCreate(12000, 32);
        DEV_INFO(TF("new cache index %p"), index9);
        PINDEX index10 = MObject::MCCreate(2000, 32);
        DEV_INFO(TF("new cache index %p"), index10);
        PINDEX index11 = MObject::MCCreate(9000, 32);
        DEV_INFO(TF("new cache index %p"), index11);
        PINDEX index12 = MObject::MCCreate(97, 32);
        DEV_INFO(TF("new cache index %p"), index12);
        PINDEX index13 = MObject::MCCreate(200, 32);
        DEV_INFO(TF("new cache index %p"), index13);
        PINDEX index14 = MObject::MCCreate(400, 32);
        DEV_INFO(TF("new cache index %p"), index14);
        PINDEX index15 = MObject::MCCreate(800, 32);
        DEV_INFO(TF("new cache index %p"), index15);

        MObject::MCDump(index8);
        MObject::MCDump(index9);
        MObject::MCDump(index10);
        MObject::MCDump(index11);
        MObject::MCDump(index12);
        MObject::MCDump(index13);
        MObject::MCDump(index14);
        MObject::MCDump(index15);

#else
        //srand(::GetTickCount());
        Int nTest = 0;
        Int nnn = 1024 * 256;
//        size_t stChunk = 0;
        CTArray<PByte> aPtr;
        aPtr.SetGrow(nnn);
        PINDEX indexCrash = MObject::MCCreate(6400, 0, 64 * 1024 * 1024);
        if (indexCrash != nullptr)
        {
            for (Int i = 0; i < nnn; ++i)
            {
                PByte pData = MObject::MCAlloc(indexCrash);
                if (pData != nullptr)
                {
                    aPtr.Add(pData);
                    ++nTest;
                }
            }
            DEV_INFO(TF("===========111[%d]"), nTest);
            nTest = 0;

            for (Int j = 0; j < nnn; ++j)
            {
                if (aPtr[j] != nullptr)
                {
                    //PByte p = MObject::MCDump(indexCrash, aPtr[j], stChunk);
                    //DEV_INFO(TF("===========%p[%d]---%p"), p, stChunk, aPtr[j]);

                    MObject::MCFree(indexCrash, aPtr[j]);
                    aPtr[j] = nullptr;
                    ++nTest;
                }
            }
            DEV_INFO(TF("===========222[%d]"), nTest);

            //for (Int i = 0; i < 257; ++i)
            //{
            //    Int nCount = nnn - i * 1024;
            //    CPlatform::SleepEx(3000);
            //    for (Int j = 0; j < nCount; ++j)
            //    {
            //        if (aPtr[j] != nullptr)
            //        {
            //            //PByte p = MObject::MCDump(indexCrash, aPtr[j], stChunk);
            //            //DEV_INFO(TF("===========%p[%d]---%p"), p, stChunk, aPtr[j]);

            //            MObject::MCFree(indexCrash, aPtr[j]);
            //            aPtr[j] = nullptr;
            //        }
            //    }

            //    CPlatform::SleepEx(3000);
            //    for (Int j = 0; j < nCount; ++j)
            //    {
            //        if (aPtr[j] == nullptr)
            //        {
            //            PByte pData = MObject::MCAlloc(indexCrash);
            //            if (pData != nullptr)
            //            {
            //                aPtr[j] = pData;
            //            }
            //        }
            //    }
            //}
            CPlatform::SleepEx(3000);
            MObject::MCDestroy(indexCrash);
        }
#endif
}


