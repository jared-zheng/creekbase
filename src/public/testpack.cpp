#include "stdafx.h"
#include "testpack.h"

namespace TEST 
{
    CNetworkEventPacket* CNetworkEventPacket::Create(uintptr_t, CStream& Stream, ULLong)
    {
        CNetworkEventPacket* pPkt = nullptr;

        //DEV_DEBUG(TF("CNetworkEventPacket::Create"));

        UInt uEvent = 0;
        Stream >> uEvent;
        switch (uEvent)
        {
        case TEST_EVENT_LIVE:
            {
                pPkt = MNEW CNetworkEventPacket(TEST_EVENT_LIVE);
            }
            break;
        case TEST_EVENT_LOGIN:
            {
                pPkt = MNEW CLoginPacket;
            }
            break;
        case TEST_EVENT_LOGINACK:
            {
                pPkt = MNEW CAckPacket(TEST_EVENT_LOGINACK);
            }
            break;
        case TEST_EVENT_CHAT:
            {
                pPkt = MNEW CChatPacket;
            }
            break;
        case TEST_EVENT_CHATACK:
            {
                pPkt = MNEW CChatPacketAck;
            }
            break;
        case TEST_EVENT_LOGOUT:
            {
                pPkt = MNEW CLogoutPacket;
            }
            break;
        case TEST_EVENT_LOGOUTACK:
            {
                pPkt = MNEW CAckPacket(TEST_EVENT_LOGOUTACK);
            }
            break;
        default:
            {
            }
        }
        if (pPkt != nullptr)
        {
            Stream.Seek(0);
            pPkt->Serialize(Stream);
            //DEV_DEBUG(TF("CNetworkEventPacket::Create::Serialize[%d]"), uEvent);
        }
        else
        {
            //DEV_DEBUG(TF("CNetworkEventPacket::Create::fail[%d]"), uEvent);
        }
        return pPkt;
    }
}


