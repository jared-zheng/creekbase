#ifndef __TEST_PACK_H__
#define __TEST_PACK_H__

#pragma once

#include "networkevent.h"

//#if (__ARCH_TARGET__ == ARCH_TARGET_64)
//    #define TEST_IPV6
//#endif
//#define BROADCAST_TEST

namespace TEST
{
using namespace CREEK;

enum TEST_EVENT
{
    TEST_EVENT_LIVE     = (CNETTraits::EVENT_RESERVED + 1),
    TEST_EVENT_LOGIN,
    TEST_EVENT_LOGINACK,

    TEST_EVENT_CHAT,
    TEST_EVENT_CHATACK,

    TEST_EVENT_LOGOUT,
    TEST_EVENT_LOGOUTACK,
};

enum TEST_SIZE
{
    TEST_SIZE_PAK = 64 * 1024,
};

// CNETPackBuildTest
class CNETPackBuildTest : public CNETTraits::CNETPackBuild
{
public:
    typedef struct tagPACK_HEAD
    {
        UInt    uFlag1;
        UInt    uFlag2;
        UInt    uFlag3;
        UInt    uSize;
    }PACK_HEAD, *PPACK_HEAD;
public:
    CNETPackBuildTest(void)
    {
        DEV_INFO(TF("TCP>>>>>>>>>Test PackBuild Created"));
    }
    virtual ~CNETPackBuildTest(void)
    {
        DEV_INFO(TF("TCP>>>>>>>>>Test PackBuild Destroyed"));
    }

    // send package head ptr, package data size
    // return package all size, include package head
    virtual size_t Build(PByte pPack, size_t stSize) OVERRIDE
    {
        PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
        pHead->uFlag1 = 0x11111111;
        pHead->uFlag2 = 0x22222222;
        pHead->uFlag3 = 0x33333333;
        pHead->uSize  = (UInt)(stSize + sizeof(PACK_HEAD));
        return (size_t)(pHead->uSize);
    }
    // check recv package valid check
    // return package all size, include package head
    // return 0 when check fail
    virtual size_t Check(PByte pPack, size_t) OVERRIDE
    {
        PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
        if (pHead->uFlag1 == 0x11111111 && pHead->uFlag2 == 0x22222222 && pHead->uFlag3 == 0x33333333)
        {
            return (size_t)(pHead->uSize);
        }
        return 0;
    }
    // package head size
    virtual size_t HeadSize(void) const OVERRIDE
    {
        return sizeof(PACK_HEAD);
    }
};

// CNetworkEventPacket
class CNetworkEventPacket : public CEventBase
{
public:
    static CNetworkEventPacket* Create(uintptr_t, CStream&, ULLong);
public:
    CNetworkEventPacket(UInt uEvent = 0);
    virtual ~CNetworkEventPacket(void);

    CNetworkEventPacket(const CNetworkEventPacket& aSrc);
    CNetworkEventPacket& operator=(const CNetworkEventPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetEvent(void);
    UInt   GetSize(void);

    void   AdjustSize(void);
private:
    UInt   m_uEvent;
    UInt   m_uSize;
};

// CAckPacket
class CAckPacket : public CNetworkEventPacket
{
public:
    CAckPacket(UInt uEvent = 0);
    virtual ~CAckPacket(void);

    CAckPacket(const CAckPacket& aSrc);
    CAckPacket& operator=(const CAckPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetAck(void);
    void   SetAck(UInt uAck);

    UInt   GetParam(void);
    void   SetParam(UInt uParam);
private:
    UInt   m_uAck;
    UInt   m_uParam;
};

// CLoginPacket
class CLoginPacket : public CNetworkEventPacket
{
public:
    CLoginPacket(UInt uId = 0, UInt uPass = 0);
    virtual ~CLoginPacket(void);

    CLoginPacket(const CLoginPacket& aSrc);
    CLoginPacket& operator=(const CLoginPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetId(void);
    void   SetId(UInt uId);

    UInt   GetPass(void);
    void   SetPass(UInt uPass);
private:
    UInt   m_uId;
    UInt   m_uPass;
};

// CChatPacket
class CChatPacket : public CNetworkEventPacket
{
public:
    CChatPacket(void);
    virtual ~CChatPacket(void);

    CChatPacket(const CChatPacket& aSrc);
    CChatPacket& operator=(const CChatPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt     GetId(void);
    void     SetId(UInt uId);

    CString& GetChat(void);
    void     SetChat(PCXStr pszChat);
public:
    UInt      m_uBufSize;
    PByte     m_pBuf;
private:
    UInt      m_uId;
    CString   m_strChat;
};

// CChatPacketAck
class CChatPacketAck : public CNetworkEventPacket
{
public:
    CChatPacketAck(void);
    virtual ~CChatPacketAck(void);

    CChatPacketAck(const CChatPacketAck& aSrc);
    CChatPacketAck& operator=(const CChatPacketAck& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetId(void);
    void   SetId(UInt uId);

    PByte  GetData(UInt& uLen);
    void   SetData(PByte pData, UInt uLen);
private:
    UInt     m_uId;
    UInt     m_uLen;
    PByte    m_pData;
};

// CLogoutPacket
class CLogoutPacket : public CNetworkEventPacket
{
public:
    CLogoutPacket(UInt uId = 0);
    virtual ~CLogoutPacket(void);

    CLogoutPacket(const CLogoutPacket& aSrc);
    CLogoutPacket& operator=(const CLogoutPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetId(void);
    void   SetId(UInt uId);
private:
    UInt   m_uId;
};

#include "testpack.inl"
}

#endif // __TEST_PACK_H__
