#ifndef __TEST_PACK_INL__
#define __TEST_PACK_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CNetworkEventPacket
INLINE CNetworkEventPacket::CNetworkEventPacket(UInt uEvent)
: m_uEvent(uEvent)
, m_uSize(0)
{
}

INLINE CNetworkEventPacket::~CNetworkEventPacket(void)
{
}

INLINE CNetworkEventPacket::CNetworkEventPacket(const CNetworkEventPacket& aSrc)
: m_uEvent(aSrc.m_uEvent)
, m_uSize(aSrc.m_uSize)
{
}

INLINE CNetworkEventPacket& CNetworkEventPacket::operator=(const CNetworkEventPacket& aSrc)
{
    if (&aSrc != this)
    {
        m_uEvent = aSrc.m_uEvent;
        m_uSize  = aSrc.m_uSize;
    }
    return (*this);
}

INLINE size_t CNetworkEventPacket::Length(void) const
{
    return (sizeof(UInt) + sizeof(UInt));
}

INLINE void CNetworkEventPacket::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> m_uEvent >> m_uSize;
    }
    else
    {
        Stream << m_uEvent << m_uSize;
    }
}

INLINE UInt CNetworkEventPacket::GetEvent(void)
{
    return m_uEvent;
}

INLINE UInt CNetworkEventPacket::GetSize(void)
{
    return m_uSize;
}

INLINE void CNetworkEventPacket::AdjustSize(void)
{
    m_uSize = (UInt)Length();
}

///////////////////////////////////////////////////////////////////
// CAckPacket
INLINE CAckPacket::CAckPacket(UInt uEvent)
: CNetworkEventPacket(uEvent)
, m_uAck(0)
, m_uParam(0)
{
}

INLINE CAckPacket::~CAckPacket(void)
{
}

INLINE CAckPacket::CAckPacket(const CAckPacket& aSrc)
: CNetworkEventPacket(aSrc)
, m_uAck(aSrc.m_uAck)
, m_uParam(aSrc.m_uParam)
{
}

INLINE CAckPacket& CAckPacket::operator=(const CAckPacket& aSrc)
{
    if (&aSrc != this)
    {
        CNetworkEventPacket::operator=(aSrc);
        m_uAck   = aSrc.m_uAck;
        m_uParam = aSrc.m_uParam;
    }
    return (*this);
}

INLINE size_t CAckPacket::Length(void) const
{
    return (sizeof(UInt) + sizeof(UInt) + CNetworkEventPacket::Length());
}

INLINE void CAckPacket::Serialize(CStream& Stream)
{
    CNetworkEventPacket::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uAck >> m_uParam;
    }
    else
    {
        Stream << m_uAck << m_uParam;
    }
}

INLINE UInt CAckPacket::GetAck(void)
{
    return m_uAck;
}

INLINE void CAckPacket::SetAck(UInt ulAck)
{
    m_uAck = ulAck;
}

INLINE UInt CAckPacket::GetParam(void)
{
    return m_uParam;
}

INLINE void CAckPacket::SetParam(UInt ulParam)
{
    m_uParam = ulParam;
}

///////////////////////////////////////////////////////////////////
// CLoginPacket
INLINE CLoginPacket::CLoginPacket(UInt uId, UInt uPass)
: CNetworkEventPacket(TEST_EVENT_LOGIN)
, m_uId(uId)
, m_uPass(uPass)
{
}

INLINE CLoginPacket::~CLoginPacket(void)
{
}

INLINE CLoginPacket::CLoginPacket(const CLoginPacket& aSrc)
: CNetworkEventPacket(aSrc)
, m_uId(aSrc.m_uId)
, m_uPass(aSrc.m_uPass)
{
}

INLINE CLoginPacket& CLoginPacket::operator=(const CLoginPacket& aSrc)
{
    if (&aSrc != this)
    {
        CNetworkEventPacket::operator=(aSrc);
        m_uId   = aSrc.m_uId;
        m_uPass = aSrc.m_uPass;
    }
    return (*this);
}

INLINE size_t CLoginPacket::Length(void) const
{
    return (sizeof(UInt) + sizeof(UInt) + CNetworkEventPacket::Length());
}

INLINE void CLoginPacket::Serialize(CStream& Stream)
{
    CNetworkEventPacket::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uId >> m_uPass;
    }
    else
    {
        Stream << m_uId << m_uPass;
    }
    //DEV_DEBUG(TF("CLoginPacket::Serialize"));
}

INLINE UInt CLoginPacket::GetId(void)
{
    return m_uId;
}

INLINE void CLoginPacket::SetId(UInt uId)
{
    m_uId = uId;
}

INLINE UInt CLoginPacket::GetPass(void)
{
    return m_uPass;
}

INLINE void CLoginPacket::SetPass(UInt uPass)
{
    m_uPass = uPass;
}

///////////////////////////////////////////////////////////////////
// CChatPacket
INLINE CChatPacket::CChatPacket(void)
: CNetworkEventPacket(TEST_EVENT_CHAT)
, m_uBufSize(0)
, m_pBuf(nullptr)
, m_uId(0)
{
}

INLINE CChatPacket::~CChatPacket(void)
{
}

INLINE CChatPacket::CChatPacket(const CChatPacket& aSrc)
: CNetworkEventPacket(aSrc)
, m_uBufSize(aSrc.m_uBufSize)
, m_pBuf(aSrc.m_pBuf)
, m_uId(aSrc.m_uId)
, m_strChat(aSrc.m_strChat)
{
}

INLINE CChatPacket& CChatPacket::operator=(const CChatPacket& aSrc)
{
    if (&aSrc != this)
    {
        CNetworkEventPacket::operator=(aSrc);
        m_uBufSize = aSrc.m_uBufSize;
        m_pBuf     = aSrc.m_pBuf;
        m_uId      = aSrc.m_uId;
        m_strChat  = aSrc.m_strChat;
    }
    return (*this);
}

INLINE size_t CChatPacket::Length(void) const
{
    return (sizeof(UInt) + sizeof(UInt) + m_uBufSize + m_strChat.Length(true) + CNetworkEventPacket::Length());
}

INLINE void CChatPacket::Serialize(CStream& Stream)
{
    CNetworkEventPacket::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uId >> m_strChat >> m_uBufSize;
    }
    else
    {
        Stream << m_uId << m_strChat << m_uBufSize;
    }
    //DEV_DEBUG(TF("CChatPacket::Serialize"));
}

INLINE UInt CChatPacket::GetId(void)
{
    return m_uId;
}

INLINE void CChatPacket::SetId(UInt uId)
{
    m_uId = uId;
}

INLINE CString& CChatPacket::GetChat(void)
{
    return m_strChat;
}

INLINE void CChatPacket::SetChat(PCXStr pszChat)
{
    m_strChat = pszChat;
}

///////////////////////////////////////////////////////////////////
// CChatPacketAck
INLINE CChatPacketAck::CChatPacketAck(void)
: CNetworkEventPacket(TEST_EVENT_CHATACK)
, m_uId(0)
, m_uLen(0)
, m_pData(nullptr)
{
}

INLINE CChatPacketAck::~CChatPacketAck(void)
{
}

INLINE CChatPacketAck::CChatPacketAck(const CChatPacketAck& aSrc)
: CNetworkEventPacket(aSrc)
, m_uId(aSrc.m_uId)
, m_uLen(aSrc.m_uLen)
, m_pData(aSrc.m_pData)
{
}

INLINE CChatPacketAck& CChatPacketAck::operator=(const CChatPacketAck& aSrc)
{
    if (&aSrc != this)
    {
        CNetworkEventPacket::operator=(aSrc);
        m_uId   = aSrc.m_uId;
        m_uLen  = aSrc.m_uLen;
        m_pData = aSrc.m_pData;
    }
    return (*this);
}

INLINE size_t CChatPacketAck::Length(void) const
{
    return (sizeof(UInt) + sizeof(UInt) + m_uLen * sizeof(Byte) + CNetworkEventPacket::Length());
}

INLINE void CChatPacketAck::Serialize(CStream& Stream)
{
    CNetworkEventPacket::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uId >> m_uLen;
        //Stream.Read(
    }
    else
    {
        Stream << m_uId << m_uLen;
        if (m_uLen > 0)
        {
            Stream.Write(m_pData, m_uLen);
        }
    }
}

INLINE UInt CChatPacketAck::GetId(void)
{
    return m_uId;
}

INLINE void CChatPacketAck::SetId(UInt uId)
{
    m_uId = uId;
}

INLINE PByte CChatPacketAck::GetData(UInt& uLen)
{
    uLen = m_uLen;
    return m_pData;
}

INLINE void CChatPacketAck::SetData(PByte pData, UInt uLen)
{
    m_pData = pData;
    m_uLen  = uLen;
}

///////////////////////////////////////////////////////////////////
// CLogoutPacket
INLINE CLogoutPacket::CLogoutPacket(UInt uId)
: CNetworkEventPacket(TEST_EVENT_LOGOUT)
, m_uId(uId)
{
}

INLINE CLogoutPacket::~CLogoutPacket(void)
{
}

INLINE CLogoutPacket::CLogoutPacket(const CLogoutPacket& aSrc)
: CNetworkEventPacket(aSrc)
, m_uId(aSrc.m_uId)
{
}

INLINE CLogoutPacket& CLogoutPacket::operator=(const CLogoutPacket& aSrc)
{
    if (&aSrc != this)
    {
        CNetworkEventPacket::operator=(aSrc);
        m_uId   = aSrc.m_uId;
    }
    return (*this);
}

INLINE size_t CLogoutPacket::Length(void) const
{
    return (sizeof(UInt) + CNetworkEventPacket::Length());
}

INLINE void CLogoutPacket::Serialize(CStream& Stream)
{
    CNetworkEventPacket::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uId;
    }
    else
    {
        Stream << m_uId;
    }
}

INLINE UInt CLogoutPacket::GetId(void)
{
    return m_uId;
}

INLINE void CLogoutPacket::SetId(UInt uId)
{
    m_uId = uId;
}

#endif // __TEST_PACK_INL__