#include "stdafx.h"
#include "testpack.h"
#include "testnetwork.h"
#include "networkhttp.h"

#include "streamfile.h"

using namespace TEST;

typedef CTNetworkEventHandler<CTNETDispatch<CNetworkEventPacket, false>> CTestNetworkEventHandler;

DECLARE_GLOBAL_LOADER( NetworkSystem )

//Byte gs_Data[5000];
static Int gs_nBegin = -1;
static Int gs_nCount = 0;

class CTestClient : public CTestNetworkEventHandler
{
    //DECLARE_CLASS_LOADER( CTestClient, NetworkSystem )
public:
    enum CLIENT_CONST
    {
#ifdef _DEBUG
#ifdef TEST_IPV6
        SERVER_CONST_PORT = 9906,
#else
        SERVER_CONST_PORT = 9903,
#endif
#else
#ifdef TEST_IPV6
        SERVER_CONST_PORT = 9907,
#else
        SERVER_CONST_PORT = 9904,
#endif
#endif
    };

    enum CLIENT_LIVE
    {
        CLIENT_LIVE_TIMER_ID  = 10001,
        CLIENT_LIVE_TIMER_VAL = 200,

        CLIENT_LIVE_MAX_TICK  = 100,
        CLIENT_LIVE_OUT_TICK  = 10,
    };

    enum CLIENT_STATE
    {
        CLIENT_STATE_NONE = 0,
        CLIENT_STATE_LOGIN,
        CLIENT_STATE_CHAT,
        CLIENT_STATE_LOGOUT,
    };
public:
    CTestClient(void) : m_bLive(false), m_bUDP(true), m_nState(CLIENT_STATE_NONE), m_nCount(0), m_sUDPTest(0), m_sConnect(0)
    {
    }

    virtual ~CTestClient(void)
    {
    }

    bool Init(void)
    {
        LOADER_GLOBAL_CREEATE( NetworkSystem, UUID_OF( CNetwork ), m_NetworkPtr.Cast<CComponent>());
        //LOADER_CLASS_CREEATE( CTestClient, NetworkSystem, UUID_OF( CNetwork ), m_NetworkPtr.Cast<CComponent>());
        if (m_NetworkPtr != nullptr)
        {
            CNETTraits::NET_ATTR attr;
#ifdef TEST_IPV6
            attr.nAttrs  = CNETTraits::ATTR_THREAD | CNETTraits::ATTR_IPV6;// | CNETTraits::ATTR_MAX_JUMBOBUF;
#else
            attr.nAttrs = CNETTraits::ATTR_THREAD;// | CNETTraits::ATTR_MAX_JUMBOBUF;
#endif
            attr.nThread = 2;
            //attr.nMaxJumbo = 112 + TEST_SIZE_PAK;
#ifdef TEST_IPV6
            if (m_NetworkPtr->Init(attr, this, MNEW CNETPackBuildTest) == RET_OKAY)
#else
            if (m_NetworkPtr->Init(attr, this) == RET_OKAY)
#endif
            {
                //CString strIp;
                //for (Int i = 0; i < 8; ++i)
                //{
                //    strIp.Format(TF("whds%d.xiaowangzu.net"), i);

                //    CNETTraits::ARY_STRING strAddrs;
                //    if (m_NetworkPtr->GetRemoteAddr(*strIp, strAddrs, CNETTraits::ATTR_IPV4))
                //    {
                //        for (Int j = 0; j < strAddrs.GetSize(); ++j)
                //        {
                //            DEV_DUMP(TF("%s %dth---IP:%s"), *strIp, j, *(strAddrs[j]));
                //        }
                //    }
                //}

                m_sUDPTest = m_NetworkPtr->Create(0, nullptr, SOCKET_UDP_BROADCAST);
                m_NetworkPtr->SetAttr(m_sUDPTest, TRUE, SOCKET_RECV_EVENT);
                //m_nCount = 10;
                Connect2Server();
#ifndef BROADCAST_TEST
                CEventQueue::CreateTickEvent(CLIENT_LIVE_TIMER_ID, CLIENT_LIVE_TIMER_VAL, *this);
#endif
                return true;
            }
        }
        return false;
    }

    void Exit(void)
    {
#ifndef BROADCAST_TEST
        CEventQueue::DestroyTickEvent(CLIENT_LIVE_TIMER_ID);
#endif
        if (m_sUDPTest != 0)
        {
            m_NetworkPtr->Destroy(m_sUDPTest);
            m_sUDPTest = 0;
        }
        if (m_sConnect != 0)
        {
            m_NetworkPtr->Destroy(m_sConnect);
            m_sConnect = 0;
        }
        if (m_NetworkPtr != nullptr)
        {
            m_NetworkPtr->Exit();
            m_NetworkPtr = nullptr;
        }
        m_bLive = false;
    }

    void Wait(void)
    {
        m_Event.Wait(10000);
    }

#ifndef BROADCAST_TEST
    virtual UInt OnHandle(uintptr_t utEvent, UInt)
    {
        if (utEvent == CLIENT_LIVE_TIMER_ID)
        {
            if (Connect2Server())
            {
                m_NetworkPtr->Check(1);
                switch (m_nState)
                {
                case CLIENT_STATE_LOGIN:
                    {
                        if (m_nCount < CLIENT_LIVE_MAX_TICK)
                        {
                            if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                            {
                                DEV_INFO(TF("TCP>>>>>>>>>Login ready is %d"), m_nCount);
                            }
                            CLoginPacket Login;
                            Login.SetId(m_nCount);
                            Login.SetPass(CPlatform::GetCurrentPId());
                            Login.AdjustSize();

                            m_NetworkPtr->Send(m_sConnect, Login);
                        }
                    }
                    break;
                case CLIENT_STATE_CHAT:
                    {
                        CChatPacket Chat;
                        //Chat.m_pBuf = gs_Data;
                        //Chat.m_uBufSize = 5000;
                        Chat.SetId(m_nCount);
                        Chat.GetChat().Format(TF("%dth k;lsdjfs;dfsa;jdfjasd;fiwre&^*(W$)O)_S<MXNC<ZXHCLJHDlkasjd"), CPlatform::GetCurrentPId());
                        Chat.AdjustSize();

                        //CStreamScopePtr StreamPtr;
                        //m_NetworkPtr->AllocJumboBuffer(StreamPtr);
                        //if (StreamPtr != nullptr)
                        //{
                        //    Chat.Serialize(*StreamPtr);
                        //    StreamPtr->Write(gs_Data, 5000);
                        //    m_NetworkPtr->Send(m_sConnect, *StreamPtr);
                        //}
                        for (Int i = 0; i < 5; ++i)
                        {
                            m_NetworkPtr->Send(m_sConnect, Chat, SEND_WAIT);
                        }
                        m_NetworkPtr->Send(m_sConnect, Chat);
                        if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                        {
                            DEV_INFO(TF("TCP>>>>>>>>>Chat %d"), m_nCount);
                        }
                    }
                    break;
                case CLIENT_STATE_LOGOUT:
                    {
                        CLogoutPacket Logout;
                        Logout.SetId(m_nCount);
                        Logout.AdjustSize();

                        m_NetworkPtr->Send(m_sConnect, Logout);

                        if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                        {
                            DEV_INFO(TF("TCP>>>>>>>>>Logout %d"), m_nCount);
                        }
                    }
                    break;
                default:
                    {
                    }
                }
            }

            if (m_bUDP)
            {
                if (m_sUDPTest == 0)
                {
                    m_sUDPTest = m_NetworkPtr->Create(0, nullptr, SOCKET_UDP_BROADCAST);
                    m_NetworkPtr->SetAttr(m_sUDPTest, TRUE, SOCKET_RECV_EVENT);
                }
                if (m_sUDPTest != 0)
                {
                    if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                    {
                        DEV_INFO(TF("UDP<<<<<<<<<Login ready is %d"), m_nCount);
                    }

                    CLoginPacket Login;
                    Login.SetId(m_nCount);
                    Login.SetPass(m_nCount);
                    Login.AdjustSize();
#ifdef TEST_IPV6
                    m_NetworkPtr->SendTo(m_sUDPTest, Login, TF("::1"), SERVER_CONST_PORT);
#else
                    m_NetworkPtr->SendTo(m_sUDPTest, Login, TF("127.0.0.1"), SERVER_CONST_PORT);
#endif
                }
            }
        }
        return RET_OKAY;
    }
#endif
protected:
    virtual bool OnTcpDispatch(const PacketPtr& PktPtr, PTCP_PARAM pTcp)
    {
        assert(pTcp->sSocket == m_sConnect);
        switch (PktPtr->GetEvent())
        {
#ifdef BROADCAST_TEST
        case TEST_EVENT_CHATACK:
            {
                CChatPacketAck* pAckPkt = static_cast<CChatPacketAck*>(PktPtr.Get());
                m_nCount = pAckPkt->GetId();
                if (gs_nBegin == -1)
                {
                    gs_nBegin = m_nCount;
                }
                ++gs_nCount;
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Chat ack is %d"), m_nCount);
                }
                if (m_nCount < CLIENT_LIVE_MAX_TICK)
                {
                    CChatPacket Chat;
                    Chat.SetId(CPlatform::GetCurrentPId());
                    Chat.GetChat().Format(TF("[[[%dth]]]˼���ķ����ֶ����ֶ�����¼�ٶȺͷ⽨ʱ��dksfj������"), m_nCount);
                    Chat.AdjustSize();
                    m_NetworkPtr->Send(m_sConnect, Chat);

                    if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                    {
                        DEV_INFO(TF("UDP<<<<<<<<<Login ready is %d"), m_nCount);
                    }
                    CLoginPacket Login;
                    Login.SetId(m_nCount);
                    Login.SetPass(m_nCount);
                    Login.AdjustSize();
#ifdef TEST_IPV6
                    m_NetworkPtr->SendTo(m_sUDPTest, Login, TF("::1"), SERVER_CONST_PORT);
#else
                    m_NetworkPtr->SendTo(m_sUDPTest, Login, TF("127.0.0.1"), SERVER_CONST_PORT);
#endif
                }
                else
                {
                    DEV_INFO(TF("Ready to exit %d---%d"), gs_nBegin, gs_nCount);
                    m_Event.Signal();
                }
            }
            break;
#else
        case TEST_EVENT_LOGINACK:
            {
                CAckPacket* pAckPkt = static_cast<CAckPacket*>(PktPtr.Get());
                if (pAckPkt->GetAck() == RET_OKAY)
                {
                    m_nState = CLIENT_STATE_CHAT;
                }
            }
            break;
        case TEST_EVENT_CHATACK:
            {
                //CChatPacketAck* pAckPkt = static_cast<CChatPacketAck*>(PktPtr.Get());
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Chat ack is %d"), m_nCount);
                }
                m_nState = CLIENT_STATE_LOGOUT;
            }
            break;
        case TEST_EVENT_LOGOUTACK:
            {
                //CAckPacket* pAckPkt = static_cast<CAckPacket*>(PktPtr.Get());
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Logout ack is %d"), m_nCount);
                }
                m_nState = CLIENT_STATE_LOGIN;
                ++m_nCount;

                if (m_nCount == CLIENT_LIVE_MAX_TICK)
                {
                    DEV_INFO(TF("Ready to exit"));
                    m_bUDP = false;
                    m_Event.Signal();
                }
            }
            break;
#endif
        default:
            {
                // live-packet
            }
        }
        return true;
    }

    virtual bool OnUdpDispatch(const PacketPtr& PktPtr, PUDP_PARAM pUdp)
    {
        assert(pUdp->sSocket == m_sUDPTest);
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_LOGINACK:
            {
                //CAckPacket* pAckPkt = static_cast<CAckPacket*>(PktPtr.Get());
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    CString strAddr;
                    UShort  usPort = 0;
                    m_NetworkPtr->TranslateAddr(strAddr, usPort, pUdp->NetAddr, false);
                    DEV_INFO(TF("$$$$$$$$$Login ack is [%d], [%s]:%d"), m_nCount, *strAddr, usPort);
                }
            }
            break;
        default:
            {
            }
        }
        return true;
    }

    virtual bool OnTcpAccept(Socket sAccept, Socket sListen)
    {
        DEV_INFO(TF("###Accept socket=%p, listen socket %p"), sAccept, sListen);
        return true;
    }

    virtual bool OnTcpConnect(UInt uError, Socket sConnect)
    {
        DEV_INFO(TF("###Connect return errid=%d, socket %p"), uError, sConnect);
        if (uError == 0)
        {
            assert(sConnect == m_sConnect);
            m_nState = CLIENT_STATE_LOGIN;

           // CNETTraits::CStreamScopePtr StreamPtr;
           // m_NetworkPtr->AllocBuffer(StreamPtr);
           //// CHTTP::Post(*StreamPtr, TF("192.168.100.76"), nullptr, TF("/api/sendSmsCode?phone=18133333333&verifyCode="));
           // //CHTTP::Get(*StreamPtr, TF("192.168.100.76"), nullptr, TF("/api/sys/news/selectNewsList?current=1&size=5"));

           // //CHTTP::Get(*StreamPtr, TF("192.168.100.76"), nullptr, TF("/api/user/selectOtherUserInfo?uuid=90f9de00-6669-4bba-8fb9-3ea760453314"));
           //

           // CHTTP::Post(*StreamPtr, TF("192.168.100.76"), nullptr, TF("/api/sendSmsByPassport?passportName=maizi007&password=abc123&verifycode="));
           // m_NetworkPtr->Send(m_sConnect, *StreamPtr);

        }
        else
        {
            m_bLive = false;
        }
        return true;
    }

    virtual bool OnTcpRecv(size_t stSize, PTCP_PARAM pTcp)
    {
        assert(pTcp->sSocket == m_sConnect);
        ////CHTTPTraits::COOKIE Cookie;
        ////bool    bOkay = false;
        ////
        ////PByte  pData  = (PByte)pTcp->pData;
        ////size_t stData = stSize;
        ////if (CChar::Cmpin((PStr)pData, "0\r\n\r\n", 5) == 0)
        ////{
        ////}
        ////else
        ////{
        ////    m_Res.Parse(pData, stData);
        ////    if (m_Res.IsContentComplete())
        ////    {
        ////        PCStr  pszUtf8 = (PCStr)m_Res.GetContent().GetBuf();
        ////        size_t stSize  = m_Res.GetContent().Size();
        ////
        ////        CBufReadStream WBuf((stSize + 1) * sizeof(WChar));
        ////        PWStr pszW = (PWStr)WBuf.GetBuf();
        ////
        ////        CBufReadStream Out;
        ////        Out.Attach((stSize + 1) * 4);
        ////        PStr psz = (PStr)Out.GetBuf();
        ////
        ////        Int nRet = CXChar::Convert(pszUtf8, (Int)stSize, pszW, (Int)(stSize), CP_UTF8);
        ////        assert(nRet > 0);
        ////        pszW[nRet] = 0;
        ////
        ////        nRet = CXChar::Convert(pszW, nRet, psz, (Int)(stSize * 4));
        ////        assert(nRet > 0);
        ////        psz[nRet] = 0;
        ////        DEV_INFO(psz);
        ////
        ////
        ////        CHTTPTraits::PAIR_COOKIE* pPair = m_Res.Cookie().Cookie().GetFirst();
        ////        if (pPair != nullptr)
        ////        {
        ////            Cookie = pPair->m_V;
        ////        }
        ////        m_Res.Close();
        ////        bOkay = true;
        ////    }
        ////}
        ////if (bOkay)
        ////{
        ////    switch (m_nState)
        ////    {
        ////    case CLIENT_STATE_LOGIN:
        ////        {
        ////            ++m_nState;
        ////            CHTTPRequest Req;
        ////            Req.SetMethod("POST");
        ////            Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(TF("/api/loginByPhone?phone=18133333333&smsCode=123456"));
        ////            Req.SetPath(TF("/api/loginByPassport?passportName=maizi007&password=abc123&smsCode=123456"));
        ////
        ////            m_strCookie.Format(TF("%s=%s"), *Cookie.strName, *Cookie.strValue);
        ////            Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            CNETTraits::CStreamScopePtr StreamPtr;
        ////            m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            CHTTP::Serialize(*StreamPtr, Req);
        ////            m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////        }
        ////        break;
        ////    case 2:
        ////        {
        ////            ++m_nState;
        ////
        ////            ////es550940633333
        ////            //CHTTPRequest Req;
        ////            //Req.SetMethod("GET");
        ////            //Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(TF("/api/sys/notify/selectUserNotify?current=1&size=5&isRead=0"));
        ////
        ////            //Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            //CNETTraits::CStreamScopePtr StreamPtr;
        ////            //m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            //CHTTP::Serialize(*StreamPtr, Req);
        ////            //m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////
        ////            //CHTTPRequest Req;
        ////            //Req.SetMethod("GET");
        ////            //Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(TF("/api/gameBind?gameId=1&playerName=Test__ME"));
        ////
        ////            //Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            //CNETTraits::CStreamScopePtr StreamPtr;
        ////            //m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            //CHTTP::Serialize(*StreamPtr, Req);
        ////            //m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////
        ////            //CTime tCurr(2018, 11, 14, 12, 25, 0);
        ////            //CString strParam;
        ////            //strParam.Format(TF("/api/match/insertMatch?roomPwd=122331&gameId=1&roomBeginDate=%lld000&dataType=2&enrollMoney=10&predictMoney=120&patternId=1&gameMap=1&firstMoney=80&gameServerNo=pc-as"), tCurr.GetTime());
        ////
        ////            //CHTTPRequest Req;
        ////            //Req.SetMethod("POST");
        ////            //Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(*strParam);
        ////
        ////            //Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            //CNETTraits::CStreamScopePtr StreamPtr;
        ////            //m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            //CHTTP::Serialize(*StreamPtr, Req);
        ////            //m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////
        ////
        ////            //es550940633333
        ////            CHTTPRequest Req;
        ////            Req.SetMethod("POST");
        ////            Req.SetHost(TF("192.168.100.76"));
        ////            Req.SetPath(TF("/api/match/insertRoomPassPort?roomId=00000036"));
        ////
        ////            Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            CNETTraits::CStreamScopePtr StreamPtr;
        ////            m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            CHTTP::Serialize(*StreamPtr, Req);
        ////            m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////
        ////
        ////            //CHTTPRequest Req;
        ////            //Req.SetMethod("POST");
        ////            //Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(TF("/api/match/passportQuitRoom?roomId=00000015"));
        ////
        ////            //Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            //CNETTraits::CStreamScopePtr StreamPtr;
        ////            //m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            //CHTTP::Serialize(*StreamPtr, Req);
        ////            //m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////
        ////            //CHTTPRequest Req;
        ////            //Req.SetMethod("POST");
        ////            //Req.SetHost(TF("192.168.100.76"));
        ////            //Req.SetPath(TF("/api/match/cancelRoom?roomId=00000015"));
        ////
        ////            //Req.Header().SetAt(TF("Cookie"), *m_strCookie);
        ////
        ////            //CNETTraits::CStreamScopePtr StreamPtr;
        ////            //m_NetworkPtr->AllocBuffer(StreamPtr);
        ////            //CHTTP::Serialize(*StreamPtr, Req);
        ////            //m_NetworkPtr->Send(m_sConnect, *StreamPtr);
        ////        }
        ////    case 3:
        ////        {
        ////            ++m_nState;
        ////        }
        ////    }
        ////}
        CNETPackBuildTest::PPACK_HEAD pHead = (CNETPackBuildTest::PPACK_HEAD)pTcp->pData;
        PUInt pEvent = (PUInt)(pHead + 1);

        //PUInt pEvent = (PUInt)pTcp->pData;

        switch (*pEvent)
        {
        case TEST_EVENT_CHATACK:
            {
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########222Chat ack is"));
                }
                m_nState = CLIENT_STATE_LOGOUT;
            }
            break;
        case TEST_EVENT_LOGINACK:
            {
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########222Login ack is %d"));
                }
                m_nState = CLIENT_STATE_CHAT;
            }
            break;
        case TEST_EVENT_LOGOUTACK:
            {
                if (pTcp->pCache != nullptr)
                {
                    MCFree(pTcp->index, pTcp->pCache);
                    pTcp->pCache = nullptr;
                }
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########222Logout ack is"));
                }
                ++m_nCount;
                m_nState = CLIENT_STATE_LOGIN;
                if (m_nCount >= CLIENT_LIVE_MAX_TICK)
                {
                    DEV_INFO(TF("Ready to exit"));
                    //m_bUDP = false;
                    //m_Event.Signal();
                    if (m_sConnect != 0)
                    {
                        m_NetworkPtr->Destroy(m_sConnect);
                    }
                    if (m_sUDPTest != 0)
                    {
                        m_NetworkPtr->Destroy(m_sUDPTest);
                    }
                }

            }
            break;
        default:
            {
                // live-packet
            }
        }
        return true;
    }

    virtual bool OnTcpSend(UInt uQueueLimit, Socket sSocket)
    {
        if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
        {
            DEV_INFO(TF("###Send return queue=%d, socket %p"), uQueueLimit, sSocket);
        }
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        //if (sSocket == m_sConnect)
        {
            m_sConnect = 0;
            m_bLive    = false;
            m_nState   = CLIENT_STATE_NONE;
//#ifndef BROADCAST_TEST
            m_nCount   = 0;
//#endif
        }
        return true;
    }

    virtual bool OnUdpRecv(size_t stSize, PUDP_PARAM pUdp)
    {
        assert(pUdp->sSocket == m_sUDPTest);
        PUInt pEvent = (PUInt)pUdp->pData;
        switch (*pEvent)
        {
        case TEST_EVENT_LOGINACK:
            {
                if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
                {
                    CString strAddr;
                    UShort  usPort = 0;
                    m_NetworkPtr->TranslateAddr(strAddr, usPort, pUdp->NetAddr, false);
                    DEV_INFO(TF("$$$$$$$$$222Login ack is, [%s]:%d"), *strAddr, usPort);
                }
            }
            break;
        default:
            {
            }
        }
        return true;
    }

    virtual bool OnUdpSend(UInt uQueueLimit, Socket sSocket)
    {
        if ((m_nCount % CLIENT_LIVE_OUT_TICK) == 0)
        {
            DEV_INFO(TF("$$$Send return queue=%d, socket %p"), uQueueLimit, sSocket);
        }
        return true;
    }

    virtual bool OnUdpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("$$$Close udp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == m_sUDPTest)
        {
            m_sUDPTest = 0;
        }
        return true;
    }

    bool Connect2Server(void)
    {
        if (m_bLive == false)
        {
            if (m_sConnect == 0)
            {
                m_sConnect = m_NetworkPtr->Create(0, TF(""));
            }
            m_NetworkPtr->SetAttr(m_sConnect, TRUE, CNETTraits::SOCKET_RECV_EVENT);

            //m_NetworkPtr->SetAttr(m_sConnect, TRUE, CNETTraits::SOCKET_EXCLUDE_HEAD);
            //m_NetworkPtr->Connect(m_sConnect, 80, TF("192.168.100.76"));

            //CString str = TF("abc#123");
            //CCString str1;
            //CHTTPTraits::URLEncodeLocalToUTF8(str, str1);
            //DEV_INFO(TF("%s --- %s"), *str, *str1);

            //CString str2 = TF("��:�� ��");
            //CCString str3;
            //CHTTPTraits::URLEncodeLocalToUTF8(str2, str3);
            //DEV_INFO(TF("%s --- %s"), *str2, *str3);

#ifdef TEST_IPV6
            m_bLive = m_NetworkPtr->Connect(m_sConnect, SERVER_CONST_PORT, TF("localhost"));
#else
            m_bLive = m_NetworkPtr->Connect(m_sConnect, SERVER_CONST_PORT, TF("localhost"));
#endif
        }
        return m_bLive;
    }
private:
    bool             m_bLive;
    bool             m_bUDP;
    Int              m_nState;
    Int              m_nCount;
    Socket           m_sUDPTest;
    Socket           m_sConnect;
    CNetworkPtr      m_NetworkPtr;
    CSyncEvent       m_Event;
    CHTTPResponse    m_Res;
    CString          m_strCookie;
};

CTestClient* g_Clt = nullptr;

IMPLEMENT_GLOBAL_LOADER( NetworkSystem )
//IMPLEMENT_CLASS_LOADER( CTestClient, NetworkSystem )

void TestNetwork(void)
{
    //CString str = "{\"gold\":0,\"diamond\":0,\"phone\":\"18106525499\",\"level\":1,\"nickName\":\"2412412\",\"logo\":\"https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_86d58ae1.png\",\"team\":\"\",\"winRate\":0.0,\"uuid\":\"5f661ac9-44b4-4fa7-a9b4-4930a3a7329c\",\"passportName\":\"maizi007\",\"canCreateMatch\":true}";
    //CBufReadStream brs(str.Length(), (PByte)str.GetBuffer());
    //CBufWriteStream bws(str.Length(), (PByte)str.GetBuffer());

    //CString strName;
    //CString strValue;

    //CKVStore kv(ENT_LOCAL);
    //CKVNode  Root;
    //CKVNode  User;
    //if (kv.Load(Root, brs))
    //{
    //    for (PINDEX index = Root.First(); index != nullptr; )
    //    {
    //        Root.Next(strName, index);
    //        Root[*strName].GetString(strValue);

    //        if (strName == "passportName")
    //        {
    //            DEV_INFO(*strValue);
    //            //pUser->m_V.strPassport = *strValue;
    //            //pUser->m_V.index = m_UserIndex.Add(pUser->m_V.strPassport, reinterpret_cast<PINDEX>(pUser));
    //        }
    //        else
    //        {
    //            User[*strName].SetValue(*strValue);
    //        }
    //    }
    //    kv.Save(User, bws, false, ENT_LOCAL);
    //    str.ResetLength(bws.Tell());
    //    DEV_INFO(*str);
    //}

    //INIT_GLOBAL_LOADER( NetworkSystem, UUID_OF( CNetworkSystem ), TF("network-rio-Debug.dll"));
    INIT_GLOBAL_LOADER( NetworkSystem, UUID_OF( CNetworkSystem ), NETWORK_MODULE_NAME);

    CString strUUId;
    CSubSystem::MAP_COMPONENT_INFO Infos;
    (GET_GLOBAL_LOADER( NetworkSystem ))->GetComponentInfo(Infos);
    for (PINDEX index = Infos.GetFirstIndex(); index != nullptr; )
    {
        CSubSystem::PAIR_COMPONENT_INFO* pPair = Infos.GetNext(index);
        pPair->m_K.ToString(strUUId);
        DEV_INFO(TF("uuid = %s name = %s"), *strUUId, *(pPair->m_V));
    }

    DEV_INFO(TF("sub-system ptr = %p"), GET_GLOBAL_SUBSYSTEM(NetworkSystem ));

    CTestClient Client;
    g_Clt = &Client;
    if (Client.Init())
    {
        Client.Wait();
        DEV_INFO(TF("wait okay"));
        CPlatform::SleepEx(2000);
    }
    Client.Exit();
    g_Clt = nullptr;
    DUMP(nullptr);
    EXIT_GLOBAL_LOADER( NetworkSystem );
    //EXIT_CLASS_LOADER( CTestClient, NetworkSystem );
    DUMP(nullptr);

}
