#include "stdafx.h"
#include "testnetwork.h"
#include "test.h"

using namespace CREEK;

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#define _tmain   main

#endif

Int _tmain(void)
{
    CoreInit();

    TestNetwork();

    CoreExit();
    return 0;
}


