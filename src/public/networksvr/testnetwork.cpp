#include "stdafx.h"
#include "testpack.h"
#include "testnetwork.h"
#include "networkhttp.h"

#include "streamfile.h"

using namespace TEST;

typedef CTNetworkEventHandler<CTNETDispatch<CNetworkEventPacket, false> > CTestNetworkEventHandler;

class CTestServer : public CTestNetworkEventHandler
{
    DECLARE_CLASS_LOADER( CTestServer, NetworkSystem )
public:
    enum SERVER_CONST
    {
#ifdef _DEBUG
#ifdef TEST_IPV6
        SERVER_CONST_PORT = 9906,
#else
        SERVER_CONST_PORT = 9903,
#endif
#else
#ifdef TEST_IPV6
        SERVER_CONST_PORT = 9907,
#else
        SERVER_CONST_PORT = 9904,
#endif
#endif
    };

    enum SERVER_LIVE
    {
        SERVER_LIVE_TIMER_ID  = 10001,
        SERVER_LIVE_TIMER_VAL = 2000,

        SERVER_LIVE_MAX_TICK  = 10000,
        SERVER_LIVE_OUT_TICK  = 10,
    };

    CTestServer(void) : m_nCount(0), m_sUDPTest(0), m_sListen(0), m_pData(nullptr)
    {
    }

    virtual ~CTestServer(void)
    {
    }

    bool Init(void)
    {
        LOADER_CLASS_CREEATE( CTestServer, NetworkSystem, UUID_OF( CNetwork ), m_NetworkPtr.Cast<CComponent>());
        if (m_NetworkPtr != nullptr)
        {
            CNETTraits::NET_ATTR attr;
#ifdef TEST_IPV6
            attr.nAttrs = CNETTraits::ATTR_THREAD | CNETTraits::ATTR_IPV6 | CNETTraits::ATTR_TIMEOUT_DETECT;// | CNETTraits::ATTR_TIMEOUT_TIME | CNETTraits::ATTR_MAX_JUMBOBUF | CNETTraits::ATTR_ACK_DETECT;
#else
            attr.nAttrs = CNETTraits::ATTR_THREAD;// | CNETTraits::ATTR_TIMEOUT_DETECT;// | CNETTraits::ATTR_MAX_JUMBOBUF;
#endif
            attr.nThread  = 2;
           // attr.nTimeout = 2000;
            //attr.nMaxJumbo = 112 + TEST_SIZE_PAK;
#ifdef TEST_IPV6
            if (m_NetworkPtr->Init(attr, this, MNEW CNETPackBuildTest) == RET_OKAY)
#else
            if (m_NetworkPtr->Init(attr, this) == RET_OKAY)
#endif
            {
                //m_pData = (PByte)ALLOC( TEST_SIZE_PAK );

                CNETTraits::ARY_STRING strAddrs;
#ifdef TEST_IPV6
                if (m_NetworkPtr->GetLocalAddr(strAddrs))
#else
                if (m_NetworkPtr->GetLocalAddr(strAddrs, CNETTraits::ATTR_IPV4))
#endif
                {
                    for (Int i = 0; i < strAddrs.GetSize(); ++i)
                    {
                        DEV_INFO(TF(" *IP:%s"), *(strAddrs[i]));
                    }
                }
#ifdef BROADCAST_TEST
                CEventQueue::CreateTickEvent(SERVER_LIVE_TIMER_ID, SERVER_LIVE_TIMER_VAL, *this);
#endif
                m_sUDPTest = m_NetworkPtr->Create(SERVER_CONST_PORT, nullptr, SOCKET_UDP_BROADCAST);
                m_sListen = m_NetworkPtr->Create(SERVER_CONST_PORT, TF(""));
                if (m_sListen != 0)
                {
                    return m_NetworkPtr->Listen(m_sListen);
                }
            }
        }
        return false;
    }

    void Exit(void)
    {
#ifdef BROADCAST_TEST
        CEventQueue::DestroyTickEvent(SERVER_LIVE_TIMER_ID);
#endif
        if (m_sUDPTest != 0)
        {
            m_NetworkPtr->Destroy(m_sUDPTest);
            m_sUDPTest = 0;
        }
        if (m_sListen != 0)
        {
            m_NetworkPtr->Destroy(m_sListen);
            m_sListen = 0;
        }
        if (m_NetworkPtr != nullptr)
        {
            m_NetworkPtr->Exit();
            m_NetworkPtr = nullptr;
        }
        if (m_pData != nullptr)
        {
            FREE( m_pData );
            m_pData = nullptr;
        }
    }

    void Wait(void)
    {
        m_Event.Wait(100000);
    }
#ifdef BROADCAST_TEST
    virtual UInt OnHandle(uintptr_t utEvent, UInt)
    {
        if (utEvent == SERVER_LIVE_TIMER_ID)
        {
            if (m_nCount >= SERVER_LIVE_MAX_TICK)
            {
                DEV_INFO(TF("Ready to exit"));
                m_Event.Signal();
            }

            CChatPacketAck ChatAck;
            ChatAck.SetId(m_nCount);
            ChatAck.AdjustSize();

            m_NetworkPtr->Send(0, ChatAck, SEND_BROADCAST_AS, 100);
            if ((m_nCount % SERVER_LIVE_OUT_TICK) == 0)
            {
                DEV_INFO(TF("TCP#########ChatAck is %d"), m_nCount);
            }
            ++m_nCount;
        }
        return RET_OKAY;
    }
#endif
protected:
    virtual bool OnTcpDispatch(const PacketPtr& PktPtr, PTCP_PARAM pTcp)
    {
        switch (PktPtr->GetEvent())
        {
#ifdef BROADCAST_TEST
        case TEST_EVENT_CHAT:
            {
                CChatPacket* pChatPkt = static_cast<CChatPacket*>(PktPtr.Get());
                if ((pChatPkt->GetId() % SERVER_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Chat : %d[%s]---%d"), pChatPkt->GetId(), pChatPkt->GetChat().GetBuffer(), pChatPkt->m_uBufSize);
                }
            }
            break;
#else
        case TEST_EVENT_LOGIN:
            {
                CLoginPacket* pLoginPkt = static_cast<CLoginPacket*>(PktPtr.Get());

                if ((pLoginPkt->GetId() % SERVER_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Login is %d[%d]"), pLoginPkt->GetId(), pLoginPkt->GetPass());
                }

                CAckPacket LoginAck(TEST_EVENT_LOGINACK);
                LoginAck.SetAck(RET_OKAY);
                LoginAck.SetParam(m_nCount);
                LoginAck.AdjustSize();

                CStreamScopePtr StreamPtr;
                if (m_NetworkPtr->AllocBuffer(StreamPtr, pTcp->pCache))
                {
                    pTcp->pCache = nullptr;
                    LoginAck.Serialize(*StreamPtr);
                    m_NetworkPtr->Send(pTcp->sSocket, LoginAck);
                }
                else
                {
                    m_NetworkPtr->Send(pTcp->sSocket, LoginAck);
                }
            }
            break;
        case TEST_EVENT_CHAT:
            {
                CChatPacket* pChatPkt = static_cast<CChatPacket*>(PktPtr.Get());
                if ((pChatPkt->GetId() % SERVER_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Chat : %d[%s]---%d"), pChatPkt->GetId(), pChatPkt->GetChat().GetBuffer(), pChatPkt->m_uBufSize);
                }
               // ++m_nCount;

                CChatPacketAck ChatAck;
                ChatAck.SetId(m_nCount);
                //ChatAck.SetData(m_pData, TEST_SIZE_PAK);
                ChatAck.AdjustSize();

                m_NetworkPtr->Send(pTcp->sSocket, ChatAck);
            }
            break;
        case TEST_EVENT_LOGOUT:
            {
                CLogoutPacket* pLogoutPkt = static_cast<CLogoutPacket*>(PktPtr.Get());
                if ((pLogoutPkt->GetId() % SERVER_LIVE_OUT_TICK) == 0)
                {
                    DEV_INFO(TF("#########Logout is %d"), pLogoutPkt->GetId());
                }
                ++m_nCount;

                CAckPacket LogoutAck(TEST_EVENT_LOGOUTACK);
                LogoutAck.SetAck(RET_OKAY);
                LogoutAck.SetParam(pLogoutPkt->GetId());
                LogoutAck.AdjustSize();

                m_NetworkPtr->Send(pTcp->sSocket, LogoutAck);

                if (m_nCount >= SERVER_LIVE_MAX_TICK)
                {
                    DEV_INFO(TF("Ready to exit"));
                    m_Event.Signal();
                }
            }
            break;
#endif
        default:
            {
                // live-packet
            }
        }
        return true;
    }

    virtual bool OnUdpDispatch(const PacketPtr& PktPtr, PUDP_PARAM pUdp)
    {
        assert(pUdp->sSocket == m_sUDPTest);
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_LOGIN:
            {
                CLoginPacket* pLoginPkt = static_cast<CLoginPacket*>(PktPtr.Get());
                if ((pLoginPkt->GetId() % SERVER_LIVE_OUT_TICK) == 0)
                {
                    CString strAddr;
                    UShort  usPort = 0;
                    m_NetworkPtr->TranslateAddr(strAddr, usPort, pUdp->NetAddr, false);

                    DEV_INFO(TF("$$$$$$$$$Login is %d[%d], [%s]:%d"), pLoginPkt->GetId(), pLoginPkt->GetPass(), *strAddr, usPort);
                }

                CAckPacket LoginAck(TEST_EVENT_LOGINACK);
                LoginAck.SetAck(RET_OKAY);
                LoginAck.SetParam(m_nCount);
                LoginAck.AdjustSize();

                m_NetworkPtr->SendTo(pUdp->sSocket, LoginAck, pUdp->NetAddr);
            }
            break;
        default:
            {
            }
        }
        return true;
    }

    virtual bool OnTcpAccept(Socket sAccept, Socket sListen)
    {
        DEV_INFO(TF("###Accept socket=%p, listen socket %p"), sAccept, sListen);
        assert(sListen == m_sListen);

        //m_NetworkPtr->Destroy(m_sListen);
#ifdef BROADCAST_TEST
        m_NetworkPtr->SetAttr(sAccept, 100);
#endif
        //m_NetworkPtr->SetAttr(sAccept, 2 * 60 * 60 * 1000, SOCKET_MANUAL_TIMEOUT);
        return true;
    }

    virtual bool OnTcpConnect(UInt uError, Socket sConnect)
    {
        DEV_INFO(TF("###Connect return errid=%d, socket %p"), uError, sConnect);
        return true;
    }

    virtual bool OnTcpRecv(size_t stSize, PTCP_PARAM pTcp)
    {
        PCStr p = (PCStr)pTcp->pData;
        DEV_INFO(TF("###OnTcpRecv [%d---%d]%s"), stSize, CChar::Length(p), p);

        //CHTTPParser Parser;
        //Parser.Parse(pTcp->pData, stSize);
        //if (Parser.IsContentComplete())
        //{
        //    CStreamScopePtr StreamPtr;
        //    m_NetworkPtr->AllocBuffer(StreamPtr);
        //    (*StreamPtr) <= "HTTP/1.1 200 OK\r\nServer: nginx\r\nContent-Type: text/html;charset=UTF-8\r\nVary: Accept-Encoding\r\nCache-Control: no-store\r\nPragrma: no-cache\r\nCache-Control: no-cache\r\nContent-Length:89\r\n\r\n<HTML><HEAD><TITLE>CREEK REST API</TITLE></HEAD><BODY>CREEK REST API TEST</BODY></HTML>\r\n";
        //    m_NetworkPtr->Send(pTcp->sSocket, StreamPtr);

        //    CHTTPRequest Request;
        //    Parser.To(Request);
        //}
        return true;
    }

    virtual bool OnTcpSend(UInt uQueueLimit, Socket sSocket)
    {
        if ((m_nCount % SERVER_LIVE_OUT_TICK) == 0)
        {
            DEV_INFO(TF("###Send return queue=%d, socket %p"), uQueueLimit, sSocket);
        }
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == m_sListen)
        {
            m_sListen = m_NetworkPtr->Create(SERVER_CONST_PORT);
            if (m_sListen != 0)
            {
                return m_NetworkPtr->Listen(m_sListen);
            }
        }
        else
        {
        }
        return true;
    }

    virtual bool OnUdpRecv(size_t stSize, PUDP_PARAM)
    {
        return true;
    }

    virtual bool OnUdpSend(UInt uQueueLimit, Socket sSocket)
    {
        if ((m_nCount % SERVER_LIVE_OUT_TICK) == 0)
        {
            DEV_INFO(TF("$$$Send return queue=%d, socket %p"), uQueueLimit, sSocket);
        }
        return true;
    }

    virtual bool OnUdpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("$$$Close udp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == m_sUDPTest)
        {
            m_sUDPTest = 0;
        }
        return true;
    }
private:
    Int              m_nCount;
    Socket           m_sUDPTest;
    Socket           m_sListen;
    PByte            m_pData;
    CNetworkPtr      m_NetworkPtr;
    CSyncEvent       m_Event;
};

CTestServer* g_Svr = nullptr;

IMPLEMENT_CLASS_LOADER( CTestServer, NetworkSystem )

void TestNetwork(void)
{
    //INIT_CLASS_LOADER( CTestServer, NetworkSystem, UUID_OF( CNetworkSystem ), TF("network-rio-Debug.dll"));
    INIT_CLASS_LOADER( CTestServer, NetworkSystem, UUID_OF( CNetworkSystem ), NETWORK_MODULE_NAME );

    CString strUUId;
    CSubSystem::MAP_COMPONENT_INFO Infos;
    (GET_CLASS_LOADER( CTestServer, NetworkSystem ))->GetComponentInfo(Infos);
    for (PINDEX index = Infos.GetFirstIndex(); index != nullptr; )
    {
        CSubSystem::PAIR_COMPONENT_INFO* pPair = Infos.GetNext(index);
        pPair->m_K.ToString(strUUId);
        DEV_INFO(TF("uuid = %s name = %s"), *strUUId, *(pPair->m_V));
    }

    DEV_INFO(TF("sub-system ptr = %p"), GET_CLASS_SUBSYSTEM( CTestServer, NetworkSystem ));

    CTestServer Server;
    g_Svr = &Server;
    if (Server.Init())
    {
        Server.Wait();
        DEV_INFO(TF("wait okay"));
        CPlatform::SleepEx(2000);
    }
    Server.Exit();
    g_Svr = nullptr;
    DUMP(nullptr);
    EXIT_CLASS_LOADER( CTestServer, NetworkSystem );
    DUMP(nullptr);

    getchar();
}
