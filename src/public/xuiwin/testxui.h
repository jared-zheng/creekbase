#pragma once

#include "xui.h"
#include "xuikit.h"
#include "resource.h"

using namespace CREEK;

class CMyView : public CXView
{
protected:
    virtual void OnPaint(void)
    {
        CXPaintDC dc(m_hWnd);
        dc.TextOut(100, 100, TF("玺帝语录 ： 处理下半身讨论群"));
    }
};

class CMyDlg : public CXDlg
{
};

class CMainWnd : public CXFrameWnd
{
    DECLARE_CLASS_LOADER( CMainWnd, XUI )
public:
    CMainWnd(void)
    : m_pDlg(nullptr)
    { 
        SetFrameMode();
    }

    ~CMainWnd(void)
    { 
    }

    bool Init(void);
    void Loop(void);
    void Exit(void);

    virtual bool PreTranslateMessage(LPMSG pMsg) 
    { 
        return m_wndMenuBar.PreTranslateMessage(pMsg);
    }

protected:
    virtual LRESULT OnCreate(LPCREATESTRUCT lpCS);
    virtual void    OnDestroy(void);
    virtual void    FrameLayout(void);

    LRESULT OnCommand(WORD wCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT OnXNotify(WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    XBEGIN_INLINE_MSG_MAP( CMainWnd )
    // {{ --
        XCOMMAND_ID_HANDLER(ID_ABOUT, OnCommand)
        XCOMMAND_ID_HANDLER(ID_EXIT, OnCommand)
        XMSG_XUIM_NOTIFY()
        XMSG_WM_CREATE()
        XMSG_WM_DESTROY()
    // --}}
    XEND_CHAIN_MSG_MAP( CXFrameWnd )

private:
    CXUIManagerPtr     m_UIManagerPtr;
    CMyView            m_wndView; 
    CXReBar            m_wndReBar;
    CXMenuBar          m_wndMenuBar;
    CXToolBar          m_wndToolBar; 
    CXStatusBar        m_wndStatusBar;

    CXStatic           m_Static;
    CXEdit             m_Edit;
    CXComboBox         m_Combox;
    CXTab              m_Tab;
    CXListBox          m_ListBox;
    CXButton           m_Button;


    CXIPAddress        m_IP;
    CXHotKey           m_HotKey;
    CXDateTimePicker   m_DateTimePicker;
    CXMonthCalendar    m_MonthCal;
    CXPager            m_Pager;
    CXHeader           m_Header;
    CXListView         m_ListView;

    CMyDlg*            m_pDlg;
};

void TestXUI(void);

