//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xuiwin.rc
//
#define IDD_ABOUTBOX                    100
#define ID_ABOUT                        101
#define ID_EXIT                         102
#define IDM_EDITOR                      103
#define IDT_EDITOR                      104
#define ID_FILENEW                      32772
#define ID_FILEOPEN                     32773
#define ID_FILESAVE                     32774
#define ID_FILESAVEAS                   32775
#define ID_FILECLOSE                    32776
#define ID_FILECLOSEALL                 32777
#define ID_FILEPRINT                    32778
#define ID_EDITSELECT                   32779
#define ID_EDITCUT                      32780
#define ID_EDITCOPY                     32781
#define ID_EDITPASTE                    32782
#define ID_EDITCROP                     32783
#define ID_EDITRESIZE                   32784
#define ID_CAPSCREEN                    32785
#define ID_CAPSELECT                    32786
#define ID_CAPOBJECT                    32787
#define IDC_STATIC                      -1

// Next default values for new objects
//  
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
