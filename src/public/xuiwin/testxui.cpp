#include "stdafx.h"
#include "testxui.h"

bool CMainWnd::Init(void)
{
    LOADER_CLASS_CREEATE( CMainWnd, XUI, UUID_OF( CXUIManager ), m_UIManagerPtr.Cast<CComponent>());
    if (m_UIManagerPtr != nullptr)
    {
        if (m_UIManagerPtr->Init() == RET_OKAY)
        {
            InitCommonCtrls(ICC_WIN95_CLASSES);//(ICC_COOL_CLASSES | ICC_BAR_CLASSES));
            CXUITraits::CREATE_PARAM cpm;
            cpm.pszName  = TF("处理下半身讨论群");
            if (m_UIManagerPtr->Create(*this, cpm) != RET_ERROR)
            {
                UpdateWindow();
                return true;
            }
        }
    }
    return false;
}

void CMainWnd::Loop(void)
{
    if (m_UIManagerPtr != nullptr)
    {
        m_UIManagerPtr->MsgLoop();
    }
}

void CMainWnd::Exit(void)
{
    if (m_UIManagerPtr != nullptr)
    {
        m_UIManagerPtr->Exit();
        m_UIManagerPtr = nullptr;
    }
}

LRESULT CMainWnd::OnCreate(LPCREATESTRUCT)
{
    CXUITraits::CREATE_PARAM cpm;
    cpm.pParent   = this;
    cpm.itMenuID  = 1001;
    cpm.ulStyle   = XUIS_REBAR_NOBORDER;
    if (m_UIManagerPtr->Create(m_wndReBar, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID = 1002;
    cpm.ulStyle  = XUIS_MENUBAR_PANE;
    if (m_UIManagerPtr->Create(m_wndMenuBar, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    if (m_wndMenuBar.LoadMenuBar(IDM_EDITOR) == false)
    {
        return RET_ERROR;
    }

    cpm.itMenuID = 1003;
    cpm.ulStyle  = XUIS_TOOLBAR_PANE;
    if (m_UIManagerPtr->Create(m_wndToolBar, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    if (m_wndToolBar.LoadEx(48, IDT_EDITOR) == false)
    {
        return RET_ERROR;
    }
    HMENU hMenu = ::GetSystemMenu(m_hWnd, false);
    m_wndToolBar.SetDropDowmBtn(ID_ABOUT, hMenu);

    m_wndReBar.InsertBand(m_wndMenuBar.m_hWnd, 0, 24);
    m_wndReBar.InsertBand(m_wndToolBar.m_hWnd, 1, 56, true);

    cpm.itMenuID = 1004;
    cpm.ulStyle  = XUIS_STATUSBAR;
    if (m_UIManagerPtr->Create(m_wndStatusBar, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1005;
    cpm.ulStyle   = XUIS_CHILD;
    cpm.ulExStyle = WS_EX_CLIENTEDGE;
    if (m_UIManagerPtr->Create(m_wndView, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.pszName   = TF("爆菊大赛");
    cpm.itMenuID  = 1006;
    cpm.ulStyle   = XUIS_CHILD_TABSTOP|SS_SIMPLE;
    cpm.ulExStyle = 0;
    if (m_UIManagerPtr->Create(m_Static, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.pszName   = TF("您的大名");
    cpm.itMenuID  = 1007;
    cpm.ulStyle   = XUIS_CHILD_TABSTOP;
    if (m_UIManagerPtr->Create(m_Edit, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.pszName   = nullptr;
    cpm.itMenuID  = 1008;
    cpm.ulStyle   = XUIS_CHILD_TABSTOP|CBS_DROPDOWN;
    if (m_UIManagerPtr->Create(m_Combox, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    m_Combox.AddString(TF("被爆"));
    m_Combox.AddString(TF("被虐"));

    cpm.itMenuID  = 1009;
    cpm.ulStyle   = XUIS_CHILD_TABSTOP;
    if (m_UIManagerPtr->Create(m_Tab, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    XChar szText[LMT_KEY] = { 0 };
    TCITEM tci = { 0 };
    tci.mask = TCIF_TEXT | TCIF_IMAGE;
    tci.iImage = -1;
    tci.pszText = szText;
    for(Int i = 0; i < 2; i++)
    {
        CXChar::Format(szText, LMT_KEY, TF("Tab %d"), i + 1);
        m_Tab.InsertItem(i, &tci);
    }

    cpm.itMenuID  = 1010;
    if (m_UIManagerPtr->Create(m_ListBox, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    m_ListBox.AddString(TF("信息总结："));

    cpm.pszName   = TF("确认报名");
    cpm.itMenuID  = 1011;
    if (m_UIManagerPtr->Create(m_Button, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }


    cpm.itMenuID  = 1012;
    cpm.ulStyle   = XUIS_CHILD_TABSTOP;
    if (m_UIManagerPtr->Create(m_IP, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    if (m_IP.IsBlank())
    {
        m_IP.SetAddress(0x0f0f0f0f);
    }

    cpm.itMenuID  = 1013;
    if (m_UIManagerPtr->Create(m_HotKey, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1014;
    if (m_UIManagerPtr->Create(m_DateTimePicker, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1015;
    if (m_UIManagerPtr->Create(m_MonthCal, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1016;
    if (m_UIManagerPtr->Create(m_Pager, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1017;
    if (m_UIManagerPtr->Create(m_Header, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }

    cpm.itMenuID  = 1018;
    cpm.ulStyle   = LVS_REPORT | LVS_ALIGNLEFT | WS_BORDER | XUIS_CHILD_TABSTOP;
    if (m_UIManagerPtr->Create(m_ListView, cpm) == RET_ERROR)
    {
        return RET_ERROR;
    }
    m_ListView.AddColumn(TF("Column1"), 0, 80, -1, LVCF_WIDTH | LVCF_TEXT);
    m_ListView.AddColumn(TF("Column2"), 1, 80, -1, LVCF_WIDTH | LVCF_TEXT);
    m_ListView.AddColumn(TF("Column3"), 2, 80, -1, LVCF_WIDTH | LVCF_TEXT);
    m_ListView.AddColumn(TF("Column4"), 3, 80, -1, LVCF_WIDTH | LVCF_TEXT);
    m_ListView.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    Int nIndex = m_ListView.InsertItem(0, TF("1"));
    if (nIndex > -1)
    {
        m_ListView.SetItemText(nIndex, 1, TF("Item1"));
        m_ListView.SetItemText(nIndex, 2, TF("Item2"));
        m_ListView.SetItemText(nIndex, 3, TF("Item3"));
    }

    ///////////////////////////////
    RECT rt = { 0, 0, 800, 600 };
    ///////////////////////////////

    AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

    long x = GetSystemMetrics(SM_CXSCREEN);
    long y = GetSystemMetrics(SM_CYSCREEN);
    x = (x - (rt.right - rt.left)) / 2;
    y = (y - (rt.bottom - rt.top)) / 2;

    SetWindowPos(nullptr, x, y, rt.right - rt.left, rt.bottom - rt.top, SWP_NOZORDER|SWP_SHOWWINDOW);
    return 0;
}

void CMainWnd::OnDestroy(void)
{
    if (m_pDlg != nullptr)
    {
        if (m_pDlg->IsWindow())
        {
            m_pDlg->EndDialog(0);
        }
        MDELETE m_pDlg;
    }
    //m_wndView.DestroyWindow();
    //m_wndMenuBar.DestroyWindow();
    //m_wndToolBar.DestroyWindow();
    //m_wndReBar.DestroyWindow();
    //m_wndStatusBar.DestroyWindow();
    CXFrameWnd::OnDestroy();
}

void CMainWnd::FrameLayout(void)
{
    RECT rc;
    GetClientRect(&rc);

    ::SendMessage(m_wndReBar.m_hWnd, XUIM_LAYOUT, MAKEWPARAM(0, XUI_LAYOUT_RECT), (LPARAM)&rc);
    ::SendMessage(m_wndStatusBar.m_hWnd, XUIM_LAYOUT, MAKEWPARAM(0, XUI_LAYOUT_RECT), (LPARAM)&rc);
    // 
    Int nHeight = rc.top;
    m_Static.MoveWindow(0, nHeight, 200, 24);
    nHeight += 24;

    m_Edit.MoveWindow(0, nHeight, 200, 24);
    nHeight += 24;

    m_Combox.MoveWindow(0, nHeight, 200, 32);
    nHeight += 32;

    m_Tab.MoveWindow(0, nHeight, 200, 256);
    nHeight += 256;

    m_Button.MoveWindow(0, rc.bottom - 24, 200, 24);

    m_ListBox.MoveWindow(0, nHeight, 200, rc.bottom - 24 - nHeight);

    rc.left = 200;
    //

    m_IP.MoveWindow(200, rc.top, 200, 20);
    m_HotKey.MoveWindow(400, rc.top, 200, 20);
    rc.top += 20;

    m_DateTimePicker.MoveWindow(200, rc.top, 200, 20);
    m_Pager.MoveWindow(200, rc.top + 20, 200, 20);
    m_Header.MoveWindow(200, rc.top + 40, 200, 20);

    m_MonthCal.MoveWindow(400, rc.top, 200, 60);

    rc.top += 60;

    m_ListView.MoveWindow(rc.left, rc.top, rc.right - rc.left, 200);

    rc.top += 200;

    // must be the lastest one
    ::SetWindowPos(m_wndView.m_hWnd, nullptr, rc.left, rc.top,
                   rc.right - rc.left, rc.bottom - rc.top,
                   SWP_NOSENDCHANGING | SWP_NOZORDER | SWP_NOACTIVATE);
}


LRESULT CMainWnd::OnCommand(WORD, WORD wID, HWND, BOOL&)
{
    if (wID == ID_ABOUT)
    {
        if (m_pDlg == nullptr)
        {
            m_pDlg = MNEW CMyDlg;
        }

        CXUITraits::CREATE_PARAM cpm;
        cpm.pParent  = this;
        cpm.itMenuID = IDD_ABOUTBOX;

        m_UIManagerPtr->Create(*m_pDlg, cpm);
    }
    else if (wID == ID_EXIT)
    {
        DestroyWindow();
    }
    return 0;
}

LRESULT CMainWnd::OnXNotify(WPARAM wParam, LPARAM lParam, BOOL&)
{
    switch(wParam)
    {
    case XUI_NOTIFY_TTSHOW:
        {
            m_wndStatusBar.SetText(0, (LPCTSTR)lParam);
        }
        break;
    case XUI_NOTIFY_TTHIDE:
        {
            m_wndStatusBar.SetText(0, DefaultReady);
        }
        break;
    default:
        {
        }
    }
    return 0;
}

IMPLEMENT_CLASS_LOADER( CMainWnd, XUI)

void TestXUI(void)
{
    INIT_CLASS_LOADER( CMainWnd, XUI, UUID_OF( CXUISystem ), XUI_MODULE_NAME);

    CMainWnd main;
    if (main.Init())
    {
        main.Loop();
    }
    main.Exit();

    EXIT_CLASS_LOADER( CMainWnd, XUI );
}