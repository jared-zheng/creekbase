// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __RTTI_INL__
#define __RTTI_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CRTTIType
INLINE CRTTIType::CRTTIType(const CRTTI& rtti, PCXStr pszOrigin, size_t stType)
: m_pRTTI(&rtti)
, m_pszOrigin(pszOrigin)
, m_stType(stType)
{
}

INLINE CRTTIType::~CRTTIType(void)
{
}

INLINE CRTTIType::CRTTIType(const CRTTIType& aSrc)
: m_pRTTI(aSrc.m_pRTTI)
, m_pszOrigin(aSrc.m_pszOrigin)
, m_stType(aSrc.m_stType)
{
}

INLINE CRTTIType& CRTTIType::operator=(const CRTTIType& aSrc)
{
    if (this != &aSrc)
    {
        m_pRTTI     = aSrc.m_pRTTI;
        m_pszOrigin = aSrc.m_pszOrigin;
        m_stType    = aSrc.m_stType;
    }
    return (*this);
}

INLINE const CRTTI& CRTTIType::GetRTTI(void) const
{ 
    return *m_pRTTI;
}

INLINE PCXStr CRTTIType::GetOrigin(void) const
{ 
    return m_pszOrigin;
}

INLINE const size_t CRTTIType::GetType(void) const
{ 
    return m_stType;
}

INLINE bool CRTTIType::IsEnum(void) const
{ 
    return ((m_stType & VART_ENUM) != 0);
}

INLINE bool CRTTIType::IsPointer(void) const
{ 
    return ((m_stType & VART_POINTER) != 0);
}

INLINE bool CRTTIType::IsStatic(void) const
{ 
    return ((m_stType & VART_STATIC) != 0);
}

INLINE bool CRTTIType::IsConst(void) const
{ 
    return ((m_stType & VART_CONST) != 0);
}

INLINE bool CRTTIType::IsFunction(void) const
{ 
    return ((m_stType & VART_FUNCTION) != 0);
}

///////////////////////////////////////////////////////////////////
// CRTTIEnum : member enum wrapper
INLINE CRTTIEnum::CRTTIEnum(const CRTTI& rtti, PCXStr pszOrigin, const ARY_ITEM& Items)
: CRTTIType(rtti, pszOrigin, VART_ENUM)
, m_Items(Items)
{
}

INLINE CRTTIEnum::~CRTTIEnum(void)
{
}

INLINE CRTTIEnum::CRTTIEnum(const CRTTIEnum& aSrc)
: CRTTIType(aSrc)
, m_Items(aSrc.m_Items)
{
}

INLINE CRTTIEnum& CRTTIEnum::operator=(const CRTTIEnum& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_Items = aSrc.m_Items;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CRTTIEnum::CRTTIEnum(const CRTTI& rtti, PCXStr pszOrigin, ARY_ITEM&& Items)
: CRTTIType(rtti, pszOrigin, VART_ENUM)
, m_Items(std::move(Items))
{
}

INLINE CRTTIEnum::CRTTIEnum(CRTTIEnum&& aSrc)
: CRTTIType(aSrc)
, m_Items(std::move(aSrc.m_Items))
{
}

INLINE CRTTIEnum& CRTTIEnum::operator=(CRTTIEnum&& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_Items = std::move(aSrc.m_Items);
    }
    return (*this);
}
#endif

INLINE ULLong CRTTIEnum::At(PCXStr pszName) const
{
    for (Int i = 0; i < m_Items.GetSize(); ++i)
    {
        if (CXChar::Cmp(m_Items[i].pszName, pszName) == 0)
        {
            return m_Items[i].ullValue;
        }
    }
    return 0;
}

INLINE ULLong CRTTIEnum::At(const CString& strName) const
{
    for (Int i = 0; i < m_Items.GetSize(); ++i)
    {
        if (strName == m_Items[i].pszName)
        {
            return m_Items[i].ullValue;
        }
    }
    return 0;
}

INLINE PCXStr CRTTIEnum::At(ULLong ullValue) const
{
    for (Int i = 0; i < m_Items.GetSize(); ++i)
    {
        if (m_Items[i].ullValue == ullValue)
        {
            return m_Items[i].pszName;
        }
    }
    return nullptr;
}

INLINE bool CRTTIEnum::At(ULLong ullValue, CString& strName) const
{
    for (Int i = 0; i < m_Items.GetSize(); ++i)
    {
        if (m_Items[i].ullValue == ullValue)
        {
            strName = m_Items[i].pszName;
            return true;
        }
    }
    return false;
}

INLINE const ENUM_ARY_ITEM& CRTTIEnum::GetItems(void) const
{
    return m_Items;
}

///////////////////////////////////////////////////////////////////
// CTRTTIVar : member variable wrapper
template <typename TOBJECT, typename TVAR>
INLINE CTRTTIVar<TOBJECT, TVAR>::CTRTTIVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TVAR TOBJECT::* pVar)
: CRTTIType(rtti, pszOrigin, stType)
, m_pVar(pVar)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTIVar<TOBJECT, TVAR>::~CTRTTIVar(void)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTIVar<TOBJECT, TVAR>::CTRTTIVar(const CTRTTIVar& aSrc)
: CRTTIType(aSrc)
, m_pVar(aSrc.m_pVar)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTIVar<TOBJECT, TVAR>& CTRTTIVar<TOBJECT, TVAR>::operator=(const CTRTTIVar& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pVar = aSrc.m_pVar;
    }
    return (*this);
}

template <typename TOBJECT, typename TVAR>
INLINE bool CTRTTIVar<TOBJECT, TVAR>::IsValid(void) const
{
    return (m_pVar != nullptr);
}

template <typename TOBJECT, typename TVAR>
INLINE TVAR& CTRTTIVar<TOBJECT, TVAR>::At(TOBJECT& ObjRef) const
{
    return ObjRef.*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE TVAR& CTRTTIVar<TOBJECT, TVAR>::At(TOBJECT* pObj) const
{
    return pObj->*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR& CTRTTIVar<TOBJECT, TVAR>::At(const TOBJECT& ObjRef) const
{
    return ObjRef.*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR& CTRTTIVar<TOBJECT, TVAR>::At(const TOBJECT* pObj) const
{
    return pObj->*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE TVAR TOBJECT::* CTRTTIVar<TOBJECT, TVAR>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == false);
    assert(IsFunction() == false);
    assert(m_pVar != nullptr);
    return (m_pVar);
}

///////////////////////////////////////////////////////////////////
// CTRTTICVar : member const variable wrapper
template <typename TOBJECT, typename TVAR>
INLINE CTRTTICVar<TOBJECT, TVAR>::CTRTTICVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, const TVAR TOBJECT::* pVarConst)
: CRTTIType(rtti, pszOrigin, stType)
, m_pVarConst(pVarConst)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTICVar<TOBJECT, TVAR>::~CTRTTICVar(void)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTICVar<TOBJECT, TVAR>::CTRTTICVar(const CTRTTICVar& aSrc)
: CRTTIType(aSrc)
, m_pVarConst(aSrc.m_pVarConst)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTICVar<TOBJECT, TVAR>& CTRTTICVar<TOBJECT, TVAR>::operator=(const CTRTTICVar& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pVarConst = aSrc.m_pVarConst;
    }
    return (*this);
}

template <typename TOBJECT, typename TVAR>
INLINE bool CTRTTICVar<TOBJECT, TVAR>::IsValid(void) const
{
    return (m_pVarConst != nullptr);
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR& CTRTTICVar<TOBJECT, TVAR>::At(const TOBJECT& ObjRef) const
{
    return ObjRef.*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR& CTRTTICVar<TOBJECT, TVAR>::At(const TOBJECT* pObj) const
{
    return pObj->*Get();
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR TOBJECT::* CTRTTICVar<TOBJECT, TVAR>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == true);
    assert(IsFunction() == false);
    assert(m_pVarConst != nullptr);
    return (m_pVarConst);
}

///////////////////////////////////////////////////////////////////
// CTRTTISVar : static member variable wrapper
template <typename TOBJECT, typename TVAR>
INLINE CTRTTISVar<TOBJECT, TVAR>::CTRTTISVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TVAR* pVarStatic)
: CRTTIType(rtti, pszOrigin, stType)
, m_pVarStatic(pVarStatic)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISVar<TOBJECT, TVAR>::~CTRTTISVar(void)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISVar<TOBJECT, TVAR>::CTRTTISVar(const CTRTTISVar& aSrc)
: CRTTIType(aSrc)
, m_pVarStatic(aSrc.m_pVarStatic)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISVar<TOBJECT, TVAR>& CTRTTISVar<TOBJECT, TVAR>::operator=(const CTRTTISVar& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pVarStatic = aSrc.m_pVarStatic;
    }
    return (*this);
}

template <typename TOBJECT, typename TVAR>
INLINE bool CTRTTISVar<TOBJECT, TVAR>::IsValid(void) const
{
    return (m_pVarStatic != nullptr);
}

template <typename TOBJECT, typename TVAR>
INLINE TVAR& CTRTTISVar<TOBJECT, TVAR>::At(void) const
{
    return *Get();
}

template <typename TOBJECT, typename TVAR>
INLINE TVAR* CTRTTISVar<TOBJECT, TVAR>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == true);
    assert(IsConst() == false);
    assert(IsFunction() == false);
    assert(m_pVarStatic != nullptr);
    return (m_pVarStatic);
}

///////////////////////////////////////////////////////////////////
// CTRTTISCVar : static const member variable wrapper
template <typename TOBJECT, typename TVAR>
INLINE CTRTTISCVar<TOBJECT, TVAR>::CTRTTISCVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, const TVAR* pVarStaticConst)
: CRTTIType(rtti, pszOrigin, stType)
, m_pVarStaticConst(pVarStaticConst)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISCVar<TOBJECT, TVAR>::~CTRTTISCVar(void)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISCVar<TOBJECT, TVAR>::CTRTTISCVar(const CTRTTISCVar& aSrc)
: CRTTIType(aSrc)
, m_pVarStaticConst(aSrc.m_pVarStaticConst)
{
}

template <typename TOBJECT, typename TVAR>
INLINE CTRTTISCVar<TOBJECT, TVAR>& CTRTTISCVar<TOBJECT, TVAR>::operator=(const CTRTTISCVar& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pVarStaticConst = aSrc.m_pVarStaticConst;
    }
    return (*this);
}

template <typename TOBJECT, typename TVAR>
INLINE bool CTRTTISCVar<TOBJECT, TVAR>::IsValid(void) const
{
    return (m_pVarStaticConst != nullptr);
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR& CTRTTISCVar<TOBJECT, TVAR>::At(void) const
{
    return *Get();
}

template <typename TOBJECT, typename TVAR>
INLINE const TVAR* CTRTTISCVar<TOBJECT, TVAR>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == true);
    assert(IsConst() == true);
    assert(IsFunction() == false);
    assert(m_pVarStaticConst != nullptr);
    return (m_pVarStaticConst);
}

#ifdef __MODERN_CXX_NOT_SUPPORTED
///////////////////////////////////////////////////////////////////
// CTRTTIFunc : member function wrapper
template <typename TOBJECT, typename TFUNC>
INLINE CTRTTIFunc<TOBJECT, TFUNC>::CTRTTIFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TFUNC pFunc)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFunc(pFunc)
{
}

template <typename TOBJECT, typename TFUNC>
INLINE CTRTTIFunc<TOBJECT, TFUNC>::~CTRTTIFunc(void)
{
}

template <typename TOBJECT, typename TFUNC>
INLINE CTRTTIFunc<TOBJECT, TFUNC>::CTRTTIFunc(const CTRTTIFunc& aSrc)
: CRTTIType(aSrc)
, m_pFunc(aSrc.m_pFunc)
{
}

template <typename TOBJECT, typename TFUNC>
INLINE CTRTTIFunc<TOBJECT, TFUNC>& CTRTTIFunc<TOBJECT, TFUNC>::operator=(const CTRTTIFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFunc = aSrc.m_pFunc;
    }
    return (*this);
}

template <typename TOBJECT, typename TFUNC>
INLINE bool CTRTTIFunc<TOBJECT, TFUNC>::IsValid(void) const
{
    return (m_pFunc != nullptr);
}

template <typename TOBJECT, typename TFUNC>
INLINE TFUNC CTRTTIFunc<TOBJECT, TFUNC>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFunc != nullptr);
    return m_pFunc;
}

///////////////////////////////////////////////////////////////////
// CTRTTICFunc : member const function wrapper
template <typename TOBJECT, typename TCFUNC>
INLINE CTRTTICFunc<TOBJECT, TCFUNC>::CTRTTICFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TCFUNC pFuncConst)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFuncConst(pFuncConst)
{
}

template <typename TOBJECT, typename TCFUNC>
INLINE CTRTTICFunc<TOBJECT, TCFUNC>::~CTRTTICFunc(void)
{
}

template <typename TOBJECT, typename TCFUNC>
INLINE CTRTTICFunc<TOBJECT, TCFUNC>::CTRTTICFunc(const CTRTTICFunc& aSrc)
: CRTTIType(aSrc)
, m_pFuncConst(aSrc.m_pFuncConst)
{
}

template <typename TOBJECT, typename TCFUNC>
INLINE CTRTTICFunc<TOBJECT, TCFUNC>& CTRTTICFunc<TOBJECT, TCFUNC>::operator=(const CTRTTICFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFuncConst = aSrc.m_pFuncConst;
    }
    return (*this);
}

template <typename TOBJECT, typename TCFUNC>
INLINE bool CTRTTICFunc<TOBJECT, TCFUNC>::IsValid(void) const
{
    return (m_pFuncConst != nullptr);
}

template <typename TOBJECT, typename TCFUNC>
INLINE TCFUNC CTRTTICFunc<TOBJECT, TCFUNC>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == true);
    assert(IsFunction() == true);
    assert(m_pFuncConst != nullptr);
    return m_pFuncConst;
}

///////////////////////////////////////////////////////////////////
// CTRTTISFunc : member static function wrapper
template <typename TOBJECT, typename TSFUNC>
INLINE CTRTTISFunc<TOBJECT, TSFUNC>::CTRTTISFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TSFUNC pFuncStatic)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFuncStatic(pFuncStatic)
{
}

template <typename TOBJECT, typename TSFUNC>
INLINE CTRTTISFunc<TOBJECT, TSFUNC>::~CTRTTISFunc(void)
{
}

template <typename TOBJECT, typename TSFUNC>
INLINE CTRTTISFunc<TOBJECT, TSFUNC>::CTRTTISFunc(const CTRTTISFunc& aSrc)
: CRTTIType(aSrc)
, m_pFuncStatic(aSrc.m_pFuncStatic)
{
}

template <typename TOBJECT, typename TSFUNC>
INLINE CTRTTISFunc<TOBJECT, TSFUNC>& CTRTTISFunc<TOBJECT, TSFUNC>::operator=(const CTRTTISFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFuncStatic = aSrc.m_pFuncStatic;
    }
    return (*this);
}

template <typename TOBJECT, typename TSFUNC>
INLINE bool CTRTTISFunc<TOBJECT, TSFUNC>::IsValid(void) const
{
    return (m_pFuncStatic != nullptr);
}

template <typename TOBJECT, typename TSFUNC>
INLINE TSFUNC CTRTTISFunc<TOBJECT, TSFUNC>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == true);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFuncStatic != nullptr);
    return m_pFuncStatic;
}
#else
///////////////////////////////////////////////////////////////////
// CTRTTIFunc : member function wrapper
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTIFunc<TOBJECT, TRET(TARGS...)>::CTRTTIFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_FUNC pFunc)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFunc(pFunc)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTIFunc<TOBJECT, TRET(TARGS...)>::~CTRTTIFunc(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTIFunc<TOBJECT, TRET(TARGS...)>::CTRTTIFunc(const CTRTTIFunc& aSrc)
: CRTTIType(aSrc)
, m_pFunc(aSrc.m_pFunc)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTIFunc<TOBJECT, TRET(TARGS...)>& CTRTTIFunc<TOBJECT, TRET(TARGS...)>::operator=(const CTRTTIFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFunc = aSrc.m_pFunc;
    }
    return (*this);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTRTTIFunc<TOBJECT, TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE typename CTRTTIFunc<TOBJECT, TRET(TARGS...)>::RTTI_FUNC CTRTTIFunc<TOBJECT, TRET(TARGS...)>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFunc != nullptr);
    return (m_pFunc);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTRTTIFunc<TOBJECT, TRET(TARGS...)>::Invoke(TOBJECT& ObjRef, TARGS&&... Args) const
{
#if __MODERN_CXX_LANG >= 201703L
    static_assert(std::is_polymorphic_v<CRTTIType>);
    static_assert(std::is_polymorphic_v<CTRTTIFunc>);
    static_assert(std::is_base_of_v<CRTTIType, CTRTTIFunc>);
#else
    static_assert(std::is_polymorphic<CRTTIType>::value, "CTRTTIFunc::Invoke1");
    static_assert(std::is_polymorphic<CTRTTIFunc>::value, "CTRTTIFunc::Invoke1");
    static_assert(std::is_base_of<CRTTIType, CTRTTIFunc>::value, "CTRTTIFunc::Invoke1");
#endif
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFunc != nullptr);
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    return (ObjRef.*(m_pFunc))(std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTRTTIFunc<TOBJECT, TRET(TARGS...)>::Invoke(const TOBJECT* pObj, TARGS&&... Args) const
{
#if __MODERN_CXX_LANG >= 201703L
    static_assert(std::is_polymorphic_v<CRTTIType>);
    static_assert(std::is_polymorphic_v<CTRTTIFunc>);
    static_assert(std::is_base_of_v<CRTTIType, CTRTTIFunc>);
#else
    static_assert(std::is_polymorphic<CRTTIType>::value, "CTRTTIFunc::Invoke2");
    static_assert(std::is_polymorphic<CTRTTIFunc>::value, "CTRTTIFunc::Invoke2");
    static_assert(std::is_base_of<CRTTIType, CTRTTIFunc>::value, "CTRTTIFunc::Invoke2");
#endif
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFunc != nullptr);
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    return (pObj->*(m_pFunc))(std::forward<TARGS>(Args)...);
}

///////////////////////////////////////////////////////////////////
// CTRTTICFunc : member const function wrapper
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::CTRTTICFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_CFUNC pFuncConst)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFuncConst(pFuncConst)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::~CTRTTICFunc(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::CTRTTICFunc(const CTRTTICFunc& aSrc)
: CRTTIType(aSrc)
, m_pFuncConst(aSrc.m_pFuncConst)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTICFunc<TOBJECT, TRET(TARGS...) const>& CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::operator=(const CTRTTICFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFuncConst = aSrc.m_pFuncConst;
    }
    return (*this);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::IsValid(void) const
{
    return (m_pFuncConst != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE typename CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::RTTI_CFUNC CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == true);
    assert(IsFunction() == true);
    assert(m_pFuncConst != nullptr);
    return (m_pFuncConst);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::Invoke(TOBJECT& ObjRef, TARGS&&... Args) const
{
#if __MODERN_CXX_LANG >= 201703L
    static_assert(std::is_polymorphic_v<CRTTIType>);
    static_assert(std::is_polymorphic_v<CTRTTICFunc>);
    static_assert(std::is_base_of_v<CRTTIType, CTRTTICFunc>);
#else
    static_assert(std::is_polymorphic<CRTTIType>::value, "CTRTTICFunc::Invoke1");
    static_assert(std::is_polymorphic<CTRTTICFunc>::value, "CTRTTICFunc::Invoke1");
    static_assert(std::is_base_of<CRTTIType, CTRTTICFunc>::value, "CTRTTICFunc::Invoke1");
#endif
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == true);
    assert(IsFunction() == true);
    assert(m_pFuncConst != nullptr);
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    return (ObjRef.*(m_pFuncConst))(std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTRTTICFunc<TOBJECT, TRET(TARGS...) const>::Invoke(const TOBJECT* pObj, TARGS&&... Args) const
{
#if __MODERN_CXX_LANG >= 201703L
    static_assert(std::is_polymorphic_v<CRTTIType>);
    static_assert(std::is_polymorphic_v<CTRTTICFunc>);
    static_assert(std::is_base_of_v<CRTTIType, CTRTTICFunc>);
#else
    static_assert(std::is_polymorphic<CRTTIType>::value, "CTRTTICFunc::Invoke2");
    static_assert(std::is_polymorphic<CTRTTICFunc>::value, "CTRTTICFunc::Invoke2");
    static_assert(std::is_base_of<CRTTIType, CTRTTICFunc>::value, "CTRTTICFunc::Invoke2");
#endif
    assert(IsPointer() == true);
    assert(IsStatic() == false);
    assert(IsConst() == true);
    assert(IsFunction() == true);
    assert(m_pFuncConst != nullptr);
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    return (pObj->*(m_pFuncConst))(std::forward<TARGS>(Args)...);
}

///////////////////////////////////////////////////////////////////
// CTRTTISFunc : member static function wrapper
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTISFunc<TOBJECT, TRET(TARGS...)>::CTRTTISFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_SFUNC pFuncStatic)
: CRTTIType(rtti, pszOrigin, stType)
, m_pFuncStatic(pFuncStatic)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTISFunc<TOBJECT, TRET(TARGS...)>::~CTRTTISFunc(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTISFunc<TOBJECT, TRET(TARGS...)>::CTRTTISFunc(const CTRTTISFunc& aSrc)
: CRTTIType(aSrc)
, m_pFuncStatic(aSrc.m_pFuncStatic)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTRTTISFunc<TOBJECT, TRET(TARGS...)>& CTRTTISFunc<TOBJECT, TRET(TARGS...)>::operator=(const CTRTTISFunc& aSrc)
{
    if (this != &aSrc)
    {
        CRTTIType::operator=(aSrc);
        m_pFuncStatic = aSrc.m_pFuncStatic;
    }
    return (*this);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTRTTISFunc<TOBJECT, TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFuncStatic != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE typename CTRTTISFunc<TOBJECT, TRET(TARGS...)>::RTTI_SFUNC CTRTTISFunc<TOBJECT, TRET(TARGS...)>::Get(void) const
{
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    assert(IsPointer() == true);
    assert(IsStatic() == true);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFuncStatic != nullptr);
    return (m_pFuncStatic);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTRTTISFunc<TOBJECT, TRET(TARGS...)>::Invoke(TARGS&&... Args) const
{
#if __MODERN_CXX_LANG >= 201703L
    static_assert(std::is_polymorphic_v<CRTTIType>);
    static_assert(std::is_polymorphic_v<CTRTTISFunc>);
    static_assert(std::is_base_of_v<CRTTIType, CTRTTISFunc>);
#else
    static_assert(std::is_polymorphic<CRTTIType>::value, "CTRTTISFunc::Invoke");
    static_assert(std::is_polymorphic<CTRTTISFunc>::value, "CTRTTISFunc::Invoke");
    static_assert(std::is_base_of<CRTTIType, CTRTTISFunc>::value, "CTRTTISFunc::Invoke");
#endif
    assert(IsPointer() == true);
    assert(IsStatic() == true);
    assert(IsConst() == false);
    assert(IsFunction() == true);
    assert(m_pFuncStatic != nullptr);
    assert(TOBJECT::ClassRTTI().IsKindOf(GetRTTI()));
    return (m_pFuncStatic)(std::forward<TARGS>(Args)...);
}
#endif

///////////////////////////////////////////////////////////////////
// CRTTITraits
template <typename TOBJECT, typename TBASE>
INLINE bool CRTTITraits::IsExactClass(TBASE* pObj)
{
    if (pObj != nullptr)
    {
        return (pObj->GetRTTI().IsExactClass(TOBJECT::ClassRTTI()));
    }
    return false;
}

template <typename TOBJECT, typename TBASE>
INLINE bool CRTTITraits::IsKindOf(TBASE* pObj)
{
    if (pObj != nullptr)
    {
        return (pObj->GetRTTI().IsKindOf(TOBJECT::ClassRTTI()));
    }
    return false;
}

template <typename TOBJECT, typename TBASE>
INLINE TOBJECT* CRTTITraits::DynamicCast(TBASE* pObj)
{
    if (pObj != nullptr)
    {
        return (pObj->GetRTTI().IsKindOf(TOBJECT::ClassRTTI()) ? static_cast<TOBJECT*>(pObj) : nullptr);
    }
    return nullptr;
}

template <typename TOBJECT, typename TBASE>
INLINE TOBJECT* CRTTITraits::DynamicCast(TBASE* pObj, PCXStr pszName)
{
    if (pObj != nullptr)
    {
        if (CXChar::Cmp(pObj->GetRTTI().GetName(), pszName) == 0)
        {
            return static_cast<TOBJECT*>(pObj);
        }
    }
    return nullptr;
}

template <typename TOBJECT>
INLINE bool CRTTITraits::AddEnum(MAP_RTTI_ENUM& EnumMap, PCXStr pszName, ENUM_ARY_ITEM& Items, PCXStr pszOrigin)
{
    assert(pszName != nullptr);
    assert(Items.GetSize() > 0);
    PAIR_RTTI_ENUM* pPair = EnumMap.Find(pszName);
    if (pPair == nullptr)
    {
#ifdef __MODERN_CXX_NOT_SUPPORTED
        CRTTIEnum* pRTTIEnum = MNEW CRTTIEnum(TOBJECT::ClassRTTI(), pszOrigin, Items);
#else
        CRTTIEnum* pRTTIEnum = MNEW CRTTIEnum(TOBJECT::ClassRTTI(), pszOrigin, std::move(Items));
#endif
        DEV_DEBUG(TF("  RTTITraits Add Enum at class = %s, enum name = %s(%s), enum type = %x, enum wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, VART_ENUM, pRTTIEnum);
        if (pRTTIEnum != nullptr)
        {
            return (EnumMap.Add(pszName, pRTTIEnum) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Enum at class = %s, enum name = %s(%s), enum type = %x, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, VART_ENUM);
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT>
INLINE const CRTTIEnum& CRTTITraits::GetEnum(PCXStr pszName)
{
    assert(pszName != nullptr);
    const CRTTIEnum* pRTTIEnum = TOBJECT::ClassRTTI().GetEnum(pszName);
    if ((pRTTIEnum != nullptr) && (pRTTIEnum->GetType() == VART_ENUM))
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum at class = %s, enum name = %s, enum type = %x, enum wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, VART_ENUM, pRTTIEnum);
        return *(pRTTIEnum);
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIEnum == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum at class = %s, enum name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum at class = %s, enum name = %s, enum type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIEnum->GetType(), VART_ENUM);
    }
#endif
    assert(0);
    return CRTTI::msc_EnumNone;
}

template <typename TOBJECT>
INLINE const CRTTIEnum& CRTTITraits::GetEnum(const CString& strName)
{
    assert(strName.Length() > 0);
    const CRTTIEnum* pRTTIEnum = TOBJECT::ClassRTTI().GetEnum(strName);
    if ((pRTTIEnum != nullptr) && (pRTTIEnum->GetType() == VART_ENUM))
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum2 at class = %s, enum name = %s, enum type = %x, enum wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, VART_ENUM, pRTTIEnum);
        return *(pRTTIEnum);
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIEnum == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum2 at class = %s, enum name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Enum2 at class = %s, enum name = %s, enum type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIEnum->GetType(), VART_ENUM);
    }
#endif
    assert(0);
    return CRTTI::msc_EnumNone;
}

template <typename TOBJECT, typename TVAR>
INLINE bool CRTTITraits::AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, TVAR TOBJECT::* pVar, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pVar != nullptr);
    PAIR_RTTI_VAR* pPair = VarMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTIVar<TOBJECT, TVAR>* pRTTIVar = MNEW CTRTTIVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER), pVar);
        DEV_DEBUG(TF("  RTTITraits Add Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER), *(reinterpret_cast<uintptr_t*>(&pVar)), pRTTIVar);
        if (pRTTIVar != nullptr)
        {
            return (VarMap.Add(pszName, pRTTIVar) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER), *(reinterpret_cast<uintptr_t*>(&pVar)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TVAR>
INLINE bool CRTTITraits::AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, const TVAR TOBJECT::* pVar, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pVar != nullptr);
    PAIR_RTTI_VAR* pPair = VarMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTICVar<TOBJECT, TVAR>* pRTTICVar = MNEW CTRTTICVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_CONST), pVar);
        DEV_DEBUG(TF("  RTTITraits Add Const Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST), *(reinterpret_cast<uintptr_t*>(&pVar)), pRTTICVar);
        if (pRTTICVar != nullptr)
        {
            return (VarMap.Add(pszName, pRTTICVar) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Const Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST), *(reinterpret_cast<uintptr_t*>(&pVar)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TVAR>
INLINE bool CRTTITraits::AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, TVAR* pVar, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pVar != nullptr);
    PAIR_RTTI_VAR* pPair = VarMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTISVar<TOBJECT, TVAR>* pRTTISVar = MNEW CTRTTISVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_STATIC), pVar);
        DEV_DEBUG(TF("  RTTITraits Add Static Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC), pVar, pRTTISVar);
        if (pRTTISVar != nullptr)
        {
            return (VarMap.Add(pszName, pRTTISVar) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Static Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC), pVar);
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TVAR>
INLINE bool CRTTITraits::AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, const TVAR* pVar, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pVar != nullptr);
    PAIR_RTTI_VAR* pPair = VarMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTISCVar<TOBJECT, TVAR>* pRTTISCVar = MNEW CTRTTISCVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_CONST), pVar);
        DEV_DEBUG(TF("  RTTITraits Add Static Const Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_CONST), pVar, pRTTISCVar);
        if (pRTTISCVar != nullptr)
        {
            return (VarMap.Add(pszName, pRTTISCVar) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Static Const Var at class = %s, var name = %s(%s), var type = %x, var ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_CONST), pVar);
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTIVar<TOBJECT, TVAR> CRTTITraits::GetVar(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Var1 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER), pRTTIType);
        return *(static_cast<const CTRTTIVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Var1 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER));
    }
#endif
    assert(0);
    return CTRTTIVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTIVar<TOBJECT, TVAR> CRTTITraits::GetVar(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Var2 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER), pRTTIType);
        return *(static_cast<const CTRTTIVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Var2 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Var2 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER));
    }
#endif
    assert(0);
    return CTRTTIVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTIVar<TOBJECT, TVAR> CRTTITraits::GetVar(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Var3 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER), pRTTIType);
        return *(static_cast<const CTRTTIVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Var3 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Var3 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER));
    }
#endif
    assert(0);
    return CTRTTIVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTICVar<TOBJECT, TVAR> CRTTITraits::GetConstVar(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var1 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTICVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var1 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTICVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTICVar<TOBJECT, TVAR> CRTTITraits::GetConstVar(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var2 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTICVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var2 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var2 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTICVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTICVar<TOBJECT, TVAR> CRTTITraits::GetConstVar(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var3 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTICVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var3 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Var3 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTICVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISVar<TOBJECT, TVAR> CRTTITraits::GetStaticVar(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var1 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_STATIC), pRTTIType);
        return *(static_cast<const CTRTTISVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var1 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC));
    }
#endif
    assert(0);
    return CTRTTISVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISVar<TOBJECT, TVAR> CRTTITraits::GetStaticVar(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var2 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_STATIC), pRTTIType);
        return *(static_cast<const CTRTTISVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var2 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var2 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC));
    }
#endif
    assert(0);
    return CTRTTISVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISVar<TOBJECT, TVAR> CRTTITraits::GetStaticVar(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var3 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_STATIC), pRTTIType);
        return *(static_cast<const CTRTTISVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var3 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Var3 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC));
    }
#endif
    assert(0);
    return CTRTTISVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISCVar<TOBJECT, TVAR> CRTTITraits::GetStaticConstVar(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ( (pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var1 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_STATIC|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTISCVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var1 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTISCVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISCVar<TOBJECT, TVAR> CRTTITraits::GetStaticConstVar(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(pszName);
    if ( (pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var2 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_STATIC|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTISCVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var2 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var2 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTISCVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TVAR>
INLINE const CTRTTISCVar<TOBJECT, TVAR> CRTTITraits::GetStaticConstVar(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetVar(strName);
    if ( (pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_CONST)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var3 at class = %s, var name = %s, var type = %x, var wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_STATIC|VART_CONST), pRTTIType);
        return *(static_cast<const CTRTTISCVar<TOBJECT, TVAR>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var3 at class = %s, var name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Const Var3 at class = %s, var name = %s, var type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_CONST));
    }
#endif
    assert(0);
    return CTRTTISCVar<TOBJECT, TVAR>(TOBJECT::ClassRTTI(), nullptr, 0);
}

#ifdef __MODERN_CXX_NOT_SUPPORTED
template <typename TOBJECT, typename TFUNC>
INLINE bool CRTTITraits::AddFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TFUNC pFunc, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTIFunc<TOBJECT, TFUNC>* pRTTIFunc = MNEW CTRTTIFunc<TOBJECT, TFUNC>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)), pRTTIFunc);
        if (pRTTIFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTIFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Func at class = %s, func name = %s, func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TCFUNC>
INLINE bool CRTTITraits::AddConstFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TCFUNC pFunc, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTICFunc<TOBJECT, TCFUNC>* pRTTICFunc = MNEW CTRTTICFunc<TOBJECT, TCFUNC>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Const Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)), pRTTICFunc);
        if (pRTTICFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTICFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Const Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TSFUNC>
INLINE bool CRTTITraits::AddStaticFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TSFUNC pFunc, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTISFunc<TOBJECT, TSFUNC>* pRTTISFunc = MNEW CTRTTISFunc<TOBJECT, TSFUNC>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Static Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pFunc, pRTTISFunc);
        if (pRTTISFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTISFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Static Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pFunc);
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TFUNC>
INLINE const CTRTTIFunc<TOBJECT, TFUNC> CRTTITraits::GetFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TFUNC>
INLINE const CTRTTIFunc<TOBJECT, TFUNC> CRTTITraits::GetFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TFUNC>
INLINE const CTRTTIFunc<TOBJECT, TFUNC> CRTTITraits::GetFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TCFUNC>
INLINE const CTRTTICFunc<TOBJECT, TCFUNC> CRTTITraits::GetConstFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TCFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TCFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TCFUNC>
INLINE const CTRTTICFunc<TOBJECT, TCFUNC> CRTTITraits::GetConstFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TCFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TCFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TCFUNC>
INLINE const CTRTTICFunc<TOBJECT, TCFUNC> CRTTITraits::GetConstFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TCFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TCFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TSFUNC>
INLINE const CTRTTISFunc<TOBJECT, TSFUNC> CRTTITraits::GetStaticFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TSFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TSFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TSFUNC>
INLINE const CTRTTISFunc<TOBJECT, TSFUNC> CRTTITraits::GetStaticFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TSFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TSFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TSFUNC>
INLINE const CTRTTISFunc<TOBJECT, TSFUNC> CRTTITraits::GetStaticFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TSFUNC>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TSFUNC>(TOBJECT::ClassRTTI(), nullptr, 0);
}

#else
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CRTTITraits::AddFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...), PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTIFunc<TOBJECT, TRET(TARGS...)>* pRTTIFunc = MNEW CTRTTIFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)), pRTTIFunc);
        if (pRTTIFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTIFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CRTTITraits::AddConstFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...) const, PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTICFunc<TOBJECT, TRET(TARGS...) const>* pRTTICFunc = MNEW CTRTTICFunc<TOBJECT, TRET(TARGS...) const>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Const Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)), pRTTICFunc);
        if (pRTTICFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTICFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Const Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), *(reinterpret_cast<uintptr_t*>(&pFunc)));
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CRTTITraits::AddStaticFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (* pFunc)(TARGS...), PCXStr pszOrigin, size_t stType)
{
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
    PAIR_RTTI_FUNC* pPair = FuncMap.Find(pszName);
    if (pPair == nullptr)
    {
        CTRTTISFunc<TOBJECT, TRET(TARGS...)>* pRTTISFunc = MNEW CTRTTISFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pFunc);
        DEV_DEBUG(TF("  RTTITraits Add Static Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pFunc, pRTTISFunc);
        if (pRTTISFunc != nullptr)
        {
            return (FuncMap.Add(pszName, pRTTISFunc) != nullptr);
        }
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Add Static Func at class = %s, func name = %s(%s), func type = %x, func ptr = %p, find name exist!"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pszOrigin, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pFunc);
    }
#endif
    assert(0);
    return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTIFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTIFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTIFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTIFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTIFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> CRTTITraits::GetConstFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TRET(TARGS...) const>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TRET(TARGS...) const>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> CRTTITraits::GetConstFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TRET(TARGS...) const>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TRET(TARGS...) const>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> CRTTITraits::GetConstFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_CONST|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_CONST|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTICFunc<TOBJECT, TRET(TARGS...) const>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Const Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_CONST|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTICFunc<TOBJECT, TRET(TARGS...) const>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTISFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetStaticFunc(const CRTTIType* pRTTIType, size_t stType)
{
    assert(pRTTIType != nullptr);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func1 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func1 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pRTTIType->GetOrigin(), pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTISFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetStaticFunc(PCXStr pszName, size_t stType)
{
    assert(pszName != nullptr);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(pszName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), pszName, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), pszName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func2 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), pszName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE const CTRTTISFunc<TOBJECT, TRET(TARGS...)> CRTTITraits::GetStaticFunc(const CString& strName, size_t stType)
{
    assert(strName.Length() > 0);
    const CRTTIType* pRTTIType = TOBJECT::ClassRTTI().GetFunc(strName);
    if ((pRTTIType != nullptr) && (pRTTIType->GetType() == (stType|VART_POINTER|VART_STATIC|VART_FUNCTION)))
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, func type = %x, func wrapper ptr = %p"),
                  TOBJECT::ClassRTTI().GetName(), *strName, (stType|VART_POINTER|VART_STATIC|VART_FUNCTION), pRTTIType);
        return *(static_cast<const CTRTTISFunc<TOBJECT, TRET(TARGS...)>*>(pRTTIType));
    }
#ifdef __RUNTIME_DEBUG__
    else if (pRTTIType == nullptr)
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, find failed!"),
                  TOBJECT::ClassRTTI().GetName(), *strName);
    }
    else
    {
        DEV_DEBUG(TF("  RTTITraits Get Static Func3 at class = %s, func name = %s, func type = %x <> %x"),
                  TOBJECT::ClassRTTI().GetName(), *strName, pRTTIType->GetType(), (stType|VART_POINTER|VART_STATIC|VART_FUNCTION));
    }
#endif
    assert(0);
    return CTRTTISFunc<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), nullptr, 0);
}
#endif


#endif // __RTTI_INL__
