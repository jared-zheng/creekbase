// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_MARKUP_INL__
#define __TARGET_MARKUP_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CUTF8
INLINE bool CUTF8::LocalToUTF8(const CString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    size_t stBuf = (stLen + 1) * ENT_UTF16_UTF8;
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PByte pszA = Buf.GetBuf();
    if (pszA != nullptr)
    {
        Int nRet = CXChar::Convert(*strIn, (Int)stLen, (PStr)pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
        assert(nRet > 0);
        pszA[nRet] = 0;
#else
    size_t stBuf = (stLen + 1) * ENT_UTF16_UTF8 + (stLen + 1) * sizeof(WChar);
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PByte pszA = Buf.GetBuf();
    PWStr pszW = (PWStr)(pszA + (stLen + 1) * ENT_UTF16_UTF8);
    if (pszA != nullptr)
    {
        Int nRet = CChar::Convert(*strIn, (Int)stLen, pszW, (Int)(stLen));
        assert(nRet > 0);
        pszW[nRet] = 0;

        nRet = CChar::Convert(pszW, nRet, (PStr)pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
        assert(nRet > 0);
        pszA[nRet] = 0;
#endif
        strOut.Attach(pszA, stBuf, (size_t)nRet);
        Buf.Detach();
        return true;
    }
    return false;
}

INLINE bool CUTF8::UTF8ToLocal(const CCString& strIn, CString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    size_t stBuf = (stLen + 1) * sizeof(WChar);
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PWStr pszW = (PWStr)Buf.GetBuf();
#else
    size_t stBuf = (stLen + 1) + (stLen + 1) * sizeof(WChar);
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PStr  pszL = (PStr)Buf.GetBuf();
    PWStr pszW = (PWStr)(pszL + (stLen + 1));
#endif
    if (pszW != nullptr)
    {
        Int nRet = CChar::Convert(*strIn, (Int)(stLen), pszW, (Int)(stLen), CP_UTF8);
        assert(nRet > 0);
        pszW[nRet] = 0;
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        strOut.Attach((PByte)pszW, stBuf, (size_t)nRet * sizeof(WChar));
        Buf.Detach();
#else
        nRet = CChar::Convert(pszW, nRet, pszL, (Int)(stLen));
        assert(nRet > 0);
        pszL[nRet] = 0;
        strOut.Attach((PByte)pszL, stBuf, (size_t)nRet);
        Buf.Detach();
#endif
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLTraits
INLINE bool CXMLTraits::CodePage(CString& strCodePage) const
{
    strCodePage = EncodingCodePage;
    XChar szEncoding[DECLL_MAX] = { 0 };
    if (::GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDEFAULTANSICODEPAGE|LOCALE_NOUSEROVERRIDE, szEncoding, DECLL_MAX) > 0)
    {
        strCodePage += szEncoding;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLDocument
INLINE bool CXMLDocument::Load(PByte pData, size_t stLen)
{
    bool bRet = false;
    Int nEncoding = CXChar::CheckBOM(pData);
    if ((m_eENCODING == ENT_UTF8) || (nEncoding == ENT_UTF8))
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Convert((PStr)(pData), -1, pszW, (Int)(stLen), CP_UTF8);
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) );
        if (pszW != nullptr)
        {
            PStr pszA = (PStr)(pszW + (stLen + 1));

            Int nRet = CXChar::Convert((PStr)(pData), -1, pszW, (Int)(stLen), CP_UTF8);
            assert(nRet > 0);
            pszW[nRet] = 0;

            nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszW );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF16) || (nEncoding == ENT_UTF16) || (nEncoding == ENT_UTF16R))
    {
        PWStr pszW = (PWStr)(pData);
        if (nEncoding == ENT_UTF16R)
        {
            size_t stSize = (stLen + 1) / sizeof(WChar);
            for (size_t i = 0; i < stSize; ++i)
            {
                pszW[i] = CPlatform::ByteSwap((UShort)pszW[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        m_eENCODING = ENT_UTF16;
        bRet = Parse(pszW);
#else
        PXStr pszA = (PXStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
        if (pszA != nullptr)
        {
            Int nRet = CXChar::Convert(pszW, -1, pszA, (Int)(stLen * ENT_UTF16_UTF8));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszA );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF32) || (nEncoding == ENT_UTF32) || (nEncoding == ENT_UTF32R))
    {
        PUInt psz32 = (PUInt)(pData);
        if (nEncoding == ENT_UTF32R)
        {
            size_t stSize = (stLen + 1) / sizeof(UInt);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz32[i] = CPlatform::ByteSwap(psz32[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Utf32ToUtf16(psz32, -1, (PUShort)pszW, (Int)(stLen * ENT_UTF32_UTF16));
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
        if (pszW != nullptr)
        {
            PXStr pszA = (PStr)(pszW + (stLen + 1) * ENT_UTF32_UTF16);

            Int nRet = CXChar::Utf32ToUtf16(psz32, -1, (PUShort)pszW, (Int)(stLen * ENT_UTF32_UTF16));
            assert(nRet > 0);
            pszW[nRet] = 0;

            nRet = CXChar::Convert(pszW, -1, pszA, (Int)(stLen * ENT_UTF16_UTF8));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszW );
        }
#endif
    }
    else // ENT_LOCAL
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Convert((PCStr)pData, -1, pszW, (Int)(stLen));
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        m_eENCODING = ENT_LOCAL;
        bRet = Parse((PXStr)pData);
#endif
    }
    return bRet;
}

INLINE bool CXMLDocument::Save(CStream& stream, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    assert(stream.IsWrite());
    assert(stream.Size() > 0);
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        size_t stLen = Size();
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF80 << (UChar)BOML_UTF81 << (UChar)BOML_UTF82;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszA );
                    return true;
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszW != nullptr)
                {
                    PStr pszA = (PStr)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF160 << (UChar)BOML_UTF161;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                stream.Write(*strBuf, (size_t)stLen * sizeof(WChar));
                return true;
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
                if (pszW != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    stream.Write(pszW, (size_t)nRet * sizeof(WChar));

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF320 << (UChar)BOML_UTF321 << (UChar)BOML_UTF322 << (UChar)BOML_UTF323;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf16ToUtf32((PUShort)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( psz32 );
                    return true;
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * sizeof(UInt) );
                if (pszW != nullptr)
                {
                    PUInt psz32 = (PUInt)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Utf16ToUtf32((PUShort)pszW, nRet, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        default: // ENT_LOCAL, local mbcs, set CXMLDeclaration.SetEncoding as local before save
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8));
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszA );
                    return true;
                }
#else
                stream.Write(*strBuf, stLen);
                return true;
#endif
            }
        }
    }
    return false;
}

INLINE bool CXMLDocument::Save(CBufWriteStream& stream, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        size_t stLen = Size();
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszA );
                    return (stream.GetBuf() != nullptr);
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszW != nullptr)
                {
                    PStr pszA = (PStr)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                size_t stSize = bAddBOM ? (stLen * sizeof(WChar) + BOML_UTF16) : (stLen * sizeof(WChar));
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                        stream.Write(bHead, BOML_UTF16);
                    }
                    stream.Write(*strBuf, (size_t)stLen * sizeof(WChar));
                }
                return (stream.GetBuf() != nullptr);
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
                if (pszW != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(WChar) + BOML_UTF16) : (size_t)nRet * sizeof(WChar);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(pszW, (size_t)nRet * sizeof(WChar));
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf16ToUtf32((PUShort)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( psz32 );
                    return (stream.GetBuf() != nullptr);
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * sizeof(UInt) );
                if (pszW != nullptr)
                {
                    PUInt psz32 = (PUInt)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Utf16ToUtf32((PUShort)pszW, nRet, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        default: // ENT_LOCAL, local mbcs, set CXMLDeclaration.SetEncoding as local before save
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8));
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    if (stream.Attach((size_t)nRet))
                    {
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszA );
                    return (stream.GetBuf() != nullptr);
                }
#else
                if (stream.Attach(stLen))
                {
                    stream.Write(*strBuf, stLen);
                }
                return (stream.GetBuf() != nullptr);
#endif
            }
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CKVNode
INLINE bool CKVNode::Load(PByte pData, size_t stLen)
{
    bool bRet  = false;
    Int nEncoding = CXChar::CheckBOM(pData);
    if ((m_eENCODING == ENT_UTF8) || (nEncoding == ENT_UTF8))
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PStr  pszA = (PStr)(pData);
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Convert(pszA, -1, pszW, (Int)(stLen), CP_UTF8);
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) );
        if (pszW != nullptr)
        {
            PStr pszA = (PStr)(pszW + (stLen + 1));

            Int nRet = CXChar::Convert((PStr)(pData), -1, pszW, (Int)(stLen), CP_UTF8);
            assert(nRet > 0);
            pszW[nRet] = 0;

            nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszW );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF16) || (nEncoding == ENT_UTF16) || (nEncoding == ENT_UTF16R))
    {
        PWStr pszW = (PWStr)(pData);
        if (nEncoding == ENT_UTF16R)
        {
            size_t stSize = (stLen + 1) / sizeof(WChar);
            for (size_t i = 0; i < stSize; ++i)
            {
                pszW[i] = CPlatform::ByteSwap((UShort)pszW[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        m_eENCODING = ENT_UTF16;
        bRet = Parse(pszW);
#else
        PXStr pszA = (PXStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
        if (pszA != nullptr)
        {
            Int nRet = CXChar::Convert(pszW, -1, pszA, (Int)(stLen * ENT_UTF16_UTF8));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszA );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF32) || (nEncoding == ENT_UTF32) || (nEncoding == ENT_UTF32R))
    {
        PUInt psz32 = (PUInt)(pData);
        if (nEncoding == ENT_UTF32R)
        {
            size_t stSize = (stLen + 1) / sizeof(UInt);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz32[i] = CPlatform::ByteSwap(psz32[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Utf32ToUtf16(psz32, -1, (PUShort)pszW, (Int)(stLen * ENT_UTF32_UTF16));
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
        if (pszW != nullptr)
        {
            PXStr pszA = (PStr)(pszW + (stLen + 1) * ENT_UTF32_UTF16);

            Int nRet = CXChar::Utf32ToUtf16(psz32, -1, (PUShort)pszW, (Int)(stLen * ENT_UTF32_UTF16));
            assert(nRet > 0);
            pszW[nRet] = 0;

            nRet = CXChar::Convert(pszW, -1, pszA, (Int)(stLen * ENT_UTF16_UTF8));
            assert(nRet > 0);
            pszA[nRet] = 0;

            m_eENCODING = ENT_LOCAL;
            bRet = Parse(pszA);
            FREE( pszW );
        }
#endif
    }
    else // ENT_LOCAL
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
        if (pszW != nullptr)
        {
            Int nRet = CXChar::Convert((PCStr)pData, -1, pszW, (Int)(stLen));
            assert(nRet > 0);
            pszW[nRet] = 0;

            m_eENCODING = ENT_UTF16;
            bRet = Parse(pszW);
            FREE( pszW );
        }
#else
        m_eENCODING = ENT_LOCAL;
        PXStr pszA = (PXStr)pData;
        bRet = Parse(pszA);
#endif
    }
    return bRet;
}

INLINE bool CKVNode::Save(CStream& stream, bool bStyle, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    assert(stream.IsWrite());
    assert(stream.Size() > 0);
    if (GetType() != VART_NONE)
    {
        size_t stLen = Size(bStyle ? 0 : -1);
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(bStyle ? 0 : -1, strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF80 << (UChar)BOML_UTF81 << (UChar)BOML_UTF82;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszA );
                    return true;
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszW != nullptr)
                {
                    PStr pszA = (PStr)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF160 << (UChar)BOML_UTF161;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                stream.Write(*strBuf, (size_t)stLen * sizeof(WChar));
                return true;
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
                if (pszW != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen), (m_eENCODING == ENT_UTF8) ? CP_UTF8 : CP_DEFAULT);
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    stream.Write(pszW, (size_t)nRet * sizeof(WChar));

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF320 << (UChar)BOML_UTF321 << (UChar)BOML_UTF322 << (UChar)BOML_UTF323;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf16ToUtf32((PUShort)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( psz32 );
                    return true;
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * sizeof(UInt) );
                if (pszW != nullptr)
                {
                    PUInt psz32 = (PUInt)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Utf16ToUtf32((PUShort)pszW, nRet, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( pszW );
                    return true;
                }
#endif
            }
            break;
        default: // ENT_LOCAL
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8));
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    stream.Write(pszA, (size_t)nRet);

                    FREE( pszA );
                    return true;
                }
#else
                stream.Write(*strBuf, stLen);
                return true;
#endif
            }
        }
    }
    return false;
}

INLINE bool CKVNode::Save(CBufWriteStream& stream, bool bStyle, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    if (GetType() != VART_NONE)
    {
        size_t stLen = Size(bStyle ? 0 : -1);
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(bStyle ? 0 : -1, strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszA );
                    return (stream.GetBuf() != nullptr);
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszW != nullptr)
                {
                    PStr pszA = (PStr)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Convert(pszW, nRet, pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                size_t stSize = bAddBOM ? (stLen * sizeof(WChar) + BOML_UTF16) : stLen * sizeof(WChar);
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                        stream.Write(bHead, BOML_UTF16);
                    }
                    stream.Write(*strBuf, (size_t)stLen * sizeof(WChar));
                }
                return (stream.GetBuf() != nullptr);
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) );
                if (pszW != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(WChar) + BOML_UTF16) : (size_t)nRet * sizeof(WChar);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(pszW, (size_t)nRet * sizeof(WChar));
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf16ToUtf32((PUShort)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( psz32 );
                    return (stream.GetBuf() != nullptr);
                }
#else
                PWStr pszW = (PWStr)ALLOC( (stLen + 1) * sizeof(WChar) + (stLen + 1) * sizeof(UInt) );
                if (pszW != nullptr)
                {
                    PUInt psz32 = (PUInt)(pszW + (stLen + 1));

                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszW, (Int)(stLen));
                    assert(nRet > 0);
                    pszW[nRet] = 0;

                    nRet = CXChar::Utf16ToUtf32((PUShort)pszW, nRet, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( pszW );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        default: // ENT_LOCAL
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
                PStr pszA = (PStr)ALLOC( (stLen + 1) * ENT_UTF16_UTF8 );
                if (pszA != nullptr)
                {
                    Int nRet = CXChar::Convert(*strBuf, (Int)stLen, pszA, (Int)(stLen * ENT_UTF16_UTF8));
                    assert(nRet > 0);
                    pszA[nRet] = 0;

                    if (stream.Attach((size_t)nRet))
                    {
                        stream.Write(pszA, (size_t)nRet);
                    }
                    FREE( pszA );
                    return (stream.GetBuf() != nullptr);
                }
#else
                if (stream.Attach(stLen))
                {
                    stream.Write(*strBuf, stLen);
                }
                return (stream.GetBuf() != nullptr);
#endif
            }
        }
    }
    return false;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_MARKUP_INL__
