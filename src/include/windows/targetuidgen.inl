// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_UID_GEN_INL__
#define __TARGET_UID_GEN_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CUUIDGenerator
INLINE CUUIDGenerator::CUUIDGenerator(void)
{
}

INLINE CUUIDGenerator::~CUUIDGenerator(void)
{
}

INLINE CUUIDGenerator::CUUIDGenerator(const CUUIDGenerator&)
{
}

INLINE CUUIDGenerator& CUUIDGenerator::operator=(const CUUIDGenerator&)
{
    return (*this);
}

INLINE bool CUUIDGenerator::Create(CUUID& uuid)
{
    UID uid;
    if (::UuidCreate(&uid) == RPC_S_OK)
    {
        uuid.SetUUID(uid);
        return true;
    }
    return false;
}

INLINE bool CUUIDGenerator::Create(CString& strUUID, bool bAppend, CUUID::UUID_FROMAT eFormat)
{
    UID uid;
    if (::UuidCreate(&uid) == RPC_S_OK)
    {
        CUUID uuid;
        uuid.SetUUID(uid);
        uuid.ToString(strUUID, bAppend, eFormat);
        return true;
    }
    return false;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_UID_GEN_INL__
