// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __INI_H__
#define __INI_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "tstring.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CIni
class CIni : public MObject
{
public:
    CIni(PCXStr pszFile = nullptr);
    ~CIni(void);

    bool CreateINI(PCXStr pszFile);
    void SetINI(PCXStr pszFile, bool bNoReadOnly = false);

    bool IsExist(void);
    bool IsSection(PCXStr pszSection);
    bool CreateSetion(PCXStr pszSection);

    bool GetKeyValue(PCXStr pszSection, PCXStr pszKey, CString& str);
    template <size_t stLenT> bool GetKeyValue(PCXStr pszSection, PCXStr pszKey, CTStringFix<CXChar, stLenT>& strFix);
    bool GetKeyValue(PCXStr pszSection, PCXStr pszKey, PXStr pszValue, Int nLength);
    bool SetKeyValue(PCXStr pszSection, PCXStr pszKey, PCXStr pszValue);
    UInt GetKeyIntValue(PCXStr pszSection, PCXStr pszKey, Int nDefault = 0);
    bool SetKeyIntValue(PCXStr pszSection, PCXStr pszKey, Int nValue);
    bool GetKeyStructValue(PCXStr pszSection, PCXStr pszKey, void* pStruct, UInt uSizeStruct);
    bool SetKeyStructValue(PCXStr pszSection, PCXStr pszKey, void* pStruct, UInt uSizeStruct);
private:
    CIni(const CIni&);
    CIni& operator=(const CIni&);
public:
    XChar  m_szINI[LMT_MAX_PATH];
};

///////////////////////////////////////////////////////////////////
#include "ini.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __INI_H__
