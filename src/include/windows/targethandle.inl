// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_HANDLE_INL__
#define __TARGET_HANDLE_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CHandle : basic Kernel Objects handle, closed by CloseHandle
INLINE CHandle::CHandle(Handle h)
: m_Handle(h)
{
}

INLINE CHandle::~CHandle(void)
{
    Close();
}

INLINE CHandle::CHandle(const CHandle& aSrc)
: m_Handle(aSrc.m_Handle)
{
}

INLINE CHandle& CHandle::operator=(const CHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_Handle);
    }
    return (*this);
}

INLINE CHandle& CHandle::operator=(Handle h)
{
    if (m_Handle != h)
    {
        Attach(h);
    }
    return (*this);
}

INLINE bool CHandle::operator==(Handle h) const
{
    return (m_Handle == h);
}

INLINE bool CHandle::operator!=(Handle h) const
{
    return (m_Handle != h);
}

INLINE bool CHandle::operator==(const CHandle& aSrc) const
{
    return (m_Handle == aSrc.m_Handle);
}

INLINE bool CHandle::operator!=(const CHandle& aSrc) const
{
    return (m_Handle != aSrc.m_Handle);
}

INLINE CHandle::operator Handle(void) const
{
    return m_Handle;
}

INLINE bool CHandle::IsValid(void) const
{
    return ((m_Handle != HANDLE_INVALID) && (m_Handle != nullptr));
}

INLINE void CHandle::Attach(Handle h)
{
    Close();
    m_Handle = h;
}

INLINE Handle CHandle::Detach(void)
{
    Handle h = m_Handle;
    m_Handle = HANDLE_INVALID;
    return h;
}

INLINE void CHandle::Close(void)
{
    if ((m_Handle != HANDLE_INVALID) && (m_Handle != nullptr))
    {
        ::CloseHandle(m_Handle);
    }
    m_Handle = HANDLE_INVALID;
}

///////////////////////////////////////////////////////////////////
// CThreadAttr
INLINE CThreadAttr::CThreadAttr(LPSECURITY_ATTRIBUTES pSA)
: m_pSA(pSA)
, m_stStack(0)
, m_ulFlags(0)
{
}

INLINE CThreadAttr::~CThreadAttr(void)
{
}

INLINE CThreadAttr::CThreadAttr(const CThreadAttr& aSrc)
: m_pSA(aSrc.m_pSA)
, m_stStack(aSrc.m_stStack)
, m_ulFlags(aSrc.m_ulFlags)
{
}

INLINE CThreadAttr& CThreadAttr::operator=(const CThreadAttr& aSrc)
{
    if (this != &aSrc)
    {
        m_pSA     = aSrc.m_pSA;
        m_stStack = aSrc.m_stStack;
        m_ulFlags = aSrc.m_ulFlags;
    }
    return (*this);
}

INLINE void CThreadAttr::Reset(void)
{
    m_pSA     = nullptr;
    m_stStack = 0;
    m_ulFlags = 0;
}

///////////////////////////////////////////////////////////////////
// CThreadHandle
INLINE CThreadHandle::CThreadHandle(Handle h)
: CHandle(h)
, m_TId(0)
{
}

INLINE CThreadHandle::~CThreadHandle(void)
{
    Close();
}

INLINE CThreadHandle::CThreadHandle(const CThreadHandle& aSrc)
: CHandle(aSrc.m_Handle)
, m_TId(aSrc.m_TId)
{
}

INLINE CThreadHandle& CThreadHandle::operator=(const CThreadHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_Handle);
        m_TId = aSrc.m_TId;
    }
    return (*this);
}

INLINE CThreadHandle& CThreadHandle::operator=(Handle h)
{
    CHandle::operator=(h);
    return (*this);
}

INLINE bool CThreadHandle::operator==(Handle h) const
{
    return CHandle::operator==(h);
}

INLINE bool CThreadHandle::operator!=(Handle h) const
{
    return CHandle::operator!=(h);
}

INLINE bool CThreadHandle::operator==(const CThreadHandle& aSrc) const
{
    return (m_Handle == aSrc.m_Handle);
}

INLINE bool CThreadHandle::operator!=(const CThreadHandle& aSrc) const
{
    return (m_Handle != aSrc.m_Handle);
}

INLINE void CThreadHandle::Reset(void)
{
    Close();
    m_TId = 0;
}

///////////////////////////////////////////////////////////////////
// module: basic Kernel Objects handle, closed by FreeLibrary(dll or exe)
INLINE CModuleHandle::CModuleHandle(Module m)
: m_Module(m)
{
}

INLINE CModuleHandle::~CModuleHandle(void)
{
    Close();
}

INLINE CModuleHandle::CModuleHandle(const CModuleHandle& aSrc)
: m_Module(aSrc.m_Module)
{
}

INLINE CModuleHandle& CModuleHandle::operator=(const CModuleHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_Module);
    }
    return (*this);
}

INLINE CModuleHandle& CModuleHandle::operator=(Module m)
{
    if (m_Module != m)
    {
        Attach(m);
    }
    return (*this);
}

INLINE bool CModuleHandle::operator==(Module m) const
{
    return (m_Module == m);
}

INLINE bool CModuleHandle::operator!=(Module m) const
{
    return (m_Module != m);
}

INLINE bool CModuleHandle::operator==(const CModuleHandle& aSrc) const
{
    return (m_Module == aSrc.m_Module);
}

INLINE bool CModuleHandle::operator!=(const CModuleHandle& aSrc) const
{
    return (m_Module != aSrc.m_Module);
}

INLINE CModuleHandle::operator Module(void) const
{
    return m_Module;
}

INLINE bool CModuleHandle::IsValid(void) const
{
    return (m_Module != nullptr);
}

INLINE void CModuleHandle::Attach(Module m)
{
    Close();
    m_Module = m;
}

INLINE Module CModuleHandle::Detach(void)
{
    Module m = m_Module;
    m_Module = nullptr;
    return m;
}

INLINE void CModuleHandle::Close(void)
{
    if (m_Module != nullptr)
    {
        ::FreeLibrary(m_Module);
        m_Module = nullptr;
    }
}

INLINE FARPROC CModuleHandle::Find(PCStr pszSymbol)
{
    if (m_Module != nullptr)
    {
        return ::GetProcAddress(m_Module, pszSymbol);
    }
    return nullptr;
}

INLINE bool CModuleHandle::Load(PCXStr pszPath)
{
    Close();
    m_Module = ::LoadLibrary(pszPath);
    return (m_Module != nullptr);
}

///////////////////////////////////////////////////////////////////
// pid
INLINE PId CProcessHandle::GetParentId(void)
{
    return 0;
}

INLINE PId CProcessHandle::GetGroupId(PId pid)
{
    return (pid == 0) ? CPlatform::GetCurrentPId() : pid;
}

INLINE CProcessHandle::CProcessHandle(void)
: m_PId(0)
{
}

INLINE CProcessHandle::~CProcessHandle(void)
{
    Close();
}

INLINE CProcessHandle::CProcessHandle(const CProcessHandle&)
: m_PId(0)
{
}

INLINE CProcessHandle& CProcessHandle::operator=(const CProcessHandle&)
{
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CProcessHandle::CProcessHandle(CProcessHandle&& aSrc)
: m_PId(0)
{
    m_PId = aSrc.m_PId;
    m_Handle.Attach(aSrc.m_Handle.Detach());
    aSrc.Detach();
}

INLINE CProcessHandle& CProcessHandle::operator=(CProcessHandle&& aSrc)
{
    if (this != &aSrc)
    {
        Close();

        m_PId = aSrc.m_PId;
        m_Handle.Attach(aSrc.m_Handle.Detach());
        aSrc.Detach();
    }
    return (*this);
}
#endif

INLINE bool CProcessHandle::operator==(PId pid) const
{
    return (m_PId == pid);
}

INLINE bool CProcessHandle::operator!=(PId pid) const
{
    return (m_PId != pid);
}

INLINE CProcessHandle::operator PId(void) const
{
    return m_PId;
}

INLINE Handle CProcessHandle::Get(void) const
{
    return m_Handle;
}

INLINE bool CProcessHandle::IsValid(void) const
{
    return m_Handle.IsValid();
}

INLINE bool CProcessHandle::Check(void) const
{
    if (m_Handle.IsValid())
    {
        return (::WaitForSingleObject(m_Handle, TIMET_IGNORE) == WAIT_TIMEOUT);
    }
    return false;
}

INLINE void CProcessHandle::Attach(CProcessHandle& aSrc)
{
    if (this != &aSrc)
    {
        Close();

        m_PId = aSrc.m_PId;
        m_Handle.Attach(aSrc.m_Handle.Detach());
        aSrc.Detach();
    }
}

INLINE void CProcessHandle::Detach(void)
{
    m_PId = 0;
    m_Handle.Detach();
}

INLINE Int CProcessHandle::Wait(UInt uWait) const
{
    if (m_Handle.IsValid())
    {
        UInt uRet = (UInt)::WaitForSingleObject(m_Handle, uWait);
        DEV_DEBUG(TF("  CProcessHandle[%p] Wait time = %u ret = %d"), this, uWait, uRet);
        return (Int)uRet;
    }
    return (Int)RET_ERROR;
}

INLINE bool CProcessHandle::Open(PCXStr pszCMD, PCXStr pszPWD, UInt uFlag)
{
    Close();

    PROCESS_INFORMATION pi = { 0 };
    STARTUPINFO si = { 0 };
    si.cb = sizeof(STARTUPINFO);

    if (::CreateProcess(nullptr, (PXStr)pszCMD, nullptr, nullptr, FALSE, uFlag, nullptr, pszPWD, &si, &pi) != FALSE)
    {
        ::CloseHandle(pi.hThread);

        m_Handle = pi.hProcess;
        m_PId    = pi.dwProcessId;
        DEV_DEBUG(TF("  CProcessHandle[%p] pszCMD = %s, flag = %d, curr-folder = %s  handle = %p, id = %d"), this, pszCMD, uFlag, pszPWD, pi.hProcess, pi.dwProcessId);
        return true;
    }
    DEV_DEBUG(TF("  CProcessHandle[%p] pszCMD = %s, flag = %d, curr-folder = %s errno = %d"), this, pszCMD, uFlag, pszPWD, ::GetLastError());
    return false;
}

INLINE void CProcessHandle::Close(void)
{
    if (m_Handle.IsValid())
    {
        UInt uRet = (UInt)::WaitForSingleObject(m_Handle, TIMET_IGNORE);
        DEV_DEBUG(TF("  CProcessHandle[%p] Close ret = %d"), this, uRet);
        if ((uRet != WAIT_FAILED) && (uRet != WAIT_OBJECT_0))
        {
            ::TerminateProcess(m_Handle, WAIT_TIMEOUT);
        }
        m_Handle.Close();
        m_PId = 0;
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_HANDLE_INL__
