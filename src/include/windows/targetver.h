#pragma once

// The following macros define the minimum required platform.  The minimum required platform
// is the earliest version of Windows, Internet Explorer etc. that has the necessary features to run
// your application.  The macros work by enabling all features available on platform versions up to and
// including the version specified.

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.

#if _USING_V110_SDK71_
#pragma message("---Windows XP Support---")
// windows xpsp3 : 0x0502
// windows vista : 0x0600
// windows 7     : 0x0601
// windows 8     : 0x0602
// windows 8.1   : 0x0603
// windows 10    : 0x0A00
// for Windows xp sp3 specified, comment all if support Windows xp sp3 is excluded
#ifndef WINVER                  // Specifies that the minimum required platform is Windows xp sp3.
#define WINVER 0x0502           // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows xp sp3.
#define _WIN32_WINNT WINVER     // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS          // Specifies that the minimum required platform is Windows xp sp3.
#define _WIN32_WINDOWS WINVER   // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_IE               // Specifies that the minimum required platform is Internet Explorer 7.0.
#define _WIN32_IE 0x0700        // Change this to the appropriate value to target other versions of IE.
#endif

#endif // _USING_V110_SDK71_

#include <sdkddkver.h>

#ifndef NTDDI_WIN8
    #define NTDDI_WIN8    0x06020000
#endif

#ifndef NTDDI_WINBLUE
    #define NTDDI_WINBLUE 0x06030000
#endif

///////////////////////////////////////////////////////////////////
#ifndef __RUNTIME_STATIC__

#define COMMON_AUTHOR                      "CREEK Studio"
#define COMMON_COPYRIGHT                   "Copyright 2007-2022 CREEK Studio. All Rights Reserved."

#define COMMON_MAJOR_VERSION               1
#define COMMON_MINOR_VERSION               6

#define COMMON_STRING_VERSION              "1, 6"
// PRODUCT_REVISION_VERSION = 1 + (git log | grep -e 'commit [a-zA-Z0-9]*' | wc -l)

#endif
