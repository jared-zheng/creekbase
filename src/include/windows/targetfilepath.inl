// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_FILE_PATH_INL__
#define __TARGET_FILE_PATH_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CFileAttr
INLINE CFileAttr::CFileAttr(void)
{
    MM_SAFE::Set(&m_Attr, 0, sizeof(FILEATTR));
}

INLINE CFileAttr::CFileAttr(const FILEATTR& Attr)
{
    MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &Attr, sizeof(FILEATTR));
}

INLINE CFileAttr::~CFileAttr(void)
{
}

INLINE CFileAttr::CFileAttr(const CFileAttr& aSrc)
{
    MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &(aSrc.m_Attr), sizeof(FILEATTR));
}

INLINE CFileAttr& CFileAttr::operator=(const CFileAttr& aSrc)
{
    if (this != &aSrc)
    {
        MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &(aSrc.m_Attr), sizeof(FILEATTR));
    }
    return (*this);
}

INLINE FILEATTR& CFileAttr::Attr(void)
{
    return m_Attr;
}

INLINE const FILEATTR& CFileAttr::Attr(void) const
{
    return m_Attr;
}

INLINE ULLong CFileAttr::Size(void) const
{
    ULARGE_INTEGER uiSize;
    uiSize.HighPart = m_Attr.nFileSizeHigh;
    uiSize.LowPart  = m_Attr.nFileSizeLow;
    return uiSize.QuadPart;
}

INLINE time_t CFileAttr::CTime(void) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftCreationTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftCreationTime.dwLowDateTime;
    return (time_t)uiTime.QuadPart;
}

INLINE time_t CFileAttr::ATime(void) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftLastAccessTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftLastAccessTime.dwLowDateTime;
    return (time_t)uiTime.QuadPart;
}

INLINE time_t CFileAttr::MTime(void) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftLastWriteTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftLastWriteTime.dwLowDateTime;
    return (time_t)uiTime.QuadPart;
}

INLINE bool CFileAttr::Size(ULLong ullSize) const
{
    ULARGE_INTEGER uiSize;
    uiSize.HighPart = m_Attr.nFileSizeHigh;
    uiSize.LowPart  = m_Attr.nFileSizeLow;
    return (uiSize.QuadPart == ullSize);
}

INLINE bool CFileAttr::CTime(time_t tc) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftCreationTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftCreationTime.dwLowDateTime;
    return ((time_t)uiTime.QuadPart == tc);
}

INLINE bool CFileAttr::ATime(time_t ta) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftLastAccessTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftLastAccessTime.dwLowDateTime;
    return ((time_t)uiTime.QuadPart == ta);
}

INLINE bool CFileAttr::MTime(time_t tm) const
{
    ULARGE_INTEGER uiTime;
    uiTime.HighPart = m_Attr.ftLastWriteTime.dwHighDateTime;
    uiTime.LowPart  = m_Attr.ftLastWriteTime.dwLowDateTime;
    return ((time_t)uiTime.QuadPart == tm);
}

///////////////////////////////////////////////////////////////////
// CFileFind
INLINE void CFileFind::SetAttr(const FILEFIND& Find)
{
    attr.m_Attr.dwFileAttributes = Find.dwFileAttributes;
    attr.m_Attr.ftCreationTime   = Find.ftCreationTime;
    attr.m_Attr.ftLastAccessTime = Find.ftLastAccessTime;
    attr.m_Attr.ftLastWriteTime  = Find.ftLastWriteTime;
    attr.m_Attr.nFileSizeHigh    = Find.nFileSizeHigh;
    attr.m_Attr.nFileSizeLow     = Find.nFileSizeLow;
}

INLINE CFileAttr& CFileFind::GetAttr(void)
{
    return attr;
}

INLINE const CFileAttr& CFileFind::GetAttr(void) const
{
    return attr;
}

INLINE FILEATTR& CFileFind::Attr(void)
{
    return attr.Attr();
}

INLINE const FILEATTR& CFileFind::Attr(void) const
{
    return attr.Attr();
}

INLINE ULLong CFileFind::Size(void) const
{
    return attr.Size();
}

INLINE time_t CFileFind::CTime(void) const
{
    return attr.CTime();
}

INLINE time_t CFileFind::ATime(void) const
{
    return attr.ATime();
}

INLINE time_t CFileFind::MTime(void) const
{
    return attr.MTime();
}

INLINE bool CFileFind::Size(ULLong ullSize) const
{
    return attr.Size(ullSize);
}

INLINE bool CFileFind::CTime(time_t tc) const
{
    return attr.CTime(tc);
}

INLINE bool CFileFind::ATime(time_t ta) const
{
    return attr.ATime(ta);
}

INLINE bool CFileFind::MTime(time_t tm) const
{
    return attr.MTime(tm);
}

///////////////////////////////////////////////////////////////////
// CFilePath
SELECTANY CPCXStr CFilePath::ModulePath    = TF("ModulePath");
SELECTANY CPCXStr CFilePath::CurFolder     = TF(".");
SELECTANY CPCXStr CFilePath::UpperFolder   = TF("..");
SELECTANY XChar   CFilePath::DotChar       = TF('.');
SELECTANY XChar   CFilePath::SlashChar     = TF('/');
SELECTANY XChar   CFilePath::BackSlashChar = TF('\\');
SELECTANY XChar   CFilePath::PathSepChar   = TF('\\');

INLINE CFilePath::CFilePath(bool bModulePath)
: m_FilePath(CContainerTraits::SIZED_GROW, CContainerTraits::HASHD_SIZE)
{
    if (bModulePath)
    {
        SetModulePath();
    }
}

INLINE CFilePath::~CFilePath(void)
{
    m_FilePath.RemoveAll();
}

INLINE CFilePath::CFilePath(const CFilePath&)
{
}

INLINE CFilePath& CFilePath::operator=(const CFilePath&)
{
    return (*this);
}

INLINE bool CFilePath::IsExist(PCXStr pszPath, PCXStr pszKey)
{
    ULong ulRet = INVALID_FILE_ATTRIBUTES;
    if (pszKey == nullptr)
    {
        ulRet = ::GetFileAttributes(pszPath);
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is exist file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        ulRet = ::GetFileAttributes(*strPath);
    }
    return (ulRet != INVALID_FILE_ATTRIBUTES);
}

INLINE bool CFilePath::IsFile(PCXStr pszPath, PCXStr pszKey)
{
    ULong ulRet = INVALID_FILE_ATTRIBUTES;
    if (pszKey == nullptr)
    {
        ulRet = ::GetFileAttributes(pszPath);
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is file file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        ulRet = ::GetFileAttributes(*strPath);
    }
    if (ulRet != INVALID_FILE_ATTRIBUTES)
    {
        return ((ulRet & FILE_ATTRIBUTE_DIRECTORY) == 0);
    }
    return false;
}

INLINE bool CFilePath::IsFolder(PCXStr pszPath, PCXStr pszKey)
{
    ULong ulRet = INVALID_FILE_ATTRIBUTES;
    if (pszKey == nullptr)
    {
        ulRet = ::GetFileAttributes(pszPath);
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is folder file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        ulRet = ::GetFileAttributes(*strPath);
    }
    if (ulRet != INVALID_FILE_ATTRIBUTES)
    {
        return ((ulRet & FILE_ATTRIBUTE_DIRECTORY) != 0);
    }
    return false;
}

INLINE bool CFilePath::FileAttr(CFileAttr& FileAttr, PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] file attr get file path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    if (::GetFileAttributesEx(*strPath, GetFileExInfoStandard, (void*)&(FileAttr.m_Attr)) == FALSE)
    {
        DEV_DEBUG(TF("  File-Path[%p] file attr get file path %s return error code %d"), this, *strPath, ::GetLastError());
        return false;
    }
    return true;
}

INLINE bool CFilePath::Copy(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting, PCXStr pszDstKey, PCXStr pszSrcKey)
{
    CString strDst;
    if ((pszDstKey != nullptr) && (GetPath(pszDstKey, strDst) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] copy get dst file path %s[key %s] failed"), this, pszDst, pszDstKey);
        return false;
    }
    strDst += pszDst;
    if (CheckFilePath(strDst, bReplaceExisting))
    {
        CString strSrc;
        if ((pszSrcKey != nullptr) && (GetPath(pszSrcKey, strSrc) == false))
        {
            DEV_DEBUG(TF("  File-Path[%p] copy get src file path %s[key %s] failed"), this, pszSrc, pszSrcKey);
            return false;
        }
        strSrc += pszSrc;
        DEV_DEBUG(TF("  File-Path[%p] copy %s[key %s] to %s[key %s]"), this, *strSrc, pszSrcKey, *strDst, pszDstKey);
        return (::CopyFile(*strSrc, *strDst, FALSE) != FALSE);
    }
    return false;
}

INLINE bool CFilePath::Move(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting, PCXStr pszDstKey, PCXStr pszSrcKey)
{
    CString strDst;
    if ((pszDstKey != nullptr) && (GetPath(pszDstKey, strDst) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] move get dst file path %s[key %s] failed"), this, pszDst, pszDstKey);
        return false;
    }
    strDst += pszDst;
    if (CheckFilePath(strDst, bReplaceExisting))
    {
        CString strSrc;
        if ((pszSrcKey != nullptr) && (GetPath(pszSrcKey, strSrc) == false))
        {
            DEV_DEBUG(TF("  File-Path[%p] move get src file path %s[key %s] failed"), this, pszSrc, pszSrcKey);
            return false;
        }
        strSrc += pszSrc;
        DEV_DEBUG(TF("  File-Path[%p] move %s[key %s] to %s[key %s]"), this, *strSrc, pszSrcKey, *strDst, pszDstKey);
        return (::MoveFile(*strSrc, *strDst) != FALSE);
    }
    return false;
}

INLINE bool CFilePath::Delete(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] delete get file path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    DEV_DEBUG(TF("  File-Path[%p] delete %s[key %s]"), this, *strPath, pszKey);
    ULong ulRet = ::GetFileAttributes(*strPath);
    if (ulRet != INVALID_FILE_ATTRIBUTES) // file exist
    {
        ::SetFileAttributes(*strPath, FILE_ATTRIBUTE_NORMAL);
        if (ulRet & FILE_ATTRIBUTE_DIRECTORY)
        {
            return (::RemoveDirectory(*strPath) != FALSE);
        }
        else
        {
            return (::DeleteFile(*strPath) != FALSE);
        }
    }
    return false;
}

INLINE Int CFilePath::Find(CTList<CString>& strFiles, PCXStr pszPath, PCXStr pszKey, Int nFlag)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] find get file path %s[key %s] failed"), this, pszPath, pszKey);
        return RET_ERROR;
    }
    strPath += pszPath;
    ULong ulRet = ::GetFileAttributes(*strPath);
    if (ulRet == INVALID_FILE_ATTRIBUTES)
    {
        Int nPos = strPath.Find(PathSepChar, 0, true);
        if (nPos >= 0)
        {
            ++nPos;
            CString strMatch = strPath.RightPos((size_t)nPos);
            strPath.ResetLength((size_t)nPos);
            if ((nFlag & FIND_FLAG_RELATIVE) == 0)
            {
                nPos = 0;
            }
            return FindFiles(strFiles, strPath, strMatch, nFlag, (size_t)nPos);
        }
        DEV_DEBUG(TF("  File-Path[%p] find %s CString::Find find \\ failed"), this, *strPath);
    }
    else
    {
        DEV_DEBUG(TF("  File-Path[%p] find %s is full path as a file or folder"), this, *strPath);
    }
    return RET_ERROR;
}

INLINE Int CFilePath::Find(CTList<CFileFind>& lFinds, PCXStr pszPath, PCXStr pszKey, Int nFlag, Int nLevel)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path 2[%p] find get file path %s[key %s] failed"), this, pszPath, pszKey);
        return RET_ERROR;
    }
    strPath += pszPath;
    ULong ulRet = ::GetFileAttributes(*strPath);
    if (ulRet == INVALID_FILE_ATTRIBUTES)
    {
        Int nPos = strPath.Find(PathSepChar, 0, true);
        if (nPos >= 0)
        {
            ++nPos;
            CString strMatch = strPath.RightPos((size_t)nPos);
            strPath.ResetLength((size_t)nPos);
            if ((nFlag & FIND_FLAG_RELATIVE) == 0)
            {
                nPos = 0;
            }
            return FindFiles(lFinds, strPath, strMatch, nFlag, nLevel, (size_t)nPos);
        }
        DEV_DEBUG(TF("  File-Path 2[%p] find %s CString::Find find \\ failed"), this, *strPath);
    }
    else
    {
        DEV_DEBUG(TF("  File-Path 2[%p] find %s is full path as a file or folder"), this, *strPath);
    }
    return RET_ERROR;
}

INLINE bool CFilePath::CreateFolder(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] create folder get folder path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    if (strPath.Length() > 0)
    {
        if (strPath[strPath.Length() - 1] != PathSepChar)
        {
            strPath += PathSepChar;
        }
        return CreateFolders(strPath);
    }
    return false;
}

INLINE bool CFilePath::DeleteFolder(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] delete folder get folder path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    if (strPath.Length() > 0)
    {
        if (strPath[strPath.Length() - 1] != PathSepChar)
        {
            strPath += PathSepChar;
        }
        return DeleteFolders(strPath);
    }
    return false;
}

INLINE bool CFilePath::SetPath(PCXStr pszKey, PCXStr pszPath)
{
    CSyncLockScope scope(m_FPLock);
    if ((pszKey != nullptr) && (pszPath != nullptr) && (*pszPath != 0))
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair == nullptr)
        {
            PINDEX index = m_FilePath.Add(pszKey);
            if (index != nullptr)
            {
                pPair = m_FilePath.GetAt(index);
                DEV_DEBUG(TF("  File-Path[%p] set path add [key %s]"), this, pszKey);
            }
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] key already has path %s"), this, pszPath, pszKey, pPair->m_V.GetBuffer());
        }
        if (pPair != nullptr)
        {
            pPair->m_V = pszPath;
            if (pPair->m_V[pPair->m_V.Length() - 1] != PathSepChar)
            {
                pPair->m_V += PathSepChar;
            }
            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] okay"), this, pPair->m_V.GetBuffer(), pszKey);
            return true;
        }
    }
    else if (pszKey != nullptr)
    {
        m_FilePath.Remove(pszKey);
        DEV_DEBUG(TF("  File-Path[%p] set path [key %s] remove okay"), this, pszKey);
        return true;
    }
    return false;
}

INLINE bool CFilePath::GetFullPath(PCXStr pszKey, CString& strPath)
{
    CSyncLockScope scope(m_FPLock);
    if (pszKey != nullptr)
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair != nullptr)
        {

            if (strPath.IsEmpty() == false)
            {
                CString strFullPath = pPair->m_V;
                strFullPath += strPath;
#ifndef __MODERN_CXX_NOT_SUPPORTED
                strPath = std::move(strFullPath);
#else
                strPath = strFullPath;
#endif
            }
            else
            {
                strPath = pPair->m_V;
            }
            return true;
        }
    }
    return false;
}

INLINE bool CFilePath::GetFullPath(PCXStr pszKey, PXStr pszPath, size_t stLen)
{
    CSyncLockScope scope(m_FPLock);
    if ((pszKey != nullptr) && (pszPath != nullptr) && (stLen > 0))
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair != nullptr)
        {
            if (*pszPath != 0)
            {
                CString strFullPath = pPair->m_V;
                strFullPath += pszPath;
                CXChar::Copy(pszPath, stLen, strFullPath.GetBuffer());
            }
            else
            {
                CXChar::Copy(pszPath, stLen, pPair->m_V.GetBuffer());
            }
            return true;
        }
    }
    return false;
}

INLINE bool CFilePath::CheckFilePath(const CString& strPath, bool bReplaceExisting)
{
    ULong ulRet = ::GetFileAttributes(*strPath);
    if (ulRet != INVALID_FILE_ATTRIBUTES) // file exist
    {
        if (bReplaceExisting == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] CheckFilePath %s file exist"), this, *strPath);
            return false;
        }
        if (ulRet & FILE_ATTRIBUTE_READONLY)
        {
            ulRet &= (~FILE_ATTRIBUTE_READONLY);
            ::SetFileAttributes(*strPath, ulRet);
        }
    }
    return true;
}

INLINE Int CFilePath::FindFiles(CTList<CString>& strFiles, CString& strPath, const CString& strMatch, Int nFlag, size_t stRelative)
{
    assert(strPath.Length() > 0);
    size_t stLen = strPath.Length();
    assert(strPath[stLen - 1] == PathSepChar);

    strPath += strMatch;
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    Int nCount = 0;
    WIN32_FIND_DATA FindData ={ 0 };
    Handle hFile = ::FindFirstFile(*strPath, &FindData);
    if (hFile != HANDLE_INVALID)
    {
        do
        {
            if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if ((CXChar::Cmp(FindData.cFileName, CurFolder)   == 0) ||
                    (CXChar::Cmp(FindData.cFileName, UpperFolder) == 0))
                {
                    continue;
                }
            }
            strPath.ResetLength(stLen);
            strPath += FindData.cFileName;
            PCXStr pszName = (nFlag & FIND_FLAG_FULLPATH) ? strPath.GetBuffer(stRelative) : strPath.GetBuffer(stLen);

            if ((nFlag & FIND_FLAG_FILE) && ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0))
            {
                strFiles.AddTail(pszName);
                ++nCount;
            }
            else if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if (nFlag & FIND_FLAG_FOLDER)
                {
                    strFiles.AddTail(pszName);
                    ++nCount;
                }
                if ((nFlag & FIND_FLAG_NOSUBFOLDER) == 0)
                {
                    strPath += PathSepChar;
                    nCount += FindFiles(strFiles, strPath, strMatch, nFlag, stRelative);
                }
            }

        } while (::FindNextFile(hFile, &FindData));
        ::FindClose(hFile);
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    return nCount;
}

INLINE Int CFilePath::FindFiles(CTList<CFileFind>& lFinds, CString& strPath, const CString& strMatch, Int nFlag, Int nLevel, size_t stRelative)
{
    assert(strPath.Length() > 0);
    size_t stLen = strPath.Length();
    assert(strPath[stLen - 1] == PathSepChar);

    Int nThisLevel = (nLevel - 1);

    strPath += strMatch;
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    Int nCount = 0;
    WIN32_FIND_DATA FindData ={ 0 };
    Handle hFile = ::FindFirstFile(*strPath, &FindData);
    if (hFile != HANDLE_INVALID)
    {
        do
        {
            if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if ((CXChar::Cmp(FindData.cFileName, CurFolder)   == 0) ||
                    (CXChar::Cmp(FindData.cFileName, UpperFolder) == 0))
                {
                    continue;
                }
            }
            strPath.ResetLength(stLen);
            strPath += FindData.cFileName;
            PCXStr pszName = (nFlag & FIND_FLAG_FULLPATH) ? strPath.GetBuffer(stRelative) : strPath.GetBuffer(stLen);

            if ((nFlag & FIND_FLAG_FILE) && ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0))
            {
                lFinds.AddTail();
                CFileFind& Find = lFinds.GetTail();
                Find.strFile = pszName;
                Find.SetAttr(FindData);
                ++nCount;
            }
            else if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if (nFlag & FIND_FLAG_FOLDER)
                {
                    lFinds.AddTail();
                    CFileFind& Find = lFinds.GetTail();
                    Find.strFile = pszName;
                    Find.SetAttr(FindData);
                    ++nCount;
                }
                if ((nFlag & FIND_FLAG_NOSUBFOLDER) == 0)
                {
                    if (nThisLevel > 0)
                    {
                        strPath += PathSepChar;
                        nCount += FindFiles(lFinds, strPath, strMatch, nFlag, nThisLevel, stRelative);
                    }
                }
            }

        } while (::FindNextFile(hFile, &FindData));
        ::FindClose(hFile);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    return nCount;
}

INLINE bool CFilePath::CreateFolders(CString& strPath)
{
    bool bRet = true;
    PXStr pszPath = *strPath;

    if (::CreateDirectory(pszPath, nullptr) == FALSE)
    {
        ULong ulRet = ::GetLastError();
        if (ulRet == ERROR_ALREADY_EXISTS)
        {
            DEV_DEBUG(TF("  File-Path[%p] create folder %s exists already"), this, pszPath);
            // return true since folder exists
        }
        else if (ulRet == ERROR_PATH_NOT_FOUND)
        {
///////////////////////////////////////////////////////////////////////////////////////////////////////////
            PXStr p = pszPath;
            do
            {
                p = (PXStr)CXChar::Chr(p, PathSepChar);
                if (p != nullptr)
                {
                    *p = 0;
                    ulRet = ::GetFileAttributes(pszPath);
                    if ((ulRet == INVALID_FILE_ATTRIBUTES) &&
                        (::CreateDirectory(pszPath, nullptr) == FALSE))
                    {
                        DEV_DEBUG(TF("  File-Path[%p] create folder %s failed return error code %d"), this, pszPath, ::GetLastError());
                        bRet = false;
                        break;
                    }
                    *p = PathSepChar;
                    ++p;
                }
                else
                {
                    DEV_DEBUG(TF("  File-Path[%p] create folder %s failed"), this, pszPath);
                    bRet = false;
                    break;
                }
            } while ((p != nullptr) && (*p != 0));
///////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] create folder %s return error code %d"), this, pszPath, ulRet);
            bRet = false;
        }
    }
    return bRet;
}

INLINE bool CFilePath::DeleteFolders(CString& strPath)
{
    bool   bRet = true;
    size_t stLen = strPath.Length();

    if (::RemoveDirectory(*strPath) == FALSE)
    {
        ULong ulRet = ::GetLastError();
        if (ulRet == ERROR_DIR_NOT_EMPTY)
        {
            strPath += TF('*');
///////////////////////////////////////////////////////////////////////////////////////////////////////////
            WIN32_FIND_DATA FindData ={ 0 };
            Handle hFile = ::FindFirstFile(*strPath, &FindData);
            if (hFile != HANDLE_INVALID)
            {
                do
                {
                    if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                        if ((CXChar::Cmp(FindData.cFileName, CurFolder)   == 0) ||
                            (CXChar::Cmp(FindData.cFileName, UpperFolder) == 0))
                        {
                            continue;
                        }
                        strPath.ResetLength(stLen);
                        strPath += FindData.cFileName;
                        strPath += PathSepChar;
                        if (DeleteFolders(strPath) == false)
                        {
                            bRet = false;
                            break;
                        }
                    }
                    else
                    {
                        strPath.ResetLength(stLen);
                        strPath += FindData.cFileName;
                        ::SetFileAttributes(*strPath, FILE_ATTRIBUTE_NORMAL);
                        ::DeleteFile(*strPath);
                    }

                }while (::FindNextFile(hFile, &FindData));
                ::FindClose(hFile);
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
            strPath.ResetLength(stLen);
            ::SetFileAttributes(*strPath, FILE_ATTRIBUTE_NORMAL);
            ::RemoveDirectory(*strPath);
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] delete folder %s return error code %d"), this, *strPath, ulRet);
            bRet = false;
        }
    }
    return bRet;
}

INLINE bool CFilePath::GetPath(PCXStr pszKey, CString& strPath)
{
    CSyncLockScope scope(m_FPLock);
    PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
    if (pPair != nullptr)
    {
        strPath = pPair->m_V;
        return true;
    }
    return false;
}

INLINE void CFilePath::SetModulePath(void)
{
    PAIR_FILEPATH* pPair = m_FilePath.Find(ModulePath);
    if (pPair == nullptr)
    {
        PINDEX index = m_FilePath.Add(ModulePath);
        if (index != nullptr)
        {
            pPair = m_FilePath.GetAt(index);

            XChar szPath[LMT_MAX_PATH];
            CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);

            pPair->m_V = szPath;

            assert(pPair->m_V.Length() > 0);
            assert(pPair->m_V[pPair->m_V.Length() - 1] == PathSepChar);

            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] okay"), this, pPair->m_V.GetBuffer(), ModulePath);
        } // index != nullptr
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_FILE_PATH_INL__
