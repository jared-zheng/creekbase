// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_XUI_H__
#define __TARGET_XUI_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "subsystem.h"

///////////////////////////////////////////////////////////////////
#if defined(XUI_EXPORT)
    #define XUIAPI                    C_EXPORT
    #define XUICLASS                  CXX_EXPORT
#elif defined(__RUNTIME_STATIC__)
    #define XUIAPI
    #define XUICLASS
    DECLARE_STATIC_CREATE_FUNC( XUIStatic );
#else   // XUI_EXPORT
    #define XUIAPI                    C_IMPORT
    #define XUICLASS                  CXX_IMPORT
#endif  // XUI_EXPORT

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
class IXWnd;

///////////////////////////////////////////////////////////////////
// CXUITraits
class NOVTABLE CXUITraits
{
public:
    typedef struct tagCREATE_PARAM
    {
    public:
        tagCREATE_PARAM(void)
        : pParent(nullptr)
        , itMenuID(0)
        , pszName(nullptr)
        , ulStyle(WS_OVERLAPPEDWINDOW)
        , ulExStyle(0)
        , nx(CW_USEDEFAULT)
        , ny(CW_USEDEFAULT)
        , ncx(CW_USEDEFAULT)
        , ncy(CW_USEDEFAULT)
        , bFlag(TRUE)
        , uClassStyle(CS_VREDRAW|CS_HREDRAW|CS_DBLCLKS)
        , pEventHandler(nullptr)
        {
        }

        ~tagCREATE_PARAM(void)
        {
        }
    public:
        IXWnd*           pParent;
        intptr_t         itMenuID;
        PCXStr           pszName;
        ULong            ulStyle;
        ULong            ulExStyle;
        Int              nx;
        Int              ny;
        Int              ncx;
        Int              ncy;
        BOOL             bFlag; // dialog : modal/modeless; controls : subclass/un-subclass
        UInt             uClassStyle;
        // ***pEventHandler AddRef when a new xwnd inst is created and SetEventHandler***
        // ***pEventHandler Release when a xwnd inst is destroyed***
        // ***to prevent pEventHandler inst MDELETE when a xwnd inst is destroyed, invoke AddRef before***
        CEventHandler*   pEventHandler;
    }CREATE_PARAM, *PCREATE_PARAM;
};

///////////////////////////////////////////////////////////////////
// CXUIManager
DECLARE_UUID( CXUIManager,  {2B21B16C-E2EB-4C50-A658-51FB8D58CBC7} )
class NOVTABLE CXUIManager ABSTRACT : public CComponent, public CXUITraits
{
public:
    virtual UInt     Init(void) PURE;
    virtual void     Exit(void) PURE;

    virtual void     MsgLoop(void) PURE;


    virtual intptr_t Create(IXWnd& xWndRef, CREATE_PARAM& cp) PURE;
    virtual intptr_t Create(IXWnd& xWndRef, ULong ulExStyle, PCXStr pszName, ULong ulStyle,
                            Int nx = CW_USEDEFAULT, Int ny = CW_USEDEFAULT, Int ncx = CW_USEDEFAULT, Int ncy = CW_USEDEFAULT,
                            IXWnd* pParent = nullptr, intptr_t itMenuID = 0, BOOL bFlag = TRUE, UInt uClassStyle = CS_VREDRAW|CS_HREDRAW|CS_DBLCLKS,
                            CEventHandler* pEventHandler = nullptr) PURE;
    virtual bool     Attach(IXWnd& xWndRef, HWND hWnd, IXWnd* pParent = nullptr) PURE;
    virtual bool     Detach(IXWnd& xWndRef) PURE;

    virtual IXWnd*   Find(PCXStr pszName) PURE;
    virtual IXWnd*   Find(HWND hWnd) PURE;
    virtual IXWnd*   GetFrame(void) PURE;

    virtual bool     LoadAccel(Int nAccelID) PURE;
    virtual bool     LoadResource(PCXStr pszFile) PURE;
    virtual void     SetResHandle(Module mResInst, bool bUnload = false) PURE;
    virtual Module   GetResHandle(void) PURE;
};
typedef CTRefCountPtr<CXUIManager> CXUIManagerPtr;

///////////////////////////////////////////////////////////////////
// CXUISystem : CSubSystem
DECLARE_UUID( CXUISystem, {B95F4935-BF60-420F-8760-BFB0FE205618} )

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_XUI_H__
