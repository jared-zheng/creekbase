// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_DEF_H__
#define __TARGET_DEF_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
#define CONCAT(x, y)                  __CONCAT__(x, y)
#define __CONCAT__(x, y)              x ## y
#define TOSTR( x )                    __STR__( x )
#define __STR__( x )                  #x
#define TF( x )                       __T__( x )
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    #define XChar                     WChar
    #define __T__( x )                L ## x
#else
    #define XChar                     Char
    #define __T__( x )                x
#endif

#define HANDLE_INVALID                INVALID_HANDLE_VALUE
#define SEEKTO_INVALID                INVALID_SET_FILE_POINTER
#define INDEX_INVALID                 TLS_OUT_OF_INDEXES

#define CP_DEFAULT                    CP_ACP
//#define CP_UTF8                       CP_UTF8

//#define E_OUTOFMEMORY                 E_OUTOFMEMORY
#define E_ACCESS_VIOLATION            EXCEPTION_ACCESS_VIOLATION
#define E_ARRAY_BOUNDS_EXCEEDED       EXCEPTION_ARRAY_BOUNDS_EXCEEDED

#define ALLOC_TYPE_NORMAL             PAGE_READWRITE
#define ALLOC_TYPE_EXECUTE            PAGE_EXECUTE_READWRITE

#define TRet                          ULong
#define TParam                        void*
#define THREAD_FUNC                   WINAPI

#define TRY_EXCEPTION                 \
__try                                 \
{

#define CATCH_EXCEPTION               \
}                                     \
__except (EXCEPTION_EXECUTE_HANDLER)  \
{

#define END_EXCEPTION                 \
}

///////////////////////////////////////////////////////////////////
//
typedef   HANDLE                           Handle,     *PHandle;
typedef   HINSTANCE                        Module,     *PModule;
typedef   ULong                            PId,        *PPId;
typedef   ULong                            TId,        *PTId;
typedef   UInt                             CodePage,   *PCodePage;
typedef   SSIZE_T                          SeekPos,    *PSeekPos;
typedef   UInt                             SizeLen,    *PSizeLen;
// UUID defined in rpcdce.h
typedef   GUID                             UID,        *PUID;

typedef   WIN32_FILE_ATTRIBUTE_DATA        FILEATTR,   *PFILEATTR;
typedef   WIN32_FIND_DATA                  FILEFIND,   *PFILEFIND;

typedef   OVERLAPPED                       AIOATTR,    *PAIOATTR;
typedef   LPOVERLAPPED_COMPLETION_ROUTINE  AIOHANDLER, *PAIOHANDLER;
typedef   Int (*EXCEPT_HANDLER)(TId tThreadId, Int nThreadType, Int nThreadRoutine, void* pRoutine, struct _EXCEPTION_POINTERS* pExceptInfo);

///////////////////////////////////////////////////////////////////
// limit length
enum LIMIT_LEN
{
    LMT_KEY              = 64,
    LMT_MIN              = 128,
    LMT_MAX_NAME         = 256,
    LMT_MAX_PATH         = MAX_PATH,
    LMT_BUF              = 1024,
    LMT_MAX              = UINT_MAX,
};

enum SEEK_OP
{
    SEEKO_BEGIN          = 0x00000000,
    SEEKO_CURRENT        = 0x00000001,
    SEEKO_END            = 0x00000002,
    SEEKO_BOUND          = 0x00000003,
};

enum FLUSH_OP
{
    FLUSHO_BUF           = 0x00000000, // flush buffer to file only
    FLUSHO_ASYNC         = 0x00000001,
    FLUSHO_INVALIDATE    = 0x00000002,
    FLUSHO_SYNC          = 0x00000004,
    FLUSHO_DEFAULT       = FLUSHO_ASYNC,
};

enum FILE_FLAG
{
    FILEF_NEW_NOEXIST    = 0x00000001, // CREATE_NEW
    FILEF_NEW_ALWAYS     = 0x00000002, // CREATE_ALWAYS
    FILEF_OPEN_EXIST     = 0x00000003, // OPEN_EXISTING
    FILEF_OPEN_ALWAYS    = 0x00000004, // OPEN_ALWAYS
    FILEF_OPEN_TRUNCATE  = 0x00000005, // TRUNCATE_EXISTING
    FILEF_NEW_OPEN_MASK  = 0x00000007,

    FILEF_EXT_APPEND     = 0x00000010, // write stream seek to end after open
    FILEF_EXT_NOBUF      = 0x00000020, // read  stream no buffering, for read once
    FILEF_EXT_TEMP       = 0x00000040, //

    FILEF_ACS_ALL        = 0x10000000, // GENERIC_ALL(0x10000000L)
    FILEF_ACS_EXECUTE    = 0x20000000, // GENERIC_EXECUTE(0x20000000L)
    FILEF_ACS_WRITE      = 0x40000000, // GENERIC_WRITE(0x40000000L)
    FILEF_ACS_READ       = 0x80000000, // GENERIC_READ(0x80000000L)
    FILEF_ACS_MASK       = 0xF0000000,
    // windows : share-mode
    FILEF_SHARE_UREAD    = 0x00000100, // windows : FILE_SHARE_READ(0x00000001)
    FILEF_SHARE_UWRITE   = 0x00000200, // windows : FILE_SHARE_WRITE(0x00000002)
    FILEF_SHARE_UDELETE  = 0x00000400, // windows : FILE_SHARE_DELETE(0x00000004)
    FILEF_SHARE_UEXECUTE = 0x00000000, // windows : ---
    FILEF_SHARE_UALL     = 0x00000700, // windows : FILE_SHARE_* all
    FILEF_SHARE_GREAD    = 0x00000000, // windows : ---
    FILEF_SHARE_GWRITE   = 0x00000000, // windows : ---
    FILEF_SHARE_GDELETE  = 0x00000000, // windows : ---
    FILEF_SHARE_GEXECUTE = 0x00000000, // windows : ---
    FILEF_SHARE_GALL     = 0x00000000, // windows : ---
    FILEF_SHARE_OREAD    = 0x00000000, // windows : ---
    FILEF_SHARE_OWRITE   = 0x00000000, // windows : ---
    FILEF_SHARE_ODELETE  = 0x00000000, // windows : ---
    FILEF_SHARE_OEXECUTE = 0x00000000, // windows : ---
    FILEF_SHARE_OALL     = 0x00000000, // windows : ---
    FILEF_SHARE_UMASK    = 0x00000008, // windows : >> 8
    FILEF_SHARE_GMASK    = 0x00000008, // windows : >> 8
    FILEF_SHARE_OMASK    = 0x00000008, // windows : >> 8
    FILEF_SHARE_USER     = (FILEF_SHARE_UREAD|FILEF_SHARE_UWRITE|FILEF_SHARE_GREAD|FILEF_SHARE_OREAD),
    FILEF_SHARE_DEFAULT  = (FILEF_SHARE_UREAD|FILEF_SHARE_UWRITE|FILEF_SHARE_GREAD|FILEF_SHARE_GWRITE|FILEF_SHARE_OREAD|FILEF_SHARE_OWRITE),
    FILEF_SHARE_ALL      = (FILEF_SHARE_UALL|FILEF_SHARE_GALL|FILEF_SHARE_OALL),
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_DEF_H__
