// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_ATOMICS_INL__
#define __TARGET_ATOMICS_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CAtomics
template <typename T>
FORCEINLINE T CAtomics::Increment16(T volatile* pDst)
{
#if (NTDDI_VERSION >= NTDDI_WIN8)
    return (T)::InterlockedIncrement16(pDst);
#else  // (_WIN32_WINNT >= NTDDI_WIN8)
    //return (T)(*pDst += 1);
    return (T)_InterlockedIncrement16(pDst);
#endif // (_WIN32_WINNT >= NTDDI_WIN8)
}

template <typename T>
FORCEINLINE T CAtomics::Increment(T volatile* pDst)
{
    return (T)::InterlockedIncrement(pDst);
}

template <typename T>
FORCEINLINE T CAtomics::Increment64(T volatile* pDst)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedIncrement64(pDst);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return (T)_InterlockedIncrement64(pDst);
#else
    #pragma message("Windows XP No Implement")
    return (T)(*pDst += 1);
#endif
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

template <typename T>
FORCEINLINE T CAtomics::Decrement16(T volatile* pDst)
{
#if (NTDDI_VERSION >= NTDDI_WIN8)
    return (T)::InterlockedDecrement16(pDst);
#else  // (_WIN32_WINNT >= NTDDI_WIN8)
    return (T)_InterlockedDecrement16(pDst);
#endif // (_WIN32_WINNT >= NTDDI_WIN8)
}

template <typename T>
FORCEINLINE T CAtomics::Decrement(T volatile* pDst)
{
    return (T)::InterlockedDecrement(pDst);
}

template <typename T>
FORCEINLINE T CAtomics::Decrement64(T volatile* pDst)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedDecrement64(pDst);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return (T)_InterlockedDecrement64(pDst);
#else
    #pragma message("Windows XP No Implement")
    return (T)(*pDst -= 1);
#endif
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

template <typename T>
FORCEINLINE T CAtomics::Add16(T volatile* pDst, T tVal)
{
    //T tOldValue = *pDst;
    //*pDst      += tVal;
    //return tOldValue;
    return (T)_InterlockedExchangeAdd16(pDst);
}

template <typename T>
FORCEINLINE T CAtomics::Add(T volatile* pDst, T tVal)
{
    return (T)::InterlockedExchangeAdd(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Add64(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedExchangeAdd64(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return (T)_InterlockedExchangeAdd64(pDst, tVal);
#else
    #pragma message("Windows XP No Implement")
    T tOldValue = *pDst;
    *pDst      += tVal;
    return tOldValue;
#endif
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

template <typename T>
FORCEINLINE T CAtomics::And16(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedAnd16(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedAnd16(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::And(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedAnd(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedAnd(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::And64(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedAnd64(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedAnd64(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Or16(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedOr16(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedOr16(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Or(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedOr(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedOr(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Or64(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedOr64(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedOr64(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Xor16(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedXor16(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedXor16(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Xor(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedXor(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedXor(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Xor64(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedXor64(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedXor64(pDst, tVal);
#endif
}

template <typename T>
FORCEINLINE T CAtomics::Exchange16(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedExchange16(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    return (T)_InterlockedExchange16(pDst, tVal);
    //#pragma message("Windows XP No Implement")
    //T tOldValue = *pDst;
    //*pDst       = tVal;
    //return tOldValue;
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

template <typename T>
FORCEINLINE T CAtomics::Exchange(T volatile* pDst, T tVal)
{
    return (T)::InterlockedExchange(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Exchange64(T volatile* pDst, T tVal)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedExchange64(pDst, tVal);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return (T)_InterlockedExchange64(pDst, tVal);
#else
    #pragma message("Windows XP No Implement")
    T tOldValue = *pDst;
    *pDst       = tVal;
    return tOldValue;
#endif
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange16(T volatile* pDst, T tExchange, T tComparand)
{
#if (NTDDI_VERSION >= NTDDI_WIN8)
    return (T)::InterlockedCompareExchange16(pDst, tExchange, tComparand);
#else  // (_WIN32_WINNT >= NTDDI_WIN8)
    //T tOldValue = *pDst;
    //if (*pDst == tComparand)
    //{
    //    *pDst = tExchange;
    //}
    //return tOldValue;
    return (T)_InterlockedCompareExchange16(pDst, tExchange, tComparand);
#endif // (_WIN32_WINNT >= NTDDI_WIN8)
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange(T volatile* pDst, T tExchange, T tComparand)
{
    return (T)::InterlockedCompareExchange(pDst, tExchange, tComparand);
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange64(T volatile* pDst, T tExchange, T tComparand)
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    return (T)::InterlockedCompareExchange64(pDst, tExchange, tComparand);
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
    //#pragma message("Windows XP No Implement")
    //T tOldValue = *pDst;
    //if (*pDst == tComparand)
    //{
    //    *pDst = tExchange;
    //}
    //return tOldValue;
    return (T)_InterlockedCompareExchange64(pDst, tExchange, tComparand);
#endif // (_WIN32_WINNT >= NTDDI_VISTA)
}

FORCEINLINE void* CAtomics::ExchangePtr(void* volatile* ppDst, void* pVal)
{
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    return ::InterlockedExchangePointer(ppDst, pVal);
#else
    //return (void*)InterlockedExchange((PLong)(ppDst), (Long)(pVal));
    return _InterlockedExchangePointer(ppDst, pVal);
#endif
}

FORCEINLINE void* CAtomics::CompareExchangePtr(void* volatile* ppDst, void* pExchange, void* pComparand)
{
    return ::InterlockedCompareExchangePointer(ppDst, pExchange, pComparand);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_ATOMICS_INL__
