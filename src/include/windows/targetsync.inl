// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_SYNC_INL__
#define __TARGET_SYNC_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CSyncBase
INLINE CSyncBase::CSyncBase(void)
{
}

INLINE CSyncBase::~CSyncBase(void)
{
}

INLINE CSyncBase::CSyncBase(const CSyncBase&)
{
}

INLINE CSyncBase& CSyncBase::operator=(const CSyncBase&)
{
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CTSyncBase
template <Int nType>
INLINE CTSyncBase<nType>::CTSyncBase(void)
{
}

template <Int nType>
INLINE CTSyncBase<nType>::~CTSyncBase(void)
{
}

template <Int nType>
INLINE CTSyncBase<nType>::CTSyncBase(const CTSyncBase<nType>& aSrc)
: CSyncBase(aSrc)
{
}

template <Int nType>
INLINE CTSyncBase<nType>& CTSyncBase<nType>::operator=(const CTSyncBase<nType>& aSrc)
{
    if (this != &aSrc)
    {
        CSyncBase::operator=(aSrc);
    }
    return (*this);
}

template <Int nType>
INLINE Int CTSyncBase<nType>::GetType(void) const
{
    return nType;
}

///////////////////////////////////////////////////////////////////
// CTSyncScope
template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::CTSyncScope(CTSyncBase<nType>& Sync, UInt uWait)
: m_pSync(&Sync)
, m_nRet(RET_OKAY)
{
    assert(m_pSync != nullptr);
    if (bWait == false)
    {
        m_nRet = m_pSync->Signal();
    }
    else
    {
        m_nRet = m_pSync->Wait(uWait);
    }
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::~CTSyncScope(void)
{
    assert(m_pSync != nullptr);
    if (nType != CSyncTraits::SYNCT_COUNTER)
    {
        if (m_nRet == RET_OKAY)
        {
            m_pSync->Reset();
        }
    }
    else if (m_nRet > 0)
    {
        m_pSync->Reset();
    }
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::CTSyncScope(const CTSyncScope<nType, bWait>&)
: m_pSync(nullptr)
, m_nRet(RET_FAIL)
{
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>& CTSyncScope<nType, bWait>::operator=(const CTSyncScope<nType, bWait>&)
{
    return (*this);
}

template <Int nType, bool bWait>
INLINE Int CTSyncScope<nType, bWait>::GetRet(void) const
{
    return m_nRet;
}

///////////////////////////////////////////////////////////////////
// CSyncCounter
INLINE CSyncCounter::CSyncCounter(Int nCounter)
: m_nCounter(nCounter)
{
}

INLINE CSyncCounter::~CSyncCounter(void)
{
}

INLINE CSyncCounter::CSyncCounter(const CSyncCounter& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_COUNTER>(aSrc)
, m_nCounter(0)
{
}

INLINE CSyncCounter& CSyncCounter::operator=(const CSyncCounter& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_COUNTER>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncCounter::Signal(void)
{
    return (Int)CAtomics::Increment<Long>((PLong)&m_nCounter);
}

INLINE Int CSyncCounter::Reset(void)
{
    return (Int)CAtomics::Decrement<Long>((PLong)&m_nCounter);
}

INLINE Int CSyncCounter::Wait(UInt uWait, bool bAlert)
{
    CPlatform::SleepEx(uWait, bAlert);
    return (Int)RET_OKAY;
}

INLINE bool CSyncCounter::IsValid(void) const
{
    return (m_nCounter >= 0);
}

INLINE void CSyncCounter::Close(void)
{
    m_nCounter = 0;
}

INLINE Int CSyncCounter::GetCount(void)
{
    return m_nCounter;
}

///////////////////////////////////////////////////////////////////
// CSyncLock
INLINE CSyncLock::CSyncLock(bool bCreate, LockAttr* pAttr)
{
    m_CSLock.SpinCount = 0;
    if (bCreate)
    {
        Open(pAttr);
    }
}

INLINE CSyncLock::~CSyncLock(void)
{
    Close();
}

INLINE CSyncLock::CSyncLock(const CSyncLock& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_LOCK>(aSrc)
{
}

INLINE CSyncLock& CSyncLock::operator=(const CSyncLock& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_LOCK>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncLock::Signal(void)
{
    assert(IsValid());
    ::EnterCriticalSection(&m_CSLock);
    return (Int)RET_OKAY;
}

INLINE Int CSyncLock::Reset(void)
{
    assert(IsValid());
    ::LeaveCriticalSection(&m_CSLock);
    return (Int)RET_OKAY;
}

INLINE Int CSyncLock::Wait(UInt uWait, bool bAlert)
{
    UNREFERENCED_PARAMETER( bAlert );
    assert(IsValid());
    if (uWait == (UInt)TIMET_IGNORE)
    {
        return (::TryEnterCriticalSection(&m_CSLock) != FALSE) ? (Int)RET_OKAY : (Int)RET_FAIL;
    }
    return Signal();
}

INLINE bool CSyncLock::IsValid(void) const
{
    return (m_CSLock.SpinCount != 0);
}

INLINE void CSyncLock::Close(void)
{
    if (IsValid())
    {
        ::DeleteCriticalSection(&m_CSLock);
        m_CSLock.SpinCount = 0;
    }
}

INLINE bool CSyncLock::Open(LockAttr* pAttr)
{
    Close();
    UNREFERENCED_PARAMETER( pAttr );
    if (::InitializeCriticalSectionAndSpinCount(&m_CSLock, (ULong)TIMET_LOCKSPIN) != FALSE)
    {
        m_CSLock.SpinCount = TIMET_LOCKSPIN;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CSyncMutex
INLINE CSyncMutex::CSyncMutex(bool bCreate, bool bCreateSignal, PCXStr pszName, MutexAttr* pAttr)
{
    if (bCreate)
    {
        Open(bCreateSignal, pszName, pAttr);
    }
}

INLINE CSyncMutex::~CSyncMutex(void)
{
}

INLINE CSyncMutex::CSyncMutex(const CSyncMutex& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_MUTEX>(aSrc)
{
}

INLINE CSyncMutex& CSyncMutex::operator=(const CSyncMutex& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_MUTEX>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncMutex::Signal(void)
{
    assert(IsValid());
    return (Int)::WaitForSingleObject(m_hMutex, (ULong)TIMET_INFINITE);
}

INLINE Int CSyncMutex::Reset(void)
{
    assert(IsValid());
    if (::ReleaseMutex(m_hMutex) == FALSE)
    {
        return (Int)RET_FAIL;
    }
    return (Int)RET_OKAY;
}

INLINE Int CSyncMutex::Wait(UInt uWait, bool bAlert)
{
    assert(IsValid());
    return (Int)::WaitForSingleObjectEx(m_hMutex, (ULong)uWait, (bAlert ? TRUE : FALSE));
}

INLINE bool CSyncMutex::IsValid(void) const
{
    return m_hMutex.IsValid();
}

INLINE void CSyncMutex::Close(void)
{
    m_hMutex.Close();
}

INLINE bool CSyncMutex::Open(bool bCreateSignal, PCXStr pszName, MutexAttr* pAttr)
{
    Close();
    m_hMutex = ::CreateMutex(pAttr, (bCreateSignal ? TRUE : FALSE), pszName);
    return m_hMutex.IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncEvent
INLINE CSyncEvent::CSyncEvent(bool bCreate, bool bCreateSignal, bool bManualReset, PCXStr pszName, EventAttr* pAttr)
{
    if (bCreate)
    {
        Open(bCreateSignal, bManualReset, pszName, pAttr);
    }
}

INLINE CSyncEvent::~CSyncEvent(void)
{
}

INLINE CSyncEvent::CSyncEvent(const CSyncEvent& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_EVENT>(aSrc)
{
}

INLINE CSyncEvent& CSyncEvent::operator=(const CSyncEvent& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_EVENT>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncEvent::Signal(void)
{
    assert(IsValid());
    if (::SetEvent(m_hEvent) != FALSE)
    {
        return (Int)RET_OKAY;
    }
    else
    {
        return (Int)RET_FAIL;
    }
}

INLINE Int CSyncEvent::Reset(void)
{
    assert(IsValid());
    if (::ResetEvent(m_hEvent) != FALSE)
    {
        return (Int)RET_OKAY;
    }
    else
    {
        return (Int)RET_FAIL;
    }
}

INLINE Int CSyncEvent::Wait(UInt uWait, bool bAlert)
{
    assert(IsValid());
    return (Int)::WaitForSingleObjectEx(m_hEvent, (ULong)uWait, (bAlert ? TRUE : FALSE));
}

INLINE bool CSyncEvent::IsValid(void) const
{
    return m_hEvent.IsValid();
}

INLINE void CSyncEvent::Close(void)
{
    m_hEvent.Close();
}

INLINE bool CSyncEvent::Open(bool bCreateSignal, bool bManualReset, PCXStr pszName, EventAttr* pAttr)
{
    Close();
    m_hEvent = ::CreateEvent(pAttr, (bManualReset ? TRUE : FALSE), (bCreateSignal ? TRUE : FALSE), pszName);
    return m_hEvent.IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncSema
INLINE CSyncSema::CSyncSema(bool bCreate, bool bCreateSignal, Int nLockCount, PCXStr pszName, SemaAttr* pAttr)
{
    if (bCreate)
    {
        Open(bCreateSignal, nLockCount, pszName, pAttr);
    }
}

INLINE CSyncSema::~CSyncSema(void)
{
}

INLINE CSyncSema::CSyncSema(const CSyncSema& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_SEMA>(aSrc)
{
}

INLINE CSyncSema& CSyncSema::operator=(const CSyncSema& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_SEMA>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncSema::Signal(void)
{
    assert(IsValid());
    return (Int)::WaitForSingleObject(m_hSemaphore, (ULong)TIMET_INFINITE);
}

INLINE Int CSyncSema::Reset(void)
{
    assert(IsValid());
    if (::ReleaseSemaphore(m_hSemaphore, 1, nullptr) != FALSE)
    {
        return (Int)RET_OKAY;
    }
    else
    {
        return (Int)RET_FAIL;
    }
}

INLINE Int CSyncSema::Wait(UInt uWait, bool bAlert)
{
    assert(IsValid());
    return (Int)::WaitForSingleObjectEx(m_hSemaphore, (ULong)uWait, (bAlert ? TRUE : FALSE));
}

INLINE bool CSyncSema::IsValid(void) const
{
    return m_hSemaphore.IsValid();
}

INLINE void CSyncSema::Close(void)
{
    m_hSemaphore.Close();
}

INLINE bool CSyncSema::Open(bool bCreateSignal, Int nLockCount, PCXStr pszName, SemaAttr* pAttr)
{
    Close();
    m_hSemaphore = ::CreateSemaphore(pAttr, (bCreateSignal ? 0 : nLockCount), nLockCount, pszName);
    return m_hSemaphore.IsValid();
}

#if (NTDDI_VERSION >= NTDDI_VISTA)
///////////////////////////////////////////////////////////////////
// CCondition
INLINE CCondition::CCondition(ConditionAttr* pAttr)
{
    UNREFERENCED_PARAMETER( pAttr );
    ::InitializeConditionVariable(&m_CVar);
}

INLINE CCondition::~CCondition(void)
{
}

INLINE CCondition::CCondition(const CCondition&)
{
}

INLINE CCondition& CCondition::operator=(const CCondition&)
{
    return (*this);
}

INLINE bool CCondition::Wait(CSyncLock& Lock, UInt uWait)
{
    return (::SleepConditionVariableCS(&m_CVar, &(Lock.m_CSLock), (ULong)uWait) != FALSE);
}

INLINE bool CCondition::Wait(CRWLock& RWLock, bool bShared, UInt uWait)
{
    return (::SleepConditionVariableSRW(&m_CVar, &(RWLock.m_RWLock), (ULong)uWait, bShared ? CONDITION_VARIABLE_LOCKMODE_SHARED : 0) != FALSE);
}

INLINE bool CCondition::Wake(bool bAll)
{
    if (bAll)
    {
        ::WakeAllConditionVariable(&m_CVar);
    }
    else
    {
        ::WakeConditionVariable(&m_CVar);
    }
    return true;
}

INLINE bool CCondition::IsValid(void) const
{
    return true;
}

///////////////////////////////////////////////////////////////////
// CRWLock
INLINE CRWLock::CRWLock(bool bManual, RWLockAttr* pAttr)
: m_bManual(bManual)
{
    UNREFERENCED_PARAMETER( pAttr );
    ::InitializeSRWLock(&m_RWLock);
}

INLINE CRWLock::~CRWLock(void)
{
}

INLINE CRWLock::CRWLock(const CRWLock&)
: m_bManual(false)
{
}

INLINE CRWLock& CRWLock::operator=(const CRWLock&)
{
    return (*this);
}

INLINE bool CRWLock::LockRead(UInt uWait)
{
#if (NTDDI_VERSION >= NTDDI_WIN7)
    if (::TryAcquireSRWLockShared(&m_RWLock) != FALSE)
    {
        return true;
    }
#endif // (_WIN32_WINNT >= NTDDI_WIN7)
    if ((uWait != (UInt)TIMET_INFINITE) &&
        (m_ReadCond.Wait(*this, true, uWait) == false))
    {
        return false;
    }
    ::AcquireSRWLockShared(&m_RWLock);
    return true;
}

INLINE void CRWLock::UnlockRead(void)
{
    ::ReleaseSRWLockShared(&m_RWLock);
    m_ReadCond.Wake(m_bManual);
}

INLINE bool CRWLock::LockWrite(UInt uWait)
{
#if (NTDDI_VERSION >= NTDDI_WIN7)
    if (::TryAcquireSRWLockExclusive(&m_RWLock) != FALSE)
    {
        return true;
    }
#endif // (_WIN32_WINNT >= NTDDI_WIN7)
    if ((uWait != (UInt)TIMET_INFINITE) &&
        (m_WriteCond.Wait(*this, false, uWait) == false))
    {
        return false;
    }
    ::AcquireSRWLockExclusive(&m_RWLock);
    return true;
}

INLINE void CRWLock::UnlockWrite(void)
{
    ::ReleaseSRWLockExclusive(&m_RWLock);
    m_WriteCond.Wake(m_bManual);
}
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
INLINE CRWLock::CRWLock(bool bManual, RWLockAttr* pAttr)
: m_uCount(0)
, m_NoneReadEvent(true, true)
{
    UNREFERENCED_PARAMETER( bManual );
    UNREFERENCED_PARAMETER( pAttr );
}

INLINE CRWLock::~CRWLock(void)
{
    assert(m_uCount == 0);
}

INLINE CRWLock::CRWLock(const CRWLock&)
{
}

INLINE CRWLock& CRWLock::operator=(const CRWLock&)
{
    return (*this);
}

INLINE bool CRWLock::LockRead(UInt uWait)
{
    if (m_WriteMutex.Wait(uWait) != (Int)RET_OKAY)
    {
        return false;
    }

    m_ReadLock.Wait();
    {
        ++m_uCount;
        if (m_uCount == 1)
        {
            m_NoneReadEvent.Reset();
        }
    }
    m_ReadLock.Reset();

    m_WriteMutex.Reset();
    return true;
}

INLINE void CRWLock::UnlockRead(void)
{
    m_ReadLock.Wait();
    {
        --m_uCount;
        if (m_uCount == 0)
        {
            m_NoneReadEvent.Signal();
        }
    }
    m_ReadLock.Reset();
}

INLINE bool CRWLock::LockWrite(UInt uWait)
{
    if (m_WriteMutex.Wait(uWait) != (Int)RET_OKAY)
    {
        return false;
    }
    if (m_NoneReadEvent.Wait(uWait) != (Int)RET_OKAY)
    {
        m_WriteMutex.Reset();
        return false;
    }
    return true;
}

INLINE void CRWLock::UnlockWrite(void)
{
    m_WriteMutex.Reset();
}
#endif // (_WIN32_WINNT >= NTDDI_VISTA)

INLINE bool CRWLock::IsValid(void) const
{
    return true;
}

///////////////////////////////////////////////////////////////////
// CTRWLockScope
template <bool bRead>
INLINE CTRWLockScope<bRead>::CTRWLockScope(CRWLock& RWLock, UInt uWait)
: m_pRWLock(&RWLock)
{
    assert(m_pRWLock != nullptr);
    if (bRead == true)
    {
        m_pRWLock->LockRead(uWait);
    }
    else
    {
        m_pRWLock->LockWrite(uWait);
    }
}

template <bool bRead>
INLINE CTRWLockScope<bRead>::~CTRWLockScope(void)
{
    assert(m_pRWLock != nullptr);
    if (bRead == true)
    {
        m_pRWLock->UnlockRead();
    }
    else
    {
        m_pRWLock->UnlockWrite();
    }
}

template <bool bRead>
INLINE CTRWLockScope<bRead>::CTRWLockScope(const CTRWLockScope<bRead>&)
{
}

template <bool bRead>
INLINE CTRWLockScope<bRead>& CTRWLockScope<bRead>::operator=(const CTRWLockScope<bRead>&)
{
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CWaitableTimer
INLINE CWaitableTimer::CWaitableTimer(bool bCreate, PCXStr pszName, TimerAttr* pAttr)
{
    if (bCreate)
    {
        CreateTimer(pszName, pAttr);
    }
}

INLINE CWaitableTimer::~CWaitableTimer(void)
{
    CloseTimer();
}

INLINE CWaitableTimer::CWaitableTimer(const CWaitableTimer&)
{
}

INLINE CWaitableTimer& CWaitableTimer::operator=(const CWaitableTimer&)
{
    return (*this);
}

INLINE bool CWaitableTimer::SetTimer(LLong llBegin, Long lInterval, PTIMERAPCROUTINE pRoutine, void* pRoutineArg)
{
    if (m_hTimer.IsValid() == false)
    {
        return false;
    }
    LARGE_INTEGER li;
    li.QuadPart = llBegin;
    if (::SetWaitableTimer(m_hTimer, &li, lInterval, pRoutine, pRoutineArg, FALSE) == FALSE)
    {
        return false;
    }
    return true;
}

INLINE bool CWaitableTimer::CancelTimer(void)
{
    if (m_hTimer.IsValid())
    {
        ::CancelWaitableTimer(m_hTimer);
        return true;
    }
    return false;
}

INLINE Int  CWaitableTimer::Wait(UInt uWait)
{
    if (m_hTimer != nullptr)
    {
        return (Int)::WaitForSingleObjectEx(m_hTimer, (ULong)uWait, TRUE);
    }
    return (Int)RET_FAIL;
}

INLINE bool CWaitableTimer::IsValid(void) const
{
    return (m_hTimer != nullptr);
}

INLINE bool CWaitableTimer::CreateTimer(PCXStr pszName, TimerAttr* pAttr)
{
    CloseTimer();
    m_hTimer = ::CreateWaitableTimer(pAttr, FALSE, pszName);
    return m_hTimer.IsValid();
}

INLINE void CWaitableTimer::CloseTimer(void)
{
    if (m_hTimer.IsValid())
    {
        CancelTimer();
        m_hTimer.Close();
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_SYNC_INL__
