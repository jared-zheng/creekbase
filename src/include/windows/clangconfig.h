// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CLANG_CONFIG_H__
#define __CLANG_CONFIG_H__

#pragma once

#include "config.h"

///////////////////////////////////////////////////////////////////
#if defined(__clang__)
///////////////////////////////////////////////////////////////////
// __ARCH_TARGET__ && __ARCH_TARGET_BIGENDIAN__
#if defined(_M_IX86)
    #define __ARCH_TARGET__           ARCH_TARGET_32
    #define __ARCH_TARGET_STR__       "ARCH_TARGET_32"
#elif defined(_M_X64) || defined(_M_AMD64)
    #define __ARCH_TARGET__           ARCH_TARGET_64
    #define __ARCH_TARGET_STR__       "ARCH_TARGET_64"
#else
    #error "__ARCH_TARGET__ No Implement"
#endif

///////////////////////////////////////////////////////////////////
// __COMPILER_TYPE__
#define __COMPILER_TYPE__             COMPILER_TYPE_CLANG

///////////////////////////////////////////////////////////////////
// __PLATFORM_TARGET__
#if defined(_WIN32) || defined(_WIN64)
    #define __PLATFORM_TARGET__       PLATFORM_TARGET_WINDOWS
    #define __PLATFORM_TARGET_STR__   "PLATFORM_TARGET_WINDOWS"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif
///////////////////////////////////////////////////////////////////
// __RUNTIME_CONFIG__
#if defined(_UNICODE) || defined(UNICODE)
    #define __RUNTIME_CHARSET_WCHAR__
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_UTF16
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_UTF16"
#elif defined(_MBCS)
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_MBCS
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_MBCS"
#else
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_UNKNOWN
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_UNKNOWN"
#endif

#if defined(_DEBUG) || defined(DEBUG)
    #define __RUNTIME_DEBUG__
#endif

#if defined(_LIB) || defined(LIB_RUNTIME)
    #define __RUNTIME_STATIC__
#endif

#define __CREEK_BASE__

///////////////////////////////////////////////////////////////////
// debug help
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    #define DBGHELP_TRANSLATE_TCHAR
#endif

///////////////////////////////////////////////////////////////////
// windows Header Files
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <intrin.h>
#include <dbghelp.h>
#include <cstdlib>
#include <cstdio>
#include <cwchar>
#include <cstring>
#include <mbstring.h>
#include <tchar.h>
#include <cassert>
#include <ctime>
#include <clocale>
#include <utility>

///////////////////////////////////////////////////////////////////
// std namespace : system memory alloc/free trace
#pragma push_macro("new")
#undef new
#pragma push_macro("delete")
#undef delete
///////////////////////////////////////////////////////////////////
// windows CRT memory alloc/free debug check
#ifdef __RUNTIME_DEBUG__
    #define _CRTDBG_MAP_ALLOC
    #undef _malloca
    #include <crtdbg.h>
    #define CRTDBG_NEW                new(_NORMAL_BLOCK, __FILE__, __LINE__)
    #define new                       CRTDBG_NEW
#endif  // __RUNTIME_DEBUG__
#pragma pop_macro("delete")
#pragma pop_macro("new")

///////////////////////////////////////////////////////////////////
// placement new operator from <new>
#include <new>
#define GNEW(adr)                     ::new( (adr) )

///////////////////////////////////////////////////////////////////
// disable warning
#pragma clang diagnostic ignored "-Wunused"
#pragma clang diagnostic ignored "-Wunused-value"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wmacro-redefined"
#pragma clang diagnostic ignored "-Wignored-qualifiers"
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
#pragma clang diagnostic ignored "-Wsign-compare"
#pragma clang diagnostic ignored "-Wmissing-braces"

///////////////////////////////////////////////////////////////////
//
#ifdef __cplusplus
    #define LINE_EXTERN_C             extern "C"
    #define BEGIN_EXTERN_C            extern "C" {
    #define END_EXTERN_C              }
#else
    #define LINE_EXTERN_C
    #define BEGIN_EXTERN_C
    #define END_EXTERN_C
#endif

#define PURE                          = 0

#define CXX_EXPORT                    __declspec(dllexport)
#define CXX_IMPORT                    __declspec(dllimport)
#define C_EXPORT                      LINE_EXTERN_C CXX_EXPORT
#define C_IMPORT                      LINE_EXTERN_C CXX_IMPORT

#define ASM_BEGIN                     __asm__ __volatile__(
#define ASM_END                       );
#define INLINE                        inline
#define FORCEINLINE                   __forceinline
#define FORCENOINLINE                 __attribute__((noinline))
#define NOVTABLE                      __declspec(novtable)
#define SELECTANY                     __declspec(selectany)
#define NAKED
#define THREADLS                      __declspec(thread)

#if __cplusplus > 199711L
    #if (__clang_major__ > 3) || ((__clang_major__ == 3) && (__clang_minor__ >= 3)) // 3.3
        #define ABSTRACT
        #define FINAL                     final
        #define OVERRIDE                  override
    #else
        #define ABSTRACT
        #define FINAL
        #define OVERRIDE
    #endif

    #if (__clang_major__ < 3) || ((__clang_major__ == 3) && (__clang_minor__ < 3)) // 3.3
        #define nullptr                   NULL
        #define __MODERN_CXX_NOT_SUPPORTED
    #else
        #define __MODERN_CXX_LANG __cplusplus
    #endif
#else
    #define ABSTRACT
    #define FINAL
    #define OVERRIDE

    #define nullptr NULL
    
    #define __MODERN_CXX_NOT_SUPPORTED
#endif

#else
    #error "config defines file for Windows Clang compiler only"
#endif  // __clang__

///////////////////////////////////////////////////////////////////
// common header files
#include "def.h"
#include "platform.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#
//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CLANG_CONFIG_H__
