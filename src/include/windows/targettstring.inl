// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_TSTRING_INL__
#define __TARGET_TSTRING_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CTStringRef
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    typedef CTStringRef<CChar>  CCStringRef;
    typedef CTStringRef<CWChar> CWStringRef;
    typedef CWStringRef         CStringRef;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_MBCS)
    typedef CTStringRef<CChar>  CCStringRef;
    typedef CTStringRef<CWChar> CWStringRef;
    typedef CTStringRef<CMChar> CStringRef;
#else
    typedef CTStringRef<CChar>  CCStringRef;
    typedef CTStringRef<CWChar> CWStringRef;
    typedef CCStringRef         CStringRef;
#endif

typedef CCStringRef::CElementTraits CCStringRefTraits;
typedef CWStringRef::CElementTraits CWStringRefTraits;
typedef CStringRef::CElementTraits  CStringRefTraits;

///////////////////////////////////////////////////////////////////
// CTStringFix
template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Load(UInt uId, Module mInst)
{
    m_stLen = (size_t)::LoadString(mInst, uId, m_szBuffer, (Int)stFix);
    return m_stLen;
}

#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    typedef CTStringFix<CChar, (size_t)LMT_KEY>       CCStringKey;
    typedef CTStringFix<CChar, (size_t)LMT_MAX_PATH>  CCStringFix;
    typedef CTStringFix<CWChar, (size_t)LMT_KEY>      CWStringKey;
    typedef CTStringFix<CWChar, (size_t)LMT_MAX_PATH> CWStringFix;
    typedef CWStringKey                               CStringKey;
    typedef CWStringFix                               CStringFix;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_MBCS)
    typedef CTStringFix<CChar, (size_t)LMT_KEY>       CCStringKey;
    typedef CTStringFix<CChar, (size_t)LMT_MAX_PATH>  CCStringFix;
    typedef CTStringFix<CWChar, (size_t)LMT_KEY>      CWStringKey;
    typedef CTStringFix<CWChar, (size_t)LMT_MAX_PATH> CWStringFix;
    typedef CTStringFix<CMChar, (size_t)LMT_KEY>      CStringKey;
    typedef CTStringFix<CMChar, (size_t)LMT_MAX_PATH> CStringFix;
#else
    typedef CTStringFix<CChar, (size_t)LMT_KEY>       CCStringKey;
    typedef CTStringFix<CChar, (size_t)LMT_MAX_PATH>  CCStringFix;
    typedef CTStringFix<CWChar, (size_t)LMT_KEY>      CWStringKey;
    typedef CTStringFix<CWChar, (size_t)LMT_MAX_PATH> CWStringFix;
    typedef CCStringKey                               CStringKey;
    typedef CCStringFix                               CStringFix;
#endif

typedef CCStringKey::CElementTraits CCStringFixTraits;
typedef CWStringKey::CElementTraits CWStringFixTraits;
typedef CStringKey::CElementTraits  CStringFixTraits;

///////////////////////////////////////////////////////////////////
// CTString
template <typename T>
INLINE size_t CTString<T>::Load(UInt uId, Module mInst)
{
    if (m_stBufferLen > 0)
    {
        m_stLen = (size_t)::LoadString(mInst, uId, m_pszBuffer, (Int)m_stBufferLen);
        return m_stLen;
    }
    return 0;
}

#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    typedef CTString<CChar>  CCString;
    typedef CTString<CWChar> CWString;
    typedef CWString         CString;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_MBCS)
    typedef CTString<CChar>  CCString;
    typedef CTString<CWChar> CWString;
    typedef CTString<CMChar> CString;
#else
    typedef CTString<CChar>  CCString;
    typedef CTString<CWChar> CWString;
    typedef CCString         CString;
#endif

typedef CCString::CElementTraits CCStringTraits;
typedef CWString::CElementTraits CWStringTraits;
typedef CString::CElementTraits  CStringTraits;

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_TSTRING_INL__