// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_FILE_LOG_INL__
#define __TARGET_FILE_LOG_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CFileLog
SELECTANY CPCXStr CFileLog::LogFileTime   = TF("\\%s_%04d%02d%02d_%02d%02d%02d%03d_%d.log");
SELECTANY CPCXStr CFileLog::LogFileName   = TF("%s\\%s.log");
SELECTANY CPCXStr CFileLog::LogFileTrace  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[TRACE][%04X]%s\r\n");
SELECTANY CPCXStr CFileLog::LogFileDebug  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DEBUG][%04X]%s\r\n");
SELECTANY CPCXStr CFileLog::LogFileInfo   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[INFO] [%04X]%s\r\n");
SELECTANY CPCXStr CFileLog::LogFileDump   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DUMP] [%04X]%s\r\n");
SELECTANY CPCXStr CFileLog::LogFileWarn   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[WARN] [%04X]%s\r\n");
SELECTANY CPCXStr CFileLog::LogFileError  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[ERROR][%04X]%s\r\n");

INLINE bool CFileLogPipe::OpenPipe(bool)
{
    m_hPipe = nullptr;
    return true;
}

INLINE void CFileLogPipe::ClosePipe(void)
{
    m_hPipe = HANDLE_INVALID;
}

INLINE void CFileLogPipe::WritePipe(PCXStr pszBuf, size_t)
{
    if ((m_Log.GetLevel() & LOGL_DEVPRINT) != 0)
    {
        _putts(pszBuf);
    }
    else
    {
        ::OutputDebugString(pszBuf);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_FILE_LOG_INL__
