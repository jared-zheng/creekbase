// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __FILE_VERSION_H__
#define __FILE_VERSION_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "tstring.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CFileVersion : #pragma comment(lib, "version.lib")
class CFileVersion : public MObject
{
public:
    enum FILE_VERSION
    {
        FILEV_BUF = 4096,
    };
public:
    CFileVersion(void);
    ~CFileVersion(void);

    bool   GetFileVersion(PCXStr pszFileName);
    bool   GetFileVersion(ULong& ulMajor, ULong& ulMinor) const;
    bool   GetProductVersion(ULong& ulMajor, ULong& ulMinor) const;
    PCXStr QueryFileVersion(PCXStr pszQuery);

    PCXStr GetFileVersion(void);
    PCXStr GetProductVersion(void);
    PCXStr GetInternalName(void);
    PCXStr GetCompanyName(void);
    PCXStr GetLegalCopyright(void);
    PCXStr GetOriginalFilename(void);
    PCXStr GetProductName(void);
    PCXStr GetFileDescription(void);
private:
    CFileVersion(const CFileVersion&);
    CFileVersion& operator=(const CFileVersion&);
private:
    ULong               m_ulLangCharset;
    ULong               m_ulInfoSize;
    VS_FIXEDFILEINFO*   m_pFileInfo;
    Byte                m_bBuffer[FILEV_BUF];
};

///////////////////////////////////////////////////////////////////
#include "fileversion.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __FILE_VERSION_H__