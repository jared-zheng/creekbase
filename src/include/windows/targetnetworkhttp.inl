// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_HTTP_INL__
#define __TARGET_NETWORK_HTTP_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CURI
INLINE bool CURI::EncodeLocal(const CString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    CBufReadStream Buf((stLen + 1) * ENT_UTF16_UTF8);
    PByte pszA = Buf.GetBuf();
    if (pszA != nullptr)
    {
        Int nRet = CXChar::Convert(*strIn, (Int)stLen, (PStr)pszA, (Int)(stLen * ENT_UTF16_UTF8));
        assert(nRet > 0);
        pszA[nRet] = 0;
#else
    Int   nRet = (Int)stLen;
    PByte pszA = (PByte)strIn.GetBuffer();
    if (pszA != nullptr)
    {
#endif
        for (Int i = 0; i < nRet; ++i)
        {
            Byte b = pszA[i];
            if (CURI::UnreservedChar((Char)b) || CURI::ReservedChar((Char)b))
            {
                strOut += (Char)b;
            }
            else
            {
                strOut += '%';
                strOut += (Char)CharToHex(b >> 4);
                strOut += (Char)CharToHex(b & 0x0F);
            }
        }
        return true;
    }
    return false;
}

INLINE bool CURI::DecodeLocal(const CCString& strIn, CString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    CBufReadStream Buf((stLen + 1) + (stLen + 1) * sizeof(WChar));
    PStr  pszA = (PStr)Buf.GetBuf();
    PWStr pszW = (PWStr)(pszA + (stLen + 1));
#else
    CBufReadStream Buf(stLen + 1);
    PStr  pszA = (PStr)Buf.GetBuf();
#endif
    if (pszA != nullptr)
    {
        Int nRet = 0;
        for (size_t i = 0; i < stLen; ++i)
        {
            if (strIn[i] != '%')
            {
                pszA[nRet] = strIn[i];
                ++nRet;
            }
            else if (i < (stLen - 2))
            {
                UChar uc1 = HexToChar(strIn[i + 1]);
                UChar uc2 = HexToChar(strIn[i + 2]);
                i += 2;

                pszA[nRet] = (Char)((uc1 << 4) + uc2);
                ++nRet;
            }
        }
        pszA[nRet] = 0;
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        nRet = CChar::Convert(pszA, nRet, pszW, (Int)(stLen));
        assert(nRet > 0);
        pszW[nRet] = 0;

        strOut = pszW;
#else
        strOut = pszA;
#endif
        return true;
    }
    return false;
}

INLINE bool CURI::EncodeLocalToUTF8(const CString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    CBufReadStream Buf((stLen + 1) * ENT_UTF16_UTF8);
    PByte pszA = Buf.GetBuf();
    if (pszA != nullptr)
    {
        Int nRet = CXChar::Convert(*strIn, (Int)stLen, (PStr)pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
        assert(nRet > 0);
        pszA[nRet] = 0;
#else
    CBufReadStream Buf((stLen + 1) * ENT_UTF16_UTF8 + (stLen + 1) * sizeof(WChar));
    PByte pszA = Buf.GetBuf();
    PWStr pszW = (PWStr)(pszA + (stLen + 1) * ENT_UTF16_UTF8);
    if (pszA != nullptr)
    {
        Int nRet = CChar::Convert(*strIn, (Int)stLen, pszW, (Int)(stLen));
        assert(nRet > 0);
        pszW[nRet] = 0;

        nRet = CChar::Convert(pszW, nRet, (PStr)pszA, (Int)(stLen * ENT_UTF16_UTF8), CP_UTF8);
        assert(nRet > 0);
        pszA[nRet] = 0;
#endif
        for (Int i = 0; i < nRet; ++i)
        {
            Byte b = pszA[i];
            if (CURI::UnreservedChar((Char)b) || CURI::ReservedChar((Char)b))
            {
                strOut += (Char)b;
            }
            else
            {
                strOut += '%';
                strOut += (Char)CharToHex(b >> 4);
                strOut += (Char)CharToHex(b & 0x0F);
            }
        }
        return true;
    }
    return false;
}

INLINE bool CURI::DecodeUTF8ToLocal(const CCString& strIn, CString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    CBufReadStream Buf((stLen + 1) + (stLen + 1) * sizeof(WChar));
    PStr  pszA = (PStr)Buf.GetBuf();
    PWStr pszW = (PWStr)(pszA + (stLen + 1));
#else
    CBufReadStream Buf((stLen + 1) + (stLen + 1) * sizeof(WChar) + (stLen + 1));
    PStr  pszA = (PStr)Buf.GetBuf();
    PWStr pszW = (PWStr)(pszA + (stLen + 1));
    PStr  pszL = (PStr)(pszW + (stLen + 1));
#endif
    if (pszA != nullptr)
    {
        Int nRet = 0;
        for (size_t i = 0; i < stLen; ++i)
        {
            if (strIn[i] != '%')
            {
                pszA[nRet] = strIn[i];
                ++nRet;
            }
            else if (i < (stLen - 2))
            {
                UChar uc1 = HexToChar(strIn[i + 1]);
                UChar uc2 = HexToChar(strIn[i + 2]);
                i += 2;

                pszA[nRet] = (Char)((uc1 << 4) + uc2);
                ++nRet;
            }
        }
        pszA[nRet] = 0;

        nRet = CChar::Convert(pszA, nRet, pszW, (Int)(stLen), CP_UTF8);
        assert(nRet > 0);
        pszW[nRet] = 0;

#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        strOut = pszW;
#else
        nRet = CChar::Convert(pszW, nRet, pszL, (Int)(stLen));
        assert(nRet > 0);
        pszL[nRet] = 0;
        strOut = pszL;
#endif
        return true;
    }
    return false;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_NETWORK_HTTP_INL__
