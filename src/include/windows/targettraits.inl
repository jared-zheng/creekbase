// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_TRAITS_INL__
#define __TARGET_TRAITS_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// Primitive Element Traits Define
//_DECLARE_PRIMITIVE_TRAITS( Handle ) == _DECLARE_PRIMITIVE_TRAITS( void* )
_DECLARE_PRIMITIVE_TRAITS( Module )

///////////////////////////////////////////////////////////////////
// CTXCharTraitsBase
template <typename T>
INLINE Int CTXCharTraitsBase<T>::Convert(PCStr pszC, Int nSizeC, PWStr pszW, Int nSizeW, CodePage cpPage)
{
    return ::MultiByteToWideChar((UInt)cpPage, 0, pszC, nSizeC, pszW, nSizeW);
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Convert(PCWStr pszW, Int nSizeW, PStr pszC, Int nSizeC, CodePage cpPage)
{
    return ::WideCharToMultiByte((UInt)cpPage, 0, pszW, nSizeW, pszC, nSizeC, nullptr, nullptr);
}

///////////////////////////////////////////////////////////////////
// CChar
INLINE bool CChar::IsAlnum(TChar ch)
{
    return (isalnum(ch) > 0);
}

INLINE bool CChar::IsAlpha(TChar ch)
{
    return (isalpha(ch) > 0);
}

INLINE bool CChar::IsPrint(TChar ch)
{
    return (isprint(ch) > 0);
}

INLINE bool CChar::IsGraph(TChar ch)
{
    return (isgraph(ch) > 0);
}

INLINE bool CChar::IsDigit(TChar ch)
{
    return (isdigit(ch) > 0);
}

INLINE bool CChar::IsXDigit(TChar ch)
{
    return (isxdigit(ch) > 0);
}

INLINE bool CChar::IsSpace(TChar ch)
{
    return (isspace(ch) > 0);
}

INLINE bool CChar::IsLower(TChar ch)
{
    return (islower(ch) > 0);
}

INLINE bool CChar::IsUpper(TChar ch)
{
    return (isupper(ch) > 0);
}

INLINE CChar::PTStr CChar::Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext)
{
    return strtok_s(pszStr, pszDelimit, &pszContext);
}

INLINE CChar::PTStr CChar::Rev(PTStr pszStr)
{
    return _strrev(pszStr);
}

INLINE CChar::PCTStr CChar::Chr(PCTStr pszStr, TChar ch)
{
    return strchr(pszStr, ch);
}

INLINE CChar::PCTStr CChar::RevChr(PCTStr pszStr, TChar ch)
{
    return strrchr(pszStr, ch);
}

INLINE CChar::PCTStr CChar::Str(PCTStr pszStr, PCTStr pszMatch)
{
    return strstr(pszStr, pszMatch);
}

INLINE CChar::PCTStr CChar::OneOf(PCTStr pszStr, PCTStr pszMatch)
{
    return strpbrk(pszStr, pszMatch);
}

INLINE size_t CChar::OneIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return strcspn(pszStr, pszMatch);
}

INLINE size_t CChar::NotIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return strspn(pszStr, pszMatch);
}

INLINE size_t CChar::Length(PCTStr pszStr, size_t stMax)
{
    return strnlen(pszStr, stMax);
}

INLINE Int CChar::Cmp(PCTStr pszA, PCTStr pszB)
{
    return strcmp(pszA, pszB);
}

INLINE Int CChar::Cmpi(PCTStr pszA, PCTStr pszB)
{
    return _stricmp(pszA, pszB);
}

INLINE Int CChar::Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return strncmp(pszA, pszB, stSize);
}

INLINE Int CChar::Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _strnicmp(pszA, pszB, stSize);
}

INLINE Int CChar::Coll(PCTStr pszA, PCTStr pszB)
{
    return strcoll(pszA, pszB);
}

INLINE Int CChar::Colli(PCTStr pszA, PCTStr pszB)
{
    return _stricoll(pszA, pszB);
}

INLINE Int CChar::Colln(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _strncoll(pszA, pszB, stSize);
}

INLINE Int CChar::Collin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _strnicoll(pszA, pszB, stSize);
}

INLINE Int CChar::FormatLength(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatLengthV(pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CChar::FormatLengthV(PCTStr pszFormat, va_list args)
{
    return _vscprintf(pszFormat, args);
}

INLINE Int CChar::Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatV(pszBuf, stSize, pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CChar::FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args)
{
    return _vsnprintf_s(pszBuf, stSize, _TRUNCATE, pszFormat, args);
}

INLINE bool CChar::Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount)
{
    return (_strnset_s(pszStr, stSize, ch, stCount) == 0);
}

INLINE bool CChar::Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (strncpy_s(pszDst, stDst, pszSrc, stSrc) == 0);
}

INLINE bool CChar::Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (strncat_s(pszDst, stDst, pszSrc, stSrc) == 0);
}

INLINE bool CChar::ToLower(PTStr pszStr, size_t stSize)
{
    return (_strlwr_s(pszStr, stSize) == 0);
}

INLINE bool CChar::ToUpper(PTStr pszStr, size_t stSize)
{
    return (_strupr_s(pszStr, stSize) == 0);
}

INLINE CChar::TChar CChar::ToLower(TChar ch)
{
    return (TChar)tolower(ch);
}

INLINE CChar::TChar CChar::ToUpper(TChar ch)
{
    return (TChar)toupper(ch);
}

INLINE Int CChar::ToInt(PCTStr pszStr)
{
    return atoi(pszStr);
}

INLINE Long CChar::ToLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtol(pszStr, ppszEnd, nRadix);
}

INLINE ULong CChar::ToULong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtoul(pszStr, ppszEnd, nRadix);
}

INLINE LLong CChar::ToLLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _strtoi64(pszStr, ppszEnd, nRadix);
}

INLINE ULLong CChar::ToULLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _strtoui64(pszStr, ppszEnd, nRadix);
}

INLINE Double CChar::ToDouble(PCTStr pszStr, PTStr* ppszEnd)
{
    return strtod(pszStr, ppszEnd);
}

INLINE bool CChar::ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_itoa_s(nValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CChar::ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ltoa_s(lValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CChar::ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ultoa_s(ulValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CChar::ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_i64toa_s(llValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CChar::ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ui64toa_s(ullValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CChar::ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_gcvt_s(pszStr, stSize, dValue, nRadix) == 0);
}

///////////////////////////////////////////////////////////////////
// CMChar
INLINE bool CMChar::IsAlnum(TChar ch)
{
    return (_ismbcalnum((UInt)ch) > 0);
}

INLINE bool CMChar::IsAlpha(TChar ch)
{
    return (_ismbcalpha((UInt)ch) > 0);
}

INLINE bool CMChar::IsPrint(TChar ch)
{
    return (_ismbcprint((UInt)ch) > 0);
}

INLINE bool CMChar::IsGraph(TChar ch)
{
    return (_ismbcgraph((UInt)ch) > 0);
}

INLINE bool CMChar::IsDigit(TChar ch)
{
    return (_ismbcdigit((UInt)ch) > 0);
}

INLINE bool CMChar::IsXDigit(TChar ch)
{
    return (isxdigit(ch) > 0);
}

INLINE bool CMChar::IsSpace(TChar ch)
{
    return (_ismbcspace((UInt)ch) > 0);
}

INLINE bool CMChar::IsLower(TChar ch)
{
    return (_ismbclower((UInt)ch) > 0);
}

INLINE bool CMChar::IsUpper(TChar ch)
{
    return (_ismbcupper((UInt)ch) > 0);
}

INLINE CMChar::PTStr CMChar::Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext)
{
    return (PTStr)_mbstok_s((PUStr)pszStr, (PCUStr)pszDelimit, (PUStr*)&pszContext);
}

INLINE CMChar::PTStr CMChar::Rev(PTStr pszStr)
{
    return (PTStr)_mbsrev((PUStr)pszStr);
}

INLINE CMChar::PCTStr CMChar::Chr(PCTStr pszStr, TChar ch)
{
    return (PCTStr)_mbschr((PCUStr)pszStr, (UInt)ch);
}

INLINE CMChar::PCTStr CMChar::RevChr(PCTStr pszStr, TChar ch)
{
    return (PCTStr)_mbsrchr((PCUStr)pszStr, (UInt)ch);
}

INLINE CMChar::PCTStr CMChar::Str(PCTStr pszStr, PCTStr pszMatch)
{
    return (PCTStr)_mbsstr((PCUStr)pszStr, (PCUStr)pszMatch);
}

INLINE CMChar::PCTStr CMChar::OneOf(PCTStr pszStr, PCTStr pszMatch)
{
    return (PCTStr)_mbspbrk((PCUStr)pszStr, (PCUStr)pszMatch);
}

INLINE size_t CMChar::OneIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return _mbscspn((PCUStr)pszStr, (PCUStr)pszMatch);
}

INLINE size_t CMChar::NotIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return _mbsspn((PCUStr)pszStr, (PCUStr)pszMatch);
}

INLINE size_t CMChar::Length(PCTStr pszStr, size_t stMax)
{
    return strnlen(pszStr, stMax);
}

INLINE Int CMChar::Cmp(PCTStr pszA, PCTStr pszB)
{
    return _mbscmp((PCUStr)pszA, (PCUStr)pszB);
}

INLINE Int CMChar::Cmpi(PCTStr pszA, PCTStr pszB)
{
    return _mbsicmp((PCUStr)pszA, (PCUStr)pszB);
}

INLINE Int CMChar::Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _mbsnbcmp((PCUStr)pszA, (PCUStr)pszB, stSize);
}

INLINE Int CMChar::Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _mbsnbicmp((PCUStr)pszA, (PCUStr)pszB, stSize);
}

INLINE Int CMChar::Coll(PCTStr pszA, PCTStr pszB)
{
    return _mbscoll((PCUStr)pszA, (PCUStr)pszB);
}

INLINE Int CMChar::Colli(PCTStr pszA, PCTStr pszB)
{
    return _mbsicoll((PCUStr)pszA, (PCUStr)pszB);
}

INLINE Int CMChar::Colln(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _mbsnbcoll((PCUStr)pszA, (PCUStr)pszB, stSize);
}

INLINE Int CMChar::Collin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _mbsnbicoll((PCUStr)pszA, (PCUStr)pszB, stSize);
}

INLINE Int CMChar::FormatLength(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatLengthV(pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CMChar::FormatLengthV(PCTStr pszFormat, va_list args)
{
    return _vscprintf(pszFormat, args);
}

INLINE Int CMChar::Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatV(pszBuf, stSize, pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CMChar::FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args)
{
    return _vsnprintf_s(pszBuf, stSize, _TRUNCATE, pszFormat, args);
}

INLINE bool CMChar::Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount)
{
    return (_mbsnbset_s((PUStr)pszStr, stSize, (UInt)ch, stCount) == 0);
}

INLINE bool CMChar::Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (_mbsnbcpy_s((PUStr)pszDst, stDst, (PCUStr)pszSrc, stSrc) == 0);
}

INLINE bool CMChar::Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (_mbsnbcat_s((PUStr)pszDst, stDst, (PCUStr)pszSrc, stSrc) == 0);
}

INLINE bool CMChar::ToLower(PTStr pszStr, size_t stSize)
{
    return (_mbslwr_s((PUStr)pszStr, stSize) == 0);
}

INLINE bool CMChar::ToUpper(PTStr pszStr, size_t stSize)
{
    return (_mbsupr_s((PUStr)pszStr, stSize) == 0);
}

INLINE CMChar::TChar CMChar::ToLower(TChar ch)
{
    return (TChar)_mbctolower((UInt)ch);
}

INLINE CMChar::TChar CMChar::ToUpper(TChar ch)
{
    return (TChar)_mbctoupper((UInt)ch);
}

INLINE Int CMChar::ToInt(PCTStr pszStr)
{
    return atoi(pszStr);
}

INLINE Long CMChar::ToLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtol(pszStr, ppszEnd, nRadix);
}

INLINE ULong CMChar::ToULong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtoul(pszStr, ppszEnd, nRadix);
}

INLINE LLong CMChar::ToLLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _strtoi64(pszStr, ppszEnd, nRadix);
}

INLINE ULLong CMChar::ToULLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _strtoui64(pszStr, ppszEnd, nRadix);
}

INLINE Double CMChar::ToDouble(PCTStr pszStr, PTStr* ppszEnd)
{
    return strtod(pszStr, ppszEnd);
}

INLINE bool CMChar::ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_itoa_s(nValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CMChar::ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ltoa_s(lValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CMChar::ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ultoa_s(ulValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CMChar::ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_i64toa_s(llValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CMChar::ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ui64toa_s(ullValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CMChar::ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_gcvt_s(pszStr, stSize, dValue, nRadix) == 0);
}

///////////////////////////////////////////////////////////////////
// CWChar
INLINE bool CWChar::IsAlnum(TChar ch)
{
    return (iswalnum(ch) > 0);
}

INLINE bool CWChar::IsAlpha(TChar ch)
{
    return (iswalpha(ch) > 0);
}

INLINE bool CWChar::IsPrint(TChar ch)
{
    return (iswprint(ch) > 0);
}

INLINE bool CWChar::IsGraph(TChar ch)
{
    return (iswgraph(ch) > 0);
}

INLINE bool CWChar::IsDigit(TChar ch)
{
    return (iswdigit(ch) > 0);
}

INLINE bool CWChar::IsXDigit(TChar ch)
{
    return (iswxdigit(ch) > 0);
}

INLINE bool CWChar::IsSpace(TChar ch)
{
    return (iswspace(ch) > 0);
}

INLINE bool CWChar::IsLower(TChar ch)
{
    return (iswlower(ch) > 0);
}

INLINE bool CWChar::IsUpper(TChar ch)
{
    return (iswupper(ch) > 0);
}

INLINE CWChar::PTStr CWChar::Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext)
{
    return wcstok_s(pszStr, pszDelimit, &pszContext);
}

INLINE CWChar::PTStr CWChar::Rev(PTStr pszStr)
{
    return _wcsrev(pszStr);
}

INLINE CWChar::PCTStr CWChar::Chr(PCTStr pszStr, TChar ch)
{
    return wcschr(pszStr, ch);
}

INLINE CWChar::PCTStr CWChar::RevChr(PCTStr pszStr, TChar ch)
{
    return wcsrchr(pszStr, ch);
}

INLINE CWChar::PCTStr CWChar::Str(PCTStr pszStr, PCTStr pszMatch)
{
    return wcsstr(pszStr, pszMatch);
}

INLINE CWChar::PCTStr CWChar::OneOf(PCTStr pszStr, PCTStr pszMatch)
{
    return wcspbrk(pszStr, pszMatch);
}

INLINE size_t CWChar::OneIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return wcscspn(pszStr, pszMatch);
}

INLINE size_t CWChar::NotIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return wcsspn(pszStr, pszMatch);
}

INLINE size_t CWChar::Length(PCTStr pszStr, size_t stMax)
{
    return wcsnlen(pszStr, stMax);
}

INLINE Int CWChar::Cmp(PCTStr pszA, PCTStr pszB)
{
    return wcscmp(pszA, pszB);
}

INLINE Int CWChar::Cmpi(PCTStr pszA, PCTStr pszB)
{
    return _wcsicmp(pszA, pszB);
}

INLINE Int CWChar::Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return wcsncmp(pszA, pszB, stSize);
}

INLINE Int CWChar::Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _wcsnicmp(pszA, pszB, stSize);
}

INLINE Int CWChar::Coll(PCTStr pszA, PCTStr pszB)
{
    return wcscoll(pszA, pszB);
}

INLINE Int CWChar::Colli(PCTStr pszA, PCTStr pszB)
{
    return _wcsicoll(pszA, pszB);
}

INLINE Int CWChar::Colln(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _wcsncoll(pszA, pszB, stSize);
}

INLINE Int CWChar::Collin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return _wcsnicoll(pszA, pszB, stSize);
}

INLINE Int CWChar::FormatLength(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatLengthV(pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CWChar::FormatLengthV(PCTStr pszFormat, va_list args)
{
    return _vscwprintf(pszFormat, args);
}

INLINE Int CWChar::Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatV(pszBuf, stSize, pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CWChar::FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args)
{
    return _vsnwprintf_s(pszBuf, stSize, _TRUNCATE, pszFormat, args);
}

INLINE bool CWChar::Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount)
{
    return (_wcsnset_s(pszStr, stSize, ch, stCount) == 0);
}

INLINE bool CWChar::Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (wcsncpy_s(pszDst, stDst, pszSrc, stSrc) == 0);
}

INLINE bool CWChar::Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    return (wcsncat_s(pszDst, stDst, pszSrc, stSrc) == 0);
}

INLINE bool CWChar::ToLower(PTStr pszStr, size_t stSize)
{
    return (_wcslwr_s(pszStr, stSize) == 0);
}

INLINE bool CWChar::ToUpper(PTStr pszStr, size_t stSize)
{
    return (_wcsupr_s(pszStr, stSize) == 0);
}

INLINE CWChar::TChar CWChar::ToLower(TChar ch)
{
    return (TChar)towlower(ch);
}

INLINE CWChar::TChar CWChar::ToUpper(TChar ch)
{
    return (TChar)towupper(ch);
}

INLINE Int CWChar::ToInt(PCTStr pszStr)
{
    return _wtoi(pszStr);
}

INLINE Long CWChar::ToLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstol(pszStr, ppszEnd, nRadix);
}

INLINE ULong CWChar::ToULong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstoul(pszStr, ppszEnd, nRadix);
}

INLINE LLong CWChar::ToLLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _wcstoi64(pszStr, ppszEnd, nRadix);
}

INLINE ULLong CWChar::ToULLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return _wcstoui64(pszStr, ppszEnd, nRadix);
}

INLINE Double CWChar::ToDouble(PCTStr pszStr, PTStr* ppszEnd)
{
    return wcstod(pszStr, ppszEnd);
}

INLINE bool CWChar::ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_itow_s(nValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CWChar::ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ltow_s(lValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CWChar::ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ultow_s(ulValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CWChar::ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_i64tow_s(llValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CWChar::ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    return (_ui64tow_s(ullValue, pszStr, stSize, nRadix) == 0);
}

INLINE bool CWChar::ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    Char cTemp[_CVTBUFSIZE] = { 0 };
    if (_gcvt_s(cTemp, _CVTBUFSIZE, dValue, nRadix) == 0)
    {
        if (stSize > _CVTBUFSIZE)
        {
            stSize = _CVTBUFSIZE;
        }
        for (size_t i = 0; i < stSize; ++i)
        {
            if (cTemp[i] != 0)
            {
                pszStr[i] = (TChar)cTemp[i];
            }
        }
        pszStr[stSize - 1] = 0;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXChar
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    typedef CWChar   CXChar;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_MBCS)
    typedef CMChar   CXChar;
#else
    typedef CChar    CXChar;
#endif

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_TRAITS_INL__
