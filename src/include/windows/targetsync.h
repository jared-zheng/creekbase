// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_SYNC_H__
#define __TARGET_SYNC_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "atomics.h"
#include "handle.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CSyncTraits
class CSyncTraits
{
public:
    enum SYNC_TYPE
    {
        SYNCT_COUNTER = 1,
        SYNCT_LOCK,
        SYNCT_MUTEX,
        SYNCT_EVENT,
        SYNCT_SEMA,
    };
};

typedef SECURITY_ATTRIBUTES   SyncAttr;
typedef SyncAttr              LockAttr;
typedef SyncAttr              MutexAttr;
typedef SyncAttr              EventAttr;
typedef SyncAttr              SemaAttr;
typedef SyncAttr              ConditionAttr;

typedef SyncAttr              RWLockAttr;
typedef SyncAttr              TimerAttr;

///////////////////////////////////////////////////////////////////
// CSyncBase
class NOVTABLE CSyncBase ABSTRACT : public MObject
{
public:
    virtual Int  Signal(void) PURE;
    virtual Int  Reset(void)  PURE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) PURE;
    virtual bool IsValid(void) const PURE;
    virtual void Close(void) PURE;

protected:
    CSyncBase(void);
    virtual ~CSyncBase(void);

    CSyncBase(const CSyncBase&);
    CSyncBase& operator=(const CSyncBase&);
};

///////////////////////////////////////////////////////////////////
// CTSyncBase
template <Int nType>
class NOVTABLE CTSyncBase ABSTRACT : public CSyncBase
{
public:
    Int GetType(void) const;
protected:
    CTSyncBase(void);
    virtual ~CTSyncBase(void);

    CTSyncBase(const CTSyncBase&);
    CTSyncBase& operator=(const CTSyncBase&);
};

///////////////////////////////////////////////////////////////////
// CTSyncScope
// This is a utility class that handles scope level locking. It's very useful
// to keep from causing deadlocks due to exceptions being caught and knowing
// about the number of locks a given thread has on a resource. Example:
//
// <code>
//    {
//        // Syncronize thread access to the following data
//        CTSyncScope Scope(SynchObject);
//        // Access data that is shared among multiple threads
//        ...
//        // When Scope goes out of scope, other threads can access data
//    }
// </code>
//
template <Int nType, bool bWait = false>
class CTSyncScope : public MObject
{
public:
    explicit CTSyncScope(CTSyncBase<nType>& Sync, UInt uWait = (UInt)TIMET_INFINITE);
    ~CTSyncScope(void);

    Int GetRet(void) const;
private:
    CTSyncScope(const CTSyncScope&);
    CTSyncScope& operator=(const CTSyncScope&);
private:
    CTSyncBase<nType>*   m_pSync;
    Int                  m_nRet;
};

typedef CTSyncScope<CSyncTraits::SYNCT_COUNTER>       CSyncCounterScope;
typedef CTSyncScope<CSyncTraits::SYNCT_LOCK>          CSyncLockScope;
typedef CTSyncScope<CSyncTraits::SYNCT_MUTEX>         CSyncMutexScope;
typedef CTSyncScope<CSyncTraits::SYNCT_EVENT>         CSyncEventScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SEMA>          CSyncSemaScope;
typedef CTSyncScope<CSyncTraits::SYNCT_COUNTER, true> CSyncCounterWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_LOCK,    true> CSyncLockWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_MUTEX,   true> CSyncMutexWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_EVENT,   true> CSyncEventWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SEMA,    true> CSyncSemaWaitScope;

///////////////////////////////////////////////////////////////////
// CSyncCounter
class CSyncCounter : public CTSyncBase<CSyncTraits::SYNCT_COUNTER>
{
public:
    CSyncCounter(Int nCounter = 0);
    virtual ~CSyncCounter(void);

    virtual Int  Signal(void) OVERRIDE; // return m_nCounter after increment
    virtual Int  Reset(void) OVERRIDE;  // return m_nCounter after decrement
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE; // RET_OKAY, wait always return okay
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    Int     GetCount(void);
public:
    CSyncCounter(const CSyncCounter&);
    CSyncCounter& operator=(const CSyncCounter&);
private:
    Int   m_nCounter;
};

///////////////////////////////////////////////////////////////////
// CSyncLock
class CSyncLock : public CTSyncBase<CSyncTraits::SYNCT_LOCK>
{
#if (NTDDI_VERSION >= NTDDI_VISTA)
    friend class CCondition;
#endif
public:
    CSyncLock(bool bCreate = true, LockAttr* pAttr = nullptr);
    virtual ~CSyncLock(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE; // TIMET_IGNORE return RET_FAIL if other thread owned lock
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(LockAttr* pAttr = nullptr);
private:
    CSyncLock(const CSyncLock&);
    CSyncLock& operator=(const CSyncLock&);
private:
    CRITICAL_SECTION  m_CSLock;
};

///////////////////////////////////////////////////////////////////
// CSyncMutex
class CSyncMutex : public CTSyncBase<CSyncTraits::SYNCT_MUTEX>
{
public:
    CSyncMutex(bool bCreate = true, bool bCreateSignal = false, PCXStr pszName = nullptr, MutexAttr* pAttr = nullptr);
    virtual ~CSyncMutex(void);

    virtual Int  Signal(void) OVERRIDE; // INFINITE wait
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(bool bCreateSignal = false, PCXStr pszName = nullptr, MutexAttr* pAttr = nullptr);
private:
    CSyncMutex(const CSyncMutex&);
    CSyncMutex& operator=(const CSyncMutex&);
public:
    CHandle         m_hMutex;
};

///////////////////////////////////////////////////////////////////
// CSyncEvent
class CSyncEvent : public CTSyncBase<CSyncTraits::SYNCT_EVENT>
{
public:
    CSyncEvent(bool bCreate = true, bool bCreateSignal = false, bool bManualReset = false, PCXStr pszName = nullptr, EventAttr* pAttr = nullptr);
    virtual ~CSyncEvent(void);

    virtual Int  Signal(void) OVERRIDE; // signaled return RET_OKAY, other return RET_FAIL
    virtual Int  Reset(void) OVERRIDE;  // manual should call. okay return RET_OKAY, else RET_FAIL
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(bool bCreateSignal = false, bool bManualReset = false, PCXStr pszName = nullptr, EventAttr* pAttr = nullptr);
private:
    CSyncEvent(const CSyncEvent&);
    CSyncEvent& operator=(const CSyncEvent&);
public:
    CHandle   m_hEvent;
};

///////////////////////////////////////////////////////////////////
// CSyncSema
class CSyncSema : public CTSyncBase<CSyncTraits::SYNCT_SEMA>
{
public:
    CSyncSema(bool bCreate = true, bool bCreateSignal = false, Int nLockCount = 1, PCXStr pszName = nullptr, SemaAttr* pAttr = nullptr);
    virtual ~CSyncSema(void);

    virtual Int  Signal(void) OVERRIDE; // INFINITE wait
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(bool bCreateSignal = false, Int nLockCount = 1, PCXStr pszName = nullptr, SemaAttr* pAttr = nullptr);
private:
    CSyncSema(const CSyncSema&);
    CSyncSema& operator=(const CSyncSema&);
public:
    CHandle   m_hSemaphore;
};

#if (NTDDI_VERSION >= NTDDI_VISTA)
///////////////////////////////////////////////////////////////////
// CCondition :
// Condition variables are synchronization primitives that enable threads to wait until a particular condition occurs.
// Condition variables are user-mode objects that cannot be shared across processes.
class CRWLock;
class CCondition : public MObject
{
public:
    CCondition(ConditionAttr* pAttr = nullptr);
    ~CCondition(void);

    bool Wait(CSyncLock& Lock, UInt uWait = (UInt)TIMET_INFINITE);
    bool Wait(CRWLock& RWLock, bool bShared = true, UInt uWait = (UInt)TIMET_INFINITE);

    bool Wake(bool bAll = false);
    bool IsValid(void) const;
private:
    CCondition(const CCondition&);
    CCondition& operator=(const CCondition&);
private:
    CONDITION_VARIABLE   m_CVar;
};

///////////////////////////////////////////////////////////////////
// CRWLock :
// Slim reader/writer (SRW) locks enable the threads of a single process to access shared resources;
// they are optimized for speed and occupy very little memory.
class CRWLock : public MObject
{
    friend class CCondition;
public:
    CRWLock(bool bManual = false, RWLockAttr* pAttr = nullptr);
    ~CRWLock(void);

    bool LockRead(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockRead(void);

    bool LockWrite(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockWrite(void);

    bool IsValid(void) const;
private:
    CRWLock(const CRWLock&);
    CRWLock& operator=(const CRWLock&);
private:
    bool         m_bManual;
    SRWLOCK      m_RWLock;
    CCondition   m_ReadCond;
    CCondition   m_WriteCond;
};
#else  // (_WIN32_WINNT >= NTDDI_VISTA)
#pragma message("Windows XP No Implement")
class CRWLock : public MObject
{
public:
    CRWLock(bool bManual = false, RWLockAttr* pAttr = nullptr);
    ~CRWLock(void);

    bool LockRead(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockRead(void);

    bool LockWrite(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockWrite(void);

    bool IsValid(void) const;
private:
    CRWLock(const CRWLock&);
    CRWLock& operator=(const CRWLock&);
private:
    UInt         m_uCount;
    CSyncEvent   m_NoneReadEvent;
    CSyncMutex   m_WriteMutex;
    CSyncLock    m_ReadLock;
};
#endif // (_WIN32_WINNT >= NTDDI_VISTA)

///////////////////////////////////////////////////////////////////
// CTRWLockScope
template <bool bRead = true>
class CTRWLockScope : public MObject
{
public:
    explicit CTRWLockScope(CRWLock& RWLock, UInt uWait = (UInt)TIMET_INFINITE);
    ~CTRWLockScope(void);
private:
    CTRWLockScope(const CTRWLockScope&);
    CTRWLockScope& operator=(const CTRWLockScope&);
private:
    CRWLock*   m_pRWLock;
};

typedef CTRWLockScope<true>  CRWLockReadScope;
typedef CTRWLockScope<false> CRWLockWriteScope;

///////////////////////////////////////////////////////////////////
// CWaitableTimer
class CWaitableTimer : public MObject
{
public:
    CWaitableTimer(bool bCreate = true, PCXStr pszName = nullptr, TimerAttr* pAttr = nullptr);
    ~CWaitableTimer(void);

    bool SetTimer(LLong llBegin, Long lInterval, PTIMERAPCROUTINE pRoutine = nullptr, void* pRoutineArg = nullptr);
    bool CancelTimer(void);

    Int  Wait(UInt uWait = (UInt)TIMET_INFINITE);

    bool IsValid(void) const;

    bool CreateTimer(PCXStr pszName = nullptr, TimerAttr* pAttr = nullptr);
    void CloseTimer(void);
private:
    CWaitableTimer(const CWaitableTimer&);
    CWaitableTimer& operator=(const CWaitableTimer&);
public:
    CHandle   m_hTimer;
};

///////////////////////////////////////////////////////////////////
#include "targetsync.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_SYNC_H__
