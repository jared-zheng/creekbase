// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_HANDLE_H__
#define __TARGET_HANDLE_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#include "mobject.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CHandle : basic Kernel Objects handle, closed by CloseHandle
class CHandle : public MObject
{
public:
    CHandle(Handle h = HANDLE_INVALID);
    ~CHandle(void);

    CHandle(const CHandle& aSrc);
    CHandle& operator=(const CHandle& aSrc);
    CHandle& operator=(Handle h);

    bool   operator==(Handle h) const;
    bool   operator!=(Handle h) const;
    bool   operator==(const CHandle& aSrc) const;
    bool   operator!=(const CHandle& aSrc) const;

    operator Handle(void) const;

    bool     IsValid(void) const;

    void     Attach(Handle h);
    Handle   Detach(void);

    void     Close(void);
public:
    Handle   m_Handle;
};

typedef CHandle   CFileHandle;
typedef CHandle   CMappingHandle;

///////////////////////////////////////////////////////////////////
// CThreadAttr
class CThreadAttr : public MObject
{
public:
    CThreadAttr(LPSECURITY_ATTRIBUTES pSA = nullptr);
    ~CThreadAttr(void);

    CThreadAttr(const CThreadAttr& aSrc);
    CThreadAttr& operator=(const CThreadAttr& aSrc);

    void     Reset(void);
public:
    LPSECURITY_ATTRIBUTES   m_pSA;
    size_t                  m_stStack;
    ULong                   m_ulFlags;
};

///////////////////////////////////////////////////////////////////
// CThreadHandle
class CThreadHandle : public CHandle
{
public:
    CThreadHandle(Handle h = nullptr);
    ~CThreadHandle(void);

    CThreadHandle(const CThreadHandle& aSrc);
    CThreadHandle& operator=(const CThreadHandle& aSrc);
    CThreadHandle& operator=(Handle h);

    bool   operator==(Handle h) const;
    bool   operator!=(Handle h) const;
    bool   operator==(const CThreadHandle& aSrc) const;
    bool   operator!=(const CThreadHandle& aSrc) const;

    void   Reset(void);
public:
    TId    m_TId;
};

///////////////////////////////////////////////////////////////////
// module: basic Kernel Objects handle, closed by FreeLibrary(dll or exe)
class CModuleHandle : public MObject
{
public:
    CModuleHandle(Module m = nullptr);
    ~CModuleHandle(void);

    CModuleHandle(const CModuleHandle& aSrc);
    CModuleHandle& operator=(const CModuleHandle& aSrc);
    CModuleHandle& operator=(Module m);

    bool   operator==(Module m) const;
    bool   operator!=(Module m) const;
    bool   operator==(const CModuleHandle& aSrc) const;
    bool   operator!=(const CModuleHandle& aSrc) const;

    operator Module(void) const;

    bool     IsValid(void) const;

    void     Attach(Module m);
    Module   Detach(void);

    void     Close(void);

    FARPROC  Find(PCStr pszSymbol);
    bool     Load(PCXStr pszPath);
public:
    Module   m_Module;
};

///////////////////////////////////////////////////////////////////
// pid
class CProcessHandle : public MObject
{
public:
    static PId GetParentId(void);
    static PId GetGroupId(PId pid = 0);
public:
    CProcessHandle(void);
    ~CProcessHandle(void);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CProcessHandle(CProcessHandle&& aSrc);
    CProcessHandle& operator=(CProcessHandle&& aSrc);
#endif

    bool      operator==(PId pid) const;
    bool      operator!=(PId pid) const;

    operator  PId(void) const;
    Handle    Get(void) const;

    bool      IsValid(void) const;
    bool      Check(void) const;

    void      Attach(CProcessHandle& aSrc);
    void      Detach(void);

    Int       Wait(UInt uWait = TIMET_IGNORE) const;

    bool      Open(PCXStr pszCMD, PCXStr pszPWD = nullptr, UInt uFlag = CREATE_NEW_CONSOLE);
    void      Close(void);
private:
    CProcessHandle(const CProcessHandle& aSrc);
    CProcessHandle& operator=(const CProcessHandle& aSrc);
public:
    PId       m_PId;
    CHandle   m_Handle;
};

///////////////////////////////////////////////////////////////////
#include "targethandle.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_HANDLE_H__
