// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_EXCEPTION_INL__
#define __TARGET_EXCEPTION_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

// Exception need pdb file
#pragma comment(lib, "dbghelp.lib")

///////////////////////////////////////////////////////////////////
// CSEHException
class CSEHException : public MObject
{
public:
    static CPCXStr DumpFileName;
public:
    static Int Dump(PEXCEPTION_POINTERS pException, PCXStr pszName, Int nDumpType = 0);
    static Int Dump(PEXCEPTION_POINTERS pException, CStream& stream);
private:
    CSEHException(void);
    ~CSEHException(void);
    CSEHException(const CSEHException&);
    CSEHException& operator=(const CSEHException&);
};

#define CATCH_EXCEPTION_DUMP( desc )   } __except (CSEHException::Dump(GetExceptionInformation(), TF(#desc))) {

SELECTANY CPCXStr CSEHException::DumpFileName   = TF("%s[%d-%d].%04d-%02d-%02d(%02d-%02d-%02d).dmp");

INLINE CSEHException::CSEHException(void)
{
}

INLINE CSEHException::~CSEHException(void)
{
}

INLINE CSEHException::CSEHException(const CSEHException&)
{
}

INLINE CSEHException& CSEHException::operator=(const CSEHException&)
{
    return (*this);
}

INLINE Int CSEHException::Dump(PEXCEPTION_POINTERS pException, PCXStr pszName, Int nDumpType)
{
    CPlatform::TIMEINFO ti;
    CPlatform::GetTimeInfo(ti);

    XChar szPath[LMT_MAX_PATH + LMT_MAX_PATH] = { 0 };
    size_t stSize = CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);
    CXChar::Format(szPath + stSize, LMT_MAX_PATH, DumpFileName, pszName, CPlatform::GetCurrentPId(), CPlatform::GetCurrentTId(),
                   ti.usYear, ti.usMonth, ti.usDay, ti.usHour, ti.usMinute, ti.usSecond);
    DEV_DEBUG(TF("Dump file name is : %s"), szPath);
    Handle hDump = ::CreateFile(szPath, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
    if (hDump != HANDLE_INVALID)
    {
        MINIDUMP_EXCEPTION_INFORMATION mei;
        mei.ThreadId          = CPlatform::GetCurrentTId();
        mei.ExceptionPointers = pException;
        mei.ClientPointers    = FALSE;

        ::MiniDumpWriteDump(::GetCurrentProcess(), CPlatform::GetCurrentPId(), hDump, (MINIDUMP_TYPE)nDumpType, &mei, nullptr, nullptr);

        ::CloseHandle(hDump);
    }

    szPath[stSize] = 0;
    CFileLog Dumplog(true, LOGL_ALL, szPath, pszName);

    LOG_ERROR(Dumplog, TF("Exception Record : -->"));
    MEMORY_BASIC_INFORMATION mbi = { 0 };
    if (::VirtualQuery(reinterpret_cast<void*>(pException->ExceptionRecord->ExceptionAddress), &mbi, sizeof(MEMORY_BASIC_INFORMATION)) > 0)
    {
        Module m = reinterpret_cast<Module>(mbi.AllocationBase);
        ::GetModuleFileName(m, szPath, LMT_MAX_PATH);

        LOGV_ERROR(Dumplog, TF("ExceptionAddress : %p(%s)"), pException->ExceptionRecord->ExceptionAddress, szPath);
    }
    LOGV_ERROR(Dumplog, TF("ExceptionCode    : %#X"), pException->ExceptionRecord->ExceptionCode);
    LOGV_ERROR(Dumplog, TF("ExceptionFlags   : %d"),  pException->ExceptionRecord->ExceptionFlags);
    LOGV_ERROR(Dumplog, TF("NumberParameters : %d"),  pException->ExceptionRecord->NumberParameters);
    LOGV_ERROR(Dumplog, TF("ExceptionStack   :"));

    CONTEXT Context;
    MM_SAFE::Cpy(&Context, sizeof(CONTEXT), pException->ContextRecord, sizeof(CONTEXT));
    STACKFRAME StackFrame;
    MM_SAFE::Set(&StackFrame, 0, sizeof(STACKFRAME));

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    StackFrame.AddrPC.Offset     = Context.Rip;
    StackFrame.AddrPC.Mode       = AddrModeFlat;
    StackFrame.AddrStack.Offset  = Context.Rsp;
    StackFrame.AddrStack.Mode    = AddrModeFlat;
    StackFrame.AddrFrame.Offset  = Context.Rbp;
    StackFrame.AddrFrame.Mode    = AddrModeFlat;

    ULong ulMachineType          = IMAGE_FILE_MACHINE_AMD64;
#else
    StackFrame.AddrPC.Offset     = Context.Eip;
    StackFrame.AddrPC.Mode       = AddrModeFlat;
    StackFrame.AddrStack.Offset  = Context.Esp;
    StackFrame.AddrStack.Mode    = AddrModeFlat;
    StackFrame.AddrFrame.Offset  = Context.Ebp;
    StackFrame.AddrFrame.Mode    = AddrModeFlat;

    ULong ulMachineType          = IMAGE_FILE_MACHINE_I386;
#endif
    ULong ulRet = 0;
    Byte bSymInfo[sizeof(SYMBOL_INFO) + LMT_MAX_NAME * sizeof(XChar)] = { 0 };
    PSYMBOL_INFO pSymbol  = reinterpret_cast<PSYMBOL_INFO>(bSymInfo);
    pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    pSymbol->MaxNameLen   = (ULong)LMT_MAX_NAME;

    ::SymSetOptions(SYMOPT_LOAD_LINES);

    HANDLE hProcess = ::GetCurrentProcess();
    ::SymInitialize(hProcess, nullptr, TRUE);
    for (; ;)
    {
        if (::StackWalk(ulMachineType, hProcess, ::GetCurrentThread(), &StackFrame, &Context, nullptr, SymFunctionTableAccess, SymGetModuleBase, nullptr) == FALSE)
        {
            break;
        }
        if (StackFrame.AddrFrame.Offset == 0)
        {
            break;
        }
        ::SymFromAddr(hProcess, StackFrame.AddrPC.Offset, nullptr, pSymbol);

        IMAGEHLP_LINE ImgLine = { 0 };
        ImgLine.SizeOfStruct  = sizeof(IMAGEHLP_LINE);
        ::SymGetLineFromAddr(hProcess, StackFrame.AddrPC.Offset, &ulRet, &ImgLine);
#if (NTDDI_VERSION >= NTDDI_WIN7)
        LOGV_ERROR(Dumplog, TF("-%p : %s[%s, %d]"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#else
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        LOGV_ERROR(Dumplog, TF("-%p : %S[%S, %d]"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#else
        LOGV_ERROR(Dumplog, TF("-%p : %s[%s, %d]"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#endif
#endif
    }
    ::SymCleanup(hProcess);
    return EXCEPTION_EXECUTE_HANDLER;
}

INLINE Int CSEHException::Dump(PEXCEPTION_POINTERS pException, CStream& Stream)
{
    CString strDump;
    strDump.Format(TF("Exception Thread : %d---%d\r\n"), CPlatform::GetCurrentPId(), CPlatform::GetCurrentTId());
    Stream <= strDump;

    MEMORY_BASIC_INFORMATION mbi = { 0 };
    if (::VirtualQuery(reinterpret_cast<void*>(pException->ExceptionRecord->ExceptionAddress), &mbi, sizeof(MEMORY_BASIC_INFORMATION)) > 0)
    {
        Module m = reinterpret_cast<Module>(mbi.AllocationBase);
        XChar szPath[LMT_MAX_PATH] = { 0 };
        ::GetModuleFileName(m, szPath, LMT_MAX_PATH);

        strDump.Format(TF("ExceptionAddress : %p(%s)\r\n"), pException->ExceptionRecord->ExceptionAddress, szPath);
        Stream <= strDump;
    }
    strDump.Format(TF("ExceptionCode    : %#X\r\n"), pException->ExceptionRecord->ExceptionCode);
    Stream <= strDump;
    strDump.Format(TF("ExceptionFlags   : %d\r\n"),  pException->ExceptionRecord->ExceptionFlags);
    Stream <= strDump;
    strDump.Format(TF("NumberParameters : %d\r\n"),  pException->ExceptionRecord->NumberParameters);
    Stream <= strDump;
    strDump = TF("ExceptionStack   :\r\n");
    Stream <= strDump;

    CONTEXT Context;
    MM_SAFE::Cpy(&Context, sizeof(CONTEXT), pException->ContextRecord, sizeof(CONTEXT));
    STACKFRAME StackFrame;
    MM_SAFE::Set(&StackFrame, 0, sizeof(STACKFRAME));

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    StackFrame.AddrPC.Offset     = Context.Rip;
    StackFrame.AddrPC.Mode       = AddrModeFlat;
    StackFrame.AddrStack.Offset  = Context.Rsp;
    StackFrame.AddrStack.Mode    = AddrModeFlat;
    StackFrame.AddrFrame.Offset  = Context.Rbp;
    StackFrame.AddrFrame.Mode    = AddrModeFlat;

    ULong ulMachineType          = IMAGE_FILE_MACHINE_AMD64;
#else
    StackFrame.AddrPC.Offset     = Context.Eip;
    StackFrame.AddrPC.Mode       = AddrModeFlat;
    StackFrame.AddrStack.Offset  = Context.Esp;
    StackFrame.AddrStack.Mode    = AddrModeFlat;
    StackFrame.AddrFrame.Offset  = Context.Ebp;
    StackFrame.AddrFrame.Mode    = AddrModeFlat;

    ULong ulMachineType          = IMAGE_FILE_MACHINE_I386;
#endif
    ULong ulRet = 0;
    Byte bSymInfo[sizeof(SYMBOL_INFO) + LMT_MAX_NAME * sizeof(XChar)] = { 0 };
    PSYMBOL_INFO pSymbol  = reinterpret_cast<PSYMBOL_INFO>(bSymInfo);
    pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    pSymbol->MaxNameLen   = (ULong)LMT_MAX_NAME;

    ::SymSetOptions(SYMOPT_LOAD_LINES);

    HANDLE hProcess = ::GetCurrentProcess();
    ::SymInitialize(hProcess, nullptr, TRUE);
    for (; ;)
    {
        if (::StackWalk(ulMachineType, hProcess, ::GetCurrentThread(), &StackFrame, &Context, nullptr, SymFunctionTableAccess, SymGetModuleBase, nullptr) == FALSE)
        {
            break;
        }
        if (StackFrame.AddrFrame.Offset == 0)
        {
            break;
        }
        ::SymFromAddr(hProcess, StackFrame.AddrPC.Offset, nullptr, pSymbol);

        IMAGEHLP_LINE ImgLine = { 0 };
        ImgLine.SizeOfStruct  = sizeof(IMAGEHLP_LINE);
        ::SymGetLineFromAddr(hProcess, StackFrame.AddrPC.Offset, &ulRet, &ImgLine);
#if (NTDDI_VERSION >= NTDDI_WIN7)
        strDump.Format(TF("-%p : %s[%s, %d]\r\n"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#else
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        strDump.Format(TF("-%p : %S[%S, %d]\r\n"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#else
        strDump.Format(TF("-%p : %s[%s, %d]\r\n"), reinterpret_cast<void*>(StackFrame.AddrPC.Offset), pSymbol->Name, ImgLine.FileName, ImgLine.LineNumber);
#endif
#endif
        Stream <= strDump;
    }
    ::SymCleanup(hProcess);
    return EXCEPTION_EXECUTE_HANDLER;
}

///////////////////////////////////////////////////////////////////
// CSEHExceptionFilter
class CSEHExceptionFilter : public MObject
{
public:
    static bool Init(Int nType = 0);
    static void Exit(void);

    static Long WINAPI UnhandledExceptFilter(PEXCEPTION_POINTERS pException);
private:
    CSEHExceptionFilter(void);
    ~CSEHExceptionFilter(void);
    CSEHExceptionFilter(const CSEHExceptionFilter&);
    CSEHExceptionFilter& operator=(const CSEHExceptionFilter&);
private:
    static Int                            ms_nType;
    static LPTOP_LEVEL_EXCEPTION_FILTER   ms_pPrevFilter;
};

SELECTANY Int                          CSEHExceptionFilter::ms_nType       = 0;
SELECTANY LPTOP_LEVEL_EXCEPTION_FILTER CSEHExceptionFilter::ms_pPrevFilter = nullptr;

INLINE CSEHExceptionFilter::CSEHExceptionFilter(void)
{
}

INLINE CSEHExceptionFilter::~CSEHExceptionFilter(void)
{
}

INLINE CSEHExceptionFilter::CSEHExceptionFilter(const CSEHExceptionFilter&)
{
}

INLINE CSEHExceptionFilter& CSEHExceptionFilter::operator=(const CSEHExceptionFilter&)
{
    return (*this);
}

INLINE bool CSEHExceptionFilter::Init(Int nType)
{
    if (ms_pPrevFilter != nullptr)
    {
        return true;
    }

    ms_nType       = nType;
    ms_pPrevFilter = ::SetUnhandledExceptionFilter(&CSEHExceptionFilter::UnhandledExceptFilter);
    return true;
}

INLINE void CSEHExceptionFilter::Exit(void)
{
    if (ms_pPrevFilter != nullptr)
    {
        ::SetUnhandledExceptionFilter(ms_pPrevFilter);
        ms_pPrevFilter = nullptr;
    }
}

INLINE Long WINAPI CSEHExceptionFilter::UnhandledExceptFilter(PEXCEPTION_POINTERS pException)
{
    XChar szPath[LMT_MAX_PATH] = { 0 };
    ::GetModuleFileName(nullptr, szPath, LMT_MAX_PATH);
    PXStr p = (PXStr)CXChar::RevChr(szPath, TF('\\'));
    assert(p > szPath);
    ++p;
    return CSEHException::Dump(pException, p, ms_nType);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_EXCEPTION_INL__
