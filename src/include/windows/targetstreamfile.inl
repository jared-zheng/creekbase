// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_STREAM_FILE_INL__
#define __TARGET_STREAM_FILE_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

///////////////////////////////////////////////////////////////////
// CFileReadStream
INLINE CFileReadStream::CFileReadStream(void)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufBase(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileReadStream::~CFileReadStream(void)
{
    Close();
}

INLINE CFileReadStream::CFileReadStream(const CFileReadStream&)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufBase(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileReadStream& CFileReadStream::operator=(const CFileReadStream&)
{
    return (*this);
}

INLINE size_t CFileReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file read stream out buf invalid"));
        return 0;
    }
    if ((m_hFile.IsValid() == false) || IsEnd())
    {
        DEV_DEBUG(TF("file read stream invalid handle or EOF"));
        return 0;
    }
    if (m_stBufSize > 0)
    {
        if ((m_pBuf == nullptr) && (PreRead() == 0))
        {
            DEV_DEBUG(TF("file read stream read failed"));
            return 0;
        }
        assert(m_stBufSize >= m_stBufPos);
        size_t stRead = 0;
        size_t stSize = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stBufBase - m_stBufPos));
        if (stSize > (m_stBufSize - m_stBufPos)) // read size > buffer size
        {
            // 1. read data from buffer first
            assert(m_stPos >= (m_stBufBase + m_stBufPos));
            if (m_stPos > (m_stBufBase + m_stBufPos))
            {
                stRead = (m_stBufSize - m_stBufPos);
                MM_SAFE::Cpy(pV, (size_t)stRead, (m_pBuf + m_stBufPos), (size_t)stRead);

                m_stBufPos  = m_stBufSize;
                stSize     -= stRead;
            }

            // 2. read data from file directly(times of buffer size)
            size_t stDirect = (stSize / m_stBufSize) * m_stBufSize;
            if (stDirect > 0)
            {
                if (::ReadFile(m_hFile, ((PByte)pV + stRead), (ULong)stDirect, (PULong)&stDirect, nullptr) != FALSE)
                {
                    stSize      -= stDirect;
                    stRead      += stDirect;
                    m_stPos     += stDirect;
                    m_stBufBase  = m_stPos;
                    m_stBufPos   = 0;
                }
                else
                {
                    DEV_DEBUG(TF("file read failed1 : %d"), ::GetLastError());
                    SetError();
                    return stRead;
                }
            }
        }
        if (stSize > 0)// remain data copy to buffer, read data from buffer
        {
            if (m_stPos == (m_stBufBase + m_stBufPos)) // buffer is empty
            {
                stSize = DEF::Min<size_t>(stSize, PreRead());
            }
            if (stSize > 0) // read data size is within buffer size
            {
                MM_SAFE::Cpy(((PByte)pV + stRead), (size_t)stSize, (m_pBuf + m_stBufPos), (size_t)stSize);
                m_stBufPos += stSize;
                stRead     += stSize;
            }
        }
        return stRead;
    }
    else // m_stBufSize = 0
    {
        assert(m_stBufBase == 0);
        assert(m_stBufPos  == m_stPos);
        assert(m_pBuf == nullptr);
        stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
        if (::ReadFile(m_hFile, pV, (ULong)stLenBytes, (PULong)&stLenBytes, nullptr) != FALSE)
        {
            m_stPos    += stLenBytes;
            m_stBufPos += stLenBytes;
        }
        else
        {
            DEV_DEBUG(TF("file read failed2 : %d"), ::GetLastError());
            SetError();
            stLenBytes = 0;
        }
        return stLenBytes;
    }
}

INLINE size_t CFileReadStream::Tell(void) const
{
    return (m_stBufBase + m_stBufPos);
}

INLINE size_t CFileReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CFileReadStream::Rest(void) const
{
    return (Size() - Tell());
}

INLINE size_t CFileReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if ((eFrom == SEEKO_CURRENT) && (m_stBufSize > 0))
        {
            if (skPos > 0)
            {
                if ((size_t)skPos <= (m_stBufSize - m_stBufPos))
                {
                    m_stBufPos += (size_t)skPos;
                    return (m_stBufBase + m_stBufPos);
                }
                else
                {
                    skPos -= (SeekPos)(m_stBufSize - m_stBufPos);
                }
            }
            else if (skPos == 0)
            {
                return (m_stBufBase + m_stBufPos);
            }
            else
            {
                size_t stPos = (size_t)DEF::Abs<SeekPos>(skPos);
                if (stPos <= m_stBufPos)
                {
                    m_stBufPos -= stPos;
                    return (m_stBufBase + m_stBufPos);
                }
                else
                {
                    stPos  = m_stBufSize - m_stBufPos;
                    skPos -= (SeekPos)stPos;
                }
            }
        }
        LARGE_INTEGER liPos = { 0 };
        liPos.QuadPart = skPos;
        LARGE_INTEGER liNewPos = { 0 };

        if (::SetFilePointerEx(m_hFile, liPos, &liNewPos, eFrom) != FALSE)
        {
            m_stPos     = (size_t)liNewPos.QuadPart;
            m_stBufBase = m_stPos;
            m_stBufPos  = 0;
        }
    }
    return m_stPos;
}

INLINE void CFileReadStream::Close(void)
{
    m_hFile.Close();

    m_stSize    = 0;
    m_stPos     = 0;
    m_stBufSize = 0;
    m_stBufBase = 0;
    m_stBufPos  = 0;
    if (m_pBuf != nullptr)
    {
        FREE( m_pBuf );
        m_pBuf = nullptr;
    }
    CStream::Close();
}

INLINE UInt CFileReadStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    ULong ulAccess = GENERIC_READ;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, OPEN_EXISTING, uAttrs, nullptr);
    if (m_hFile != HANDLE_INVALID)
    {
        LARGE_INTEGER liSize = { 0 };
        if (::GetFileSizeEx(m_hFile, &liSize) != FALSE)
        {
            m_stSize = (size_t)liSize.QuadPart;
            if ((uFlag & FILEF_EXT_NOBUF) == 0)
            {
                m_stBufSize = DEF::Maxmin<size_t>((size_t)READ_BUF_MINSIZE, m_stSize, (size_t)READ_BUF_MAXSIZE);
                m_stBufSize = DEF::Align<size_t>(m_stBufSize, (size_t)READ_BUF_MINSIZE) - MEM_CHUNK_OFFSET;
            }
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    DEV_DEBUG(TF("file read create or open %s with acs = %x, share = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, uAttrs, uRet);
    return uRet;
}

INLINE const CFileHandle& CFileReadStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CFileReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

INLINE size_t CFileReadStream::PreRead(void)
{
    if ((IsError() == false) && (m_stBufSize > 0))
    {
        if (m_pBuf == nullptr)
        {
            m_pBuf = reinterpret_cast<PByte>( ALLOC( m_stBufSize ) );
        }
        if (m_pBuf != nullptr)
        {
            size_t stSize = DEF::Min<size_t>(m_stBufSize, (m_stSize - m_stPos));
            if (::ReadFile(m_hFile, m_pBuf, (ULong)stSize, (PULong)&stSize, nullptr) != FALSE)
            {
                m_stBufBase  = m_stPos;
                m_stBufPos   = 0;
                m_stPos     += stSize;
                return stSize;
            }
            DEV_DEBUG(TF("file read failed3 : %d"), ::GetLastError());
            SetError();
        }
    }
    return 0;
}

///////////////////////////////////////////////////////////////////
// CFileWriteStream
INLINE CFileWriteStream::CFileWriteStream(size_t stBuf)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_stPos(0)
, m_stBufSize(stBuf)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileWriteStream::~CFileWriteStream(void)
{
    Close();
}

INLINE CFileWriteStream::CFileWriteStream(const CFileWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileWriteStream& CFileWriteStream::operator=(const CFileWriteStream&)
{
    return (*this);
}

INLINE size_t CFileWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file write stream in buf invalid"));
        return 0;
    }
    if ((m_hFile.IsValid() == false) || IsError())
    {
        DEV_DEBUG(TF("file write stream invalid handle or error"));
        return 0;
    }
    if (m_pBuf != nullptr)
    {
        assert(m_stBufSize > 0);
        size_t stSize = m_stBufSize - m_stBufPos;
        if (stSize > stLenBytes)
        {
            MM_SAFE::Cpy((m_pBuf + m_stBufPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
            m_stBufPos += stLenBytes;
            stSize = stLenBytes;
        }
        else
        {
            ULong ulRet = 0;
            // write buffer data
            if (m_stBufPos > 0)
            {
                if (::WriteFile(m_hFile, m_pBuf, (ULong)m_stBufPos, &ulRet, nullptr) != FALSE)
                {
                    m_stPos    += m_stBufPos;
                    m_stBufPos  = 0;
                }
                else
                {
                    DEV_DEBUG(TF("file write failed0 : %d"), ::GetLastError());
                    stLenBytes = 0;
                }
            }
            //
            if ((stLenBytes > 0) && (::WriteFile(m_hFile, pV, (ULong)stLenBytes, &ulRet, nullptr) != FALSE))
            {
                m_stPos += stLenBytes;
                stSize   = stLenBytes;
            }
            else
            {
                DEV_DEBUG(TF("file write failed1 : %d"), ::GetLastError());
                SetError();
                stSize = 0;
            }
        }
        return stSize;
    }
    else
    {
        assert(m_stBufPos == 0);
        ULong ulRet = 0;
        if (::WriteFile(m_hFile, pV, (ULong)stLenBytes, &ulRet, nullptr) != FALSE)
        {
            m_stPos += stLenBytes;
            return stLenBytes;
        }
        else
        {
            DEV_DEBUG(TF("file write failed2 : %d"), ::GetLastError());
            SetError();
        }
    }
    return 0;
}

INLINE size_t CFileWriteStream::Tell(void) const
{
    return (m_stPos + m_stBufPos);
}

INLINE size_t CFileWriteStream::Size(void) const
{
    return (m_stPos + m_stBufPos);
}

INLINE size_t CFileWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    //assert((size_t)DEF::Abs<SeekPos>(skPos) <= Tell());
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_stBufPos > 0)
        {
            ULong ulRet = 0;
            if (::WriteFile(m_hFile, m_pBuf, (ULong)m_stBufPos, &ulRet, nullptr) == FALSE)
            {
                DEV_DEBUG(TF("file write seek failed : %d"), ::GetLastError());
                SetError();
                return 0;
            }
            m_stPos    += m_stBufPos;
            m_stBufPos  = 0;
        }
        LARGE_INTEGER liPos = { 0 };
        liPos.QuadPart = skPos;
        LARGE_INTEGER liNewPos = { 0 };

        if (::SetFilePointerEx(m_hFile, liPos, &liNewPos, eFrom) != FALSE)
        {
            m_stPos     = (size_t)liNewPos.QuadPart;
            m_stBufPos  = 0;
        }
    }
    return m_stPos;
}

INLINE bool CFileWriteStream::Flush(Int nFlag)
{
    if (m_hFile.IsValid())
    {
        if (m_stBufPos > 0)
        {
            ULong ulRet = 0;
            if (::WriteFile(m_hFile, m_pBuf, (ULong)m_stBufPos, &ulRet, nullptr) == FALSE)
            {
                DEV_DEBUG(TF("file write flush failed : %d"), ::GetLastError());
                SetError();
                return false;
            }
            m_stPos    += m_stBufPos;
            m_stBufPos  = 0;
        }
        if (nFlag != FLUSHO_BUF)
        {
            return (::FlushFileBuffers(m_hFile) != FALSE);
        }
        return true;
    }
    return false;
}

INLINE void CFileWriteStream::Close(void)
{
    Flush();
    m_hFile.Close();

    m_stPos     = 0;
    m_stBufSize = 0;
    m_stBufPos  = 0;
    if (m_pBuf != nullptr)
    {
        FREE( m_pBuf );
        m_pBuf = nullptr;
    }
    CStream::Close();
}

INLINE UInt CFileWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    ULong ulNewOrOpen = (uFlag & FILEF_NEW_OPEN_MASK);
    if (ulNewOrOpen == 0)
    {
        ulNewOrOpen = CREATE_ALWAYS;
    }
    ULong ulAccess = GENERIC_WRITE;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
    {
        uAttrs |= FILE_ATTRIBUTE_TEMPORARY;
        XChar szPath[LMT_MAX_PATH + LMT_MAX_PATH] = { 0 };

        PXStr pszTemp = (szPath + LMT_MAX_PATH);
        ULong ulRet   = ::GetTempPath(LMT_MAX_PATH, szPath);
        if ((ulRet > 0) && (ulRet < LMT_MAX_PATH))
        {
            if (::GetTempFileName(szPath, TF("CREEK"), 0, pszTemp) > 0)
            {
                m_hFile = ::CreateFile(pszTemp, ulAccess, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
            }
        }
    }
    else
    {
        m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
    }
    if (m_hFile != HANDLE_INVALID)
    {
        if (uFlag & FILEF_EXT_APPEND)
        {
            Seek(0, SEEKO_END);
        }
        if ((uFlag & FILEF_EXT_NOBUF) == 0)
        {
            m_stBufSize = DEF::Maxmin<size_t>((size_t)WRITE_BUF_MINSIZE, m_stBufSize, (size_t)WRITE_BUF_MAXSIZE);
            m_stBufSize = DEF::Align<size_t>(m_stBufSize, (size_t)WRITE_BUF_MINSIZE) - MEM_CHUNK_OFFSET;
            assert(m_pBuf == nullptr);
            m_pBuf = reinterpret_cast<PByte>( ALLOC( m_stBufSize ) );
            if (m_pBuf != nullptr)
            {
                return RET_OKAY;
            }
        }
        else
        {
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    if (uFlag & FILEF_EXT_TEMP)
    {
        DEV_DEBUG(TF("file write create or open temp with acs = %x, share = %x, flag = %x, attr = %x return : %d"), ulAccess, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    else
    {
        DEV_DEBUG(TF("file write create or open %s with acs = %x, share = %x, flag = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    return uRet;
}

INLINE const CFileHandle& CFileWriteStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CFileWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CAFileReadStream
INLINE CAFileReadStream::CAFileReadStream(void)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_pHandler(nullptr)
{
}

INLINE CAFileReadStream::~CAFileReadStream(void)
{
    Close();
}

INLINE CAFileReadStream::CAFileReadStream(const CAFileReadStream&)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_pHandler(nullptr)
{
}

INLINE CAFileReadStream& CAFileReadStream::operator=(const CAFileReadStream&)
{
    return (*this);
}

INLINE size_t CAFileReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file aio read stream out buf invalid"));
        return RET_FAIL;
    }
    if ((m_hFile.IsValid() == false) || IsEnd())
    {
        DEV_DEBUG(TF("file aio read stream invalid handle or EOF"));
        return RET_FAIL;
    }
    if (m_pHandler != nullptr)
    {
        if (::ReadFileEx(m_hFile, pV, (ULong)stLenBytes, &m_Attr, m_pHandler) == FALSE)
        {
            return (size_t)::GetLastError();
        }
    }
    else if (::ReadFile(m_hFile, pV, (ULong)stLenBytes, nullptr, &m_Attr) == FALSE)
    {
        // ERROR_IO_PENDING ERROR_HANDLE_EOF
        return (size_t)::GetLastError();
    }
    return RET_OKAY;
}

INLINE size_t CAFileReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CAFileReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        ULARGE_INTEGER uiPos;
        uiPos.LowPart  = m_Attr.Offset;
        uiPos.HighPart = m_Attr.OffsetHigh;
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    uiPos.QuadPart = DEF::Min<ULLong>((ULLong)skPos, (ULLong)m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((ULLong)skPos < uiPos.QuadPart)
                    {
                        uiPos.QuadPart -= (ULLong)skPos;
                    }
                    else
                    {
                        uiPos.QuadPart = 0;
                    }
                }
                else if ((ULLong)skPos <= ((ULLong)m_stSize - uiPos.QuadPart))
                {
                    uiPos.QuadPart += (ULLong)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        uiPos.QuadPart = (ULLong)m_stSize - (ULLong)skPos;
                    }
                }
            }
            break;
        default:{}
        }
        m_Attr.Offset     = uiPos.LowPart;
        m_Attr.OffsetHigh = uiPos.HighPart;
        return (size_t)(uiPos.QuadPart);
    }
    return 0;
}

INLINE void CAFileReadStream::Close(void)
{
    Cancel();
    m_hFile.Close();

    m_stSize   = 0;
    m_pHandler = nullptr;
    MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
    CStream::Close();
}

INLINE UInt CAFileReadStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs, AIOHANDLER pHandler, Handle hSigEvent)
{
    Close();

    ULong ulAccess = GENERIC_READ;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    uAttrs |= FILE_FLAG_OVERLAPPED;
    m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, OPEN_EXISTING, uAttrs, nullptr);
    if (m_hFile != HANDLE_INVALID)
    {
        LARGE_INTEGER liSize = { 0 };
        if (::GetFileSizeEx(m_hFile, &liSize) != FALSE)
        {
            m_stSize = (size_t)liSize.QuadPart;

            m_pHandler = pHandler;
            MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
            if (hSigEvent != INVALID_HANDLE_VALUE)
            {
                m_Attr.hEvent = hSigEvent;
            }
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    DEV_DEBUG(TF("file aio read create or open %s with acs = %x, share = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, uAttrs, uRet);
    return uRet;
}

INLINE bool CAFileReadStream::Wait(UInt uWait, bool bWaitCompleted)
{
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_pHandler != nullptr)
        {
            Int nRet = (Int)::WaitForSingleObjectEx(m_hFile, (ULong)uWait, TRUE);
            return ((nRet == WAIT_IO_COMPLETION) || (nRet == WAIT_OBJECT_0));
        }
        else
        {
            ULong ulTransferred = 0;
#if (NTDDI_VERSION >= NTDDI_WIN8)
            return (::GetOverlappedResultEx(m_hFile, &m_Attr, &ulTransferred, uWait, bWaitCompleted ? TRUE : FALSE) != FALSE);
#else
            return (::GetOverlappedResult(m_hFile, &m_Attr, &ulTransferred, bWaitCompleted ? TRUE : FALSE) != FALSE);
#endif
        }
    }
    return false;
}

INLINE Int CAFileReadStream::Result(size_t& stTransferred, bool bWaitCompleted, UInt uWait)
{
    stTransferred = 0;
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_pHandler != nullptr)
        {
            Int nRet = (Int)::WaitForSingleObjectEx(m_hFile, (ULong)uWait, TRUE);
            if ((nRet != WAIT_IO_COMPLETION) && (nRet != WAIT_OBJECT_0))
            {
                return nRet;
            }
            if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, FALSE) != FALSE)
            {
                return RET_OKAY;
            }
        }
        else
        {
#if (NTDDI_VERSION >= NTDDI_WIN8)
            if (::GetOverlappedResultEx(m_hFile, &m_Attr, (PULong)&stTransferred, uWait, bWaitCompleted ? TRUE : FALSE) == FALSE)
            {
                return (Int)::GetLastError();
            }
            return RET_OKAY;
#else
            if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, bWaitCompleted ? TRUE : FALSE) != FALSE)
            {
                return RET_OKAY;
            }
#endif
        }
    }
    return RET_FAIL;
}

INLINE Int CAFileReadStream::Cancel(void)
{
    size_t stTransferred = 0;
    if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, FALSE) == FALSE)
    {
        ULong ulRet = ::GetLastError();
        if ((ulRet == ERROR_IO_INCOMPLETE) || (ulRet == ERROR_IO_PENDING))
        {
#if (NTDDI_VERSION >= NTDDI_VISTA)
            return (::CancelIoEx(m_hFile, &m_Attr) != FALSE) ? RET_OKAY : RET_FAIL;
#else
            #pragma message("Windows XP No Implement")
            return (::CancelIo(m_hFile) != FALSE) ? RET_OKAY : RET_FAIL;
#endif
        }
    }
    return RET_OKAY;
}

INLINE const AIOATTR& CAFileReadStream::GetAttr(void) const
{
    return m_Attr;
}

INLINE const CFileHandle& CAFileReadStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CAFileReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CAFileWriteStream
INLINE CAFileWriteStream::CAFileWriteStream(void)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_pHandler(nullptr)
{
}

INLINE CAFileWriteStream::~CAFileWriteStream(void)
{
    Close();
}

INLINE CAFileWriteStream::CAFileWriteStream(const CAFileWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_pHandler(nullptr)
{
}

INLINE CAFileWriteStream& CAFileWriteStream::operator=(const CAFileWriteStream&)
{
    return (*this);
}

INLINE size_t CAFileWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file aio write stream in buf invalid"));
        return RET_FAIL;
    }
    if ((m_hFile.IsValid() == false) || IsError())
    {
        DEV_DEBUG(TF("file aio write stream invalid handle or error"));
        return RET_FAIL;
    }
    if (m_pHandler != nullptr)
    {
        if (::WriteFileEx(m_hFile, pV, (ULong)stLenBytes, &m_Attr, m_pHandler) == FALSE)
        {
            return (size_t)::GetLastError();
        }
    }
    else if (::WriteFile(m_hFile, pV, (ULong)stLenBytes, nullptr, &m_Attr) == FALSE)
    {
        // ERROR_IO_PENDING
        return (size_t)::GetLastError();
    }
    return RET_OKAY;
}

INLINE size_t CAFileWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        ULARGE_INTEGER uiPos;
        uiPos.LowPart  = m_Attr.Offset;
        uiPos.HighPart = m_Attr.OffsetHigh;
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    uiPos.QuadPart = DEF::Min<ULLong>((ULLong)skPos, uiPos.QuadPart);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos > 0)
                {
                    uiPos.QuadPart += (ULLong)skPos;
                }
                else if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((ULLong)skPos < uiPos.QuadPart)
                    {
                        uiPos.QuadPart -= (ULLong)skPos;
                    }
                    else
                    {
                        uiPos.QuadPart = 0;
                    }
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((ULLong)skPos <= uiPos.QuadPart)
                    {
                        uiPos.QuadPart -= (ULLong)skPos;
                    }
                }
            }
            break;
        default:{}
        }
        m_Attr.Offset     = uiPos.LowPart;
        m_Attr.OffsetHigh = uiPos.HighPart;
        return (size_t)(uiPos.QuadPart);
    }
    return 0;
}

INLINE bool CAFileWriteStream::Flush(Int nFlag)
{
    if (m_hFile.IsValid())
    {
        if (nFlag != FLUSHO_BUF)
        {
            return (::FlushFileBuffers(m_hFile) != FALSE);
        }
        return true;
    }
    return false;
}

INLINE void CAFileWriteStream::Close(void)
{
    Cancel();
    Flush();
    m_hFile.Close();

    m_pHandler = nullptr;
    MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));

    CStream::Close();
}

INLINE UInt CAFileWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs, AIOHANDLER pHandler, Handle hSigEvent)
{
    Close();

    ULong ulNewOrOpen = (uFlag & FILEF_NEW_OPEN_MASK);
    if (ulNewOrOpen == 0)
    {
        ulNewOrOpen = CREATE_ALWAYS;
    }

    ULong ulAccess = GENERIC_WRITE;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    uAttrs |= FILE_FLAG_OVERLAPPED;
    if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
    {
        uAttrs |= FILE_ATTRIBUTE_TEMPORARY;
        XChar szPath[LMT_MAX_PATH + LMT_MAX_PATH] = { 0 };

        PXStr pszTemp = (szPath + LMT_MAX_PATH);
        ULong ulRet   = ::GetTempPath(LMT_MAX_PATH, szPath);
        if ((ulRet > 0) && (ulRet < LMT_MAX_PATH))
        {
            if (::GetTempFileName(szPath, TF("CREEK"), 0, pszTemp) > 0)
            {
                m_hFile = ::CreateFile(pszTemp, GENERIC_WRITE, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
            }
        }
    }
    else
    {
        m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
    }
    if (m_hFile != HANDLE_INVALID)
    {
        if (uFlag & FILEF_EXT_APPEND)
        {
            Seek(0, SEEKO_END);
        }
        m_pHandler = pHandler;
        MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
        if (hSigEvent != INVALID_HANDLE_VALUE)
        {
            m_Attr.hEvent = hSigEvent;
        }
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    if (uFlag & FILEF_EXT_TEMP)
    {
        DEV_DEBUG(TF("file aio write create or open temp with acs = %x, share = %x, flag = %x, attr = %x return : %d"), GENERIC_WRITE, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    else
    {
        DEV_DEBUG(TF("file aio write create or open %s with acs = %x, share = %x, flag = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    return uRet;
}

INLINE bool CAFileWriteStream::Wait(UInt uWait, bool bWaitCompleted)
{
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_pHandler != nullptr)
        {
            Int nRet = (Int)::WaitForSingleObjectEx(m_hFile, (ULong)uWait, TRUE);
            return ((nRet == WAIT_IO_COMPLETION) || (nRet == WAIT_OBJECT_0));
        }
        else
        {
            ULong ulTransferred = 0;
#if (NTDDI_VERSION >= NTDDI_WIN8)
            return (::GetOverlappedResultEx(m_hFile, &m_Attr, &ulTransferred, uWait, bWaitCompleted ? TRUE : FALSE) != FALSE);
#else
            return (::GetOverlappedResult(m_hFile, &m_Attr, &ulTransferred, bWaitCompleted ? TRUE : FALSE) != FALSE);
#endif
        }
    }
    return false;
}

INLINE Int CAFileWriteStream::Result(size_t& stTransferred, bool bWaitCompleted, UInt uWait)
{
    stTransferred = 0;
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_pHandler != nullptr)
        {
            Int nRet = (Int)::WaitForSingleObjectEx(m_hFile, (ULong)uWait, TRUE);
            if ((nRet != WAIT_IO_COMPLETION) && (nRet != WAIT_OBJECT_0))
            {
                return nRet;
            }
            if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, FALSE) != FALSE)
            {
                return RET_OKAY;
            }
        }
        else
        {
#if (NTDDI_VERSION >= NTDDI_WIN8)
            if (::GetOverlappedResultEx(m_hFile, &m_Attr, (PULong)&stTransferred, uWait, bWaitCompleted ? TRUE : FALSE) == FALSE)
            {
                return (Int)::GetLastError();
            }
            return RET_OKAY;
#else
            if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, bWaitCompleted ? TRUE : FALSE) != FALSE)
            {
                return RET_OKAY;
            }
#endif
        }
    }
    return RET_FAIL;
}

INLINE Int CAFileWriteStream::Cancel(void)
{
    size_t stTransferred = 0;
    if (::GetOverlappedResult(m_hFile, &m_Attr, (PULong)&stTransferred, FALSE) == FALSE)
    {
        ULong ulRet = ::GetLastError();
        if ((ulRet == ERROR_IO_INCOMPLETE) || (ulRet == ERROR_IO_PENDING))
        {
#if (NTDDI_VERSION >= NTDDI_VISTA)
            return (::CancelIoEx(m_hFile, &m_Attr) != FALSE) ? RET_OKAY : RET_FAIL;
#else
            #pragma message("Windows XP No Implement")
            return (::CancelIo(m_hFile) != FALSE) ? RET_OKAY : RET_FAIL;
#endif
        }
    }
    return RET_OKAY;
}

INLINE const AIOATTR& CAFileWriteStream::GetAttr(void) const
{
    return m_Attr;
}

INLINE const CFileHandle& CAFileWriteStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CAFileWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CMapReadStream
INLINE CMapReadStream::CMapReadStream(Int nMode)
: CStream(STREAMM_READ | (nMode & STREAMM_FILE) | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapReadStream::~CMapReadStream(void)
{
    Close();
}

INLINE CMapReadStream::CMapReadStream(const CMapReadStream&)
: CStream(STREAMM_READ | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapReadStream& CMapReadStream::operator=(const CMapReadStream&)
{
    return (*this);
}

INLINE size_t CMapReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("map read stream out buf invalid"));
        return 0;
    }
    if ((m_hMapping.IsValid() == false) || IsEnd())
    {
        DEV_DEBUG(TF("map read stream invalid handle or EOF"));
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy(pV, (size_t)stLenBytes, (m_pBuf + m_stPos), (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE size_t CMapReadStream::Tell(void) const
{
    return m_stPos;
}

INLINE size_t CMapReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CMapReadStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CMapReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE void CMapReadStream::Close(void)
{
    ExitMap();
    m_hFile.Close();
    CStream::Close();
}

INLINE UInt CMapReadStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    if ((GetMode() & STREAMM_FILE) == 0)
    {
        return RET_OKAY;
    }

    ULong ulAccess = GENERIC_READ;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, OPEN_EXISTING, uAttrs, nullptr);
    if (m_hFile.IsValid())
    {
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    DEV_DEBUG(TF("map read stream create or open %s with acs = %x, share = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, uAttrs, uRet);
    return uRet;
}

INLINE PByte CMapReadStream::InitMap(size_t stSize, size_t stOffset, UInt uProtect, UInt uAttrs, PCXStr pszName)
{
    ExitMap();
    if (m_pBuf == nullptr)
    {
        ULARGE_INTEGER uliSize = { 0 };
        uliSize.QuadPart = stSize;

        if (uProtect == 0)
        {
            uProtect = PAGE_READONLY;
        }
        if ((GetMode() & STREAMM_FILE) && m_hFile.IsValid())
        {
            if (stSize == 0)
            {
                ::GetFileSizeEx(m_hFile, (PLARGE_INTEGER)&uliSize);
            }
            m_hMapping = ::CreateFileMapping(m_hFile, nullptr, uProtect, uliSize.HighPart, uliSize.LowPart, pszName);
        }
        else if ((GetMode() & STREAMM_FILE) == 0)
        {
            m_hMapping = ::CreateFileMapping(HANDLE_INVALID, nullptr, uProtect, uliSize.HighPart, uliSize.LowPart, pszName);
        }
        if (m_hMapping.IsValid())
        {
            ULARGE_INTEGER uliOffset = { 0 };
            uliOffset.QuadPart = stOffset;

            if (uAttrs == 0)
            {
                uAttrs = FILE_MAP_READ;
            }
            m_pBuf = reinterpret_cast<PByte>(::MapViewOfFile(m_hMapping, uAttrs, uliOffset.HighPart, uliOffset.LowPart, (SIZE_T)stSize));
            if (m_pBuf != nullptr)
            {
                m_stSize = stSize;
            }
            else
            {
                SetError();
            }
        }
    }
    return (m_pBuf);
}

INLINE void CMapReadStream::ExitMap(void)
{
    if (m_pBuf != nullptr)
    {
        ::UnmapViewOfFile(m_pBuf);
        m_pBuf = nullptr;
    }
    m_hMapping.Close();
    m_stSize = 0;
    m_stPos  = 0;
}

INLINE const PByte CMapReadStream::GetMap(void) const
{
    return (m_pBuf);
}

INLINE bool CMapReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CMapWriteStream
INLINE CMapWriteStream::CMapWriteStream(Int nMode)
: CStream(STREAMM_WRITE | (nMode & STREAMM_FILE) | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapWriteStream::~CMapWriteStream(void)
{
    Close();
}

INLINE CMapWriteStream::CMapWriteStream(const CMapWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapWriteStream& CMapWriteStream::operator=(const CMapWriteStream&)
{
    return (*this);
}

INLINE size_t CMapWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("map write stream out buf invalid"));
        return 0;
    }
    if ((m_hMapping.IsValid() == false) || IsError())
    {
        DEV_DEBUG(TF("map write stream invalid handle or error"));
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy((m_pBuf + m_stPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE size_t CMapWriteStream::Tell(void) const
{
    return m_stPos;
}

INLINE size_t CMapWriteStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CMapWriteStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CMapWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE bool CMapWriteStream::Flush(Int nFlag)
{
    if (m_pBuf != nullptr)
    {
        if (nFlag != FLUSHO_BUF)
        {
            return (::FlushViewOfFile(m_pBuf, m_stPos) != FALSE);
        }
        return true;
    }
    return false;
}

INLINE void CMapWriteStream::Close(void)
{
    ExitMap();
    m_hFile.Close();
    CStream::Close();
}

INLINE UInt CMapWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    if ((GetMode() & STREAMM_FILE) == 0)
    {
        return RET_OKAY;
    }

    ULong ulNewOrOpen = (uFlag & FILEF_NEW_OPEN_MASK);
    if (ulNewOrOpen == 0)
    {
        ulNewOrOpen = CREATE_ALWAYS;
    }
    ULong ulAccess = GENERIC_WRITE;
    ulAccess |= (uFlag & FILEF_ACS_MASK);

    ULong ulShare = (uFlag & FILEF_SHARE_UALL);
    ulShare >>= FILEF_SHARE_UMASK;

    if (uAttrs & FILE_FLAG_DELETE_ON_CLOSE)
    {
        ulShare |= FILE_SHARE_DELETE;
    }
    if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
    {
        uAttrs |= FILE_ATTRIBUTE_TEMPORARY;
        XChar szPath[LMT_MAX_PATH + LMT_MAX_PATH] = { 0 };
        PXStr pszTemp = (szPath + LMT_MAX_PATH);
        ULong ulRet   = ::GetTempPath(LMT_MAX_PATH, szPath);
        if ((ulRet > 0) && (ulRet < LMT_MAX_PATH))
        {
            if (::GetTempFileName(szPath, TF("CREEK"), 0, pszTemp) > 0)
            {
                m_hFile = ::CreateFile(pszTemp, ulAccess, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
            }
        }
    }
    else
    {
        m_hFile = ::CreateFile(pszFile, ulAccess, ulShare, nullptr, ulNewOrOpen, uAttrs, nullptr);
    }
    if (m_hFile.IsValid())
    {
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)::GetLastError();
    if ((uFlag & FILEF_EXT_TEMP) != 0)
    {
        DEV_DEBUG(TF("map write stream create or open temp with acs = %x, share = %x, flag = %x, attr = %x return : %d"), ulAccess, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    else
    {
        DEV_DEBUG(TF("map write stream create or open %s with acs = %x, share = %x, flag = %x, attr = %x return : %d"), pszFile, ulAccess, ulShare, ulNewOrOpen, uAttrs, uRet);
    }
    return uRet;
}

INLINE PByte CMapWriteStream::InitMap(size_t stSize, size_t stOffset, UInt uProtect, UInt uAttrs, PCXStr pszName)
{
    ExitMap();
    if (m_pBuf == nullptr)
    {
        ULARGE_INTEGER uliSize = { 0 };
        uliSize.QuadPart = stSize;

        if (uProtect == 0)
        {
            uProtect = PAGE_READWRITE;
        }
        if ((GetMode() & STREAMM_FILE) && m_hFile.IsValid())
        {
            if (stSize == 0)
            {
                ::GetFileSizeEx(m_hFile, (PLARGE_INTEGER)&uliSize);
            }
            m_hMapping = ::CreateFileMapping(m_hFile, nullptr, uProtect, uliSize.HighPart, uliSize.LowPart, pszName);
        }
        else if ((GetMode() & STREAMM_FILE) == 0)
        {
            m_hMapping = ::CreateFileMapping(HANDLE_INVALID, nullptr, uProtect, uliSize.HighPart, uliSize.LowPart, pszName);
        }
        if (m_hMapping.IsValid())
        {
            ULARGE_INTEGER uliOffset = { 0 };
            uliOffset.QuadPart = stOffset;

            if (uAttrs == 0)
            {
                uAttrs = FILE_MAP_WRITE;
            }
            m_pBuf = reinterpret_cast<PByte>(::MapViewOfFile(m_hMapping, uAttrs, uliOffset.HighPart, uliOffset.LowPart, (SIZE_T)stSize));
            if (m_pBuf != nullptr)
            {
                m_stSize = stSize;
            }
            else
            {
                SetError();
            }
        }
    }
    return (m_pBuf);
}

INLINE void CMapWriteStream::ExitMap(void)
{
    if (m_pBuf != nullptr)
    {
        ::UnmapViewOfFile(m_pBuf);
        m_pBuf = nullptr;
    }
    m_hMapping.Close();
    m_stSize = 0;
    m_stPos  = 0;
}

INLINE const PByte CMapWriteStream::GetMap(void) const
{
    return (m_pBuf);
}

INLINE bool CMapWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_STREAM_FILE_INL__
