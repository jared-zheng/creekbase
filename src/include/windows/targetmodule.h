// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_MODULE_H__
#define __TARGET_MODULE_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
#ifdef __RUNTIME_STATIC__
    #define CORE_MODULE_NAME          CoreStatic
    #define NETWORK_MODULE_NAME       NetworkStatic
    #define XUI_MODULE_NAME           XUIStatic
#else  // __RUNTIME_STATIC__
    #ifdef __RUNTIME_DEBUG__
        #define CORE_MODULE_NAME      TF("coreDebug.dll")
        #define NETWORK_MODULE_NAME   TF("networkDebug.dll")
        #define XUI_MODULE_NAME       TF("xuiDebug.dll")
    #else  // __RUNTIME_DEBUG__
        #define CORE_MODULE_NAME      TF("core.dll")
        #define NETWORK_MODULE_NAME   TF("network.dll")
        #define XUI_MODULE_NAME       TF("xui.dll")
    #endif // __RUNTIME_DEBUG__
#endif // __RUNTIME_STATIC__

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)

#endif // __TARGET_MODULE_H__
