// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CONFIG_H__
#define __CONFIG_H__

#pragma once

///////////////////////////////////////////////////////////////////
// defines description
// __ARCH_TARGET__
#define ARCH_TARGET_32                0x0000000000000001ULL
#define ARCH_TARGET_64                0x0000000000000002ULL
#define ARCH_TARGET_BIGENDIAN         0x0000000000000004ULL
// __COMPILER_TYPE__
#define COMPILER_TYPE_GCC             0x0000000000010000ULL
#define COMPILER_TYPE_CLANG           0x0000000000020000ULL
#define COMPILER_TYPE_MSVC            0x0000000000040000ULL
// __PLATFORM_TARGET__
#define PLATFORM_TARGET_WINDOWS       0x0000000100000000ULL
#define PLATFORM_TARGET_LINUX         0x0000000200000000ULL
#define PLATFORM_TARGET_MAC           0x0000000400000000ULL
#define PLATFORM_TARGET_IOS           0x0000000800000000ULL
#define PLATFORM_TARGET_ANDROID       0x0000001000000000ULL
// __RUNTIME_CONFIG__
#define RUNTIME_CONFIG_UTF8           0x0001000000000000ULL
#define RUNTIME_CONFIG_UTF16          0x0002000000000000ULL
#define RUNTIME_CONFIG_UTF32          0x0004000000000000ULL
#define RUNTIME_CONFIG_MBCS           0x0008000000000000ULL
#define RUNTIME_CONFIG_UNKNOWN        0x0010000000000000ULL
#define RUNTIME_CONFIG_DEBUG          0x0020000000000000ULL
#define RUNTIME_CONFIG_STATIC         0x0040000000000000ULL

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#
//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CONFIG_H__