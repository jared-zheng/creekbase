// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __EXCEPTION_H__
#define __EXCEPTION_H__

#pragma once

#include "filelog.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CException
class CException : public MObject
{
public:
    CException(UInt uCode = 0);
    CException(UInt uCode, PCXStr pszInfo);
    CException(UInt uCode, const CString& strInfo);

    virtual ~CException(void);

    CException(const CException& aSrc);
    CException& operator=(const CException& aSrc);

    UInt  GetCode(void) const;
    void  SetCode(UInt uCode = 0);

    const CString& GetInfo(void) const;
    void  SetInfo(PCXStr pszInfo);

    virtual void Serialize(CStream& Stream);

protected:
    UInt     m_uCode;
    CString  m_strInfo;
};

///////////////////////////////////////////////////////////////////
#include "exception.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetexception.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetexception.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __EXCEPTION_H__
