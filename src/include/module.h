// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MODULE_H__
#define __MODULE_H__

#pragma once

#include "handle.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CModule
class CModule : public MObject
{
public:
    CModule(Module m = nullptr, bool bUnload = false);
    ~CModule(void);

    CModule(const CModule& aSrc);
    CModule& operator=(const CModule& aSrc);
    CModule& operator=(Module m); // m_bUnload = true

    bool    operator==(Module m) const;
    bool    operator!=(Module m) const;
    bool    operator==(const CModule& aSrc) const;
    bool    operator!=(const CModule& aSrc) const;

    operator Module(void) const;

    template <typename I> I Find(PCStr pszSymbol);

    bool   Attach(PCXStr pszPath, bool bUnload = true);
    void   Attach(Module m, bool bUnload = true);
    Module Detach(void);
    void   Close(void);
public:
    bool            m_bUnload;
    CModuleHandle   m_hModule;
};

///////////////////////////////////////////////////////////////////
#include "module.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __MODULE_H__