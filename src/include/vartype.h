// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __VAR_TYPE_H__
#define __VAR_TYPE_H__

#pragma once

#include <typeinfo>
#include "streambuf.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CVarType
class CVarType : public MObject
{
public:
    // default type
    CVarType(void);
    CVarType(Short sValue);
    CVarType(UShort usValue);
    CVarType(Int nValue);
    CVarType(UInt uValue);
    CVarType(Long lValue);
    CVarType(ULong ulValue);
    CVarType(LLong llValue);
    CVarType(ULLong ullValue);
    CVarType(Double dValue);
    CVarType(bool bValue);
    template <typename T>
    CVarType(T* pValue); // VART_POINTER + const type_info*
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    CVarType(const CCString& strValue);
    CVarType(CCString& strValue, bool bAttach = false);
    CVarType(PCStr pszValue, size_t stSize = 0);
#else
    CVarType(const CWString& strValue);
    CVarType(CWString& strValue, bool bAttach = false);
    CVarType(PCWStr pszValue, size_t stSize = 0);
#endif
    CVarType(const CString& strValue);
    CVarType(CString& strValue, bool bAttach = false);
    CVarType(PCXStr pszValue, size_t stSize = 0);
    CVarType(const CBufStream& bsValue);
    CVarType(CBufStream& bsValue, bool bAttach = false);
    CVarType(PByte pbValue, size_t stSize, bool bAttach = false);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T>
    CVarType(const T& t);
    CVarType(const CVarType& aSrc);

    // default type 
    CVarType& operator=(Short sValue);
    CVarType& operator=(UShort usValue);
    CVarType& operator=(Int nValue);
    CVarType& operator=(UInt uValue);
    CVarType& operator=(Long lValue);
    CVarType& operator=(ULong ulValue);
    CVarType& operator=(LLong llValue);
    CVarType& operator=(ULLong ullValue);
    CVarType& operator=(Double dValue);
    CVarType& operator=(bool bValue);
    template <typename T>
    CVarType& operator=(T* pValue); // VART_POINTER + const type_info*
    // mem-cache type assign-ctr == copy ctr
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    CVarType& operator=(const CCString& strValue);
    CVarType& operator=(PCStr pszValue);
#else
    CVarType& operator=(const CWString& strValue);
    CVarType& operator=(PCWStr pszValue);
#endif
    CVarType& operator=(const CString& strValue);
    CVarType& operator=(PCXStr pszValue);
    CVarType& operator=(const CBufStream& bsValue);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T>
    CVarType& operator=(const T& t);
    CVarType& operator=(const CVarType& aSrc);

#ifndef __MODERN_CXX_NOT_SUPPORTED
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    CVarType(CCString&& strValue);
#else
    CVarType(CWString&& strValue);
#endif
    CVarType(CString&& strValue);
    CVarType(CBufStream&& bsValue);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T, typename X = typename std::enable_if<!std::is_same<typename std::decay<T>::type, CVarType>::value, T>::type>
    CVarType(T&& t);
    CVarType(CVarType&& aSrc);
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    CVarType& operator=(CCString&& strValue);
#else
    CVarType& operator=(CWString&& strValue);
#endif
    CVarType& operator=(CString&& strValue);
    CVarType& operator=(CBufStream&& bsValue);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T, typename X = typename std::enable_if<!std::is_same<typename std::decay<T>::type, CVarType>::value, T>::type>
    CVarType& operator=(T&& t);
    CVarType& operator=(CVarType&& aSrc);
#endif
    ~CVarType(void);

    VAR_TYPE GetType(void) const;
    size_t   GetSize(void) const;
    // VART_NONE
    bool     IsNone(void) const;
    // default type
    bool     IsType(VAR_TYPE eType) const;
    // VART_POINTER + const type_info*
    template <typename T>
    bool     IsPtrType(void) const;
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
    bool     IsCacheType(VAR_TYPE eType) const;
    // VART_ANYTYPE + const type_info*
    template <typename T>
    bool     IsAnyType(void) const;

    // default type
    Short    GetShort(void) const;
    UShort   GetUShort(void) const;
    Int      GetInt(void) const;
    UInt     GetUInt(void) const;
    Long     GetLong(void) const;
    ULong    GetULong(void) const;
    LLong    GetLLong(void) const;
    ULLong   GetULLong(void) const;
    Double   GetDouble(void) const;
    bool     GetBoolean(void) const;
    // VART_POINTER + const type_info*
    template <typename T>
    T*       GetPtr(void) const;
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR
    CCString GetCString(bool bAttach = false);
    CWString GetWString(bool bAttach = false);
    CString  GetXString(bool bAttach = false);
    // VART_ANYTYPE + const type_info*
    template <typename T>
    const T& AnyCast(void) const;
    template <typename T>
    T&       AnyCast(void);
    template <typename T>
    T&       NewCast(void);
    template <typename T>
    T&       ResetCast(void);

    // default type
    bool     GetValue(Short& sValue) const;
    bool     GetValue(UShort& usValue) const;
    bool     GetValue(Int& nValue) const;
    bool     GetValue(UInt& uValue) const;
    bool     GetValue(Long& lValue) const;
    bool     GetValue(ULong& ulValue) const;
    bool     GetValue(LLong& llValue) const;
    bool     GetValue(ULLong& ullValue) const;
    bool     GetValue(Double& dValue) const;
    bool     GetValue(bool& bValue) const;
    // VART_POINTER + const type_info*
    template <typename T>
    bool     GetValue(T*& pValue) const;
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    bool     GetValue(CCString& strValue, bool bAttach = false);
#else
    bool     GetValue(CWString& strValue, bool bAttach = false);
#endif
    bool     GetValue(CString& strValue, bool bAttach = false);
    bool     GetValue(CBufStream& bsValue, bool bAttach = false);
    // VART_ANYTYPE + const type_info*
    template <typename T>
    const T* TryCast(void) const;
    template <typename T>
    T*       TryCast(void);
    template <typename T>
    T*       TryNew(void);
    template <typename T>
    T*       TryReset(void);
    
    // default type 
    bool     SetValue(Short sValue);
    bool     SetValue(UShort usValue);
    bool     SetValue(Int nValue);
    bool     SetValue(UInt uValue);
    bool     SetValue(Long lValue);
    bool     SetValue(ULong ulValue);
    bool     SetValue(LLong llValue);
    bool     SetValue(ULLong ullValue);
    bool     SetValue(Double dValue);
    bool     SetValue(bool bValue);
    template <typename T>
    bool     SetValue(T* pValue); // VART_POINTER + const type_info*
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    bool     SetValue(const CCString& strValue);
    bool     SetValue(CCString& strValue, bool bAttach = false);
#else
    bool     SetValue(const CWString& strValue);
    bool     SetValue(CWString& strValue, bool bAttach = false);
#endif
    bool     SetValue(const CString& strValue);
    bool     SetValue(CString& strValue, bool bAttach = false);

    bool     SetCString(PCStr  pszValue, size_t stSize = 0);
    bool     SetWString(PCWStr pszValue, size_t stSize = 0);
    bool     SetXString(PCXStr pszValue, size_t stSize = 0);

    bool     SetValue(const CBufStream& bsValue);
    bool     SetValue(CBufStream& bsValue, bool bAttach = false);
    bool     SetValue(PByte pbValue, size_t stSize, bool bAttach = false);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T>
    bool     SetValue(void);
    template <typename T>
    bool     SetValue(const T& t);
    bool     SetValue(const CVarType& aSrc);

#ifndef __MODERN_CXX_NOT_SUPPORTED
    // mem-cache type
    // VART_MEMCACHE + VART_CHAR/VART_WCHAR/VART_XCHAR/VART_UCHAR
#ifdef __RUNTIME_CHARSET_WCHAR__
    bool     SetValue(CCString&& strValue);
#else
    bool     SetValue(CWString&& strValue);
#endif
    bool     SetValue(CString&& strValue);
    bool     SetValue(CBufStream&& bsValue);
    // other any type
    // VART_ANYTYPE + const type_info*
    template <typename T, typename X = typename std::enable_if<!std::is_same<typename std::decay<T>::type, CVarType>::value, T>::type>
    bool     SetValue(T&& t);
    bool     SetValue(CVarType&& aSrc);
#endif

    void     Reset(bool bFree = true);
private:
    bool     GetCString(CCString& strValue, bool bAttach = false);
    bool     GetWString(CWString& strValue, bool bAttach = false);
    bool     GetXString(CString&  strValue, bool bAttach = false);

    void     Attach(PByte pData, size_t stData);
    bool     Alloc(size_t stNew, bool bAlign = true);

    void     Free(void);
private:
    class NOVTABLE CAny ABSTRACT : public MObject
    {
    public:
        CAny(void) { }
        virtual ~CAny(void) { }
        virtual bool Clone(CAny*& pAny) const PURE;
    };

    template <typename X>
    class CTAny : public CAny
    {
    public:
        CTAny(void) : m_Any() { }
#ifndef __MODERN_CXX_NOT_SUPPORTED
        template <typename T>
        CTAny(T&& t) : m_Any(std::forward<T>(t)) { }
#else
        CTAny(const X& x) : m_Any(x) { }
#endif
        virtual bool Clone(CAny*& pAny) const OVERRIDE
        {
            pAny = MNEW CTAny<X>(m_Any);
            return (pAny != nullptr);
        }
    public:
        X   m_Any;
    };
    
    typedef union tagVALUE
    {
        void*    pValue;
        CAny*    ptValue;
        PByte    pbValue;
        LLong    llValue;
        ULLong   ullValue;
        Double   dValue;
        bool     bValue;
    }VALUE;

    typedef const std::type_info CTYPE_INFO, *PCTYPE_INFO;

    bool     Clone(CAny*& pAny) const;
private:
    PCTYPE_INFO   m_pCType;
    size_t        m_stType;  // VAR_TYPE
    size_t        m_stCache; // string or buffer size
    size_t        m_stSize;  // string or buffer size
    VALUE         m_xValue;
};

///////////////////////////////////////////////////////////////////
#include "vartype.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __VAR_TYPE_H__
