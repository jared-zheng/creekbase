// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TIME_SCOPE_H__
#define __TIME_SCOPE_H__

#pragma once

#include "stream.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTimeScope
class CTimeScope : public MObject
{
public:
    CTimeScope(void);
    CTimeScope(LLong llTime);
    CTimeScope(Int nDays, Int nHours, Int nMinutes, Int nSeconds);
    ~CTimeScope(void);

    CTimeScope(const CTimeScope& aSrc);
    CTimeScope& operator=(const CTimeScope& aSrc);

    CTimeScope& operator+=(const CTimeScope& aSrc);
    CTimeScope& operator-=(const CTimeScope& aSrc);
    CTimeScope  operator+(const CTimeScope& aSrc) const;
    CTimeScope  operator-(const CTimeScope& aSrc) const;

    bool        operator<=(const CTimeScope& aSrc) const;
    bool        operator<(const CTimeScope& aSrc) const;
    bool        operator>=(const CTimeScope& aSrc) const;
    bool        operator>(const CTimeScope& aSrc) const;
    bool        operator==(const CTimeScope& aSrc) const;
    bool        operator!=(const CTimeScope& aSrc) const;

    LLong       GetScope(void) const;
    LLong       GetDays(void) const;

    LLong       GetTotalHours(void) const;
    LLong       GetTotalMinutes(void) const;
    LLong       GetTotalSeconds(void) const;

    Int         GetHours(void) const;
    Int         GetMinutes(void) const;
    Int         GetSeconds(void) const;

    void        Serialize(CStream&);
    CString     Format(PCXStr pszFormat = nullptr) const;
private:
    LLong       m_llTimeScope;
};

///////////////////////////////////////////////////////////////////
// CTime
class CTime : public MObject
{
public:
    CTime(void);
    CTime(LLong llTime);
    CTime(ULLong ullFileTime, Int nDST = -1);
    CTime(Int nYear, Int nMonth, Int nDay, Int nHour, Int nMinute, Int nSecond, Int nDST = -1);
    CTime(const CPlatform::TIMEINFO& ti, Int nDST = -1);
    ~CTime(void);

    CTime(const CTime& aSrc);
    CTime& operator=(const CTime& aSrc);

    CTime&     operator+=(const CTimeScope& scope);
    CTime&     operator-=(const CTimeScope& scope);
    CTime      operator+(const CTimeScope& scope) const;
    CTime      operator-(const CTimeScope& scope) const;
    CTimeScope operator-(const CTime& aSrc) const;

    bool       operator<=(const CTime& aSrc) const;
    bool       operator<(const CTime& aSrc) const;
    bool       operator>=(const CTime& aSrc) const;
    bool       operator>(const CTime& aSrc) const;
    bool       operator==(const CTime& aSrc) const;
    bool       operator!=(const CTime& aSrc) const;

    struct tm* GetTm(struct tm& TmRef, bool bLocal = true) const;
    LLong      GetTime(void) const;

    Int        GetYear(void) const;
    Int        GetMonth(void) const;
    Int        GetDay(void) const;
    Int        GetHour(void) const;
    Int        GetMinute(void) const;
    Int        GetSecond(void) const;
    Int        GetDayOfWeek(void) const;

    void       Serialize(CStream&);
    CString    Format(PCXStr pszFormat) const;
private:
    LLong      m_llTime;
};

///////////////////////////////////////////////////////////////////
#include "timescope.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targettimescope.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targettimescope.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __TIME_SCOPE_H__