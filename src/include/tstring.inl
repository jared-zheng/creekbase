// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TSTRING_INL__
#define __TSTRING_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CTStringRef
template <typename T>
INLINE CTStringRef<T>::CTStringRef(PCTStr psz)
: m_stLen(0)
, m_pszRef(nullptr)
{
    if (psz != nullptr)
    {
        m_stLen = T::Length(psz);
        if (m_stLen > 0)
        {
            m_pszRef = psz;
        }
    }
}

template <typename T>
INLINE CTStringRef<T>::CTStringRef(PCTStr psz, size_t stLen)
: m_stLen(0)
, m_pszRef(nullptr)
{
    if (psz != nullptr)
    {
        m_stLen = stLen;
        if (m_stLen > 0)
        {
            m_pszRef = psz;
        }
    }
}

template <typename T>
INLINE CTStringRef<T>::~CTStringRef(void)
{
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T>
INLINE CTStringRef<T>::CTStringRef(CTStringRef<T>&& aSrc)
: m_stLen(aSrc.m_stLen)
, m_pszRef(aSrc.m_pszRef)
{
    aSrc.m_stLen  = 0;
    aSrc.m_pszRef = nullptr;
}

template <typename T>
INLINE CTStringRef<T>& CTStringRef<T>::operator=(CTStringRef<T>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        m_stLen       = aSrc.m_stLen;
        m_pszRef      = aSrc.m_pszRef;
        aSrc.m_stLen  = 0;
        aSrc.m_pszRef = nullptr;
    }
    return (*this);
}
#endif

template <typename T>
INLINE CTStringRef<T>::CTStringRef(const CTStringRef<T>& aSrc)
: m_stLen(aSrc.m_stLen)
, m_pszRef(aSrc.m_pszRef)
{
}

template <typename T>
template <size_t stLenT>
INLINE CTStringRef<T>::CTStringRef(const CTStringFix<T, stLenT>& aSrc)
: m_stLen(aSrc.Length())
, m_pszRef(*aSrc)
{
}

template <typename T>
INLINE CTStringRef<T>::CTStringRef(const CTString<T>& aSrc)
: m_stLen(aSrc.Length())
, m_pszRef(*aSrc)
{
}

template <typename T>
INLINE CTStringRef<T>& CTStringRef<T>::operator=(PCTStr psz)
{
    if (psz != nullptr)
    {
        m_stLen = T::Length(psz);
        if (m_stLen > 0)
        {
            m_pszRef = psz;
        }
        else
        {
            m_pszRef = nullptr;
        }
    }
    else
    {
        m_stLen  = 0;
        m_pszRef = nullptr;
    }
    return (*this);
}

template <typename T>
INLINE CTStringRef<T>& CTStringRef<T>::operator=(const CTStringRef<T>& aSrc)
{
    if (&aSrc != this)
    {
        m_stLen  = aSrc.m_stLen;
        m_pszRef = aSrc.m_pszRef;
    }
    return (*this);
}

template <typename T>
template <size_t stLenT>
INLINE CTStringRef<T>& CTStringRef<T>::operator=(const CTStringFix<T, stLenT>& aSrc)
{
    SetBufferLength(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
INLINE CTStringRef<T>& CTStringRef<T>::operator=(const CTString<T>& aSrc)
{
    SetBufferLength(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
INLINE bool CTStringRef<T>::operator<=(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) <= 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator<(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) < 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>=(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) >= 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) > 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator==(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) == 0);
        }
    }
    return (m_pszRef == psz);
}

template <typename T>
INLINE bool CTStringRef<T>::operator!=(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, psz, stLen) != 0);
        }
        return true;
    }
    return (m_pszRef != psz);
}

template <typename T>
INLINE bool CTStringRef<T>::operator<=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) <= 0);
        }
        else if (m_stLen < aSrc.m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator<(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) < 0);
        }
        else if (m_stLen < aSrc.m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) >= 0);
        }
        else if (m_stLen > aSrc.m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) > 0);
        }
        else if (m_stLen > aSrc.m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator==(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) == 0);
        }
        return false;
    }
    return (m_pszRef == aSrc.m_pszRef);
}

template <typename T>
INLINE bool CTStringRef<T>::operator!=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszRef != nullptr) && (aSrc.m_pszRef != nullptr))
    {
        if (m_stLen == aSrc.m_stLen)
        {
            return (T::Cmpn(m_pszRef, aSrc.m_pszRef, m_stLen) != 0);
        }
        return true;
    }
    return (m_pszRef != aSrc.m_pszRef);
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator<=(const CTStringFix<T, stLenT>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) <= 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator<(const CTStringFix<T, stLenT>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) < 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator>=(const CTStringFix<T, stLenT>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) >= 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator>(const CTStringFix<T, stLenT>&aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) > 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator==(const CTStringFix<T, stLenT>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen == m_stLen))
    {
        return (T::Cmpn(m_pszRef, *aSrc, stLen) == 0);
    }
    return (m_pszRef == *aSrc);
}

template <typename T>
template <size_t stLenT>
INLINE bool CTStringRef<T>::operator!=(const CTStringFix<T, stLenT>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen == m_stLen))
    {
        return (T::Cmpn(m_pszRef, *aSrc, stLen) != 0);
    }
    return (m_pszRef != *aSrc);
}

template <typename T>
INLINE bool CTStringRef<T>::operator<=(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) <= 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator<(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) < 0);
        }
        else if (stLen > m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>=(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) >= 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator>(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen > 0))
    {
        if (stLen == m_stLen)
        {
            return (T::Cmpn(m_pszRef, *aSrc, stLen) > 0);
        }
        else if (stLen < m_stLen)
        {
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::operator==(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen == m_stLen))
    {
        return (T::Cmpn(m_pszRef, *aSrc, stLen) == 0);
    }
    return (m_pszRef == *aSrc);
}

template <typename T>
INLINE bool CTStringRef<T>::operator!=(const CTString<T>& aSrc) const
{
    size_t stLen = aSrc.Length();
    if ((m_pszRef != nullptr) && (stLen == m_stLen))
    {
        return (T::Cmpn(m_pszRef, *aSrc, stLen) != 0);
    }
    return (m_pszRef != *aSrc);
}

template <typename T>
INLINE typename CTStringRef<T>::TChar CTStringRef<T>::operator[](size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_pszRef[stIndex];
    }
    return 0;
}

template <typename T>
INLINE const typename CTStringRef<T>::TChar CTStringRef<T>::operator[](size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_pszRef[stIndex];
    }
    return 0;
}

template <typename T>
INLINE typename CTStringRef<T>::PTStr CTStringRef<T>::operator*(void)
{
    return (PTStr)m_pszRef;
}

template <typename T>
INLINE typename CTStringRef<T>::PCTStr CTStringRef<T>::operator*(void) const
{
    return m_pszRef;
}

template <typename T>
INLINE typename CTStringRef<T>::PCTStr CTStringRef<T>::GetBuffer(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return (m_pszRef + stIndex);
    }
    return nullptr;
}

template <typename T>
INLINE typename CTStringRef<T>::TChar CTStringRef<T>::GetAt(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_pszRef[stIndex];
    }
    return 0;
}

template <typename T>
INLINE const typename CTStringRef<T>::TChar CTStringRef<T>::GetAt(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_pszRef[stIndex];
    }
    return 0;
}

template <typename T>
INLINE bool CTStringRef<T>::SetBufferLength(PCTStr psz, size_t stLen)
{
    if (psz != nullptr)
    {
        m_stLen = stLen;
        if (m_stLen > 0)
        {
            m_pszRef = psz;
        }
        else
        {
            m_pszRef = nullptr;
        }
    }
    else
    {
        m_stLen  = 0; 
        m_pszRef = nullptr;
    }
    return (m_pszRef != nullptr);
}

template <typename T>
INLINE bool CTStringRef<T>::IsEmpty(void) const
{
    return (m_stLen == 0);
}

template <typename T>
INLINE void CTStringRef<T>::Empty(void)
{
    m_stLen  = 0;
    m_pszRef = nullptr;
}

template <typename T>
INLINE size_t CTStringRef<T>::ResetLength(size_t stLen)
{
    m_stLen = stLen;
    if (m_stLen == 0)
    {
        m_pszRef = nullptr;
    }
    return stLen;
}

template <typename T>
INLINE size_t CTStringRef<T>::Length(bool bStream) const
{
    if (bStream == false)
    {
        return m_stLen;
    }
    else
    {
        return (m_stLen * sizeof(TChar) + sizeof(SizeLen));
    }
}

template <typename T>
INLINE Int CTStringRef<T>::Find(TChar ch, size_t stStart, bool bRev) const
{
    if (stStart < m_stLen)
    {
        PCTStr p = nullptr;
        if (bRev == false)
        {
            p = T::Chr(GetBuffer(stStart), ch);
        }
        else
        {
            p = T::RevChr(GetBuffer(stStart), ch);
        }
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_pszRef);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Find(PCTStr pszSub, size_t stStart) const
{
    if (stStart < m_stLen)
    {
        PCTStr p = T::Str(GetBuffer(stStart), pszSub);
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_pszRef);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTStringRef<T>::OneOf(PCTStr pszMatch, size_t stStart, bool bInMatch) const
{
    if (stStart < m_stLen)
    {
        size_t stRet = 0;
        if (bInMatch)
        {
            stRet = T::OneIndex(GetBuffer(stStart), pszMatch);
        }
        else
        {
            stRet = T::NotIndex(GetBuffer(stStart), pszMatch);
        }
        if (stRet < m_stLen)
        {
            return ((Int)stRet);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Cmp(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return T::Cmpn(m_pszRef, psz, stLen);
        }
        else if (stLen < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Cmpi(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return T::Cmpin(m_pszRef, psz, stLen);
        }
        else if (stLen < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Cmpn(PCTStr psz, size_t stSize) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        if (stSize == m_stLen)
        {
            return T::Cmpn(m_pszRef, psz, stSize);
        }
        else if (stSize < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Cmpin(PCTStr psz, size_t stSize) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        if (stSize == m_stLen)
        {
            return T::Cmpin(m_pszRef, psz, stSize);
        }
        else if (stSize < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Coll(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return T::Colln(m_pszRef, psz, stLen);
        }
        else if (stLen < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Colli(PCTStr psz) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        size_t stLen = T::Length(psz);
        if (stLen == m_stLen)
        {
            return T::Collin(m_pszRef, psz, stLen);
        }
        else if (stLen < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Colln(PCTStr psz, size_t stSize) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        if (stSize == m_stLen)
        {
            return T::Colln(m_pszRef, psz, stSize);
        }
        else if (stSize < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTStringRef<T>::Collin(PCTStr psz, size_t stSize) const
{
    if ((m_pszRef != nullptr) && (psz != nullptr))
    {
        if (stSize == m_stLen)
        {
            return T::Collin(m_pszRef, psz, stSize);
        }
        else if (stSize < m_stLen)
        {
            return 1;
        }
        return -1;
    }
    return (m_pszRef == psz) ? 0 : -1;
}

template <typename T>
INLINE bool CTStringRef<T>::IsAlnumChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlnum(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsAlphaChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlpha(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsPrintChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsPrint(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsGraphChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsGraph(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsDigit(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsXDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsXDigit(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsSpaceChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsSpace(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsLowerChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsLower(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTStringRef<T>::IsUpperChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsUpper(m_pszRef[stIndex]);
    }
    return false;
}

template <typename T>
INLINE Int CTStringRef<T>::ToInt(void) const
{
    if (m_stLen > 0)
    {
        return T::ToInt(m_pszRef);
    }
    return 0;
}

template <typename T>
INLINE Long CTStringRef<T>::ToLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLong(m_pszRef, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE ULong CTStringRef<T>::ToULong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULong(m_pszRef, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE LLong CTStringRef<T>::ToLLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLLong(m_pszRef, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE ULLong CTStringRef<T>::ToULLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULLong(m_pszRef, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE Double CTStringRef<T>::ToDouble(void) const
{
    if (m_stLen > 0)
    {
        return T::ToDouble(m_pszRef, nullptr);
    }
    return 0;
}

template <typename T>
INLINE Long CTStringRef<T>::ToLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd = nullptr;
        Long  lRet = T::ToLong((m_pszRef + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszRef + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszRef);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return lRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE ULong CTStringRef<T>::ToULong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        ULong ulRet = T::ToULong((m_pszRef + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszRef + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszRef);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ulRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE LLong CTStringRef<T>::ToLLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        LLong llRet = T::ToLLong((m_pszRef + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszRef + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszRef);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return llRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE ULLong CTStringRef<T>::ToULLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd   = nullptr;
        ULLong ullRet = T::ToULLong((m_pszRef + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszRef + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszRef);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ullRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE Double CTStringRef<T>::ToDouble(size_t& stIndex) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd = nullptr;
        Double dRet = T::ToDouble((m_pszRef + stIndex), &pEnd);
        if ((pEnd != (m_pszRef + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszRef);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return dRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

///////////////////////////////////////////////////////////////////
// CTStringFix
template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::CTStringFix(PCTStr psz, size_t stLen)
: m_stLen(0)
{
    m_szBuffer[0] = 0;
    FillBuffer(psz, stLen);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::~CTStringFix(void)
{
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::CTStringFix(CTStringFix<T, stFix>&& aSrc)
: m_stLen(aSrc.m_stLen)
{
    FillBuffer(aSrc.m_szBuffer, aSrc.m_stLen);
    aSrc.m_stLen  = 0;
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(CTStringFix<T, stFix>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        FillBuffer(aSrc.m_szBuffer, aSrc.m_stLen);
        aSrc.m_stLen  = 0;
    }
    return (*this);
}
#endif

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::CTStringFix(const CTStringFix<T, stFix>& aSrc)
: m_stLen(0)
{
    m_szBuffer[0] = 0;
    FillBuffer(aSrc.m_szBuffer, aSrc.m_stLen);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE CTStringFix<T, stFix>::CTStringFix(const CTStringFix<T, stLenT>& aSrc)
: m_stLen(0)
{
    m_szBuffer[0] = 0;
    FillBuffer(*aSrc, aSrc.Length());
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::CTStringFix(const CTStringRef<T>& aSrc)
: m_stLen(0)
{
    m_szBuffer[0] = 0;
    FillBuffer(*aSrc, aSrc.Length());
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>::CTStringFix(const CTString<T>& aSrc)
: m_stLen(0)
{
    m_szBuffer[0] = 0;
    FillBuffer(*aSrc, aSrc.Length());
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(PCTStr psz)
{
    FillBuffer(psz);
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(const CTStringFix<T, stFix>& aSrc)
{
    if (&aSrc != this)
    {
        FillBuffer(aSrc.m_szBuffer, aSrc.m_stLen);
    }
    return (*this);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(const CTStringFix<T, stLenT>& aSrc)
{
    FillBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(const CTStringRef<T>& aSrc)
{
    FillBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator=(const CTString<T>& aSrc)
{
    FillBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(TChar ch)
{
    AppendBuffer(ch);
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(PCTStr psz)
{
    AppendBuffer(psz);
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(const CTStringFix<T, stFix>& aSrc)
{
    AppendBuffer(aSrc.m_szBuffer, aSrc.m_stLen);
    return (*this);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(const CTStringFix<T, stLenT>& aSrc)
{
    AppendBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(const CTStringRef<T>& aSrc)
{
    AppendBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix>& CTStringFix<T, stFix>::operator+=(const CTString<T>& aSrc)
{
    AppendBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<=(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) <= 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) < 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>=(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) >= 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) > 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator==(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) == 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator!=(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return (T::Cmp(m_szBuffer, psz) != 0);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<=(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) <= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) < 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>=(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) >= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) > 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator==(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) == 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator!=(const CTStringFix<T, stFix>& aSrc) const
{
    return (T::Cmp(m_szBuffer, aSrc.m_szBuffer) != 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator<=(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) <= 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator<(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) < 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator>=(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) >= 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator>(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) > 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator==(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) == 0);
}

template <typename T, size_t stFix>
template <size_t stLenT>
INLINE bool CTStringFix<T, stFix>::operator!=(const CTStringFix<T, stLenT>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) != 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<=(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) <= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) < 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>=(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) >= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) > 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator==(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) == 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator!=(const CTStringRef<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) != 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<=(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) <= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator<(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) < 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>=(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) >= 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator>(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) > 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator==(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) == 0);
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::operator!=(const CTString<T>& aSrc) const
{
    return (T::Cmp(m_szBuffer, *aSrc) != 0);
}

template <typename T, size_t stFix>
INLINE typename CTStringFix<T, stFix>::TChar CTStringFix<T, stFix>::operator[](size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_szBuffer[stIndex];
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE const typename CTStringFix<T, stFix>::TChar CTStringFix<T, stFix>::operator[](size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_szBuffer[stIndex];
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE typename CTStringFix<T, stFix>::PTStr CTStringFix<T, stFix>::operator*(void)
{
    return m_szBuffer;
}

template <typename T, size_t stFix>
INLINE typename CTStringFix<T, stFix>::PCTStr CTStringFix<T, stFix>::operator*(void) const
{
    return m_szBuffer;
}

//template <typename T, size_t stFix>
//INLINE CTStringFix<T, stFix>::operator typename CTStringFix<T, stFix>::PCTStr(void) const
//{
//    return m_szBuffer;
//}

template <typename T, size_t stFix>
INLINE typename CTStringFix<T, stFix>::PCTStr CTStringFix<T, stFix>::GetBuffer(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return (m_szBuffer + stIndex);
    }
    return nullptr;
}

template <typename T, size_t stFix>
INLINE typename T::TChar CTStringFix<T, stFix>::GetAt(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_szBuffer[stIndex];
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE const typename T::TChar CTStringFix<T, stFix>::GetAt(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_szBuffer[stIndex];
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::SetAt(size_t stIndex, TChar ch)
{
    if ((stIndex < m_stLen) && (ch != 0))
    {
        m_szBuffer[stIndex] = ch;
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::FillBuffer(TChar ch, size_t stCount)
{
    if ((ch != 0) && (stCount > 0))
    {
        if (stCount >= stFix)
        {
            stCount = stFix - 1;
        }
        for (size_t i = 0; i < stCount; ++i)
        {
            m_szBuffer[i] = ch;
        }
        m_stLen = stCount;
        m_szBuffer[m_stLen] = 0;
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::FillBuffer(PCTStr psz, size_t stLen)
{
    if (psz == nullptr)
    {
        stLen = 0;
    }
    else
    {
        if (psz == m_szBuffer)
        {
            return false;
        }
        if (stLen == 0)
        {
            stLen = T::Length(psz, stFix);
        }
        if (stLen > 0)
        {
            if (stLen >= stFix)
            {
                stLen = stFix - 1;
            }
            //T::Copy(m_szBuffer, stFix, psz, stLen);
            MM_SAFE::Cpy(m_szBuffer, stFix * sizeof(TChar), psz, stLen * sizeof(TChar));
        }
    }
    m_szBuffer[stLen] = 0;
    m_stLen = stLen;
    return true;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::AppendBuffer(PCTStr psz, size_t stLen)
{
    if (psz == nullptr)
    {
        return false;
    }
    if (psz == m_szBuffer)
    {
        return false;
    }
    if (stLen == 0)
    {
        stLen = T::Length(psz, stFix);
    }
    if (stLen == 0)
    {
        return false;
    }
    else if ((m_stLen + stLen) >= stFix)
    {
        stLen = stFix - 1 - m_stLen;
    }
    //T::Copy(m_szBuffer + m_stLen, stFix - m_stLen, psz, stLen);
    MM_SAFE::Cpy((m_szBuffer + m_stLen), (stFix - m_stLen) * sizeof(TChar), psz, stLen * sizeof(TChar));
    m_stLen += stLen;
    m_szBuffer[m_stLen] = 0;
    return true;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::AppendBuffer(TChar ch, size_t stCount)
{
    if ((ch != 0) && (stCount > 0))
    {
        if ((m_stLen + stCount) < stFix)
        {
            for (size_t i = 0; i < stCount; ++i)
            {
                m_szBuffer[m_stLen + i] = ch;
            }
            m_stLen += stCount;
            m_szBuffer[m_stLen] = 0;
            return true;
        }
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsEmpty(void) const
{
    return (m_stLen == 0);
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Empty(void)
{
    m_szBuffer[0] = 0;
    m_stLen = 0;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::ResetLength(size_t stLen)
{
    if (stLen == 0)
    {
        m_stLen = T::Length(m_szBuffer, stFix);
    }
    else
    {
        m_stLen = stLen;
    }
    if (m_stLen >= stFix)
    {
        m_stLen = 0;
    }
    m_szBuffer[m_stLen] = 0;
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Length(bool bStream) const
{
    if (bStream == false)
    {
        return m_stLen;
    }
    else
    {
        return (m_stLen * sizeof(TChar) + sizeof(SizeLen));
    }
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::BufferLength(void) const
{
    return stFix;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Find(TChar ch, size_t stStart, bool bRev) const
{
    assert(stStart < m_stLen);
    if (stStart < m_stLen)
    {
        PCTStr p = nullptr;
        if (bRev == false)
        {
            p = T::Chr(GetBuffer(stStart), ch);
        }
        else
        {
            p = T::RevChr(GetBuffer(stStart), ch);
        }
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_szBuffer);
        }
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Find(PCTStr pszSub, size_t stStart) const
{
    assert(stStart <= m_stLen);
    if (stStart < m_stLen)
    {
        PCTStr p = T::Str(GetBuffer(stStart), pszSub);
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_szBuffer);
        }
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::OneOf(PCTStr pszMatch, size_t stStart, bool bInMatch) const
{
    assert(stStart <= m_stLen);
    if (stStart < m_stLen)
    {
        size_t stRet = 0;
        if (bInMatch)
        {
            stRet = T::OneIndex(GetBuffer(stStart), pszMatch);
        }
        else
        {
            stRet = T::NotIndex(GetBuffer(stStart), pszMatch);
        }
        if (stRet < m_stLen)
        {
            return ((Int)stRet);
        }
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Cmp(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return T::Cmp(m_szBuffer, psz);
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Cmpi(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return T::Cmpi(m_szBuffer, psz);
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Cmpn(PCTStr psz, size_t stLen) const
{
    if (psz == nullptr)
    {
        return -1;
    }
    if (stLen == 0)
    {
        stLen = T::Length(psz);
    }
    stLen = DEF::Max<size_t>(stLen, m_stLen);
    return T::Cmpn(m_szBuffer, psz, stLen);
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Cmpin(PCTStr psz, size_t stLen) const
{
    if (psz == nullptr)
    {
        return -1;
    }
    if (stLen == 0)
    {
        stLen = T::Length(psz);
    }
    stLen = DEF::Max<size_t>(stLen, m_stLen);
    return T::Cmpin(m_szBuffer, psz, stLen);
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Coll(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return T::Coll(m_szBuffer, psz);
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Colli(PCTStr psz) const
{
    if (psz != nullptr)
    {
        return T::Colli(m_szBuffer, psz);
    }
    return -1;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Colln(PCTStr psz, size_t stLen) const
{
    if (psz == nullptr)
    {
        return -1;
    }
    if (stLen == 0)
    {
        stLen = T::Length(psz);
    }
    stLen = DEF::Max<size_t>(stLen, m_stLen);
    return T::Colln(m_szBuffer, psz, stLen);
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::Collin(PCTStr psz, size_t stLen) const
{
    if (psz == nullptr)
    {
        return -1;
    }
    if (stLen == 0)
    {
        stLen = T::Length(psz);
    }
    stLen = DEF::Max<size_t>(stLen, m_stLen);
    return T::Collin(m_szBuffer, psz, stLen);
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Delete(size_t stIndex, size_t stCount)
{
    if ((stIndex < m_stLen) && (stCount > 0))
    {
        if ((stIndex + stCount) > m_stLen)
        {
            stCount = m_stLen - stIndex;
        }
        size_t stMove = (m_stLen - (stIndex + stCount) + 1) * sizeof(TChar);
        m_stLen -= stCount;
        MM_SAFE::Mov((m_szBuffer + stIndex), stMove, (m_szBuffer + stIndex + stCount), stMove);
        m_szBuffer[m_stLen] = 0;
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Insert(size_t stIndex, TChar ch)
{
    if ((m_stLen < (stFix - 1)) && (ch > 0))
    {
        if (stIndex < m_stLen)
        {
            ++m_stLen;
            size_t stMove = (m_stLen - stIndex) * sizeof(TChar); // include 0
            MM_SAFE::Mov((m_szBuffer + stIndex + 1), stMove, (m_szBuffer + stIndex), stMove);
            m_szBuffer[stIndex] = ch;
        }
        else
        {
            m_szBuffer[m_stLen] = ch;
            ++m_stLen;
            m_szBuffer[m_stLen] = 0;
        }
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Insert(size_t stIndex, PCTStr psz)
{
    if (psz == nullptr)
    {
        return m_stLen;
    }
    size_t stLen = T::Length(psz);
    if ((stLen + m_stLen) <= (stFix - 1))
    {
        if (stIndex < m_stLen)
        {
            size_t stMove = (m_stLen - stIndex + 1) * sizeof(TChar); // include 0
            MM_SAFE::Mov((m_szBuffer + stIndex + stLen), stMove, (m_szBuffer + stIndex), stMove);
            MM_SAFE::Cpy((m_szBuffer + stIndex), stLen * sizeof(TChar), psz, stLen * sizeof(TChar));

            m_stLen += stLen;
        }
        else
        {
            MM_SAFE::Cpy((m_szBuffer + m_stLen), stLen * sizeof(TChar), psz, stLen * sizeof(TChar));
            m_stLen += stLen;
            m_szBuffer[m_stLen] = 0;
        }
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Replace(TChar chOld, TChar chNew)
{
    size_t stReplace = 0;
    if ((chOld != chNew) && (chNew > 0))
    {
        for (size_t i = 0; i < m_stLen; ++i)
        {
            if (m_szBuffer[i] == chOld)
            {
                m_szBuffer[i] = chNew;
                ++stReplace;
            }
        }
    }
    return stReplace;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Replace(PCTStr pszOld, PCTStr pszNew)
{
    size_t stOldLen = T::Length(pszOld);
    if (stOldLen > 0)
    {
        size_t stNewLen = T::Length(pszNew);
        if ((stNewLen != stOldLen) ||
            (T::Cmpn(pszOld, pszNew, stNewLen) != 0))
        {
            size_t stCount = 0;
            PCTStr pFind   = nullptr;
            for (size_t i = 0; i < m_stLen; )
            {
                pFind = T::Str((m_szBuffer + i), pszOld);
                if (pFind != nullptr)
                {
                    ++stCount;
                    i = (pFind - m_szBuffer) + stOldLen;
                }
                else
                {
                    break;
                }
            }

            if ( (stCount > 0) && ((m_stLen + stNewLen * stCount - stOldLen * stCount) <= (stFix - 1)) )
            {
                size_t   stCopy          = 0;
                size_t   stLen           = 0;
                TChar    szBuffer[stFix] = { 0 };
                for (PCTStr pFindOld = m_szBuffer; pFindOld != nullptr; )
                {
                    pFind = T::Str(pFindOld, pszOld);
                    if (pFind != nullptr)
                    {
                        stCopy = (pFind - pFindOld);
                        MM_SAFE::Cpy((szBuffer + stLen), stCopy * sizeof(TChar), pFindOld, stCopy * sizeof(TChar));
                        stLen += stCopy;
                        MM_SAFE::Cpy((szBuffer + stLen), stNewLen * sizeof(TChar), pszNew, stNewLen * sizeof(TChar));
                        stLen += stNewLen;

                        pFindOld = pFind + stOldLen;
                    }
                    else
                    {
                        stCopy = (m_stLen - (pFindOld - m_szBuffer));
                        MM_SAFE::Cpy((szBuffer + stLen), stCopy * sizeof(TChar), pFindOld, stCopy * sizeof(TChar));
                        stLen += stCopy;
                        MM_SAFE::Cpy(m_szBuffer, stLen * sizeof(TChar), szBuffer, stLen * sizeof(TChar));
                        m_szBuffer[stLen] = 0;
                        m_stLen = stLen;
                        break;
                    }
                }
                return stCount;
            }
        }
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Remove(TChar ch)
{
    if (ch != 0)
    {
        size_t stIndex = 0;
        for (size_t i = 0; i < m_stLen; ++i)
        {
            if (m_szBuffer[i] != ch)
            {
                m_szBuffer[stIndex] = m_szBuffer[i];
                ++stIndex;
            }
        }
        if (stIndex > 0)
        {
            m_szBuffer[stIndex] = 0;
            m_stLen = stIndex;
        }
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::Format(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    FormatV(pszFormat, vl);
    va_end(vl);
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::FormatV(PCTStr pszFormat, va_list vl)
{
    Int nRet = T::FormatV(m_szBuffer, stFix, pszFormat, vl);
    if (nRet > 0)
    {
        m_stLen = (size_t)nRet;
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::AppendFormat(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    AppendFormatV(pszFormat, vl);
    va_end(vl);
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE size_t CTStringFix<T, stFix>::AppendFormatV(PCTStr pszFormat, va_list vl)
{
    Int nRet = T::FormatV((m_szBuffer + m_stLen), stFix - m_stLen, pszFormat, vl);
    if (nRet > 0)
    {
        m_stLen += (size_t)nRet;
    }
    return m_stLen;
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Upper(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        m_szBuffer[stIndex] = T::ToUpper(m_szBuffer[stIndex]);
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Lower(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        m_szBuffer[stIndex] = T::ToLower(m_szBuffer[stIndex]);
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Upper(void)
{
    if (m_stLen > 0)
    {
        T::ToUpper(m_szBuffer, m_stLen + 1);
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Lower(void)
{
    if (m_stLen > 0)
    {
        T::ToLower(m_szBuffer, m_stLen + 1);
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Reverse(void)
{
    if (m_stLen > 0)
    {
        T::Rev(m_szBuffer);
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::TrimLeft(TChar ch)
{
    size_t stTrim = 0;
    for (size_t i = 0; i < m_stLen; ++i)
    {
        if (m_szBuffer[i] == ch)
        {
            ++stTrim;
        }
        else
        {
            assert(stTrim <= m_stLen);
            if (stTrim > 0)
            {
                MM_SAFE::Mov(m_szBuffer,          (m_stLen - stTrim) * sizeof(TChar),
                             m_szBuffer + stTrim, (m_stLen - stTrim) * sizeof(TChar));
                m_stLen -= stTrim;
                m_szBuffer[m_stLen] = 0;
            }
            break;
        }
    }
    if (stTrim == m_stLen)
    {
        m_stLen       = 0;
        m_szBuffer[0] = 0;
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::TrimRight(TChar ch)
{
    size_t stTrim = m_stLen;
    for (size_t i = m_stLen ; i > 0; --i)
    {
        if (m_szBuffer[i - 1] == ch)
        {
            --stTrim;
        }
        else
        {
            if (stTrim < m_stLen)
            {
                m_szBuffer[stTrim] = 0;
                m_stLen = stTrim;
            }
            break;
        }
    }
    if (stTrim == 0)
    {
        m_stLen       = 0;
        m_szBuffer[0] = 0;
    }
}

template <typename T, size_t stFix>
INLINE void CTStringFix<T, stFix>::Trim(TChar ch)
{
    TrimRight(ch);
    TrimLeft(ch);
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix> CTStringFix<T, stFix>::Left(size_t stCount) const
{
    assert(stCount > 0);
    stCount = DEF::Min<size_t>(stCount, m_stLen);
    return (CTStringFix<T, stFix>(m_szBuffer, stCount));
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix> CTStringFix<T, stFix>::Right(size_t stCount) const
{
    assert(stCount > 0);
    stCount = DEF::Min<size_t>(stCount, m_stLen);
    return (CTStringFix<T, stFix>(m_szBuffer + m_stLen - stCount, stCount));
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix> CTStringFix<T, stFix>::RightPos(size_t stStart) const
{
    assert(stStart >= 0);
    stStart = DEF::Min<size_t>(stStart, m_stLen);
    return (CTStringFix<T, stFix>(m_szBuffer + stStart));
}

template <typename T, size_t stFix>
INLINE CTStringFix<T, stFix> CTStringFix<T, stFix>::Mid(size_t stStart, size_t stCount) const
{
    assert(stStart <= m_stLen);

    stStart = DEF::Min<size_t>(stStart, m_stLen);
    stCount = DEF::Min<size_t>(stCount, (m_stLen - stStart));
    return (CTStringFix<T, stFix>(m_szBuffer + stStart, stCount));
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsAlnumChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlnum(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsAlphaChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlpha(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsPrintChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsPrint(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsGraphChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsGraph(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsDigit(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsXDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsXDigit(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsSpaceChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsSpace(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsLowerChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsLower(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::IsUpperChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsUpper(m_szBuffer[stIndex]);
    }
    return false;
}

template <typename T, size_t stFix>
INLINE Int CTStringFix<T, stFix>::ToInt(void) const
{
    if (m_stLen > 0)
    {
        return T::ToInt(m_szBuffer);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE Long CTStringFix<T, stFix>::ToLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLong(m_szBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE ULong CTStringFix<T, stFix>::ToULong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULong(m_szBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE LLong CTStringFix<T, stFix>::ToLLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLLong(m_szBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE ULLong CTStringFix<T, stFix>::ToULLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULLong(m_szBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE Double CTStringFix<T, stFix>::ToDouble(void) const
{
    if (m_stLen > 0)
    {
        return T::ToDouble(m_szBuffer, nullptr);
    }
    return 0;
}

template <typename T, size_t stFix>
INLINE Long CTStringFix<T, stFix>::ToLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd = nullptr;
        Long  lRet = T::ToLong((m_szBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_szBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_szBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return lRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T, size_t stFix>
INLINE ULong CTStringFix<T, stFix>::ToULong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        ULong ulRet = T::ToULong((m_szBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_szBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_szBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ulRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T, size_t stFix>
INLINE LLong CTStringFix<T, stFix>::ToLLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        LLong llRet = T::ToLLong((m_szBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_szBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_szBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return llRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T, size_t stFix>
INLINE ULLong CTStringFix<T, stFix>::ToULLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd   = nullptr;
        ULLong ullRet = T::ToULLong((m_szBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_szBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_szBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ullRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T, size_t stFix>
INLINE Double CTStringFix<T, stFix>::ToDouble(size_t& stIndex) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd = nullptr;
        Double dRet = T::ToDouble((m_szBuffer + stIndex), &pEnd);
        if ((pEnd != (m_szBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_szBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return dRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(Int nValue, Int nRadix)
{
    if (T::ToString(nValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(UInt uValue, Int nRadix)
{
    if (T::ToString((ULong)uValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(Long lValue, Int nRadix)
{
    if (T::ToString(lValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(ULong ulValue, Int nRadix)
{
    if (T::ToString(ulValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(LLong llValue, Int nRadix)
{
    if (T::ToString(llValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(ULLong ullValue, Int nRadix)
{
    if (T::ToString(ullValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T, size_t stFix>
INLINE bool CTStringFix<T, stFix>::ToString(Double dValue, Int nRadix)
{
    if (T::ToString(dValue, m_szBuffer, stFix, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CTString
template <typename T>
INLINE CTString<T>::CTString(void)
: m_stLen(0)
, m_stBufferLen(0)
, m_pszBuffer(nullptr)
{
}

template <typename T>
INLINE CTString<T>::~CTString(void)
{
    if (m_pszBuffer != nullptr)
    {
        FREE( m_pszBuffer );
        m_pszBuffer = nullptr;
    }
}

template <typename T>
INLINE CTString<T>::CTString(PCTStr psz, size_t stLen)
: m_stLen(0)
, m_stBufferLen(0)
, m_pszBuffer(nullptr)
{
    FillBuffer(psz, stLen);
}

template <typename T>
INLINE CTString<T>::CTString(const CTString<T>& aSrc)
: m_stLen(0)
, m_stBufferLen(0)
, m_pszBuffer(nullptr)
{
    FillBuffer(aSrc.m_pszBuffer, aSrc.m_stLen);
}

template <typename T>
INLINE CTString<T>::CTString(const CTStringRef<T>& aSrc)
: m_stLen(0)
, m_stBufferLen(0)
, m_pszBuffer(nullptr)
{
    FillBuffer(*aSrc, aSrc.Length());
}

template <typename T>
template <size_t stLenT>
INLINE CTString<T>::CTString(const CTStringFix<T, stLenT>& aSrc)
: m_stLen(0)
, m_stBufferLen(0)
, m_pszBuffer(nullptr)
{
    FillBuffer(*aSrc, aSrc.Length());
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T>
INLINE CTString<T>::CTString(CTString<T>&& aSrc)
: m_stLen(aSrc.m_stLen)
, m_stBufferLen(aSrc.m_stBufferLen)
, m_pszBuffer(aSrc.m_pszBuffer)
{
    aSrc.m_stLen       = 0;
    aSrc.m_stBufferLen = 0;
    aSrc.m_pszBuffer   = nullptr;
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator=(CTString<T>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        if (m_pszBuffer != nullptr)
        {
            FREE(m_pszBuffer);
            m_pszBuffer = nullptr;
        }
        m_stLen            = aSrc.m_stLen;
        m_stBufferLen      = aSrc.m_stBufferLen;
        m_pszBuffer        = aSrc.m_pszBuffer;
        aSrc.m_stLen       = 0;
        aSrc.m_stBufferLen = 0;
        aSrc.m_pszBuffer   = nullptr;
    }
    return (*this);
}
#endif

template <typename T>
INLINE CTString<T>& CTString<T>::operator=(PCTStr psz)
{
    FillBuffer(psz);
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator=(const CTString<T>& aSrc)
{
    if (this != &aSrc)
    {
        FillBuffer(aSrc.m_pszBuffer, aSrc.m_stLen);
    }
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator=(const CTStringRef<T>& aSrc)
{
    FillBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
template <size_t stLenT>
INLINE CTString<T>& CTString<T>::operator=(const CTStringFix<T, stLenT>& aSrc)
{
    FillBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator+=(TChar ch)
{
    AppendBuffer(ch);
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator+=(PCTStr psz)
{
    AppendBuffer(psz);
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator+=(const CTString<T>& aSrc)
{
    AppendBuffer(aSrc.m_pszBuffer, aSrc.m_stLen);
    return (*this);
}

template <typename T>
INLINE CTString<T>& CTString<T>::operator+=(const CTStringRef<T>& aSrc)
{
    AppendBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
template <size_t stLenT>
INLINE CTString<T>& CTString<T>::operator+=(const CTStringFix<T, stLenT>& aSrc)
{
    AppendBuffer(*aSrc, aSrc.Length());
    return (*this);
}

template <typename T>
INLINE CTString<T> CTString<T>::operator+(TChar ch)
{
    return (CTString(*this) += ch);
}

template <typename T>
INLINE CTString<T> CTString<T>::operator+(PCTStr psz)
{
    return (CTString(*this) += psz);
}

template <typename T>
INLINE CTString<T> CTString<T>::operator+(const CTString<T>& aSrc)
{
    return (CTString(*this) += aSrc);
}

template <typename T>
INLINE CTString<T> CTString<T>::operator+(const CTStringRef<T>& aSrc)
{
    return (CTString(*this) += aSrc);
}

template <typename T>
template <size_t stLenT>
INLINE CTString<T> CTString<T>::operator+(const CTStringFix<T, stLenT>& aSrc)
{
    return (CTString(*this) += aSrc);
}

template <typename T>
INLINE bool CTString<T>::operator<=(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) <= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator<(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) < 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>=(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) >= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) > 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator==(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) == 0);
    }
    return (m_pszBuffer == psz);
}

template <typename T>
INLINE bool CTString<T>::operator!=(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return (T::Cmp(m_pszBuffer, psz) != 0);
    }
    return (m_pszBuffer != psz);
}

template <typename T>
INLINE bool CTString<T>::operator<=(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) <= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator<(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) < 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>=(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) >= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) > 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator==(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) == 0);
    }
    return (m_pszBuffer == aSrc.m_pszBuffer);
}

template <typename T>
INLINE bool CTString<T>::operator!=(const CTString<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (aSrc.m_pszBuffer != nullptr))
    {
        return (T::Cmp(m_pszBuffer, aSrc.m_pszBuffer) != 0);
    }
    return (m_pszBuffer != aSrc.m_pszBuffer);
}

template <typename T>
INLINE bool CTString<T>::operator<=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) <= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator<(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) < 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) >= 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator>(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) > 0);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::operator==(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) == 0);
    }
    return (m_pszBuffer == *aSrc);
}

template <typename T>
INLINE bool CTString<T>::operator!=(const CTStringRef<T>& aSrc) const
{
    if ((m_pszBuffer != nullptr) && (*aSrc != nullptr))
    {
        return (T::Cmp(m_pszBuffer, *aSrc) != 0);
    }
    return (m_pszBuffer != *aSrc);
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator<=(const CTStringFix<T, stLenT>& aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) <= 0);
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator<(const CTStringFix<T, stLenT>& aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) < 0);
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator>=(const CTStringFix<T, stLenT>& aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) >= 0);
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator>(const CTStringFix<T, stLenT>&aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) > 0);
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator==(const CTStringFix<T, stLenT>& aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) == 0);
    }
    return false;
}

template <typename T>
template <size_t stLenT>
INLINE bool CTString<T>::operator!=(const CTStringFix<T, stLenT>& aSrc) const
{
    if (m_pszBuffer != nullptr)
    {
        return (T::Cmp(m_pszBuffer, *aSrc) != 0);
    }
    return false;
}

template <typename T>
INLINE typename CTString<T>::TChar CTString<T>::operator[](size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_pszBuffer[stIndex];
    }
    return 0;
}

template <typename T>
INLINE const typename CTString<T>::TChar CTString<T>::operator[](size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_pszBuffer[stIndex];
    }
    return 0;
}

template <typename T>
INLINE typename CTString<T>::PTStr CTString<T>::operator*(void)
{
    return m_pszBuffer;
}

template <typename T>
INLINE typename CTString<T>::PCTStr CTString<T>::operator*(void) const
{
    return m_pszBuffer;
}

//template <typename T>
//INLINE CTString<T>::operator typename CTString<T>::PCTStr(void) const
//{
//    return m_pszBuffer;
//}

template <typename T>
INLINE typename CTString<T>::PCTStr CTString<T>::GetBuffer(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return (m_pszBuffer + stIndex);
    }
    return nullptr;
}

template <typename T>
INLINE typename CTString<T>::TChar CTString<T>::GetAt(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        return m_pszBuffer[stIndex];
    }
    return 0;
}

template <typename T>
INLINE const typename CTString<T>::TChar CTString<T>::GetAt(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return m_pszBuffer[stIndex];
    }
    return 0;
}

template <typename T>
INLINE bool CTString<T>::SetAt(size_t stIndex, TChar ch)
{
    if ((stIndex < m_stLen) && (ch != 0))
    {
        m_pszBuffer[stIndex] = ch;
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::FillBuffer(TChar ch, size_t stCount)
{
    if ((ch != 0) && (stCount > 0))
    {
        if ((stCount + 1) > m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>((stCount + 1), (size_t)LMT_KEY), true) == false) // add 1 for '\0'
            {
                return false;
            }
        }
        for (size_t i = 0; i < stCount; ++i)
        {
            m_pszBuffer[i] = ch;
        }
        m_stLen = stCount;
        m_pszBuffer[m_stLen] = 0;
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::FillBuffer(PCTStr psz, size_t stLen)
{
    if (psz == nullptr)
    {
        m_stLen = 0;
    }
    else
    {
        if (psz == m_pszBuffer)
        {
            return false;
        }
        if (stLen == 0)
        {
            stLen = (Int)T::Length(psz);
        }
        if (stLen > 0)
        {
            ++stLen; // add 1 for '\0'
            if (DEF::Align<size_t>(stLen, (size_t)LMT_KEY) > m_stBufferLen)
            {
                if (AllocBuffer(DEF::Align<size_t>(stLen, (size_t)LMT_KEY)) == false)
                {
                    return false;
                }
            }
            m_stLen = stLen - 1;
            //T::Copy(m_pszBuffer, m_stBufferLen, psz, m_stLen);
            MM_SAFE::Cpy(m_pszBuffer, m_stBufferLen * sizeof(TChar), psz, m_stLen * sizeof(TChar));
        }
        else
        {
            m_stLen = 0;
        }
    }
    if (m_pszBuffer != nullptr)
    {
        m_pszBuffer[m_stLen] = 0;
    }
    return true;
}

template <typename T>
INLINE bool CTString<T>::AppendBuffer(PCTStr psz, size_t stLen)
{
    if (psz == nullptr)
    {
        return false;
    }
    if (psz == m_pszBuffer)
    {
        return false;
    }
    if (stLen == 0)
    {
        stLen = (Int)T::Length(psz);
    }
    if (stLen == 0)
    {
        return false;
    }
    ++stLen; // add 1 for '\0'
    if (stLen + m_stLen > m_stBufferLen)
    {
        if (AllocBuffer(DEF::Align<size_t>(stLen + m_stLen, (size_t)LMT_KEY), true) == false)
        {
            return false;
        }
    }
    --stLen;
    //T::Copy(m_pszBuffer + m_stLen, (m_stBufferLen - m_stLen), psz, stLen);
    MM_SAFE::Cpy((m_pszBuffer + m_stLen), (m_stBufferLen - m_stLen) * sizeof(TChar), psz, stLen * sizeof(TChar));
    m_stLen += stLen;
    m_pszBuffer[m_stLen] = 0;
    return true;
}

template <typename T>
INLINE bool CTString<T>::AppendBuffer(TChar ch, size_t stCount)
{
    if ((ch != 0) && (stCount > 0))
    {
        if ((m_stLen + stCount + 1) >= m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>((m_stLen + stCount + 1), (size_t)LMT_KEY), true) == false) // add 1 for '\0'
            {
                return false;
            }
        }
        for (size_t i = 0; i < stCount; ++i)
        {
            m_pszBuffer[m_stLen + i] = ch;
        }
        m_stLen += stCount;
        m_pszBuffer[m_stLen] = 0;
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::SetBufferLength(size_t stLen)
{
    if (stLen == 0)
    {
        return false;
    }
    if (DEF::Align<size_t>(stLen, (size_t)LMT_KEY) > m_stBufferLen)
    {
        if (AllocBuffer(DEF::Align<size_t>(stLen, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    m_pszBuffer[0] = 0;
    m_pszBuffer[stLen - 1] = 0;
    m_stLen = 0;
    return true;
}

template <typename T>
INLINE bool CTString<T>::IsEmpty(void) const
{
    return (m_stLen == 0);
}

template <typename T>
INLINE void CTString<T>::Empty(bool bRelease)
{
    if (m_pszBuffer != nullptr)
    {
        if (bRelease)
        {
            FREE( m_pszBuffer );
            m_pszBuffer = nullptr;
        }
        else
        {
            m_pszBuffer[0] = 0;
        }
    }
    m_stLen = 0;
    m_stBufferLen = 0;
}

template <typename T>
INLINE size_t CTString<T>::ResetLength(size_t stLen)
{
    if (m_pszBuffer != nullptr)
    {
        if (stLen == 0)
        {
            m_stLen = T::Length(m_pszBuffer, m_stBufferLen);
        }
        else
        {
            m_stLen = stLen;
        }
        if (m_stLen >= m_stBufferLen)
        {
            m_stLen = 0;
        }
        m_pszBuffer[m_stLen] = 0;
    }
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::Length(bool bStream) const
{
    if (bStream == false)
    {
        return m_stLen;
    }
    else
    {
        return (m_stLen * sizeof(TChar) + sizeof(SizeLen));
    }
}

template <typename T>
INLINE size_t CTString<T>::BufferLength(void) const
{
    return m_stBufferLen;
}

template <typename T>
INLINE Int CTString<T>::Find(TChar ch, size_t stStart, bool bRev) const
{
    if (stStart < m_stLen)
    {
        PCTStr p = nullptr;
        if (bRev == false)
        {
            p = T::Chr(GetBuffer(stStart), ch);
        }
        else
        {
            p = T::RevChr(GetBuffer(stStart), ch);
        }
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_pszBuffer);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTString<T>::Find(PCTStr pszSub, size_t stStart) const
{
    if (stStart < m_stLen)
    {
        PCTStr p = T::Str(GetBuffer(stStart), pszSub);
        if (p != nullptr)
        {
            return (Int)(intptr_t)(p - m_pszBuffer);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTString<T>::OneOf(PCTStr pszMatch, size_t stStart, bool bInMatch) const
{
    if (stStart < m_stLen)
    {
        size_t stRet = 0;
        if (bInMatch)
        {
            stRet = T::OneIndex(GetBuffer(stStart), pszMatch);
        }
        else
        {
            stRet = T::NotIndex(GetBuffer(stStart), pszMatch);
        }
        if (stRet < m_stLen)
        {
            return ((Int)stRet);
        }
    }
    return -1;
}

template <typename T>
INLINE Int CTString<T>::Cmp(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return T::Cmp(m_pszBuffer, psz);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Cmpi(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return T::Cmpi(m_pszBuffer, psz);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Cmpn(PCTStr psz, size_t stLen) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        if (stLen == 0)
        {
            stLen = T::Length(psz);
        }
        stLen = DEF::Max<size_t>(stLen, m_stLen);
        return T::Cmpn(m_pszBuffer, psz, stLen);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Cmpin(PCTStr psz, size_t stLen) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        if (stLen == 0)
        {
            stLen = T::Length(psz);
        }
        stLen = DEF::Max<size_t>(stLen, m_stLen);
        return T::Cmpin(m_pszBuffer, psz, stLen);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Coll(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return T::Coll(m_pszBuffer, psz);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Colli(PCTStr psz) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        return T::Colli(m_pszBuffer, psz);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Colln(PCTStr psz, size_t stLen) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        if (stLen == 0)
        {
            stLen = T::Length(psz);
        }
        stLen = DEF::Max<size_t>(stLen, m_stLen);
        return T::Colln(m_pszBuffer, psz, stLen);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE Int CTString<T>::Collin(PCTStr psz, size_t stLen) const
{
    if ((m_pszBuffer != nullptr) && (psz != nullptr))
    {
        if (stLen == 0)
        {
            stLen = T::Length(psz);
        }
        stLen = DEF::Max<size_t>(stLen, m_stLen);
        return T::Collin(m_pszBuffer, psz, stLen);
    }
    return (m_pszBuffer == psz) ? 0 : -1;
}

template <typename T>
INLINE size_t CTString<T>::Delete(size_t stIndex, size_t stCount)
{
    if ((stIndex < m_stLen) && (stCount > 0))
    {
        if ((stIndex + stCount) > m_stLen)
        {
            stCount = m_stLen - stIndex;
        }
        size_t stMove = (m_stLen - (stIndex + stCount) + 1) * sizeof(TChar);
        m_stLen -= stCount;
        MM_SAFE::Mov((m_pszBuffer + stIndex), stMove, (m_pszBuffer + stIndex + stCount), stMove);
        m_pszBuffer[m_stLen] = 0;
    }
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::Insert(size_t stIndex, TChar ch)
{
    if (ch != 0)
    {
        if ((m_stLen + 1) >= m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>((m_stLen + 1 + 1), (size_t)LMT_KEY), true) == false) // add 1 for '\0'
            {
                return m_stLen;
            }
        }
        if (stIndex < m_stLen)
        {
            ++m_stLen;
            size_t stMove = (m_stLen - stIndex) * sizeof(TChar); // include 0
            MM_SAFE::Mov((m_pszBuffer + stIndex + 1), stMove, (m_pszBuffer + stIndex), stMove);
            m_pszBuffer[stIndex] = ch;
        }
        else
        {
            m_pszBuffer[m_stLen] = ch;
            ++m_stLen;
            m_pszBuffer[m_stLen] = 0;
        }
    }
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::Insert(size_t stIndex, PCTStr psz)
{
    if (psz == nullptr)
    {
        return m_stLen;
    }
    size_t stLen = T::Length(psz);
    if (stLen > 0)
    {
        if ((m_stLen + stLen) >= m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>((m_stLen + stLen + 1), (size_t)LMT_KEY), true) == false) // add 1 for '\0'
            {
                return m_stLen;
            }
        }
        if (stIndex < m_stLen)
        {
            size_t stMove = (m_stLen - stIndex + 1) * sizeof(TChar); // include 0
            MM_SAFE::Mov((m_pszBuffer + stIndex + stLen), stMove, (m_pszBuffer + stIndex), stMove);
            MM_SAFE::Cpy((m_pszBuffer + stIndex), stLen * sizeof(TChar), psz, stLen * sizeof(TChar));

            m_stLen += stLen;
        }
        else
        {
            MM_SAFE::Cpy((m_pszBuffer + m_stLen), stLen * sizeof(TChar), psz, stLen * sizeof(TChar));
            m_stLen += stLen;
            m_pszBuffer[m_stLen] = 0;
        }
    }
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::Replace(TChar chOld, TChar chNew)
{
    size_t stReplace = 0;
    if ((chOld != chNew) && (chNew > 0))
    {
        for (size_t i = 0; i < m_stLen; ++i)
        {
            if (m_pszBuffer[i] == chOld)
            {
                m_pszBuffer[i] = chNew;
                ++stReplace;
            }
        }
    }
    return stReplace;
}

template <typename T>
INLINE size_t CTString<T>::Replace(PCTStr pszOld, PCTStr pszNew)
{
    size_t stOldLen = T::Length(pszOld);
    if (stOldLen > 0)
    {
        size_t stNewLen = T::Length(pszNew);
        if ((stNewLen != stOldLen) ||
            (T::Cmpn(pszOld, pszNew, stNewLen) != 0))
        {
            size_t stCount = 0;
            PCTStr pFind   = nullptr;
            for (size_t i = 0; i < m_stLen; )
            {
                pFind = T::Str((m_pszBuffer + i), pszOld);
                if (pFind != nullptr)
                {
                    ++stCount;
                    i = (pFind - m_pszBuffer) + stOldLen;
                }
                else
                {
                    break;
                }
            }

            if (stCount > 0)
            {
                PTStr  pszBuffer      = nullptr;
                size_t stNewBufferLen = DEF::Align<size_t>((m_stLen + stNewLen * stCount - stOldLen * stCount + 1), (size_t)LMT_KEY);  // add 1 for '\0'
                if (AllocBuffer(stNewBufferLen, pszBuffer))
                {
                    size_t stCopy = 0;
                    size_t stLen  = 0;
                    for (PCTStr pFindOld  = m_pszBuffer; pFindOld != nullptr; )
                    {
                        pFind = T::Str(pFindOld, pszOld);
                        if (pFind != nullptr)
                        {
                            stCopy = (pFind - pFindOld);
                            MM_SAFE::Cpy((pszBuffer + stLen), stCopy * sizeof(TChar), pFindOld, stCopy * sizeof(TChar));
                            stLen  += stCopy;
                            MM_SAFE::Cpy((pszBuffer + stLen), stNewLen * sizeof(TChar), pszNew, stNewLen * sizeof(TChar));
                            stLen  += stNewLen;

                            pFindOld = pFind + stOldLen;
                        }
                        else
                        {
                            stCopy = (m_stLen - (pFindOld - m_pszBuffer));
                            MM_SAFE::Cpy((pszBuffer + stLen), stCopy * sizeof(TChar), pFindOld, stCopy * sizeof(TChar));
                            stLen += stCopy;
                            pszBuffer[stLen] = 0;
                            Empty(true);
                            m_pszBuffer   = pszBuffer;
                            m_stLen       = stLen;
                            m_stBufferLen = stNewBufferLen;
                            break;
                        }
                    }
                    return stCount;
                }
            } // (stCount > 0)
        } // pszOld <> pszNew
    } // (stOldLen > 0)
    return 0;
}

template <typename T>
INLINE size_t CTString<T>::Remove(TChar ch)
{
    if (ch != 0)
    {
        size_t stIndex = 0;
        for (size_t i = 0; i < m_stLen; ++i)
        {
            if (m_pszBuffer[i] != ch)
            {
                m_pszBuffer[stIndex] = m_pszBuffer[i];
                ++stIndex;
            }
        }
        if (stIndex > 0)
        {
            m_pszBuffer[stIndex] = 0;
            m_stLen = stIndex;
        }
    }
    return m_stLen;
}

template <typename T>
INLINE void CTString<T>::Shrink(void)
{
    // add 1 for '\0'
    if (DEF::Align<size_t>(m_stLen + 1, (size_t)LMT_KEY) < (m_stBufferLen >> 1))
    {
        AllocBuffer(DEF::Align<size_t>(m_stLen + 1, (size_t)LMT_KEY), true);
    }
}

template <typename T>
INLINE bool CTString<T>::Attach(CTString& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        if (m_pszBuffer != nullptr)
        {
            FREE(m_pszBuffer);
            m_pszBuffer = nullptr;
        }
        m_stLen            = aSrc.m_stLen;
        m_stBufferLen      = aSrc.m_stBufferLen;
        m_pszBuffer        = aSrc.m_pszBuffer;
        aSrc.m_stLen       = 0;
        aSrc.m_stBufferLen = 0;
        aSrc.m_pszBuffer   = nullptr;
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::Attach(PByte pBuf, size_t stBuf, size_t stLen)
{
    assert(pBuf != nullptr);
    assert(stLen > 0);
    assert(stBuf >= stLen);
    if (m_pszBuffer != nullptr)
    {
        FREE(m_pszBuffer);
    }
    m_pszBuffer   = (PTStr)pBuf;
    m_stBufferLen = stBuf / sizeof(TChar);
    m_stLen       = stLen / sizeof(TChar);
    m_pszBuffer[m_stLen] = 0;
    return true;
}

template <typename T>
INLINE void CTString<T>::Detach(PByte& pBuf, size_t& stBuf, size_t& stLen)
{
    pBuf  = (PByte)m_pszBuffer;
    stBuf = m_stBufferLen * sizeof(TChar);
    stLen = m_stLen * sizeof(TChar);
    m_stLen       = 0;
    m_stBufferLen = 0;
    m_pszBuffer   = nullptr;
}

template <typename T>
INLINE void CTString<T>::Detach(void)
{
    m_stLen       = 0;
    m_stBufferLen = 0;
    m_pszBuffer   = nullptr;
}

template <typename T>
INLINE size_t CTString<T>::Format(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    FormatV(pszFormat, vl);
    va_end(vl);
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::FormatV(PCTStr pszFormat, va_list vl)
{
    Int nRet = T::FormatLengthV(pszFormat, vl);
    if (nRet > 0)
    {
        ++nRet; // add 1 for '\0'
        if (DEF::Align<size_t>((size_t)nRet, (size_t)LMT_KEY) > m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>((size_t)nRet, (size_t)LMT_KEY)) == false)
            {
                return m_stLen;
            }
        }
        nRet = T::FormatV(m_pszBuffer, m_stBufferLen, pszFormat, vl);
        if (nRet > 0)
        {
            m_stLen = (size_t)nRet;
        }
    }
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::AppendFormat(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    AppendFormatV(pszFormat, vl);
    va_end(vl);
    return m_stLen;
}

template <typename T>
INLINE size_t CTString<T>::AppendFormatV(PCTStr pszFormat, va_list vl)
{
    Int nRet = T::FormatLengthV(pszFormat, vl);
    if (nRet > 0)
    {
        ++nRet; // add 1 for '\0'
        if (DEF::Align<size_t>(m_stLen + (size_t)nRet, (size_t)LMT_KEY) > m_stBufferLen)
        {
            if (AllocBuffer(DEF::Align<size_t>(m_stLen + (size_t)nRet, (size_t)LMT_KEY), true) == false)
            {
                return m_stLen;
            }
        }
        nRet = T::FormatV((m_pszBuffer + m_stLen), (m_stBufferLen - m_stLen), pszFormat, vl);
        if (nRet > 0)
        {
            m_stLen += (size_t)nRet;
        }
    }
    return m_stLen;
}

template <typename T>
INLINE void CTString<T>::Upper(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        m_pszBuffer[stIndex] = T::ToUpper(m_pszBuffer[stIndex]);
    }
}

template <typename T>
INLINE void CTString<T>::Lower(size_t stIndex)
{
    if (stIndex < m_stLen)
    {
        m_pszBuffer[stIndex] = T::ToLower(m_pszBuffer[stIndex]);
    }
}

template <typename T>
INLINE void CTString<T>::Upper(void)
{
    if (m_stLen > 0)
    {
        T::ToUpper(m_pszBuffer, m_stLen + 1);
    }
}

template <typename T>
INLINE void CTString<T>::Lower(void)
{
    if (m_stLen > 0)
    {
        T::ToLower(m_pszBuffer, m_stLen + 1);
    }
}

template <typename T>
INLINE void CTString<T>::Reverse(void)
{
    if (m_stLen > 0)
    {
        T::Rev(m_pszBuffer);
    }
}

template <typename T>
INLINE void CTString<T>::TrimLeft(TChar ch)
{
    size_t stTrim = 0;
    for (size_t i = 0; i < m_stLen; ++i)
    {
        if (m_pszBuffer[i] == ch)
        {
            ++stTrim;
        }
        else
        {
            assert(stTrim <= m_stLen);
            if (stTrim > 0)
            {
                MM_SAFE::Mov(m_pszBuffer,          (m_stLen - stTrim) * sizeof(TChar),
                             m_pszBuffer + stTrim, (m_stLen - stTrim) * sizeof(TChar));
                m_stLen -= stTrim;
                m_pszBuffer[m_stLen] = 0;
            }
            break;
        }
    }
    if (stTrim == m_stLen)
    {
        m_stLen        = 0;
        m_pszBuffer[0] = 0;
    }
}

template <typename T>
INLINE void CTString<T>::TrimRight(TChar ch)
{
    size_t stTrim = m_stLen;
    for (size_t i = m_stLen; i > 0; --i)
    {
        if (m_pszBuffer[i - 1] == ch)
        {
            --stTrim;
        }
        else
        {
            if (stTrim < m_stLen)
            {
                m_pszBuffer[stTrim] = 0;
                m_stLen = stTrim;
            }
            break;
        }
    }
    if (stTrim == 0)
    {
        m_stLen        = 0;
        m_pszBuffer[0] = 0;
    }
}

template <typename T>
INLINE void CTString<T>::Trim(TChar ch)
{
    TrimRight(ch);
    TrimLeft(ch);
}

template <typename T>
INLINE CTString<T> CTString<T>::Left(size_t stCount) const
{
    assert(stCount > 0);
    stCount = DEF::Min<size_t>(stCount, m_stLen);
    return (CTString<T>(m_pszBuffer, stCount));
}

template <typename T>
INLINE CTString<T> CTString<T>::Right(size_t stCount) const
{
    assert(stCount > 0);
    stCount = DEF::Min<size_t>(stCount, m_stLen);
    return (CTString<T>(m_pszBuffer + m_stLen - stCount, stCount));
}

template <typename T>
INLINE CTString<T> CTString<T>::RightPos(size_t stStart) const
{
    assert(stStart >= 0);
    stStart = DEF::Min<size_t>(stStart, m_stLen);
    return (CTString<T>(m_pszBuffer + stStart));
}

template <typename T>
INLINE CTString<T> CTString<T>::Mid(size_t stStart, size_t stCount) const
{
    assert(stStart <= m_stLen);

    stStart = DEF::Min<size_t>(stStart, m_stLen);
    stCount = DEF::Min<size_t>(stCount, (m_stLen - stStart));
    return (CTString<T>(m_pszBuffer + stStart, stCount));
}

template <typename T>
INLINE bool CTString<T>::IsAlnumChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlnum(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsAlphaChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsAlpha(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsPrintChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsPrint(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsGraphChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsGraph(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsDigit(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsXDigitChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsXDigit(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsSpaceChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsSpace(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsLowerChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsLower(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::IsUpperChar(size_t stIndex) const
{
    if (stIndex < m_stLen)
    {
        return T::IsUpper(m_pszBuffer[stIndex]);
    }
    return false;
}

template <typename T>
INLINE Int CTString<T>::ToInt(void) const
{
    if (m_stLen > 0)
    {
        return T::ToInt(m_pszBuffer);
    }
    return 0;
}

template <typename T>
INLINE Long CTString<T>::ToLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLong(m_pszBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE ULong CTString<T>::ToULong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULong(m_pszBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE LLong CTString<T>::ToLLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToLLong(m_pszBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE ULLong CTString<T>::ToULLong(Int nRadix) const
{
    if (m_stLen > 0)
    {
        return T::ToULLong(m_pszBuffer, nullptr, nRadix);
    }
    return 0;
}

template <typename T>
INLINE Double CTString<T>::ToDouble(void) const
{
    if (m_stLen > 0)
    {
        return T::ToDouble(m_pszBuffer, nullptr);
    }
    return 0;
}

template <typename T>
INLINE Long CTString<T>::ToLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd = nullptr;
        Long  lRet = T::ToLong((m_pszBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return lRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE ULong CTString<T>::ToULong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        ULong ulRet = T::ToULong((m_pszBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ulRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE LLong CTString<T>::ToLLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr pEnd  = nullptr;
        LLong llRet = T::ToLLong((m_pszBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return llRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE ULLong CTString<T>::ToULLong(size_t& stIndex, Int nRadix) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd   = nullptr;
        ULLong ullRet = T::ToULLong((m_pszBuffer + stIndex), &pEnd, nRadix);
        if ((pEnd != (m_pszBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return ullRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE Double CTString<T>::ToDouble(size_t& stIndex) const
{
    if (stIndex < m_stLen)
    {
        PTStr  pEnd = nullptr;
        Double dRet = T::ToDouble((m_pszBuffer + stIndex), &pEnd);
        if ((pEnd != (m_pszBuffer + stIndex)) && (*pEnd != 0))
        {
            stIndex = (pEnd - m_pszBuffer);
        }
        else
        {
            stIndex = (size_t)-1;
        }
        return dRet;
    }
    stIndex = (size_t)-1;
    return 0;
}

template <typename T>
INLINE bool CTString<T>::ToString(Int nValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(nValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(UInt uValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString((ULong)uValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(Long lValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(lValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(ULong ulValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(ulValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(LLong llValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(llValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(ULLong ullValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(ullValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::ToString(Double dValue, Int nRadix)
{
    if (m_stBufferLen < (size_t)LMT_KEY)
    {
        if (AllocBuffer(DEF::Align<size_t>(m_stBufferLen + (size_t)LMT_KEY, (size_t)LMT_KEY)) == false)
        {
            return false;
        }
    }
    if (T::ToString(dValue, m_pszBuffer, m_stBufferLen, nRadix))
    {
        ResetLength();
        return true;
    }
    return false;
}

template <typename T>
INLINE bool CTString<T>::AllocBuffer(size_t stBufferLen, bool bAlloc)
{
    if (m_pszBuffer == nullptr)
    {
        m_pszBuffer = reinterpret_cast<PTStr>(ALLOC( stBufferLen * sizeof(TChar) ));
        if (m_pszBuffer == nullptr)
        {
            return false;
        }
        m_stBufferLen = stBufferLen;
    }
    else
    {
        if (bAlloc)
        {
            PTStr p = reinterpret_cast<PTStr>(REALLOC(m_pszBuffer, stBufferLen * sizeof(TChar)));
            if (p == nullptr)
            {
                return false;
            }
            m_pszBuffer   = p;
            m_stBufferLen = stBufferLen;
        }
        else
        {
            PTStr p = reinterpret_cast<PTStr>(ALLOC( stBufferLen * sizeof(TChar) ));
            if (p == nullptr)
            {
                return false;
            }
            Empty(true);
            m_pszBuffer   = p;
            m_stBufferLen = stBufferLen;
        }
    }
    return true;
}

template <typename T>
INLINE bool CTString<T>::AllocBuffer(size_t stBufferLen, PTStr& pszBuffer)
{
    pszBuffer = reinterpret_cast<PTStr>(ALLOC( stBufferLen * sizeof(TChar) ));
    return (pszBuffer != nullptr);
}

#endif // __TSTRING_INL__
