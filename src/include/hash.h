// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __HASH_H__
#define __HASH_H__

#pragma once

#include "mobject.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// HASH_DUMP
typedef struct tagHASH_DUMP
{
public:
    tagHASH_DUMP(void)
    : llUsedCount(0)
    , llUsedTick(0)
    {
    }

    ~tagHASH_DUMP(void)
    {
    }
public:
    LLong   llUsedCount;
    LLong   llUsedTick;     // MS
}HASH_DUMP, *PHASH_DUMP;

///////////////////////////////////////////////////////////////////
// CHash
class CORECLASS CHash : public MObject
{
public:
    static size_t Hash(Char c);
    static size_t Hash(UChar uc);
    static size_t Hash(Short s);
    static size_t Hash(UShort us);
    static size_t Hash(WChar wc);
    static size_t Hash(Int n);
    static size_t Hash(UInt u);
    static size_t Hash(Long l);
    static size_t Hash(ULong ul);
    static size_t Hash(LLong ll);
    static size_t Hash(ULLong ull);
    static size_t Hash(void* p);

    static size_t Hash(PCStr  pszString, size_t stLen = (size_t)LMT_MAX);
    static size_t Hash(PCWStr pszString, size_t stLen = (size_t)LMT_MAX);
    static size_t Hash(PByte  pBuffer, size_t stLen);

    static UInt   Hash32(LLong ll);
    static UInt   Hash32(ULLong ull);
    static UInt   Hash32(void* p);

    static UInt   Hash32(PCStr  pszString, size_t stLen = (size_t)LMT_MAX);
    static UInt   Hash32(PCWStr pszString, size_t stLen = (size_t)LMT_MAX);
    static UInt   Hash32(PByte  pBuffer, size_t stLen);

    static ULLong Hash64(LLong ll);
    static ULLong Hash64(ULLong ull);
    static ULLong Hash64(void* p);

    static ULLong Hash64(PCStr  pszString, size_t stLen = (size_t)LMT_MAX);
    static ULLong Hash64(PCWStr pszString, size_t stLen = (size_t)LMT_MAX);
    static ULLong Hash64(PByte  pBuffer, size_t stLen);

    static void   Dump(HASH_DUMP& Dump);
private:
    CHash(void);
    ~CHash(void);
    CHash(const CHash&);
    CHash& operator=(const CHash&);
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __HASH_H__
