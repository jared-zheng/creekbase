// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __UID_H__
#define __UID_H__

#pragma once

#include "stream.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUUID
class CUUID : public MObject
{
public:
    enum UUID_FROMAT
    {
        UUID_FROMAT_DIGITS_BRACES,    // {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX} - 32 hex-digits + 4 Hyphens + 2 Braces
        UUID_FROMAT_DIGITS_HYPHENS,   //  XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX  - 32 hex-digits + 4 Hyphens
        UUID_FROMAT_DIGITS_AVERAGE,   //  XXXXXXXX-XXXXXXXX-XXXXXXXX-XXXXXXXX   - 32 hex-digits + 3 Hyphens
        UUID_FROMAT_DIGITS_ONLY,      //  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX      - 32 hex-digits
    };

    enum UUID_LEN
    {
        UUIDC_LEN_BRACES  = 38,
        UUIDC_LEN_HYPHENS = 36,
        UUIDC_LEN_AVERAGE = 35,
        UUIDC_LEN_ONLY    = 32,
        UUIDC_LEN_HEX     = 16,
        UUIDC_LEN_MAX     = 40,
    };
    typedef CTStringFix<CXChar, UUIDC_LEN_MAX>   CStringUUID;

    static CPCXStr FROMAT_DIGITS_BRACES;
    static CPCXStr FROMAT_DIGITS_HYPHENS;
    static CPCXStr FROMAT_DIGITS_AVERAGE;
    static CPCXStr FROMAT_DIGITS_ONLY;

    class CElementTraits
    {
    public:
        typedef const CUUID& INARGTYPE;
        typedef CUUID&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
        typedef CUUID&& RVARGTYPE;
#endif
    public:
        static void CopyElements(CUUID* pDst, const CUUID* pSrc, size_t stElements)
        {
            for (size_t i = 0; i < stElements; ++i)
            {
                pDst[i] = pSrc[i];
            }
        }

        static void RelocateElements(CUUID* pDst, CUUID* pSrc, size_t stElements)
        {
            MM_SAFE::Mov(pDst, stElements * sizeof(CUUID), pSrc, stElements * sizeof(CUUID));
        }

        static bool CompareElements(INARGTYPE t1, INARGTYPE t2)
        {
            return (t1 == t2);
        }

        static Int CompareElementsOrdered(INARGTYPE t1, INARGTYPE t2)
        {
            const UInt* puData1 = reinterpret_cast<const UInt*>(&t1);
            const UInt* puData2 = reinterpret_cast<const UInt*>(&t2);

            for (Int i = 3; i >= 0; --i)
            {
                if (puData1[i] > puData2[i] )
                {
                    return (Int)(1);
                }
                else if (puData1[i] < puData2[i])
                {
                    return (Int)(-1);
                }
            }
            return (Int)(0);
        }

        static size_t HashElements(INARGTYPE t)
        {
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
            const ULLong* pullData = reinterpret_cast<const ULLong*>(&t);
            return (size_t)(pullData[0] ^ pullData[1]);
#else
            const UInt* puData = reinterpret_cast<const UInt*>(&t);
            return (size_t)(puData[0] ^ puData[1] ^ puData[2] ^ puData[3]);
#endif
        }
    };
public:
    CUUID(void);
    ~CUUID(void);
    CUUID(const UID& uuid);
    CUUID(PCXStr pszUUID);

    CUUID(const CUUID& aSrc);
    CUUID& operator=(const CUUID& aSrc);

    bool operator==(const CUUID& aSrc) const;
    bool operator!=(const CUUID& aSrc) const;
    bool operator==(const UID& uuid) const;
    bool operator!=(const UID& uuid) const;

    void Serialize(CStream&);

    bool IsValid(void) const;

    void GetUUID(UID& uuid) const;
    void SetUUID(const UID& uuid);
    void Reset(void);

    // default : {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
    void ToString(CString& str, bool bAppend = false, UUID_FROMAT eFormat = UUID_FROMAT_DIGITS_BRACES) const;
    template <size_t stLenT> void ToString(CTStringFix<CXChar, stLenT>& strFix, bool bAppend = false, UUID_FROMAT eFormat = UUID_FROMAT_DIGITS_BRACES) const;
    void ToString(PXStr pszBuf, size_t stLen, UUID_FROMAT eFormat = UUID_FROMAT_DIGITS_BRACES) const;

    // pszBuf pointer to default : {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
    bool ToUUID(PCXStr pszBuf, UUID_FROMAT eFormat = UUID_FROMAT_DIGITS_BRACES);
private:
    UID   m_UUID;
};

typedef CUUID::CElementTraits CUUIDTraits;

template <>
class CTElementTraits<CUUID> : public CUUIDTraits {};

///////////////////////////////////////////////////////////////////
//
#define DECLARE_UUID( name, uuid ) static CPCXStr UUID_##name = (TF(#uuid)); \
                                   static CPCXStr NAME_##name = (TF(#name));
#define UUID_OF( name )            (CUUID((UUID_##name)))
#define UUID_STR( name )           (UUID_##name)
#define NAME_OF( name )            (NAME_##name)

///////////////////////////////////////////////////////////////////
#include "uid.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __UID_H__
