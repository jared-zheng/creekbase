// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __STREAM_BUF_H__
#define __STREAM_BUF_H__

#pragma once

#include "stream.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CBufStreamBase
class NOVTABLE CBufStreamBase ABSTRACT: public CStream
{
public:
    virtual ~CBufStreamBase(void);

    virtual PByte GetBuf(size_t stPos = 0) PURE;
    virtual const PByte GetBuf(size_t stPos = 0) const PURE;
    virtual PByte GetBufPos(void) PURE;
    virtual const PByte GetBufPos(void) const PURE;
protected:
    explicit CBufStreamBase(Int nMode);
private:
    CBufStreamBase(const CBufStreamBase&);
    CBufStreamBase& operator=(const CBufStreamBase&);
};

///////////////////////////////////////////////////////////////////
// CTBufStreamFix
template <size_t stFix = (size_t)LMT_KEY>
class NOVTABLE CTBufStreamFix ABSTRACT : public CBufStreamBase
{
public:
    virtual ~CTBufStreamFix(void);

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;

    virtual PByte  GetBuf(size_t stPos = 0) OVERRIDE;
    virtual const  PByte GetBuf(size_t stPos = 0) const OVERRIDE;
    virtual PByte  GetBufPos(void) OVERRIDE;
    virtual const  PByte GetBufPos(void) const OVERRIDE;
protected:
    explicit CTBufStreamFix(Int nMode);
protected:
    size_t   m_stPos;
    Byte     m_pBuf[stFix];
};

///////////////////////////////////////////////////////////////////
// CTBufReadStreamFix
template <size_t stFix = (size_t)LMT_KEY>
class CTBufReadStreamFix : public CTBufStreamFix<stFix>
{
    typedef CTBufStreamFix<stFix> TBuf;
public:
    CTBufReadStreamFix(void);
    virtual ~CTBufReadStreamFix(void);

    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CTBufWriteStreamFix
template <size_t stFix = (size_t)LMT_KEY>
class CTBufWriteStreamFix : public CTBufStreamFix<stFix>
{
    typedef CTBufStreamFix<stFix> TBuf;
public:
    CTBufWriteStreamFix(void);
    virtual ~CTBufWriteStreamFix(void);

    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CBufStream
class NOVTABLE CBufStream ABSTRACT: public CBufStreamBase
{
public:
    virtual ~CBufStream(void);

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;

    virtual void   Close(void) OVERRIDE;

    virtual PByte  GetBuf(size_t stPos = 0) OVERRIDE;
    virtual const  PByte GetBuf(size_t stPos = 0) const OVERRIDE;
    virtual PByte  GetBufPos(void) OVERRIDE;
    virtual const  PByte GetBufPos(void) const OVERRIDE;
    // assign/copy data, pos == 0
    bool    Attach(const CBufStream& BufStream);
    bool    Attach(CBufStream& BufStream, bool bCopy = false);
    // pBuf == nullptr CBufStream ALLOC managed buffer, ignore bManage value
    bool    Attach(size_t stSize = 0, PByte pBuf = nullptr, bool bManage = false);
    void    Detach(void);
protected:
    CBufStream(Int nMode, size_t stSize, PByte pBuf = nullptr, bool bManage = false);

    bool    Create(size_t stSize = 0, PByte pBuf = nullptr, bool bManage = false);
protected:
    bool     m_bManage;
    size_t   m_stSize;
    size_t   m_stPos;
    PByte    m_pBuf;
};

///////////////////////////////////////////////////////////////////
// CBufReadStream
class CBufReadStream : public CBufStream
{
public:
    explicit CBufReadStream(size_t stSize = 0, PByte pBuf = nullptr, bool bManage = false);
    virtual ~CBufReadStream(void);

    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;

    virtual bool   Refer(CStream& aSrc) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CBufWriteStream
class CBufWriteStream : public CBufStream
{
public:
    explicit CBufWriteStream(size_t stSize = 0, PByte pBuf = nullptr, bool bManage = false);
    virtual ~CBufWriteStream(void);

    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
#include "streambuf.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __STREAM_BUF_H__
