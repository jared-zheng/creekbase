// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __ATOMICS_H__
#define __ATOMICS_H__

#pragma once

#include "core.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CAtomics
class CAtomics
{
public:
    template <typename T> static T Increment16(T volatile* pDst);
    template <typename T> static T Increment(T volatile* pDst);
    template <typename T> static T Increment64(T volatile* pDst);

    template <typename T> static T Decrement16(T volatile* pDst);
    template <typename T> static T Decrement(T volatile* pDst);
    template <typename T> static T Decrement64(T volatile* pDst);

    template <typename T> static T Add16(T volatile* pDst, T tVal);
    template <typename T> static T Add(T volatile* pDst, T tVal);
    template <typename T> static T Add64(T volatile* pDst, T tVal);

    template <typename T> static T And16(T volatile* pDst, T tVal);
    template <typename T> static T And(T volatile* pDst, T tVal);
    template <typename T> static T And64(T volatile* pDst, T tVal);

    template <typename T> static T Or16(T volatile* pDst, T tVal);
    template <typename T> static T Or(T volatile* pDst, T tVal);
    template <typename T> static T Or64(T volatile* pDst, T tVal);

    template <typename T> static T Xor16(T volatile* pDst, T tVal);
    template <typename T> static T Xor(T volatile* pDst, T tVal);
    template <typename T> static T Xor64(T volatile* pDst, T tVal);

    template <typename T> static T Exchange16(T volatile* pDst, T tVal);
    template <typename T> static T Exchange(T volatile* pDst, T tVal);
    template <typename T> static T Exchange64(T volatile* pDst, T tVal);

    template <typename T> static T CompareExchange16(T volatile* pDst, T tExchange, T tComparand);
    template <typename T> static T CompareExchange(T volatile* pDst, T tExchange, T tComparand);
    template <typename T> static T CompareExchange64(T volatile* pDst, T tExchange, T tComparand);

    static void* ExchangePtr(void* volatile* ppDst, void* pVal);
    static void* CompareExchangePtr(void* volatile* ppDst, void* pExchange, void* pComparand);
};

///////////////////////////////////////////////////////////////////
#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetatomics.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetatomics.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __ATOMICS_H__
