// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MARKUP_H__
#define __MARKUP_H__

#pragma once

#include "container.h"
#include "streambuf.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUTF8
class CUTF8 : public MObject
{
public:
    // local to UTF-8
    static bool  LocalToUTF8(const CString& strIn, CCString& strOut);
    static bool  UTF8ToLocal(const CCString& strIn, CString& strOut);
};

///////////////////////////////////////////////////////////////////
// simple xml load/save
class CXMLNode;
class CXMLDocument;
class CXMLElement;
class CXMLComment;
class CXMLText;
class CXMLDeclaration;
class CXMLUnknown;
class CXMLAttribute;

///////////////////////////////////////////////////////////////////
// CXMLTraits
class CXMLTraits : public MObject
{
public:
    enum XML_TYPE
    {
        XMLT_NODE,
        XMLT_DOCUMENT,
        XMLT_ELEMENT,
        XMLT_COMMENT,
        XMLT_TEXT,
        XMLT_DECL,
        XMLT_UNKNOWN,
    };

    enum TAG_LEN
    {
        TAGL_CHAR        = 1,
        TAGL_CHAR_MAX    = 8,

        TAGL_COMMENT     = 4,
        TAGL_CDATA       = 9,
        TAGL_DECL        = 5,
        TAGL_UNKNOWN     = 2,
        TAGL_XML         = 3,

        TAGL_COMMENT_END = 3,
        TAGL_CDATA_END   = 3,
        TAGL_DECL_END    = 2,
    };

    enum ENTITY_LEN
    {
        ENTITYL_LESS     = 4,
        ENTITYL_GREATER  = 4,
        ENTITYL_AND      = 5,
        ENTITYL_SINGLE   = 6,
        ENTITYL_DOUBLE   = 6,
        ENTITYL_MIN      = 8,
        ENTITYL_MIN_CHAR = 31,
    };

    enum DECLARATION_LEN
    {
        DECLL_VERSION    = 7,
        DECLL_ENCODING   = 8,
        DECLL_STANDALONE = 10,
        DECLL_MAX        = 64,
    };
public:
    static CPCXStr CommentTag;        // <!--
    static CPCXStr CDataTag;          // <![CDATA[
    static CPCXStr DeclarationTag;    // <?xml
    static CPCXStr XMLTag;            // xml

    static CPCXStr CommentEndTag;     // -->
    static CPCXStr CDataEndTag;       // ]]>
    static CPCXStr DeclarationEndTag; // ?>

    static CPCXStr LessEntity;        // &lt;
    static CPCXStr GreaterEntity;     // &gt;
    static CPCXStr AndEntity;         // &amp;
    static CPCXStr SingleQuoteEntity; // &apos;
    static CPCXStr DoubleQuoteEntity; // &quot;

    static CPCXStr DeclDefault;       // version="1.0"
    static CPCXStr DeclVersion;       // version
    static CPCXStr DeclEncoding;      // encoding
    static CPCXStr DeclStandalone;    // standalone
    static CPCXStr EncodingDefault;   // encoding="UTF-8"
    static CPCXStr EncodingUTF8;      // UTF-8
    static CPCXStr EncodingUTF16;     // UTF-16
    static CPCXStr EncodingUTF32;     // UTF-32
    static CPCXStr EncodingUnicode;   // Unicode
    static CPCXStr EncodingCodePage;  // cp
public:
    typedef CTList<CXMLNode*>   LST_XMLNODE;
protected:
    bool   IsAlnum(UInt u) const;
    bool   IsAlpha(UInt u) const;

    bool   IsSkipChar(XChar c) const;

    bool   SkipChars(PXStr& pszBuf) const;

    bool   ToName(PXStr& pszBuf, CString& strName) const;
    bool   CheckName(PCXStr pszName) const;

    bool   ToEntity(const CString& strValue, CString& strEntity) const;
    bool   ToChars(CString& strValue) const;

    bool   CodePage(CString& strCodePage) const;
};

///////////////////////////////////////////////////////////////////
// CXMLNode
class NOVTABLE CXMLNode ABSTRACT : public CXMLTraits
{
    friend class CXMLDocument;
public:
    XML_TYPE GetType(void) const;

    template <typename T, XML_TYPE eType> T* To(void);
    template <typename T, XML_TYPE eType> const T* To(void) const;
    // element : name of the element
    // comment : the comment text
    // text    : the text
    // unknown : the tag contents
    template <size_t stLenT>
    void      GetValue(CTStringFix<CXChar, stLenT>& strFixValue) const;
    void      GetValue(CString& strValue) const;
    void      SetValue(PCXStr pszValue);

    bool      HasChild(void) const;
    bool      IsRootNode(void) const;
    bool      IsChildNode(void) const;
    // add new node at first/last, exist node just move to first/last
    bool      AddFirst(CXMLNode* pNode);
    bool      AddLast(CXMLNode* pNode);

    CXMLNode* RemoveFirst(void);
    CXMLNode* RemoveLast(void);
    bool      Remove(CXMLNode* pNode);
    void      RemoveAll(void);
    // add new node, exist node just move to new position
    bool      InsertBefore(CXMLNode* pNode, CXMLNode* pBefore);
    bool      InsertAfter(CXMLNode* pNode, CXMLNode* pAfter);
    // element : name of the element
    // comment : the comment text
    // text    : the text
    // unknown : the tag contents
    CXMLNode* Find(PCXStr pszValue, CXMLNode* pStartAfter = nullptr) const;

    CXMLDocument* GetDocument(void);
    const CXMLDocument* GetDocument(void) const;
    CXMLNode* GetParent(void);
    const   CXMLNode* GetParent(void) const;
    // first/last child node
    CXMLNode* GetFirst(void);
    const CXMLNode* GetFirst(void) const;
    CXMLNode* GetLast(void);
    const CXMLNode* GetLast(void) const;
    // prev/next sibling
    CXMLNode* GetPrev(void);
    const CXMLNode* GetPrev(void) const;
    CXMLNode* GetNext(void);
    const CXMLNode* GetNext(void) const;

    // clone only this node to aSrc document
    virtual CXMLNode* Clone(CXMLDocument& aSrc) const PURE;
    // only test this node
    virtual bool Equal(const CXMLNode& aSrc) const PURE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING) const PURE;
    virtual size_t Size(void) const PURE;
protected:
    CXMLNode(CXMLDocument* pDocument, XML_TYPE eType);
    virtual ~CXMLNode(void);
    // ...
    virtual bool ParseNode(PXStr& pszBuf);
    // destroy this node & child node & attributes
    virtual void Destroy(bool bSelfOnly = false);
private:
    CXMLNode(const CXMLNode&);
    CXMLNode& operator=(const CXMLNode&);

    bool DetectNode(PXStr& pszBuf, CXMLNode*& pNode);
protected:
    XML_TYPE        m_eType;
    CXMLDocument*   m_pDocument;
    PINDEX          m_Index;
    //
    CXMLNode*       m_pParent;
    CXMLNode*       m_pFirst;
    CXMLNode*       m_pLast;
    CXMLNode*       m_pPrev;
    CXMLNode*       m_pNext;
    //
    size_t          m_stHash;
    CString         m_strValue;
};

///////////////////////////////////////////////////////////////////
// CXMLDocument
class CXMLDocument : public CXMLNode
{
public:
    CXMLDocument(void);
    ~CXMLDocument(void);

    bool Load(CStream& stream, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);
    bool Load(CString& strSrc, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);
    bool Load(PCXStr pszSrc, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);
    // stream size & buffer should be created before invoke
    bool Save(CStream& stream, ENCODING_TYPE eENCODING = ENT_UTF8, bool bAddBOM = false);
    // stream size & buffer create by document
    bool Save(CBufWriteStream& stream, ENCODING_TYPE eENCODING = ENT_UTF8, bool bAddBOM = false);

    CXMLElement*     CreateElement(PCXStr pszName);
    CXMLComment*     CreateComment(PCXStr pszComment);
    CXMLText*        CreateText(PCXStr pszText);
    CXMLDeclaration* CreateDeclaration(PCXStr pszDeclaration = DeclDefault);
    CXMLUnknown*     CreateUnknown(PCXStr pszUnknown);
    // destroy node , all children & attributes destroyed
    bool             DestroyNode(CXMLNode* pNode);

    ENCODING_TYPE    GetEncoding(void) const;

    template <size_t stLenT>
    bool Save(CTStringFix<CXChar, stLenT>& strFixBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const;

    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE; // nullptr
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;    // false
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE; // ...
    virtual size_t Size(void) const OVERRIDE;
private:
    void RemoveNode(CXMLNode* pNode);
    void RemoveAll(void);

    bool Load(PByte pData, size_t stLen);
    bool Parse(PXStr pszBuf);
private:
    ENCODING_TYPE   m_eENCODING;
    LST_XMLNODE     m_XMLNode;
};

///////////////////////////////////////////////////////////////////
// CXMLElement
class CXMLElement : public CXMLNode
{
    friend class CXMLDocument;
public:
    template <size_t stLenT>
    void GetName(CTStringFix<CXChar, stLenT>& strFixName) const;
    void GetName(CString& strName) const;
    void SetName(PCXStr pszName);
    // pszName & pszValue = nullptr check has any attribute
    // pszName != nullptr check the attribute is exist
    // pszName & pszValue != nullptr check the exact attribute is exist
    bool HasAttribute(PCXStr pszName = nullptr, PCXStr pszValue = nullptr) const;

    template <size_t stLenT>
    bool GetAttribute(PCXStr pszName, CTStringFix<CXChar, stLenT>& strFixValue) const;
    bool GetAttribute(PCXStr pszName, CString& strValue) const;
    bool GetAttribute(PCXStr pszName, Short& sValue) const;
    bool GetAttribute(PCXStr pszName, UShort& usValue) const;
    bool GetAttribute(PCXStr pszName, Int& nValue) const;
    bool GetAttribute(PCXStr pszName, UInt& uValue) const;
    bool GetAttribute(PCXStr pszName, Long& lValue) const;
    bool GetAttribute(PCXStr pszName, ULong& ulValue) const;
    bool GetAttribute(PCXStr pszName, LLong& llValue) const;
    bool GetAttribute(PCXStr pszName, ULLong& ullValue) const;
    bool GetAttribute(PCXStr pszName, Double& dValue) const;
    // set exist attribute or add new attribute at last
    bool SetAttribute(PCXStr pszName, PCXStr pszValue);
    bool SetAttribute(PCXStr pszName, Short sValue);
    bool SetAttribute(PCXStr pszName, UShort usValue);
    bool SetAttribute(PCXStr pszName, Int nValue);
    bool SetAttribute(PCXStr pszName, UInt uValue);
    bool SetAttribute(PCXStr pszName, Long lValue);
    bool SetAttribute(PCXStr pszName, ULong ulValue);
    bool SetAttribute(PCXStr pszName, LLong llValue);
    bool SetAttribute(PCXStr pszName, ULLong ullValue);
    bool SetAttribute(PCXStr pszName, Double dValue);
    // add new attribute, exist attribute just move to first/last position
    // exist attribute change value if pszValue != nullptr
    CXMLAttribute* AddFirstAttribute(PCXStr pszName, PCXStr pszValue = nullptr);
    CXMLAttribute* AddLastAttribute(PCXStr pszName, PCXStr pszValue = nullptr);
    bool MoveAttributeToFirst(CXMLAttribute* pAttr);
    bool MoveAttributeToLast(CXMLAttribute* pAttr);

    bool RemoveFirstAttribute(void);
    bool RemoveLastAttribute(void);
    bool RemoveAttribute(PCXStr pszName);
    bool RemoveAttribute(CXMLAttribute* pAttr);
    void RemoveAllAttribute(void);
    // add new attribute, exist attribute just move to new position
    // pszValue can be nullptr
    bool InsertAttributeBefore(PCXStr pszName, PCXStr pszValue, CXMLAttribute* pBefore);
    bool InsertAttributeAfter(PCXStr pszName, PCXStr pszValue, CXMLAttribute* pAfter);
    bool InsertAttributeBefore(CXMLAttribute* pAttr, CXMLAttribute* pBefore);
    bool InsertAttributeAfter(CXMLAttribute* pAttr, CXMLAttribute* pAfter);

    CXMLAttribute* FindAttribute(PCXStr pszName, CXMLAttribute* pStartAfter = nullptr) const;

    CXMLAttribute* GetFirstAttribute(void);
    const   CXMLAttribute* GetFirstAttribute(void) const;
    CXMLAttribute* GetLastAttribute(void);
    const   CXMLAttribute* GetLastAttribute(void) const;

    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE;
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
protected:
    explicit CXMLElement(CXMLDocument* pDocument);
    virtual ~CXMLElement(void);

    CXMLAttribute* AddAttributeAtFirst(PCXStr pszName, PCXStr pszValue = nullptr);
    CXMLAttribute* AddAttributeAtLast(PCXStr pszName, PCXStr pszValue = nullptr);

    void RemoveAttributeList(CXMLAttribute* pAttr);
    bool ParseAttributes(PXStr& pszBuf);
    // ...
    virtual bool ParseNode(PXStr& pszBuf) OVERRIDE;
    virtual void Destroy(bool bSelfOnly = false) OVERRIDE;
private:
    CXMLAttribute*   m_pFirstAttribute;
    CXMLAttribute*   m_pLastAttribute;
};

///////////////////////////////////////////////////////////////////
// CXMLComment
class CXMLComment : public CXMLNode
{
    friend class CXMLDocument;
public:
    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE;
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
protected:
    explicit CXMLComment(CXMLDocument* pDocument);
    virtual ~CXMLComment(void);
    // ...
    virtual bool ParseNode(PXStr& pszBuf) OVERRIDE;
};

///////////////////////////////////////////////////////////////////
// CXMLText
class CXMLText : public CXMLNode
{
    friend class CXMLDocument;
public:
    bool   IsCData(void) const;
    void   SetCData(bool bCData);

    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE;
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
protected:
    explicit CXMLText(CXMLDocument* pDocument);
    virtual ~CXMLText(void);
    // ...
    virtual bool ParseNode(PXStr& pszBuf) OVERRIDE;
private:
    bool   m_bCData;
};

///////////////////////////////////////////////////////////////////
// CXMLDeclaration
class CXMLDeclaration : public CXMLNode
{
    friend class CXMLDocument;
public:
    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE;
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;

    template <size_t stLenT>
    void    GetVersion(CTStringFix<CXChar, stLenT>& strFixVersion) const;
    void    GetVersion(CString& strVersion) const;
    void    SetVersion(PCXStr pszVersion);

    template <size_t stLenT>
    void    GetEncoding(CTStringFix<CXChar, stLenT>& strFixEncoding) const;
    void    GetEncoding(CString& strEncoding) const;
    void    SetEncoding(PCXStr pszEncoding);

    template <size_t stLenT>
    void    GetStandalone(CTStringFix<CXChar, stLenT>& strFixStandalone) const;
    void    GetStandalone(CString& strStandalone) const;
    void    SetStandalone(PCXStr pszStandalone);
protected:
    explicit CXMLDeclaration(CXMLDocument* pDocument);
    virtual ~CXMLDeclaration(void);
    // ...
    virtual bool ParseNode(PXStr& pszBuf) OVERRIDE;
private:
    void    ParseAttributes(PXStr& pszBuf, PCXStr pszName, size_t stNameLen, CString& strAttribute);
private:
    CString   m_strVersion;
    CString   m_strEncoding;
    CString   m_strStandalone;
};

///////////////////////////////////////////////////////////////////
// CXMLUnknown
class CXMLUnknown : public CXMLNode
{
    friend class CXMLDocument;
public:
    bool   IsDtd(void) const;
    void   SetDtd(bool bDtd);

    virtual CXMLNode* Clone(CXMLDocument& aSrc) const OVERRIDE;
    virtual bool Equal(const CXMLNode& aSrc) const OVERRIDE;
    virtual void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
protected:
    explicit CXMLUnknown(CXMLDocument* pDocument);
    virtual ~CXMLUnknown(void);
    // ...
    virtual bool ParseNode(PXStr& pszBuf) OVERRIDE;
private:
    bool   m_bDtd;
};

///////////////////////////////////////////////////////////////////
// CXMLAttribute
class CXMLAttribute : public CXMLTraits
{
    friend class CXMLElement;
public:
    CXMLElement* GetElement(void);
    const CXMLElement* GetElement(void) const;

    template <size_t stLenT>
    bool GetName(CTStringFix<CXChar, stLenT>& strFixName) const;
    bool GetName(CString& strName) const;
    template <size_t stLenT>
    bool GetValue(CTStringFix<CXChar, stLenT>& strFixValue) const;
    bool GetValue(CString& strValue) const;
    bool SetValue(PCXStr pszValue);

    bool GetValue(Short& sValue) const;
    bool GetValue(UShort& usValue) const;
    bool GetValue(Int& nValue) const;
    bool GetValue(UInt& uValue) const;
    bool GetValue(Long& lValue) const;
    bool GetValue(ULong& ulValue) const;
    bool GetValue(LLong& llValue) const;
    bool GetValue(ULLong& ullValue) const;
    bool GetValue(Double& dValue) const;
    
    bool SetValue(Short sValue);
    bool SetValue(UShort usValue);
    bool SetValue(Int nValue);
    bool SetValue(UInt uValue);
    bool SetValue(Long lValue);
    bool SetValue(ULong ulValue);
    bool SetValue(LLong llValue);
    bool SetValue(ULLong ullValue);
    bool SetValue(Double dValue);

    const CXMLAttribute* Prev(void) const;
    const CXMLAttribute* Next(void) const;

    void Save(CString& strBuf, ENCODING_TYPE eENCODING = ENT_UTF8) const;
    size_t Size(void) const;
protected:
    CXMLAttribute(CXMLElement* pElement, PCXStr pszName, PCXStr pszValue);
    virtual ~CXMLAttribute(void);
private:
    CXMLAttribute(const CXMLAttribute&);
    CXMLAttribute& operator=(const CXMLAttribute&);
protected:
    CXMLElement*     m_pElement;
    size_t           m_stName;
    size_t           m_stValue;
    CXMLAttribute*   m_pPrev;
    CXMLAttribute*   m_pNext;

    CString          m_strName;
    CString          m_strValue;
};

///////////////////////////////////////////////////////////////////
// simple json load/save
class CKVNode;
class CKVStore;

///////////////////////////////////////////////////////////////////
// CKVTraits
class CKVTraits : public MObject
{
public:
    enum TOKEN_TYPE
    {
        TOKEN_TYPE_NONE,
        TOKEN_TYPE_INTEGER,
        TOKEN_TYPE_FLOAT,
        TOKEN_TYPE_TRUE,
        TOKEN_TYPE_FALSE,
        TOKEN_TYPE_STRING,
        TOKEN_TYPE_ARRAY,
        TOKEN_TYPE_OBJECT,
        TOKEN_TYPE_END,
        TOKEN_TYPE_ERROR,
    };

    enum TOKEN_LEN
    {
        TOKEN_LEN_DEFAULT = 20,
        TOKEN_LEN_CHAR    = 1,
        TOKEN_LEN_STRING  = 2,
        TOKEN_LEN_ARRAY   = 3,
        TOKEN_LEN_OBJECT  = 3,
        TOKEN_LEN_NAMEMIN = 3,
        TOKEN_LEN_NAME    = 6,
    };
    typedef CTMap<Int, CKVNode>               MAP_ARRAY, *PMAP_ARRAY;
    typedef CTMap<Int, CKVNode>::PAIR         PAIR_ARRAY;
    typedef CTMap<CString, CKVNode>           MAP_OBJECT, *PMAP_OBJECT;
    typedef CTMap<CString, CKVNode>::PAIR     PAIR_OBJECT;

    typedef union tagVALUE
    {
        LLong         llValue;
        ULLong        ullValue;
        Double        dValue;
        bool          bValue;

        CString*      pString;
        PMAP_ARRAY    pArray;
        PMAP_OBJECT   pObject;
    }VALUE;
protected:
    bool   IsAlnum(UInt u) const;
    bool   IsAlpha(UInt u) const;

    bool   IsSkipChar(XChar c) const;
    bool   IsSkipCharWithComma(XChar c) const;

    bool   SkipChars(PXStr& pszBuf, bool SkipComma = false) const;
};

///////////////////////////////////////////////////////////////////
// CKVNode
class CKVNode : public CKVTraits
{
public:
    static CKVNode const ms_None;
public:
    CKVNode(VAR_TYPE eType = VART_NONE);
    CKVNode(Short sValue);
    CKVNode(UShort usValue);
    CKVNode(Int nValue);
    CKVNode(UInt uValue);
    CKVNode(Long lValue);
    CKVNode(ULong ulValue);
    CKVNode(LLong llValue);
    CKVNode(ULLong ullValue);
    CKVNode(Double dValue);
    CKVNode(bool bValue);
    CKVNode(PCXStr pszValue);
    CKVNode(const CString& strValue);
    CKVNode(const CKVNode& aSrc);

    CKVNode& operator=(VAR_TYPE eType);
    CKVNode& operator=(Short sValue);
    CKVNode& operator=(UShort usValue);
    CKVNode& operator=(Int nValue);
    CKVNode& operator=(UInt uValue);
    CKVNode& operator=(Long lValue);
    CKVNode& operator=(ULong ulValue);
    CKVNode& operator=(LLong llValue);
    CKVNode& operator=(ULLong ullValue);
    CKVNode& operator=(Double dValue);
    CKVNode& operator=(bool bValue);
    CKVNode& operator=(PCXStr pszValue);
    CKVNode& operator=(const CString& strValue);
    CKVNode& operator=(const CKVNode& aSrc);
    
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CKVNode(CKVNode&& aSrc);
    CKVNode& operator=(CKVNode&& aSrc);
#endif
    ~CKVNode(void);

    ENCODING_TYPE GetEncoding(void) const;

    VAR_TYPE GetType(void) const;
    Int      GetSize(void) const; // array or object size, others -1

    Short    GetShort(void) const;
    UShort   GetUShort(void) const;
    Int      GetInt(void) const;
    UInt     GetUInt(void) const;
    Long     GetLong(void) const;
    ULong    GetULong(void) const;
    LLong    GetLLong(void) const;
    ULLong   GetULLong(void) const;
    Double   GetDouble(void) const;
    bool     GetBoolean(void) const;
    bool     GetString(CString& strValue) const;
    CString  GetString(void);

    bool     SetValue(VAR_TYPE eType);
    bool     SetValue(Short sValue);
    bool     SetValue(UShort usValue);
    bool     SetValue(Int nValue);
    bool     SetValue(UInt uValue);
    bool     SetValue(Long lValue);
    bool     SetValue(ULong ulValue);
    bool     SetValue(LLong llValue);
    bool     SetValue(ULLong ullValue);
    bool     SetValue(Double dValue);
    bool     SetValue(bool bValue);
    bool     SetValue(PCXStr pszValue);
    bool     SetValue(const CString& strValue);

    bool     SetValue(const CKVNode& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool     SetValue(CKVNode&& aSrc);
    bool     SetValue(CString&& strValue);
#endif
    // return a default none node if nIndex does not exist
    // use SetValue to set new value
    CKVNode& operator[](Int nIndex);
    const CKVNode& operator[](Int nIndex) const;

    // return a default none node if pszName does not exist
    // use SetValue to set new value
    CKVNode& operator[](PCXStr pszName);
    const CKVNode& operator[](PCXStr pszName) const;

    CKVNode& Add(const CKVNode& aSrc); // array
    CKVNode& Add(PCXStr pszName, const CKVNode& aSrc); // object
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CKVNode& Add(CKVNode&& aSrc);
    CKVNode& Add(PCXStr pszName, CKVNode&& aSrc);
#endif

    bool   Remove(Int nIndex);
    bool   Remove(PCXStr pszName);
    void   Remove(void);
    // index is exist
    bool   Find(Int nIndex) const;
    // key name is exist
    bool   Find(PCXStr pszName) const;

    PINDEX First(void) const;
    PINDEX Last(void) const;

    bool   Next(CString& strName, PINDEX& index) const;
    bool   Prev(CString& strName, PINDEX& index) const;

    bool   Load(CStream& stream, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);
    bool   Load(CString& strSrc, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);
    bool   Load(PCXStr pszSrc, size_t stSize = 0, ENCODING_TYPE eENCODING = ENT_UTF8);

    // stream size & buffer should be created before invoke
    bool   Save(CStream& stream, bool bStyle = true, ENCODING_TYPE eENCODING = ENT_UTF8, bool bAddBOM = false);
    // stream size & buffer create by save
    bool   Save(CBufWriteStream& stream, bool bStyle = true, ENCODING_TYPE eENCODING = ENT_UTF8, bool bAddBOM = false);
    bool   Save(CString& strBuf, bool bStyle = true);
protected:
    // parse
    bool   Parse(PXStr& pszBuf);
    bool   ParseComment(PXStr& pszBuf, bool SkipComma = false);
    TOKEN_TYPE ParseToken(PXStr& pszBuf);

    bool   ParseArray(PXStr& pszBuf);
    bool   ParseObject(PXStr& pszBuf);

    bool   Load(PByte pData, size_t stLen);
    void   Save(Int nIndent, CString& strBuf) const;
    size_t Size(Int nIndent) const;
private:
    VAR_TYPE        m_eType;
    ENCODING_TYPE   m_eENCODING;
    VALUE           m_Value;
};

///////////////////////////////////////////////////////////////////
#include "markup.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetmarkup.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetmarkup.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __MARKUP_H__
