// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_HTTP_TLS_INL__
#define __NETWORK_HTTP_TLS_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CHTTPSSession
INLINE CHTTPSSession::CHTTPSSession(CNETTraits::Socket sSocket)
: CHTTPSession(sSocket, WEB_SESSION_HTTP_TLS)
{
}

INLINE CHTTPSSession::CHTTPSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef)
: CHTTPSession(sSocket, WEB_SESSION_HTTP_TLS, EventHandlerRef)
{
}

INLINE CHTTPSSession::~CHTTPSSession(void)
{
}

///////////////////////////////////////////////////////////////////
// CWSSSession
INLINE CWSSSession::CWSSSession(CNETTraits::Socket sSocket)
: CWSSession(sSocket, WEB_SESSION_WS_TLS)
{
}

INLINE CWSSSession::CWSSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef)
: CWSSession(sSocket, WEB_SESSION_WS_TLS, EventHandlerRef)
{
}

INLINE CWSSSession::~CWSSSession(void)
{
}

///////////////////////////////////////////////////////////////////
// CWEBTLSServer
INLINE CWEBTLSServer::CWEBTLSServer(void)
: m_stMaxBlock(CWEBSession::WEB_DEF_SIZE_BLOCK)
, m_stMaxCache(CWEBSession::WEB_DEF_SIZE_CACHE)
, m_bManage(false)
, m_bNetBuffer(false)
, m_sSocket(0)
{
}

INLINE CWEBTLSServer::~CWEBTLSServer(void)
{
    Exit();
}

INLINE bool CWEBTLSServer::Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, CEventHandler& EventHandlerRef, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_NetworkTLSPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        Param.bClientMode = false;
        if (CNetworkTLS::StaticCreate(m_NetworkTLSPtr, Param) == false)
        {
            return false;
        }
        if (m_NetworkTLSPtr->Init(this) == false)
        {
            return false;
        }
        m_bManage = bManage;
        m_EventHandlerPtr = &EventHandlerRef;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_EventHandlerPtr != nullptr);
    }
    return false;
}

INLINE void CWEBTLSServer::Exit(void)
{
    CSyncLockScope scope(m_SessionLock);
    m_NetAddr.Reset();
    if (m_NetworkTLSPtr != nullptr)
    {
        m_NetworkTLSPtr->Exit();
        m_NetworkTLSPtr = nullptr;
    }
    if (m_NetworkPtr != nullptr)
    {
        if (m_sSocket != 0)
        {
            m_NetworkPtr->Destroy(m_sSocket);
            m_sSocket = 0;
        }
        if (m_bManage) 
        {
            m_NetworkPtr->Exit();
        }
        else
        {
            for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
            {
                PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
                m_NetworkPtr->Destroy(pPair->m_K);
            }
        }
        m_NetworkPtr = nullptr;
        m_Session.RemoveAll();
    }
    if (m_EventHandlerPtr != nullptr)
    {
        m_EventHandlerPtr = nullptr;
    }
    m_bNetBuffer = false;
}

INLINE bool CWEBTLSServer::Check(void)
{
    if ((m_sSocket == 0) && (m_NetworkPtr != nullptr))
    {
        return ListenHandle();
    }
    return true;
}

INLINE bool CWEBTLSServer::Listen(PCXStr pszIp, UShort usPort)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        m_NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszIp, usPort, m_NetAddr);
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBTLSServer::Listen(STR_ADDR& strAddr)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        m_NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(*strAddr.strIp, strAddr.usPort, m_NetAddr);
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBTLSServer::Listen(NET_ADDR& NetAddr)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddr.usAttr & ATTR_IPV6));
        m_NetAddr = NetAddr;
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBTLSServer::Send(Socket sSocket, PCStr pszData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, (PByte)pszData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSServer::Send(Socket sSocket, const CCString& strData)
{
    assert(strData.Length() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(strData.Length(), (PByte)strData.GetBuffer());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSServer::Send(Socket sSocket, PByte pData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, pData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSServer::Send(Socket sSocket, const CBufStream& Stream)
{
    assert(Stream.Size() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(Stream.Size(), Stream.GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSServer::Send(Socket sSocket, CStreamScopePtr& StreamPtr)
{
    assert(StreamPtr != nullptr);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(StreamPtr->Tell(), StreamPtr->GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSServer::Destory(Socket sSocket)
{
    if (m_NetworkPtr != nullptr)
    {
        return m_NetworkPtr->Destroy(sSocket);
    }
    return false;
}

INLINE void CWEBTLSServer::SetHash(Int nHashSize)
{
    m_Session.SetHash(nHashSize);
    if (m_NetworkTLSPtr != nullptr)
    {
        m_NetworkTLSPtr->SetHash(nHashSize);
    }
}

INLINE bool CWEBTLSServer::IsNetBufferMode(void) const
{
    return m_bNetBuffer;
}

INLINE void CWEBTLSServer::SetNetBufferMode(void)
{
    m_bNetBuffer = true;
}

INLINE size_t CWEBTLSServer::GetMaxBlockSize(void) const
{
    return m_stMaxBlock;
}

INLINE void CWEBTLSServer::SetMaxBlockSize(size_t stMaxBlock)
{
    m_stMaxBlock = stMaxBlock;
}

INLINE size_t CWEBTLSServer::GetMaxCacheSize(void) const
{
    return m_stMaxCache;
}

INLINE void CWEBTLSServer::SetMaxCacheSize(size_t stMaxCache)
{
    m_stMaxCache = stMaxCache;
}

INLINE CNetworkPtr& CWEBTLSServer::GetNetwork(void)
{
    return m_NetworkPtr;
}

INLINE const CNetworkPtr& CWEBTLSServer::GetNetwork(void) const
{
    return m_NetworkPtr;
}

INLINE CEventHandlerPtr& CWEBTLSServer::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CWEBTLSServer::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE bool CWEBTLSServer::OnTcpAccept(Socket sAccept, Socket sListen)
{
    if (m_EventHandlerPtr->OnHandle(EVENT_TCP_ACCEPT, sAccept, sListen) == RET_OKAY)
    {
        return CreateSession(sAccept);
    }
    return false;
}

INLINE bool CWEBTLSServer::OnTcpClose(Socket sSocket, ULLong ullLiveData)
{
    if (sSocket != m_sSocket)
    {
        CWEBSessionPtr SessionPtr;
        if (RemoveSession(sSocket, SessionPtr))
        {
            CloseHandle(SessionPtr);
        }
        m_EventHandlerPtr->OnHandle(EVENT_TCP_CLOSE, sSocket, ullLiveData);
    }
    else
    {
        m_sSocket = 0;
    }
    return true;
}

INLINE bool CWEBTLSServer::OnTlsHandShake(UInt uError, Socket sSocket)
{
    return (m_EventHandlerPtr->OnHandle(EVENT_TLS_HANDSHAKE, uError, sSocket) == RET_OKAY);
}

INLINE bool CWEBTLSServer::OnTlsRecv(size_t stSize, PTCP_PARAM pTcp)
{
    bool bComplete = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(pTcp->sSocket, SessionPtr))
    {
        if (IsNetBufferMode() == false)
        {
            bComplete = OnData(SessionPtr, stSize, pTcp);
        }
        else
        {
            bComplete = OnDataNetBuffer(SessionPtr, stSize, pTcp);
        }
        if (bComplete == false)
        {
            ErrorHandle(SessionPtr);
        }
    }
    return bComplete;
}

INLINE bool CWEBTLSServer::OnTlsSend(size_t stSize, PTCP_PARAM pTcp)
{
    bool   bRet   = false;
    PByte  pData  = pTcp->pData;
    size_t stData = 0;
    size_t stSend = 0;
    while (stData < stSize)
    {
        CStreamScopePtr StreamPtr;
        bRet = m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            stSend = DEF::Min<size_t>(StreamPtr->Size(), (stSize - stData));
            bRet = (StreamPtr->Write(pData, stSend) == stSend);
        }
        if (bRet)
        {
            stData += stSend;
            bRet = m_NetworkPtr->Send(pTcp->sSocket, StreamPtr);
        }
        if (bRet == false)
        {
            break;
        }
    }
    return bRet;
}

INLINE bool CWEBTLSServer::OnTlsError(UInt uError, Socket sSocket)
{
    m_EventHandlerPtr->OnHandle(EVENT_TLS_ERROR, uError, sSocket);
    return true;
}

INLINE bool CWEBTLSServer::OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    PByte  pData     = pTcp->pData;
    while (SessionPtr->OnData(pData, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBTLSServer::OnData RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBTLSServer::OnData stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBTLSServer::OnData stData = %d, stSize = %d"), stData, stSize);
        pData  += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBTLSServer::OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    while (SessionPtr->OnData(m_NetAttr, pTcp, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBTLSServer::OnDataNetBuffer RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBTLSServer::OnDataNetBuffer stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBTLSServer::OnDataNetBuffer stData = %d, stSize = %d"), stData, stSize);
        pTcp->pData += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBTLSServer::CreateSession(Socket sSocket)
{
    CHTTPSSessionPtr SessionPtr = MNEW CHTTPSSession(sSocket);
    if (SessionPtr != nullptr)
    {
        SessionPtr->SetMaxBlockSize(m_stMaxBlock);
        SessionPtr->SetMaxCacheSize(m_stMaxCache);
        if (m_bNetBuffer)
        {
            SessionPtr->GetParser().SetNetBufferClone();
        }
        CSyncLockScope scope(m_SessionLock);
        return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
    }
    return false;
}

INLINE bool CWEBTLSServer::DestroySession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return m_Session.Remove(sSocket);
}

INLINE bool CWEBTLSServer::CheckSession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return (m_Session.FindIndex(sSocket) != nullptr);
}

INLINE bool CWEBTLSServer::GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    CSyncLockScope scope(m_SessionLock);
    PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
    if (pPair != nullptr)
    {
        SessionPtr = pPair->m_V;
        return true;
    }
    return false;
}

INLINE bool CWEBTLSServer::RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    if (m_NetAddr.IsValid())
    {
        CSyncLockScope scope(m_SessionLock);
        PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
        if (pPair != nullptr)
        {
            SessionPtr = pPair->m_V;
            return m_Session.RemoveAt(reinterpret_cast<PINDEX>(pPair));
        }
    }
    return false;
}

INLINE bool CWEBTLSServer::ListenHandle(void)
{
    if (m_NetAddr.IsValid())
    {
        m_sSocket = m_NetworkPtr->Create(*m_NetworkTLSPtr, m_NetAddr);
        if (m_sSocket != 0)
        {
            m_NetworkPtr->SetAttr(m_sSocket, TRUE, SOCKET_RECV_EVENT);
            m_NetworkPtr->SetAttr(m_sSocket, TRUE, SOCKET_EXCLUDE_HEAD);
            m_NetworkTLSPtr->Attach(m_sSocket);
            return m_NetworkPtr->Listen(m_sSocket);
        }
    }
    return false;
}

INLINE bool CWEBTLSServer::SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs)
{
    return m_NetworkTLSPtr->Send(SessionPtr->GetSocket(), brs);
}

INLINE bool CWEBTLSServer::RecvHandle(CWEBSessionPtr& SessionPtr)
{
    bool bRet = UpgradeHandle(SessionPtr);
    if (bRet == false)
    {
        return false;
    }
    if (SessionPtr->CheckDataCompleted())
    {
        bRet = CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return bRet;
}

INLINE bool CWEBTLSServer::CloseHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->CheckDataCompleted())
    {
        SessionPtr->SetStatus((UInt)RET_ERROR); // closed
        CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBTLSServer::ErrorHandle(CWEBSessionPtr& SessionPtr)
{
    if ((SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS_TLS) && 
        (SessionPtr->GetStatus() != 0) &&
        (SessionPtr->ApplyHandle() != false))
    {
        CPCStr pszStatus = CWSTraits::StatusToString((Int)SessionPtr->GetStatus());
        size_t stSize    = CChar::Length(pszStatus);
        size_t stPayload = stSize + sizeof(UShort);
        if (stPayload > CWSTraits::WS_PAY_LOAD)
        {
            stPayload = CWSTraits::WS_PAY_LOAD;
            stSize    = stPayload - sizeof(UShort);
        }
        CBufWriteStream bws((stPayload + CWSTraits::WS_NORMAL_LEN));
        // code = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE) | (UShort)(stPayload)
        // head = CWSTraits::WS_NORMAL_LEN
        CWSParser::SetFrameHead(bws.GetBuf(), (UShort)((CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE)|stPayload), 0, CWSTraits::WS_NORMAL_LEN, stPayload);
        bws.Seek(CWSTraits::WS_NORMAL_LEN);
#ifndef __ARCH_TARGET_BIGENDIAN__
        bws << CPlatform::ByteSwap((UShort)SessionPtr->GetStatus());
#else
        bws << (UShort)SessionPtr->GetStatus();
#endif
        bws.Write((PByte)pszStatus, stSize);
        CBufReadStream brs(bws.Tell(), bws.GetBuf());
        SendHandle(SessionPtr, brs);

        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBTLSServer::CompleteHandle(CWEBSessionPtr& SessionPtr)
{
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = SessionPtr->GetSocket();
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_TCP;
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP_TLS)
    {
        CHTTPSSession* pSession = static_cast<CHTTPSSession*>(SessionPtr.Get());
        Param.nSize = pSession->GetParser().GetCacheSize();
        Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
        return (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS_TLS);
        CWSSSession* pSession = static_cast<CWSSSession*>(SessionPtr.Get());
        Int nRet = RET_OKAY;
        if (pSession->GetControlCheck())
        {
            nRet = CompleteWSCheck(pSession, SessionPtr);
        }
        if (nRet == RET_OKAY)
        {
            Param.nSize = pSession->GetParser().GetFrameSize();
            Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
            return (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
        }
        return (nRet != CWSTraits::WS_CLOSE_FRAME);
    }
}

INLINE Int CWEBTLSServer::CompleteWSCheck(CWSSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSParser& Parser = pSession->GetParser();
    if (Parser.IsControlFrame())
    {
        DEV_DEBUG(TF("CWEBTLSServer::CompleteWSCheck opcode = %x"), (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK));
        if (SessionPtr->GetStatus() == (UInt)RET_ERROR)
        {
            return CWSTraits::WS_CLOSE_FRAME;
        }
        Int nFrame = 0;
        switch (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK)
        {
        case CWSTraits::WS_CLOSE:
            {
                if (pSession->GetCloseCheck())
                {
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE;
                }
            }
            break;
        case CWSTraits::WS_PING:
            {
                if (pSession->GetPingCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
            }
            break;
        case CWSTraits::WS_PONG:
            {
                if (pSession->GetPongCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
            }
            break;
        default:{}
        }
        DEV_DEBUG(TF("CWEBTLSServer::CompleteWSCheck ret opcode = %x"), nFrame);
        if (nFrame != 0)
        {
            Int nPayload = Parser.GetPayloadSize();
            if (nPayload > CWSTraits::WS_PAY_LOAD)
            {
                nPayload = CWSTraits::WS_PAY_LOAD;
            }
            CBufWriteStream bws((nPayload + CWSTraits::WS_NORMAL_LEN));
            // code = nFrame | nPayload;
            // head = CWSTraits::WS_NORMAL_LEN
            CWSParser::SetFrameHead(bws.GetBuf(), (UShort)(nFrame|nPayload), 0, CWSTraits::WS_NORMAL_LEN, (size_t)nPayload);
            bws.Seek(CWSTraits::WS_NORMAL_LEN);
            if (nPayload > 0)
            {
                bws.Write(Parser.GetCachePayload(), (size_t)nPayload);
            }
            CBufReadStream brs(bws.Tell(), bws.GetBuf());
            DEV_DEBUG(TF("CWEBTLSServer::CompleteWSCheck ret stream size = %d"), brs.Size());
            SendHandle(SessionPtr, brs);
            return nFrame;
        }
    }
    return RET_OKAY;
}

INLINE bool CWEBTLSServer::UpgradeHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->ApplyHandle() == false)
    {
        return false;
    }
    bool bRet = true;
    if (SessionPtr->CheckUpgrade() == false)
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP_TLS);
        CHTTPSSession* pSession = static_cast<CHTTPSSession*>(SessionPtr.Get());
        CHTTPRequest Request;
        pSession->GetParser().To(Request, true);

        //SetStatus(SWITCHING_PROTOCOLS) after set http-parser->from(custom CHTTPResponse)
        if (CWSParser::CheckUpgrade(Request, bRet))
        {
            bRet = UpgradeWSSession(Request, pSession, SessionPtr);
        }
        // else if (CHTTP2Parser::CheckUpgrade(Request))
        // {
        //     bRet = UpgradeH2CSession(Request, pSession, SessionPtr);
        // }
        if (bRet)
        {
            SessionPtr->SetUpgraded();
        }
    }
    SessionPtr->ApplyHandle(false);
    return bRet;
}

INLINE bool CWEBTLSServer::UpgradeWSSession(const CHTTPRequest& Request, CHTTPSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSSessionPtr WSSessionPtr;

    bool bRet = (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_BEFORE_UPGRADE, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    if (bRet)
    {   // https session --> wss-session
        bRet = false;
        WSSessionPtr = MNEW CWSSSession(SessionPtr->GetSocket(), pSession->GetEventHandler());
        if (WSSessionPtr != nullptr)
        {
            WSSessionPtr->SetMaxBlockSize(m_stMaxBlock);
            WSSessionPtr->SetMaxCacheSize(m_stMaxCache);
            WSSessionPtr->SetPingCheck();
            if (m_bNetBuffer)
            {
                WSSessionPtr->GetParser().SetNetBufferClone();
            }

            CSyncLockScope scope(m_SessionLock);
            PAIR_WEBSESSION* pPair = m_Session.Find(pSession->GetSocket());
            if (pPair != nullptr)
            {
                pPair->m_V = WSSessionPtr;
                bRet = true;
            }
        }
    }
    if (bRet)
    {   // response
        if (pSession->GetStatus() != CHTTPTraits::SWITCHING_PROTOCOLS)
        {
            CHTTPResponse Response(CHTTPTraits::HTTP_CONTENT_NONE);
            CWSParser::UpgradeResponse(Request, Response);
            bRet = pSession->GetParser().From(Response);
        }
        CBufReadStream brs;
        if (bRet)
        {
            brs.Attach(pSession->GetParser().GetCacheSize(), pSession->GetParser().GetCacheBuf());
            bRet = SendHandle(SessionPtr, brs);
        }
        SessionPtr = WSSessionPtr;
        bRet = (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_AFTER_UPGRADE, reinterpret_cast<uintptr_t>(SessionPtr.Get()), (ULLong)SessionPtr->GetType()) == RET_OKAY);
    }
    return bRet;
}

///////////////////////////////////////////////////////////////////
// CWEBTLSClient
INLINE CWEBTLSClient::CWEBTLSClient(void)
: m_stMaxBlock(CWEBSession::WEB_DEF_SIZE_BLOCK)
, m_stMaxCache(CWEBSession::WEB_DEF_SIZE_CACHE)
, m_bManage(false)
, m_bExited(false)
, m_bNetBuffer(false)
, m_bAutoCheck(false)
, m_uControlCode(CWSTraits::WS_PING)
{
}

INLINE CWEBTLSClient::~CWEBTLSClient(void)
{
    Exit();
}

INLINE bool CWEBTLSClient::Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_NetworkTLSPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        Param.bClientMode = true;
        if (CNetworkTLS::StaticCreate(m_NetworkTLSPtr, Param) == false)
        {
            return false;
        }
        if (m_NetworkTLSPtr->Init(this) == false)
        {
            return false;
        }
        m_bManage = bManage;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_NetworkPtr != nullptr);
    }
    return false;
}

INLINE bool CWEBTLSClient::Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, CEventHandler& EventHandlerRef, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_NetworkTLSPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        Param.bClientMode = true;
        if (CNetworkTLS::StaticCreate(m_NetworkTLSPtr, Param) == false)
        {
            return false;
        }
        if (m_NetworkTLSPtr->Init(this) == false)
        {
            return false;
        }
        m_bManage = bManage;
        m_EventHandlerPtr = &EventHandlerRef;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_EventHandlerPtr != nullptr);
    }
    return false;
}

INLINE void CWEBTLSClient::Exit(void)
{
    CSyncLockScope scope(m_SessionLock);
    m_bExited = true;
    if (m_NetworkTLSPtr != nullptr)
    {
        m_NetworkTLSPtr->Exit();
        m_NetworkTLSPtr = nullptr;
    }
    if (m_NetworkPtr != nullptr)
    {
        if (m_bManage) 
        {
            m_NetworkPtr->Exit();
        }
        else
        {
            for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
            {
                PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
                m_NetworkPtr->Destroy(pPair->m_K);
            }
        }
        m_NetworkPtr = nullptr;
        m_Session.RemoveAll();
    }
    if (m_EventHandlerPtr != nullptr)
    {
        m_EventHandlerPtr = nullptr;
    }
    m_bNetBuffer   = false;
    m_bAutoCheck   = false;
    m_uControlCode = CWSTraits::WS_PING;
}

INLINE bool CWEBTLSClient::Check(void)
{
    if (m_bAutoCheck == false)
    {
        return true;
    }
    ARY_WEBSESSION LiveChecks;
    if (m_Session.GetSize() > 0)
    {
        CSyncLockScope scope(m_SessionLock);
        for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
        {
            PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
            if (pPair->m_V->GetType() == CWEBSession::WEB_SESSION_WS_TLS)
            {
                CWSSSession* pSession = static_cast<CWSSSession*>(pPair->m_V.Get());
                if (pSession->IsLiveCheck())
                {
                    LiveChecks.Add(pPair->m_V);
                }
            }
        }
    }
    if (LiveChecks.GetSize() > 0)
    {
        UShort usaFrame[3] = { 0 };
#ifndef __ARCH_TARGET_BIGENDIAN__
        usaFrame[0] = CPlatform::ByteSwap((UShort)(m_uControlCode|CWSTraits::WS_MASK));
#else
        usaFrame[0] = (UShort)(m_uControlCode|CWSTraits::WS_MASK);
#endif
        *((PUInt)(usaFrame + 1)) = (UInt)CPlatform::GetRunningTime();
        CBufReadStream brs(sizeof(UShort) * 3, (PByte)usaFrame);
        for (int i = 0; i < LiveChecks.GetSize(); ++i)
        {
            brs.Seek(0);
            SendHandle(LiveChecks[i], brs);
        }
    }
    return true;
}

INLINE bool CWEBTLSClient::Create(Socket& sSocket, UShort usLocalPort, PCXStr pszLocalAddr)
{
    if (m_NetworkPtr != nullptr)
    {
        NET_ADDR NetAddr;
        NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszLocalAddr, usLocalPort, NetAddr);
        return CreateSession(sSocket, NetAddr);
    }
    return false;
}

INLINE bool CWEBTLSClient::Create(Socket& sSocket, CEventHandler& EventHandlerRef, UShort usLocalPort, PCXStr pszLocalAddr)
{
    if (m_NetworkPtr != nullptr)
    {
        NET_ADDR NetAddr;
        NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszLocalAddr, usLocalPort, NetAddr);
        return CreateSession(sSocket, NetAddr, EventHandlerRef);
    }
    return false;
}

INLINE bool CWEBTLSClient::Create(Socket& sSocket, NET_ADDR& NetAddrRef)
{
    if ((m_NetworkPtr != nullptr) && (m_EventHandlerPtr != nullptr))
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddrRef.usAttr & ATTR_IPV6));
        return CreateSession(sSocket, NetAddrRef);
    }
    return false;
}

INLINE bool CWEBTLSClient::Create(Socket& sSocket, CEventHandler& EventHandlerRef, NET_ADDR& NetAddrRef)
{
    if (m_NetworkPtr != nullptr)
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddrRef.usAttr & ATTR_IPV6));
        return CreateSession(sSocket, NetAddrRef, EventHandlerRef);
    }
    return false;
}

INLINE bool CWEBTLSClient::Destory(Socket sSocket)
{
    if (m_NetworkPtr != nullptr)
    {
        return m_NetworkPtr->Destroy(sSocket);
    }
    return false;
}

INLINE bool CWEBTLSClient::Connect(Socket sSocket, PCXStr pszIp, UShort usPort)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, usPort, pszIp);
    }
    return false;
}

INLINE bool CWEBTLSClient::Connect(Socket sSocket, STR_ADDR& strAddr)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, strAddr.usPort, *strAddr.strIp);
    }
    return false;
}

INLINE bool CWEBTLSClient::Connect(Socket sSocket, NET_ADDR& NetAddr)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, NetAddr);
    }
    return false;
}

INLINE bool CWEBTLSClient::Send(Socket sSocket, PCStr pszData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, (PByte)pszData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSClient::Send(Socket sSocket, const CCString& strData)
{
    assert(strData.Length() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(strData.Length(), (PByte)strData.GetBuffer());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSClient::Send(Socket sSocket, PByte pData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, pData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSClient::Send(Socket sSocket, const CBufStream& Stream)
{
    assert(Stream.Size() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(Stream.Size(), Stream.GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBTLSClient::Send(Socket sSocket, CStreamScopePtr& StreamPtr)
{
    assert(StreamPtr != nullptr);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(StreamPtr->Tell(), StreamPtr->GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE void CWEBTLSClient::SetHash(Int nHashSize)
{
    m_Session.SetHash(nHashSize);
    if (m_NetworkTLSPtr != nullptr)
    {
        m_NetworkTLSPtr->SetHash(nHashSize);
    }
}

INLINE bool CWEBTLSClient::IsNetBufferMode(void) const
{
    return m_bNetBuffer;
}

INLINE void CWEBTLSClient::SetNetBufferMode(void)
{
    m_bNetBuffer = true;
}

INLINE bool CWEBTLSClient::IsAutoCheckMode(void) const
{
    return m_bAutoCheck;
}

INLINE void CWEBTLSClient::SetAutoCheckMode(void)
{
    m_bAutoCheck = true;
}

INLINE UInt CWEBTLSClient::GetControlCode(void) const
{
    return m_uControlCode;
}

INLINE void CWEBTLSClient::SetControlCode(bool bPing)
{
    if (bPing)
    {
        m_uControlCode = CWSTraits::WS_PING;
    }
    else
    {
        m_uControlCode = CWSTraits::WS_PONG;
    }
}

INLINE size_t CWEBTLSClient::GetMaxBlockSize(void) const
{
    return m_stMaxBlock;
}

INLINE void CWEBTLSClient::SetMaxBlockSize(size_t stMaxBlock)
{
    m_stMaxBlock = stMaxBlock;
}

INLINE size_t CWEBTLSClient::GetMaxCacheSize(void) const
{
    return m_stMaxCache;
}

INLINE void CWEBTLSClient::SetMaxCacheSize(size_t stMaxCache)
{
    m_stMaxCache = stMaxCache;
}

INLINE CNetworkPtr& CWEBTLSClient::GetNetwork(void)
{
    return m_NetworkPtr;
}

INLINE const CNetworkPtr& CWEBTLSClient::GetNetwork(void) const
{
    return m_NetworkPtr;
}

INLINE CEventHandlerPtr& CWEBTLSClient::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CWEBTLSClient::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE bool CWEBTLSClient::OnTcpConnect(UInt uError, Socket sConnect)
{
    CEventHandlerPtr EventHandlerPtr;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sConnect, SessionPtr) && GetHandler(SessionPtr, EventHandlerPtr))
    {
        assert(m_NetworkPtr != nullptr);
        return (EventHandlerPtr->OnHandle(EVENT_TCP_CONNECT, uError, sConnect) == RET_OKAY);
    }
    return false;
}

INLINE bool CWEBTLSClient::OnTcpClose(Socket sSocket, ULLong ullLiveData)
{
    CWEBSessionPtr SessionPtr;
    if (RemoveSession(sSocket, SessionPtr))
    {
        CloseHandle(SessionPtr);
    }
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr))
    {
        EventHandlerPtr->OnHandle(EVENT_TCP_CLOSE, sSocket, ullLiveData);
    }
    return true;
}

INLINE bool CWEBTLSClient::OnTlsHandShake(UInt uError, Socket sSocket)
{
    CEventHandlerPtr EventHandlerPtr;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr) && GetHandler(SessionPtr, EventHandlerPtr))
    {
        return (EventHandlerPtr->OnHandle(EVENT_TLS_HANDSHAKE, uError, sSocket) == RET_OKAY);
    }
    return false;
}

INLINE bool CWEBTLSClient::OnTlsRecv(size_t stSize, PTCP_PARAM pTcp)
{
    bool bComplete = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(pTcp->sSocket, SessionPtr))
    {
        if (IsNetBufferMode() == false)
        {
            bComplete = OnData(SessionPtr, stSize, pTcp);
        }
        else
        {
            bComplete = OnDataNetBuffer(SessionPtr, stSize, pTcp);
        }
        if (bComplete == false)
        {
            ErrorHandle(SessionPtr);
        }
    }
    return bComplete;
}

INLINE bool CWEBTLSClient::OnTlsSend(size_t stSize, PTCP_PARAM pTcp)
{
    bool   bRet   = false;
    PByte  pData  = pTcp->pData;
    size_t stData = 0;
    size_t stSend = 0;
    while (stData < stSize)
    {
        CStreamScopePtr StreamPtr;
        bRet = m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            stSend = DEF::Min<size_t>(StreamPtr->Size(), (stSize - stData));
            bRet = (StreamPtr->Write(pData, stSend) == stSend);
        }
        if (bRet)
        {
            stData += stSend;
            bRet = m_NetworkPtr->Send(pTcp->sSocket, StreamPtr);
        }
        if (bRet == false)
        {
            break;
        }
    }
    return bRet;
}

INLINE bool CWEBTLSClient::OnTlsError(UInt uError, Socket sSocket)
{
    CEventHandlerPtr EventHandlerPtr;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr) && GetHandler(SessionPtr, EventHandlerPtr))
    {
        EventHandlerPtr->OnHandle(EVENT_TLS_ERROR, uError, sSocket);
    }
    return true;
}

INLINE bool CWEBTLSClient::OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    PByte  pData     = pTcp->pData;
    while (SessionPtr->OnData(pData, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBTLSClient::OnData RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBTLSClient::OnData stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBTLSClient::OnData stData = %d, stSize = %d"), stData, stSize);
        pData  += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBTLSClient::OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    while (SessionPtr->OnData(m_NetAttr, pTcp, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBTLSClient::OnDataNetBuffer RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBTLSClient::OnDataNetBuffer stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBTLSClient::OnDataNetBuffer stData = %d, stSize = %d"), stData, stSize);
        pTcp->pData += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBTLSClient::CreateSession(Socket& sSocket, NET_ADDR& NetAddr)
{
    sSocket = m_NetworkPtr->Create(*m_NetworkTLSPtr, NetAddr);
    if (sSocket != 0)
    {
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_RECV_EVENT);
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_EXCLUDE_HEAD);
        m_NetworkTLSPtr->Attach(sSocket);

        CHTTPSSessionPtr SessionPtr = MNEW CHTTPSSession(sSocket);
        if (SessionPtr != nullptr)
        {
            SessionPtr->SetMaxBlockSize(m_stMaxBlock);
            SessionPtr->SetMaxCacheSize(m_stMaxCache);
            if (m_bNetBuffer)
            {
                SessionPtr->GetParser().SetNetBufferClone();
            }
            CSyncLockScope scope(m_SessionLock);
            return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
        }
        m_NetworkPtr->Destroy(sSocket);
        sSocket = 0;
    }
    return false;
}

INLINE bool CWEBTLSClient::CreateSession(Socket& sSocket, NET_ADDR& NetAddr, CEventHandler& EventHandlerRef)
{
    sSocket = m_NetworkPtr->Create(*m_NetworkTLSPtr, NetAddr);
    if (sSocket != 0)
    {
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_RECV_EVENT);
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_EXCLUDE_HEAD);
        m_NetworkTLSPtr->Attach(sSocket);

        CEventHandlerPtr EventHandlerPtr = &EventHandlerRef;
        CHTTPSSessionPtr SessionPtr = MNEW CHTTPSSession(sSocket, EventHandlerPtr);
        if (SessionPtr != nullptr)
        {
            SessionPtr->SetMaxBlockSize(m_stMaxBlock);
            SessionPtr->SetMaxCacheSize(m_stMaxCache);
            if (m_bNetBuffer)
            {
                SessionPtr->GetParser().SetNetBufferClone();
            }
            CSyncLockScope scope(m_SessionLock);
            return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
        }
        m_NetworkPtr->Destroy(sSocket);
        sSocket = 0;
    }
    return false;
}

INLINE bool CWEBTLSClient::DestroySession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return m_Session.Remove(sSocket);
}

INLINE bool CWEBTLSClient::CheckSession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return (m_Session.FindIndex(sSocket) != nullptr);
}

INLINE bool CWEBTLSClient::GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    CSyncLockScope scope(m_SessionLock);
    PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
    if (pPair != nullptr)
    {
        SessionPtr = pPair->m_V;
        return true;
    }
    return false;
}

INLINE bool CWEBTLSClient::RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    if (m_bExited == false)
    {
        CSyncLockScope scope(m_SessionLock);
        PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
        if (pPair != nullptr)
        {
            SessionPtr = pPair->m_V;
            return m_Session.RemoveAt(reinterpret_cast<PINDEX>(pPair));
        }
    }
    return false;
}

INLINE bool CWEBTLSClient::GetHandler(CWEBSessionPtr& SessionPtr, CEventHandlerPtr& EventHandlerPtr)
{
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP_TLS)
    {
        CHTTPSSession* pSession = static_cast<CHTTPSSession*>(SessionPtr.Get());
        EventHandlerPtr = pSession->GetEventHandler();
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS_TLS);
        CWSSSession* pSession = static_cast<CWSSSession*>(SessionPtr.Get());
        EventHandlerPtr = pSession->GetEventHandler();
    }
    if (EventHandlerPtr == nullptr)
    {
        EventHandlerPtr = m_EventHandlerPtr;
    }
    return (EventHandlerPtr != nullptr);
}

INLINE bool CWEBTLSClient::SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs)
{
    return m_NetworkTLSPtr->Send(SessionPtr->GetSocket(), brs);
}

INLINE bool CWEBTLSClient::RecvHandle(CWEBSessionPtr& SessionPtr)
{
    bool bRet = UpgradeHandle(SessionPtr);
    if (bRet == false)
    {
        return false;
    }
    if (SessionPtr->CheckDataCompleted())
    {
        bRet = CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return bRet;
}

INLINE bool CWEBTLSClient::CloseHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->CheckDataCompleted())
    {
        SessionPtr->SetStatus((UInt)RET_ERROR); // closed
        CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBTLSClient::ErrorHandle(CWEBSessionPtr&)// SessionPtr)
{
//     if ((SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS_TLS) && 
//         (SessionPtr->GetStatus() != 0) &&
//         (SessionPtr->ApplyHandle() != false))
//     {
//         CPCStr pszStatus = CWSTraits::StatusToString((Int)SessionPtr->GetStatus());
//         size_t stSize    = CChar::Length(pszStatus);
//         size_t stPayload = stSize + sizeof(UShort);
//         if (stPayload > CWSTraits::WS_PAY_LOAD)
//         {
//             stPayload = CWSTraits::WS_PAY_LOAD;
//             stSize    = stPayload - sizeof(UShort);
//         }
//         CBufWriteStream bws((stPayload + CWSTraits::WS_MASK_LEN + CWSTraits::WS_NORMAL_LEN));
//         // code = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE|CWSTraits::WS_MASK) | (UShort)(stPayload)
//         // head = CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN
//         // mask = (UInt)CPlatform::GetRunningTime();
//         bws.Seek(CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN);
// #ifndef __ARCH_TARGET_BIGENDIAN__
//         bws << CPlatform::ByteSwap((UShort)SessionPtr->GetStatus());
// #else
//         bws << (UShort)SessionPtr->GetStatus();
// #endif
//         bws.Write((PByte)pszStatus, stSize);

//         UShort usFrame = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE|CWSTraits::WS_MASK)|(UShort)(stPayload);
//         CWSParser::SetFrameHead(bws.GetBuf(), usFrame, (UInt)CPlatform::GetRunningTime(), (CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN), stPayload);

//         CBufReadStream brs(bws.Tell(), bws.GetBuf());
//         SendHandle(SessionPtr, brs);

//         SessionPtr->OnDataHandled();
//     }
    return true;
}

INLINE bool CWEBTLSClient::CompleteHandle(CWEBSessionPtr& SessionPtr)
{
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr) == false)
    {
        return false;
    }
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = SessionPtr->GetSocket();
    Param.nAttr   = (Int)CNETTraits::ATTR_PARAM_TCP;
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP_TLS)
    {
        CHTTPSSession* pSession = static_cast<CHTTPSSession*>(SessionPtr.Get());
        Param.nSize = pSession->GetParser().GetCacheSize();
        Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
        return (EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS_TLS);
        CWSSSession* pSession = static_cast<CWSSSession*>(SessionPtr.Get());
        Int nRet = RET_OKAY;
        if (pSession->GetControlCheck())
        {
            nRet = CompleteWSCheck(pSession, SessionPtr);
        }
        if (nRet == RET_OKAY)
        {
            Param.nSize = pSession->GetParser().GetFrameSize();
            Param.pData  = (PByte)pSession->GetParser().GetCacheBuf();
            return (EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
        }
        return (nRet != CWSTraits::WS_CLOSE_FRAME);
    }
}

INLINE Int CWEBTLSClient::CompleteWSCheck(CWSSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSParser& Parser = pSession->GetParser();
    if (Parser.IsControlFrame())
    {
        DEV_DEBUG(TF("CWEBTLSClient::CompleteWSCheck opcode = %x"), (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK));
        if (SessionPtr->GetStatus() == (UInt)RET_ERROR)
        {
            return CWSTraits::WS_CLOSE_FRAME;
        }
        Int nFrame = 0;
        switch (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK)
        {
        case CWSTraits::WS_CLOSE:
            {
                if (pSession->GetCloseCheck())
                {
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE;
                }
            }
            break;
        case CWSTraits::WS_PING:
            {
                if (m_uControlCode == CWSTraits::WS_PONG)
                {
                    // return from server
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
                else if (pSession->GetPingCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
            }
            break;
        case CWSTraits::WS_PONG:
            {
                if (m_uControlCode == CWSTraits::WS_PING)
                {
                    // return from server
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
                else if (pSession->GetPongCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
            }
            break;
        default:{}
        }
        if (nFrame != 0)
        {
            return nFrame;
        }
    }
    return RET_OKAY;
}

INLINE bool CWEBTLSClient::UpgradeHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->ApplyHandle() == false)
    {
        return false;
    }
    bool bRet = true;
    if (SessionPtr->CheckUpgrade() == false)
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP_TLS);
        CHTTPSSession* pSession = static_cast<CHTTPSSession*>(SessionPtr.Get());
        CHTTPResponse Response;
        pSession->GetParser().To(Response);

        if (CWSParser::CheckUpgrade(Response, bRet))
        {
            bRet = UpgradeWSSession(Response, pSession, SessionPtr);
        }
        // else if (CHTTP2Parser::CheckUpgrade(Response))
        // {
        //     bRet = UpgradeH2CSession(Response, pSession, SessionPtr);
        // }
        if (bRet)
        {
            SessionPtr->SetUpgraded();
        }
    }
    SessionPtr->ApplyHandle(false);
    return bRet;
}

INLINE bool CWEBTLSClient::UpgradeWSSession(const CHTTPResponse& Response, CHTTPSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr) == false)
    {
        return false;
    }
    CWSSessionPtr WSSessionPtr;
    bool bRet = (EventHandlerPtr->OnHandle(CNETTraits::EVENT_BEFORE_UPGRADE, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    if (bRet)
    {   // https session --> wss-session
        bRet = false;
        WSSessionPtr = MNEW CWSSSession(SessionPtr->GetSocket(), pSession->GetEventHandler());
        if (WSSessionPtr != nullptr)
        {
            WSSessionPtr->SetMaxBlockSize(m_stMaxBlock);
            WSSessionPtr->SetMaxCacheSize(m_stMaxCache);
            WSSessionPtr->SetPongCheck();
            if (m_bNetBuffer)
            {
                WSSessionPtr->GetParser().SetNetBufferClone();
            }
            if (m_bAutoCheck == false)
            {
                WSSessionPtr->SetAutoCheck(false);
            }

            CSyncLockScope scope(m_SessionLock);
            PAIR_WEBSESSION* pPair = m_Session.Find(pSession->GetSocket());
            if (pPair != nullptr)
            {
                pPair->m_V = WSSessionPtr;
                bRet = true;
            }
        }
    }
    if (bRet)
    {   
        SessionPtr = WSSessionPtr;
        bRet = (EventHandlerPtr->OnHandle(CNETTraits::EVENT_AFTER_UPGRADE, reinterpret_cast<uintptr_t>(SessionPtr.Get()), (ULLong)SessionPtr->GetType()) == RET_OKAY);
    }
    return bRet;
}


#endif // __NETWORK_HTTP_TLS_INL__
