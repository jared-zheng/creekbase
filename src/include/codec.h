// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CODEC_H__
#define __CODEC_H__

#pragma once

#include "stream.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CBase64
class CBase64 : public MObject
{
public:
    enum BASE64_LEN
    {
        BASE64_LEN_PAD = 2,
        BASE64_LEN_SRC = 3,
        BASE64_LEN_DST = 4,
    };
    static CPCStr     EncodeTable;
    static const Byte DecodeTable[];
public:
    static size_t GetEncodeSize(size_t stSrcSize);
    static size_t GetDecodeSize(PByte pSrc, size_t stSrcSize);
    static size_t GetMaxDecodeSize(size_t stSrcSize);

    static bool Encode(CCString& strDst, PByte pSrc, size_t stSrcSize);
    static bool Encode(CCString& strDst, const CCString& strSrc);

    static bool Decode(CCString& strDst, PByte pSrc, size_t stSrcSize);
    static bool Decode(CCString& strDst, const CCString& strSrc);
private:
    CBase64(void);
    ~CBase64(void);
    CBase64(const CBase64&);
    CBase64& operator=(const CBase64&);

    static bool Encode(PByte pSrc, size_t stSrcSize, PByte pDst, size_t stDstSize);
    static bool Decode(PByte pSrc, size_t stSrcSize, PByte pDst, size_t stDstSize);
};

///////////////////////////////////////////////////////////////////
// CAES : Rijndael AES
class CAES : public MObject
{
public:
    enum AES_KEY
    {
        AES_KEY_128    = 128,
        AES_KEY_192    = 192,
        AES_KEY_256    = 256,

        AES_KEY_128_NR = 10,
        AES_KEY_192_NR = 12,
        AES_KEY_256_NR = 14,
    };

    enum AES_LEN
    {
        AES_LEN_MASK    = 15,
        AES_LEN_BLOCK   = 16,
        AES_LEN_MAX_KEY = 32,
        AES_LEN_MAX_RK  = 60,
        AES_LEN_TABLE   = 256,
    };
    static const UInt TE0[AES_LEN_TABLE];
    static const UInt TE1[AES_LEN_TABLE];
    static const UInt TE2[AES_LEN_TABLE];
    static const UInt TE3[AES_LEN_TABLE];
    static const UInt TE4[AES_LEN_TABLE];

    static const UInt TD0[AES_LEN_TABLE];
    static const UInt TD1[AES_LEN_TABLE];
    static const UInt TD2[AES_LEN_TABLE];
    static const UInt TD3[AES_LEN_TABLE];
    static const UInt TD4[AES_LEN_TABLE];

    static const UInt RCON[];

    static CPCXStr StrEncodeFormat;
    static CPCXStr StrDecodeFormat;
public:
    static bool Encode(const CString& strKey, const CString& strSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);
    static bool Encode(const CString& strKey, CStream& StreamSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);
    static bool Encode(CStream& StreamKey, CStream& StreamSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);

    static bool Encode(const CString& strKey, const CString& strSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);
    static bool Encode(const CString& strKey, CStream& StreamSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);
    static bool Encode(CStream& StreamKey, CStream& StreamSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);

    static bool Decode(const CString& strKey, const CString& strSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);
    static bool Decode(const CString& strKey, CStream& StreamSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);
    static bool Decode(CStream& StreamKey, CStream& StreamSrc, CString& strDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0, bool bAppend = false);

    static bool Decode(const CString& strKey, const CString& strSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);
    static bool Decode(const CString& strKey, CStream& StreamSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);
    static bool Decode(CStream& StreamKey, CStream& StreamSrc, CStream& StreamDst, size_t stKey = AES_KEY_128, size_t stSrcSize = 0);

    static bool   AlignBlock(size_t stSize);
    static size_t AlignSize(size_t stSize);
private:
    CAES(void);
    ~CAES(void);
    CAES(const CAES&);
    CAES& operator=(const CAES&);

    static size_t KeyLength(size_t stKey);
    static size_t RoundKeyLength(size_t stKey);
    static size_t NRoundLength(size_t stKey);

    static UInt   InBlock(const PByte pInBlock);
    static void   OutBlock(UInt uValue, PByte pOutBlock);

    static size_t RoundKeyEncode(PUInt pRK, const PByte pKey, size_t stKey);
    static size_t RoundKeyDecode(PUInt pRK, const PByte pKey, size_t stKey);

    static void   DataEncode(const PUInt pRK, size_t stNRound, const PByte pInBlock, PByte pOutBlock);
    static void   DataDecode(const PUInt pRK, size_t stNRound, const PByte pInBlock, PByte pOutBlock);
};

///////////////////////////////////////////////////////////////////
#include "codec.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CODEC_H__
