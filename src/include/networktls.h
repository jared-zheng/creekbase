// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_TLS_H__
#define __NETWORK_TLS_H__

#pragma once

#include "network.h"

///////////////////////////////////////////////////////////////////
#if defined(NETWORKTLS_EXPORT)
    #define NETWORKTLSAPI             C_EXPORT
    #define NETWORKTLSCLASS           CXX_EXPORT
#elif defined(__RUNTIME_STATIC__)
    #define NETWORKTLSAPI
    #define NETWORKTLSCLASS
#else   // NETWORKTLS_EXPORT
    #define NETWORKTLSAPI             C_IMPORT
    #define NETWORKTLSCLASS           CXX_IMPORT
#endif  // NETWORKTLS_EXPORT

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNetworkTLS
// windows dll : networktls$(Configuration)_$(PlatformTarget)_$(PlatformToolset)_$(CharacterSet).lib
// windows lib : networktlslib$(Configuration)_$(PlatformTarget)_$(PlatformToolset)_$(CharacterSet).lib
// linux   so  : networktls[Debug]
// linux   a   : networktlslib[Debug]
class CNetworkTLS;
typedef CTRefCountPtr<CNetworkTLS> CNetworkTLSPtr;

class NETWORKTLSCLASS NOVTABLE CNetworkTLS ABSTRACT : public CEventHandler
{
public:
    typedef struct tagCONFIG_PARAM
    {
    public:
        tagCONFIG_PARAM(void)
        : uDHMParam(0)
        , uTLSBuffer(CNETTraits::ATTR_DEF_MAX_BUFFER)
        , bClientMode(false)
        , bStrictVerify(false)
        , bCacheEnable(true)
        , bTicketEnable(true)
        { }

        ~tagCONFIG_PARAM(void)
        { }

        tagCONFIG_PARAM(const tagCONFIG_PARAM& aSrc)
        : uDHMParam(aSrc.uDHMParam)
        , uTLSBuffer(aSrc.uTLSBuffer)
        , bClientMode(aSrc.bClientMode)
        , bStrictVerify(aSrc.bStrictVerify)
        , bCacheEnable(aSrc.bCacheEnable)
        , bTicketEnable(aSrc.bTicketEnable)
        , strCertFile(aSrc.strCertFile)
        , strKeyFile(aSrc.strKeyFile)
        , strKeyPass(aSrc.strKeyPass)
        , strCAChain(aSrc.strCAChain)
        , strCRLChain(aSrc.strCRLChain)
        , strSNI(aSrc.strSNI)
        , strAlpn(aSrc.strAlpn)
        {
        }

        tagCONFIG_PARAM& operator=(const tagCONFIG_PARAM& aSrc)
        {
            if (&aSrc != this)
            {
                uDHMParam     = aSrc.uDHMParam;
                uTLSBuffer    = aSrc.uTLSBuffer;
                bClientMode   = aSrc.bClientMode;
                bStrictVerify = aSrc.bStrictVerify;
                bCacheEnable  = aSrc.bCacheEnable;
                bTicketEnable = aSrc.bTicketEnable;
                strCertFile   = aSrc.strCertFile;
                strKeyFile    = aSrc.strKeyFile;
                strKeyPass    = aSrc.strKeyPass;
                strCAChain    = aSrc.strCAChain;
                strCRLChain   = aSrc.strCRLChain;
                strSNI        = aSrc.strSNI;
                strAlpn       = aSrc.strAlpn;
            }
            return (*this);
        }
    public:
        UInt      uDHMParam;     // server mode only
        UInt      uTLSBuffer;

        bool      bClientMode;
        bool      bStrictVerify; // true or false, default:false
        bool      bCacheEnable;  // server always enable
        bool      bTicketEnable; // server always enable

        CString   strCertFile;
        CString   strKeyFile;
        CString   strKeyPass;
        CString   strCAChain;    // CA file or include CAs folder(trusted top-level CAs)
        CString   strCRLChain;   // CRL file or include CRLs folder

        CString   strSNI;        // json---[{"Name":"xxx", "CertFile":"xxx", "KeyFile":"xxx", "KeyPass":"xxx", "CAChain":"xxx", "CRLChain":"xxx", "AuthMode":1}, {...}]
        CString   strAlpn;       // ALPN protocols separate by ','
    }CONFIG_PARAM, *PCONFIG_PARAM;
public:
    static  bool StaticCreate(CNetworkTLSPtr& TLSPtrRef, CONFIG_PARAM& Param);
public:
    virtual bool Init(CEventHandler* pEventHandler = nullptr) PURE;
    virtual void Exit(void) PURE;

    // ***special : client-mode only Connect Socket, server-mode only Listen Socket
    // Socket attach the tls handler
    // Socket SetAttr with SOCKET_RECV_EVENT + SOCKET_EXCLUDE_HEAD
    virtual bool Attach(CNETTraits::Socket sSocket) PURE;
    virtual bool Attach(CEventHandler& EventHandlerRef, CNETTraits::Socket sSocket) PURE;
    // Socket close detach the tls handler
    virtual void Detach(CNETTraits::Socket sSocket) PURE;

    virtual bool Send(CNETTraits::Socket sSocket, CBufStream& BufStream) PURE;
    virtual bool Send(CNETTraits::Socket sSocket, PByte pBuf, size_t stSize) PURE;

    virtual void SetHash(Int nHashSize) PURE;
protected:
    CNetworkTLS(void);
    virtual ~CNetworkTLS(void);
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_TLS_H__
