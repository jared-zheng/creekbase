// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__

#pragma once

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
typedef            char               Char  , *PChar;
typedef            short              Short , *PShort;
typedef            int                Int   , *PInt;
typedef            long               Long  , *PLong;
typedef            long long          LLong , *PLLong;

typedef   unsigned char               UChar , *PUChar;
typedef   unsigned short              UShort, *PUShort;
typedef   unsigned int                UInt  , *PUInt;
typedef   unsigned long               ULong , *PULong;
typedef   unsigned long long          ULLong, *PULLong;

typedef   float                       Float , *PFloat;
typedef   double                      Double, *PDouble;

typedef   UChar                       Byte  , *PByte;
typedef   wchar_t                     WChar , *PWChar;

typedef   struct tagINDEX{ }          INDEX,  *PINDEX;

///////////////////////////////////////////////////////////////////
// time
enum TIME_TRANS
{
    TIMET_TM_MONTH       = 1,    // tm.tm_mon  + 1
    TIMET_TM_YEAR        = 1900, // tm.tm_year + 1900

    TIMET_DAY_HOUR       = 24,
    TIMET_HOUR_MIN       = 60,
    TIMET_MIN_SEC        = 60,

    TIMET_IGNORE         = 0,
    TIMET_S2MS           = 1000,
    TIMET_XS2XS          = 1000, // S - MS MS - US US - NS
    // MS
    TIMET_LOCKSPIN       = 4000,
    TIMET_100NS2MS       = 10000,
    //
    TIMET_US2SEC         = 1000000,
    //
    TIMET_NS2US          = 1000,
    TIMET_NS2MS          = 1000000,
    TIMET_NS2SEC         = 1000000000,
    //
    TIMET_INFINITE       = 0xFFFFFFFF,
};

///////////////////////////////////////////////////////////////////
// capacity
enum CAPA_POW
{
    CAPAP_10             = 10,   // 2^10,
    CAPAP_20             = 20,   // 2^20,
    CAPAP_30             = 30,   // 2^30,
    CAPAP_40             = 40,   // 2^40,
    CAPAP_MASK           = 1023, // 2^10 - 1
};

///////////////////////////////////////////////////////////////////
// radix
enum RADIX_TYPE
{
    RADIXT_BIN           = 2,
    RADIXT_DIGITS        = 5,
    RADIXT_DEC           = 10,
    RADIXT_HEX           = 16,
    RADIXT_BASE36        = 36,
};

///////////////////////////////////////////////////////////////////
// return value
enum RET_VALUE
{
    RET_OKAY             = 0,
    RET_FAIL,
    RET_ERROR            = -1,
};

///////////////////////////////////////////////////////////////////
// variant type
enum VAR_TYPE
{
    // default types
    VART_NONE,

    VART_CHAR,
    VART_UCHAR,
    VART_SHORT,
    VART_USHORT,

    VART_INT,
    VART_UINT,
    VART_LONG,
    VART_ULONG,

    VART_LLONG,
    VART_ULLONG,
    VART_FLOAT,
    VART_DOUBLE,

    VART_WCHAR,
    VART_XCHAR,
    VART_BOOL,
    VART_SIZE_T,

    VART_HANDLE,
    VART_UUID,
    VART_STRING,
    VART_ANYTYPE,

    VART_ARRAY,
    VART_LIST,
    VART_QUEUE,
    VART_STACK,
    VART_MAP,

    VART_REFCOUNT,
    VART_STREAMBUF,
    VART_STREAMFILE,

    VART_SUBSYSTEM,
    VART_COMPONENT,

    // special types add
    // ...
    
    // default ext-type
    VART_MASK            = 0x000000FF,

    VART_ENUM            = 0x00000100,
    VART_STRUCT          = 0x00000200,
    VART_CLASS           = 0x00000400,
    VART_CONTAINER       = 0x00000800,

    VART_OBJECT          = 0x00001000,
    VART_SLOTOBJECT      = 0x00002000,
    VART_THREAD          = 0x00004000,
    VART_COROUTINE       = 0x00008000,

    VART_RUNNABLE        = 0x00010000,
    VART_QUEUETASK       = 0x00020000,
    VART_EVENTQUEUE      = 0x00040000,
    VART_EVENTTICK       = 0x00080000,

    VART_EVENTSIGNAL     = 0x00100000,
    VART_RTTITYPE        = 0x00200000,
    VART_RTTIDYNAMIC     = 0x00400000,
    VART_MEMCACHE        = 0x00800000,

    VART_FUNCTION        = 0x10000000,
    VART_CONST           = 0x20000000,
    VART_STATIC          = 0x40000000,
    VART_POINTER         = 0x80000000,
};

///////////////////////////////////////////////////////////////////
// log level
enum LOG_LEVEL
{
    LOGL_MEMMG           = 0x00000040, // memory manager
    LOGL_TRACE           = 0x00000020, // deep info trace
    LOGL_DEBUG           = 0x00000010, // debug output
    LOGL_INFO            = 0x00000008, // standard information
    LOGL_DUMP            = 0x00000004, // dump information
    LOGL_WARN            = 0x00000002, // warnning information
    LOGL_ERROR           = 0x00000001, // error information
#ifdef __RUNTIME_DEBUG__
    LOGL_ALL             = 0x0000003F,
#else
    LOGL_ALL             = 0x0000000F,
#endif
    LOGL_DEVOUT          = 0x00000000, // windows : outputdebugstring; linux : stdout
    LOGL_DEVPRINT        = 0x00001000, // windows : print to console; linux : syslog as LOG_DEBUG|LOG_USER or pipe
    LOGL_DEVFILE         = 0x00002000, // DEV+time.log file
};

///////////////////////////////////////////////////////////////////
// encoding & byte order mark
enum ENCODING_TYPE
{
    ENT_LOCAL,
    ENT_UTF8,
    ENT_UTF16,
    ENT_UTF16R,
    ENT_UTF32,
    ENT_UTF32R,
};

enum ENCODING_TRAN
{
    ENT_UTF32_UTF16      = 2,
    ENT_UTF16_UTF8       = 4,
    ENT_UTF32_UTF8       = 6,
};

enum BOM_LEADBYTE
{
    BOML_UTF80           = 0xEF,
    BOML_UTF81           = 0xBB,
    BOML_UTF82           = 0xBF,
    BOML_UTF8            = 3, // lead byte length

#ifndef __ARCH_TARGET_BIGENDIAN__
    BOML_UTF160          = 0xFF,
    BOML_UTF161          = 0xFE,
#else
    BOML_UTF160          = 0xFE,
    BOML_UTF161          = 0xFF,
#endif
    BOML_UTF16           = 2, // lead byte length

#ifndef __ARCH_TARGET_BIGENDIAN__
    BOML_UTF320          = 0xFF,
    BOML_UTF321          = 0xFE,
    BOML_UTF322          = 0x00,
    BOML_UTF323          = 0x00,
#else
    BOML_UTF320          = 0x00,
    BOML_UTF321          = 0x00,
    BOML_UTF322          = 0xFE,
    BOML_UTF323          = 0xFF,
#endif
    BOML_UTF32           = 4, // lead byte length

    BOML_MAXLEN          = 4,
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __COMMON_DEF_H__
