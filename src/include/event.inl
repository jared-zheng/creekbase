// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __EVENT_INL__
#define __EVENT_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CEventBase
INLINE CEventBase::CEventBase(void)
{
}

INLINE CEventBase::~CEventBase(void)
{
}

INLINE CEventBase::CEventBase(const CEventBase& aSrc)
: CTRefCount<CEventBase>(aSrc)
{
}

INLINE CEventBase& CEventBase::operator=(const CEventBase& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CEventBase>::operator=(aSrc);
    }
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CEventHandler
INLINE CEventHandler::CEventHandler(void)
{
}

INLINE CEventHandler::~CEventHandler(void)
{
}

INLINE CEventHandler::CEventHandler(const CEventHandler& aSrc)
: CTRefCount<CEventHandler>(aSrc)
{
}

INLINE CEventHandler& CEventHandler::operator=(const CEventHandler& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CEventHandler>::operator=(aSrc);
    }
    return (*this);
}

#endif // __EVENT_INL__