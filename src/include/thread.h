// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __THREAD_H__
#define __THREAD_H__

#pragma once

#include "refcount.h"
#include "handle.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
// threads:
// export class             inner class             commit
// CRunnable                CRunnableThread         --+
// CQueueTask               CQueueTaskThread          +- CThreadPool
// CThread                  ---                     --+
// CRunnableThread --> CThread <-- CQueueTaskThread

///////////////////////////////////////////////////////////////////
// CRunnable
// This is the base interface for "runnable" object. A runnable object is an
// object that is "run" on an arbitrary thread. The call usage pattern is
// OnStart(), Run(), OnStop(). The thread that is going to "run" this object always
// uses those calling semantics. It does this on the thread that is created so
// that any thread specific uses (TLS, etc.) are available in the contexts of
// those calls. A "runnable" does all initialization in OnStart(). If
// initialization fails, the thread stops execution and returns an error code.
// If it succeeds, Run() is called where the real threaded work is done. Upon
// completion, OnStop() is called to allow correct clean up.
class NOVTABLE CRunnable ABSTRACT : public CTRefCount<CRunnable>
{
public:
    virtual bool OnStart(TId tId) PURE; // before run, all per runnable inst init
    virtual void OnStop(void) PURE;     // before exit, release data, terminated thread do not called
    virtual bool Run(void) PURE;        // return true continue running, false thread should exit
protected:
    CRunnable(void);
    virtual ~CRunnable(void);
private:
    CRunnable(const CRunnable&);
    CRunnable& operator=(const CRunnable&);
};
typedef CTRefCountPtr<CRunnable> CRunnablePtr;

///////////////////////////////////////////////////////////////////
// CQueueTask
// This interface is a type of runnable object that requires no per thread
// initialization. It is meant to be used with pools of threads in an
// abstract way that prevents the pool from needing to know any details
// about the object being run. This allows queueing of disparate tasks and
// servicing those tasks with a generic thread pool.
class NOVTABLE CQueueTask ABSTRACT : public CTRefCount<CQueueTask>
{
public:
    virtual void Run(TId tId) PURE;
protected:
    CQueueTask(void);
    virtual ~CQueueTask(void);
private:
    CQueueTask(const CQueueTask&);
    CQueueTask& operator=(const CQueueTask&);
};
typedef CTRefCountPtr<CQueueTask> CQueueTaskPtr;

///////////////////////////////////////////////////////////////////
// CThread
class CORECLASS NOVTABLE CThread ABSTRACT : public MObject
{
    friend class CThreadPool;
public:
    enum THREAD_TYPE
    {
        THREAD_TYPE_THREAD,
        THREAD_TYPE_RUNNABLE,
        THREAD_TYPE_QUEUETASK,
    };

    enum THREAD_ROUTINE
    {
        THREAD_ROUTINE_ONSTART,
        THREAD_ROUTINE_RUN,
        THREAD_ROUTINE_ONSTOP,
    };
public:
    // tls  create fail return INDEX_INVALID
    static  UInt  TlsCreateIndex(void* pParam = nullptr);
    static  bool  TlsDestroyIndex(UInt uIndex);
    static  bool  TlsSetIndexValue(UInt uIndex, void* pValue);
    static  void* TlsGetIndexValue(UInt uIndex);
    // concurrent runnable inst
    static  bool  StartRunnable(CRunnable& RunnableRef, Int nPriority = 0, Int nPolicy = -1);
    static  bool  StartRunnable(CRunnablePtr RunnablePtr, Int nPriority = 0, Int nPolicy = -1);
    // queuetask-thread inst
    static  bool  QueueTask(CQueueTask& QueueTaskRef);
    static  bool  QueueTask(CQueueTaskPtr QueueTaskPtr);
    // 0 --- default queuetask-thread count = 1, max queuetask-thread = CPU core * 2
    static  void  QueueThread(UInt uThreadCount = 0);
public:
    virtual bool  Start(const CThreadAttr* pAttr = nullptr);
    // Wait for thread stop, reset m_hThread, call onStop; if thread not yet exit after time-out, return false
    virtual bool  Stop(UInt uWait = (UInt)TIMET_INFINITE); // MS

    // nPolicy == -1 --> IGNORE nPriority value, set to normal priority
    bool  GetPriority(Int& nPriority, Int& nPolicy) const;
    bool  SetPriority(Int nPriority, Int nPolicy);

    bool  IsRunning(void) const;
    // thread exception check
    void  UpdateStatus(void);
    bool  CheckStatus(UInt uTimeout); // MS

    bool  GetWaitStatus(void);
    void  SetWaitStatus(bool bWait = true);
    void  UpdateWaitStatus(bool bWait = true);

    // ***linux special : not implement suspend***
    // failed if return -1
    // Suspend increment suspend count, succeed return suspend count > 0
    // Resume  decrement suspend count, succeed return suspend count = 0, thread is resumed
    Int   Suspend(bool bSuspend = false);

    // ***linux special : uCode > 0 --> pthread_kill, = 0 --> pthread_cancel***
    Int   Kill(UInt uCode = 0);
    // ***linux special : join/tryjoin
    Int   Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false); // MS
    Int   Detach(void);
public:
    TId   GetId(void) const;
    Int   GetType(void) const;
protected:
    CThread(void);
    virtual ~CThread(void);

    virtual bool  OnStart(void);
    virtual void  OnStop(void);
    virtual void  OnKill(void);
    virtual bool  Run(void) PURE;
private:
    CThread(const CThread&);
    CThread& operator=(const CThread&);

    void    Routine(void);
private:
    ULLong          m_ullCheck;
    UInt            m_uStatus;
    Int             m_nType;
protected:
    CThreadHandle   m_hThread;
};

///////////////////////////////////////////////////////////////////
#include "thread.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __THREAD_H__
