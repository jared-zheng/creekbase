// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CONTAINER_H__
#define __CONTAINER_H__

#pragma once

#include "traits.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTContainerAlloc
template <typename T>
class CTContainerAlloc : public MObject
{
public:
    static T* Create(CTContainerAlloc*& pThis, size_t stElements);

    T*     GetData(void);
    void   Destroy(void);
private:
    CTContainerAlloc(void);
    ~CTContainerAlloc(void);
    CTContainerAlloc(const CTContainerAlloc&);
    CTContainerAlloc& operator=(const CTContainerAlloc&);
private:
    CTContainerAlloc*   m_pThis;
};

///////////////////////////////////////////////////////////////////
// CContainerTraits
class CContainerTraits : public MObject
{
public:
    enum SIZE_DEFAULT
    {
        SIZED_MIN   = 4,
        SIZED_GROW  = 32,
        SIZED_MAX   = (INT_MAX - SIZED_MIN),
    };

    enum HASH_DEFAULT
    {
        HASHD_LOW   = 4,
        HASHD_RADIX = 16,
        HASHD_MIN   = 32,
        HASHD_HIGH  = 36,
        HASHD_SIZE  = 1024,
        HASHD_MAX   = (INT_MAX - 32),
    };
};

///////////////////////////////////////////////////////////////////
// CTArray
template <typename T,
          typename Traits = CTElementTraits<T> >
class CTArray : public CContainerTraits
{
public:
    typedef typename Traits::INARGTYPE  INARGTYPE;
    typedef typename Traits::OUTARGTYPE OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename Traits::RVARGTYPE  RVARGTYPE;
#endif
public:
    CTArray(Int nGrow = SIZED_GROW);
    ~CTArray(void);

    CTArray(const CTArray& aSrc);
    CTArray& operator=(const CTArray& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTArray(CTArray&& aSrc);
    CTArray& operator=(CTArray&& aSrc);
#endif
    Int      GetAllocSize(void) const;
    Int      GetSize(void) const;
    Int      GetGrow(void) const;
    void     SetGrow(Int nGrow);

    Int      Add(INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    Int      Add(RVARGTYPE t);
#endif
    Int      Add(Int nCount, bool bZeroed);

    Int      Find(INARGTYPE t) const;

    bool     Remove(INARGTYPE t);
    bool     RemoveAt(Int nIndex);
    void     RemoveAll(void);

    bool     Check(Int nIndex) const;

    bool     Insert(Int nIndex, Int nCount = 1, bool bZeroed = false);
    bool     SetAt(Int nIndex, INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool     SetAt(Int nIndex, RVARGTYPE t);
#endif
    bool     Shrink(void);

    T&       operator[](Int nIndex);
    const T& operator[](Int nIndex) const;

    const T* GetData(void) const;

    T*       GetItem(Int nIndex);
    const T* GetItem(Int nIndex) const;

    bool     Append(const CTArray& aSrc);
    bool     Copy(const CTArray& aSrc);
    bool     Attach(CTArray& aSrc);
    void     Detach(void);
private:
    bool     SetCount(Int nNewCount = 1, bool bZeroed = true);
    void     SetAtIndex(Int nIndex, INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    void     SetAtIndex(Int nIndex, RVARGTYPE t);
#endif
    void     SetAtIndex(Int nIndex);
private:
    T*       m_paT;
    Int      m_nAllocSize;
    Int      m_nSize;
    Int      m_nGrow;
};

///////////////////////////////////////////////////////////////////
// CTList
template <typename T,
          typename Traits = CTElementTraits<T> >
class CTList : public CContainerTraits
{
public:
    typedef typename Traits::INARGTYPE INARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename Traits::RVARGTYPE RVARGTYPE;
#endif
private:
    class NODE : public INDEX
    {
    public:
        NODE(NODE* pPrev, NODE* pNext)
        : m_pNext(pNext)
        , m_pPrev(pPrev)
        {
        }

        NODE(INARGTYPE t, NODE* pPrev, NODE* pNext)
        : m_pNext(pNext)
        , m_pPrev(pPrev)
        , m_T(t)
        {
        }

#ifndef __MODERN_CXX_NOT_SUPPORTED
        NODE(RVARGTYPE t, NODE* pPrev, NODE* pNext)
        : m_pNext(pNext)
        , m_pPrev(pPrev)
        , m_T(std::move(t))
        {
        }
#endif

        NODE(const NODE& aSrc)
        : m_pNext(aSrc.m_pNext)
        , m_pPrev(aSrc.m_pPrev)
        , m_T(aSrc.m_T)
        {
        }

        NODE& operator=(const NODE& aSrc)
        {
            m_pNext = aSrc.m_pNext;
            m_pPrev = aSrc.m_pPrev;
            m_T     = aSrc.m_T;
            return (*this);
        }

        ~NODE(void)
        {
            m_pNext = nullptr;
            m_pPrev = nullptr;
        }
    public:
        NODE*   m_pNext;
        NODE*   m_pPrev;
        T       m_T;
    };
public:
    CTList(Int nGrow = SIZED_GROW);
    ~CTList(void);

    CTList(const CTList& aSrc);
    CTList& operator=(const CTList& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTList(CTList&& aSrc);
    CTList& operator=(CTList&& aSrc);
#endif

    Int      GetAllocSize(void) const;
    Int      GetSize(void) const;
    Int      GetGrow(void) const;
    void     SetGrow(Int nGrow);

    PINDEX   AddHead(void);
    PINDEX   AddHead(INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    PINDEX   AddHead(RVARGTYPE t);
#endif
    void     AddHeadList(const CTList& aSrc);
    PINDEX   AddTail(void);
    PINDEX   AddTail(INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    PINDEX   AddTail(RVARGTYPE t);
#endif
    void     AddTailList(const CTList& aSrc);

    bool     Attach(CTList& aSrc);
    void     Detach(void);

    PINDEX   Find(INARGTYPE t, PINDEX pStartAfter = nullptr) const;
    PINDEX   FindIndex(Int nIndex) const;

    T        RemoveHead(void);
    void     RemoveHeadDirect(void);
    T        RemoveTail(void);
    void     RemoveTailDirect(void);

    bool     Remove(INARGTYPE t);
    bool     RemoveAt(PINDEX index);
    void     RemoveAll(void);

    bool     Check(PINDEX index) const;

    PINDEX   InsertBefore(PINDEX index, INARGTYPE t);
    PINDEX   InsertAfter(PINDEX index, INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    PINDEX   InsertBefore(PINDEX index, RVARGTYPE t);
    PINDEX   InsertAfter(PINDEX index, RVARGTYPE t);
#endif

    bool     MoveToHead(PINDEX index);
    bool     MoveToTail(PINDEX index);
    bool     Swap(PINDEX index1, PINDEX index2);

    bool     SetAt(PINDEX index, INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool     SetAt(PINDEX index, RVARGTYPE t);
#endif

    T&       operator[](PINDEX index);
    const T& operator[](PINDEX index) const;

    T&       GetAt(PINDEX index);
    const T& GetAt(PINDEX index) const;

    T&       GetHead(void);
    const T& GetHead(void) const;
    T&       GetHead(PINDEX& index);
    const T& GetHead(PINDEX& index) const;
    PINDEX   GetHeadIndex(void) const;

    T&       GetTail(void);
    const T& GetTail(void) const;
    T&       GetTail(PINDEX& index);
    const T& GetTail(PINDEX& index) const;
    PINDEX   GetTailIndex(void) const;

    T&       GetNext(PINDEX& index);
    const T& GetNext(PINDEX& index) const;
    T&       GetPrev(PINDEX& index);
    const T& GetPrev(PINDEX& index) const;

    T&       GetNext(PINDEX& index, PINDEX& indexT);
    const T& GetNext(PINDEX& index, PINDEX& indexT) const;
    T&       GetPrev(PINDEX& index, PINDEX& indexT);
    const T& GetPrev(PINDEX& index, PINDEX& indexT) const;
private:
    void     FreeNode(NODE* pNode);
    bool     GetFreeNode(void);
    NODE*    NewNode(NODE* pPrev, NODE* pNext);
    NODE*    NewNode(INARGTYPE t, NODE* pPrev, NODE* pNext);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    NODE*    NewNode(RVARGTYPE t, NODE* pPrev, NODE* pNext);
#endif
private :
    NODE*                     m_pHead;
    NODE*                     m_pTail;
    NODE*                     m_pFree;
    CTContainerAlloc<NODE>*   m_pAlloc;
    Int                       m_nAllocSize;
    Int                       m_nSize;
    Int                       m_nGrow;
};

///////////////////////////////////////////////////////////////////
// CTQueue
template <typename T,
          typename Traits = CTElementTraits<T> >
class CTQueue : public CContainerTraits
{
public:
    typedef typename Traits::INARGTYPE INARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename Traits::RVARGTYPE RVARGTYPE;
#endif
public:
    CTQueue(Int nGrow = SIZED_GROW);
    ~CTQueue(void);

    CTQueue(const CTQueue& aSrc);
    CTQueue& operator=(const CTQueue& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTQueue(CTQueue&& aSrc);
    CTQueue& operator=(CTQueue&& aSrc);
#endif

    Int      GetSize(void) const;
    void     SetGrow(Int nGrow);

    bool     IsEmpty(void) const;
    bool     IsFull(void) const;

    bool     In(INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool     In(RVARGTYPE t);
#endif
    T        Out(void);

    void     RemoveAll(void);

    T&       GetItem(void);
    const T& GetItem(void) const;

    bool     Attach(CTQueue& aSrc);
    void     Detach(void);
private:
    CTList<T, Traits>   m_List;
};

///////////////////////////////////////////////////////////////////
// CTStack
template <typename T,
          typename Traits = CTElementTraits<T> >
class CTStack : public CContainerTraits
{
public:
    typedef typename Traits::INARGTYPE INARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename Traits::RVARGTYPE RVARGTYPE;
#endif
public:
    CTStack(Int nGrow = SIZED_GROW);
    ~CTStack(void);

    CTStack(const CTStack& aSrc);
    CTStack& operator=(const CTStack& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTStack(CTStack&& aSrc);
    CTStack& operator=(CTStack&& aSrc);
#endif

    Int      GetSize(void) const;
    void     SetGrow(Int nGrow);

    bool     IsEmpty(void) const;
    bool     IsFull(void) const;

    bool     Push(INARGTYPE t);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool     Push(RVARGTYPE t);
#endif
    T        Pop(void);

    void     RemoveAll(void);

    T&       GetItem(void);
    const T& GetItem(void) const;

    bool     Attach(CTStack& aSrc);
    void     Detach(void);
private:
    CTList<T, Traits>   m_List;
};

///////////////////////////////////////////////////////////////////
// CTMap
template <typename K,
          typename V,
          typename KTraits = CTElementTraits<K>,
          typename VTraits = CTElementTraits<V> >
class CTMap : public CContainerTraits
{
public:
    typedef typename KTraits::INARGTYPE  KINARGTYPE;
    typedef typename KTraits::OUTARGTYPE KOUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename KTraits::RVARGTYPE  KRVARGTYPE;
#endif
    typedef typename VTraits::INARGTYPE  VINARGTYPE;
    typedef typename VTraits::OUTARGTYPE VOUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef typename VTraits::RVARGTYPE  VRVARGTYPE;
#endif
public:
    class PAIR : public INDEX
    {
    public:
        PAIR(KINARGTYPE tK)
        : m_K(tK)
        {
        }

        PAIR(KINARGTYPE tK, VINARGTYPE tV)
        : m_K(tK)
        , m_V(tV)
        {
        }
#ifndef __MODERN_CXX_NOT_SUPPORTED
        PAIR(KRVARGTYPE tK)
        : m_K(std::move(tK))
        {
        }

        PAIR(KINARGTYPE tK, VRVARGTYPE tV)
        : m_K(tK)
        , m_V(std::move(tV))
        {
        }

        PAIR(KRVARGTYPE tK, VRVARGTYPE tV)
        : m_K(std::move(tK))
        , m_V(std::move(tV))
        {
        }

        PAIR(KRVARGTYPE tK, VINARGTYPE tV)
        : m_K(std::move(tK))
        , m_V(tV)
        {
        }
#endif

        PAIR(const PAIR& aSrc)
        : m_K(aSrc.m_K)
        , m_V(aSrc.m_V)
        {
        }

        PAIR& operator=(const PAIR& aSrc)
        {
            m_K = aSrc.m_K;
            m_V = aSrc.m_V;
            return (*this);
        }

        ~PAIR(void)
        {
        }
    public:
        const K   m_K;
        V         m_V;
    };
private :
    class NODE : public PAIR
    {
    public:
        NODE(KINARGTYPE tK)
        : PAIR(tK)
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }

        NODE(KINARGTYPE tK, VINARGTYPE tV)
        : PAIR(tK, tV)
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }
#ifndef __MODERN_CXX_NOT_SUPPORTED
        NODE(KRVARGTYPE tK)
        : PAIR(std::move(tK))
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }

        NODE(KINARGTYPE tK, VRVARGTYPE tV)
        : PAIR(tK, std::move(tV))
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }

        NODE(KRVARGTYPE tK, VRVARGTYPE tV)
        : PAIR(std::move(tK), std::move(tV))
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }

        NODE(KRVARGTYPE tK, VINARGTYPE tV)
        : PAIR(std::move(tK), tV)
        , m_pNext(nullptr)
        , m_pPrev(nullptr)
        , m_nHash(0)
        {
        }
#endif

        NODE(const NODE& aSrc)
        : PAIR(aSrc.m_K, aSrc.m_V)
        , m_pNext(aSrc.m_pNext)
        , m_pPrev(aSrc.m_pPrev)
        , m_nHash(aSrc.m_nHash)
        {
        }

        NODE& operator=(const NODE& aSrc)
        {
            if (&aSrc != this)
            {
                PAIR::operator=(aSrc);
                m_pNext = aSrc.m_pNext;
                m_pPrev = aSrc.m_pPrev;
                m_nHash = aSrc.m_nHash;
            }
            return (*this);
        }

        ~NODE(void)
        {
            m_pNext = nullptr;
            m_pPrev = nullptr;
            m_nHash = 0;
        }

        void Reset(void)
        {
            m_pNext = nullptr;
            m_pPrev = nullptr;
            m_nHash = 0;
        }
    public:
        NODE*   m_pNext;
        NODE*   m_pPrev;
        Int     m_nHash;
    };
public :
    CTMap(Int nGrow = SIZED_GROW, Int nHash = HASHD_MIN);
    ~CTMap(void);

    CTMap(const CTMap& aSrc);
    CTMap& operator=(const CTMap& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTMap(CTMap&& aSrc);
    CTMap& operator=(CTMap&& aSrc);
#endif
    Int         GetAllocSize(void) const;
    Int         GetSize(void) const;
    Int         GetGrow(void) const;
    void        SetGrow(Int nGrow);
    Int         GetHash(void) const;
    void        SetHash(Int nHash);

    PINDEX      Add(KINARGTYPE Key);
    PINDEX      Add(KINARGTYPE Key, VINARGTYPE Val);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    PINDEX      Add(KRVARGTYPE Key);
    PINDEX      Add(KINARGTYPE Key, VRVARGTYPE Val);
    PINDEX      Add(KRVARGTYPE Key, VRVARGTYPE Val);
    PINDEX      Add(KRVARGTYPE Key, VINARGTYPE Val);
#endif

    PAIR*       Find(KINARGTYPE Key, PINDEX pStartAfter = nullptr);
    const PAIR* Find(KINARGTYPE Key, PINDEX pStartAfter = nullptr) const;
    PINDEX      FindIndex(KINARGTYPE Key, PINDEX pStartAfter = nullptr) const;

    bool        Remove(KINARGTYPE Key);
    bool        RemoveAt(PINDEX index);
    void        RemoveAll(void);

    bool        Check(PINDEX index) const;

    // if can not find the key, add a new node
    PINDEX      SetAt(KINARGTYPE Key, VINARGTYPE Val);
    bool        SetAtIndex(PINDEX index, VINARGTYPE Val);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    PINDEX      SetAt(KINARGTYPE Key, VRVARGTYPE Val);
    PINDEX      SetAt(KRVARGTYPE Key, VRVARGTYPE Val);
    PINDEX      SetAt(KRVARGTYPE Key, VINARGTYPE Val);
    bool        SetAtIndex(PINDEX index, VRVARGTYPE Val);
#endif

    PAIR*       GetAt(PINDEX index);
    const PAIR* GetAt(PINDEX index) const;
    const K&    GetKeyAt(PINDEX index) const;
    V&          GetValueAt(PINDEX index);
    const V&    GetValueAt(PINDEX index) const;

    V&          operator[](PINDEX index);
    const V&    operator[](PINDEX index) const;

    PAIR*       GetFirst(void);
    const PAIR* GetFirst(void) const;
    PINDEX      GetFirstIndex(void) const;

    PAIR*       GetLast(void);
    const PAIR* GetLast(void) const;
    PINDEX      GetLastIndex(void) const;

    PAIR*       GetNext(PINDEX& index);
    const PAIR* GetNext(PINDEX& index) const;
    PAIR*       GetPrev(PINDEX& index);
    const PAIR* GetPrev(PINDEX& index) const;

    void        Append(const CTMap& aSrc);
    void        Copy(const CTMap& aSrc);
    bool        Attach(CTMap& aSrc);
    void        Detach(void);
private :
    bool        InitHash(void);
    bool        BuildHash(Int nHash);
    bool        Hash(NODE* pNode);
    bool        ReHash(NODE* pNode);

    bool        GetFreeNode(void);
    NODE*       NewNode(KINARGTYPE Key);
    NODE*       NewNode(KINARGTYPE Key, VINARGTYPE Val);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    NODE*       NewNode(KRVARGTYPE Key);
    NODE*       NewNode(KINARGTYPE Key, VRVARGTYPE Val);
    NODE*       NewNode(KRVARGTYPE Key, VRVARGTYPE Val);
    NODE*       NewNode(KRVARGTYPE Key, VINARGTYPE Val);
#endif
    void        DeleteNode(NODE* pNode);
private :
    NODE*                     m_pFree;
    NODE**                    m_ppHash;
    CTContainerAlloc<NODE>*   m_pAlloc;
    Int                       m_nAllocSize;
    Int                       m_nSize;
    Int                       m_nGrow;
    Int                       m_nHash;
    Int                       m_nBuild;
    Int                       m_nHead;
    Int                       m_nTail;
};

///////////////////////////////////////////////////////////////////
#include "container.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CONTAINER_H__
