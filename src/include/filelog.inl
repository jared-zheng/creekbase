// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __FILE_LOG_INL__
#define __FILE_LOG_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CFileLog
SELECTANY CPCXStr CFileLog::LogFilePrefix = TF("LOG");

INLINE CFileLog::CFileLog(bool bCreate, Int nLogLevel, PCXStr pszPath, PCXStr pszPrefix)
: m_nLogLevel(nLogLevel & (LOGL_ALL| LOGL_DEVPRINT))
, m_nStatus(FALSE)
, m_nFlag(FALSE)
, m_nOpenIndex(0)
, m_stLogSize(0)
, m_stMaxSize(FILE_LOG_MAX_SIZE)
, m_File((size_t)FILE_LOG_CACHE)
{
    if (bCreate)
    {
        Create(pszPath, pszPrefix);
    }
}

INLINE CFileLog::~CFileLog(void)
{
    Close();
}

INLINE CFileLog::CFileLog(const CFileLog&)
: m_nLogLevel(LOGL_DEVFILE)
, m_nStatus(FALSE)
, m_nFlag(FALSE)
, m_nOpenIndex(0)
, m_stLogSize(0)
, m_stMaxSize(FILE_LOG_MAX_SIZE)
{
}

INLINE CFileLog& CFileLog::operator=(const CFileLog&)
{
    return (*this);
}

INLINE void CFileLog::SetMaxSize(size_t stMaxSize)
{
    m_stMaxSize = stMaxSize;
}

INLINE Int CFileLog::GetLevel(void) const
{
    return m_nLogLevel;
}

INLINE void CFileLog::SetLevel(Int nLogLevel)
{
    m_nLogLevel = (nLogLevel & (LOGL_ALL | LOGL_DEVPRINT));
}

INLINE void CFileLog::EnableLineFlush(bool bEnabled)
{
    m_nStatus = bEnabled ? TRUE : FALSE;
}

INLINE void CFileLog::Flush(void)
{
    m_File.Flush();
}

INLINE bool CFileLog::IsValid(void) const
{
    return (m_File.IsValid());
}

INLINE bool CFileLog::Create(PCXStr pszPath, PCXStr pszPrefix)
{
    Close();

    m_nOpenIndex = 0;
    if (pszPrefix == nullptr)
    {
        if (m_strPrefix.IsEmpty())
        {
            m_strPrefix = LogFilePrefix;
        }
        pszPrefix = *m_strPrefix;
    }
    else if (m_strPrefix != pszPrefix)
    {
        m_strPrefix = pszPrefix;
    }

    CPlatform::TIMEINFO ti = { 0 };
    CPlatform::GetTimeInfo(ti);

    CString strName;
    if (pszPath == nullptr)
    {
        if (m_strPath.IsEmpty())
        {
            XChar  szPath[LMT_MAX_PATH];
            CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);
            m_strPath = szPath;
            m_strPath.TrimRight(CFilePath::SlashChar);
            m_strPath.TrimRight(CFilePath::BackSlashChar);
        }
        strName = m_strPath;
    }
    else
    {
        strName = pszPath;
        strName.TrimRight(CFilePath::SlashChar);
        strName.TrimRight(CFilePath::BackSlashChar);
        m_strPath = strName;
    }
    DEV_DEBUG(TF("file log Create m_strPrefix = %s, m_strPath = %s"), *m_strPrefix, *m_strPath);
    strName.AppendFormat(LogFileTime, pszPrefix, ti.usYear, ti.usMonth, ti.usDay,
                         ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond, CPlatform::GetCurrentPId());
    if (m_File.Create(*strName) == RET_OKAY)
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        m_File << (Byte)BOML_UTF80 << (Byte)BOML_UTF81 << (Byte)BOML_UTF82;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        m_File << (Byte)BOML_UTF320 << (Byte)BOML_UTF321 << (Byte)BOML_UTF322 << (Byte)BOML_UTF323;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        m_File << (Byte)BOML_UTF160 << (Byte)BOML_UTF161;
#endif
        CAtomics::Exchange<UInt>((PUInt)&m_nFlag, TRUE);
        return true;
    }
    return false;
}

INLINE bool CFileLog::Open(PCXStr pszName, PCXStr pszPath)
{
    if (pszName == nullptr)
    {
        return false;
    }

    Close();

    if (m_strPrefix != pszName)
    {
        m_strPrefix = pszName;
    }
    if (pszPath == nullptr)
    {
        if (m_strPath.IsEmpty())
        {
            XChar  szPath[LMT_MAX_PATH];
            CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);
            m_strPath = szPath;
            m_strPath.TrimRight(CFilePath::SlashChar);
            m_strPath.TrimRight(CFilePath::BackSlashChar);
        }
    }
    else
    {
        m_strPath = pszPath;
        m_strPath.TrimRight(CFilePath::SlashChar);
        m_strPath.TrimRight(CFilePath::BackSlashChar);
    }
    DEV_DEBUG(TF("file log Open m_strPrefix = %s, m_strPath = %s, m_nOpenIndex = %d"), *m_strPrefix, *m_strPath, m_nOpenIndex);
    CString strName = m_strPrefix;
    if (m_nOpenIndex > 0)
    {
        strName.AppendFormat(TF("-%d"), m_nOpenIndex);
    }
    CString strPath;
    strPath.Format(LogFileName, m_strPath.GetBuffer(), strName.GetBuffer());

    if (m_File.Create(*strPath, FILEF_OPEN_ALWAYS|FILEF_EXT_APPEND|FILEF_SHARE_DEFAULT) == RET_OKAY)
    {
        if (m_File.Size() == 0)
        {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
            m_File << (Byte)BOML_UTF80 << (Byte)BOML_UTF81 << (Byte)BOML_UTF82;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
            m_File << (Byte)BOML_UTF320 << (Byte)BOML_UTF321 << (Byte)BOML_UTF322 << (Byte)BOML_UTF323;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
            m_File << (Byte)BOML_UTF160 << (Byte)BOML_UTF161;
#endif
        }
        ++m_nOpenIndex;
        CAtomics::Exchange<UInt>((PUInt)&m_nFlag, TRUE);
        return true;
    }
    return false;
}

INLINE void CFileLog::Close(void)
{
    if (m_File.IsValid())
    {
        CAtomics::Exchange<UInt>((PUInt)&m_nFlag, FALSE);
        m_File.Close();
    }
}

INLINE Int CFileLog::Put(Int nLevel, PCXStr pszLog)
{
    if (IsValid() && (nLevel & m_nLogLevel))
    {
        Int nLen = (Int)CXChar::Length(pszLog, FILE_LOG_BUF);
        if ((nLen > 0) && (CAtomics::CompareExchange<UInt>((PUInt)&m_nFlag, TRUE, TRUE) == TRUE))
        {
            return Write(nLen, pszLog);
        }
    }
    return 0;
}

INLINE Int CFileLog::Log(Int nLevel, PCXStr pszLog)
{
    if (IsValid() && (nLevel & m_nLogLevel))
    {
        PCXStr pszFormat = GetFormat(nLevel);
        if (pszFormat != nullptr)
        {
            CPlatform::TIMEINFO ti = { 0 };
            CPlatform::GetTimeInfo(ti);

            Int nLen = CXChar::Format(m_szBuf, FILE_LOG_BUF, pszFormat,
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId(), pszLog);
            if ((nLen > 0) && (CAtomics::CompareExchange<UInt>((PUInt)&m_nFlag, TRUE, TRUE) == TRUE))
            {
                m_szBuf[nLen] = 0;
                return Write(nLen, m_szBuf);
            }
        }
    }
    return 0;
}

INLINE Int CFileLog::LogV(Int nLevel, PCXStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nLen = LogL(nLevel, pszFormat, vl);
    va_end(vl);
    return nLen;
}

INLINE Int CFileLog::LogL(Int nLevel, PCXStr pszFormat, va_list vl)
{
    if (IsValid() && (nLevel & m_nLogLevel))
    {
        XChar szBuf[FILE_LOG_BUF];
        Int nLen = CXChar::FormatV(szBuf, FILE_LOG_BUF, pszFormat, vl);
        if (nLen > 0)
        {
            szBuf[FILE_LOG_BUF - 1] = 0;
            nLen = Log(nLevel, szBuf);
            return nLen;
        }
    }
    return 0;
}

INLINE Int CFileLog::Write(Int nLen, PCXStr pszBuf)
{
    size_t stSize = m_File.Write(pszBuf, nLen * sizeof(XChar));
    if (m_nStatus != FALSE)
    {
        m_File.Flush();
    }

    stSize += CAtomics::Add<size_t>(&m_stLogSize, stSize);
    if ((stSize >= m_stMaxSize) &&
        (CAtomics::CompareExchange<size_t>(&m_stLogSize, 0, stSize) == stSize))
    {
        if (m_nOpenIndex == 0)
        {
            Create();
        }
        else
        {
            Open(*m_strPrefix);
        }
    }
    return nLen;
}

INLINE PCXStr CFileLog::GetFormat(Int nLevel)
{
    switch (nLevel)
    {
#ifdef __RUNTIME_DEBUG__
    case LOGL_TRACE:
        {
            return LogFileTrace;
        }
        break;
    case LOGL_DEBUG:
        {
            return LogFileDebug;
        }
        break;
#endif
    case LOGL_INFO:
        {
            return LogFileInfo;
        }
        break;
    case LOGL_DUMP:
        {
            return LogFileDump;
        }
        break;
    case LOGL_WARN:
        {
            return LogFileWarn;
        }
        break;
    case LOGL_ERROR:
        {
            return LogFileError;
        }
        break;
    default:
        {
        }
    }
    return nullptr;
}

///////////////////////////////////////////////////////////////////
// CFileLogLock
INLINE CFileLogLock::CFileLogLock(bool bCreate, Int nLogLevel, PCXStr pszPath, PCXStr pszPrefix)
: m_Log(bCreate, nLogLevel, pszPath, pszPrefix)
{
}

INLINE CFileLogLock::~CFileLogLock(void)
{
}

INLINE CFileLogLock::CFileLogLock(const CFileLogLock&)
: m_Log(false)
{
}

INLINE CFileLogLock& CFileLogLock::operator=(const CFileLogLock&)
{
    return (*this);
}

INLINE void CFileLogLock::SetMaxSize(size_t stMaxSize)
{
    m_Log.SetMaxSize(stMaxSize);
}

INLINE Int CFileLogLock::GetLevel(void) const
{
    return m_Log.GetLevel();
}

INLINE void CFileLogLock::SetLevel(Int nLogLevel)
{
    m_Log.SetLevel(nLogLevel);
}

INLINE void CFileLogLock::EnableLineFlush(bool bEnabled)
{
    m_Log.EnableLineFlush(bEnabled);
}

INLINE void CFileLogLock::Flush(void)
{
    m_Log.Flush();
}

INLINE bool CFileLogLock::IsValid(void) const
{
    return m_Log.IsValid();
}

INLINE bool CFileLogLock::Create(PCXStr pszPath, PCXStr pszPrefix)
{
    return m_Log.Create(pszPath, pszPrefix);
}

INLINE bool CFileLogLock::Open(PCXStr pszName, PCXStr pszPath)
{
    return m_Log.Open(pszName, pszPath);
}

INLINE void CFileLogLock::Close(void)
{
    m_Log.Close();
}

INLINE Int CFileLogLock::Put(Int nLevel, PCXStr pszLog)
{
    CSyncLockWaitScope scope(m_Lock);
    return m_Log.Put(nLevel, pszLog);
}

INLINE Int CFileLogLock::Log(Int nLevel, PCXStr pszLog)
{
    CSyncLockWaitScope scope(m_Lock);
    return m_Log.Log(nLevel, pszLog);
}

INLINE Int CFileLogLock::LogV(Int nLevel, PCXStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nLen = LogL(nLevel, pszFormat, vl);
    va_end(vl);
    return nLen;
}

INLINE Int CFileLogLock::LogL(Int nLevel, PCXStr pszFormat, va_list vl)
{
    CSyncLockWaitScope scope(m_Lock);
    return m_Log.LogL(nLevel, pszFormat, vl);
}

///////////////////////////////////////////////////////////////////
// CFileLogPipe
INLINE CFileLogPipe::CFileLogPipe(bool bCreate, Int nLogLevel, PCXStr pszPath, PCXStr pszPrefix)
: m_hPipe(HANDLE_INVALID)
, m_Log(false, nLogLevel)
{
    if (bCreate && m_Log.Create(pszPath, pszPrefix))
    {
        OpenPipe();
    }
}

INLINE CFileLogPipe::~CFileLogPipe(void)
{
    ClosePipe();
}

INLINE CFileLogPipe::CFileLogPipe(const CFileLogPipe&)
: m_hPipe(HANDLE_INVALID)
, m_Log(false)
{
}

INLINE CFileLogPipe& CFileLogPipe::operator=(const CFileLogPipe&)
{
    return (*this);
}

INLINE void CFileLogPipe::SetMaxSize(size_t stMaxSize)
{
    m_Log.SetMaxSize(stMaxSize);
}

INLINE Int CFileLogPipe::GetLevel(void) const
{
    return m_Log.GetLevel();
}

INLINE void CFileLogPipe::SetLevel(Int nLogLevel)
{
    m_Log.SetLevel(nLogLevel);
}

INLINE void CFileLogPipe::EnableLineFlush(bool bEnabled)
{
    m_Log.EnableLineFlush(bEnabled);
}

INLINE void CFileLogPipe::Flush(void)
{
    m_Log.Flush();
}

INLINE bool CFileLogPipe::IsValid(void) const
{
    return m_Log.IsValid();
}

INLINE bool CFileLogPipe::IsPipeValid(void) const
{
    if ((m_Log.GetLevel() & LOGL_DEVPRINT) == 0)
    {
        return (m_hPipe != HANDLE_INVALID);
    }
    return true;
}

INLINE bool CFileLogPipe::Create(PCXStr pszPath, PCXStr pszPrefix)
{
    if (m_Log.Create(pszPath, pszPrefix))
    {
        OpenPipe();
    }
    return false;
}

INLINE bool CFileLogPipe::Open(PCXStr pszName, PCXStr pszPath)
{
    if (m_Log.Open(pszName, pszPath))
    {
        OpenPipe(true);
    }
    return false;
}

INLINE void CFileLogPipe::Close(void)
{
    m_Log.Close();
    ClosePipe();
}

INLINE Int CFileLogPipe::Put(Int nLevel, PCXStr pszLog)
{
    CSyncLockWaitScope scope(m_Lock);
    if (IsValid() && (nLevel & m_Log.m_nLogLevel))
    {
        Int nLen = (Int)CXChar::Length(pszLog, CFileLog::FILE_LOG_BUF);
        if (nLen > 0)
        {
            //DEV_DEBUG(TF("file log pipe Put pszLog = %s[%d]"), pszLog, nLen);
            if (CAtomics::CompareExchange<UInt>((PUInt)&m_Log.m_nFlag, TRUE, TRUE) == TRUE)
            {
                m_Log.Write(nLen, pszLog);
            }
            if (IsPipeValid())
            {
                m_Log.m_szBuf[0] = TF('[');
                CXChar::Copy((m_Log.m_szBuf + 1), (CFileLog::FILE_LOG_BUF - 1), m_Log.m_strPrefix.GetBuffer());
                m_Log.m_szBuf[m_Log.m_strPrefix.Length() + 1] = TF(']');

                size_t stPrefix = (m_Log.m_strPrefix.Length() + 2);
                PXStr  pszBuf   = m_Log.m_szBuf + stPrefix;
                size_t stBuf    = (CFileLog::FILE_LOG_BUF - stPrefix);

                CXChar::Copy(pszBuf, stBuf, pszLog, (size_t)nLen);
                stPrefix += (size_t)nLen;
                if (stPrefix > CFileLog::FILE_LOG_BUF)
                {
                    stPrefix = CFileLog::FILE_LOG_BUF;
                }
                //DEV_DEBUG(TF("file log pipe WritePipe szBuf = %s[%d]"), m_Log.m_szBuf, stPrefix);
                WritePipe(m_Log.m_szBuf, stPrefix);
            }

            return nLen;
        }
    }
    return 0;
}

INLINE Int CFileLogPipe::Log(Int nLevel, PCXStr pszLog)
{
    CSyncLockWaitScope scope(m_Lock);
    if (IsPipeValid() == false)
    {
        return m_Log.Log(nLevel, pszLog);
    }
    if (IsValid() && (nLevel & m_Log.m_nLogLevel))
    {
        PCXStr pszFormat = m_Log.GetFormat(nLevel);
        if (pszFormat != nullptr)
        {
            m_Log.m_szBuf[0] = TF('[');
            CXChar::Copy((m_Log.m_szBuf + 1), (CFileLog::FILE_LOG_BUF - 1), m_Log.m_strPrefix.GetBuffer());
            m_Log.m_szBuf[m_Log.m_strPrefix.Length() + 1] = TF(']');

            size_t stPrefix = (m_Log.m_strPrefix.Length() + 2);
            PXStr  pszBuf   = m_Log.m_szBuf + stPrefix;
            size_t stBuf    = (CFileLog::FILE_LOG_BUF - stPrefix);

            CPlatform::TIMEINFO ti = { 0 };
            CPlatform::GetTimeInfo(ti);

            Int nLen = CXChar::Format(pszBuf, stBuf, pszFormat,
                                      ti.usYear, ti.usMonth, ti.usDay,
                                      ti.usHour, ti.usMinute, ti.usSecond, ti.usMSecond,
                                      CPlatform::GetCurrentTId(), pszLog);
            if (nLen > 0)
            {
                if (CAtomics::CompareExchange<UInt>((PUInt)&m_Log.m_nFlag, TRUE, TRUE) == TRUE)
                {
                    pszBuf[nLen] = 0;
                    //DEV_DEBUG(TF("file log pipe Log pszBuf = %s[%d]"), pszBuf, nLen);
                    m_Log.Write(nLen, pszBuf);
                }
                //DEV_DEBUG(TF("file log pipe WritePipe pszBuf = %s[%d]"), m_Log.m_szBuf, ((size_t)nLen + stPrefix));
                WritePipe(m_Log.m_szBuf, ((size_t)nLen + stPrefix));

                return nLen;
            }
        }
    }
    return 0;
}

INLINE Int CFileLogPipe::LogV(Int nLevel, PCXStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nLen = LogL(nLevel, pszFormat, vl);
    va_end(vl);
    return nLen;
}

INLINE Int CFileLogPipe::LogL(Int nLevel, PCXStr pszFormat, va_list vl)
{
    CSyncLockWaitScope scope(m_Lock);
    if (IsValid() && (nLevel & m_Log.m_nLogLevel))
    {
        XChar szBuf[CFileLog::FILE_LOG_BUF];
        Int nLen = CXChar::FormatV(szBuf, CFileLog::FILE_LOG_BUF, pszFormat, vl);
        if (nLen > 0)
        {
            szBuf[CFileLog::FILE_LOG_BUF - 1] = 0;
            nLen = Log(nLevel, szBuf);
            return nLen;
        }
    }
    return 0;
}

#endif // __FILE_LOG_INL__
