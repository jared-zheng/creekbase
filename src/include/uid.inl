// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __UID_INL__
#define __UID_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CUUID

SELECTANY CPCXStr CUUID::FROMAT_DIGITS_BRACES  = TF("{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}");
SELECTANY CPCXStr CUUID::FROMAT_DIGITS_HYPHENS = TF("%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X");
SELECTANY CPCXStr CUUID::FROMAT_DIGITS_AVERAGE = TF("%08X-%04X%04X-%02X%02X%02X%02X-%02X%02X%02X%02X");
SELECTANY CPCXStr CUUID::FROMAT_DIGITS_ONLY    = TF("%08X%04X%04X%02X%02X%02X%02X%02X%02X%02X%02X");

INLINE CUUID::CUUID(void)
{
    MM_SAFE::Set(&m_UUID, 0, sizeof(UID));
}

INLINE CUUID::~CUUID(void)
{
}

INLINE CUUID::CUUID(const UID& uuid)
{
    m_UUID = uuid;
}

INLINE CUUID::CUUID(PCXStr pszUUID)
{
    ToUUID(pszUUID);
}

INLINE CUUID::CUUID(const CUUID& aSrc)
{
    m_UUID = aSrc.m_UUID;
}

INLINE CUUID& CUUID::operator=(const CUUID& aSrc)
{
    if (this != &aSrc)
    {
        m_UUID = aSrc.m_UUID;
    }
    return (*this);
}

INLINE bool CUUID::operator==(const CUUID& aSrc) const
{
    return (MM_SAFE::Cmp(&m_UUID, &(aSrc.m_UUID), sizeof(UID)) == 0);
}

INLINE bool CUUID::operator!=(const CUUID& aSrc) const
{
    return (MM_SAFE::Cmp(&m_UUID, &(aSrc.m_UUID), sizeof(UID)) != 0);
}

INLINE bool CUUID::operator==(const UID& uuid) const
{
    return (MM_SAFE::Cmp(&m_UUID, &uuid, sizeof(UID)) == 0);
}

INLINE bool CUUID::operator!=(const UID& uuid) const
{
    return (MM_SAFE::Cmp(&m_UUID, &uuid, sizeof(UID)) != 0);
}

INLINE void CUUID::Serialize(CStream& stream)
{
    if (stream.IsRead())
    {
        stream.Read(&m_UUID, sizeof(UID));
    }
    else if (stream.IsWrite())
    {
        stream.Write(&m_UUID, sizeof(UID));
    }
}

INLINE bool CUUID::IsValid(void) const
{
    const ULLong* pullData = reinterpret_cast<const ULLong*>(&m_UUID);
    return ((pullData[0] != 0) && (pullData[1] != 0));
}

INLINE void CUUID::GetUUID(UID& uuid) const
{
    uuid = m_UUID;
}

INLINE void CUUID::SetUUID(const UID& uuid)
{
    m_UUID = uuid;
}

INLINE void CUUID::Reset(void)
{
    MM_SAFE::Set(&m_UUID, 0, sizeof(UID));
}

INLINE void CUUID::ToString(CString& str, bool bAppend, UUID_FROMAT eFormat) const
{
    PCXStr pszFormat = nullptr;
    switch (eFormat)
    {
    case UUID_FROMAT_DIGITS_ONLY:
        {
            pszFormat = FROMAT_DIGITS_ONLY;
        }
        break;
    case UUID_FROMAT_DIGITS_AVERAGE:
        {
            pszFormat = FROMAT_DIGITS_AVERAGE;
        }
        break;
    case UUID_FROMAT_DIGITS_HYPHENS:
        {
            pszFormat = FROMAT_DIGITS_HYPHENS;
        }
        break;
    case UUID_FROMAT_DIGITS_BRACES:
    default:
        {
            pszFormat = FROMAT_DIGITS_BRACES;
        }
    }
    if (bAppend)
    {
        str.AppendFormat(pszFormat,
                         m_UUID.Data1, m_UUID.Data2, m_UUID.Data3,
                         m_UUID.Data4[0], m_UUID.Data4[1],
                         m_UUID.Data4[2], m_UUID.Data4[3],
                         m_UUID.Data4[4], m_UUID.Data4[5],
                         m_UUID.Data4[6], m_UUID.Data4[7]);
    }
    else
    {
        str.Format(pszFormat,
                   m_UUID.Data1, m_UUID.Data2, m_UUID.Data3,
                   m_UUID.Data4[0], m_UUID.Data4[1],
                   m_UUID.Data4[2], m_UUID.Data4[3],
                   m_UUID.Data4[4], m_UUID.Data4[5],
                   m_UUID.Data4[6], m_UUID.Data4[7]);
    }
}

template <size_t stLenT>
INLINE void CUUID::ToString(CTStringFix<CXChar, stLenT>& strFix, bool bAppend, UUID_FROMAT eFormat) const
{
    PCXStr pszFormat = nullptr;
    switch (eFormat)
    {
    case UUID_FROMAT_DIGITS_ONLY:
        {
            pszFormat = FROMAT_DIGITS_ONLY;
        }
        break;
    case UUID_FROMAT_DIGITS_AVERAGE:
        {
            pszFormat = FROMAT_DIGITS_AVERAGE;
        }
        break;
    case UUID_FROMAT_DIGITS_HYPHENS:
        {
            pszFormat = FROMAT_DIGITS_HYPHENS;
        }
        break;
    case UUID_FROMAT_DIGITS_BRACES:
    default:
        {
            pszFormat = FROMAT_DIGITS_BRACES;
        }
    }
    if (bAppend)
    {
        strFix.AppendFormat(pszFormat,
                            m_UUID.Data1, m_UUID.Data2, m_UUID.Data3,
                            m_UUID.Data4[0], m_UUID.Data4[1],
                            m_UUID.Data4[2], m_UUID.Data4[3],
                            m_UUID.Data4[4], m_UUID.Data4[5],
                            m_UUID.Data4[6], m_UUID.Data4[7]);
    }
    else
    {
        strFix.Format(pszFormat,
                      m_UUID.Data1, m_UUID.Data2, m_UUID.Data3,
                      m_UUID.Data4[0], m_UUID.Data4[1],
                      m_UUID.Data4[2], m_UUID.Data4[3],
                      m_UUID.Data4[4], m_UUID.Data4[5],
                      m_UUID.Data4[6], m_UUID.Data4[7]);
    }
}

INLINE void CUUID::ToString(PXStr pszBuf, size_t stLen, UUID_FROMAT eFormat) const
{
    assert(stLen >= (size_t)UUIDC_LEN_MAX);
    assert(pszBuf != nullptr);

    PCXStr pszFormat = nullptr;
    switch (eFormat)
    {
    case UUID_FROMAT_DIGITS_ONLY:
        {
            pszFormat = FROMAT_DIGITS_ONLY;
        }
        break;
    case UUID_FROMAT_DIGITS_AVERAGE:
        {
            pszFormat = FROMAT_DIGITS_AVERAGE;
        }
        break;
    case UUID_FROMAT_DIGITS_HYPHENS:
        {
            pszFormat = FROMAT_DIGITS_HYPHENS;
        }
        break;
    case UUID_FROMAT_DIGITS_BRACES:
    default:
        {
            pszFormat = FROMAT_DIGITS_BRACES;
        }
    }
    CXChar::Format(pszBuf, stLen, pszFormat,
                   m_UUID.Data1, m_UUID.Data2, m_UUID.Data3,
                   m_UUID.Data4[0], m_UUID.Data4[1],
                   m_UUID.Data4[2], m_UUID.Data4[3],
                   m_UUID.Data4[4], m_UUID.Data4[5],
                   m_UUID.Data4[6], m_UUID.Data4[7]);
}

INLINE bool CUUID::ToUUID(PCXStr pszBuf, UUID_FROMAT eFormat)
{
    assert(pszBuf != nullptr);
    switch (eFormat)
    {
    case UUID_FROMAT_DIGITS_BRACES:
        {
            if ((pszBuf     == nullptr) ||
                (pszBuf[0]  != TF('{')) ||
                (pszBuf[9]  != TF('-')) ||
                (pszBuf[14] != TF('-')) ||
                (pszBuf[19] != TF('-')) ||
                (pszBuf[24] != TF('-')) ||
                (pszBuf[37] != TF('}')) )
            {
                return false;
            }
            PXStr pszEnd    = nullptr;
            m_UUID.Data1    = CXChar::ToULong((pszBuf + 1), &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data2    = (UShort)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data3    = (UShort)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;

            ULLong ullData  = CXChar::ToULLong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data4[0] = (UChar)(ullData >> 8);
            m_UUID.Data4[1] = (UChar)(ullData);

            ullData         = CXChar::ToULLong(pszEnd, &pszEnd, RADIXT_HEX);
            m_UUID.Data4[2] = (UChar)(ullData >> 40);
            m_UUID.Data4[3] = (UChar)(ullData >> 32);
            m_UUID.Data4[4] = (UChar)(ullData >> 24);
            m_UUID.Data4[5] = (UChar)(ullData >> 16);
            m_UUID.Data4[6] = (UChar)(ullData >> 8);
            m_UUID.Data4[7] = (UChar)(ullData);
        }
        break;
    case UUID_FROMAT_DIGITS_HYPHENS:
        {
            if ((pszBuf     == nullptr) ||
                (pszBuf[8]  != TF('-')) ||
                (pszBuf[13] != TF('-')) ||
                (pszBuf[18] != TF('-')) ||
                (pszBuf[23] != TF('-')) )
            {
                return false;
            }
            PXStr pszEnd    = nullptr;
            m_UUID.Data1    = CXChar::ToULong(pszBuf, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data2    = (UShort)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data3    = (UShort)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;

            ULLong ullData  = CXChar::ToULLong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data4[0] = (UChar)(ullData >> 8);
            m_UUID.Data4[1] = (UChar)(ullData);

            ullData         = CXChar::ToULLong(pszEnd, &pszEnd, RADIXT_HEX);
            m_UUID.Data4[2] = (UChar)(ullData >> 40);
            m_UUID.Data4[3] = (UChar)(ullData >> 32);
            m_UUID.Data4[4] = (UChar)(ullData >> 24);
            m_UUID.Data4[5] = (UChar)(ullData >> 16);
            m_UUID.Data4[6] = (UChar)(ullData >> 8);
            m_UUID.Data4[7] = (UChar)(ullData);
        }
        break;
    case UUID_FROMAT_DIGITS_AVERAGE:
        {
            if ((pszBuf     == nullptr) ||
                (pszBuf[8]  != TF('-')) ||
                (pszBuf[17] != TF('-')) ||
                (pszBuf[26] != TF('-')) )
            {
                return false;
            }
            PXStr pszEnd    = nullptr;
            m_UUID.Data1    = CXChar::ToULong(pszBuf, &pszEnd, RADIXT_HEX); ++pszEnd;
            UInt   uData    = (UInt)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;

            m_UUID.Data2    = (UShort)(uData >> 16);
            m_UUID.Data3    = (UShort)(uData);

            uData           = (UInt)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data4[0] = (UChar)(uData >> 24);
            m_UUID.Data4[1] = (UChar)(uData >> 16);
            m_UUID.Data4[2] = (UChar)(uData >> 8);
            m_UUID.Data4[3] = (UChar)(uData);

            uData           = (UInt)CXChar::ToULong(pszEnd, &pszEnd, RADIXT_HEX); ++pszEnd;
            m_UUID.Data4[4] = (UChar)(uData >> 24);
            m_UUID.Data4[5] = (UChar)(uData >> 16);
            m_UUID.Data4[6] = (UChar)(uData >> 8);
            m_UUID.Data4[7] = (UChar)(uData);
        }
        break;
    case UUID_FROMAT_DIGITS_ONLY:
        {
            if (pszBuf == nullptr)
            {
                return false;
            }
            XChar szUUID[UUIDC_LEN_ONLY] = { 0 };
            CXChar::Copy(szUUID, UUIDC_LEN_ONLY, pszBuf, UUIDC_LEN_HEX);
            PXStr pszEnd    = nullptr;
            ULLong ullData1 = CXChar::ToULLong(szUUID, &pszEnd, RADIXT_HEX);
            if (pszEnd != (szUUID + UUIDC_LEN_HEX))
            {
                return false;
            }
            pszEnd = (PXStr)(pszBuf + UUIDC_LEN_HEX);
            ULLong ullData2 = CXChar::ToULLong(pszEnd, &pszEnd, RADIXT_HEX);
            if (pszEnd != (pszBuf + UUIDC_LEN_ONLY))
            {
                return false;
            }

            m_UUID.Data1    = (ULong)(ullData1 >> 32);
            m_UUID.Data2    = (UShort)(ullData1 >> 16);
            m_UUID.Data3    = (UShort)(ullData1);
            m_UUID.Data4[0] = (UChar)(ullData2 >> 56);
            m_UUID.Data4[1] = (UChar)(ullData2 >> 48);
            m_UUID.Data4[2] = (UChar)(ullData2 >> 40);
            m_UUID.Data4[3] = (UChar)(ullData2 >> 32);
            m_UUID.Data4[4] = (UChar)(ullData2 >> 24);
            m_UUID.Data4[5] = (UChar)(ullData2 >> 16);
            m_UUID.Data4[6] = (UChar)(ullData2 >> 8);
            m_UUID.Data4[7] = (UChar)(ullData2);
        }
        break;
    default:
        {
            return false;
        }
    }
    return true;
}

#endif // __UID_INL__
