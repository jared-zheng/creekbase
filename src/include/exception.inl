// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __EXCEPTION_INL__
#define __EXCEPTION_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CException
INLINE CException::CException(UInt uCode)
: m_uCode(uCode)
{
}

INLINE CException::CException(UInt uCode, PCXStr pszInfo)
: m_uCode(uCode)
, m_strInfo(pszInfo)
{
}

INLINE CException::CException(UInt uCode, const CString& strInfo)
: m_uCode(uCode)
, m_strInfo(strInfo)
{
}

INLINE CException::~CException(void)
{
}

INLINE CException::CException(const CException& aSrc)
: m_uCode(aSrc.m_uCode)
, m_strInfo(aSrc.m_strInfo)
{
}

INLINE CException& CException::operator=(const CException& aSrc)
{
    if (this != &aSrc)
    {
        m_uCode   = aSrc.m_uCode;
        m_strInfo = aSrc.m_strInfo;
    }
    return (*this);
}

INLINE UInt CException::GetCode(void) const
{
    return m_uCode;
}

INLINE void CException::SetCode(UInt uCode)
{
    m_uCode = uCode;
}

INLINE const CString& CException::GetInfo(void) const
{
    return m_strInfo;
}

INLINE void CException::SetInfo(PCXStr pszInfo)
{
    m_strInfo = pszInfo;
}

INLINE void CException::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> m_uCode >> m_strInfo;
    }
    else
    {
        Stream << m_uCode << m_strInfo;
    }
}

#endif // __EXCEPTION_INL__