// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __DEF_H__
#define __DEF_H__

#pragma once

#include "commondef.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetdef.h"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetdef.h"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// msic
#define TF_FILE                       TF(__FILE__)
#define TF_FUNC                       TF(__FUNCTION__)
#define ST_LINE                       (size_t)(__LINE__)
#define CASE_TOSTR( x )               case x: return TF(#x);
#define CASE_RETSTR( x, desc )        case x: return desc;

///////////////////////////////////////////////////////////////////
//
typedef   Char*                       PStr;
typedef   const Char*                 PCStr;
typedef   const Char* const           CPCStr;
typedef   UChar*                      PUStr;
typedef   const UChar*                PCUStr;
typedef   const UChar* const          CPCUStr;
typedef   WChar*                      PWStr;
typedef   const WChar*                PCWStr;
typedef   const WChar* const          CPCWStr;
typedef   XChar*                      PXStr;
typedef   const XChar*                PCXStr;
typedef   const XChar* const          CPCXStr;

///////////////////////////////////////////////////////////////////
// global INLINE functions
class DEF
{
public:
    template <typename T> static T Abs(const T t);
    template <typename T> static T Max(const T a, const T b);
    template <typename T> static T Min(const T a, const T b);
    template <typename T> static T Maxmin(const T maxa, const T maxb, const T minc);
    template <typename T> static T Align(const T t, const T align);
    template <typename T> static T Swap(T& a, T& b);
};

///////////////////////////////////////////////////////////////////
// convertible detection
template <typename X, typename T>
struct TConvertible
{
public:
    typedef Char  YES;
    typedef Short NO;

    static  YES   Convert(T*);
    static  NO    Convert(...);
public:
    enum CONVERTIBLE
    {
        VALUE = sizeof( (Convert)(static_cast<X*>(nullptr)) ) == sizeof(YES)
    };
};

struct ConvertibleTag { public: bool Check(void) { return true; } };

template <bool> struct TConvertibleImp;
template <>     struct TConvertibleImp<true>  { typedef ConvertibleTag TAG; };
template <>     struct TConvertibleImp<false> {                             };

template <typename X, typename T>
struct TConvertCheck : public TConvertibleImp<TConvertible<X, T>::VALUE> { };

///////////////////////////////////////////////////////////////////
#include "def.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __DEF_H__
