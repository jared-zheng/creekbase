// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __THREAD_INL__
#define __THREAD_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CRunnable
INLINE CRunnable::CRunnable(void)
{
}

INLINE CRunnable::~CRunnable(void)
{
}

INLINE CRunnable::CRunnable(const CRunnable& aSrc)
: CTRefCount<CRunnable>(aSrc)
{
}

INLINE CRunnable& CRunnable::operator=(const CRunnable& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CRunnable>::operator=(aSrc);
    }
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CQueueTask
INLINE CQueueTask::CQueueTask(void)
{
}

INLINE CQueueTask::~CQueueTask(void)
{
}

INLINE CQueueTask::CQueueTask(const CQueueTask& aSrc)
: CTRefCount<CQueueTask>(aSrc)
{
}

INLINE CQueueTask& CQueueTask::operator=(const CQueueTask& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CQueueTask>::operator=(aSrc);
    }
    return (*this);
}

#endif // __THREAD_INL__