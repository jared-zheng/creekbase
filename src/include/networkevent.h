// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_EVENT_H__
#define __NETWORK_EVENT_H__

#pragma once

#include "network.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETPacket : SOCKET_RECV_EVENT
class CNETPacket : public CEventBase
{
public:
    static CNETPacket* Create(uintptr_t, CStream&, ULLong);
public:
    CNETPacket(PByte pData = nullptr, size_t stSize = 0);
    virtual ~CNETPacket(void);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);
private:
    CNETPacket(const CNETPacket& aSrc);
    CNETPacket& operator=(const CNETPacket& aSrc);
private:
    PByte    m_pData;
    size_t   m_stSize;
};

///////////////////////////////////////////////////////////////////
// CTNETDispatch
template <typename TPACKET, bool bNetByteOrder>
class NOVTABLE CTNETDispatch : public CNETTraits
{
public:
    enum DUMP_HEX
    {
        DUMP_HEX_DWORD = 4,
        DUMP_HEX_LINE  = 32,
    };

    typedef CTRefCountPtr<TPACKET> PacketPtr;
public:
    virtual bool OnDispatch(uintptr_t utEvent, CStream& Stream, ULLong ullParam);
    virtual bool OnEvent(uintptr_t utEvent, uintptr_t utData, ULLong ullParam);
protected:
    CTNETDispatch(void);
    virtual ~CTNETDispatch(void);

    virtual bool OnTcpDispatch(const PacketPtr& PktPtr, PTCP_PARAM pTcp);
    virtual bool OnUdpDispatch(const PacketPtr& PktPtr, PUDP_PARAM pUdp);

    virtual bool OnTcpAccept(Socket sAccept, Socket sListen);
    virtual bool OnTcpConnect(UInt uError, Socket sConnect);
    virtual bool OnTcpRecv(size_t stSize, PTCP_PARAM pTcp);
    virtual bool OnTcpSend(UInt uQueueLimit, Socket sSocket); //uQueueLimit--ATTR_MAX_SEND, NET_ATTR.nMaxSend
    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData);

    virtual bool OnUdpRecv(size_t stSize, PUDP_PARAM pUdp);
    virtual bool OnUdpSend(UInt uQueueLimit, Socket sSocket); //uQueueLimit--ATTR_MAX_SEND, NET_ATTR.nMaxSend
    virtual bool OnUdpClose(Socket sSocket, ULLong ullLiveData);

    virtual bool OnTlsHandShake(UInt uError, Socket sSocket);
    virtual bool OnTlsRecv(size_t stSize, PTCP_PARAM pTcp);
    virtual bool OnTlsSend(size_t stSize, PTCP_PARAM pTcp);
    virtual bool OnTlsError(UInt uError, Socket sSocket);

    virtual bool OnBeforeUpgrade(uintptr_t utSessionPtr, ULLong ullType);
    virtual bool OnAfterUpgrade(uintptr_t utSessionPtr, ULLong ullType);
    virtual bool OnWebSession(uintptr_t utSessionPtr, ULLong ullType);

    void    DumpHexPacketData(uintptr_t utEvent, CStream& Stream, ULLong ullParam);
private:
    CTNETDispatch(const CTNETDispatch&);
    CTNETDispatch& operator=(const CTNETDispatch&);
};

///////////////////////////////////////////////////////////////////
// CTNetworkEventHandler
template <typename TDISPATCH>
class NOVTABLE CTNetworkEventHandler : public CEventHandler, public TDISPATCH
{
    typedef TDISPATCH TDispath;
public:
    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount) OVERRIDE;
protected:
    CTNetworkEventHandler(void);
    virtual ~CTNetworkEventHandler(void);
};

typedef CTNetworkEventHandler<CTNETDispatch<CNETPacket, false> > CNulPackNetworkEventHandler;
typedef CTNetworkEventHandler<CTNETDispatch<CNETPacket, true> >  CNulPackNetOrderNetworkEventHandler;

///////////////////////////////////////////////////////////////////
#include "networkevent.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_EVENT_H__
