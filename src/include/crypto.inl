// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CRYPTO_INL__
#define __CRYPTO_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CMD5
SELECTANY CPCXStr CMD5::StrFormat = TF("%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X");

INLINE CMD5::CMD5(void)
{
}

INLINE CMD5::~CMD5(void)
{
}

INLINE CMD5::CMD5(const CMD5&)
{
}

INLINE CMD5& CMD5::operator=(const CMD5&)
{
    return (*this);
}

INLINE bool CMD5::Crypto(const CString& strSrc, CString& str, size_t stSize, bool bAppend)
{
    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    Byte bOut[MD5C_NUM] = { 0 };
    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, bOut);

    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7],
                         bOut[8],  bOut[9],  bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15]);
    }
    else
    {
        str.Format(StrFormat,
                   bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7],
                   bOut[8],  bOut[9],  bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15]);
    }
    return true;
}

INLINE bool CMD5::Crypto(const CString& strSrc, CStream& StreamDst, size_t stSize)
{
    assert(StreamDst.IsWrite());
    assert(StreamDst.Rest() >= MD5C_NUM);

    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    Byte bOut[MD5C_NUM] = { 0 };
    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, bOut);

    return (StreamDst.Write(bOut, MD5C_NUM) > 0);
}

INLINE bool CMD5::Crypto(const CString& strSrc, CMD5::NUM& Num, size_t stSize)
{
    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, Num);
    return true;
}

INLINE bool CMD5::Crypto(CStream& StreamSrc, CString& str, size_t stSize, bool bAppend)
{
    Byte bOut[MD5C_NUM] = { 0 };
    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, bOut);

    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7],
                         bOut[8],  bOut[9],  bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15]);
    }
    else
    {
        str.Format(StrFormat,
                   bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7],
                   bOut[8],  bOut[9],  bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15]);
    }
    return true;
}

INLINE bool CMD5::Crypto(CStream& StreamSrc, CStream& StreamDst, size_t stSize)
{
    assert(StreamDst.IsWrite());
    assert(StreamDst.Rest() >= MD5C_NUM);

    Byte bOut[MD5C_NUM] = { 0 };
    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, bOut);

    return (StreamDst.Write(bOut, MD5C_NUM) > 0);
}

INLINE bool CMD5::Crypto(CStream& StreamSrc, CMD5::NUM& Num, size_t stSize)
{
    MD5_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, Num);
    return true;
}

INLINE bool CMD5::ToString(const CMD5::NUM& Num, CString& str, bool bAppend)
{
    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],
                         Num[8],  Num[9],  Num[10], Num[11], Num[12], Num[13], Num[14], Num[15]);
    }
    else
    {
        str.Format(StrFormat,
                   Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],
                   Num[8],  Num[9],  Num[10], Num[11], Num[12], Num[13], Num[14], Num[15]);
    }
    return true;
}

INLINE bool CMD5::ToString(const CMD5::NUM& Num, CString& str, PCXStr pszFormat, bool bAppend)
{
    if (bAppend)
    {
        str.AppendFormat(pszFormat,
                         Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],
                         Num[8],  Num[9],  Num[10], Num[11], Num[12], Num[13], Num[14], Num[15]);
    }
    else
    {
        str.Format(pszFormat,
                   Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],
                   Num[8],  Num[9],  Num[10], Num[11], Num[12], Num[13], Num[14], Num[15]);
    }
    return true;
}

INLINE void CMD5::Init(MD5_CONTEXT& ctx)
{
    // http://www.ietf.org/rfc/rfc1321.txt
    // Zeroize sensitive information
    MM_SAFE::Set(&ctx, 0, sizeof(MD5_CONTEXT));

    ctx.uState[0] = 0X67452301;
    ctx.uState[1] = 0XEFCDAB89;
    ctx.uState[2] = 0X98BADCFE;
    ctx.uState[3] = 0X10325476;
}

INLINE void CMD5::Update(MD5_CONTEXT& ctx, CStream& StreamSrc, size_t stSize)
{
    assert(StreamSrc.IsRead());
    size_t stLen = StreamSrc.Rest();
    if (stSize > 0)
    {
        stSize = DEF::Min<size_t>(stLen, stSize);
    }
    else
    {
        stSize = stLen;
    }
    if (stSize > 0)
    {
        UInt   uIndex  = (UInt)(ctx.uTotal[0] & 0X3F);
        size_t stFill  = 64 - uIndex;

        ctx.uTotal[0] += (UInt)(stSize);
        ctx.uTotal[0] &= UINT_MAX;
        if (ctx.uTotal[0] < (UInt)(stSize))
        {
            ++ctx.uTotal[1];
        }

        if ((uIndex > 0) && (stSize >= stFill))
        {
            StreamSrc.Read((ctx.bBuffer + uIndex), stFill);
            Transform(ctx, ctx.bBuffer);
            stSize -= stFill;
            uIndex  = 0;
        }

        Byte bBuffer[64] = { 0 };
        while (stSize >= 64)
        {
            StreamSrc.Read(bBuffer, 64);
            Transform(ctx, bBuffer);

            stSize -= 64;
        }

        if (stSize > 0)
        {
            StreamSrc.Read((ctx.bBuffer + uIndex), stSize);
        }
    }
}

INLINE void CMD5::Final(MD5_CONTEXT& ctx, PByte pOut)
{
    UInt uIndex = (UInt)(ctx.uTotal[0] & 0X3F);
    ctx.bBuffer[uIndex] = 0x80;
    ++uIndex;

    if (uIndex <= 56)
    {
        MM_SAFE::Set((ctx.bBuffer + uIndex), 0, (56 - uIndex));
    }
    else
    {
        MM_SAFE::Set((ctx.bBuffer + uIndex), 0, (64 - uIndex));
        Transform(ctx, ctx.bBuffer);
        MM_SAFE::Set(ctx.bBuffer, 0, 56);
    }

    UInt uHigh = (ctx.uTotal[0] >> 29) | (ctx.uTotal[1] << 3);
    UInt uLow  = (ctx.uTotal[0] << 3);

    FromUInt((ctx.bBuffer + 56), uLow);
    FromUInt((ctx.bBuffer + 60), uHigh);

    Transform(ctx, ctx.bBuffer);

    FromUInt((pOut + 0 ), ctx.uState[0]);
    FromUInt((pOut + 4 ), ctx.uState[1]);
    FromUInt((pOut + 8 ), ctx.uState[2]);
    FromUInt((pOut + 12), ctx.uState[3]);
}

INLINE void CMD5::Transform(MD5_CONTEXT& ctx, PByte pIn)
{
    UInt a = ctx.uState[0];
    UInt b = ctx.uState[1];
    UInt c = ctx.uState[2];
    UInt d = ctx.uState[3];

    UInt x[16] = { 0 };
    ToUInt(x[ 0], (pIn + 0 ));
    ToUInt(x[ 1], (pIn + 4 ));
    ToUInt(x[ 2], (pIn + 8 ));
    ToUInt(x[ 3], (pIn + 12));
    ToUInt(x[ 4], (pIn + 16));
    ToUInt(x[ 5], (pIn + 20));
    ToUInt(x[ 6], (pIn + 24));
    ToUInt(x[ 7], (pIn + 28));
    ToUInt(x[ 8], (pIn + 32));
    ToUInt(x[ 9], (pIn + 36));
    ToUInt(x[10], (pIn + 40));
    ToUInt(x[11], (pIn + 44));
    ToUInt(x[12], (pIn + 48));
    ToUInt(x[13], (pIn + 52));
    ToUInt(x[14], (pIn + 56));
    ToUInt(x[15], (pIn + 60));


    T1(a, b, c, d, x[ 0], (UInt)7,  (UInt)0XD76AA478); // 1
    T1(d, a, b, c, x[ 1], (UInt)12, (UInt)0XE8C7B756); // 2
    T1(c, d, a, b, x[ 2], (UInt)17, (UInt)0X242070DB); // 3
    T1(b, c, d, a, x[ 3], (UInt)22, (UInt)0XC1BDCEEE); // 4
    T1(a, b, c, d, x[ 4], (UInt)7,  (UInt)0XF57C0FAF); // 5
    T1(d, a, b, c, x[ 5], (UInt)12, (UInt)0X4787C62A); // 6
    T1(c, d, a, b, x[ 6], (UInt)17, (UInt)0XA8304613); // 7
    T1(b, c, d, a, x[ 7], (UInt)22, (UInt)0XFD469501); // 8
    T1(a, b, c, d, x[ 8], (UInt)7,  (UInt)0X698098D8); // 9
    T1(d, a, b, c, x[ 9], (UInt)12, (UInt)0X8B44F7AF); // 10
    T1(c, d, a, b, x[10], (UInt)17, (UInt)0XFFFF5BB1); // 11
    T1(b, c, d, a, x[11], (UInt)22, (UInt)0X895CD7BE); // 12
    T1(a, b, c, d, x[12], (UInt)7,  (UInt)0X6B901122); // 13
    T1(d, a, b, c, x[13], (UInt)12, (UInt)0XFD987193); // 14
    T1(c, d, a, b, x[14], (UInt)17, (UInt)0XA679438E); // 15
    T1(b, c, d, a, x[15], (UInt)22, (UInt)0X49B40821); // 16

    T2(a, b, c, d, x[ 1], (UInt)5,  (UInt)0XF61E2562); // 17
    T2(d, a, b, c, x[ 6], (UInt)9,  (UInt)0XC040B340); // 18
    T2(c, d, a, b, x[11], (UInt)14, (UInt)0X265E5A51); // 19
    T2(b, c, d, a, x[ 0], (UInt)20, (UInt)0XE9B6C7AA); // 20
    T2(a, b, c, d, x[ 5], (UInt)5,  (UInt)0XD62F105D); // 21
    T2(d, a, b, c, x[10], (UInt)9,  (UInt)0X02441453); // 22
    T2(c, d, a, b, x[15], (UInt)14, (UInt)0XD8A1E681); // 23
    T2(b, c, d, a, x[ 4], (UInt)20, (UInt)0XE7D3FBC8); // 24
    T2(a, b, c, d, x[ 9], (UInt)5,  (UInt)0X21E1CDE6); // 25
    T2(d, a, b, c, x[14], (UInt)9,  (UInt)0XC33707D6); // 26
    T2(c, d, a, b, x[ 3], (UInt)14, (UInt)0XF4D50D87); // 27
    T2(b, c, d, a, x[ 8], (UInt)20, (UInt)0X455A14ED); // 28
    T2(a, b, c, d, x[13], (UInt)5,  (UInt)0XA9E3E905); // 29
    T2(d, a, b, c, x[ 2], (UInt)9,  (UInt)0XFCEFA3F8); // 30
    T2(c, d, a, b, x[ 7], (UInt)14, (UInt)0X676F02D9); // 31
    T2(b, c, d, a, x[12], (UInt)20, (UInt)0X8D2A4C8A); // 32

    T3(a, b, c, d, x[ 5], (UInt)4,  (UInt)0XFFFA3942); // 33
    T3(d, a, b, c, x[ 8], (UInt)11, (UInt)0X8771F681); // 34
    T3(c, d, a, b, x[11], (UInt)16, (UInt)0X6D9D6122); // 35
    T3(b, c, d, a, x[14], (UInt)23, (UInt)0XFDE5380C); // 36
    T3(a, b, c, d, x[ 1], (UInt)4,  (UInt)0XA4BEEA44); // 37
    T3(d, a, b, c, x[ 4], (UInt)11, (UInt)0X4BDECFA9); // 38
    T3(c, d, a, b, x[ 7], (UInt)16, (UInt)0XF6BB4B60); // 39
    T3(b, c, d, a, x[10], (UInt)23, (UInt)0XBEBFBC70); // 40
    T3(a, b, c, d, x[13], (UInt)4,  (UInt)0X289B7EC6); // 41
    T3(d, a, b, c, x[ 0], (UInt)11, (UInt)0XEAA127FA); // 42
    T3(c, d, a, b, x[ 3], (UInt)16, (UInt)0XD4EF3085); // 43
    T3(b, c, d, a, x[ 6], (UInt)23, (UInt)0X04881D05); // 44
    T3(a, b, c, d, x[ 9], (UInt)4,  (UInt)0XD9D4D039); // 45
    T3(d, a, b, c, x[12], (UInt)11, (UInt)0XE6DB99E5); // 46
    T3(c, d, a, b, x[15], (UInt)16, (UInt)0X1FA27CF8); // 47
    T3(b, c, d, a, x[ 2], (UInt)23, (UInt)0XC4AC5665); // 48

    T4(a, b, c, d, x[ 0], (UInt)6,  (UInt)0XF4292244); // 49
    T4(d, a, b, c, x[ 7], (UInt)10, (UInt)0X432AFF97); // 50
    T4(c, d, a, b, x[14], (UInt)15, (UInt)0XAB9423A7); // 51
    T4(b, c, d, a, x[ 5], (UInt)21, (UInt)0XFC93A039); // 52
    T4(a, b, c, d, x[12], (UInt)6,  (UInt)0X655B59C3); // 53
    T4(d, a, b, c, x[ 3], (UInt)10, (UInt)0X8F0CCC92); // 54
    T4(c, d, a, b, x[10], (UInt)15, (UInt)0XFFEFF47D); // 55
    T4(b, c, d, a, x[ 1], (UInt)21, (UInt)0X85845DD1); // 56
    T4(a, b, c, d, x[ 8], (UInt)6,  (UInt)0X6FA87E4F); // 57
    T4(d, a, b, c, x[15], (UInt)10, (UInt)0XFE2CE6E0); // 58
    T4(c, d, a, b, x[ 6], (UInt)15, (UInt)0XA3014314); // 59
    T4(b, c, d, a, x[13], (UInt)21, (UInt)0X4E0811A1); // 60
    T4(a, b, c, d, x[ 4], (UInt)6,  (UInt)0XF7537E82); // 61
    T4(d, a, b, c, x[11], (UInt)10, (UInt)0XBD3AF235); // 62
    T4(c, d, a, b, x[ 2], (UInt)15, (UInt)0X2AD7D2BB); // 63
    T4(b, c, d, a, x[ 9], (UInt)21, (UInt)0XEB86D391); // 64

    ctx.uState[0] += a;
    ctx.uState[1] += b;
    ctx.uState[2] += c;
    ctx.uState[3] += d;
}

INLINE void CMD5::FromUInt(PByte pOut, UInt uIn)
{
    pOut[0] = (Byte)((uIn      ) & 0XFF);
    pOut[1] = (Byte)((uIn >> 8 ) & 0XFF);
    pOut[2] = (Byte)((uIn >> 16) & 0XFF);
    pOut[3] = (Byte)((uIn >> 24) & 0XFF);
}

INLINE void CMD5::ToUInt(UInt& uOut, PByte pIn)
{
    uOut = ((UInt)pIn[0]      ) |
           ((UInt)pIn[1] << 8 ) |
           ((UInt)pIn[2] << 16) |
           ((UInt)pIn[3] << 24);
}

INLINE UInt CMD5::F1(UInt x, UInt y, UInt z)
{
    return (z ^ (x & (y ^ z)));
}

INLINE UInt CMD5::F2(UInt x, UInt y, UInt z)
{
    return (y ^ (z & (x ^ y)));
}

INLINE UInt CMD5::F3(UInt x, UInt y, UInt z)
{
    return (x ^ y ^ z);
}

INLINE UInt CMD5::F4(UInt x, UInt y, UInt z)
{
    return (y ^(x | (~z)));
}

INLINE UInt CMD5::S(UInt x, UInt n)
{
    return ((x << n) | (x >> (32 - n)));
}

INLINE void CMD5::T1(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t)
{
    a += (F1(b, c, d) + x + t);
    a  = S(a, s);
    a += b;
}

INLINE void CMD5::T2(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t)
{
    a += (F2(b, c, d) + x + t);
    a  = S(a, s);
    a += b;
}

INLINE void CMD5::T3(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t)
{
    a += (F3(b, c, d) + x + t);
    a  = S(a, s);
    a += b;
}

INLINE void CMD5::T4(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t)
{
    a += (F4(b, c, d) + x + t);
    a  = S(a, s);
    a += b;
}

///////////////////////////////////////////////////////////////////
// CSHA1
SELECTANY CPCXStr CSHA1::StrFormat = TF("%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X");

INLINE CSHA1::CSHA1(void)
{
}

INLINE CSHA1::~CSHA1(void)
{
}

INLINE CSHA1::CSHA1(const CSHA1&)
{
}

INLINE CSHA1& CSHA1::operator=(const CSHA1&)
{
    return (*this);
}

INLINE bool CSHA1::Crypto(const CString& strSrc, CString& str, size_t stSize, bool bAppend)
{
    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    Byte bOut[SHA1C_NUM] = { 0 };
    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, bOut);

    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7], bOut[8], bOut[9],
                         bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15], bOut[16], bOut[17], bOut[18], bOut[19]);
    }
    else
    {
        str.Format(StrFormat,
                   bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7], bOut[8], bOut[9],
                   bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15], bOut[16], bOut[17], bOut[18], bOut[19]);
    }
    return true;
}

INLINE bool CSHA1::Crypto(const CString& strSrc, CStream& StreamDst, size_t stSize)
{
    assert(StreamDst.IsWrite());
    assert(StreamDst.Rest() >= SHA1C_NUM);

    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    Byte bOut[SHA1C_NUM] = { 0 };
    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, bOut);

    return (StreamDst.Write(bOut, SHA1C_NUM) > 0);
}

INLINE bool CSHA1::Crypto(const CString& strSrc, CSHA1::NUM& Num, size_t stSize)
{
    CBufReadStream brs;
    if (strSrc.Length() > 0)
    {
        brs.Attach(strSrc.Length(), (PByte)strSrc.GetBuffer());
    }

    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, brs, stSize);
    Final(ctx, Num);
    return true;
}

INLINE bool CSHA1::Crypto(CStream& StreamSrc, CString& str, size_t stSize, bool bAppend)
{
    Byte bOut[SHA1C_NUM] = { 0 };
    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, bOut);

    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7], bOut[8], bOut[9],
                         bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15], bOut[16], bOut[17], bOut[18], bOut[19]);
    }
    else
    {
        str.Format(StrFormat,
                   bOut[0],  bOut[1],  bOut[2],  bOut[3],  bOut[4],  bOut[5],  bOut[6],  bOut[7], bOut[8], bOut[9],
                   bOut[10], bOut[11], bOut[12], bOut[13], bOut[14], bOut[15], bOut[16], bOut[17], bOut[18], bOut[19]);
    }
    return true;
}

INLINE bool CSHA1::Crypto(CStream& StreamSrc, CStream& StreamDst, size_t stSize)
{
    assert(StreamDst.IsWrite());
    assert(StreamDst.Rest() >= SHA1C_NUM);

    Byte bOut[SHA1C_NUM] = { 0 };
    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, bOut);

    return (StreamDst.Write(bOut, SHA1C_NUM) > 0);
}

INLINE bool CSHA1::Crypto(CStream& StreamSrc, CSHA1::NUM& Num, size_t stSize)
{
    SHA1_CONTEXT ctx;
    Init(ctx);
    Update(ctx, StreamSrc, stSize);
    Final(ctx, Num);
    return true;
}

INLINE bool CSHA1::ToString(const CSHA1::NUM& Num, CString& str, bool bAppend)
{
    if (bAppend)
    {
        str.AppendFormat(StrFormat,
                         Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],  Num[8],  Num[9],
                         Num[10], Num[11], Num[12], Num[13], Num[14], Num[15], Num[16], Num[17], Num[18], Num[19]);
    }
    else
    {
        str.Format(StrFormat,
                   Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],  Num[8],  Num[9],
                   Num[10], Num[11], Num[12], Num[13], Num[14], Num[15], Num[16], Num[17], Num[18], Num[19]);
    }
    return true;
}

INLINE bool CSHA1::ToString(const CSHA1::NUM& Num, CString& str, PCXStr pszFormat, bool bAppend)
{
    if (bAppend)
    {
        str.AppendFormat(pszFormat,
                         Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],  Num[8],  Num[9],
                         Num[10], Num[11], Num[12], Num[13], Num[14], Num[15], Num[16], Num[17], Num[18], Num[19]);
    }
    else
    {
        str.Format(pszFormat,
                   Num[0],  Num[1],  Num[2],  Num[3],  Num[4],  Num[5],  Num[6],  Num[7],  Num[8],  Num[9],
                   Num[10], Num[11], Num[12], Num[13], Num[14], Num[15], Num[16], Num[17], Num[18], Num[19]);
    }
    return true;
}

INLINE void CSHA1::Init(SHA1_CONTEXT& ctx)
{
    MM_SAFE::Set(&ctx, 0, sizeof(SHA1_CONTEXT));

    ctx.uState[0] = 0x67452301;
    ctx.uState[1] = 0xEFCDAB89;
    ctx.uState[2] = 0x98BADCFE;
    ctx.uState[3] = 0x10325476;
    ctx.uState[4] = 0xC3D2E1F0;
}

INLINE void CSHA1::Update(SHA1_CONTEXT& ctx, CStream& StreamSrc, size_t stSize)
{
    assert(StreamSrc.IsRead());
    size_t stLen = StreamSrc.Rest();
    if (stSize > 0)
    {
        stSize = DEF::Min<size_t>(stLen, stSize);
    }
    else
    {
        stSize = stLen;
    }
    if (stSize > 0)
    {
        UInt   uIndex  = (UInt)(ctx.uTotal[0] & 0X3F);
        size_t stFill  = 64 - uIndex;

        ctx.uTotal[0] += (UInt)(stSize);
        ctx.uTotal[0] &= UINT_MAX;
        if (ctx.uTotal[0] < (UInt)(stSize))
        {
            ++ctx.uTotal[1];
        }

        if ((uIndex > 0) && (stSize >= stFill))
        {
            StreamSrc.Read((ctx.bBuffer + uIndex), stFill);
            Transform(ctx, ctx.bBuffer);
            stSize -= stFill;
            uIndex  = 0;
        }

        Byte bBuffer[64] = { 0 };
        while (stSize >= 64)
        {
            StreamSrc.Read(bBuffer, 64);
            Transform(ctx, bBuffer);

            stSize -= 64;
        }

        if (stSize > 0)
        {
            StreamSrc.Read((ctx.bBuffer + uIndex), stSize);
        }
    }
}

INLINE void CSHA1::Final(SHA1_CONTEXT& ctx, PByte pOut)
{
    UInt uIndex = (UInt)(ctx.uTotal[0] & 0X3F);
    ctx.bBuffer[uIndex] = 0x80;
    ++uIndex;

    if (uIndex <= 56)
    {
        MM_SAFE::Set((ctx.bBuffer + uIndex), 0, (56 - uIndex));
    }
    else
    {
        MM_SAFE::Set((ctx.bBuffer + uIndex), 0, (64 - uIndex));
        Transform(ctx, ctx.bBuffer);
        MM_SAFE::Set(ctx.bBuffer, 0, 56);
    }

    UInt uHigh = (ctx.uTotal[0] >> 29) | (ctx.uTotal[1] << 3);
    UInt uLow  = (ctx.uTotal[0] << 3);

    FromUInt((ctx.bBuffer + 56), uHigh);
    FromUInt((ctx.bBuffer + 60), uLow);

    Transform(ctx, ctx.bBuffer);

    FromUInt((pOut + 0 ), ctx.uState[0]);
    FromUInt((pOut + 4 ), ctx.uState[1]);
    FromUInt((pOut + 8 ), ctx.uState[2]);
    FromUInt((pOut + 12), ctx.uState[3]);
    FromUInt((pOut + 16), ctx.uState[4]);
}

INLINE void CSHA1::Transform(SHA1_CONTEXT& ctx, PByte pIn)
{
    UInt a = ctx.uState[0];
    UInt b = ctx.uState[1];
    UInt c = ctx.uState[2];
    UInt d = ctx.uState[3];
    UInt e = ctx.uState[4];

    UInt w[16] = { 0 };
    ToUInt(w[ 0], (pIn + 0 ));
    ToUInt(w[ 1], (pIn + 4 ));
    ToUInt(w[ 2], (pIn + 8 ));
    ToUInt(w[ 3], (pIn + 12));
    ToUInt(w[ 4], (pIn + 16));
    ToUInt(w[ 5], (pIn + 20));
    ToUInt(w[ 6], (pIn + 24));
    ToUInt(w[ 7], (pIn + 28));
    ToUInt(w[ 8], (pIn + 32));
    ToUInt(w[ 9], (pIn + 36));
    ToUInt(w[10], (pIn + 40));
    ToUInt(w[11], (pIn + 44));
    ToUInt(w[12], (pIn + 48));
    ToUInt(w[13], (pIn + 52));
    ToUInt(w[14], (pIn + 56));
    ToUInt(w[15], (pIn + 60));

    T1(a, b, c, d, e, w[ 0]   );
    T1(e, a, b, c, d, w[ 1]   );
    T1(d, e, a, b, c, w[ 2]   );
    T1(c, d, e, a, b, w[ 3]   );
    T1(b, c, d, e, a, w[ 4]   );
    T1(a, b, c, d, e, w[ 5]   );
    T1(e, a, b, c, d, w[ 6]   );
    T1(d, e, a, b, c, w[ 7]   );
    T1(c, d, e, a, b, w[ 8]   );
    T1(b, c, d, e, a, w[ 9]   );
    T1(a, b, c, d, e, w[10]   );
    T1(e, a, b, c, d, w[11]   );
    T1(d, e, a, b, c, w[12]   );
    T1(c, d, e, a, b, w[13]   );
    T1(b, c, d, e, a, w[14]   );
    T1(a, b, c, d, e, w[15]   );
    T1(e, a, b, c, d, R(w, 16));
    T1(d, e, a, b, c, R(w, 17));
    T1(c, d, e, a, b, R(w, 18));
    T1(b, c, d, e, a, R(w, 19));

    T2(a, b, c, d, e, R(w, 20));
    T2(e, a, b, c, d, R(w, 21));
    T2(d, e, a, b, c, R(w, 22));
    T2(c, d, e, a, b, R(w, 23));
    T2(b, c, d, e, a, R(w, 24));
    T2(a, b, c, d, e, R(w, 25));
    T2(e, a, b, c, d, R(w, 26));
    T2(d, e, a, b, c, R(w, 27));
    T2(c, d, e, a, b, R(w, 28));
    T2(b, c, d, e, a, R(w, 29));
    T2(a, b, c, d, e, R(w, 30));
    T2(e, a, b, c, d, R(w, 31));
    T2(d, e, a, b, c, R(w, 32));
    T2(c, d, e, a, b, R(w, 33));
    T2(b, c, d, e, a, R(w, 34));
    T2(a, b, c, d, e, R(w, 35));
    T2(e, a, b, c, d, R(w, 36));
    T2(d, e, a, b, c, R(w, 37));
    T2(c, d, e, a, b, R(w, 38));
    T2(b, c, d, e, a, R(w, 39));

    T3(a, b, c, d, e, R(w, 40));
    T3(e, a, b, c, d, R(w, 41));
    T3(d, e, a, b, c, R(w, 42));
    T3(c, d, e, a, b, R(w, 43));
    T3(b, c, d, e, a, R(w, 44));
    T3(a, b, c, d, e, R(w, 45));
    T3(e, a, b, c, d, R(w, 46));
    T3(d, e, a, b, c, R(w, 47));
    T3(c, d, e, a, b, R(w, 48));
    T3(b, c, d, e, a, R(w, 49));
    T3(a, b, c, d, e, R(w, 50));
    T3(e, a, b, c, d, R(w, 51));
    T3(d, e, a, b, c, R(w, 52));
    T3(c, d, e, a, b, R(w, 53));
    T3(b, c, d, e, a, R(w, 54));
    T3(a, b, c, d, e, R(w, 55));
    T3(e, a, b, c, d, R(w, 56));
    T3(d, e, a, b, c, R(w, 57));
    T3(c, d, e, a, b, R(w, 58));
    T3(b, c, d, e, a, R(w, 59));

    T4(a, b, c, d, e, R(w, 60));
    T4(e, a, b, c, d, R(w, 61));
    T4(d, e, a, b, c, R(w, 62));
    T4(c, d, e, a, b, R(w, 63));
    T4(b, c, d, e, a, R(w, 64));
    T4(a, b, c, d, e, R(w, 65));
    T4(e, a, b, c, d, R(w, 66));
    T4(d, e, a, b, c, R(w, 67));
    T4(c, d, e, a, b, R(w, 68));
    T4(b, c, d, e, a, R(w, 69));
    T4(a, b, c, d, e, R(w, 70));
    T4(e, a, b, c, d, R(w, 71));
    T4(d, e, a, b, c, R(w, 72));
    T4(c, d, e, a, b, R(w, 73));
    T4(b, c, d, e, a, R(w, 74));
    T4(a, b, c, d, e, R(w, 75));
    T4(e, a, b, c, d, R(w, 76));
    T4(d, e, a, b, c, R(w, 77));
    T4(c, d, e, a, b, R(w, 78));
    T4(b, c, d, e, a, R(w, 79));

    ctx.uState[0] += a;
    ctx.uState[1] += b;
    ctx.uState[2] += c;
    ctx.uState[3] += d;
    ctx.uState[4] += e;
}

INLINE void CSHA1::FromUInt(PByte pOut, UInt uIn)
{
    pOut[0] = (Byte)((uIn >> 24) & 0XFF);
    pOut[1] = (Byte)((uIn >> 16) & 0XFF);
    pOut[2] = (Byte)((uIn >> 8 ) & 0XFF);
    pOut[3] = (Byte)((uIn      ) & 0XFF);
}

INLINE void CSHA1::ToUInt(UInt& uOut, PByte pIn)
{
    uOut = ((UInt)pIn[0] << 24) |
           ((UInt)pIn[1] << 16) |
           ((UInt)pIn[2] << 8 ) |
           ((UInt)pIn[3]      );
}

INLINE UInt CSHA1::F1(UInt x, UInt y, UInt z)
{
    return (z ^ (x & (y ^ z)));
}

INLINE UInt CSHA1::F2(UInt x, UInt y, UInt z)
{
    return (x ^ y ^ z);
}

INLINE UInt CSHA1::F3(UInt x, UInt y, UInt z)
{
    return ((x & y) | (z & (x | y)));
}

INLINE UInt CSHA1::F4(UInt x, UInt y, UInt z)
{
    return (x ^ y ^ z);
}

INLINE UInt CSHA1::S(UInt x, UInt n)
{
    return ((x << n) | (x >> (32 - n)));
}

INLINE UInt CSHA1::R(PUInt pw, UInt n)
{
    UInt uTemp = pw[(n - 3) & 0x0F] ^ pw[(n - 8) & 0x0F] ^ pw[(n - 14) & 0x0F] ^ pw[n & 0x0F];
    pw[n & 0x0F] = S(uTemp, 1);
    return pw[n & 0x0F];
}

INLINE void CSHA1::T1(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x)
{
    e += S(a, 5) + F1(b, c, d) + 0x5A827999 + x;
    b  = S(b, 30);
}

INLINE void CSHA1::T2(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x)
{
    e += S(a, 5) + F2(b, c, d) + 0x6ED9EBA1 + x;
    b  = S(b, 30);
}

INLINE void CSHA1::T3(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x)
{
    e += S(a, 5) + F3(b, c, d) + 0x8F1BBCDC + x;
    b  = S(b, 30);
}

INLINE void CSHA1::T4(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x)
{
    e += S(a, 5) + F4(b, c, d) + 0xCA62C1D6 + x;
    b  = S(b, 30);
}

#endif // __CRYPTO_INL__
