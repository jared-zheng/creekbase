// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __OBJECT_INL__
#define __OBJECT_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CSlotEvent
INLINE CSlotEvent::CSlotEvent(CObject* pObj, size_t stType)
: CSlotObject(pObj, (uintptr_t)0, ((stType & (VART_MASK|VART_RTTIDYNAMIC|VART_EVENTQUEUE|VART_EVENTTICK))|VART_FUNCTION))
, m_Index(nullptr)
{
}

INLINE CSlotEvent::CSlotEvent(PCXStr pszClass, PCXStr pszName, size_t stType)
: CSlotObject(pszClass, pszName, (uintptr_t)0, ((stType & (VART_MASK|VART_RTTIDYNAMIC|VART_EVENTQUEUE|VART_EVENTTICK))|VART_FUNCTION))
, m_Index(nullptr)
{
}

INLINE CSlotEvent::~CSlotEvent(void)
{
    assert(m_Index == nullptr);
}

INLINE UInt CSlotEvent::operator()(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
    if (ObjectPtr != nullptr)
    {
        uRet = ObjectPtr->OnHandle(utEvent, utData, ullParam);
    }
    return uRet;
}

INLINE UInt CSlotEvent::operator()(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
    if (ObjectPtr != nullptr)
    {
        uRet = ObjectPtr->OnHandle(utEvent, EventRef, ullParam);
    }
    return uRet;
}

INLINE UInt CSlotEvent::operator()(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
    if (ObjectPtr != nullptr)
    {
        uRet = ObjectPtr->OnHandle(utEvent, Stream, ullParam);
    }
    return uRet;
}

INLINE UInt CSlotEvent::operator()(uintptr_t utEvent, UInt uCount)
{
    UInt uRet = (UInt)RET_ERROR;
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
    if (ObjectPtr != nullptr)
    {
        uRet = ObjectPtr->OnHandle(utEvent, uCount);
    }
    return uRet;
}

INLINE PINDEX CSlotEvent::GetIndex(void) const
{
    return m_Index;
}

INLINE void CSlotEvent::SetIndex(PINDEX index)
{
    m_Index = index;
}

///////////////////////////////////////////////////////////////////
// CTSlotObject
template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(CObject* pObj, uintptr_t utFunc, size_t stType)
: CSlotObject(pObj, utFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(CObject* pObj, CPCXStr pszFunc, size_t stType)
: CSlotObject(pObj, pszFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(const CRTTI& rtti, uintptr_t utFunc, size_t stType)
: CSlotObject(rtti, utFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(const CRTTI& rtti, CPCXStr pszFunc, size_t stType)
: CSlotObject(rtti, pszFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(PCXStr pszClass, PCXStr pszName, uintptr_t utFunc, size_t stType)
: CSlotObject(pszClass, pszName, utFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::CTSlotObject(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
: CSlotObject(pszClass, pszName, pszFunc, stType)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSlotObject<TRET, TARGS...>::~CTSlotObject(void)
{
}

///////////////////////////////////////////////////////////////////
// CTSlotFunc : func ptr
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotFunc<TOBJECT, TRET(TARGS...)>::CTSlotFunc(CObject* pObj, SLOT_FUNC pFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pObj, *(reinterpret_cast<uintptr_t*>(&pFunc)), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|VART_FUNCTION))
, m_pFunc(pFunc)
{
    assert(pObj != nullptr);
    assert(pFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotFunc<TOBJECT, TRET(TARGS...)>::CTSlotFunc(PCXStr pszClass, PCXStr pszName, SLOT_FUNC pFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, pszName, *(reinterpret_cast<uintptr_t*>(&pFunc)), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|VART_FUNCTION))
, m_pFunc(pFunc)
{
    assert(pszClass != nullptr);
    assert(pszName != nullptr);
    assert(pFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotFunc<TOBJECT, TRET(TARGS...)>::~CTSlotFunc(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotFunc<TOBJECT, TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTSlotFunc<TOBJECT, TRET(TARGS...)>::operator()(TARGS... Args)
{
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && EXACT_CLASS(TOBJECT, CObject, ObjectPtr.Get()))
    {
        return (static_cast<TOBJECT*>(ObjectPtr.Get())->*(m_pFunc))(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TOBJECT, TRET, TARGS...>(static_cast<TOBJECT*>(ObjectPtr.Get()), m_pFunc, std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotFunc<TOBJECT, TRET(TARGS...)>::Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args)
{
    if ((pObj != nullptr) && (pFunc != nullptr))
    {
        return (pObj->*(pFunc))(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotFunc<TOBJECT, TRET(TARGS...)>::Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args, bool)
{
    if ((pObj != nullptr) && (pFunc != nullptr))
    {
        (pObj->*(pFunc))(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CTSlotType : get func ptr from rtti-type
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotType<TOBJECT, TRET(TARGS...)>::CTSlotType(CObject* pObj, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pObj, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|VART_RTTITYPE))
, m_pFunc(nullptr)
{
    assert(pObj != nullptr);
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotType<TOBJECT, TRET(TARGS...)>::CTSlotType(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, pszName, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|VART_RTTITYPE))
, m_pFunc(nullptr)
{
    assert(pszClass != nullptr);
    assert(pszName != nullptr);
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotType<TOBJECT, TRET(TARGS...)>::~CTSlotType(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotType<TOBJECT, TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotType<TOBJECT, TRET(TARGS...)>::GetRTTIFunc(void)
{
#ifdef __MODERN_CXX_NOT_SUPPORTED
    const CTRTTIFunc<TOBJECT, SLOT_FUNC> RTTIFunc = CRTTITraits::GetFunc<TOBJ, SLOT_FUNC>(CSlotObject::GetFuncName());
#else
    const CTRTTIFunc<TOBJECT, TRET(TARGS...)> RTTIFunc = CRTTITraits::GetFunc<TOBJECT, TRET, TARGS...>(CSlotObject::GetFuncName());
#endif
    if (RTTIFunc.IsValid())
    {
        m_pFunc = (SLOT_FUNC)RTTIFunc.Get();
        return true;
    }
return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTSlotType<TOBJECT, TRET(TARGS...)>::operator()(TARGS... Args)
{
    if (IsValid() == false)
    {
        GetRTTIFunc();
    }

    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && EXACT_CLASS(TOBJECT, CObject, ObjectPtr.Get()))
    {
        return (static_cast<TOBJECT*>(ObjectPtr.Get())->*(m_pFunc))(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TOBJECT, TRET, TARGS...>(static_cast<TOBJECT*>(ObjectPtr.Get()), m_pFunc, std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotType<TOBJECT, TRET(TARGS...)>::Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args)
{
    if ((pObj != nullptr) && (pFunc != nullptr))
    {
        return (pObj->*(pFunc))(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotType<TOBJECT, TRET(TARGS...)>::Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args, bool)
{
    if ((pObj != nullptr) && (pFunc != nullptr))
    {
        (pObj->*(pFunc))(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CTSlotCFunc : const func ptr
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::CTSlotCFunc(CObject* pObj, SLOT_CFUNC pFuncConst, size_t stType)
: CTSlotObject<TRET, TARGS...>(pObj, *(reinterpret_cast<uintptr_t*>(&pFuncConst)), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_FUNCTION|VART_CONST)))
, m_pFuncConst(pFuncConst)
{
    assert(pObj != nullptr);
    assert(pFuncConst != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::CTSlotCFunc(PCXStr pszClass, PCXStr pszName, SLOT_CFUNC pFuncConst, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, pszName, *(reinterpret_cast<uintptr_t*>(&pFuncConst)), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_FUNCTION|VART_CONST)))
, m_pFuncConst(pFuncConst)
{
    assert(pszClass != nullptr);
    assert(pszName != nullptr);
    assert(pFuncConst != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::~CTSlotCFunc(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::IsValid(void) const
{
    return (m_pFuncConst != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::operator()(TARGS... Args)
{
    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && EXACT_CLASS(TOBJECT, CObject, ObjectPtr.Get()))
    {
        return (static_cast<TOBJECT*>(ObjectPtr.Get())->*(m_pFuncConst))(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TOBJECT, TRET, TARGS...>(static_cast<TOBJECT*>(ObjectPtr.Get()), m_pFuncConst, std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args)
{
    if ((pObj != nullptr) && (pFuncConst != nullptr))
    {
        return (pObj->*(pFuncConst))(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotCFunc<TOBJECT, TRET(TARGS...) const>::Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args, bool)
{
    if ((pObj != nullptr) && (pFuncConst != nullptr))
    {
        (pObj->*(pFuncConst))(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CTSlotCType : get const func ptr from rtti-type
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCType<TOBJECT, TRET(TARGS...) const>::CTSlotCType(CObject* pObj, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pObj, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_RTTITYPE|VART_CONST)))
, m_pFuncConst(nullptr)
{
    assert(pObj != nullptr);
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCType<TOBJECT, TRET(TARGS...) const>::CTSlotCType(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, pszName, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_RTTITYPE|VART_CONST)))
, m_pFuncConst(nullptr)
{
    assert(pszClass != nullptr);
    assert(pszName != nullptr);
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotCType<TOBJECT, TRET(TARGS...) const>::~CTSlotCType(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotCType<TOBJECT, TRET(TARGS...) const>::IsValid(void) const
{
    return (m_pFuncConst != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotCType<TOBJECT, TRET(TARGS...) const>::GetRTTICFunc(void)
{
#ifdef __MODERN_CXX_NOT_SUPPORTED
    const CTRTTICFunc<TOBJECT, SLOT_CFUNC> RTTICFunc = CRTTITraits::GetConstFunc<TOBJ, SLOT_CFUNC>(CSlotObject::GetFuncName());
#else
    const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> RTTICFunc = CRTTITraits::GetConstFunc<TOBJECT, TRET, TARGS...>(CSlotObject::GetFuncName());
#endif
    if (RTTICFunc.IsValid())
    {
        m_pFuncConst = (SLOT_CFUNC)RTTICFunc.Get();
        return true;
    }
    return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTSlotCType<TOBJECT, TRET(TARGS...) const>::operator()(TARGS... Args)
{
    if (IsValid() == false)
    {
        GetRTTICFunc();
    }

    CObjectPtr ObjectPtr = CSlotObject::GetObjectPtr();
#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && EXACT_CLASS(TOBJECT, CObject, ObjectPtr.Get()))
    {
        return (static_cast<TOBJECT*>(ObjectPtr.Get())->*(m_pFuncConst))(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TOBJECT, TRET, TARGS...>(static_cast<TOBJECT*>(ObjectPtr.Get()), m_pFuncConst, std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotCType<TOBJECT, TRET(TARGS...) const>::Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args)
{
    if ((pObj != nullptr) && (pFuncConst != nullptr))
    {
        return (pObj->*(pFuncConst))(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotCType<TOBJECT, TRET(TARGS...) const>::Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args, bool)
{
    if ((pObj != nullptr) && (pFuncConst != nullptr))
    {
        (pObj->*(pFuncConst))(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CTSlotSFunc : static func ptr
template <typename TRET, typename... TARGS>
INLINE CTSlotSFunc<TRET(TARGS...)>::CTSlotSFunc(const CRTTI& rtti, SLOT_SFUNC pFuncStatic, size_t stType)
: CTSlotObject<TRET, TARGS...>(rtti, reinterpret_cast<uintptr_t>(pFuncStatic), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_FUNCTION|VART_STATIC)))
, m_pFuncStatic(pFuncStatic)
{
    assert(pFuncStatic != nullptr);
}

template <typename TRET, typename... TARGS>
INLINE CTSlotSFunc<TRET(TARGS...)>::CTSlotSFunc(PCXStr pszClass, SLOT_SFUNC pFuncStatic, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, nullptr, reinterpret_cast<uintptr_t>(pFuncStatic), ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_FUNCTION|VART_STATIC)))
, m_pFuncStatic(pFuncStatic)
{
    assert(pszClass != nullptr);
    assert(pFuncStatic != nullptr);
}

template <typename TRET, typename... TARGS>
INLINE CTSlotSFunc<TRET(TARGS...)>::~CTSlotSFunc(void)
{
}

template <typename TRET, typename... TARGS>
INLINE bool CTSlotSFunc<TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFuncStatic != nullptr);
}

template <typename TRET, typename... TARGS>
INLINE TRET CTSlotSFunc<TRET(TARGS...)>::operator()(TARGS... Args)
{
#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && (CSlotObject::GetRTTI() != nullptr))
    {
        return (m_pFuncStatic)(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TRET, TARGS...>(CSlotObject::GetRTTI(), m_pFuncStatic, std::forward<TARGS>(Args)...);
}

template <typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotSFunc<TRET(TARGS...)>::Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args)
{
    if ((pRTTI != nullptr) && (pFuncStatic != nullptr))
    {
        return (pFuncStatic)(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotSFunc<TRET(TARGS...)>::Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args, bool)
{
    if ((pRTTI != nullptr) && (pFuncStatic != nullptr))
    {
        (pFuncStatic)(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CTSlotSType : get static func ptr from rtti-type
template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotSType<TOBJECT, TRET(TARGS...)>::CTSlotSType(const CRTTI& rtti, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(rtti, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_RTTITYPE|VART_STATIC)))
, m_pFuncStatic(nullptr)
{
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotSType<TOBJECT, TRET(TARGS...)>::CTSlotSType(PCXStr pszClass, CPCXStr pszFunc, size_t stType)
: CTSlotObject<TRET, TARGS...>(pszClass, nullptr, pszFunc, ((stType & (VART_MASK|VART_RTTIDYNAMIC))|(VART_RTTITYPE|VART_STATIC)))
, m_pFuncStatic(nullptr)
{
    assert(pszClass != nullptr);
    assert(pszFunc != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE CTSlotSType<TOBJECT, TRET(TARGS...)>::~CTSlotSType(void)
{
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotSType<TOBJECT, TRET(TARGS...)>::IsValid(void) const
{
    return (m_pFuncStatic != nullptr);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE bool CTSlotSType<TOBJECT, TRET(TARGS...)>::GetRTTISFunc(void)
{
#ifdef __MODERN_CXX_NOT_SUPPORTED
    const CTRTTISFunc<TOBJECT, SLOT_SFUNC> RTTISFunc = CRTTITraits::GetStaticFunc<TOBJ, SLOT_SFUNC>(CSlotObject::GetFuncName());
#else
    const CTRTTISFunc<TOBJECT, TRET(TARGS...)> RTTISFunc = CRTTITraits::GetStaticFunc<TOBJECT, TRET, TARGS...>(CSlotObject::GetFuncName());
#endif
    if (RTTISFunc.IsValid())
    {
        m_pFuncStatic = (SLOT_SFUNC)RTTISFunc.Get();
        return true;
    }
    return false;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE TRET CTSlotSType<TOBJECT, TRET(TARGS...)>::operator()(TARGS... Args)
{
    if (IsValid() == false)
    {
        GetRTTISFunc();
    }

#if __MODERN_CXX_LANG >= 201703L
    if (IsValid() && (CSlotObject::GetRTTI() != nullptr))
    {
        return (m_pFuncStatic)(std::forward<TARGS>(Args)...);
    }
    if constexpr (std::is_constructible_v<TRET>)
    {
        return TRET{};
    }
}
#else
    return Functor<TRET, TARGS...>(CSlotObject::GetRTTI(), m_pFuncStatic, std::forward<TARGS>(Args)...);
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSlotSType<TOBJECT, TRET(TARGS...)>::Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args)
{
    if ((pRTTI != nullptr) && (pFuncStatic != nullptr))
    {
        return (pFuncStatic)(std::forward<TARGUMENTS>(Args)...);
    }
    return TRETURN{};
}

template <typename TOBJECT, typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSlotSType<TOBJECT, TRET(TARGS...)>::Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args, bool)
{
    if ((pRTTI != nullptr) && (pFuncStatic != nullptr))
    {
        (pFuncStatic)(std::forward<TARGUMENTS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CSignalEvent
INLINE CSignalEvent::CSignalEvent(bool bRegular, UInt uThreads)
: m_bRegular(bRegular)
, m_uThreads(uThreads)
, m_uFlag(TRUE)
, m_uStartEvent(0)
, m_uStopEvent(0)
, m_stCacheSize(0)
{
}

INLINE CSignalEvent::~CSignalEvent(void)
{
    DEV_DEBUG(TF("  CSignalEvent::~CSignalEvent all Size = %d"), m_Events.GetSize());
    StopQueue();
    if (m_Events.GetSize() > 0)
    {
        for (PINDEX index = m_Events.GetHeadIndex(); index != nullptr; )
        {
            CSlotEventPtr& EventPtr = m_Events.GetNext(index);
            if (EventPtr->GetIndex() != nullptr)
            {
                if (EventPtr->IsEventTick())
                {
                    CEventQueue::DestroyTickEvent(reinterpret_cast<uintptr_t>(EventPtr->GetIndex()));
                }
                EventPtr->SetIndex(nullptr);
            }
            EventPtr = nullptr;
        }
        m_Events.RemoveAll();
    }
}

INLINE bool CSignalEvent::Init(void)
{
    CAtomics::Exchange<UInt>(&m_uFlag, FALSE);
    return true;
}

INLINE void CSignalEvent::Exit(void)
{
    if (CAtomics::Exchange<UInt>(&m_uFlag, TRUE) == FALSE)
    {
        Disconnect();
        DEV_DEBUG(TF(" CSignalEvent::Disconnect Set Close flag!"));
    }
}

INLINE PINDEX CSignalEvent::Connect(CSlotEventPtr& EventPtr)
{
    if ((EventPtr != nullptr) && EventPtr->IsEvent())
    {
        if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
        {
            DEV_DEBUG(TF(" CSignalEvent::Connect Close flag!"));
            return nullptr;
        }

        CSyncLockScope scope(m_EventsLock);
        PINDEX index = nullptr;
        for (index = m_Events.GetHeadIndex(); index != nullptr; )
        {
            CSlotEventPtr& TheEvent = m_Events.GetNext(index);
            if (*TheEvent == *EventPtr)
            {
                DEV_WARN(TF(" CSignalEvent::Connect find exist (EventPtr = %p, class = %s, name = %s, type = %x, func = %p)")
                         TF("== (EventPtr = %p, class = %s, name = %s, type = %x, func = %p)"),
                         TheEvent.Get(), TheEvent->GetClass(), TheEvent->GetName(), TheEvent->GetType(), TheEvent->GetFunc(),
                         EventPtr.Get(), EventPtr->GetClass(), EventPtr->GetName(), EventPtr->GetType(), EventPtr->GetFunc());
                return nullptr;
            }
        }
        index = m_Events.AddTail(EventPtr);
        DEV_DEBUG(TF(" CSignalEvent::Connect at index = %p (SlotPtr = %p, class = %s, name = %s, type = %x, func = %p)"),
                  index, EventPtr.Get(), EventPtr->GetClass(), EventPtr->GetName(), EventPtr->GetType(), EventPtr->GetFunc());
        return index;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_WARN(TF(" CSignalEvent::Connect EventPtr = %p invalid or EventPtr is event type = %d!"), EventPtr.Get(), EventPtr->IsEvent());
    }
#endif
    return nullptr;
}

INLINE bool CSignalEvent::Disconnect(PINDEX index)
{
    CSyncLockScope scope(m_EventsLock);
    if (index != nullptr)
    {
        if (m_Events.Check(index))
        {
            CSlotEventPtr& EventPtr = m_Events[index];
            if (EventPtr->GetIndex() != nullptr)
            {
                if (EventPtr->IsEventTick() && (EventPtr->GetIndex() == index))
                {
                    CEventQueue::DestroyTickEvent(reinterpret_cast<uintptr_t>(index));
                }
                EventPtr->SetIndex(nullptr);
            }
            EventPtr = nullptr;
            m_Events.RemoveAt(index);
            DEV_DEBUG(TF("  CSignalEvent::Disconnect at index = %p"), index);
            return true;
        }
    }
    else
    {
        StopQueue();
        for (index = m_Events.GetHeadIndex(); index != nullptr; )
        {
            CSlotEventPtr& EventPtr = m_Events.GetNext(index);
            if (EventPtr->GetIndex() != nullptr)
            {
                if (EventPtr->IsEventTick())
                {
                    CEventQueue::DestroyTickEvent(reinterpret_cast<uintptr_t>(EventPtr->GetIndex()));
                }
                EventPtr->SetIndex(nullptr);
            }
            EventPtr = nullptr;
        }
        DEV_DEBUG(TF("  CSignalEvent::Disconnect all Size = %d"), m_Events.GetSize());
        m_Events.RemoveAll();
        return true;
    }
    return false;
}

INLINE UInt CSignalEvent::operator()(uintptr_t utEvent, uintptr_t utData, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    UInt uRet = (UInt)RET_ERROR;
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::operator()(uintptr_t, uintptr_t, ULLong) Close flag!"));
        return uRet;
    }

    if ((nType & CEventQueue::EVENT_TYPE_SYNC) != 0)
    {
        uRet = OnHandle(utEvent, utData, ullParam);
    }
    else if (StartQueue())
    {
        uRet = m_QueuePtr->Add(utEvent, utData, ullParam, pSync, nType) ? (UInt)RET_OKAY : (UInt)RET_ERROR;;
    }
    return uRet;
}

INLINE UInt CSignalEvent::operator()(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    UInt uRet = (UInt)RET_ERROR;
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::operator()(uintptr_t, CEventBase&, ULLong) Close flag!"));
        return uRet;
    }

    if ((nType & CEventQueue::EVENT_TYPE_SYNC) != 0)
    {
        uRet = OnHandle(utEvent, EventRef, ullParam);
    }
    else if (StartQueue())
    {
        uRet = m_QueuePtr->Add(utEvent, EventRef, ullParam, pSync, nType) ? (UInt)RET_OKAY : (UInt)RET_ERROR;
    }
    return uRet;
}

INLINE UInt CSignalEvent::operator()(uintptr_t utEvent, CStream& Stream, ULLong ullParam, CSyncBase* pSync, Int nType)
{
    UInt uRet = (UInt)RET_ERROR;
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::operator()(uintptr_t, CStream&, ULLong) Close flag!"));
        return uRet;
    }

    if ((nType & CEventQueue::EVENT_TYPE_SYNC) != 0)
    {
        uRet = OnHandle(utEvent, Stream, ullParam);
    }
    else if (StartQueue())
    {
        uRet = m_QueuePtr->Add(utEvent, Stream, ullParam, pSync, nType) ? (UInt)RET_OKAY : (UInt)RET_ERROR;;
    }
    return uRet;
}

INLINE bool CSignalEvent::Start(PINDEX index, UInt uInterval, UInt uCount)
{
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::Start Close flag!"));
        return false;
    }
    return StartTick(index, uInterval, uCount);
}

INLINE bool CSignalEvent::Stop(PINDEX index)
{
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::Stop Close flag!"));
        return false;
    }
    return StopTick(index);
}

INLINE UInt CSignalEvent::GetCount(void) const
{
    if (m_QueuePtr != nullptr)
    {
        return m_QueuePtr->GetCount();
    }
    return 0;
}

INLINE UInt CSignalEvent::GetThreads(void) const
{
    return m_uThreads;
}

INLINE void CSignalEvent::SetThreads(UInt uThreads)
{
    m_uThreads = uThreads;
}

INLINE size_t CSignalEvent::GetCacheSize(void) const
{
    return m_stCacheSize;
}

INLINE void CSignalEvent::SetCacheSize(size_t stCacheSize)
{
    m_stCacheSize = stCacheSize;
}

INLINE void CSignalEvent::SetThreadEvent(UInt uStart, UInt uStop)
{
    m_uStartEvent = uStart;
    m_uStopEvent  = uStop;
}

INLINE bool CSignalEvent::StartQueue(void)
{
    if (CAtomics::Exchange<UInt>(&m_uFlag, FALSE) != FALSE)
    {
        DEV_DEBUG(TF(" CSignalEvent::StartQueue Close flag!"));
        return false;
    }
    if (m_QueuePtr != nullptr)
    {
        return true;
    }
    if (CEventQueue::EventQueue(m_QueuePtr, *this, m_stCacheSize, m_uThreads))
    {
        if ((m_uStartEvent > 0) || (m_uStopEvent > 0))
        {
            m_QueuePtr->SetThreadEvent(m_uStartEvent, m_uStopEvent);
        }
        if (m_QueuePtr->Init())
        {
            return true;
        }
        m_QueuePtr->Exit();
        m_QueuePtr = nullptr;
    }
    return false;
}

INLINE bool CSignalEvent::StopQueue(void)
{
    if (m_QueuePtr != nullptr)
    {
        m_QueuePtr->Exit();
        m_QueuePtr = nullptr;
    }
    return true;
}

INLINE CEventQueuePtr& CSignalEvent::GetEventQueuePtr(void)
{
    return m_QueuePtr;
}

INLINE UInt CSignalEvent::OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    if (m_bRegular)
    {
        return OnRegularHandle(utEvent, utData, ullParam);
    }
    else
    {
        return OnAssignHandle(utEvent, utData, ullParam);
    }
}

INLINE UInt CSignalEvent::OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
{
    if (m_bRegular)
    {
        return OnRegularHandle(utEvent, EventRef, ullParam);
    }
    else
    {
        return OnAssignHandle(utEvent, EventRef, ullParam);
    }
}

INLINE UInt CSignalEvent::OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    if (m_bRegular)
    {
        return OnRegularHandle(utEvent, Stream, ullParam);
    }
    else
    {
        return OnAssignHandle(utEvent, Stream, ullParam);
    }
}

INLINE UInt CSignalEvent::OnHandle(uintptr_t utEvent, UInt uCount)
{
    if (m_bRegular)
    {
        return OnRegularHandle(utEvent, uCount);
    }
    else
    {
        return OnAssignHandle(utEvent, uCount);
    }
}

INLINE UInt CSignalEvent::OnRegularHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    for (PINDEX index = m_Events.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = m_Events.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, utData, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnRegularHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    for (PINDEX index = m_Events.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = m_Events.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, EventRef, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnRegularHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    for (PINDEX index = m_Events.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = m_Events.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, Stream, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnRegularHandle(uintptr_t utEvent, UInt uCount)
{
    UInt   uRet  = (UInt)RET_ERROR;
    PINDEX index = reinterpret_cast<PINDEX>(utEvent);
    if (m_Events.Check(index) == false)
    {
        return uRet;
    }
    CSlotEventPtr& EventPtr = m_Events[index];
    assert(EventPtr->IsEventTick());
    assert(EventPtr->GetIndex() == index);

    uRet = (*EventPtr)(utEvent, uCount);
    if (uRet != RET_OKAY)
    {
        EventPtr->SetIndex(nullptr);
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnAssignHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    LST_EVENT EventList;
    {
        CSyncLockScope scope(m_EventsLock);
        EventList = m_Events;
    }
    for (PINDEX index = EventList.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = EventList.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, utData, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnAssignHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    LST_EVENT EventList;
    {
        CSyncLockScope scope(m_EventsLock);
        EventList = m_Events;
    }
    for (PINDEX index = EventList.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = EventList.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, EventRef, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnAssignHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    UInt uRet = (UInt)RET_ERROR;
    LST_EVENT EventList;
    {
        CSyncLockScope scope(m_EventsLock);
        EventList = m_Events;
    }
    for (PINDEX index = EventList.GetHeadIndex(); index != nullptr;)
    {
        CSlotEventPtr& EventPtr = EventList.GetNext(index);
        if (EventPtr->IsEventQueue())
        {
            uRet = (*EventPtr)(utEvent, Stream, ullParam);
        }
    }
    return uRet;
}

INLINE UInt CSignalEvent::OnAssignHandle(uintptr_t utEvent, UInt uCount)
{
    UInt   uRet  = (UInt)RET_ERROR;
    PINDEX index = reinterpret_cast<PINDEX>(utEvent);
    CSlotEventPtr EventPtr;
    {
        CSyncLockScope scope(m_EventsLock);
        if (m_Events.Check(index))
        {
            EventPtr = m_Events[index];
            assert(EventPtr->IsEventTick());
            assert(EventPtr->GetIndex() == index);
        }
    }
    uRet = (*EventPtr)(utEvent, uCount);
    if (uRet != RET_OKAY)
    {
        CSyncLockScope scope(m_EventsLock);
        EventPtr->SetIndex(nullptr);
    }
    return uRet;
}

INLINE bool CSignalEvent::StartTick(PINDEX index, UInt uInterval, UInt uCount)
{
    CSyncLockScope scope(m_EventsLock);
    if (m_Events.Check(index) == false)
    {
#ifdef __RUNTIME_DEBUG__
        DEV_WARN(TF(" CSignalEvent::StartTick index = %p check failed!"), index);
#endif
        return false;
    }
    CSlotEventPtr EventPtr = m_Events[index];
    if (EventPtr->IsEventTick() && (EventPtr->GetIndex() == nullptr) &&
        CEventQueue::CreateTickEvent(reinterpret_cast<uintptr_t>(index), uInterval, *this, uCount))
    {
        EventPtr->SetIndex(index);
        DEV_DEBUG(TF(" CSignalEvent::StartTick index = %p uInterval = %lld---uCount = %x"), index, uInterval, uCount);
        return true;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_WARN(TF(" CSignalEvent::StartTick index = %p uInterval = %lld---uCount = %x failed, event-tick = %d or inxTick = %p not nullptr!"),
                 index, uInterval, uCount, EventPtr->IsEventTick(), EventPtr->GetIndex());
    }
#endif
    return false;
}

INLINE bool CSignalEvent::StopTick(PINDEX index)
{
    CSyncLockScope scope(m_EventsLock);
    if (m_Events.Check(index) == false)
    {
#ifdef __RUNTIME_DEBUG__
        DEV_WARN(TF(" CSignalEvent::StopTick index = %p check failed!"), index);
#endif
        return false;
    }
    CSlotEventPtr EventPtr = m_Events[index];
    if (EventPtr->IsEventTick() && (EventPtr->GetIndex() == index))
    {
        EventPtr->SetIndex(nullptr);
        CEventQueue::DestroyTickEvent(reinterpret_cast<uintptr_t>(index));
        DEV_DEBUG(TF(" CSignalEvent::StopTick index = %p"), index);
        return true;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_WARN(TF(" CSignalEvent::StopTick index = %p failed, event-tick = %d or inxTick = %p nullptr!"),
                 index, EventPtr->IsEventTick(), EventPtr->GetIndex());
    }
#endif
    return false;
}

///////////////////////////////////////////////////////////////////
// CTSignal
template <typename TRET, typename... TARGS>
INLINE CTSignal<TRET(TARGS...)>::CTSignal(bool bRegular)
: m_bRegular(bRegular)
{
}

template <typename TRET, typename... TARGS>
INLINE CTSignal<TRET(TARGS...)>::~CTSignal(void)
{
}

template <typename TRET, typename... TARGS>
INLINE PINDEX CTSignal<TRET(TARGS...)>::Connect(CSlotPtr& SlotPtr)
{
    if ((SlotPtr != nullptr) && (SlotPtr->IsEvent() == false))
    {
        CSyncLockScope scope(m_SlotsLock);
        PINDEX index = nullptr;
        for (index = m_Slots.GetHeadIndex(); index != nullptr; )
        {
            CSlotPtr& TheSlot = m_Slots.GetNext(index);
            if (*TheSlot == *SlotPtr)
            {
                DEV_WARN(TF(" CTSignal::Connect find exist (SlotPtr = %p, class = %s, name = %s, type = %x, func = %p)")
                         TF("== (SlotPtr = %p, class = %s, name = %s, type = %x, func = %p)"),
                         TheSlot.Get(), TheSlot->GetClass(), TheSlot->GetName(), TheSlot->GetType(), TheSlot->GetFunc(),
                         SlotPtr.Get(), SlotPtr->GetClass(), SlotPtr->GetName(), SlotPtr->GetType(), SlotPtr->GetFunc());
                return nullptr;
            }
        }
        index = m_Slots.AddTail(SlotPtr);
        DEV_DEBUG(TF(" CTSignal::Connect at index = %p (SlotPtr = %p, class = %s, name = %s, type = %x, func = %p)"),
                  index, SlotPtr.Get(), SlotPtr->GetClass(), SlotPtr->GetName(), SlotPtr->GetType(), SlotPtr->GetFunc());
        return index;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        DEV_WARN(TF(" CTSignal::Connect SlotPtr = %p invalid or SlotPtr is event type = %d!"), SlotPtr.Get(), SlotPtr->IsEvent());
    }
#endif
    return nullptr;
}

template <typename TRET, typename... TARGS>
INLINE bool CTSignal<TRET(TARGS...)>::Disconnect(PINDEX index)
{
    CSyncLockScope scope(m_SlotsLock);
    if (index != nullptr)
    {
        if (m_Slots.Check(index))
        {
            m_Slots.RemoveAt(index);
            DEV_DEBUG(TF(" CTSignal::Disconnect at index = %p"), index);
            return true;
        }
    }
    else
    {
        m_Slots.RemoveAll();
        DEV_DEBUG(TF(" CTSignal::Disconnect all"));
        return true;
    }
    return false;
}

template <typename TRET, typename... TARGS>
INLINE TRET CTSignal<TRET(TARGS...)>::operator()(TARGS... Args)
{
    if (m_bRegular)
    {
        return InvokeRegular(std::forward<TARGS>(Args)...);
    }
    else
    {
        return InvokeAssign(std::forward<TARGS>(Args)...);
    }
}

template <typename TRET, typename... TARGS>
INLINE TRET CTSignal<TRET(TARGS...)>::InvokeRegular(TARGS... Args)
{
#if __MODERN_CXX_LANG >= 201703L
    if constexpr (std::is_constructible_v<TRET>)
    {
        TRET tRet{};
        for (PINDEX index = m_Slots.GetHeadIndex(); index != nullptr;)
        {
            CSlotPtr& SlotPtr = m_Slots.GetNext(index);
            assert(SlotPtr->IsEvent() == false);
            tRet = (*SlotPtr)(std::forward<TARGS>(Args)...);
        }
        return tRet;
    }
    else
    {
        for (PINDEX index = m_Slots.GetHeadIndex(); index != nullptr;)
        {
            CSlotPtr& SlotPtr = m_Slots.GetNext(index);
            assert(SlotPtr->IsEvent() == false);
            (*SlotPtr)(std::forward<TARGS>(Args)...);
        }
    }
#else
    return Functor<TRET, TARGS...>(m_Slots, std::forward<TARGS>(Args)...);
#endif
}

template <typename TRET, typename... TARGS>
INLINE TRET CTSignal<TRET(TARGS...)>::InvokeAssign(TARGS... Args)
{
    LST_SLOT SlotList;
    {
        CSyncLockScope scope(m_SlotsLock);
        SlotList = m_Slots;
    }
#if __MODERN_CXX_LANG >= 201703L
    if constexpr (std::is_constructible_v<TRET>)
    {
        TRET tRet{};
        for (PINDEX index = SlotList.GetHeadIndex(); index != nullptr;)
        {
            CSlotPtr& SlotPtr = SlotList.GetNext(index);
            assert(SlotPtr->IsEvent() == false);
            tRet = (*SlotPtr)(std::forward<TARGS>(Args)...);
        }
        return tRet;
    }
    else
    {
        for (PINDEX index = SlotList.GetHeadIndex(); index != nullptr;)
        {
            CSlotPtr& SlotPtr = SlotList.GetNext(index);
            assert(SlotPtr->IsEvent() == false);
            (*SlotPtr)(std::forward<TARGS>(Args)...);
        }
    }
}
#else
    return Functor<TRET, TARGS...>(SlotList, std::forward<TARGS>(Args)...);
}

template <typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
CTSignal<TRET(TARGS...)>::Functor(LST_SLOT& SlotList, TARGUMENTS... Args)
{
    TRETURN tRet{};
    for (PINDEX index = SlotList.GetHeadIndex(); index != nullptr;)
    {
        CSlotPtr& SlotPtr = SlotList.GetNext(index);
        assert(SlotPtr->IsEvent() == false);
        tRet = (*SlotPtr)(std::forward<TARGS>(Args)...);
    }
    return tRet;
}

template <typename TRET, typename... TARGS>
template <typename TRETURN, typename... TARGUMENTS>
INLINE typename std::enable_if<std::is_void<TRETURN>::value>::type
CTSignal<TRET(TARGS...)>::Functor(LST_SLOT& SlotList, TARGUMENTS... Args, bool)
{
    for (PINDEX index = SlotList.GetHeadIndex(); index != nullptr;)
    {
        CSlotPtr& SlotPtr = SlotList.GetNext(index);
        assert(SlotPtr->IsEvent() == false);
        (*SlotPtr)(std::forward<TARGS>(Args)...);
    }
}
#endif

///////////////////////////////////////////////////////////////////
// CObjectTraits
template <typename TOBJECT>
INLINE bool CObjectTraits::Link(PCXStr pszName, CTRefCountPtr<TOBJECT>& ObjectPtr)
{
    if (pszName != nullptr)
    {
        ObjectPtr = MNEW TOBJECT;
        assert(ObjectPtr != nullptr);
        if (ObjectPtr != nullptr)
        {
            if (ObjectPtr->Link(pszName) == false)
            {
                ObjectPtr = nullptr;
            }
        }
    }
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Link(const CString& strName, CTRefCountPtr<TOBJECT>& ObjectPtr)
{
    return Link<TOBJECT>(*strName, ObjectPtr);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Link(const CUUID& uuid, CTRefCountPtr<TOBJECT>& ObjectPtr)
{
    ObjectPtr = MNEW TOBJECT;
    assert(ObjectPtr != nullptr);
    if (ObjectPtr != nullptr)
    {
        assert(uuid.IsValid());
        if (ObjectPtr->Link(uuid) == false)
        {
            ObjectPtr = nullptr;
        }
    }
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::LinkRTTI(PCXStr pszName, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass)
{
    if (pszName != nullptr)
    {
        ObjectPtr = static_cast<TOBJECT*>(CRTTI::CreateByName((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName()));
        if (ObjectPtr != nullptr)
        {
            if (ObjectPtr->Link(pszName) == false)
            {
                ObjectPtr = nullptr;
            }
        }
    }
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::LinkRTTI(const CString& strName, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass)
{
    return LinkRTTI<TOBJECT>(*strName, ObjectPtr, pszClass);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::LinkRTTI(const CUUID& uuid, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass)
{
    ObjectPtr = static_cast<TOBJECT*>(CRTTI::CreateByName((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName()));
    if (ObjectPtr != nullptr)
    {
        assert(uuid.IsValid());
        if (ObjectPtr->Link(uuid) == false)
        {
            ObjectPtr = nullptr;
        }
    }
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE void CObjectTraits::Unlink(CTRefCountPtr<TOBJECT>& ObjectPtr)
{
    assert(ObjectPtr != nullptr);
    if (ObjectPtr != nullptr)
    {
        assert(ObjectPtr->GetKey() != nullptr);
        ObjectPtr->Unlink();

        ObjectPtr = nullptr;
    }
}


template <typename TOBJECT>
INLINE bool CObjectTraits::Load(PCXStr pszName, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass, size_t stFlag)
{
    assert(pszModule != nullptr);
    if (pszName != nullptr)
    {
        ObjectPtr = static_cast<TOBJECT*>(CObject::StaticLink((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName(), pszName, pszModule, stFlag));
    }
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Load(const CString& strName, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass, size_t stFlag)
{
    return Load<TOBJECT>(*strName, pszModule, ObjectPtr, pszClass, stFlag);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Load(const CUUID& uuid, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass, size_t stFlag)
{
    assert(uuid.IsValid());
    assert(pszModule != nullptr);
    ObjectPtr = static_cast<TOBJECT*>(CObject::StaticLink((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName(), uuid, pszModule, stFlag));
    return (ObjectPtr != nullptr);
}

template <typename TOBJECT>
INLINE void CObjectTraits::Unload(PCXStr pszClass, PCXStr pszName)
{
    CObject::StaticUnlink((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName(), pszName);
}

template <typename TOBJECT>
INLINE void CObjectTraits::UnloadUUID(const CUUID& uuid, PCXStr pszClass)
{
    assert(uuid.IsValid());
    CObject::StaticUnlink((pszClass != nullptr) ? pszClass : TOBJECT::ClassRTTI().GetName(), uuid);
}


template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, PCXStr pszClass)
{
    if (pszClass != nullptr)
    {
        CTArray<CObjectPtr> ObjPtrs;
        if (CObject::StaticFind(ObjPtrs, pszClass) > 0)
        {
            for (Int i = 0; i < ObjPtrs.GetSize(); ++i)
            {
                ObjectPtrs.Add(static_cast<TOBJECT*>(ObjPtrs[i].Get()));
            }
            return (ObjectPtrs.GetSize() > 0);
        }
    }
    return false;
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, const CString& strClass)
{
    return Find<TOBJECT>(ObjectPtrs, strClass.GetBuffer());
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, bool bExactClass)
{
    CTArray<CObjectPtr> ObjPtrs;
    if (CObject::StaticFind(ObjPtrs, TOBJECT::ClassRTTI(), bExactClass) > 0)
    {
        for (Int i = 0; i < ObjPtrs.GetSize(); ++i)
        {
            ObjectPtrs.Add(static_cast<TOBJECT*>(ObjPtrs[i].Get()));
        }
        return (ObjectPtrs.GetSize() > 0);
    }
    return false;
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszName, bool bExactClass)
{
    assert(pszName != 0);
    if (pszName != 0)
    {
        CObjectPtr ObjPtr;
        if (CObject::StaticFind(ObjPtr, pszName, TOBJECT::ClassRTTI(), bExactClass))
        {
            ObjectPtr = ObjPtr.Cast<TOBJECT>();
            return true;
        }
    }
    return false;
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CString& strName, bool bExactClass)
{
    return Find<TOBJECT>(ObjectPtr, strName.GetBuffer(), bExactClass);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CUUID& uuid, bool bExactClass)
{
    assert(uuid.IsValid());
    if (uuid.IsValid())
    {
        CObjectPtr ObjPtr;
        if (CObject::StaticFind(ObjPtr, uuid, TOBJECT::ClassRTTI(), bExactClass))
        {
            ObjectPtr = ObjPtr.Cast<TOBJECT>();
            return true;
        }
    }
    return false;
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszName, PCXStr pszClass)
{
    assert(pszName != 0);
    if (pszName != 0)
    {
        CObjectPtr ObjPtr;
        if (CObject::StaticFind(ObjPtr, pszName, pszClass))
        {
            ObjectPtr = ObjPtr.Cast<TOBJECT>();
            return true;
        }
    }
    return false;
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CString& strName, PCXStr pszClass)
{
    return Find<TOBJECT>(ObjectPtr, strName.GetBuffer(), pszClass);
}

template <typename TOBJECT>
INLINE bool CObjectTraits::Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CUUID& uuid, PCXStr pszClass)
{
    assert(uuid.IsValid());
    if (uuid.IsValid())
    {
        CObjectPtr ObjPtr;
        if (CObject::StaticFind(ObjPtr, uuid, TOBJECT::ClassRTTI(), pszClass))
        {
            ObjectPtr = ObjPtr.Cast<TOBJECT>();
            return true;
        }
    }
    return false;
}

// signal-slot
template <typename TOBJECT>
INLINE PINDEX CObjectTraits::Connect(CSignalEvent& Signal, const TOBJECT& ObjectRef, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits Connect SignalEvent(%p) object %p(class = %s) type = %x failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType);
        return nullptr;
    }

    PINDEX index = nullptr;
    CSlotEventPtr EventPtr = MNEW CSlotEvent(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), stType);
    if (EventPtr != nullptr)
    {
        index = Signal.Connect(EventPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect SignalEvent(%p) object %p(class = %s) type = %x return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, index);
    return index;
}

INLINE PINDEX CObjectTraits::Connect(CSignalEvent& Signal, PCXStr pszClass, PCXStr pszName, size_t stType)
{
    if ((pszClass == nullptr) || (pszName == nullptr))
    {
        DEV_DEBUG(TF("  ObjectTraits Connect SignalEvent(%p) type = %x class = %s or name = %s invalid!"), &Signal, stType, pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    CSlotEventPtr EventPtr = MNEW CSlotEvent(pszClass, pszName, stType);
    if (EventPtr != nullptr)
    {
        index = Signal.Connect(EventPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect SignalEvent(%p) type = %x class = %s and name = %s return %p"), &Signal, stType, pszClass, pszName, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, TRET (TOBJECT::* pFunc)(TARGS...), size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x func ptr = %p failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, *(reinterpret_cast<uintptr_t*>(&pFunc)));
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotFunc<TOBJECT, TRET(TARGS...)>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x func ptr = %p return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...), size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if ((pszClass == nullptr) || (pszName == nullptr))
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) type = %x func ptr = %p class = %s or name = %s invalid!"), &Signal, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotFunc<TOBJECT, TRET(TARGS...)>(pszClass, pszName, pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s and name = %s type = %x func ptr = %p return %p"), &Signal, pszClass, pszName, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, TRET (TOBJECT::* pFunc)(TARGS...) const, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x const func ptr = %p failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, *(reinterpret_cast<uintptr_t*>(&pFunc)));
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotCFunc<TOBJECT, TRET(TARGS...) const>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x const func ptr = %p return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...) const, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if ((pszClass == nullptr) || (pszName == nullptr))
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) type = %x const func ptr = %p class = %s or name = %s invalid!"), &Signal, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotCFunc<TOBJECT, TRET(TARGS...) const>(pszClass, pszName, pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s and name = %s type = %x const func ptr = %p return %p"), &Signal, pszClass, pszName, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, TRET (* pFunc)(TARGS...), size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotSFunc<TRET(TARGS...)>(TOBJECT::ClassRTTI(), pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s type = %x static func ptr = %p return %p"), &Signal, TOBJECT::ClassRTTI().GetName(), stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, TRET (* pFunc)(TARGS...), size_t stType)
{
    if (pszClass == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) type = %x static func ptr = %p class name invalid!"), &Signal, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)));
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotSFunc<TRET(TARGS...)>(pszClass, pFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s type = %x static func ptr = %p return %p"), &Signal, pszClass, stType, *(reinterpret_cast<uintptr_t*>(&pFunc)), index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x func name = %s failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr;
    if ((stType & VART_CONST) != 0)
    {
        SlotPtr = MNEW CTSlotCType<TOBJECT, TRET(TARGS...) const>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pszFunc, stType);
    }
    else
    {
        assert((stType & VART_STATIC) != 0);
        SlotPtr = MNEW CTSlotType<TOBJECT, TRET(TARGS...)>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pszFunc, stType);
    }
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) object %p(class = %s) type = %x func name = %s return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotSType<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s type = %x static func name = %s return %p"), &Signal, TOBJECT::ClassRTTI().GetName(), stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if ((pszClass == nullptr) || (((stType & VART_STATIC) == 0) && (pszName == nullptr)))
    {
        DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) type = %x func name = %s class = %s or name = %s invalid!"), &Signal, stType, pszFunc, pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr;
    if ((stType & VART_CONST) != 0)
    {
        SlotPtr = MNEW CTSlotCType<TOBJECT, TRET(TARGS...) const>(pszClass, pszName, pszFunc, stType);
    }
    else if ((stType & VART_STATIC) != 0)
    {
        SlotPtr = MNEW CTSlotSType<TOBJECT, TRET(TARGS...)>(pszClass, pszFunc, stType);
    }
    else
    {
        SlotPtr = MNEW CTSlotType<TOBJECT, TRET(TARGS...)>(pszClass, pszName, pszFunc, stType);
    }
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits Connect Signal(%p) class = %s and name = %s type = %x func name = %s return %p"), &Signal, pszClass, pszName, stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectFunc(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits ConnectFunc Signal(%p) object %p(class = %s) type = %x func name = %s failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotType<TOBJECT, TRET(TARGS...)>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectFunc Signal(%p) object %p(class = %s) type = %x func name = %s return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectCFunc(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (ObjectRef.GetKey() == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits ConnectCFunc Signal(%p) object %p(class = %s) type = %x func name = %s failed by object is not linked!"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotCType<TOBJECT, TRET(TARGS...) const>(static_cast<CObject*>(const_cast<TOBJECT*>(&ObjectRef)), pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectCFunc Signal(%p) object %p(class = %s) type = %x func name = %s return %p"), &Signal, &ObjectRef, TOBJECT::ClassRTTI().GetName(), stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectSFunc(CTSignal<TRET(TARGS...)>& Signal, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotSType<TOBJECT, TRET(TARGS...)>(TOBJECT::ClassRTTI(), pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectSFunc Signal(%p) class = %s type = %x static func name = %s return %p"), &Signal, TOBJECT::ClassRTTI().GetName(), stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if ((pszClass == nullptr) || (pszName == nullptr))
    {
        DEV_DEBUG(TF("  ObjectTraits ConnectFunc Signal(%p) type = %x func name = %s class = %s or name = %s invalid!"), &Signal, stType, pszFunc, pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotType<TOBJECT, TRET(TARGS...)>(pszClass, pszName, pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectFunc Signal(%p) class = %s and name = %s type = %x func name = %s return %p"), &Signal, pszClass, pszName, stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectCFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if ((pszClass == nullptr) || (pszName == nullptr))
    {
        DEV_DEBUG(TF("  ObjectTraits ConnectCFunc Signal(%p) type = %x func name = %s class = %s or name = %s invalid!"), &Signal, stType, pszFunc, pszClass, pszName);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotCType<TOBJECT, TRET(TARGS...) const>(pszClass, pszName, pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectCFunc Signal(%p) class = %s and name = %s type = %x func name = %s return %p"), &Signal, pszClass, pszName, stType, pszFunc, index);
    return index;
}

template <typename TOBJECT, typename TRET, typename... TARGS>
INLINE PINDEX CObjectTraits::ConnectSFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, CPCXStr pszFunc, size_t stType)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<TOBJECT, CObject>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (pszClass == nullptr)
    {
        DEV_DEBUG(TF("  ObjectTraits ConnectSFunc Signal(%p) type = %x func name = %s class = %s invalid!"), &Signal, stType, pszFunc, pszClass);
        return nullptr;
    }

    PINDEX index = nullptr;
    typename CTSignal<TRET(TARGS...)>::CSlotPtr SlotPtr = MNEW CTSlotSType<TOBJECT, TRET(TARGS...)>(pszClass, pszFunc, stType);
    if (SlotPtr != nullptr)
    {
        index = Signal.Connect(SlotPtr);
    }
    DEV_DEBUG(TF("  ObjectTraits ConnectSFunc Signal(%p) class = %s type = %x func name = %s return %p"), &Signal, pszClass, stType, pszFunc, index);
    return index;
}

#endif // __OBJECT_INL__
