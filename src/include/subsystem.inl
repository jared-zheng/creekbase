// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __SUBSYSTEM_INL__
#define __SUBSYSTEM_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CComponent
INLINE CComponent::CComponent(void)
{
}

INLINE CComponent::~CComponent(void)
{
}

INLINE CComponent::CComponent(const CComponent& aSrc)
: CTRefCount<CComponent>(aSrc)
{
}

INLINE CComponent& CComponent::operator=(const CComponent& aSrc)
{
    if (this != &aSrc)
    {
        CTRefCount<CComponent>::operator=(aSrc);
    }
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CSubSystem
SELECTANY CPCStr CSubSystem::CreateSubSystem = (CPCStr)("CreateSubSystem");

INLINE CSubSystem::CSubSystem(void)
{
}

INLINE CSubSystem::~CSubSystem(void)
{
}

///////////////////////////////////////////////////////////////////
// CTLoader
template <typename T>
INLINE CTLoader<T>::CTLoader(void)
: m_pSubSystem(nullptr)
, m_bInited(false)
{
}

template <typename T>
INLINE CTLoader<T>::~CTLoader(void)
{
    Unload();
}

template <typename T>
INLINE CTLoader<T>::CTLoader(const CTLoader<T>&)
: m_pSubSystem(nullptr)
, m_bInited(false)
{
}

template <typename T>
INLINE CTLoader<T>& CTLoader<T>::operator=(const CTLoader<T>&)
{
    return (*this);
}

template <typename T>
INLINE T* CTLoader<T>::operator->(void) const
{
    assert(m_pSubSystem != nullptr);
    return m_pSubSystem;
}

template <typename T>
INLINE T* CTLoader<T>::Get(void) const
{
    return m_pSubSystem;
}

template <typename T>
INLINE Module CTLoader<T>::GetModule(void) const
{
    return m_LoadModule;
}

template <typename T>
INLINE bool CTLoader<T>::IsInited(void) const
{
    return (m_bInited);
}

template <typename T>
INLINE bool CTLoader<T>::IsLoaded(void) const
{
    return (m_pSubSystem != nullptr);
}

template <typename T>
INLINE bool CTLoader<T>::Load(const CUUID& uuId, PCXStr pszLoadPath, bool bInit)
{
    if (m_pSubSystem == nullptr)
    {
        if (pszLoadPath == nullptr)
        {
            DEV_DEBUG(TF("Load module file failed, path invalid!"));
            return false;
        }
        m_LoadModule.Attach(pszLoadPath);
        if (m_LoadModule == nullptr)
        {
            DEV_DEBUG(TF("Load module file[%s]failed"), pszLoadPath);
            return false;
        }

        CreateSubSystemFunc* pCreateFunc = m_LoadModule.Find<CreateSubSystemFunc*>(CSubSystem::CreateSubSystem);

        if (pCreateFunc == nullptr)
        {
            DEV_DEBUG(TF("Interface not found on module[%s, handle=%#X]"), pszLoadPath, (Module)m_LoadModule);
            return false;
        }

        m_pSubSystem = reinterpret_cast<T*>(pCreateFunc(uuId));
        if (m_pSubSystem == nullptr)
        {
            DEV_DEBUG(TF("Create interface on module[%s, handle=%#X]failed"), pszLoadPath, (Module)m_LoadModule);
            return false;
        }

        if (bInit)
        {
            m_bInited = (m_pSubSystem->Init() == RET_OKAY);
            if (m_bInited == false)
            {
                DEV_DEBUG(TF("init interface on module[%s, handle=%#X]failed"), pszLoadPath, (Module)m_LoadModule);
                return false;
            }
        }
    }
    return true;
}

template <typename T>
INLINE void CTLoader<T>::Unload(void)
{
    Int nRet = 0;
    if (m_pSubSystem != nullptr)
    {
        m_pSubSystem->Exit();

        nRet = m_pSubSystem->Release();
        m_pSubSystem = nullptr;
    }
    if ((nRet <= 0) && (m_LoadModule != nullptr))
    {
        m_LoadModule.Close();
    }
}

///////////////////////////////////////////////////////////////////
// CTLoaderRef
template <typename T>
INLINE CTLoaderRef<T>::CTLoaderRef(Module m)
: m_pSubSystem(nullptr)
, m_bInited(false)
{
    assert(m != nullptr);
    m_RefModule.Attach(m, false);
}

template <typename T>
INLINE CTLoaderRef<T>::~CTLoaderRef(void)
{
    Unload();
}

template <typename T>
INLINE CTLoaderRef<T>::CTLoaderRef(const CTLoaderRef<T>&)
: m_pSubSystem(nullptr)
, m_bInited(false)
{
}

template <typename T>
INLINE CTLoaderRef<T>& CTLoaderRef<T>::operator=(const CTLoaderRef<T>&)
{
    return (*this);
}

template <typename T>
INLINE T* CTLoaderRef<T>::operator->(void) const
{
    assert(m_pSubSystem != nullptr);
    return m_pSubSystem;
}

template <typename T>
INLINE T* CTLoaderRef<T>::Get(void) const
{
    return m_pSubSystem;
}

template <typename T>
INLINE Module CTLoaderRef<T>::GetModule(void) const
{
    return m_RefModule;
}

template <typename T>
INLINE bool CTLoaderRef<T>::IsInited(void) const
{
    return (m_bInited);
}

template <typename T>
INLINE bool CTLoaderRef<T>::IsLoaded(void) const
{
    return (m_pSubSystem != nullptr);
}

template <typename T>
INLINE bool CTLoaderRef<T>::Load(const CUUID& uuId, bool bInit)
{
    if (m_pSubSystem == nullptr)
    {
        if (m_RefModule == nullptr)
        {
            DEV_DEBUG(TF("Module file handle is null!"));
            return false;
        }
        CreateSubSystemFunc* pCreateFunc = m_RefModule.Find<CreateSubSystemFunc*>(CSubSystem::CreateSubSystem);
        if (pCreateFunc == nullptr)
        {
            DEV_DEBUG(TF("Interface not found on module[handle=%#X]!"), (Module)m_RefModule);
            return false;
        }

        m_pSubSystem = reinterpret_cast<T*>(pCreateFunc(uuId));
        if (m_pSubSystem == nullptr)
        {
            DEV_DEBUG(TF("Create interface on module[handle=%#X]failed!"), (Module)m_RefModule);
            return false;
        }

        if (bInit)
        {
            m_bInited = (m_pSubSystem->Init() == RET_OKAY);
            if (m_bInited == false)
            {
                DEV_DEBUG(TF("init interface on module[handle=%#X]failed"), (Module)m_RefModule);
                return false;
            }
        }
    }
    return true;
}

template <typename T>
INLINE void CTLoaderRef<T>::Unload(void)
{
    if (m_pSubSystem != nullptr)
    {
        m_pSubSystem->Exit();
        m_pSubSystem->Release();
        m_pSubSystem = nullptr;
    }
    m_RefModule.Detach();
}

#endif // __SUBSYSTEM_INL__
