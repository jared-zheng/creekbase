// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_HTTP_H__
#define __NETWORK_HTTP_H__

#pragma once

#include "networkevent.h"
#include "codec.h"
#include "crypto.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// simple http & ws
class CURI;

class CHTTPHeader;
class CHTTPCookie;
class CHTTPParameter;

class CHTTPHeaderRef;
class CHTTPCookieRef;
class CHTTPParameterRef;

class CHTTPRequest;
class CHTTPResponse;

class CHTTPRequestRef;
class CHTTPResponseRef;

class CHTTPParser;

class CWSParser;

///////////////////////////////////////////////////////////////////
// NET_BUFFER
struct tagNET_BUFFER : public MObject
{
public:
    tagNET_BUFFER(void);
    ~tagNET_BUFFER(void);

    bool   Create(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver);
    bool   Append(CNETTraits::NET_ATTR& NetAttr, PByte pData, Int nData, Int nCached, bool& bRebind);
    bool   Check(CNETTraits::PNET_PARAM pParam, Int nCached, Int nAppend, bool bHandOver, bool& bRebind);

    void   Bind(PINDEX inxCache, Int nCacheSize, Int nCacheOffset);
    void   Bind(PINDEX inxCache, PByte pCachePtr, Int nCacheSize, Int nCacheOffset);

    PByte  Data(void) const;

    size_t Length(void) const;
    // NET_BUFFER'Serialize only used for current process async event handle
    void   Serialize(CStream& Stream);

    bool   IsValid(void) const;

    void   Free(void);
    void   Reset(void);
public:
    PINDEX   index;   // cache index
    PByte    pCache;  // cache ptr
    Int      nSize;   // data  size
    Int      nOffset; // offset size from pCache to read/write buffer
};
typedef tagNET_BUFFER NET_BUFFER, *PNET_BUFFER;

///////////////////////////////////////////////////////////////////
// CHTTPTraits
class CHTTPTraits : public MObject
{
public:
    // https://www.w3.org/Protocols/rfc2616/rfc2616.html
    enum HTTP_STATUS
    { 
        UNKNOWN = 0,

        CONTINUE = 100,
        SWITCHING_PROTOCOLS = 101,
        
        OK = 200,
        CREATED = 201,
        ACCEPTED = 202,
        NON_AUTHORITATIVE_INFORMATION = 203,
        NO_CONTENT = 204,
        RESET_CONTENT = 205 ,
        PARTIAL_CONTENT = 206,

        MULTIPLE_CHOICES = 300,
        MOVED_PERMANENTLY = 301,
        FOUND = 302,
        SEE_OTHER = 303,
        NOT_MODIFIED = 304,
        USE_PROXY = 305,
        TEMPORARY_REDIRECT = 307,

        BAD_REQUEST = 400,
        UNAUTHORIZED = 401,
        PAYMENT_REQUIRED = 402 ,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        METHOD_NOT_ALLOWED = 405,
        NOT_ACCEPTABLE = 406,
        PROXY_AUTHENTICATION_REQUIRED = 407,
        REQUEST_TIMEOUT = 408,
        CONFLICT = 409,
        GONE = 410,
        LENGTH_REQUIRED = 411,
        PRECONDITION_FAILED = 412,
        REQUEST_ENTITY_TOO_LARGE = 413,
        REQUEST_URI_TOO_LONG = 414,
        UNSUPPORTED_MEDIA_TYPE = 415,
        REQUESTED_RANGE_NOT_SATISFIABLE = 416,
        EXPECTATION_FAILED = 417,
        UPGRADE_REQUIRED = 426,

        INTERNAL_SERVER_ERROR = 500,
        NOT_IMPLEMENTED = 501,
        BAD_GATEWAY = 502,
        SERVICE_UNAVAILABLE = 503,
        GATEWAY_TIMEOUT = 504,
        HTTP_VERSION_NOT_SUPPORTED = 505,
    };

    enum HTTP_CONTENT
    {
        HTTP_CONTENT_NONE,
        HTTP_CONTENT_LENGTH,
        HTTP_CONTENT_CHUNKED,
    };
    // http head charset ISO-8859-1
    typedef CTMap<CCString, CCString>               MAP_HEADER, *PMAP_HEADER;
    typedef CTMap<CCString, CCString>::PAIR         PAIR_HEADER;

    typedef MAP_HEADER                              MAP_COOKIE, *PMAP_COOKIE;
    typedef PAIR_HEADER                             PAIR_COOKIE;

    typedef MAP_HEADER                              MAP_PARAM, *PMAP_PARAM;
    typedef PAIR_HEADER                             PAIR_PARAM;

    typedef CTMap<CCStringRef, CCStringRef>         MAP_HEADER_REF, *PMAP_HEADER_REF;
    typedef CTMap<CCStringRef, CCStringRef>::PAIR   PAIR_HEADER_REF;

    typedef MAP_HEADER_REF                          MAP_COOKIE_REF, *PMAP_COOKIE_REF;
    typedef PAIR_HEADER_REF                         PAIR_COOKIE_REF;

    typedef MAP_HEADER_REF                          MAP_PARAM_REF, *PMAP_PARAM_REF;
    typedef PAIR_HEADER_REF                         PAIR_PARAM_REF;

    struct tagMARK : public MObject
    {
    public:
        tagMARK(void) : nPos(0), nSize(0) {}
        ~tagMARK(void) {}

        void Reset(void) { nPos = 0; nSize = 0; }
    public:
        Int nPos;
        Int nSize;
    };
    typedef struct tagMARK   MARK, *PMARK;
    typedef CTArray<MARK>    ARY_MARK;

    typedef CTArray<CBufReadStream>   ARY_BUF;
    typedef CTArray<CHTTPCookie>      ARY_COOKIE;
    typedef CTArray<CHTTPCookieRef>   ARY_COOKIE_REF;
public:
    static CPCStr  StatusToString(Int nStatus);
    static CPCXStr StatusToLocalString(Int nStatus);
protected:
    static bool    IsSkipChar(Char c);
    static bool    IsSkipCharWithSpecial(Char c, Char x);

    static bool    SkipChars(PCStr& pszBuf, Char x = 0);
    static size_t  GetLength(PCStr pszBegin, PCStr pszEnd);

    static void    FormatChunkData(CCString& strFormat, PCStr pszChunk, size_t stSize);
    static void    FormatChunkData(CStream& Stream, PByte pbChunk, size_t stSize);
    static void    FormatChunkEOF(CCString& strFormat);
    static void    FormatChunkEOF(CStream& Stream);
};

///////////////////////////////////////////////////////////////////
// CWSTraits
class CWSTraits : public MObject
{
    //   0                   1                   2                   3
    //   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    //  +-+-+-+-+-------+-+-------------+-------------------------------+
    //  |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
    //  |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
    //  |N|V|V|V|       |S|             |   (if payload len==126/127)   |
    //  | |1|2|3|       |K|             |                               |
    //  +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
    //  |     Extended payload length continued, if payload len == 127  |
    //  + - - - - - - - - - - - - - - - +-------------------------------+
    //  |                               |Masking-key, if MASK set to 1  |
    //  +-------------------------------+-------------------------------+
    //  | Masking-key (continued)       |          Payload Data         |
    //  +-------------------------------- - - - - - - - - - - - - - - - +
    //  :                     Payload Data continued ...                :
    //  + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
    //  |                     Payload Data continued ...                |
    //  +---------------------------------------------------------------+
    //  FIN:  1 bit(x0--more frames, x1--final frame)
    //  RSV1, RSV2, RSV3:  1 bit each(x0--default)
    //  Opcode:  4 bits
    //   *  %x0 denotes a continuation frame
    //   *  %x1 denotes a text frame
    //   *  %x2 denotes a binary frame
    //   *  %x3-7 are reserved for further non-control frames
    //   *  %x8 denotes a connection close
    //   *  %x9 denotes a ping
    //   *  %xA denotes a pong
    //   *  %xB-F are reserved for further control frames
    //  Mask:  1 bit(All frames sent from client to server have this bit set to 1)
    //  Payload length:  7 bits, 7+16 bits, or 7+64 bits
    //
    //  1. An unfragmented message consists of a single frame with the FIN
    //     bit set and an opcode other than 0
    //
    //  2. A fragmented message consists of a single frame with the FIN bit
    //     clear and an opcode other than 0, followed by zero or more frames
    //     with the FIN bit clear and the opcode set to 0, and terminated by
    //     a single frame with the FIN bit set and an opcode of 0
    //
    //  3. Control frames MAY be injected in the middle of a fragmented
    //     message. Control frames themselves MUST NOT be fragmented
public:
    enum WS_OPCODE
    {
        CONTINUE = 0x0,
        TEXT     = 0x1,
        BINARY   = 0x2,
        CLOSE    = 0x8,
        PING     = 0x9,
        PONG     = 0xA,
    };

    enum WS_FRAME
    {
        WS_CONTINUE    = 0x0000,
        WS_PAY_LOAD    = 0x007D,
        WS_EXT_SHORT   = 0x007E,
        WS_EXT_LONG    = 0x007F,
        WS_EXT_MASK    = 0x007F,
        WS_MASK        = 0x0080,
        WS_TEXT        = 0x0100,
        WS_BINARY      = 0x0200,
        WS_CLOSE       = 0x0800,
        WS_PING        = 0x0900,
        WS_PONG        = 0x0A00,
        WS_OPCODE_MASK = 0x0F00,
        WS_FINAL       = 0x8000,
        WS_HEAD_MASK   = 0x8F00,
        WS_FRAME_MASK  = 0x8F80,

        WS_CLOSE_FRAME = (WS_FINAL|WS_CLOSE),
        WS_PING_FRAME  = (WS_FINAL|WS_PING),
        WS_PONG_FRAME  = (WS_FINAL|WS_PONG),
    };

    enum WS_FRAMELEN
    {
        WS_NORMAL_LEN  = 2,
        WS_SHORT_LEN   = 4,
        WS_LONG_LEN    = 10,
        WS_MASK_LEN    = 4,
    };
    // https://tools.ietf.org/html/rfc6455#page-45
    enum WS_STATUS
    {
        // 0-999 Status codes in the range 0-999 are not used.
        // 1000-2999 Status codes in the range 1000-2999 are reserved for definition by this protocol
        NORMAL = 1000,
        GOING_AWAY = 1001,
        PROTOCOL_ERROR = 1002,
        UNSUPPORTED = 1003,
        RESERVED1 = 1004,
        NO_STATUS = 1005,
        CLOSED_ABNORMALLY = 1006,
        DATA_NOT_CONSISTENT = 1007,
        POLICY_VIOLATE = 1008,
        TOO_BIG_PROCESS = 1009,
        NO_NEGOTIATE_EXTENSION = 1010,
        PREVENT_REQUEST = 1011,
        TLS_HANDSHAKE_FAILED = 1015,
        // 3000-3999 Status codes in the range 3000-3999 are reserved for use by libraries, frameworks, and applications
        // 4000-4999 Status codes in the range 4000-4999 are reserved for private use and thus can't be registered
        // WS_STATUS_MAX = 5000,
    };

    enum WS_KEY
    {
        WS_KEY_REQUEST  = 24,
        WS_KEY_RESPONSE = 28,
        WS_KEY_MASK     = 0x12345678,
    };

    static CPCStr  AcceptUID;
public:
    static CPCStr  StatusToString(Int nStatus);
    static CPCXStr StatusToLocalString(Int nStatus);
    static bool    IsControlFrame(Int nFrameCode);
    static bool    IsFinalFrame(Int nFrameCode);
public:
};

///////////////////////////////////////////////////////////////////
// CURI
class CURI : public CHTTPTraits
{
public:
    struct tagURI : public MObject
    {
    public:
        tagURI(void);
        ~tagURI(void);

        tagURI(const tagURI& aSrc);
        tagURI& operator=(const tagURI& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
        tagURI(tagURI&& aSrc);
        tagURI& operator=(tagURI&& aSrc);
#endif
        void       Reset(void);
        CCString   Get(void);
    public:
        CCString   strScheme;
        CCString   strUserInfo;
        CCString   strHost;
        CCString   strPath;
        CCString   strQuery;
        CCString   strFragment;
        UInt       uPort;
    };
    typedef struct tagURI URI, *PURI;
public:
    static bool  Encode(const CCString& strIn, CCString& strOut);
    static bool  Decode(const CCString& strIn, CCString& strOut);
    // local
    static bool  EncodeLocal(const CString& strIn, CCString& strOut);
    static bool  DecodeLocal(const CCString& strIn, CString& strOut);
    // local to UTF-8
    static bool  EncodeLocalToUTF8(const CString& strIn, CCString& strOut);
    static bool  DecodeUTF8ToLocal(const CCString& strIn, CString& strOut);
    // RFC 3986
    static bool  UnreservedChar(Char c);
    static bool  GenDelimChar(Char c);
    static bool  SubDelimChar(Char c);

    static bool  ReservedChar(Char c);
    static bool  SchemeChar(Char c);
    static bool  UserInfoChar(Char c);
    static bool  AuthorityChar(Char c);
    static bool  PathChar(Char c);
    static bool  QueryChar(Char c);
    static bool  FragmentChar(Char c);

    static UChar CharToHex(UChar uc);
    static UChar HexToChar(UChar uc);
public:
    static bool Parse(CCString& strUri, URI& Uri);
    static bool Check(CCString& strUri);
public:
    CURI(void);
    ~CURI(void);

    CURI(const CURI& aSrc);
    CURI(const URI& aSrc);
    CURI& operator=(const CURI& aSrc);
    CURI& operator=(const URI& Uri);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CURI(CURI&& aSrc);
    CURI(URI&& aSrc);
    CURI& operator=(CURI&& aSrc);
    CURI& operator=(URI&& Uri);
#endif
    bool  operator==(const CURI& aSrc) const;
    bool  operator==(const URI& Uri) const;

    bool  Parse(PCStr pszUri);
    bool  Parse(CCString& strUri);

    const CCString&  GetScheme(void) const;
    const CCString&  GetUserInfo(void) const;
    const CCString&  GetHost(void) const;
    const CCString&  GetPath(void) const;
    const CCString&  GetQuery(void) const;
    const CCString&  GetFragment(void) const;
    UInt  GetPort(void) const;

    URI   GetAuthority(void) const;
    URI   GetResource(void) const;

    CURI& SetScheme(CCString& strScheme);
    CURI& SetUserInfo(CCString& strUserInfo);
    CURI& SetHost(CCString& strHost);
    CURI& SetPath(CCString& strPath);
    CURI& SetQuery(CCString& strQuery);
    CURI& SetFragment(CCString& strFragment);
    CURI& SetPort(UInt uPort);

    void     Reset(void);
    CCString ToString(void);
private:
    URI   m_Uri;
};

///////////////////////////////////////////////////////////////////
// CHTTPHeader
class CHTTPHeader : public CHTTPTraits
{
public:
    CHTTPHeader(void);
    ~CHTTPHeader(void);

    CHTTPHeader(const CHTTPHeader& aSrc);
    CHTTPHeader& operator=(const CHTTPHeader& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPHeader(CHTTPHeader&& aSrc);
    CHTTPHeader& operator=(CHTTPHeader&& aSrc);
#endif

    void  SetAt(const CCString& strName, const CCString& strValue);
    void  SetAt(const CCString& strName, CCString& strValue);
    void  SetAt(const CCString& strName, PCStr pszValue);
    void  SetAt(PCStr pszName, PCStr pszValue);
    void  SetAt(PCStr pszName, const CCString& strValue);
    void  SetAt(PCStr pszName, CCString& strValue);
    void  SetAt(PCStr pszName, const CCStringRef& strValueRef);
    void  SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void  Close(void);

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_HEADER& Header(void) const;
    MAP_HEADER& Header(void);

    const MAP_HEADER& operator*(void) const;
    MAP_HEADER& operator*(void);

    const MAP_HEADER* operator->(void) const;
    MAP_HEADER* operator->(void);
private:
    MAP_HEADER   m_Header;
};

///////////////////////////////////////////////////////////////////
// CHTTPCookie
class CHTTPCookie : public CHTTPTraits
{
public:
    CHTTPCookie(void);
    ~CHTTPCookie(void);

    CHTTPCookie(const CHTTPCookie& aSrc);
    CHTTPCookie& operator=(const CHTTPCookie& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPCookie(CHTTPCookie&& aSrc);
    CHTTPCookie& operator=(CHTTPCookie&& aSrc);
#endif

    void   SetAt(const CCString& strName, const CCString& strValue);
    void   SetAt(const CCString& strName, CCString& strValue);
    void   SetAt(const CCString& strName, PCStr pszValue);
    void   SetAt(PCStr pszName, PCStr pszValue);
    void   SetAt(PCStr pszName, const CCString& strValue);
    void   SetAt(PCStr pszName, CCString& strValue);
    void   SetAt(PCStr pszName, const CCStringRef& strValueRef);
    void   SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void   Close(void);
    void   Parse(PCStr pszCookie);

    size_t FormatLength(void) const;
    void   Format(CCString& strCookie) const;
    void   Format(CStream& Stream) const;

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_COOKIE& Cookie(void) const;
    MAP_COOKIE& Cookie(void);

    const MAP_COOKIE& operator*(void) const;
    MAP_COOKIE& operator*(void);

    const MAP_COOKIE* operator->(void) const;
    MAP_COOKIE* operator->(void);
private:
    MAP_COOKIE   m_Cookie;
};

///////////////////////////////////////////////////////////////////
// CHTTPParameter : parameter value encode & decode by user
class CHTTPParameter : public CHTTPTraits
{
public:
    CHTTPParameter(void);
    ~CHTTPParameter(void);

    CHTTPParameter(const CHTTPParameter& aSrc);
    CHTTPParameter& operator=(const CHTTPParameter& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPParameter(CHTTPParameter&& aSrc);
    CHTTPParameter& operator=(CHTTPParameter&& aSrc);
#endif

    void   SetAt(const CCString& strName, const CCString& strValue);
    void   SetAt(const CCString& strName, CCString& strValue);
    void   SetAt(const CCString& strName, PCStr pszValue);
    void   SetAt(PCStr pszName, PCStr pszValue);
    void   SetAt(PCStr pszName, const CCString& strValue);
    void   SetAt(PCStr pszName, CCString& strValue);
    void   SetAt(PCStr pszName, const CCStringRef& strValueRef);
    void   SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void   Close(void);
    void   Parse(PCStr pszParameter);

    size_t FormatLength(void) const;
    void   Format(CCString& strParam) const;
    void   Format(CStream& Stream) const;

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_PARAM& Param(void) const;
    MAP_PARAM& Param(void);

    const MAP_PARAM& operator*(void) const;
    MAP_PARAM& operator*(void);

    const MAP_PARAM* operator->(void) const;
    MAP_PARAM* operator->(void);
private:
    MAP_PARAM   m_Param;
};

///////////////////////////////////////////////////////////////////
// CHTTPHeaderRef
class CHTTPHeaderRef : public CHTTPTraits
{
public:
    CHTTPHeaderRef(void);
    ~CHTTPHeaderRef(void);

    CHTTPHeaderRef(const CHTTPHeaderRef& aSrc);
    CHTTPHeaderRef& operator=(const CHTTPHeaderRef& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPHeaderRef(CHTTPHeaderRef&& aSrc);
    CHTTPHeaderRef& operator=(CHTTPHeaderRef&& aSrc);
#endif

    void  SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue);
    void  SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void  Close(void);

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_HEADER_REF& Header(void) const;
    MAP_HEADER_REF& Header(void);

    const MAP_HEADER_REF& operator*(void) const;
    MAP_HEADER_REF& operator*(void);

    const MAP_HEADER_REF* operator->(void) const;
    MAP_HEADER_REF* operator->(void);
private:
    MAP_HEADER_REF   m_Header;
};

///////////////////////////////////////////////////////////////////
// CHTTPCookieRef
class CHTTPCookieRef : public CHTTPTraits
{
public:
    CHTTPCookieRef(void);
    ~CHTTPCookieRef(void);

    CHTTPCookieRef(const CHTTPCookieRef& aSrc);
    CHTTPCookieRef& operator=(const CHTTPCookieRef& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPCookieRef(CHTTPCookieRef&& aSrc);
    CHTTPCookieRef& operator=(CHTTPCookieRef&& aSrc);
#endif

    void   SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue);
    void   SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void   Close(void);
    void   Parse(PCStr pszCookie, PCStr pszCookieEnd);

    size_t FormatLength(void) const;
    void   Format(CCString& strCookie) const;
    void   Format(CStream& Stream) const;

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_COOKIE_REF& Cookie(void) const;
    MAP_COOKIE_REF& Cookie(void);

    const MAP_COOKIE_REF& operator*(void) const;
    MAP_COOKIE_REF& operator*(void);

    const MAP_COOKIE_REF* operator->(void) const;
    MAP_COOKIE_REF* operator->(void);
private:
    MAP_COOKIE_REF   m_Cookie;
};

///////////////////////////////////////////////////////////////////
// CHTTPParameterRef : parameter value encode & decode by user
class CHTTPParameterRef : public CHTTPTraits
{
public:
    CHTTPParameterRef(void);
    ~CHTTPParameterRef(void);

    CHTTPParameterRef(const CHTTPParameterRef& aSrc);
    CHTTPParameterRef& operator=(const CHTTPParameterRef& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPParameterRef(CHTTPParameterRef&& aSrc);
    CHTTPParameterRef& operator=(CHTTPParameterRef&& aSrc);
#endif

    void   SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue);
    void   SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef);

    void   Close(void);
    void   Parse(PCStr pszParameter, PCStr pszParameterEnd);

    size_t FormatLength(void) const;
    void   Format(CCString& strParam) const;
    void   Format(CStream& Stream) const;

    CCString Find(const CCString& strName) const;
    CCString Find(PCStr pszName) const;

    const MAP_PARAM_REF& Param(void) const;
    MAP_PARAM_REF& Param(void);

    const MAP_PARAM_REF& operator*(void) const;
    MAP_PARAM_REF& operator*(void);

    const MAP_PARAM_REF* operator->(void) const;
    MAP_PARAM_REF* operator->(void);
private:
    MAP_PARAM_REF   m_Param;
};

///////////////////////////////////////////////////////////////////
// CHTTPRequest
class CHTTPRequest : public CHTTPTraits
{
    friend class CHTTPParser;
public:
    CHTTPRequest(HTTP_CONTENT eContent = HTTP_CONTENT_LENGTH);
    ~CHTTPRequest(void);

    CHTTPRequest(const CHTTPRequest& aSrc);
    CHTTPRequest& operator=(const CHTTPRequest& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPRequest(CHTTPRequest&& aSrc);
    CHTTPRequest& operator=(CHTTPRequest&& aSrc);
#endif
    bool   SetContentValue(UInt uValue);

    size_t FormatLength(void) const;

    void   Format(CCString& strFormat) const;
    void   Format(CStream& Stream) const;

    void   Close(void);

    void   SetMethod(const CCString& strMethod);
    void   SetMethod(PCStr pszMethod);
    const  CCString& GetMethod(void) const;

    void   SetPath(const CCString& strPath);
    void   SetPath(PCStr pszPath);
    const  CCString& GetPath(void) const;

    void   SetVersion(const CCString& strVersion);
    void   SetVersion(PCStr pszVersion);
    const  CCString& GetVersion(void) const;

    bool   SetContent(const CCString& strContent);
    bool   SetContent(PCStr pszContent);

    bool   SetContent(PByte pContent, size_t stSize, bool bManage = false);
    bool   SetContent(CBufStream& BufRef, bool bCopy = true);
    // chunked
    bool   AddContent(const CCString& strContent);
    bool   AddContent(PCStr pszContent);

    bool   AddContent(PByte pContent, size_t stSize, bool bManage = false);
    bool   AddContent(CBufStream& BufRef, bool bCopy = true);

    void   CleanContent(void);

    const ARY_BUF& GetContent(void) const;
    ARY_BUF& GetContent(void);

    const CHTTPHeader& GetHeader(void) const;
    CHTTPHeader& GetHeader(void);

    const CHTTPCookie& GetCookie(void) const;
    CHTTPCookie& GetCookie(void);

    const CHTTPParameter& GetParam(void) const;
    CHTTPParameter& GetParam(void);

    const MAP_HEADER& Header(void) const;
    MAP_HEADER& Header(void);

    const MAP_COOKIE& Cookie(void) const;
    MAP_COOKIE& Cookie(void);

    const MAP_PARAM& Param(void) const;
    MAP_PARAM& Param(void);
private:
    HTTP_CONTENT     m_eContentType;
    UInt             m_uContentValue;
    CCString         m_strMethod;
    CCString         m_strPath;
    CCString         m_strVersion;

    CHTTPHeader      m_Header;
    CHTTPCookie      m_Cookie;
    CHTTPParameter   m_Param;

    ARY_BUF          m_Content;
};

///////////////////////////////////////////////////////////////////
// CHTTPResponse
class CHTTPResponse : public CHTTPTraits
{
    friend class CHTTPParser;
public:
    CHTTPResponse(HTTP_CONTENT eContent = HTTP_CONTENT_LENGTH);
    ~CHTTPResponse(void);

    CHTTPResponse(const CHTTPResponse& aSrc);
    CHTTPResponse& operator=(const CHTTPResponse& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPResponse(CHTTPResponse&& aSrc);
    CHTTPResponse& operator=(CHTTPResponse&& aSrc);
#endif
    bool   SetContentValue(UInt uValue);

    size_t FormatLength(void) const;

    void   Format(CCString& strFormat) const;
    void   Format(CStream& Stream) const;

    void   Close(void);

    void   SetVersion(const CCString& strVersion);
    void   SetVersion(PCStr pszVersion);
    const  CCString& GetVersion(void) const;

    void   SetStatus(const CCString& strStatus);
    void   SetStatus(PCStr pszStatus);
    void   SetStatusCode(Int nStatus);
    const  CCString& GetStatus(void) const;
    Int    GetStatusCode(void) const;

    void   SetMessage(const CCString& strMessage);
    void   SetMessage(PCStr pszMessage);
    const  CCString& GetMessage(void) const;

    bool   SetContent(const CCString& strContent);
    bool   SetContent(PCStr pszContent);

    bool   SetContent(PByte pContent, size_t stSize, bool bManage = false);
    bool   SetContent(CBufStream& BufRef, bool bCopy = true);
    // chunked
    bool   AddContent(const CCString& strContent);
    bool   AddContent(PCStr pszContent);

    bool   AddContent(PByte pContent, size_t stSize, bool bManage = false);
    bool   AddContent(CBufStream& BufRef, bool bCopy = true);

    void   CleanContent(void);

    const ARY_BUF& GetContent(void) const;
    ARY_BUF& GetContent(void);

    const CHTTPHeader& GetHeader(void) const;
    CHTTPHeader& GetHeader(void);

    const ARY_COOKIE& GetCookies(void) const;
    ARY_COOKIE& GetCookies(void);

    const MAP_HEADER& Header(void) const;
    MAP_HEADER& Header(void);
private:
    HTTP_CONTENT     m_eContentType;
    UInt             m_uContentValue;
    CCString         m_strVersion;
    CCString         m_strStatus;
    CCString         m_strMessage;

    CHTTPHeader      m_Header;
    ARY_COOKIE       m_Cookies;

    ARY_BUF          m_Content;
};

///////////////////////////////////////////////////////////////////
// CHTTPRequestRef
class CHTTPRequestRef : public CHTTPTraits
{
    friend class CHTTPParser;
public:
    CHTTPRequestRef(HTTP_CONTENT eContent = HTTP_CONTENT_LENGTH);
    ~CHTTPRequestRef(void);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPRequestRef(CHTTPRequestRef&& aSrc);
    CHTTPRequestRef& operator=(CHTTPRequestRef&& aSrc);
#endif
    void Close(void);

    const CCStringRef& GetMethod(void) const;
    const void GetMethod(CCString& strMethod) const;

    const CCStringRef& GetPath(void) const;
    const void GetPath(CCString& strPath) const;

    const CCStringRef& GetVersion(void) const;
    const void GetVersion(CCString& strVersion) const;

    const ARY_BUF& GetContent(void) const;
    ARY_BUF& GetContent(void);

    const CHTTPHeaderRef& GetHeader(void) const;
    CHTTPHeaderRef& GetHeader(void);

    const CHTTPCookieRef& GetCookie(void) const;
    CHTTPCookieRef& GetCookie(void);

    const CHTTPParameterRef& GetParam(void) const;
    CHTTPParameterRef& GetParam(void);

    const MAP_HEADER_REF& Header(void) const;
    MAP_HEADER_REF& Header(void);

    const MAP_COOKIE_REF& Cookie(void) const;
    MAP_COOKIE_REF& Cookie(void);

    const MAP_PARAM_REF& Param(void) const;
    MAP_PARAM_REF& Param(void);

    // net-buf
    bool Attach(const NET_BUFFER& NetBuffer);
    void Detach(NET_BUFFER& NetBuffer);
    void Detach(void);

    const NET_BUFFER& NetBuffer(void) const;
    NET_BUFFER& NetBuffer(void);
private:
    CHTTPRequestRef(const CHTTPRequestRef& aSrc);
    CHTTPRequestRef& operator=(const CHTTPRequestRef& aSrc);
private:
    HTTP_CONTENT        m_eContentType;
    NET_BUFFER          m_NetBuffer;
    CCStringRef         m_strMethod;
    CCStringRef         m_strPath;
    CCStringRef         m_strVersion;

    CHTTPHeaderRef      m_Header;
    CHTTPCookieRef      m_Cookie;
    CHTTPParameterRef   m_Param;

    ARY_BUF             m_Content;
};

///////////////////////////////////////////////////////////////////
// CHTTPResponseRef
class CHTTPResponseRef : public CHTTPTraits
{
    friend class CHTTPParser;
public:
    CHTTPResponseRef(HTTP_CONTENT eContent = HTTP_CONTENT_LENGTH);
    ~CHTTPResponseRef(void);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPResponseRef(CHTTPResponseRef&& aSrc);
    CHTTPResponseRef& operator=(CHTTPResponseRef&& aSrc);
#endif
    void Close(void);

    const CCStringRef& GetVersion(void) const;
    const void GetVersion(CCString& strVersion) const;

    const CCStringRef& GetStatus(void) const;
    const void GetStatus(CCString& strStatus) const;
    Int   GetStatusCode(void) const;

    const CCStringRef& GetMessage(void) const;
    const void GetMessage(CCString& strMessage) const;

    const ARY_BUF& GetContent(void) const;
    ARY_BUF& GetContent(void);

    const CHTTPHeaderRef& GetHeader(void) const;
    CHTTPHeaderRef& GetHeader(void);

    const ARY_COOKIE_REF& GetCookies(void) const;
    ARY_COOKIE_REF& GetCookies(void);

    const MAP_HEADER_REF& Header(void) const;
    MAP_HEADER_REF& Header(void);

    // net-buf
    bool Attach(const NET_BUFFER& NetBuffer);
    void Detach(NET_BUFFER& NetBuffer);
    void Detach(void);

    const NET_BUFFER& NetBuffer(void) const;
    NET_BUFFER& NetBuffer(void);
private:
    CHTTPResponseRef(const CHTTPResponseRef& aSrc);
    CHTTPResponseRef& operator=(const CHTTPResponseRef& aSrc);
private:
    HTTP_CONTENT     m_eContentType;
    NET_BUFFER       m_NetBuffer;
    CCStringRef      m_strVersion;
    CCStringRef      m_strStatus;
    CCStringRef      m_strMessage;

    CHTTPHeaderRef   m_Header;
    ARY_COOKIE_REF   m_Cookies;

    ARY_BUF          m_Content;
};

///////////////////////////////////////////////////////////////////
// CHTTPParser
class CHTTPParser : public CHTTPTraits
{
public:
    CHTTPParser(void);
    ~CHTTPParser(void);

    CHTTPParser(const CHTTPParser& aSrc);
    CHTTPParser& operator=(const CHTTPParser& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CHTTPParser(CHTTPParser&& aSrc);
    CHTTPParser& operator=(CHTTPParser&& aSrc);
#endif
    Int  GetCacheSize(void) const;
    Int  GetHeaderLength(void) const;
    Int  GetContentLength(void) const;
    Int  GetAllChunkLength(void) const;

    bool IsHeaderComplete(void) const;
    bool IsResponseIdent(void) const;
    bool IsContentLength(void) const;
    bool IsContentChunked(void) const;
    bool IsContentComplete(void) const;
    bool IsNetBufferClone(void) const;
    bool IsNetBufferCache(void) const;

    CCString GetIdent(Int nIndex = 0) const;
    void GetContentMark(ARY_MARK& Mark) const;

    const CBufReadStream& GetCache(void) const;
    CBufReadStream& GetCache(void);
    // cache ptr include header, size = GetCacheSize()
    const PByte GetCacheBuf(void) const;
    // cache ptr begin at content, size = GetContentLength()
    const PByte GetCacheContent(void) const;

    void  Close(void);

    size_t Length(void) const;
    void Serialize(CStream& Stream);

    bool To(CHTTPRequest&  Request, bool bOnlyHead = false) const;
    bool To(CHTTPRequest&  Request, PCStr pszBuf);
    bool To(CHTTPRequest&  Request, PByte pBuf, size_t stSize);
    bool To(CHTTPResponse& Response) const;
    bool To(CHTTPResponse& Response, PCStr pszBuf);
    bool To(CHTTPResponse& Response, PByte pBuf, size_t stSize);
    // net-buffer only
    // net-buffer from req/res
    bool Attach(CHTTPRequestRef&  RequestRef);
    bool Attach(CHTTPResponseRef& ResponseRef);
    // net-buffer to req/res
    bool Detach(CHTTPRequestRef&  RequestRef);
    bool Detach(CHTTPResponseRef& ResponseRef);

    bool From(CHTTPRequest& Request);
    bool From(CHTTPResponse& Response);

    bool Format(CCString& strFormat) const;
    bool Format(CStream& Stream) const;

    bool Parse(PByte pBuf, size_t& stSize, bool bComplete = false);
    bool Parse(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool bComplete = false);
    bool ParseComplete(void);
    // net-buf
    const NET_BUFFER& NetBuffer(void) const;
    NET_BUFFER& NetBuffer(void);

    void SetNetBufferClone(bool bEnable = true);
private:
    Int  Cache(PByte pBuf, size_t stSize);
    Int  CacheNetBuffer(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver);

    bool AllocNetBuffer(const CBufReadStream& BufCache);
    bool FreeNetBuffer(void);
    bool AttachNetBuffer(NET_BUFFER& NetBuffer);
    bool DetachNetBuffer(NET_BUFFER& NetBuffer);

    bool ParseCache(Int nOldCacheSize, size_t& stSize, bool bComplete);
    bool ParseHeaderComplete(void);
    bool ParseIdent(PCStr pszBegin, PCStr pszCRLF);
    bool ParseHeader(PCStr pszBegin);

    bool ParseContentChunked(void);
    bool ParseContentLength(void);

    bool SetHeader(CHTTPRequest& Request) const;
    bool SetHeader(CHTTPResponse& Response) const;

    bool SetHeaderRef(CHTTPRequestRef& RequestRef) const;
    bool SetHeaderRef(CHTTPResponseRef& ResponseRef) const;

    bool SetContent(ARY_BUF& Content) const;
    bool SetContentRef(ARY_BUF& Content) const;

    bool SetParam(CHTTPRequest& Request, CCString& strQuery) const;
    bool SetParamRef(CHTTPRequestRef& RequestRef, PCStr pszQuery, size_t stQuery) const;
private:
    Int              m_nBufCacheSize;    // all data size
    Int              m_nHeaderLength;    // head size
    Int              m_nContentLength;   // content size
    Int              m_nAllChunkLength;  // all chunk data length, m_nContentLength is all chunk data length + all chunk desc length

    bool             m_bHeaderComplete;  // header complete
    bool             m_bResponseIdent;   // response or request
    bool             m_bContentLength;   // 'Content-Length' header
    bool             m_bContentChunked;  // 'Transfer-Encoding : chunked' header
    bool             m_bContentComplete; // content complete
    bool             m_bNetBufferClone;  // 

    NET_BUFFER       m_NetBuffer; // network buffer

    MARK             m_Ident[3];  // request-method, path, version; response-version, status, message
    ARY_MARK         m_Content;

    CBufReadStream   m_BufCache;
};

///////////////////////////////////////////////////////////////////
// CHTTP2Parser

///////////////////////////////////////////////////////////////////
// CWSParser
class CWSParser : public CWSTraits
{
public:
    // check upgrade head
    static bool   CheckUpgrade(const CHTTPRequest& Request, bool& bRet);
    static bool   CheckUpgrade(const CHTTPRequestRef& RequestRef, bool& bRet);
    static bool   CheckUpgrade(const CHTTPResponse& Response, bool& bRet);
    static bool   CheckUpgrade(const CHTTPResponseRef& ResponseRef, bool& bRet);
    // create default upgrade request, then add extensions in head
    static bool   UpgradeRequest(CHTTPRequest& Request, PCStr pszHost = nullptr, Int nVersion = 13);
    // create default upgrade response, then add extensions in head
    static bool   UpgradeResponse(const CHTTPRequest& Request, CHTTPResponse& Response);
    static bool   UpgradeResponse(const CHTTPRequestRef& RequestRef, CHTTPResponse& Response);
    // usFrame : frame-head(2BYTE)
    // uMask   : client mask
    // stFrame : frame-head-size(offset from head to payload)
    static bool   InitFrameHead(UShort& usFrame, UInt& uMask, size_t& stFrame, size_t stSize);
    static bool   InitFrameHead(UShort& usFrame, size_t& stFrame, size_t stSize);
    // 
    static bool   SetFrameHead(PByte pBuf, UShort usFrame, UInt uMask, size_t stFrame, size_t stSize);
    static size_t SetFrameHead(PByte pBuf, UShort usFrame, size_t stSize, UInt uMask = 0);
public:
    CWSParser(void);
    ~CWSParser(void);

    CWSParser(const CWSParser& aSrc);
    CWSParser& operator=(const CWSParser& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CWSParser(CWSParser&& aSrc);
    CWSParser& operator=(CWSParser&& aSrc);
#endif
    Int  GetFrameSize(void) const;
    Int  GetFrameCode(void) const;
    Int  GetPayloadPos(void) const;
    Int  GetPayloadSize(void) const;

    bool IsHeadComplete(void) const;
    bool IsFrameMasked(void) const;
    bool IsFramePayload(void) const;
    bool IsFrameComplete(void) const;
    bool IsControlFrame(void) const;
    bool IsFinalFrame(void) const;
    bool IsNetBufferClone(void) const;
    bool IsNetBufferCache(void) const;

    const CBufReadStream& GetCache(void) const;
    CBufReadStream& GetCache(void);
    // cache ptr include frame head, size = GetFrameSize()
    const PByte GetCacheBuf(void) const;
    // cache ptr begin at payload, size = GetPayloadSize()
    const PByte GetCachePayload(void) const;

    void Close(void);

    size_t Length(void) const;
    void Serialize(CStream& Stream);

    bool To(CStream&  Stream) const;
    bool To(CStream&  Stream, PCStr pszBuf);
    bool To(CStream&  Stream, PByte pBuf, size_t stSize);
    bool To(CCString& strContent) const;
    bool To(CCString& strContent, PCStr pszBuf);
    bool To(CCString& strContent, PByte pBuf, size_t stSize);
    // An unfragmented frame message : (WS_FINAL|WS_BINARY), (WS_FINAL|WS_BINARY|WS_MASK)
    //                                 (WS_FINAL|WS_TEXT), (WS_FINAL|WS_TEXT|WS_MASK)
    //
    // A fragmented frame message    : (WS_BINARY), (WS_BINARY|WS_MASK), (WS_TEXT), (WS_TEXT|WS_MASK)
    //                                 (WS_CONTINUE), (WS_CONTINUE|WS_MASK)
    //                                 (WS_FINAL), (WS_FINAL|WS_MASK)
    //
    // A control frame : (WS_FINAL|WS_CLOSE), (WS_FINAL|WS_PING), (WS_FINAL|WS_PONG)
    //                   (WS_FINAL|WS_CLOSE|WS_MASK), (WS_FINAL|WS_PING|WS_MASK), (WS_FINAL|WS_PONG|WS_MASK)
    bool From(CStream& Stream, Int nFrame = (WS_FINAL|WS_BINARY), UInt uMask = 0);
    bool From(const CCString& strContent, Int nFrame = (WS_FINAL|WS_TEXT), UInt uMask = 0);
    bool From(Int nFrame = (WS_FINAL|WS_BINARY), UInt uMask = 0);

    bool Format(CStream& Stream) const;
    bool Format(CCString& strFormat) const;

    Int  Parse(PByte pBuf, size_t& stSize);
    Int  Parse(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize);
    Int  Parse(Int nFrameCode, Int nFrameData, Int nPayload);
    // net-buf
    bool Attach(const NET_BUFFER& NetBuffer);
    bool Attach(Int nFrameCode, Int nFrameData, Int nPayload);
    bool Attach(NET_BUFFER& NetBuffer, Int nFrameCode, Int nFrameData, Int nPayload);

    void Detach(NET_BUFFER& NetBuffer);
    void Detach(Int& nFrameSize, Int& nFrameCode, Int& nFrameData);
    void Detach(NET_BUFFER& NetBuffer, Int& nFrameSize, Int& nFrameCode, Int& nFrameData);
    void Detach(void);

    const NET_BUFFER& NetBuffer(void) const;
    NET_BUFFER& NetBuffer(void);

    void SetNetBufferClone(bool bEnable = true);
private:
    Int  Cache(PByte pBuf, size_t stSize);
    Int  CacheNetBuffer(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver);

    bool AllocNetBuffer(const CBufReadStream& BufCache);
    bool FreeNetBuffer(void);

    Int  ParseCache(Int nFrameCache, size_t& stSize);
    Int  ParseFrameHead(void);
    Int  ParsePayload(void);

    bool GetFrame(CStream& Stream) const;
    bool GetFrame(CCString& strContent) const;

    bool SetFrame(UShort usFrame, UInt uMask, CStream* pStream = nullptr);
private:
    Int              m_nFrameCache;     // cache size
    Int              m_nFrameSize;      // all data size
    Int              m_nFrameCode;      // WS_FRAME
    Int              m_nFrameData;      // payload data begin pos

    bool             m_bHeadComplete;   // frame head complete
    bool             m_bFrameMasked;    // if frame-code with WS_MASK; true-->frame-data XOR frame-mask transformed, false-->frame-data not transformed
    bool             m_bFramePayload;   // payload > 0
    bool             m_bFrameComplete;  // frame complete
    bool             m_bNetBufferClone; //

    NET_BUFFER       m_NetBuffer; // network buffer

    CBufReadStream   m_FrameCache;
};

///////////////////////////////////////////////////////////////////
// CWEBSession
class NOVTABLE CWEBSession ABSTRACT : public CTRefCount<CWEBSession>
{
public:
    enum WEB_SESSION
    {
        WEB_SESSION_NONE,
        WEB_SESSION_HTTP,
        WEB_SESSION_HTTP_TLS,
        WEB_SESSION_WS,
        WEB_SESSION_WS_TLS,
    };

    enum WEB_DEF
    {
        WEB_DEF_LIVE_CHECK = 32 * 1000,
        WEB_DEF_SIZE_BLOCK = 80 * 1024,
        WEB_DEF_SIZE_CACHE = 10 * 1024 * 1024,
    };

public:
    virtual ~CWEBSession(void);
    virtual bool   OnData(PByte pData, size_t& stSize, bool& bComplete) PURE;
    virtual bool   OnData(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool& bComplete) PURE;
    virtual bool   OnDataHandled(void) PURE;

    virtual bool   CheckDataCompleted(void) PURE;
    virtual size_t Length(void) const PURE;
    virtual void   Serialize(CStream& Stream) PURE;

    CNETTraits::Socket GetSocket(void) const;

    UInt    GetType(void) const;

    UInt    GetStatus(void) const;
    void    SetStatus(UInt uStatus);

    bool    CheckUpgrade(void) const;
    void    SetUpgraded(void);

    bool    ApplyHandle(bool bApply = true);
    // http : max head size
    // ws : max frame size
    size_t  GetMaxBlockSize(void) const;
    void    SetMaxBlockSize(size_t stMaxBlock);
    // http : max head + body size
    // ws : max cache size
    size_t  GetMaxCacheSize(void) const;
    void    SetMaxCacheSize(size_t stMaxCache);
protected:
    CWEBSession(CNETTraits::Socket sSocket, UInt uType);
protected:
    CNETTraits::Socket   m_sSocket;
    UInt                 m_uType;
    UInt                 m_uStatus;
    UInt                 m_uUpgraded;
    UInt                 m_uHandled;
    size_t               m_stMaxBlock;
    size_t               m_stMaxCache;
};
typedef CTRefCountPtr<CWEBSession> CWEBSessionPtr;

typedef CTArray<CWEBSessionPtr>                           ARY_WEBSESSION;

typedef CTMap<CNETTraits::Socket, CWEBSessionPtr>         MAP_WEBSESSION, *PMAP_WEBSESSION;
typedef CTMap<CNETTraits::Socket, CWEBSessionPtr>::PAIR   PAIR_WEBSESSION;

///////////////////////////////////////////////////////////////////
// CHTTPSession
class CHTTPSession : public CWEBSession
{
public:
    CHTTPSession(CNETTraits::Socket sSocket);
    CHTTPSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef);
    virtual ~CHTTPSession(void);

    virtual bool   OnData(PByte pData, size_t& stSize, bool& bComplete) OVERRIDE;
    virtual bool   OnData(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool& bComplete) OVERRIDE; 
    virtual bool   OnDataHandled(void) OVERRIDE;

    virtual bool   CheckDataCompleted(void) OVERRIDE;
    // CHTTPParser's length & serialize
    virtual size_t Length(void) const OVERRIDE;
    virtual void   Serialize(CStream& Stream) OVERRIDE;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;

    CHTTPParser& GetParser(void);
    const CHTTPParser& GetParser(void) const;
protected:
    CHTTPSession(CNETTraits::Socket sSocket, UInt uType);
    CHTTPSession(CNETTraits::Socket sSocket, UInt uType, CEventHandlerPtr& EventHandlerRef);
protected:
    CEventHandlerPtr   m_EventHandlerPtr;
    CHTTPParser        m_Parser;
};
typedef CTRefCountPtr<CHTTPSession> CHTTPSessionPtr;

///////////////////////////////////////////////////////////////////
// CWSSession
class CWSSession : public CWEBSession
{
public:
    enum WS_CHECK
    {
        WS_CHECK_NONE  = 0x0000,
        WS_CHECK_AUTO  = 0x0001, // auto keep-alive
        WS_CHECK_CLOSE = 0x0002, // close control frame handle
        WS_CHECK_PING  = 0x0004,
        WS_CHECK_PONG  = 0x0008,
        WS_CHECK_UNITE = 0x0010, // unite fragmented frame
    };
public:
    CWSSession(CNETTraits::Socket sSocket);
    CWSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef);
    virtual ~CWSSession(void);

    virtual bool   OnData(PByte pData, size_t& stSize, bool& bComplete) OVERRIDE;
    virtual bool   OnData(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool& bComplete) OVERRIDE;
    virtual bool   OnDataHandled(void) OVERRIDE;

    virtual bool   CheckDataCompleted(void) OVERRIDE;
    // (last fragmented frame) CWSParser's length & serialize
    virtual size_t Length(void) const OVERRIDE;
    virtual void   Serialize(CStream& Stream) OVERRIDE;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;

    CWSParser& GetParser(void);
    const CWSParser& GetParser(void) const;

    bool IsLiveCheck(bool bUpdate = true);
    void UpdateLive(void);

    UInt GetLiveCheck(void) const; // MS
    void SetLiveCheck(UInt uCheck = CWEBSession::WEB_DEF_LIVE_CHECK);

    bool GetAutoCheck(void) const;
    void SetAutoCheck(bool bEnable = true);

    bool GetControlCheck(void) const;

    bool GetCloseCheck(void) const;
    void SetCloseCheck(bool bEnable = true);

    bool GetPingCheck(void) const;
    void SetPingCheck(bool bEnable = true);

    bool GetPongCheck(void) const;
    void SetPongCheck(bool bEnable = true);

    bool GetUniteFrame(void) const;
    void SetUniteFrame(bool bEnable = true);

    bool IsUniteCache(void) const;
    bool IsCacheComplete(void) const;
protected:
    CWSSession(CNETTraits::Socket sSocket, UInt uType);
    CWSSession(CNETTraits::Socket sSocket, UInt uType, CEventHandlerPtr& EventHandlerRef);
private:
    bool UniteCache(void);
    bool UniteCache(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam);
protected:
    LLong              m_llOnline;
    UInt               m_uLiveCheck; // default : CWEBSession::WEB_DEF_LIVE_CHECK
    UInt               m_uCheckFlag; // default : auto-check, close
    Int                m_nUniteCode;
    Int                m_nUniteData;
    Int                m_nUniteSize;

    CEventHandlerPtr   m_EventHandlerPtr;

    CWSParser          m_Parser;
    CWSParser          m_UniteCache;
};
typedef CTRefCountPtr<CWSSession> CWSSessionPtr;

///////////////////////////////////////////////////////////////////
// CWEBServer
class CWEBServer : public CNulPackNetworkEventHandler
{
public:
    enum WEB_PORT
    {
        WEB_DEFPORT = 80,
    };
public:
    CWEBServer(void);
    virtual ~CWEBServer(void);

    bool Init(CNetworkPtr& NetworkPtr, CEventHandler& EventHandlerRef, bool bManage = true);
    void Exit(void);

    bool Check(void);

    bool Listen(PCXStr pszIp = nullptr, UShort usPort = WEB_DEFPORT); 
    bool Listen(STR_ADDR& strAddr);
    bool Listen(NET_ADDR& NetAddr);

    bool Send(Socket sSocket, PCStr pszData, size_t stSize);
    bool Send(Socket sSocket, const CCString& strData);
    bool Send(Socket sSocket, PByte pData, size_t stSize);
    bool Send(Socket sSocket, const CBufStream& Stream);
    bool Send(Socket sSocket, CStreamScopePtr& StreamPtr);

    bool Destory(Socket sSocket);

    void    SetHash(Int nHashSize);

    bool    IsNetBufferMode(void) const;
    void    SetNetBufferMode(void);

    size_t  GetMaxBlockSize(void) const;
    void    SetMaxBlockSize(size_t stMaxBlock);

    size_t  GetMaxCacheSize(void) const;
    void    SetMaxCacheSize(size_t stMaxCache);

    CNetworkPtr& GetNetwork(void);
    const CNetworkPtr& GetNetwork(void) const;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;
protected:
    virtual bool OnTcpAccept(Socket sAccept, Socket sListen) OVERRIDE;
    virtual bool OnTcpRecv(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData) OVERRIDE;

    bool OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);
    bool OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);

    bool CreateSession(Socket sSocket);
    bool DestroySession(Socket sSocket);
    bool CheckSession(Socket sSocket);

    bool GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr);
    bool RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr);

    bool ListenHandle(void);
    bool SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs);
    bool RecvHandle(CWEBSessionPtr& SessionPtr);
    bool CloseHandle(CWEBSessionPtr& SessionPtr);

    bool ErrorHandle(CWEBSessionPtr& SessionPtr);

    bool CompleteHandle(CWEBSessionPtr& SessionPtr);
    Int  CompleteWSCheck(CWSSession* pSession, CWEBSessionPtr& SessionPtr);

    bool UpgradeHandle(CWEBSessionPtr& SessionPtr);
    // ws enable ping-control frame handle
    bool UpgradeWSSession(const CHTTPRequest& Request, CHTTPSession* pSession, CWEBSessionPtr& SessionPtr);
protected:
    size_t             m_stMaxBlock;
    size_t             m_stMaxCache;

    bool               m_bManage;
    bool               m_bNetBuffer;
    Socket             m_sSocket;
    NET_ATTR           m_NetAttr;
    NET_ADDR           m_NetAddr;

    CNetworkPtr        m_NetworkPtr;
    CEventHandlerPtr   m_EventHandlerPtr;

    MAP_WEBSESSION     m_Session;
    CSyncLock          m_SessionLock;
};

///////////////////////////////////////////////////////////////////
// CWEBClient
class CWEBClient : public CNulPackNetworkEventHandler
{
public:
   CWEBClient(void);
   virtual ~CWEBClient(void);

    bool Init(CNetworkPtr& NetworkPtr, bool bManage = true);
    bool Init(CNetworkPtr& NetworkPtr, CEventHandler& EventHandlerRef, bool bManage = true);
    void Exit(void);

    bool Check(void);

    bool Create(Socket& sSocket, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr);
    bool Create(Socket& sSocket, CEventHandler& EventHandlerRef, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr);
    bool Create(Socket& sSocket, NET_ADDR& NetAddrRef);
    bool Create(Socket& sSocket, CEventHandler& EventHandlerRef, NET_ADDR& NetAddrRef);

    bool Destory(Socket sSocket);

    bool Connect(Socket sSocket, PCXStr pszIp = nullptr, UShort usPort = CWEBServer::WEB_DEFPORT); 
    bool Connect(Socket sSocket, STR_ADDR& strAddr);
    bool Connect(Socket sSocket, NET_ADDR& NetAddr);

    bool Send(Socket sSocket, PCStr pszData, size_t stSize);
    bool Send(Socket sSocket, const CCString& strData);
    bool Send(Socket sSocket, PByte pData, size_t stSize);
    bool Send(Socket sSocket, const CBufStream& Stream);
    bool Send(Socket sSocket, CStreamScopePtr& StreamPtr);

    void    SetHash(Int nHashSize);

    bool    IsNetBufferMode(void) const;
    void    SetNetBufferMode(void);

    bool    IsAutoCheckMode(void) const;
    void    SetAutoCheckMode(void);

    UInt    GetControlCode(void) const;
    void    SetControlCode(bool bPing = true); // false = pong

    size_t  GetMaxBlockSize(void) const;
    void    SetMaxBlockSize(size_t stMaxBlock);

    size_t  GetMaxCacheSize(void) const;
    void    SetMaxCacheSize(size_t stMaxCache);

    CNetworkPtr& GetNetwork(void);
    const CNetworkPtr& GetNetwork(void) const;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;
protected:
    virtual bool OnTcpConnect(UInt uError, Socket sConnect) OVERRIDE;
    virtual bool OnTcpRecv(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData) OVERRIDE;

    bool OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);
    bool OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);

    bool CreateSession(Socket& sSocket, NET_ADDR& NetAddr);
    bool CreateSession(Socket& sSocket, NET_ADDR& NetAddr, CEventHandler& EventHandlerRef);
    
    bool DestroySession(Socket sSocket);
    bool CheckSession(Socket sSocket);

    bool GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr);
    bool RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr);

    bool GetHandler(CWEBSessionPtr& SessionPtr, CEventHandlerPtr& EventHandlerPtr);

    bool SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs);
    bool RecvHandle(CWEBSessionPtr& SessionPtr);
    bool CloseHandle(CWEBSessionPtr& SessionPtr);

    bool ErrorHandle(CWEBSessionPtr& SessionPtr);

    bool CompleteHandle(CWEBSessionPtr& SessionPtr);
    Int  CompleteWSCheck(CWSSession* pSession, CWEBSessionPtr& SessionPtr);

    bool UpgradeHandle(CWEBSessionPtr& SessionPtr);
    // set m_bAutoCheck to ws-session
    bool UpgradeWSSession(const CHTTPResponse& Response, CHTTPSession* pSession, CWEBSessionPtr& SessionPtr);

protected:
    size_t             m_stMaxBlock;
    size_t             m_stMaxCache;

    bool               m_bManage;
    bool               m_bExited;
    bool               m_bNetBuffer;
    bool               m_bAutoCheck;   // ws auto check
    UInt               m_uControlCode; // default : ping
    NET_ATTR           m_NetAttr;

    CNetworkPtr        m_NetworkPtr;
    CEventHandlerPtr   m_EventHandlerPtr;

    MAP_WEBSESSION     m_Session;
    CSyncLock          m_SessionLock;
};

///////////////////////////////////////////////////////////////////
#include "networkhttp.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
#include "windows/targetnetworkhttp.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
#include "linux/targetnetworkhttp.inl"
#else
#error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_HTTP_H__
