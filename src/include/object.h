// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __OBJECT_H__
#define __OBJECT_H__

#pragma once

#include "container.h"
#include "uid.h"
#include "rtti.h"
#include "event.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// rtti object base
typedef CTRefCountPtr<CObject> CObjectPtr;

///////////////////////////////////////////////////////////////////
// CObject
class CORECLASS NOVTABLE CObject ABSTRACT : public CEventHandler
{
    DECL_RTTI_TYPE( CObject )
#ifndef __MODERN_CXX_NOT_SUPPORTED
        DECL_TYPE_SFUNCV( StaticLinkName,      CObject*, CObject, StaticLink,   PCXStr pszClass, PCXStr pszName, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED );
        DECL_TYPE_SFUNCV( StaticLinkUUID,      CObject*, CObject, StaticLink,   PCXStr pszClass, const CUUID& uuid, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED );

        DECL_TYPE_SFUNCV( StaticUnlinkName,    bool,     CObject, StaticUnlink, PCXStr pszClass, PCXStr pszName = nullptr );
        DECL_TYPE_SFUNCV( StaticUnlinkUUID,    bool,     CObject, StaticUnlink, PCXStr pszClass, const CUUID& uuid );

        DECL_TYPE_SFUNCV( StaticFindClass,     Int,      CObject, StaticFind,   CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName = nullptr );
        DECL_TYPE_SFUNCV( StaticFindRTTI,      Int,      CObject, StaticFind,   CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );

        DECL_TYPE_SFUNCV( StaticFindName,      bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, PCXStr pszName, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );
        DECL_TYPE_SFUNCV( StaticFindUUID,      bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, const CUUID& uuid, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false );

        DECL_TYPE_SFUNCV( StaticFindNameClass, bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, PCXStr pszName, PCXStr pszClassName = nullptr );
        DECL_TYPE_SFUNCV( StaticFindUUIDClass, bool,     CObject, StaticFind,   CObjectPtr& ObjectPtr, const CUUID& uuid, PCXStr pszClassName = nullptr );

        DECL_TYPE_CFUNC( GetKey, PINDEX, CObject, GetKey );

        DECL_TYPE_CFUNCV( GetName,   bool, CObject, GetName, CString& strName );
        DECL_TYPE_CFUNCV( GetModule, bool, CObject, GetModule, CString& strPath );

        DECL_TYPE_FUNCV( Link,     bool, CObject, Link, PCXStr pszName );
        DECL_TYPE_FUNCV( LinkUUID, bool, CObject, Link, const CUUID& uuid );

        DECL_TYPE_FUNC( Unlink, void, CObject, Unlink );
#else
        DECL_TYPE_SFUNCV( StaticLinkName,      CObject*, CObject, StaticLink,   PCXStr, PCXStr, PCXStr, size_t );
        DECL_TYPE_SFUNCV( StaticLinkUUID,      CObject*, CObject, StaticLink,   PCXStr, const CUUID&, PCXStr, size_t );

        DECL_TYPE_SFUNCV( StaticUnlinkName,    bool,     CObject, StaticUnlink, PCXStr, PCXStr );
        DECL_TYPE_SFUNCV( StaticUnlinkUUID,    bool,     CObject, StaticUnlink, PCXStr, const CUUID& );

        DECL_TYPE_SFUNCV( StaticFindClass,     Int,      CObject, StaticFind,   CTArray<CObjectPtr>&, PCXStr );
        DECL_TYPE_SFUNCV( StaticFindRTTI,      Int,      CObject, StaticFind,   CTArray<CObjectPtr>&, const CRTTI&, bool );

        DECL_TYPE_SFUNCV( StaticFindName,      bool,     CObject, StaticFind,   CObjectPtr&, PCXStr, const CRTTI&, bool );
        DECL_TYPE_SFUNCV( StaticFindUUID,      bool,     CObject, StaticFind,   CObjectPtr&, const CUUID&, const CRTTI&, bool );

        DECL_TYPE_SFUNCV( StaticFindNameClass, bool,     CObject, StaticFind,   CObjectPtr&, PCXStr, PCXStr );
        DECL_TYPE_SFUNCV( StaticFindUUIDClass, bool,     CObject, StaticFind,   CObjectPtr&, const CUUID&, PCXStr );

        DECL_TYPE_CFUNC( GetKey, PINDEX, CObject, GetKey );

        DECL_TYPE_CFUNCV( GetName,   bool, CObject, GetName,   CString& );
        DECL_TYPE_CFUNCV( GetModule, bool, CObject, GetModule, CString& );

        DECL_TYPE_FUNCV( Link,     bool, CObject, Link, PCXStr );
        DECL_TYPE_FUNCV( LinkUUID, bool, CObject, Link, const CUUID& );

        DECL_TYPE_FUNC( Unlink, void, CObject, Unlink );
#endif
    DECL_TYPE_END( CObject )
public:
    enum LINK_FLAG
    {
        FLAG_NONE    = 0x00000000,
        FLAG_MANAGED = 0x00000001,
    };
public:
    // MANAGED linked object instance(need rtti create-object implement)
    // 1. StaticLink create object instance by rtti default constructor, linked successed INCREMENT REFCOUNT
    // 2. StaticUnlink to DECREMENT REFCOUNT, MANAGED object instance's REFCOUNT = 0 will be destroyed
    static  CObject* StaticLink(PCXStr pszClass, PCXStr pszName, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED);
    static  CObject* StaticLink(PCXStr pszClass, const CUUID& uuid, PCXStr pszModule = nullptr, size_t stFlag = FLAG_MANAGED);

    // pszName = nullptr or empty uuid value DECREMENT REFCOUNT of all MANAGED pszClass's object instance
    static  bool     StaticUnlink(PCXStr pszClass, PCXStr pszName = nullptr);
    static  bool     StaticUnlink(PCXStr pszClass, const CUUID& uuid);

    // find linked or MANAGED linked object instance
    static  Int      StaticFind(CTArray<CObjectPtr>& ObjectPtrs, PCXStr pszClassName = nullptr);
    static  Int      StaticFind(CTArray<CObjectPtr>& ObjectPtrs, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false);

    static  bool     StaticFind(CObjectPtr& ObjectPtr, PCXStr pszName, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false);
    static  bool     StaticFind(CObjectPtr& ObjectPtr, const CUUID& uuid, const CRTTI& rtti = ClassRTTI(), bool bExactClass = false);

    static  bool     StaticFind(CObjectPtr& ObjectPtr, PCXStr pszName, PCXStr pszClassName = nullptr);
    static  bool     StaticFind(CObjectPtr& ObjectPtr, const CUUID& uuid, PCXStr pszClassName = nullptr);
public:
    // abstract functions, override only
    virtual UInt     Init(void);
    virtual void     Exit(void);
    // abstract functions, override only
    virtual void     Serialize(CStream&);
    
    // linked object instance key, not linked is nullptr
    PINDEX  GetKey(void) const;
    // uuid linked name is CUUID::UUID_FROMAT_DIGITS_BRACES string {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
    bool    GetName(CString& strName) const;
    // if object instance is StaticLink from module, return module path
    bool    GetModule(CString& strPath) const;

    // link object instance by name or uuid
    // or reset linked object instance name or uuid
    bool    Link(PCXStr pszName);
    bool    Link(const CUUID& uuid);
    // unlink object instance
    void    Unlink(void);
protected:
    CObject(void);
    virtual ~CObject(void);
private:
    PINDEX   m_inxKey;
    PINDEX   m_inxModule;
};

///////////////////////////////////////////////////////////////////
// CSlotObject : object slot
class CORECLASS NOVTABLE CSlotObject ABSTRACT : public CTRefCount<CSlotObject>
{
public:
    virtual ~CSlotObject(void);

    CSlotObject(const CSlotObject& aSrc);
    CSlotObject& operator=(const CSlotObject& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CSlotObject(CSlotObject&& aSrc);
    CSlotObject& operator=(CSlotObject&& aSrc);
#endif

    bool  operator==(const CSlotObject& aSrc);

    CObjectPtr GetObjectPtr(void);
    const CRTTI* GetRTTI(void);

    PCXStr GetClass(void) const;
    PCXStr GetName(void) const;
    PCXStr GetFuncName(void) const;
    uintptr_t GetFunc(void) const;
    size_t GetType(void) const;

    // IsEventQueue() | IsEventTick()
    bool   IsEvent(void) const;
    // stType|VART_EVENTQUEUE : support event-queue
    bool   IsEventQueue(void) const;
    // stType|VART_EVENTTICK : support event-tick
    bool   IsEventTick(void) const;
    // stType|VART_RTTITYPE : get func from rtti-type
    bool   IsRTTIType(void) const;
    // stType|VART_RTTIDYNAMIC : get object ptr each time by rtti class & name
    // otherwise m_pObject or m_pRTTI ensure safe during the life cycle 
    bool   IsRTTIDynamic(void) const;
    // func ptr(m_utFunc) is valid?
    bool   IsFunction(void) const;
    // const func
    bool   IsConst(void) const;
    // static func
    bool   IsStatic(void) const;
protected:
    CSlotObject(CObject* pObj, uintptr_t utFunc, size_t stType);
    CSlotObject(CObject* pObj, CPCXStr pszFunc, size_t stType);
    CSlotObject(const CRTTI& rtti, uintptr_t utFunc, size_t stType);
    CSlotObject(const CRTTI& rtti, CPCXStr pszFunc, size_t stType);
    CSlotObject(PCXStr pszClass, PCXStr pszName, uintptr_t utFunc, size_t stType);
    CSlotObject(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType);
private:
    const CRTTI*    m_pRTTI;
    CObject*        m_pObject;
    // !!! slot will be repeated if func ptr is the same as rtti-func-type
    uintptr_t       m_utFunc;
    size_t          m_stType;
    CString         m_strFunc; // get func from rtti-type
    CString         m_strClass;
    CString         m_strName;
};
typedef CTRefCountPtr<CSlotObject> CSlotObjectPtr;

///////////////////////////////////////////////////////////////////
// CSlotEvent : object slot for eventhandler
class CSlotEvent : public CSlotObject
{
public:
    CSlotEvent(CObject* pObj, size_t stType = VART_NONE);
    CSlotEvent(PCXStr pszClass, PCXStr pszName, size_t stType = VART_NONE);
    virtual ~CSlotEvent(void);

    UInt operator()(uintptr_t utEvent, uintptr_t utData, ULLong ullParam);
    UInt operator()(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam);
    UInt operator()(uintptr_t utEvent, CStream& Stream, ULLong ullParam);
    UInt operator()(uintptr_t utEvent, UInt uCount);

    PINDEX GetIndex(void) const;
    void   SetIndex(PINDEX index);
private:
    PINDEX   m_Index;
};
typedef CTRefCountPtr<CSlotEvent> CSlotEventPtr;

///////////////////////////////////////////////////////////////////
// CTSlotObject
template <typename TRET, typename... TARGS>
class CTSlotObject : public CSlotObject
{
public:
    virtual ~CTSlotObject(void);
    virtual TRET operator()(TARGS... Args) PURE;
protected:
    CTSlotObject(CObject* pObj, uintptr_t utFunc, size_t stType);
    CTSlotObject(CObject* pObj, CPCXStr pszFunc, size_t stType);
    CTSlotObject(const CRTTI& rtti, uintptr_t utFunc, size_t stType);
    CTSlotObject(const CRTTI& rtti, CPCXStr pszFunc, size_t stType);
    CTSlotObject(PCXStr pszClass, PCXStr pszName, uintptr_t utFunc, size_t stType);
    CTSlotObject(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType);
};

///////////////////////////////////////////////////////////////////
// CTSlotFunc : func ptr
template <typename TOBJECT, typename TFUNC>
class CTSlotFunc;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTSlotFunc<TOBJECT, TRET(TARGS...)> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (TOBJECT::* SLOT_FUNC)(TARGS...);
public:
    CTSlotFunc(CObject* pObj, SLOT_FUNC pFunc, size_t stType = VART_NONE);
    // TOBJECT - base class(rtti maybe not decl & def), pszClass - dervie class name, pFunc - virtual function
    CTSlotFunc(PCXStr pszClass, PCXStr pszName, SLOT_FUNC pFunc, size_t stType = VART_NONE);
    virtual ~CTSlotFunc(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args);

    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_FUNC   m_pFunc;
};

///////////////////////////////////////////////////////////////////
// CTSlotType : get func ptr from rtti-type
template <typename TOBJECT, typename TFUNC>
class CTSlotType;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTSlotType<TOBJECT, TRET(TARGS...)> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (TOBJECT::* SLOT_FUNC)(TARGS...);
public:
    CTSlotType(CObject* pObj, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    // TOBJECT - base class(ensure rtti decl & def), pszClass - dervie class name, pszFunc - virtual function name reflect by TOBJECT
    CTSlotType(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    virtual ~CTSlotType(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
    bool GetRTTIFunc(void);

#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args);

    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(TOBJ* pObj, SLOT_FUNC pFunc, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_FUNC   m_pFunc;
};

///////////////////////////////////////////////////////////////////
// CTSlotCFunc : const func ptr
template <typename TOBJECT, typename TFUNC>
class CTSlotCFunc;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTSlotCFunc<TOBJECT, TRET(TARGS...) const> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (TOBJECT::* SLOT_CFUNC)(TARGS...) const;
public:
    CTSlotCFunc(CObject* pObj, SLOT_CFUNC pFuncConst, size_t stType = VART_NONE);
    // TOBJECT - base class(rtti maybe not decl & def), pszClass - dervie class name, pFunc - virtual function
    CTSlotCFunc(PCXStr pszClass, PCXStr pszName, SLOT_CFUNC pFuncConst, size_t stType = VART_NONE);
    virtual ~CTSlotCFunc(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args);

    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_CFUNC   m_pFuncConst;
};

///////////////////////////////////////////////////////////////////
// CTSlotCType : get const func ptr from rtti-type
template <typename TOBJECT, typename TFUNC>
class CTSlotCType;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTSlotCType<TOBJECT, TRET(TARGS...) const> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (TOBJECT::* SLOT_CFUNC)(TARGS...) const;
public:
    CTSlotCType(CObject* pObj, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    // TOBJECT - base class(ensure rtti decl & def), pszClass - dervie class name, pszFunc - virtual function name reflect by TOBJECT
    CTSlotCType(PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    virtual ~CTSlotCType(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
    bool GetRTTICFunc(void);

#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args);

    template <typename TOBJ, typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(TOBJ* pObj, SLOT_CFUNC pFuncConst, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_CFUNC   m_pFuncConst;
};

///////////////////////////////////////////////////////////////////
// CTSlotSFunc : static func ptr
template <typename TFUNC>
class CTSlotSFunc;
template <typename TRET, typename... TARGS>
class CTSlotSFunc<TRET(TARGS...)> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (* SLOT_SFUNC)(TARGS...);
public:
    CTSlotSFunc(const CRTTI& rtti, SLOT_SFUNC pFuncStatic, size_t stType = VART_NONE);
    CTSlotSFunc(PCXStr pszClass, SLOT_SFUNC pFuncStatic, size_t stType = VART_NONE);
    virtual ~CTSlotSFunc(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args);

    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_SFUNC   m_pFuncStatic;
};

///////////////////////////////////////////////////////////////////
// CTSlotSType : get static func ptr from rtti-type
template <typename TOBJECT, typename TFUNC>
class CTSlotSType;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTSlotSType<TOBJECT, TRET(TARGS...)> : public CTSlotObject<TRET, TARGS...>
{
public:
    typedef TRET (* SLOT_SFUNC)(TARGS...);
public:
    CTSlotSType(const CRTTI& rtti, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    CTSlotSType(PCXStr pszClass, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    virtual ~CTSlotSType(void);

    virtual TRET operator()(TARGS... Args) OVERRIDE;

    bool IsValid(void) const; 
private:
    bool GetRTTISFunc(void);

#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args);

    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(const CRTTI* pRTTI, SLOT_SFUNC pFuncStatic, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    SLOT_SFUNC   m_pFuncStatic;
};

///////////////////////////////////////////////////////////////////
// CSignalEvent
class CSignalEvent : public CEventHandler
{
public:
    typedef CTList<CSlotEventPtr>   LST_EVENT;
public:
    // bRegular = true : slots instances within the life cycle are fixed
    CSignalEvent(bool bRegular = true, UInt uThreads = 0);
    virtual ~CSignalEvent(void);

    bool   Init(void);
    void   Exit(void);

    PINDEX Connect(CSlotEventPtr& EventPtr);
    bool   Disconnect(PINDEX index = nullptr);

    UInt   operator()(uintptr_t utEvent, uintptr_t utData, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = CEventQueue::EVENT_TYPE_NONE);
    UInt   operator()(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = CEventQueue::EVENT_TYPE_REFCOUNT);
    UInt   operator()(uintptr_t utEvent, CStream& Stream, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = CEventQueue::EVENT_TYPE_CACHE_QUEUE);
    // tick create & destroy, index from connect, utEvent = index
    bool   Start(PINDEX index, UInt uInterval, UInt uCount = (UInt)TIMET_INFINITE);
    bool   Stop(PINDEX index);

    UInt   GetCount(void) const;
    UInt   GetThreads(void) const;
    void   SetThreads(UInt uThreads);

    size_t GetCacheSize(void) const;
    void   SetCacheSize(size_t stCacheSize);

    void   SetThreadEvent(UInt uStart, UInt uStop);

    bool   StartQueue(void);
    bool   StopQueue(void);

    CEventQueuePtr& GetEventQueuePtr(void);
protected:
    // CEventHandler
    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam) OVERRIDE;
    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount) OVERRIDE;

    UInt OnRegularHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam);
    UInt OnRegularHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam);
    UInt OnRegularHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam);
    UInt OnRegularHandle(uintptr_t utEvent, UInt uCount);

    UInt OnAssignHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam);
    UInt OnAssignHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam);
    UInt OnAssignHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam);
    UInt OnAssignHandle(uintptr_t utEvent, UInt uCount);

    bool StartTick(PINDEX index, UInt uInterval, UInt uCount);
    bool StopTick(PINDEX index);
private:
    bool             m_bRegular;
    UInt             m_uThreads;
    UInt             m_uFlag;
    UInt             m_uStartEvent;
    UInt             m_uStopEvent;
    size_t           m_stCacheSize;
    CEventQueuePtr   m_QueuePtr;
    LST_EVENT        m_Events;
    CSyncLock        m_EventsLock;
};

///////////////////////////////////////////////////////////////////
// CTSignal
template <typename TFUNC>
class CTSignal;
template <typename TRET, typename... TARGS>
class CTSignal<TRET(TARGS...)> : public MObject
{
public:
    typedef CTSlotObject<TRET, TARGS...>   TSlot;
    typedef CTRefCountPtr<TSlot>           CSlotPtr;
    typedef CTList<CSlotPtr>               LST_SLOT;
public:
    // bRegular = true : slots instances within the life cycle are fixed
    CTSignal(bool bRegular = true);
    virtual ~CTSignal(void);

    PINDEX Connect(CSlotPtr& SlotPtr);
    bool   Disconnect(PINDEX index = nullptr);

    TRET operator()(TARGS... Args);
private:
    TRET InvokeRegular(TARGS... Args);
    TRET InvokeAssign(TARGS... Args);
#if __MODERN_CXX_LANG >= 201703L
#else
    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_constructible<TRETURN>::value, TRETURN>::type
    Functor(LST_SLOT& SlotList, TARGUMENTS... Args);

    template <typename TRETURN, typename... TARGUMENTS>
    static typename std::enable_if<std::is_void<TRETURN>::value>::type
    Functor(LST_SLOT& SlotList, TARGUMENTS... Args, bool bVoid = true);
#endif
private:
    bool        m_bRegular;
    LST_SLOT    m_Slots;
    CSyncLock   m_SlotsLock;
};

///////////////////////////////////////////////////////////////////
// CObjectTraits
class CObjectTraits
{
public:
    // link a new object instance, pszName or uuid should be unique,
    // if the Name is alredy exist, return nullptr
    template <typename TOBJECT>
    static bool Link(PCXStr pszName, CTRefCountPtr<TOBJECT>& ObjectPtr);

    template <typename TOBJECT>
    static bool Link(const CString& strName, CTRefCountPtr<TOBJECT>& ObjectPtr);

    template <typename TOBJECT>
    static bool Link(const CUUID& uuid, CTRefCountPtr<TOBJECT>& ObjectPtr);

    // link a new object instance by rtti create
    template <typename TOBJECT>
    static bool LinkRTTI(PCXStr pszName, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr);

    template <typename TOBJECT>
    static bool LinkRTTI(const CString& strName, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr);

    template <typename TOBJECT>
    static bool LinkRTTI(const CUUID& uuid, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr);

    // unlink object instance and release
    template <typename TOBJECT>
    static void Unlink(CTRefCountPtr<TOBJECT>& ObjectPtr);

    // link a new object instance by rtti create from module, after object instance is released, use Unload to unload module
    template <typename TOBJECT>
    static bool Load(PCXStr pszName, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr, size_t stFlag = CObject::FLAG_NONE);

    template <typename TOBJECT>
    static bool Load(const CString& strName, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr, size_t stFlag = CObject::FLAG_NONE);

    template <typename TOBJECT>
    static bool Load(const CUUID& uuid, PCXStr pszModule, CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszClass = nullptr, size_t stFlag = CObject::FLAG_NONE);

    template <typename TOBJECT>
    static void Unload(PCXStr pszClass = nullptr, PCXStr pszName = nullptr);

    template <typename TOBJECT>
    static void UnloadUUID(const CUUID& uuid, PCXStr pszClass = nullptr);

    // find
    template <typename TOBJECT>
    static bool Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, PCXStr pszClass);

    template <typename TOBJECT>
    static bool Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, const CString& strClass);

    template <typename TOBJECT>
    static bool Find(CTArray<CTRefCountPtr<TOBJECT> >& ObjectPtrs, bool bExactClass = false);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszName, bool bExactClass = false);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CString& strName, bool bExactClass = false);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CUUID& uuid, bool bExactClass = false);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, PCXStr pszName, PCXStr pszClass);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CString& strName, PCXStr pszClass);

    template <typename TOBJECT>
    static bool Find(CTRefCountPtr<TOBJECT>& ObjectPtr, const CUUID& uuid, PCXStr pszClass);

    // signal-slot
    template <typename TOBJECT>
    static PINDEX Connect(CSignalEvent& Signal, const TOBJECT& ObjectRef, size_t stType = VART_EVENTQUEUE); // VART_EVENTTICK or (VART_EVENTQUEUE|VART_EVENTTICK)

    static PINDEX Connect(CSignalEvent& Signal, PCXStr pszClass, PCXStr pszName, size_t stType = VART_EVENTQUEUE); // VART_EVENTTICK or (VART_EVENTQUEUE|VART_EVENTTICK)


    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, TRET (TOBJECT::* pFunc)(TARGS...), size_t stType = VART_NONE);
    // TOBJECT - base class(rtti maybe not decl & def), pszClass - dervie class name, pFunc - virtual function
    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...), size_t stType = VART_NONE);


    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, TRET (TOBJECT::* pFunc)(TARGS...) const, size_t stType = VART_NONE);
    // TOBJECT - base class(rtti maybe not decl & def), pszClass - dervie class name, pFunc - virtual function
    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...) const, size_t stType = VART_NONE);


    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, TRET (* pFunc)(TARGS...), size_t stType = VART_NONE);

    template <typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, TRET (* pFunc)(TARGS...), size_t stType = VART_NONE);


    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType = VART_RTTITYPE); // VART_RTTITYPE[|VART_CONST]

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX Connect(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType = VART_RTTITYPE); // VART_CONST or VART_STATIC[|VART_RTTITYPE]


    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectFunc(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectCFunc(CTSignal<TRET(TARGS...)>& Signal, const TOBJECT& ObjectRef, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectSFunc(CTSignal<TRET(TARGS...)>& Signal, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    
    // TOBJECT - base class(ensure rtti decl & def), pszClass - dervie class name, pszFunc - virtual function name reflect by TOBJECT
    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
    // TOBJECT - base class(ensure rtti decl & def), pszClass - dervie class name, pszFunc - virtual function name reflect by TOBJECT
    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectCFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, PCXStr pszName, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static PINDEX ConnectSFunc(CTSignal<TRET(TARGS...)>& Signal, PCXStr pszClass, CPCXStr pszFunc, size_t stType = VART_RTTITYPE);
};

///////////////////////////////////////////////////////////////////
#include "object.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __OBJECT_H__
