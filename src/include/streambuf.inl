// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __STREAM_BUF_INL__
#define __STREAM_BUF_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CBufStreamBase
INLINE CBufStreamBase::CBufStreamBase(Int nMode)
: CStream(nMode|STREAMM_BUFFER)
{
}

INLINE CBufStreamBase::~CBufStreamBase(void)
{
}

INLINE CBufStreamBase::CBufStreamBase(const CBufStreamBase& aSrc)
: CStream(aSrc)
{
}

INLINE CBufStreamBase& CBufStreamBase::operator=(const CBufStreamBase& aSrc)
{
    if (this != &aSrc)
    {
        CStream::operator=(aSrc);
    }
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CTBufStreamFix
template <size_t stFix>
INLINE CTBufStreamFix<stFix>::CTBufStreamFix(Int nMode)
: CBufStreamBase(nMode|STREAMM_BUFFER)
, m_stPos(0)
{
}

template <size_t stFix>
INLINE CTBufStreamFix<stFix>::~CTBufStreamFix(void)
{
}

template <size_t stFix>
INLINE size_t CTBufStreamFix<stFix>::Tell(void) const
{
    return (m_stPos);
}

template <size_t stFix>
INLINE size_t CTBufStreamFix<stFix>::Size(void) const
{
    return (stFix);
}

template <size_t stFix>
INLINE size_t CTBufStreamFix<stFix>::Rest(void) const
{
    return (stFix - m_stPos);
}

template <size_t stFix>
INLINE size_t CTBufStreamFix<stFix>::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= stFix);
    assert(eFrom < SEEKO_BOUND);
    if (stFix > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, (size_t)stFix);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= ((size_t)stFix - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= (size_t)stFix)
                    {
                        m_stPos = (size_t)stFix - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

template <size_t stFix>
INLINE PByte CTBufStreamFix<stFix>::GetBuf(size_t stPos) 
{
    if (stPos < stFix)
    {
        return (PByte)(m_pBuf + stPos);
    }
    return nullptr;
}

template <size_t stFix>
INLINE const PByte CTBufStreamFix<stFix>::GetBuf(size_t stPos) const
{
    if (stPos < stFix)
    {
        return (const PByte)(m_pBuf + stPos);
    }
    return nullptr;
}

template <size_t stFix>
INLINE PByte CTBufStreamFix<stFix>::GetBufPos(void) 
{
    return (PByte)(m_pBuf + m_stPos);
}

template <size_t stFix>
INLINE const PByte CTBufStreamFix<stFix>::GetBufPos(void) const
{
    return (PByte)(m_pBuf + m_stPos);
}

///////////////////////////////////////////////////////////////////
// CTBufReadStreamFix
template <size_t stFix>
INLINE CTBufReadStreamFix<stFix>::CTBufReadStreamFix(void)
: CTBufStreamFix<stFix>(CStream::STREAMM_READ)
{
}

template <size_t stFix>
INLINE CTBufReadStreamFix<stFix>::~CTBufReadStreamFix(void)
{
}

template <size_t stFix>
INLINE size_t CTBufReadStreamFix<stFix>::Read(void* pV, size_t stLenBytes)
{
    assert(TBuf::m_stPos <= stFix);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    if (TBuf::IsEnd())
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, ((size_t)stFix - TBuf::m_stPos));
    MM_SAFE::Cpy(pV, (size_t)stLenBytes, (TBuf::m_pBuf + TBuf::m_stPos), (size_t)stLenBytes);
    CTBufStreamFix<stFix>::m_stPos += stLenBytes;
    return stLenBytes;
}

///////////////////////////////////////////////////////////////////
// CTBufWriteStreamFix
template <size_t stFix>
INLINE CTBufWriteStreamFix<stFix>::CTBufWriteStreamFix(void)
: CTBufStreamFix<stFix>(CStream::STREAMM_WRITE)
{
}

template <size_t stFix>
INLINE CTBufWriteStreamFix<stFix>::~CTBufWriteStreamFix(void)
{
}

template <size_t stFix>
INLINE size_t CTBufWriteStreamFix<stFix>::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(TBuf::m_stPos <= stFix);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, ((size_t)stFix - TBuf::m_stPos));
    MM_SAFE::Cpy((TBuf::m_pBuf + TBuf::m_stPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
    CTBufStreamFix<stFix>::m_stPos += stLenBytes;
    return stLenBytes;
}

///////////////////////////////////////////////////////////////////
// CBufStream
INLINE CBufStream::CBufStream(Int nMode, size_t stSize, PByte pBuf, bool bManage)
: CBufStreamBase(nMode)
, m_bManage(false)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
    Create(stSize, pBuf, bManage);
}

INLINE CBufStream::~CBufStream(void)
{
    Close();
}

INLINE size_t CBufStream::Tell(void) const
{
    return (m_stPos);
}

INLINE size_t CBufStream::Size(void) const
{
    return (m_stSize);
}

INLINE size_t CBufStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CBufStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE void CBufStream::Close(void)
{
    if (m_bManage && (m_pBuf != nullptr))
    {
        FREE( m_pBuf );
    }
    m_pBuf    = nullptr;
    m_stPos   = 0;
    m_stSize  = 0;
    m_bManage = false;
    CBufStreamBase::Close();
}

INLINE PByte CBufStream::GetBuf(size_t stPos)
{
    if (stPos < m_stSize)
    {
        return (PByte)(m_pBuf + stPos);
    }
    return nullptr;
}

INLINE const PByte CBufStream::GetBuf(size_t stPos) const
{
    if (stPos < m_stSize)
    {
        return (const PByte)(m_pBuf + stPos);
    }
    return nullptr;
}

INLINE PByte CBufStream::GetBufPos(void)
{
    return (PByte)(m_pBuf + m_stPos);
}

INLINE const PByte CBufStream::GetBufPos(void) const
{
    return (const PByte)(m_pBuf + m_stPos);
}

INLINE bool CBufStream::Attach(const CBufStream& BufStream)
{
    Close();
    if (BufStream.GetBuf() != nullptr)
    {
        m_bManage = true;
        m_stSize  = BufStream.Size();
        m_stPos   = 0;
        m_pBuf    = (PByte)ALLOC(m_stSize);
        if (m_pBuf != nullptr)
        {
            MM_SAFE::Cpy(m_pBuf, m_stSize, BufStream.GetBuf(), m_stSize);
            return true;
        }
        return false;
    }
    return true;
}

INLINE bool CBufStream::Attach(CBufStream& BufStream, bool bCopy)
{
    Close();
    if (bCopy == false)
    {
        m_bManage = BufStream.m_bManage;
        m_stSize  = BufStream.m_stSize;
        m_stPos   = 0;
        m_pBuf    = BufStream.m_pBuf;

        BufStream.Detach();
        return true;
    }
    else if (BufStream.Size() > 0)
    {
        m_bManage = true;
        m_stSize  = BufStream.Size();
        m_stPos   = 0;
        m_pBuf    = (PByte)ALLOC(m_stSize);
        if (m_pBuf != nullptr)
        {
            MM_SAFE::Cpy(m_pBuf, m_stSize, BufStream.GetBuf(), m_stSize);
            return true;
        }
    }
    return false;
}

INLINE bool CBufStream::Attach(size_t stSize, PByte pBuf, bool bManage)
{
    Close();
    return Create(stSize, pBuf, bManage);
}

INLINE void CBufStream::Detach(void)
{
    m_bManage = false;
    Close();
}

INLINE bool CBufStream::Create(size_t stSize, PByte pBuf, bool bManage)
{
    if (stSize > 0)
    {
        if (pBuf == nullptr)
        {
            m_bManage = true;
            m_stSize  = stSize;
            m_stPos   = 0;
            m_pBuf    = (PByte)ALLOC(stSize);
            return (m_pBuf != nullptr);
        }
        m_bManage = bManage;
        m_stSize  = stSize;
        m_stPos   = 0;
        m_pBuf    = pBuf;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CBufReadStream
INLINE CBufReadStream::CBufReadStream(size_t stSize, PByte pBuf, bool bManage)
: CBufStream(STREAMM_READ, stSize, pBuf, bManage)
{
}

INLINE CBufReadStream::~CBufReadStream(void)
{
}

INLINE size_t CBufReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(m_stSize >= m_stPos);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    if (IsEnd())
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy(pV, (size_t)stLenBytes, (m_pBuf + m_stPos), (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE bool CBufReadStream::Refer(CStream& aSrc)
{
    if (aSrc.GetMode() & STREAMM_BUFFER)
    {
        Close();

        CBufStreamBase* pBufStream = static_cast<CBufStreamBase*>(&aSrc);
        if (aSrc.IsRead())
        {
            return Attach(aSrc.Size(), pBufStream->GetBuf());
        }
        else
        {
            assert(aSrc.IsWrite());
            return Attach(aSrc.Tell(), pBufStream->GetBuf());
        }
    }
    return false;
}


///////////////////////////////////////////////////////////////////
// CBufWriteStream
INLINE CBufWriteStream::CBufWriteStream(size_t stSize, PByte pBuf, bool bManage)
: CBufStream(STREAMM_WRITE, stSize, pBuf, bManage)
{
}

INLINE CBufWriteStream::~CBufWriteStream(void)
{
}

INLINE size_t CBufWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(m_stSize >= m_stPos);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        return 0;
    }
    if (m_stSize == 0)
    {
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy((m_pBuf + m_stPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

#endif // __STREAM_BUF_INL__
