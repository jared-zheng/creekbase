// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __KEY_VALUE_H__
#define __KEY_VALUE_H__

#pragma once

#include "container.h"
#include "vartype.h"
#include "sync.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CKeyValue
class CKeyValue : public MObject
{
private:
    typedef CTMap<CString, CVarType>         MAP_KEYVALUE, *PMAP_KEYVALUE;
    typedef CTMap<CString, CVarType>::PAIR   PAIR_KEYVALUE;
public:
    CKeyValue(void);
    ~CKeyValue(void);

    Int    GetCount(void) const;
    size_t GetSize(PCXStr pszKey);
    VAR_TYPE GetType(PCXStr pszKey);

    template <typename T>
    T&     AnyCast(PCXStr pszKey);
    template <typename T>
    T*     TryCast(PCXStr pszKey);

    template <typename T>
    T*     GetValue(PCXStr pszKey);

#ifdef __RUNTIME_CHARSET_WCHAR__
    bool   GetValue(PCXStr pszKey, CCString& strValue, bool bAttach = false);
#else
    bool   GetValue(PCXStr pszKey, CWString& strValue, bool bAttach = false);
#endif
    bool   GetValue(PCXStr pszKey, CString& strValue, bool bAttach = false);

    bool   GetValue(PCXStr pszKey, CBufStream& bsValue, bool bAttach = false);

    bool   GetValue(PCXStr pszKey, Short& sValue);
    bool   GetValue(PCXStr pszKey, UShort& usValue);
    bool   GetValue(PCXStr pszKey, Int& nValue);
    bool   GetValue(PCXStr pszKey, UInt& uValue);
    bool   GetValue(PCXStr pszKey, Long& lValue);
    bool   GetValue(PCXStr pszKey, ULong& ulValue);
    bool   GetValue(PCXStr pszKey, LLong& llValue);
    bool   GetValue(PCXStr pszKey, ULLong& ullValue);
    bool   GetValue(PCXStr pszKey, Double& dValue);
    bool   GetValue(PCXStr pszKey, bool& bValue);

    template <typename T>
    bool   SetAny(PCXStr pszKey);
    template <typename T>
    bool   SetAny(PCXStr pszKey, const T& tValue);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    template <typename T>
    bool   SetAny(PCXStr pszKey, T&& tValue);
#endif

    template <typename T>
    bool   SetValue(PCXStr pszKey, T* pValue);

#ifdef __RUNTIME_CHARSET_WCHAR__
    bool   SetValue(PCXStr pszKey, const CCString& strValue);
    bool   SetValue(PCXStr pszKey, CCString& strValue, bool bAttach);
    bool   SetValue(PCXStr pszKey, PCStr pszValue, size_t stSize = 0);
#else
    bool   SetValue(PCXStr pszKey, const CWString& strValue);
    bool   SetValue(PCXStr pszKey, CWString& strValue, bool bAttach);
    bool   SetValue(PCXStr pszKey, PCWStr pszValue, size_t stSize = 0);
#endif
    bool   SetValue(PCXStr pszKey, const CString& strValue);
    bool   SetValue(PCXStr pszKey, CString& strValue, bool bAttach);
    bool   SetValue(PCXStr pszKey, PCXStr pszValue, size_t stSize = 0);

    bool   SetValue(PCXStr pszKey, const CBufStream& bsValue);
    bool   SetValue(PCXStr pszKey, CBufStream& bsValue, bool bAttach = false);
    bool   SetValue(PCXStr pszKey, PByte pbValue, size_t stSize, bool bAttach = false);

    bool   SetValue(PCXStr pszKey, Short sValue);
    bool   SetValue(PCXStr pszKey, UShort usValue);
    bool   SetValue(PCXStr pszKey, Int nValue);
    bool   SetValue(PCXStr pszKey, UInt uValue);
    bool   SetValue(PCXStr pszKey, Long lValue);
    bool   SetValue(PCXStr pszKey, ULong ulValue);
    bool   SetValue(PCXStr pszKey, LLong llValue);
    bool   SetValue(PCXStr pszKey, ULLong ullValue);
    bool   SetValue(PCXStr pszKey, Double dValue);
    bool   SetValue(PCXStr pszKey, bool bValue);

    bool   Remove(PCXStr pszKey);

    bool   Find(PCXStr pszKey);
private:
    CKeyValue(const CKeyValue&);
    CKeyValue& operator=(const CKeyValue&);

    PAIR_KEYVALUE* FindKV(PCXStr pszKey, bool bAdd = false);
private:
    MAP_KEYVALUE    m_KeyValue;
    CSyncLock       m_KVLock;
};

///////////////////////////////////////////////////////////////////
#include "keyvalue.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __KEY_VALUE_H__
