// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CORE_H__
#define __CORE_H__

#pragma once

///////////////////////////////////////////////////////////////////
#if defined(CORE_EXPORT)
    #define COREAPI                   C_EXPORT
    #define CORECLASS                 CXX_EXPORT
#elif defined(__RUNTIME_STATIC__)
    #define COREAPI
    #define CORECLASS
#else   // CORE_EXPORT
    #define COREAPI                   C_IMPORT
    #define CORECLASS                 CXX_IMPORT
#endif  // CORE_EXPORT

#define CORE_FUNC_INIT                ("CoreInit")
#define CORE_FUNC_EXIT                ("CoreExit") // C-style api

// init thread, event & object manager
COREAPI bool                          CoreInit(void);
COREAPI void                          CoreExit(void);

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#
//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CORE_H__
