// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TRAITS_H__
#define __TRAITS_H__

#pragma once

#include "hash.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTElementTraitsBase
template <typename T>
class CTElementTraitsBase
{
public:
    typedef const T& INARGTYPE;
    typedef T&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    typedef T&& RVARGTYPE;
#endif
public:
    static void CopyElements(T* pDst, const T* pSrc, size_t stElements);
    static void RelocateElements(T* pDst, T* pSrc, size_t stElements);
};

///////////////////////////////////////////////////////////////////
// CTElementTraitsCompare
template <typename T>
class CTElementTraitsCompare
{
public:
    static bool CompareElements(const T& t1, const T& t2);
    static Int  CompareElementsOrdered(const T& t1, const T& t2);
};

///////////////////////////////////////////////////////////////////
// CTElementTraitsHash
template <typename T>
class CTElementTraitsHash
{
public:
    static size_t HashElements(const T& t);
};

///////////////////////////////////////////////////////////////////
// CTDefaultElementTraits
template <typename T>
class CTDefaultElementTraits :
    public CTElementTraitsBase<T>,
    public CTElementTraitsCompare<T>,
    public CTElementTraitsHash<T>
{
};

///////////////////////////////////////////////////////////////////
// CTElementTraits
template <typename T>
class CTElementTraits : public CTDefaultElementTraits<T>
{
};

///////////////////////////////////////////////////////////////////
// CTPrimitiveElementTraits
template <typename T>
class CTPrimitiveElementTraits : public CTDefaultElementTraits<T>
{
};

///////////////////////////////////////////////////////////////////
// Primitive Element Traits Define
#define _DECLARE_PRIMITIVE_TRAITS( T )           \
template<>                                       \
class CTElementTraits<T> :                       \
    public CTPrimitiveElementTraits<T>           \
    {                                            \
    };

_DECLARE_PRIMITIVE_TRAITS( Char )
_DECLARE_PRIMITIVE_TRAITS( Short )
_DECLARE_PRIMITIVE_TRAITS( Int )
_DECLARE_PRIMITIVE_TRAITS( Long )
_DECLARE_PRIMITIVE_TRAITS( LLong )

_DECLARE_PRIMITIVE_TRAITS( UChar )
_DECLARE_PRIMITIVE_TRAITS( UShort )
_DECLARE_PRIMITIVE_TRAITS( UInt )
_DECLARE_PRIMITIVE_TRAITS( ULong )
_DECLARE_PRIMITIVE_TRAITS( ULLong )

_DECLARE_PRIMITIVE_TRAITS( Float )
_DECLARE_PRIMITIVE_TRAITS( Double )

_DECLARE_PRIMITIVE_TRAITS( WChar )

_DECLARE_PRIMITIVE_TRAITS( bool )
_DECLARE_PRIMITIVE_TRAITS( void* )

///////////////////////////////////////////////////////////////////
// CTXCharTraitsBase
template <typename T>
class CTXCharTraitsBase : public MObject
{
public:
    enum CONVERT_CONST
    {
        CONVERTC_UTF8_PREFIX      = 6,
        CONVERTC_UTF8_MASKS       = 6,
        CONVERTC_UTF32_CODEUP     = 6,

        CONVERTC_UTF8_BIT_OFF     = 6,
        CONVERTC_UTF8_BIT_LEAD    = 1,
        CONVERTC_UTF8_BIT_MASK    = 0x3F,
        CONVERTC_UTF8_BIT_SINGLE  = 0x80,
        CONVERTC_UTF8_BIT_MAX     = 0xBF,

        CONVERTC_UTF8_LEAD_MIN    = 0xC0,
        CONVERTC_UTF8_LEAD_MAX    = 0xFD,
        CONVERTC_UTF8_LEAD_MASK   = 0xE0,

        CONVERTC_UTF16_BIT_OFF    = 10,
        CONVERTC_UTF16_BIT_LEAD   = 1,
        CONVERTC_UTF16_BIT_DATA   = 0x40,
        CONVERTC_UTF16_BIT_MASK   = 0x03FF,
        CONVERTC_UTF16_BIT_SINGLE = 0xFFFF,
        CONVERTC_UTF16_BIT_MAX    = 0xEFFFF,

        CONVERTC_UTF16_LEAD_MIN   = 0xD800,
        CONVERTC_UTF16_LEAD_MAX   = 0xDC00,
        CONVERTC_UTF16_LEAD_MASK  = 0xDFFF,
    };
    static const Byte UTF8_PREFIX[];
    static const Byte UTF8_MASKS[];
    static const Byte UTF8_LEN[];
    static const UInt UTF32_CODEUP[];
public:
    static Int CheckBOM(PUChar& psz, bool bOffset = true);

    static Int Utf32ToUtf8(UInt u32, PUChar psz8, Int nSize8);
    static Int Utf8ToUtf32(const PUChar psz8, UInt& u32);
    static Int Utf32ToUtf16(UInt u32, PUShort psz16, Int nSize16);
    static Int Utf16ToUtf32(const PUShort psz16, UInt& u32);

    static Int Utf32ToUtf8(const PUInt psz32, Int nSize32, PUChar psz8, Int nSize8);
    static Int Utf8ToUtf32(const PUChar psz8, Int nSize8, PUInt psz32, Int nSize32);
    static Int Utf32ToUtf16(const PUInt   psz32, Int nSize32, PUShort psz16, Int nSize16);
    static Int Utf16ToUtf32(const PUShort psz16, Int nSize16, PUInt   psz32, Int nSize32);

    static Int Convert(PCStr  pszC, Int nSizeC, PWStr pszW, Int nSizeW, CodePage cpPage = CP_DEFAULT);
    static Int Convert(PCWStr pszW, Int nSizeW, PStr  pszC, Int nSizeC, CodePage cpPage = CP_DEFAULT);
};

///////////////////////////////////////////////////////////////////
// CChar
class CChar : public CTXCharTraitsBase<Char>
{
public:
    typedef Char     TChar;
    typedef PStr     PTStr;
    typedef PCStr    PCTStr;
    typedef CPCStr   CPCTStr;
public:
    // Character Classification
    static bool   IsAlnum(TChar ch);
    static bool   IsAlpha(TChar ch);
    static bool   IsPrint(TChar ch);
    static bool   IsGraph(TChar ch);
    static bool   IsDigit(TChar ch);
    static bool   IsXDigit(TChar ch);
    static bool   IsSpace(TChar ch);
    static bool   IsLower(TChar ch);
    static bool   IsUpper(TChar ch);

    static PTStr  Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext);
    static PTStr  Rev(PTStr pszStr);

    static PCTStr Chr(PCTStr pszStr, TChar ch);
    static PCTStr RevChr(PCTStr pszStr, TChar ch);
    static PCTStr Str(PCTStr pszStr, PCTStr pszMatch);
    static PCTStr OneOf(PCTStr pszStr, PCTStr pszMatch);

    static size_t OneIndex(PCTStr pszStr, PCTStr pszMatch);
    static size_t NotIndex(PCTStr pszStr, PCTStr pszMatch);

    static size_t Length(PCTStr pszStr, size_t stMax = (size_t)LMT_MAX);

    static Int    Cmp(PCTStr pszA, PCTStr pszB);
    static Int    Cmpi(PCTStr pszA, PCTStr pszB);
    static Int    Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize);
    // compare strings using locale-specific information
    static Int    Coll(PCTStr pszA, PCTStr pszB);
    static Int    Colli(PCTStr pszA, PCTStr pszB);
    static Int    Colln(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Collin(PCTStr pszA, PCTStr pszB, size_t stSize);

    static Int    FormatLength(PCTStr pszFormat, ...);
    static Int    FormatLengthV(PCTStr pszFormat, va_list args);
    static Int    Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...);
    static Int    FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args);

    static bool   Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount);
    static bool   Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);
    static bool   Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);

    static bool   ToLower(PTStr pszStr, size_t stSize);
    static bool   ToUpper(PTStr pszStr, size_t stSize);

    static TChar  ToLower(TChar ch);
    static TChar  ToUpper(TChar ch);

    static Int    ToInt(PCTStr pszStr);
    static Long   ToLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULong  ToULong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static LLong  ToLLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULLong ToULLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static Double ToDouble(PCTStr pszStr, PTStr* ppszEnd = nullptr);

    static bool   ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DIGITS);
};

///////////////////////////////////////////////////////////////////
// CMChar : Multi-Byte Chactacter
class CMChar : public CTXCharTraitsBase<Char>
{
public:
    typedef Char     TChar;
    typedef PStr     PTStr;
    typedef PCStr    PCTStr;
    typedef CPCStr   CPCTStr;
public:
    // Character Classification
    static bool   IsAlnum(TChar ch);
    static bool   IsAlpha(TChar ch);
    static bool   IsPrint(TChar ch);
    static bool   IsGraph(TChar ch);
    static bool   IsDigit(TChar ch);
    static bool   IsXDigit(TChar ch);
    static bool   IsSpace(TChar ch);
    static bool   IsLower(TChar ch);
    static bool   IsUpper(TChar ch);

    static PTStr  Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext);
    static PTStr  Rev(PTStr pszStr);

    static PCTStr Chr(PCTStr pszStr, TChar ch);
    static PCTStr RevChr(PCTStr pszStr, TChar ch);
    static PCTStr Str(PCTStr pszStr, PCTStr pszMatch);
    static PCTStr OneOf(PCTStr pszStr, PCTStr pszMatch);

    static size_t OneIndex(PCTStr pszStr, PCTStr pszMatch);
    static size_t NotIndex(PCTStr pszStr, PCTStr pszMatch);

    static size_t Length(PCTStr pszStr, size_t stMax = (size_t)LMT_MAX);

    static Int    Cmp(PCTStr pszA, PCTStr pszB);
    static Int    Cmpi(PCTStr pszA, PCTStr pszB);
    static Int    Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize);
    // compare strings using locale-specific information
    static Int    Coll(PCTStr pszA, PCTStr pszB);
    static Int    Colli(PCTStr pszA, PCTStr pszB);
    static Int    Colln(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Collin(PCTStr pszA, PCTStr pszB, size_t stSize);

    static Int    FormatLength(PCTStr pszFormat, ...);
    static Int    FormatLengthV(PCTStr pszFormat, va_list args);
    static Int    Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...);
    static Int    FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args);

    static bool   Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount);
    static bool   Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);
    static bool   Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);

    static bool   ToLower(PTStr pszStr, size_t stSize);
    static bool   ToUpper(PTStr pszStr, size_t stSize);

    static TChar  ToLower(TChar ch);
    static TChar  ToUpper(TChar ch);

    static Int    ToInt(PCTStr pszStr);
    static Long   ToLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULong  ToULong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static LLong  ToLLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULLong ToULLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static Double ToDouble(PCTStr pszStr, PTStr* ppszEnd = nullptr);

    static bool   ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DIGITS);
};

///////////////////////////////////////////////////////////////////
// CWChar
class CWChar : public CTXCharTraitsBase<WChar>
{
public:
    typedef WChar     TChar;
    typedef PWStr     PTStr;
    typedef PCWStr    PCTStr;
    typedef CPCWStr   CPCTStr;
public:
    // Character Classification
    static bool   IsAlnum(TChar ch);
    static bool   IsAlpha(TChar ch);
    static bool   IsPrint(TChar ch);
    static bool   IsGraph(TChar ch);
    static bool   IsDigit(TChar ch);
    static bool   IsXDigit(TChar ch);
    static bool   IsSpace(TChar ch);
    static bool   IsLower(TChar ch);
    static bool   IsUpper(TChar ch);

    static PTStr  Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext);
    static PTStr  Rev(PTStr pszStr);

    static PCTStr Chr(PCTStr pszStr, TChar ch);
    static PCTStr RevChr(PCTStr pszStr, TChar ch);
    static PCTStr Str(PCTStr pszStr, PCTStr pszMatch);
    static PCTStr OneOf(PCTStr pszStr, PCTStr pszMatch);

    static size_t OneIndex(PCTStr pszStr, PCTStr pszMatch);
    static size_t NotIndex(PCTStr pszStr, PCTStr pszMatch);

    static size_t Length(PCTStr pszStr, size_t stMax = (size_t)LMT_MAX);

    static Int    Cmp(PCTStr pszA, PCTStr pszB);
    static Int    Cmpi(PCTStr pszA, PCTStr pszB);
    static Int    Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize);
    // compare strings using locale-specific information
    static Int    Coll(PCTStr pszA, PCTStr pszB);
    static Int    Colli(PCTStr pszA, PCTStr pszB);
    static Int    Colln(PCTStr pszA, PCTStr pszB, size_t stSize);
    static Int    Collin(PCTStr pszA, PCTStr pszB, size_t stSize);

    static Int    FormatLength(PCTStr pszFormat, ...);
    static Int    FormatLengthV(PCTStr pszFormat, va_list args);
    static Int    Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...);
    static Int    FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args);

    static bool   Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount);
    static bool   Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);
    static bool   Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc = _TRUNCATE);

    static bool   ToLower(PTStr pszStr, size_t stSize);
    static bool   ToUpper(PTStr pszStr, size_t stSize);

    static TChar  ToLower(TChar ch);
    static TChar  ToUpper(TChar ch);

    static Int    ToInt(PCTStr pszStr);
    static Long   ToLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULong  ToULong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static LLong  ToLLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static ULLong ToULLong(PCTStr pszStr, PTStr* ppszEnd = nullptr, Int nRadix = RADIXT_DEC);
    static Double ToDouble(PCTStr pszStr, PTStr* ppszEnd = nullptr);

    static bool   ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DEC);
    static bool   ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix = RADIXT_DIGITS);
};

///////////////////////////////////////////////////////////////////
// CXChar

///////////////////////////////////////////////////////////////////
// CTSize
template <typename T>
class CTSize : public MObject
{
public:
    CTSize(T InX = 0, T InY = 0);
    ~CTSize(void);

    CTSize(const CTSize& aSrc);
    CTSize& operator=(const CTSize& aSrc);

    CTSize& operator+=(const CTSize& aSrc);
    CTSize& operator-=(const CTSize& aSrc);

    CTSize  operator+(const CTSize& aSrc);
    CTSize  operator-(const CTSize& aSrc);
    CTSize  operator-(void);

    bool     operator==(const CTSize& aSrc) const;
    bool     operator!=(const CTSize& aSrc) const;

    T        X(void) const;
    void     X(T InX);

    T        Y(void) const;
    void     Y(T InY);

    void     SetValue(T InX = 0, T InY = 0);
    void     AddValue(T InX = 0, T InY = 0);
    void     SubValue(T InX = 0, T InY = 0);
public:
    T    m_tX;
    T    m_tY;
};

///////////////////////////////////////////////////////////////////
// CTRect
template <typename T>
class CTRect : public MObject
{
public:
    CTRect(T InLeft = 0, T InTop = 0, T InRight = 0, T InBottom = 0);
    ~CTRect(void);

    CTRect(const CTRect& aSrc);
    CTRect& operator=(const CTRect& aSrc);

    CTRect& operator+=(const CTRect& aSrc);
    CTRect& operator-=(const CTRect& aSrc);
    CTRect& operator+=(const CTSize<T>& tSize);
    CTRect& operator-=(const CTSize<T>& tSize);

    CTRect  operator+(const CTRect& aSrc);
    CTRect  operator-(const CTRect& aSrc);
    CTRect  operator+(const CTSize<T>& tSize);
    CTRect  operator-(const CTSize<T>& tSize);
    CTRect  operator-(void);

    bool    operator==(const CTRect& aSrc) const;
    bool    operator!=(const CTRect& aSrc) const;

    T       Width(void) const;
    T       Height(void) const;

    CTSize<T> Size(void) const;
    CTSize<T> LeftTop(void) const;
    CTSize<T> RightBottom(void) const;
    CTSize<T> Center(void) const;

    bool    IsEmpty(void) const; // has no area
    bool    IsNull(void) const;  // at(0,0) and has no area
    bool    IsEqual(const CTRect& tRect) const;
    bool    InRect(const CTSize<T>& tSize) const;

    T       Left(void) const;
    void    Left(T InLeft);

    T       Top(void) const;
    void    Top(T InTop);

    T       Right(void) const;
    void    Right(T InRight);

    T       Bottom(void) const;
    void    Bottom(T InBottom);

    void    SetValue(T InLeft = 0, T InTop = 0, T InRight = 0, T InBottom = 0);
    void    AddValue(T InLeft = 0, T InTop = 0, T InRight = 0, T InBottom = 0);
    void    SubValue(T InLeft = 0, T InTop = 0, T InRight = 0, T InBottom = 0);

    void    InflateRect(T x, T y);
    void    OffseteRect(T x, T y);
    void    Normalize(void);

public:
    T    m_tLeft;
    T    m_tTop;
    T    m_tRight;
    T    m_tBottom;
};

///////////////////////////////////////////////////////////////////
#include "traits.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targettraits.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targettraits.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __TRAITS_H__
