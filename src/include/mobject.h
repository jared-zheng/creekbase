// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MOBJECT_H__
#define __MOBJECT_H__

#pragma once

#include "core.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// managed memory
#pragma push_macro("new")
#undef new
#pragma push_macro("delete")
#undef delete
#ifdef __RUNTIME_DEBUG__
    #define MNEW                      new(TF_FILE, TF_FUNC, ST_LINE)
    #define MDELETE                   delete
    #define ALLOC( size )             MObject::MAlloc(size, TF_FILE, TF_FUNC, ST_LINE)
    #define ALLOCEX( size, type )     MObject::MAlloc(size, TF_FILE, TF_FUNC, ST_LINE, type) // page type special when size >= 32K
    #define REALLOC( pOld, newsize )  MObject::MReAlloc(pOld, newsize, TF_FILE, TF_FUNC, ST_LINE)
#else   // __RUNTIME_DEBUG__
    #define MNEW                      new
    #define MDELETE                   delete
    #define ALLOC( size )             MObject::MAlloc(size)
    #define ALLOCEX( size, type )     MObject::MAlloc(size, type) // page type special when size >= 32K
    #define REALLOC( pOld, newsize )  MObject::MReAlloc(pOld, newsize)
#endif  // __RUNTIME_DEBUG__
#define FREE( p )                     MObject::MFree(p, 0) // set size == 0 if not know it's size
#define DUMP( p )                     MObject::MDump(p)    // dump current memory manager alloc information
#define CHECK( p )                    MObject::MCheck(p)   // check the pointer's memory is not overwrite by other mobject
#pragma pop_macro("delete")
#pragma pop_macro("new")

///////////////////////////////////////////////////////////////////
// MM_DUMP
typedef struct tagMM_DUMP
{
public:
    tagMM_DUMP(void)
    : llUsedSize(0)
    , llMaxUsedSize(0)
    , llCacheSize(0)
    , llMaxCacheSize(0)
    , llUsedTick(0)
    , llAllocTick(0)
    , llReallocTick(0)
    , llFreeTick(0)
    {
    }

    ~tagMM_DUMP(void)
    {
    }
public:
    LLong   llUsedSize;     // MB
    LLong   llMaxUsedSize;  // MB
    LLong   llCacheSize;    // MB
    LLong   llMaxCacheSize; // MB
    LLong   llUsedTick;     // MS
    LLong   llAllocTick;    // MS
    LLong   llReallocTick;  // MS
    LLong   llFreeTick;     // MS
}MM_DUMP, *PMM_DUMP;

///////////////////////////////////////////////////////////////////
// MM_SAFE
class MM_SAFE
{
public:
    template <typename T>
    static void DELETE_PTR(T* pV);

    template <typename T>
    static void DELETE_PTRREF(T*& pV);

    template <typename X, typename T>
    static void DELETE_PTR(X* pV);

    template <typename X, typename T>
    static void DELETE_PTRREF(X*& pV);

    static Int   Cmp(const void* pDst, const void* pSrc, size_t stSize);
    static void* Set(void* pDst, Int nValue, size_t stSize);
    static void* Cpy(void* pDst, size_t stDst, const void* pSrc, size_t stSrc);
    static void* Mov(void* pDst, size_t stDst, const void* pSrc, size_t stSrc);
};

///////////////////////////////////////////////////////////////////
// MObject
// base class for allocate/free memory from managed memory pool
class CORECLASS MObject
{
public:
    enum MEM_CONST
    {
        MEM_CHUNK_OFFSET = 72, // max chunk offset -- MM_CHUNK_WITHBLOCK
    };
public:
#ifdef __RUNTIME_DEBUG__
    static void*  operator new(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine);
    static void*  operator new[](size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine);
    static void   operator delete(void*, PCXStr, PCXStr, size_t);
    static void   operator delete[](void*, PCXStr, PCXStr, size_t);
#else
    static void*  operator new(size_t stSize);
    static void*  operator new[](size_t stSize);
#endif
    static void   operator delete(void* p, size_t stSize);
    static void   operator delete[](void* p, size_t stSize);

#ifdef __RUNTIME_DEBUG__
    static void*  MAlloc(size_t stSize, PCXStr szFile, PCXStr szFunc, size_t stLine, size_t stType = 0);
    static void*  MReAlloc(void* pOld, size_t stNewSize, PCXStr szFile, PCXStr szFunc, size_t stLine);
#else
    static void*  MAlloc(size_t stSize, size_t stType = 0);
    static void*  MReAlloc(void* pOld, size_t stNewSize);
#endif
    static void   MFree(void* p, size_t stSize = 0);
    // dump or check special memory
    static void   MDump(MM_DUMP& Dump);
    static void   MDump(void* p);
    static bool   MCheck(void* p);
    // cache : boundary to align pagesize
    static PINDEX MCCreate(size_t stSize, size_t stGrow = 0, size_t stMaxSize = 0, size_t stType = 0);
    static bool   MCDestroy(PINDEX index);

    static size_t MCAlignSize(PINDEX index);  // cache align size
    static size_t MCUsedSize(PINDEX index);   // cache used size

    static PByte  MCAlloc(PINDEX index);
    static bool   MCFree(PINDEX index, PByte pData);
    static bool   MCCheck(PINDEX index, PByte pData);

    static PByte  MCDump(PINDEX index, PByte pData, size_t& stSize);
    static void   MCDump(PINDEX index);
};

// ///////////////////////////////////////////////////////////////////
// // CTMAlloc
// // std allocator
// //    std::vector<int, CTMAlloc<int> > vi;
// //    std::list<short, CTMAlloc<short> > ls;
// //    std::map<int, double, std::less<int>, CTMAlloc<std::pair<int, double> > > md;
// template <typename T>
// class CTMAlloc : public MObject
// {
// public:
//     typedef T                 value_type;
//     typedef value_type*       pointer;
//     typedef const value_type* const_pointer;
//     typedef value_type&       reference;
//     typedef const value_type& const_reference;
//     typedef size_t            size_type;
//     typedef ptrdiff_t         difference_type;
//
//     template <typename X>
//     struct rebind
//     {
//         typedef CTMAlloc<X> other;
//     };
// public:
//     CTMAlloc(void)
//     {
//     }
//
//     ~CTMAlloc(void)
//     {
//     }
//
//     CTMAlloc(const CTMAlloc&)
//     {
//     }
//
//     template <typename X>
//     CTMAlloc(const CTMAlloc<X>&)
//     {
//     }
//
//     pointer address(reference ref) const
//     {
//         return &ref;
//     }
//     const_pointer address(const_reference ref) const
//     {
//         return &ref;
//     }
//
//     pointer allocate(size_type stElements, const_pointer = nullptr)
//     {
//         void* p = ALLOC( stElements * sizeof(T) );
//         if (p == nullptr)
//         {
//             throw std::bad_alloc();
//         }
//         return static_cast<pointer>(p);
//     }
//
//     void deallocate(pointer p, size_type stElements)
//     {
//         MFree( p, stElements * sizeof(T) );
//     }
//
//     size_type max_size(void) const
//     {
//         return static_cast<size_type>(-1);
//     }
//
//     void construct(pointer p, const value_type& ref)
//     {
//         GNEW( p ) value_type(ref);
//     }
//
//     void destroy(pointer p)
//     {
//         p->~value_type();
//     }
// private:
//     CTMAlloc& operator=(const CTMAlloc&)
//     {
//         return (*this);
//     }
// };
//
// ///////////////////////////////////////////////////////////////////
// // CTMAlloc<void>
// template<>
// class CTMAlloc<void> : public MObject
// {
// public:
//     typedef void        value_type;
//     typedef void*       pointer;
//     typedef const void* const_pointer;
//
//     template <typename X>
//     struct rebind
//     {
//         typedef CTMAlloc<X> other;
//     };
// };
//
// template <typename T>
// INLINE bool operator==(const CTMAlloc<T>&, const CTMAlloc<T>&)
// {
//     return true;
// }
//
// template <typename T>
// INLINE bool operator!=(const CTMAlloc<T>&, const CTMAlloc<T>&)
// {
//     return false;
// }

///////////////////////////////////////////////////////////////////
#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetmobject.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetmobject.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __MOBJECT_H__
