// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __FILE_LOG_H__
#define __FILE_LOG_H__

#pragma once

#include "streamfile.h"
#include "filepath.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CFileLog : *.log
class CFileLog : public MObject
{
public:
    friend class CFileLogPipe;
public:
    enum FILE_LOG
    {
        FILE_LOG_BUF      = 8192,
        FILE_LOG_CACHE    = 1024 * 1024,
        FILE_LOG_MAX_SIZE = (16 << CAPAP_20),
    };

    static CPCXStr LogFilePrefix;
    static CPCXStr LogFileTime;
    static CPCXStr LogFileName;
    static CPCXStr LogFileTrace;
    static CPCXStr LogFileDebug;
    static CPCXStr LogFileInfo;
    static CPCXStr LogFileDump;
    static CPCXStr LogFileWarn;
    static CPCXStr LogFileError;
public:
    CFileLog(bool bCreate = true, Int nLogLevel = LOGL_ALL, PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    ~CFileLog(void);

    void   SetMaxSize(size_t stMaxSize);

    Int    GetLevel(void) const;
    void   SetLevel(Int nLogLevel = LOGL_ALL);

    void   EnableLineFlush(bool bEnabled = true);
    void   Flush(void);

    bool   IsValid(void) const;
    bool   Create(PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    bool   Open(PCXStr pszName, PCXStr pszPath = nullptr);
    void   Close(void);
    // return > 0, write in size
    // return < 0, error
    Int    Put(Int nLevel, PCXStr pszLog);
    Int    Log(Int nLevel, PCXStr pszLog);
    Int    LogV(Int nLevel, PCXStr pszFormat, ...);
    Int    LogL(Int nLevel, PCXStr pszFormat, va_list vl);

private:
    CFileLog(const CFileLog&);
    CFileLog& operator=(const CFileLog&);

    Int    Write(Int nLen, PCXStr pszBuf);
    PCXStr GetFormat(Int nLevel);
private:
    Int                m_nLogLevel;
    Int                m_nStatus;
    Int                m_nFlag;
    Int                m_nOpenIndex;
    size_t             m_stLogSize;
    size_t             m_stMaxSize;
    CFileWriteStream   m_File;
    CString            m_strPath;
    CString            m_strPrefix;
    XChar              m_szBuf[FILE_LOG_BUF];
};

///////////////////////////////////////////////////////////////////
// CFileLogLock
class CFileLogLock : public MObject
{
public:
    CFileLogLock(bool bCreate = true, Int nLogLevel = LOGL_ALL, PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    ~CFileLogLock(void);

    void  SetMaxSize(size_t stMaxSize);

    Int   GetLevel(void) const;
    void  SetLevel(Int nLogLevel = LOGL_ALL);

    void  EnableLineFlush(bool bEnabled = true);
    void  Flush(void);

    bool  IsValid(void) const;
    bool  Create(PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    bool  Open(PCXStr pszName, PCXStr pszPath = nullptr);
    void  Close(void);

    Int   Put(Int nLevel, PCXStr pszLog);
    Int   Log(Int nLevel, PCXStr pszLog);
    Int   LogV(Int nLevel, PCXStr pszFormat, ...);
    Int   LogL(Int nLevel, PCXStr pszFormat, va_list vl);

private:
    CFileLogLock(const CFileLogLock&);
    CFileLogLock& operator=(const CFileLogLock&);
private:
    CFileLog    m_Log;
    CSyncLock   m_Lock;
};

///////////////////////////////////////////////////////////////////
// CFileLogPipe
// Windows : OutputDebugString-->[filter : pszPrefix]
// Linux : pszPrefix-[PID].fifo
class CFileLogPipe : public MObject
{
public:
    CFileLogPipe(bool bCreate = true, Int nLogLevel = LOGL_ALL, PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    ~CFileLogPipe(void);

    void  SetMaxSize(size_t stMaxSize);

    Int   GetLevel(void) const;
    void  SetLevel(Int nLogLevel = LOGL_ALL);

    void  EnableLineFlush(bool bEnabled = true);
    void  Flush(void);

    bool  IsValid(void) const;
    bool  IsPipeValid(void) const;
    bool  Create(PCXStr pszPath = nullptr, PCXStr pszPrefix = nullptr);
    bool  Open(PCXStr pszName, PCXStr pszPath = nullptr);
    void  Close(void);

    Int   Put(Int nLevel, PCXStr pszLog);
    Int   Log(Int nLevel, PCXStr pszLog);
    Int   LogV(Int nLevel, PCXStr pszFormat, ...);
    Int   LogL(Int nLevel, PCXStr pszFormat, va_list vl);

private:
    CFileLogPipe(const CFileLogPipe&);
    CFileLogPipe& operator=(const CFileLogPipe&);

    bool  OpenPipe(bool bCheck = false);
    void  ClosePipe(void);

    void  WritePipe(PCXStr pszBuf, size_t stBuf);
private:
    Handle      m_hPipe;
    CFileLog    m_Log;
    CString     m_strPipe;
    CSyncLock   m_Lock;
};

#ifdef __RUNTIME_DEBUG__
    #define LOG_TRACE(__log__, __str__)             (__log__).Log(LOGL_TRACE, (__str__))
    #define LOG_DEBUG(__log__, __str__)             (__log__).Log(LOGL_DEBUG, (__str__))
    #define LOGV_TRACE(__log__, ...)                (__log__).LogV(LOGL_TRACE, __VA_ARGS__)
    #define LOGV_DEBUG(__log__, ...)                (__log__).LogV(LOGL_DEBUG, __VA_ARGS__)
    #define LOGL_TRACE(__log__, __fmt__, __vl__)    (__log__).LogL(LOGL_TRACE, (__fmt__), (__vl__))
    #define LOGL_DEBUG(__log__, __fmt__, __vl__)    (__log__).LogL(LOGL_DEBUG, (__fmt__), (__vl__))
#else
    #define LOG_TRACE(__log__, __str__)
    #define LOG_DEBUG(__log__, __str__)
    #define LOGV_TRACE(__log__, ...)
    #define LOGV_DEBUG(__log__, ...)
    #define LOGL_TRACE(__log__, __fmt__, __vl__)
    #define LOGL_DEBUG(__log__, __fmt__, __vl__)
#endif
#define LOG_INFO(__log__, __str__)                   (__log__).Log(LOGL_INFO,  (__str__))
#define LOG_DUMP(__log__, __str__)                   (__log__).Log(LOGL_DUMP,  (__str__))
#define LOG_WARN(__log__, __str__)                   (__log__).Log(LOGL_WARN,  (__str__))
#define LOG_ERROR(__log__, __str__)                  (__log__).Log(LOGL_ERROR, (__str__))
#define LOGV_INFO(__log__, ...)                      (__log__).LogV(LOGL_INFO,  __VA_ARGS__)
#define LOGV_DUMP(__log__, ...)                      (__log__).LogV(LOGL_DUMP,  __VA_ARGS__)
#define LOGV_WARN(__log__, ...)                      (__log__).LogV(LOGL_WARN,  __VA_ARGS__)
#define LOGV_ERROR(__log__, ...)                     (__log__).LogV(LOGL_ERROR, __VA_ARGS__)
#define LOGL_INFO(__log__, __fmt__, __vl__)          (__log__).LogL(LOGL_INFO,  (__fmt__), (__vl__))
#define LOGL_DUMP(__log__, __fmt__, __vl__)          (__log__).LogL(LOGL_DUMP,  (__fmt__), (__vl__))
#define LOGL_WARN(__log__, __fmt__, __vl__)          (__log__).LogL(LOGL_WARN,  (__fmt__), (__vl__))
#define LOGL_ERROR(__log__, __fmt__, __vl__)         (__log__).LogL(LOGL_ERROR, (__fmt__), (__vl__))

///////////////////////////////////////////////////////////////////
#include "filelog.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetfilelog.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetfilelog.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __FILE_LOG_H__
