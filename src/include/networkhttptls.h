// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_HTTP_TLS_H__
#define __NETWORK_HTTP_TLS_H__

#pragma once

#include "networkhttp.h"
#include "networktls.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CHTTPSSession
class CHTTPSSession : public CHTTPSession
{
public:
    CHTTPSSession(CNETTraits::Socket sSocket);
    CHTTPSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef);
    virtual ~CHTTPSSession(void);
};
typedef CTRefCountPtr<CHTTPSSession> CHTTPSSessionPtr;

///////////////////////////////////////////////////////////////////
// CWSSSession
class CWSSSession : public CWSSession
{
public:
    CWSSSession(CNETTraits::Socket sSocket);
    CWSSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef);
    virtual ~CWSSSession(void);
};
typedef CTRefCountPtr<CWSSSession> CWSSSessionPtr;

///////////////////////////////////////////////////////////////////
// CWEBTLSServer
class CWEBTLSServer : public CNulPackNetworkEventHandler
{
public:
    enum WEBTLS_PORT
    {
        WEBTLS_DEFPORT = 443,
    };
public:
    CWEBTLSServer(void);
    virtual ~CWEBTLSServer(void);

    bool Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, CEventHandler& EventHandlerRef, bool bManage = true);
    void Exit(void);

    bool Check(void);

    bool Listen(PCXStr pszIp = nullptr, UShort usPort = WEBTLS_DEFPORT); 
    bool Listen(STR_ADDR& strAddr);
    bool Listen(NET_ADDR& NetAddr);

    bool Send(Socket sSocket, PCStr pszData, size_t stSize);
    bool Send(Socket sSocket, const CCString& strData);
    bool Send(Socket sSocket, PByte pData, size_t stSize);
    bool Send(Socket sSocket, const CBufStream& Stream);
    bool Send(Socket sSocket, CStreamScopePtr& StreamPtr);

    bool Destory(Socket sSocket);
    // sethash after init
    void    SetHash(Int nHashSize);

    bool    IsNetBufferMode(void) const;
    void    SetNetBufferMode(void);

    size_t  GetMaxBlockSize(void) const;
    void    SetMaxBlockSize(size_t stMaxBlock);

    size_t  GetMaxCacheSize(void) const;
    void    SetMaxCacheSize(size_t stMaxCache);

    CNetworkPtr& GetNetwork(void);
    const CNetworkPtr& GetNetwork(void) const;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;
protected:
    virtual bool OnTcpAccept(Socket sAccept, Socket sListen) OVERRIDE;
    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData) OVERRIDE;

    virtual bool OnTlsHandShake(UInt uError, Socket sSocket) OVERRIDE;
    virtual bool OnTlsRecv(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTlsSend(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTlsError(UInt uError, Socket sSocket) OVERRIDE;

    bool OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);
    bool OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);

    bool CreateSession(Socket sSocket);
    bool DestroySession(Socket sSocket);
    bool CheckSession(Socket sSocket);

    bool GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr);
    bool RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr);

    bool ListenHandle(void);
    bool SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs);
    bool RecvHandle(CWEBSessionPtr& SessionPtr);
    bool CloseHandle(CWEBSessionPtr& SessionPtr);

    bool ErrorHandle(CWEBSessionPtr& SessionPtr);

    bool CompleteHandle(CWEBSessionPtr& SessionPtr);
    Int  CompleteWSCheck(CWSSSession* pSession, CWEBSessionPtr& SessionPtr);

    bool UpgradeHandle(CWEBSessionPtr& SessionPtr);
    // ws enable ping-control frame handle
    bool UpgradeWSSession(const CHTTPRequest& Request, CHTTPSSession* pSession, CWEBSessionPtr& SessionPtr);
protected:
    size_t             m_stMaxBlock;
    size_t             m_stMaxCache;

    bool               m_bManage;
    bool               m_bNetBuffer;
    Socket             m_sSocket;
    NET_ATTR           m_NetAttr;
    NET_ADDR           m_NetAddr;

    CNetworkPtr        m_NetworkPtr;
    CNetworkTLSPtr     m_NetworkTLSPtr;
    CEventHandlerPtr   m_EventHandlerPtr;

    MAP_WEBSESSION     m_Session;
    CSyncLock          m_SessionLock;
};

///////////////////////////////////////////////////////////////////
// CWEBTLSClient
class CWEBTLSClient : public CNulPackNetworkEventHandler
{
public:
   CWEBTLSClient(void);
   virtual ~CWEBTLSClient(void);

    bool Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, bool bManage = true);
    bool Init(CNetworkPtr& NetworkPtr, CNetworkTLS::CONFIG_PARAM& Param, CEventHandler& EventHandlerRef, bool bManage = true);
    void Exit(void);

    bool Check(void);

    bool Create(Socket& sSocket, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr);
    bool Create(Socket& sSocket, CEventHandler& EventHandlerRef, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr);
    bool Create(Socket& sSocket, NET_ADDR& NetAddrRef);
    bool Create(Socket& sSocket, CEventHandler& EventHandlerRef, NET_ADDR& NetAddrRef);

    bool Destory(Socket sSocket);

    bool Connect(Socket sSocket, PCXStr pszIp = nullptr, UShort usPort = CWEBTLSServer::WEBTLS_DEFPORT); 
    bool Connect(Socket sSocket, STR_ADDR& strAddr);
    bool Connect(Socket sSocket, NET_ADDR& NetAddr);

    bool Send(Socket sSocket, PCStr pszData, size_t stSize);
    bool Send(Socket sSocket, const CCString& strData);
    bool Send(Socket sSocket, PByte pData, size_t stSize);
    bool Send(Socket sSocket, const CBufStream& Stream);
    bool Send(Socket sSocket, CStreamScopePtr& StreamPtr);
    // sethash after init
    void    SetHash(Int nHashSize);

    bool    IsNetBufferMode(void) const;
    void    SetNetBufferMode(void);

    bool    IsAutoCheckMode(void) const;
    void    SetAutoCheckMode(void);

    UInt    GetControlCode(void) const;
    void    SetControlCode(bool bPing = true); // false = pong

    size_t  GetMaxBlockSize(void) const;
    void    SetMaxBlockSize(size_t stMaxBlock);

    size_t  GetMaxCacheSize(void) const;
    void    SetMaxCacheSize(size_t stMaxCache);

    CNetworkPtr& GetNetwork(void);
    const CNetworkPtr& GetNetwork(void) const;

    CEventHandlerPtr& GetEventHandler(void);
    const CEventHandlerPtr& GetEventHandler(void) const;
protected:
    virtual bool OnTcpConnect(UInt uError, Socket sConnect) OVERRIDE;
    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData) OVERRIDE;

    virtual bool OnTlsHandShake(UInt uError, Socket sSocket) OVERRIDE;
    virtual bool OnTlsRecv(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTlsSend(size_t stSize, PTCP_PARAM pTcp) OVERRIDE;
    virtual bool OnTlsError(UInt uError, Socket sSocket) OVERRIDE;

    bool OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);
    bool OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp);

    bool CreateSession(Socket& sSocket, NET_ADDR& NetAddr);
    bool CreateSession(Socket& sSocket, NET_ADDR& NetAddr, CEventHandler& EventHandlerRef);
    
    bool DestroySession(Socket sSocket);
    bool CheckSession(Socket sSocket);

    bool GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr);
    bool RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr);

    bool GetHandler(CWEBSessionPtr& SessionPtr, CEventHandlerPtr& EventHandlerPtr);

    bool SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs);
    bool RecvHandle(CWEBSessionPtr& SessionPtr);
    bool CloseHandle(CWEBSessionPtr& SessionPtr);

    bool ErrorHandle(CWEBSessionPtr& SessionPtr);

    bool CompleteHandle(CWEBSessionPtr& SessionPtr);
    Int  CompleteWSCheck(CWSSSession* pSession, CWEBSessionPtr& SessionPtr);

    bool UpgradeHandle(CWEBSessionPtr& SessionPtr);
    // set m_bAutoCheck to ws-session
    bool UpgradeWSSession(const CHTTPResponse& Response, CHTTPSSession* pSession, CWEBSessionPtr& SessionPtr);

protected:
    size_t             m_stMaxBlock;
    size_t             m_stMaxCache;

    bool               m_bManage;
    bool               m_bExited;
    bool               m_bNetBuffer;
    bool               m_bAutoCheck;   // ws auto check
    UInt               m_uControlCode; // default : ping
    NET_ATTR           m_NetAttr;

    CNetworkPtr        m_NetworkPtr;
    CNetworkTLSPtr     m_NetworkTLSPtr;
    CEventHandlerPtr   m_EventHandlerPtr;

    MAP_WEBSESSION     m_Session;
    CSyncLock          m_SessionLock;
};

///////////////////////////////////////////////////////////////////
#include "networkhttptls.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_HTTP_TLS_H__
