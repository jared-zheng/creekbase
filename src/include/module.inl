// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MODULE_INL__
#define __MODULE_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CModule
INLINE CModule::CModule(Module m, bool bUnload)
: m_bUnload(bUnload)
, m_hModule(m)
{
}

INLINE CModule::~CModule(void)
{
    Close();
}

INLINE CModule::CModule(const CModule& aSrc)
: m_bUnload(false)
, m_hModule(aSrc.m_hModule)
{
}

INLINE CModule& CModule::operator=(const CModule& aSrc)
{
    if (this != &aSrc)
    {
        Close();
        m_bUnload = false;
        m_hModule = aSrc.m_hModule;
    }
    return (*this);
}

INLINE CModule& CModule::operator=(Module m)
{
    if (m_hModule != m)
    {
        Close();
        m_bUnload = true;
        m_hModule = m;
    }
    return (*this);
}

INLINE bool CModule::operator==(Module m) const
{
    return (m_hModule == m);
}

INLINE bool CModule::operator!=(Module m) const
{
    return (m_hModule != m);
}

INLINE bool CModule::operator==(const CModule& aSrc) const
{
    return (m_hModule == aSrc.m_hModule);
}

INLINE bool CModule::operator!=(const CModule& aSrc) const
{
    return (m_hModule != aSrc.m_hModule);
}

INLINE CModule::operator Module(void) const
{
    return m_hModule;
}

template <typename I>
INLINE I CModule::Find(PCStr pszSymbol)
{
    return reinterpret_cast<I>(m_hModule.Find(pszSymbol));
}

INLINE bool CModule::Attach(PCXStr pszPath, bool bUnload)
{
    Close();
    m_bUnload = bUnload;
    return m_hModule.Load(pszPath);
}

INLINE void CModule::Attach(Module m, bool bUnload)
{
    Close();
    m_bUnload = bUnload;
    m_hModule = m;
}

INLINE Module CModule::Detach(void)
{
    m_bUnload = false;
    return m_hModule.Detach();
}

INLINE void CModule::Close(void)
{
    if (m_bUnload == true)
    {
        m_hModule.Close();
    }
    else
    {
        m_hModule.Detach();
    }
    m_bUnload = false;
}

#endif // __MODULE_INL__