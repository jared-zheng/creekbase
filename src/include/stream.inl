// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __STREAM_INL__
#define __STREAM_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CStream
INLINE CStream::CStream(Int nMode)
: m_nMode(nMode)
{
}

INLINE CStream::~CStream(void)
{
}

INLINE CStream::CStream(const CStream& aSrc)
: m_nMode(aSrc.m_nMode)
{
}

INLINE CStream& CStream::operator=(const CStream& aSrc)
{
    if (&aSrc != this)
    {
        m_nMode = aSrc.m_nMode;
    }
    return (*this);
}

INLINE size_t CStream::Read(void*, size_t)
{
    return 0;
}

INLINE size_t CStream::Write(const void*, size_t)
{
    return 0;
}

INLINE size_t CStream::Tell(void) const
{
    return 0;
}

INLINE size_t CStream::Size(void) const
{
    return 0;
}

INLINE size_t CStream::Rest(void) const
{
    return 0;
}

INLINE bool CStream::Flush(Int)
{
    return false;
}

INLINE void CStream::Close(void)
{
    m_nMode &= ~STREAMM_ERROR;
}

INLINE bool CStream::Refer(CStream&)
{
    return false;
}

INLINE Int CStream::GetMode(void) const
{
    return m_nMode;
}

INLINE void CStream::SetError(void)
{
    m_nMode |= STREAMM_ERROR;
}

INLINE bool CStream::IsError(void) const
{
    return ((m_nMode & STREAMM_ERROR) != 0);
}

INLINE bool CStream::IsByteSwap(void) const
{
    return ((m_nMode & STREAMM_BYTESWAP) != 0);
}

INLINE bool CStream::IsEnd(void) const
{
    if (IsError() == false)
    {
        return (Rest() == 0);
    }
    return true;
}

INLINE bool CStream::IsRead(void) const
{
    if (IsError() == false)
    {
        return ((m_nMode & STREAMM_READ) != 0);
    }
    return false;
}

INLINE bool CStream::IsWrite(void) const
{
    if (IsError() == false)
    {
        return ((m_nMode & STREAMM_WRITE) != 0);
    }
    return false;
}

INLINE void CStream::SetByteSwap(bool bEnabled)
{
    if (bEnabled)
    {
        m_nMode |= STREAMM_BYTESWAP;
    }
    else
    {
        m_nMode &= (~STREAMM_BYTESWAP);
    }
}

// write operations
INLINE CStream& CStream::operator<<(Char c)
{
    if (IsWrite())
    {
        Write(&c, sizeof(Char));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(Short s)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            s = (Short)CPlatform::ByteSwap((UShort)s);
        }
        Write(&s, sizeof(Short));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(Int n)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            n = (Int)CPlatform::ByteSwap((UInt)n);
        }
        Write(&n, sizeof(Int));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(Long l)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            l = (Long)CPlatform::ByteSwap((ULong)l);
        }
        Write(&l, sizeof(Long));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(LLong ll)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            ll = (LLong)CPlatform::ByteSwap((ULLong)ll);
        }
        Write(&ll, sizeof(LLong));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(UChar uc)
{
    if (IsWrite())
    {
        Write(&uc, sizeof(UChar));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(UShort us)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            us = CPlatform::ByteSwap(us);
        }
        Write(&us, sizeof(UShort));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(UInt u)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            u = CPlatform::ByteSwap(u);
        }
        Write(&u, sizeof(UInt));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(ULong ul)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            ul = CPlatform::ByteSwap(ul);
        }
        Write(&ul, sizeof(ULong));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(ULLong ull)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
            ull = CPlatform::ByteSwap(ull);
        }
        Write(&ull, sizeof(ULLong));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(Float f)
{
    if (IsWrite())
    {
        if (IsByteSwap() == false)
        {
            Write(&f, sizeof(Float));
        }
        else
        {
            Byte bFloat[sizeof(Float)] = { 0 };
            MM_SAFE::Cpy(bFloat, sizeof(Float), &f, sizeof(Float));
            Write(bFloat, sizeof(Float));
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(Double d)
{
    if (IsWrite())
    {
        if (IsByteSwap() == false)
        {
            Write(&d, sizeof(Double));
        }
        else
        {
            Byte bDouble[sizeof(Double)] = { 0 };
            MM_SAFE::Cpy(bDouble, sizeof(Double), &d, sizeof(Double));
            Write(bDouble, sizeof(Double));
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(bool b)
{
    if (IsWrite())
    {
        Write(&b, sizeof(bool));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(WChar w)
{
    if (IsWrite())
    {
        if (IsByteSwap())
        {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
            w = CPlatform::ByteSwap((UInt)w);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
            w = CPlatform::ByteSwap((UShort)w);
#endif
        }
        Write(&w, sizeof(WChar));
    }
    return (*this);
}

template <typename T>
INLINE CStream& CStream::operator<<(T* p)
{
    if (IsWrite())
    {
        uintptr_t ut = reinterpret_cast<uintptr_t>(p);
        if (IsByteSwap())
        {
            ut = CPlatform::ByteSwap(ut);
        }
        Write(&ut, sizeof(uintptr_t));
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(const CString& str)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)str.Length();
        if (IsByteSwap() == false)
        {
            Write(&sLen, sizeof(SizeLen));
            if (sLen > 0)
            {
                Write(*str, sLen * sizeof(XChar));
            }
        }
        else
        {
            SizeLen sl = CPlatform::ByteSwap(sLen);
            Write(&sl, sizeof(SizeLen));
            WriteSwapString(*str, sLen);
        }
    }
    return (*this);
}

template <size_t stLenT>
INLINE CStream& CStream::operator<<(const CTStringFix<CXChar, stLenT>& strFix)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)strFix.Length();
        if (IsByteSwap() == false)
        {
            Write(&sLen, sizeof(SizeLen));
            if (sLen > 0)
            {
                Write(*strFix, sLen * sizeof(XChar));
            }
        }
        else
        {
            SizeLen sl = CPlatform::ByteSwap(sLen);
            Write(&sl, sizeof(SizeLen));
            WriteSwapString(*strFix, sLen);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(const CStringRef& strRef)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)strRef.Length();
        if (IsByteSwap() == false)
        {
            Write(&sLen, sizeof(SizeLen));
            if (sLen > 0)
            {
                Write(*strRef, sLen * sizeof(XChar));
            }
        }
        else
        {
            SizeLen sl = CPlatform::ByteSwap(sLen);
            Write(&sl, sizeof(SizeLen));
            WriteSwapString(*strRef, sLen);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<<(PCXStr psz)
{
    if (IsWrite())
    {
        SizeLen sLen = (psz != nullptr) ? (SizeLen)CXChar::Length(psz) : 0;
        if (IsByteSwap() == false)
        {
            Write(&sLen, sizeof(SizeLen));
            if (sLen > 0)
            {
                Write(psz, sLen * sizeof(XChar));
            }
        }
        else
        {
            SizeLen sl = CPlatform::ByteSwap(sLen);
            Write(&sl, sizeof(SizeLen));
            WriteSwapString(psz, sLen);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<=(const CString& str)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)str.Length();
        if (sLen > 0)
        {
            if (IsByteSwap() == false)
            {
                Write(*str, sLen * sizeof(XChar));
            }
            else
            {
                WriteSwapString(*str, sLen);
            }
        }
    }
    return (*this);
}

template <size_t stLenT>
INLINE CStream& CStream::operator<=(const CTStringFix<CXChar, stLenT>& strFix)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)strFix.Length();
        if (sLen > 0)
        {
            if (IsByteSwap() == false)
            {
                Write(*strFix, sLen * sizeof(XChar));
            }
            else
            {
                WriteSwapString(*strFix, sLen);
            }
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<=(const CStringRef& strRef)
{
    if (IsWrite())
    {
        SizeLen sLen = (SizeLen)strRef.Length();
        if (sLen > 0)
        {
            if (IsByteSwap() == false)
            {
                Write(*strRef, sLen * sizeof(XChar));
            }
            else
            {
                WriteSwapString(*strRef, sLen);
            }
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator<=(PCXStr psz)
{
    if (IsWrite())
    {
        SizeLen sLen = (psz != nullptr) ? (SizeLen)CXChar::Length(psz) : 0;
        if (sLen > 0)
        {
            if (IsByteSwap() == false)
            {
                Write(psz, sLen * sizeof(XChar));
            }
            else
            {
                WriteSwapString(psz, sLen);
            }
        }
    }
    return (*this);
}
// read operations
INLINE CStream& CStream::operator>>(Char& c)
{
    if (IsRead())
    {
        Read(&c, sizeof(Char));
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(Short& s)
{
    if (IsRead())
    {
        Read(&s, sizeof(Short));
        if (IsByteSwap())
        {
            s = (Short)CPlatform::ByteSwap((UShort)s);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(Int& n)
{
    if (IsRead())
    {
        Read(&n, sizeof(Int));
        if (IsByteSwap())
        {
            n = (Int)CPlatform::ByteSwap((UInt)n);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(Long& l)
{
    if (IsRead())
    {
        Read(&l, sizeof(Long));
        if (IsByteSwap())
        {
            l = (Long)CPlatform::ByteSwap((ULong)l);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(LLong& ll)
{
    if (IsRead())
    {
        Read(&ll, sizeof(LLong));
        if (IsByteSwap())
        {
            ll = (LLong)CPlatform::ByteSwap((ULLong)ll);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(UChar& uc)
{
    if (IsRead())
    {
        Read(&uc, sizeof(UChar));
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(UShort& us)
{
    if (IsRead())
    {
        Read(&us, sizeof(UShort));
        if (IsByteSwap())
        {
            us = CPlatform::ByteSwap(us);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(UInt& u)
{
    if (IsRead())
    {
        Read(&u, sizeof(UInt));
        if (IsByteSwap())
        {
            u = CPlatform::ByteSwap(u);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(ULong& ul)
{
    if (IsRead())
    {
        Read(&ul, sizeof(ULong));
        if (IsByteSwap())
        {
            ul = CPlatform::ByteSwap(ul);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(ULLong& ull)
{
    if (IsRead())
    {
        Read(&ull, sizeof(ULLong));
        if (IsByteSwap())
        {
            ull = CPlatform::ByteSwap(ull);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(Float& f)
{
    if (IsRead())
    {
        if (IsByteSwap() == false)
        {
            Read(&f, sizeof(Float));
        }
        else
        {
            Byte bFloat[sizeof(Float)] = { 0 };
            Read(bFloat, sizeof(Float));
            MM_SAFE::Cpy(&f, sizeof(Float), bFloat, sizeof(Float));
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(Double& d)
{
    if (IsRead())
    {
        if (IsByteSwap() == false)
        {
            Read(&d, sizeof(Double));
        }
        else
        {
            Byte bDouble[sizeof(Double)] = { 0 };
            Read(bDouble, sizeof(Double));
            MM_SAFE::Cpy(&d, sizeof(Double), bDouble, sizeof(Double));
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(bool& b)
{
    if (IsRead())
    {
        Read(&b, sizeof(bool));
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(WChar& w)
{
    if (IsRead())
    {
        Read(&w, sizeof(WChar));
        if (IsByteSwap())
        {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
            w = CPlatform::ByteSwap((UInt)w);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
            w = CPlatform::ByteSwap((UShort)w);
#endif
        }
    }
    return (*this);
}

template <typename T>
INLINE CStream& CStream::operator>>(T*& p)
{
    if (IsRead())
    {
        uintptr_t ut = 0;
        Read(&ut, sizeof(uintptr_t));
        if (IsByteSwap())
        {
            ut = CPlatform::ByteSwap(ut);
        }
        p = reinterpret_cast<T*>(ut);
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(CString& str)
{
    if (IsRead())
    {
        SizeLen sLen = 0;
        Read(&sLen, sizeof(SizeLen));
        if (IsByteSwap())
        {
            sLen = CPlatform::ByteSwap(sLen);
        }
        if ((sLen > 0) && str.SetBufferLength((size_t)sLen + 1))
        {
            ReadSwapString(*str, sLen);
            str.ResetLength((size_t)sLen);
        }
        else
        {
            str.Empty();
        }
    }
    return (*this);
}

template <size_t stLenT>
INLINE CStream& CStream::operator>>(CTStringFix<CXChar, stLenT>& strFix)
{
    if (IsRead())
    {
        SizeLen sLen = 0;
        Read(&sLen, sizeof(SizeLen));
        if (IsByteSwap())
        {
            sLen = CPlatform::ByteSwap(sLen);
        }
        if ((sLen > 0) && ((size_t)sLen < stLenT))
        {
            ReadSwapString(*strFix, sLen);
            strFix.ResetLength((size_t)sLen);
        }
        else
        {
            strFix.Empty();
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>>(CStringRef& strRef)
{
    if (IsRead())
    {
        SizeLen sLen = 0;
        Read(&sLen, sizeof(SizeLen));
        if (IsByteSwap())
        {
            sLen = CPlatform::ByteSwap(sLen);
        }
        if ((sLen > 0) && ((size_t)sLen < strRef.Length()))
        {
            ReadSwapString(*strRef, sLen);
            strRef.ResetLength((size_t)sLen);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>=(CString& str)
{
    // str -->SetBufferLength(set buffer) -->ResetLength(set length)
    if (IsRead())
    {
        SizeLen sLen = (SizeLen)str.Length();
        if (sLen > 0)
        {
            ReadSwapString(*str, sLen);
        }
    }
    return (*this);
}

template <size_t stLenT>
INLINE CStream& CStream::operator>=(CTStringFix<CXChar, stLenT>& strFix)
{
    if (IsRead())
    {
        // strFix -->ResetLength(set length)
        SizeLen sLen = (SizeLen)strFix.Length();
        if (sLen > 0)
        {
            ReadSwapString(*strFix, sLen);
        }
    }
    return (*this);
}

INLINE CStream& CStream::operator>=(CStringRef& strRef)
{
    if (IsRead())
    {
        // strRef -->SetBufferLength(set buffer) -->ResetLength(set length)
        SizeLen sLen = (SizeLen)strRef.Length();
        if (sLen > 0)
        {
            ReadSwapString(*strRef, sLen);
        }
    }
    return (*this);
}

INLINE void CStream::WriteSwapString(PCXStr psz, SizeLen sLen)
{
    if (sLen > 0)
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        XChar xc = 0;
        for (SizeLen i = 0; i < sLen; ++i)
        {
            xc = (XChar)CPlatform::ByteSwap((UInt)psz[i]);
            Write(&xc, sizeof(XChar));
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        XChar xc = 0;
        for (SizeLen i = 0; i < sLen; ++i)
        {
            xc = (XChar)CPlatform::ByteSwap((UShort)psz[i]);
            Write(&xc, sizeof(XChar));
        }
#else
        Write(psz, sLen * sizeof(XChar));
#endif
    }
}

INLINE void CStream::ReadSwapString(PXStr psz, SizeLen sLen)
{
    if (IsByteSwap())
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        XChar xc = 0;
        for (SizeLen i = 0; i < sLen; ++i)
        {
            Read(&xc, sizeof(UInt));
            xc = (XChar)CPlatform::ByteSwap((UInt)xc);
            psz[i] = xc;
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        XChar xc = 0;
        for (SizeLen i = 0; i < sLen; ++i)
        {
            Read(&xc, sizeof(UShort));
            xc = (XChar)CPlatform::ByteSwap((UShort)xc);
            psz[i] = xc;
        }
#else
        Read(psz, sLen * sizeof(XChar));
#endif
    }
    else
    {
        Read(psz, sLen * sizeof(XChar));
    }
    psz[sLen] = 0;
}

///////////////////////////////////////////////////////////////////
// CStreamSeekScope
INLINE CStreamSeekScope::CStreamSeekScope(CStream& Stream, SeekPos skPos, SEEK_OP eFrom)
: m_pStream(&Stream)
, m_stPos(0)
{
    m_stPos = Stream.Tell();
    Stream.Seek(skPos, eFrom);
}

INLINE CStreamSeekScope::CStreamSeekScope(CStream& Stream)
: m_pStream(&Stream)
{
    m_stPos = Stream.Tell();
}

INLINE CStreamSeekScope::~CStreamSeekScope(void)
{
    if (m_stPos != m_pStream->Tell())
    {
        m_pStream->Seek(m_stPos);
    }
}

INLINE CStreamSeekScope::CStreamSeekScope(const CStreamSeekScope&)
: m_pStream(nullptr)
, m_stPos(0)
{
}

INLINE CStreamSeekScope& CStreamSeekScope::operator=(const CStreamSeekScope&)
{
    return (*this);
}

#endif // __STREAM_INL__
