// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __REF_COUNT_INL__
#define __REF_COUNT_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CTRefCount
template <typename T>
INLINE CTRefCount<T>::CTRefCount(void)
{
}

template <typename T>
INLINE CTRefCount<T>::~CTRefCount(void)
{
}

template <typename T>
INLINE CTRefCount<T>::CTRefCount(const CTRefCount<T>&)
{
}

template <typename T>
INLINE CTRefCount<T>& CTRefCount<T>::operator=(const CTRefCount<T>&)
{
    return (*this);
}

template <typename T>
INLINE Int CTRefCount<T>::AddRef(void)
{
    return m_Counter.Signal();
}

template <typename T>
INLINE Int CTRefCount<T>::Release(void)
{
    Int nRef = m_Counter.Reset();
    if ((nRef == 0) && MCheck(this))
    {
        MDELETE this;
    }
    return nRef;
}

///////////////////////////////////////////////////////////////////
// CTRefCountPtr
template <typename T>
INLINE CTRefCountPtr<T>::CTRefCountPtr(T* pRefCount, bool bAddRef)
: m_pRefCount(pRefCount)
{
    if ((m_pRefCount != nullptr) && (bAddRef == true))
    {
        m_pRefCount->AddRef();
    }
}

template <typename T>
INLINE CTRefCountPtr<T>::~CTRefCountPtr(void)
{
    if (m_pRefCount != nullptr)
    {
        m_pRefCount->Release();
    }
    m_pRefCount = nullptr;
}

template <typename T>
INLINE CTRefCountPtr<T>::CTRefCountPtr(const CTRefCountPtr<T>& aSrc)
: m_pRefCount(aSrc.m_pRefCount)
{
    if (m_pRefCount != nullptr)
    {
        m_pRefCount->AddRef();
    }
}

template <typename T>
template <typename X>
INLINE CTRefCountPtr<T>::CTRefCountPtr(const CTRefCountPtr<X>& aSrc)
: m_pRefCount(static_cast<T*>(aSrc.Get()))
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pRefCount != nullptr)
    {
        m_pRefCount->AddRef();
    }
}

template <typename T>
INLINE CTRefCountPtr<T>& CTRefCountPtr<T>::operator=(const CTRefCountPtr<T>& aSrc)
{
    if (m_pRefCount != aSrc.m_pRefCount)
    {
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->Release();
        }
        m_pRefCount = aSrc.m_pRefCount;
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->AddRef();
        }
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTRefCountPtr<T>& CTRefCountPtr<T>::operator=(const CTRefCountPtr<X>& aSrc)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pRefCount != static_cast<T*>(aSrc.Get()))
    {
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->Release();
        }
        m_pRefCount = static_cast<T*>(aSrc.Get());
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->AddRef();
        }
    }
    return (*this);
}

template <typename T>
INLINE CTRefCountPtr<T>& CTRefCountPtr<T>::operator=(T* pRefCount)
{
    if (m_pRefCount != pRefCount)
    {
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->Release();
        }
        m_pRefCount = pRefCount;
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->AddRef();
        }
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTRefCountPtr<T>& CTRefCountPtr<T>::operator=(X* pRefCount)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pRefCount != static_cast<T*>(pRefCount))
    {
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->Release();
        }
        m_pRefCount = static_cast<T*>(pRefCount);
        if (m_pRefCount != nullptr)
        {
            m_pRefCount->AddRef();
        }
    }
    return (*this);
}

template <typename T>
INLINE CTRefCountPtr<T>::operator T*(void) const
{
    return m_pRefCount;
}

template <typename T>
INLINE T& CTRefCountPtr<T>::operator*(void) const
{
    return (*m_pRefCount);
}

template <typename T>
INLINE T* CTRefCountPtr<T>::operator->(void) const
{
    return m_pRefCount;
}

template <typename T>
INLINE T* CTRefCountPtr<T>::Get(void) const
{
    return m_pRefCount;
}

template <typename T>
INLINE bool CTRefCountPtr<T>::operator==(T* pRefCount) const
{
    return (m_pRefCount == pRefCount);
}

template <typename T>
INLINE bool CTRefCountPtr<T>::operator!=(T* pRefCount) const
{
    return (m_pRefCount != pRefCount);
}

template <typename T>
INLINE bool CTRefCountPtr<T>::operator==(const CTRefCountPtr<T>& aSrc) const
{
    return (m_pRefCount == aSrc.m_pRefCount);
}

template <typename T>
template <typename X>
INLINE bool CTRefCountPtr<T>::operator==(const CTRefCountPtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pRefCount == static_cast<T*>(aSrc.Get()));
}

template <typename T>
INLINE bool CTRefCountPtr<T>::operator!=(const CTRefCountPtr<T>& aSrc) const
{
    return (m_pRefCount != aSrc.m_pRefCount);
}

template <typename T>
template <typename X>
INLINE bool CTRefCountPtr<T>::operator!=(const CTRefCountPtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pRefCount != static_cast<T*>(aSrc.Get()));
}

template <typename T>
template <typename X>
INLINE CTRefCountPtr<X>& CTRefCountPtr<T>::Cast(void)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<T, X>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (CTRefCountPtr<X>&)(*this);
}

///////////////////////////////////////////////////////////////////
// CTSharePtr
template <typename T>
INLINE CTSharePtr<T>::CTSharePtr(T* pObject)
: m_pObject(pObject)
{
    m_pCounter = MNEW CSyncCounter(1);
    assert(m_pCounter != nullptr);
}

template <typename T>
INLINE CTSharePtr<T>::~CTSharePtr(void)
{
    if (m_pCounter->Reset() == 0)
    {
        MM_SAFE::DELETE_PTR<T>(m_pObject);
        MM_SAFE::DELETE_PTR<CSyncCounter>(m_pCounter);
    }
    m_pObject  = nullptr;
    m_pCounter = nullptr;
}

template <typename T>
INLINE CTSharePtr<T>::CTSharePtr(const CTSharePtr<T>& aSrc)
: m_pObject(aSrc.m_pObject)
{
    m_pCounter = aSrc.m_pCounter;
    if (m_pCounter != nullptr)
    {
        m_pCounter->Signal();
    }
}

template <typename T>
template <typename X>
INLINE CTSharePtr<T>::CTSharePtr(const CTSharePtr<X>& aSrc)
: m_pObject(static_cast<T*>(aSrc.m_pObject))
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    m_pCounter = aSrc.m_pCounter;
    if (m_pCounter != nullptr)
    {
        m_pCounter->Signal();
    }
}

template <typename T>
template <typename X>
INLINE CTSharePtr<T>::CTSharePtr(const CTWeakPtr<X>& aSrc)
: m_pObject(static_cast<T*>(aSrc.m_pObject))
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (MCheck(aSrc.m_pObject) &&
        MCheck(aSrc.m_pCounter) &&
        (aSrc.m_pCounter->GetCount() > 0))
    {
        m_pCounter = aSrc.m_pCounter;
        if (m_pCounter != nullptr)
        {
            m_pCounter->Signal();
        }
    }
    else
    {
        m_pObject  = nullptr;
        m_pCounter = MNEW CSyncCounter(1);
        assert(m_pCounter != nullptr);
    }
}

template <typename T>
INLINE CTSharePtr<T>& CTSharePtr<T>::operator=(const CTSharePtr<T>& aSrc)
{
    if (m_pObject != aSrc.m_pObject)
    {
        if (m_pCounter->Reset() == 0)
        {
            MM_SAFE::DELETE_PTR<T>(m_pObject);
            MM_SAFE::DELETE_PTR<CSyncCounter>(m_pCounter);
        }

        m_pObject  = aSrc.m_pObject;
        m_pCounter = aSrc.m_pCounter;
        if (m_pCounter != nullptr)
        {
            m_pCounter->Signal();
        }
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTSharePtr<T>& CTSharePtr<T>::operator=(const CTSharePtr<X>& aSrc)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pObject != static_cast<T*>(aSrc.m_pObject))
    {
        if (m_pCounter->Reset() == 0)
        {
            MM_SAFE::DELETE_PTR<T>(m_pObject);
            MM_SAFE::DELETE_PTR<CSyncCounter>(m_pCounter);
        }

        m_pObject  = static_cast<T*>(aSrc.m_pObject);
        m_pCounter = aSrc.m_pCounter;
        if (m_pCounter != nullptr)
        {
            m_pCounter->Signal();
        }
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTSharePtr<T>& CTSharePtr<T>::operator=(const CTWeakPtr<X>& aSrc)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (MCheck(aSrc.m_pObject) &&
        MCheck(aSrc.m_pCounter) &&
        (aSrc.m_pCounter->GetCount() > 0))
    {
        if (m_pObject != static_cast<T*>(aSrc.m_pObject))
        {
            if (m_pCounter->Reset() == 0)
            {
                MM_SAFE::DELETE_PTR<T>(m_pObject);
                MM_SAFE::DELETE_PTR<CSyncCounter>(m_pCounter);
            }

            m_pObject  = static_cast<T*>(aSrc.m_pObject);
            m_pCounter = aSrc.m_pCounter;
            if (m_pCounter != nullptr)
            {
                m_pCounter->Signal();
            }
        }
    }
    return (*this);
}

template <typename T>
INLINE CTSharePtr<T>& CTSharePtr<T>::operator=(T* pObject)
{
    if (m_pObject != pObject)
    {
        if (m_pCounter->Reset() == 0)
        {
            MM_SAFE::DELETE_PTR<T>(m_pObject);
            MM_SAFE::DELETE_PTR<CSyncCounter>(m_pCounter);
        }
        m_pObject  = pObject;
        m_pCounter = MNEW CSyncCounter(1);
        assert(m_pCounter != nullptr);
    }
    return (*this);
}

template <typename T>
INLINE CTSharePtr<T>::operator T*(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T& CTSharePtr<T>::operator*(void) const
{
    return (*m_pObject);
}

template <typename T>
INLINE T* CTSharePtr<T>::operator->(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T* CTSharePtr<T>::Get(void) const
{
    return m_pObject;
}

template <typename T>
INLINE bool CTSharePtr<T>::Check(void) const
{
    return (MCheck(m_pObject) && MCheck(m_pCounter));
}

template <typename T>
INLINE bool CTSharePtr<T>::operator==(T* pObject) const
{
    return (m_pObject == pObject);
}

template <typename T>
INLINE bool CTSharePtr<T>::operator!=(T* pObject) const
{
    return (m_pObject != pObject);
}

template <typename T>
INLINE bool CTSharePtr<T>::operator==(const CTSharePtr<T>& aSrc) const
{
    return (m_pObject == aSrc.m_pObject);
}

template <typename T>
template <typename X>
INLINE bool CTSharePtr<T>::operator==(const CTSharePtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pObject == static_cast<T*>(aSrc.m_pObject));
}

template <typename T>
INLINE bool CTSharePtr<T>::operator!=(const CTSharePtr<T>& aSrc) const
{
    return (m_pObject != aSrc.m_pObject);
}

template <typename T>
template <typename X>
INLINE bool CTSharePtr<T>::operator!=(const CTSharePtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pObject != static_cast<T*>(aSrc.m_pObject));
}

///////////////////////////////////////////////////////////////////
// CTWeakPtr
template <typename T>
INLINE CTWeakPtr<T>::CTWeakPtr(void)
: m_pObject(nullptr)
, m_pCounter(nullptr)
{
}

template <typename T>
INLINE CTWeakPtr<T>::~CTWeakPtr(void)
{
    m_pObject  = nullptr;
    m_pCounter = nullptr;
}

template <typename T>
INLINE CTWeakPtr<T>::CTWeakPtr(const CTWeakPtr<T>& aSrc)
: m_pObject(aSrc.m_pObject)
, m_pCounter(aSrc.m_pCounter)
{
}

template <typename T>
template <typename X>
INLINE CTWeakPtr<T>::CTWeakPtr(const CTWeakPtr<X>& aSrc)
: m_pObject(static_cast<T*>(aSrc.m_pObject))
, m_pCounter(aSrc.m_pCounter)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
}

template <typename T>
template <typename X>
INLINE CTWeakPtr<T>::CTWeakPtr(const CTSharePtr<X>& aSrc)
: m_pObject(static_cast<T*>(aSrc.m_pObject))
, m_pCounter(aSrc.m_pCounter)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
}

template <typename T>
INLINE CTWeakPtr<T>& CTWeakPtr<T>::operator=(const CTWeakPtr<T>& aSrc)
{
    if (m_pObject != aSrc.m_pObject)
    {
        m_pObject  = aSrc.m_pObject;
        m_pCounter = aSrc.m_pCounter;
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTWeakPtr<T>& CTWeakPtr<T>::operator=(const CTWeakPtr<X>& aSrc)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pObject != static_cast<T*>(aSrc.m_pObject))
    {
        m_pObject  = static_cast<T*>(aSrc.m_pObject);
        m_pCounter = aSrc.m_pCounter;
    }
    return (*this);
}

template <typename T>
template <typename X>
INLINE CTWeakPtr<T>& CTWeakPtr<T>::operator=(const CTSharePtr<X>& aSrc)
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    if (m_pObject != static_cast<T*>(aSrc.m_pObject))
    {
        m_pObject  = static_cast<T*>(aSrc.m_pObject);
        m_pCounter = aSrc.m_pCounter;
    }
    return (*this);
}

template <typename T>
INLINE CTWeakPtr<T>::operator T*(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T& CTWeakPtr<T>::operator*(void) const
{
    return (*m_pObject);
}

template <typename T>
INLINE T* CTWeakPtr<T>::operator->(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T* CTWeakPtr<T>::Get(void) const
{
    return m_pObject;
}

template <typename T>
INLINE bool CTWeakPtr<T>::Check(void) const
{
    return (MCheck(m_pObject) && MCheck(m_pCounter));
}

template <typename T>
INLINE bool CTWeakPtr<T>::operator==(T* pObject) const
{
    return (m_pObject == pObject);
}

template <typename T>
INLINE bool CTWeakPtr<T>::operator!=(T* pObject) const
{
    return (m_pObject != pObject);
}

template <typename T>
INLINE bool CTWeakPtr<T>::operator==(const CTWeakPtr<T>& aSrc) const
{
    return (m_pObject == aSrc.m_pObject);
}

template <typename T>
template <typename X>
INLINE bool CTWeakPtr<T>::operator==(const CTWeakPtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pObject == static_cast<T*>(aSrc.m_pObject));
}

template <typename T>
INLINE bool CTWeakPtr<T>::operator!=(const CTWeakPtr<T>& aSrc) const
{
    return (m_pObject != aSrc.m_pObject);
}

template <typename T>
template <typename X>
INLINE bool CTWeakPtr<T>::operator!=(const CTWeakPtr<X>& aSrc) const
{
#ifdef __RUNTIME_DEBUG__
    typename TConvertCheck<X, T>::TAG tag = ConvertibleTag();
    tag.Check();
#endif
    return (m_pObject != static_cast<T*>(aSrc.m_pObject));
}

///////////////////////////////////////////////////////////////////
// CTScopePtr
template <typename T>
INLINE CTScopePtr<T>::CTScopePtr(T* pObject, bool bManage)
: m_pObject(pObject)
, m_bManage(bManage)
{
}

template <typename T>
INLINE CTScopePtr<T>::~CTScopePtr(void)
{
    if (m_bManage)
    {
        MM_SAFE::DELETE_PTRREF<T>(m_pObject);
    }
    else
    {
        m_pObject = nullptr;
    }
}

template <typename T>
INLINE CTScopePtr<T>::CTScopePtr(const CTScopePtr<T>&)
: m_pObject(nullptr)
, m_bManage(true)
{
}

template <typename T>
INLINE CTScopePtr<T>& CTScopePtr<T>::operator=(const CTScopePtr<T>&)
{
    return (*this);
}

template <typename T>
INLINE CTScopePtr<T>& CTScopePtr<T>::operator=(T* pObject)
{
    if (m_pObject != pObject)
    {
        if (m_bManage)
        {
            MM_SAFE::DELETE_PTR<T>(m_pObject);
        }
        m_pObject = pObject;
        m_bManage = true;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T>
INLINE CTScopePtr<T>::CTScopePtr(CTScopePtr<T>&& aSrc)
: m_pObject(aSrc.m_pObject)
, m_bManage(aSrc.m_bManage)
{
    aSrc.m_pObject = nullptr;
    aSrc.m_bManage = false;
}

template <typename T>
INLINE CTScopePtr<T>& CTScopePtr<T>::operator=(CTScopePtr<T>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        if (m_bManage)
        {
            MM_SAFE::DELETE_PTRREF<T>(m_pObject);
        }
        m_pObject      = aSrc.m_pObject;
        m_bManage      = aSrc.m_bManage;
        aSrc.m_pObject = nullptr;
        aSrc.m_bManage = false;
    }
    return (*this);
}
#endif

template <typename T>
INLINE CTScopePtr<T>::operator T*(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T& CTScopePtr<T>::operator*(void) const
{
    return (*m_pObject);
}

template <typename T>
INLINE T* CTScopePtr<T>::operator->(void) const
{
    return m_pObject;
}

template <typename T>
INLINE T* CTScopePtr<T>::Get(void) const
{
    return m_pObject;
}

template <typename T>
INLINE bool CTScopePtr<T>::operator==(T* pObject) const
{
    return (m_pObject == pObject);
}

template <typename T>
INLINE bool CTScopePtr<T>::operator!=(T* pObject) const
{
    return (m_pObject != pObject);
}

template <typename T>
INLINE bool CTScopePtr<T>::Attach(CTScopePtr<T>& scope)
{
    assert(this != &scope);
    if (this != &scope)
    {
        if (m_bManage)
        {
            MM_SAFE::DELETE_PTRREF<T>(m_pObject);
        }
        m_pObject = scope.m_pObject;
        m_bManage = scope.m_bManage; 
        scope.Detach();
    }
    return true;
}

template <typename T>
INLINE void CTScopePtr<T>::Detach(void)
{
    m_pObject = nullptr;
    m_bManage = false;
}

#endif // __REF_COUNT_INL__
