// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __REF_COUNT_H__
#define __REF_COUNT_H__

#pragma once

#include "sync.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTRefCount : root class of all Reference Counting
template <typename T>
class NOVTABLE CTRefCount ABSTRACT : public MObject
{
public:
    virtual Int AddRef(void);
    virtual Int Release(void);
protected:
    CTRefCount(void);
    virtual ~CTRefCount(void);

    CTRefCount(const CTRefCount&);
    CTRefCount& operator=(const CTRefCount&);
protected:
    CSyncCounter   m_Counter;
};

///////////////////////////////////////////////////////////////////
// CTRefCountPtr : smart pointer, Class T must be derived from CTRefCount
template <typename T>
class CTRefCountPtr : public MObject
{
public:
    CTRefCountPtr(T* pRefCount = nullptr, bool bAddRef = true);
    ~CTRefCountPtr(void);
    CTRefCountPtr(const CTRefCountPtr& aSrc);
    template <typename X> CTRefCountPtr(const CTRefCountPtr<X>& aSrc);

    CTRefCountPtr& operator=(const CTRefCountPtr& aSrc);
    template <typename X> CTRefCountPtr& operator=(const CTRefCountPtr<X>& aSrc);
    CTRefCountPtr& operator=(T* pRefCount);
    template <typename X> CTRefCountPtr& operator=(X* pRefCount);

    operator T*(void)   const;
    T& operator*(void)  const;
    T* operator->(void) const;
    T* Get(void)        const;

    bool operator==(T* pRefCount) const;
    bool operator!=(T* pRefCount) const;
    bool operator==(const CTRefCountPtr& aSrc) const;
    template <typename X> bool operator==(const CTRefCountPtr<X>& aSrc) const;
    bool operator!=(const CTRefCountPtr& aSrc) const;
    template <typename X> bool operator!=(const CTRefCountPtr<X>& aSrc) const;

    template <typename X> CTRefCountPtr<X>& Cast(void);
private:
    T*   m_pRefCount;
};

///////////////////////////////////////////////////////////////////
template <typename T> class CTSharePtr;
template <typename T> class CTWeakPtr;
template <typename T> class CTScopePtr;
///////////////////////////////////////////////////////////////////
// CTSharePtr : smartpointer
template <typename T>
class CTSharePtr : public MObject
{
private:
    template <typename X> friend class CTSharePtr;
    template <typename X> friend class CTWeakPtr;
public:
    CTSharePtr(T* pObject = nullptr);
    ~CTSharePtr(void);
    CTSharePtr(const CTSharePtr& aSrc);
    template <typename X> CTSharePtr(const CTSharePtr<X>& aSrc);
    template <typename X> CTSharePtr(const CTWeakPtr<X>& aSrc);

    CTSharePtr& operator=(const CTSharePtr& aSrc);
    template <typename X> CTSharePtr& operator=(const CTSharePtr<X>& aSrc);
    template <typename X> CTSharePtr& operator=(const CTWeakPtr<X>& aSrc);
    CTSharePtr& operator=(T* pObject);

    operator T*(void)   const;
    T& operator*(void)  const;
    T* operator->(void) const;
    T* Get(void)        const;

    bool Check(void) const;
    bool operator==(T* pObject) const;
    bool operator!=(T* pObject) const;
    bool operator==(const CTSharePtr& aSrc) const;
    template <typename X> bool operator==(const CTSharePtr<X>& aSrc) const;
    bool operator!=(const CTSharePtr& aSrc) const;
    template <typename X> bool operator!=(const CTSharePtr<X>& aSrc) const;
private:
    T*              m_pObject;
    CSyncCounter*   m_pCounter;
};

///////////////////////////////////////////////////////////////////
// CTWeakPtr : smartpointer
template <typename T>
class CTWeakPtr : public MObject
{
private:
    template <typename X> friend class CTWeakPtr;
    template <typename X> friend class CTSharePtr;
public:
    CTWeakPtr(void);
    ~CTWeakPtr(void);
    CTWeakPtr(const CTWeakPtr& aSrc);
    template <typename X> CTWeakPtr(const CTWeakPtr<X>& aSrc);
    template <typename X> CTWeakPtr(const CTSharePtr<X>& aSrc);

    CTWeakPtr& operator=(const CTWeakPtr& aSrc);
    template <typename X> CTWeakPtr& operator=(const CTWeakPtr<X>& aSrc);
    template <typename X> CTWeakPtr& operator=(const CTSharePtr<X>& aSrc);

    operator T*(void)   const;
    T& operator*(void)  const;
    T* operator->(void) const;
    T* Get(void)        const;

    bool Check(void) const;
    bool operator==(T* pObject) const;
    bool operator!=(T* pObject) const;
    bool operator==(const CTWeakPtr& aSrc) const;
    template <typename X> bool operator==(const CTWeakPtr<X>& aSrc) const;
    bool operator!=(const CTWeakPtr& aSrc) const;
    template <typename X> bool operator!=(const CTWeakPtr<X>& aSrc) const;
private:
    T*              m_pObject;
    CSyncCounter*   m_pCounter;
};

///////////////////////////////////////////////////////////////////
// CTScopePtr : smartpointer
template <typename T>
class CTScopePtr : public MObject
{
public:
    CTScopePtr(T* pObject = nullptr, bool bManage = true);
    ~CTScopePtr(void);
    CTScopePtr& operator=(T* pObject); // bManage = true

#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTScopePtr(CTScopePtr&& aSrc);
    CTScopePtr& operator=(CTScopePtr&& aSrc);
#endif

    operator T*(void)   const;
    T& operator*(void)  const;
    T* operator->(void) const;
    T* Get(void)        const;

    bool operator==(T* pObject) const;
    bool operator!=(T* pObject) const;

    bool Attach(CTScopePtr& scope);
    void Detach(void);
private:
    CTScopePtr(const CTScopePtr& aSrc);
    CTScopePtr& operator=(const CTScopePtr& aSrc);
private:
    T*     m_pObject;
    bool   m_bManage;
};

///////////////////////////////////////////////////////////////////
#include "refcount.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __REF_COUNT_H__
