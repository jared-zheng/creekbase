// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#pragma once

#include "core.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CPlatform
class CComponent;
class CORECLASS CPlatform
{
public:
    enum OS_TYPE
    {
        OST_UNKNOWN = 0,
        OST_WINDOWS,
        OST_LINUX,
        OST_MAC,
        OST_IOS,
        OST_ANDROID,
    };

    enum OS_PAGESIZE
    {
        OSP_DEFAULT = 0x00001000, // 4K
    };

    typedef struct tagCPUSTAT
    {
        ULLong   ullUsed;
        ULLong   ullTotal;
    }CPUSTAT, *PCPUSTAT;

    typedef struct tagCPUINFO
    {
        UInt     uCores;
        XChar    szInfo[LMT_MIN];
    }CPUINFO, *PCPUINFO;

    typedef struct tagMEMINFO
    {
        UInt     uUsedPercent;  // 0 - 100
        UInt     uPageSize;     // KB
        UInt     uTotalPhys;    // MB
        UInt     uAvailPhys;    // MB
    }MEMINFO, *PMEMINFO;

    typedef struct tagOSINFO
    {
        UInt     uOSType;
        UInt     uMajor;
        UInt     uMinor;
        UInt     uBuild;
        XChar    szInfo[LMT_MAX_NAME];
    }OSINFO, *POSINFO;

    typedef struct tagTIMEINFO
    {
        UShort   usYear;
        UShort   usMonth;
        UShort   usWeekDay;
        UShort   usDay;
        UShort   usHour;
        UShort   usMinute;
        UShort   usSecond;
        UShort   usMSecond;
    }TIMEINFO, *PTIMEINFO;
public:
    static ULLong GetRuntimeConfig(void);  // defined in config.h
    static UInt   GetPageSize(void);
    static PId    GetCurrentPId(void);     // current process id
    static TId    GetCurrentTId(void);     // current thread  id
    static Int    SleepEx(UInt uSleep, bool bAlert = false); // MS
    static void   YieldEx(void);

    static UInt   CheckThread(UInt uCount, UInt uTimes = 1, UInt uMin = 1);
    static UInt   GetCPURate(CPUSTAT& cpus1, CPUSTAT& cpus2); // 0 - 10000
    static void   GetCPUStat(CPUSTAT& cpus);
    static void   GetCPUInfo(CPUINFO& cpui);

    static void   GetMemInfo(MEMINFO& mem);
    static void   GetOSInfo(OSINFO& os);
    static void   GetTimeInfo(TIMEINFO& ti, bool bLocal = true);
    static ULLong GetTimeStamp(void); // MS

    static size_t GetFullCmd(PXStr pszBuf, size_t stBufLen);
    static size_t GetPathInfo(PXStr pszBuf, size_t stBufLen, bool bFull = false);

    static PXStr  SetLocal(int nLocal = LC_ALL, PCXStr pszLanguage = nullptr);
    // ***windows special : only msvc supported
    static Int    SetExceptHandler(EXCEPT_HANDLER ExceptHandler);

    static LLong  GetRunningTime(void);   // MS
    static LLong  GetOSRunningTime(void); // MS
    static LLong  GetOSRunningTick(void);
    static LLong  GetOSBaseTick(void);
    static LLong  Tick2MilliSecond(LLong& llTick);

    static UShort ByteSwap(UShort usValue);
    static UInt   ByteSwap(UInt   uValue);
    static ULong  ByteSwap(ULong  ulValue);
    static ULLong ByteSwap(ULLong ullValue);
    // ---develop output only, max output string length=(8000)---
    static bool   DevelopPrint(CComponent* pComponent, UInt uLevel = LOGL_ALL); // AddRef & Release
    static void   DevelopPrint(UInt uLevel, PCXStr pszFormat, ...);
protected:
    CPlatform(void);
    ~CPlatform(void);
    CPlatform(const CPlatform&);
    CPlatform& operator=(const CPlatform&);
};

#ifdef __RUNTIME_DEBUG__
    #define DEV_MEMMG(...)    CPlatform::DevelopPrint(LOGL_MEMMG, __VA_ARGS__)
    #define DEV_TRACE(...)    CPlatform::DevelopPrint(LOGL_TRACE, __VA_ARGS__)
    #define DEV_DEBUG(...)    CPlatform::DevelopPrint(LOGL_DEBUG, __VA_ARGS__)
#else
    #define DEV_MEMMG(...)
    #define DEV_TRACE(...)
    #define DEV_DEBUG(...)
#endif
#define DEV_INFO(...)         CPlatform::DevelopPrint(LOGL_INFO,  __VA_ARGS__)
#define DEV_DUMP(...)         CPlatform::DevelopPrint(LOGL_DUMP,  __VA_ARGS__)
#define DEV_WARN(...)         CPlatform::DevelopPrint(LOGL_WARN,  __VA_ARGS__)
#define DEV_ERROR(...)        CPlatform::DevelopPrint(LOGL_ERROR, __VA_ARGS__)

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __PLATFORM_H__
