// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CONTAINER_INL__
#define __CONTAINER_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CTContainerAlloc
template <typename T>
INLINE CTContainerAlloc<T>::CTContainerAlloc(void)
: m_pThis(nullptr)
{
}

template <typename T>
INLINE CTContainerAlloc<T>::~CTContainerAlloc(void)
{
}

template <typename T>
INLINE CTContainerAlloc<T>::CTContainerAlloc(const CTContainerAlloc<T>&)
: m_pThis(nullptr)
{
}

template <typename T>
INLINE CTContainerAlloc<T>& CTContainerAlloc<T>::operator=(const CTContainerAlloc<T>&)
{
    return (*this);
}

template <typename T>
INLINE T* CTContainerAlloc<T>::Create(CTContainerAlloc<T>*& pThis, size_t stElements)
{
    assert(stElements > 0);
    if (stElements > 0)
    {
        CTContainerAlloc<T>* pAlloc = reinterpret_cast<CTContainerAlloc<T>*>( ALLOC( sizeof(CTContainerAlloc<T>) + stElements * sizeof(T) ) );
        if (pAlloc != nullptr)
        {
            pAlloc->m_pThis = pThis;
            pThis = pAlloc;

            return (reinterpret_cast<T*>(pAlloc + 1));
        }
    }
    return nullptr;
}

template <typename T>
INLINE T* CTContainerAlloc<T>::GetData(void)
{
    return (reinterpret_cast<T*>(this + 1));
}

template <typename T>
INLINE void CTContainerAlloc<T>::Destroy(void)
{
    CTContainerAlloc<T>* pAlloc = nullptr;
    for (CTContainerAlloc<T>* pThis = this; pThis != nullptr; pThis = pAlloc)
    {
        pAlloc = pThis->m_pThis;
        FREE( pThis );
    }
}

///////////////////////////////////////////////////////////////////
// CContainerTraits

///////////////////////////////////////////////////////////////////
// CTArray
template <typename T, typename Traits>
INLINE CTArray<T, Traits>::CTArray(Int nGrow)
: m_paT(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(0)
{
    SetGrow(nGrow);
}

template <typename T, typename Traits>
INLINE CTArray<T, Traits>::~CTArray(void)
{
    RemoveAll();
}

template <typename T, typename Traits>
INLINE CTArray<T, Traits>::CTArray(const CTArray<T, Traits>& aSrc)
: m_paT(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(aSrc.m_nGrow)
{
    Copy(aSrc);
}

template <typename T, typename Traits>
INLINE CTArray<T, Traits>& CTArray<T, Traits>::operator=(const CTArray<T, Traits>& aSrc)
{
    Copy(aSrc);
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE CTArray<T, Traits>::CTArray(CTArray<T, Traits>&& aSrc)
: m_paT(aSrc.m_paT)
, m_nAllocSize(aSrc.m_nAllocSize)
, m_nSize(aSrc.m_nSize)
, m_nGrow(aSrc.m_nGrow)
{
    aSrc.m_paT        = nullptr;
    aSrc.m_nAllocSize = 0;
    aSrc.m_nSize      = 0;
    //aSrc.m_nGrow      = 0;
}

template <typename T, typename Traits>
INLINE CTArray<T, Traits>& CTArray<T, Traits>::operator=(CTArray<T, Traits>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll();
        m_paT             = aSrc.m_paT;
        m_nAllocSize      = aSrc.m_nAllocSize;
        m_nSize           = aSrc.m_nSize;
        m_nGrow           = aSrc.m_nGrow;
        aSrc.m_paT        = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        //aSrc.m_nGrow      = 0;
    }
    return (*this);
}
#endif
template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::GetAllocSize(void) const
{
    return m_nAllocSize;
}

template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::GetSize(void) const
{
    return m_nSize;
}

template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::GetGrow(void) const
{
    return m_nGrow;
}

template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::SetGrow(Int nGrow)
{
    assert(nGrow > 0);
    nGrow   = DEF::Maxmin<Int>(nGrow, SIZED_MIN, SIZED_MAX);
    m_nGrow = DEF::Align<Int>(nGrow, SIZED_GROW);
}

template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::Add(INARGTYPE t)
{
    if (SetCount())
    {
        SetAtIndex(m_nSize, t);
        ++m_nSize;
        return (m_nSize - 1);
    }
    return -1;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::Add(RVARGTYPE t)
{
    if (SetCount())
    {
        SetAtIndex(m_nSize, std::move(t));
        ++m_nSize;
        return (m_nSize - 1);
    }
    return -1;
}
#endif
template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::Add(Int nCount, bool bZeroed)
{
    assert(nCount > 0);
    if (SetCount(nCount, bZeroed))
    {
        for (Int i = 0; i < nCount; ++i)
        {
            SetAtIndex(m_nSize + i);
        }
        Int nRet = m_nSize;
        m_nSize += nCount;
        return nRet;
    }
    return -1;
}

template <typename T, typename Traits>
INLINE Int CTArray<T, Traits>::Find(INARGTYPE t) const
{
    for (Int i = 0; i < m_nSize; ++i)
    {
        if (Traits::CompareElements(m_paT[i], t))
        {
            return i;
        }
    }
    return -1;  // not found
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Remove(INARGTYPE t)
{
    Int nIndex = Find(t);
    if (nIndex == -1)
    {
        return false;
    }
    return RemoveAt(nIndex);
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::RemoveAt(Int nIndex)
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    if (Check(nIndex) == false)
    {
        return false;
    }
    m_paT[nIndex].~T();
    if (nIndex < (m_nSize - 1))
    {
        Traits::RelocateElements((m_paT + nIndex), (m_paT + nIndex + 1), (m_nSize - (nIndex + 1)));
    }
    --m_nSize;
    return true;
}

template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::RemoveAll(void)
{
    if (m_paT != nullptr)
    {
        for (Int i = 0; i < m_nSize; ++i)
        {
            m_paT[i].~T();
        }
        FREE( m_paT );
        m_paT = nullptr;
    }
    m_nAllocSize = 0;
    m_nSize = 0;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Check(Int nIndex) const
{
    return ((nIndex >= 0) && (nIndex < m_nSize));
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Insert(Int nIndex, Int nCount, bool bZeroed)
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    assert(nCount > 0);
    if (Check(nIndex) && SetCount(nCount))
    {
        Traits::RelocateElements((m_paT + nIndex + nCount), (m_paT + nIndex), (m_nSize - nIndex));
        if (bZeroed)
        {
            MM_SAFE::Set((m_paT + nIndex), 0, nCount * sizeof(T));
        }
        m_nSize += nCount;
        return true;
    }
    return false;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::SetAt(Int nIndex, INARGTYPE t)
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    if (Check(nIndex) == false)
    {
        return false;
    }
    SetAtIndex(nIndex, t);
    return true;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::SetAt(Int nIndex, RVARGTYPE t)
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    if (Check(nIndex) == false)
    {
        return false;
    }
    SetAtIndex(nIndex, std::move(t));
    return true;
}
#endif
template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Shrink(void)
{
    Int nNewCount = DEF::Align<Int>((m_nSize + m_nGrow), m_nGrow);
    if (nNewCount < m_nAllocSize)
    {
        T* paT = reinterpret_cast<T*>( ALLOC( nNewCount * sizeof(T) ) );
        if (paT == nullptr)
        {
            return false;
        }
        Traits::RelocateElements(paT, m_paT, m_nSize);
        FREE( m_paT );
        m_paT = paT;
        m_nAllocSize = nNewCount;
        return true;
    }
    return false;
}

template <typename T, typename Traits>
INLINE T& CTArray<T, Traits>::operator[](Int nIndex)
{
    assert((nIndex >= 0) && (nIndex < m_nSize));
    if (Check(nIndex) == false)
    {
        throw (E_ARRAY_BOUNDS_EXCEEDED);
    }
    return m_paT[nIndex];
}

template <typename T, typename Traits>
INLINE const T& CTArray<T, Traits>::operator[](Int nIndex) const
{
    assert((nIndex >= 0) && (nIndex < m_nSize));
    if (Check(nIndex) == false)
    {
        throw (E_ARRAY_BOUNDS_EXCEEDED);
    }
    return m_paT[nIndex];
}

template <typename T, typename Traits>
INLINE const T* CTArray<T, Traits>::GetData(void) const
{
    return m_paT;
}

template <typename T, typename Traits>
INLINE T* CTArray<T, Traits>::GetItem(Int nIndex)
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    if (Check(nIndex))
    {
        return (m_paT + nIndex);
    }
    return nullptr;
}

template <typename T, typename Traits>
INLINE const T* CTArray<T, Traits>::GetItem(Int nIndex) const
{
    assert(nIndex >= 0 && nIndex < m_nSize);
    if (Check(nIndex))
    {
        return (m_paT + nIndex);
    }
    return nullptr;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Append(const CTArray<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        Int j = aSrc.GetSize();
        if (SetCount(j))
        {
            for (Int i = 0; i < j; ++i)
            {
                SetAtIndex(m_nSize + i, aSrc.m_paT[i]);
            }
            m_nSize += j;
            return true;
        }
    }
    return false;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Copy(const CTArray<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll(); // only empty array can copy
        Int j = aSrc.GetSize();
        if (SetCount(j))
        {
            for (Int i = 0; i < j; ++i)
            {
                SetAtIndex(m_nSize + i, aSrc.m_paT[i]);
            }
            m_nSize += j;
            return true;
        }
    }
    return false;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::Attach(CTArray<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll(); // only empty array can copy
        m_paT        = aSrc.m_paT;
        m_nAllocSize = aSrc.m_nAllocSize;
        m_nSize      = aSrc.m_nSize;
        m_nGrow      = aSrc.m_nGrow;
        aSrc.m_paT        = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        return true;
    }
    return false;
}

template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::Detach(void)
{
    m_paT        = nullptr;
    m_nAllocSize = 0;
    m_nSize      = 0;
}

template <typename T, typename Traits>
INLINE bool CTArray<T, Traits>::SetCount(Int nNewCount, bool bZeroed)
{
    if (nNewCount <= 0)
    {
        return false;
    }
    nNewCount += m_nSize;

    if (m_paT == nullptr)
    {
        assert(m_nSize == 0);
        nNewCount = DEF::Align<Int>(nNewCount, m_nGrow);
        m_paT = reinterpret_cast<T*>( ALLOC( nNewCount * sizeof(T) ) );
        if (m_paT == nullptr)
        {
            return false;
        }
        if (bZeroed)
        {
            MM_SAFE::Set(m_paT, 0, nNewCount * sizeof(T));
        }
        m_nAllocSize = nNewCount;
    }
    else if (nNewCount > m_nAllocSize)
    {
        nNewCount = DEF::Align<Int>(nNewCount, m_nGrow);
        T* paT = reinterpret_cast<T*>( ALLOC( nNewCount * sizeof(T) ) );
        if (paT == nullptr)
        {
            return false;
        }
        if (bZeroed)
        {
            MM_SAFE::Set(paT, 0, nNewCount * sizeof(T));
        }
        Traits::RelocateElements(paT, m_paT, m_nSize);
        FREE( m_paT );
        m_paT = paT;
        m_nAllocSize = nNewCount;
    }
    assert(m_paT != nullptr);
    return true;
}

#pragma push_macro("new")
#undef new
template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::SetAtIndex(Int nIndex, INARGTYPE t)
{
    GNEW(m_paT + nIndex) T(t);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::SetAtIndex(Int nIndex, RVARGTYPE t)
{
    GNEW(m_paT + nIndex) T(std::move(t));
}
#endif

template <typename T, typename Traits>
INLINE void CTArray<T, Traits>::SetAtIndex(Int nIndex)
{
    GNEW(m_paT + nIndex) T();
}
#pragma pop_macro("new")

///////////////////////////////////////////////////////////////////
// CTList
template <typename T, typename Traits>
INLINE CTList<T, Traits>::CTList(Int nGrow)
: m_pHead(nullptr)
, m_pTail(nullptr)
, m_pFree(nullptr)
, m_pAlloc(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(0)
{
    SetGrow(nGrow);
}

template <typename T, typename Traits>
INLINE CTList<T, Traits>::~CTList(void)
{
    RemoveAll();
}

template <typename T, typename Traits>
INLINE CTList<T, Traits>::CTList(const CTList<T, Traits>& aSrc)
: m_pHead(nullptr)
, m_pTail(nullptr)
, m_pFree(nullptr)
, m_pAlloc(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(aSrc.m_nGrow)
{
    AddHeadList(aSrc);
}

template <typename T, typename Traits>
INLINE CTList<T, Traits>& CTList<T, Traits>::operator=(const CTList<T, Traits>& aSrc)
{
    AddHeadList(aSrc);
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE CTList<T, Traits>::CTList(CTList<T, Traits>&& aSrc)
: m_pHead(aSrc.m_pHead)
, m_pTail(aSrc.m_pTail)
, m_pFree(aSrc.m_pFree)
, m_pAlloc(aSrc.m_pAlloc)
, m_nAllocSize(aSrc.m_nAllocSize)
, m_nSize(aSrc.m_nSize)
, m_nGrow(aSrc.m_nGrow)
{
    aSrc.m_pHead      = nullptr;
    aSrc.m_pTail      = nullptr;
    aSrc.m_pFree      = nullptr;
    aSrc.m_pShare     = nullptr;
    aSrc.m_pAlloc     = nullptr;
    aSrc.m_nAllocSize = 0;
    aSrc.m_nSize      = 0;
    //aSrc.m_nGrow      = 0;
}

template <typename T, typename Traits>
INLINE CTList<T, Traits>& CTList<T, Traits>::operator=(CTList<T, Traits>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll();
        m_pHead           = aSrc.m_pHead;
        m_pTail           = aSrc.m_pTail;
        m_pFree           = aSrc.m_pFree;
        m_pAlloc          = aSrc.m_pAlloc;
        m_nAllocSize      = aSrc.m_nAllocSize;
        m_nSize           = aSrc.m_nSize;
        m_nGrow           = aSrc.m_nGrow;
        aSrc.m_pHead      = nullptr;
        aSrc.m_pTail      = nullptr;
        aSrc.m_pFree      = nullptr;
        aSrc.m_pAlloc     = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        //aSrc.m_nGrow      = 0;
    }
    return (*this);
}
#endif
template <typename T, typename Traits>
INLINE Int CTList<T, Traits>::GetAllocSize(void) const
{
    return m_nAllocSize;
}

template <typename T, typename Traits>
INLINE Int CTList<T, Traits>::GetSize(void) const
{
    return m_nSize;
}

template <typename T, typename Traits>
INLINE Int CTList<T, Traits>::GetGrow(void) const
{
    return m_nGrow;
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::SetGrow(Int nGrow)
{
    assert(nGrow > 0);
    nGrow   = DEF::Maxmin<Int>(nGrow, SIZED_MIN, SIZED_MAX);
    m_nGrow = DEF::Align<Int>(nGrow, SIZED_GROW);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddHead(void)
{
    NODE* pNode = NewNode(nullptr, m_pHead);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pHead != nullptr)
    {
        m_pHead->m_pPrev = pNode;
    }
    else
    {
        m_pTail = pNode;
    }
    m_pHead = pNode;

    return ((PINDEX)pNode);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddHead(INARGTYPE t)
{
    NODE* pNode = NewNode(t, nullptr, m_pHead);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pHead != nullptr)
    {
        m_pHead->m_pPrev = pNode;
    }
    else
    {
        m_pTail = pNode;
    }
    m_pHead = pNode;

    return ((PINDEX)pNode);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddHead(RVARGTYPE t)
{
    NODE* pNode = NewNode(std::move(t), nullptr, m_pHead);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pHead != nullptr)
    {
        m_pHead->m_pPrev = pNode;
    }
    else
    {
        m_pTail = pNode;
    }
    m_pHead = pNode;

    return ((PINDEX)pNode);
}
#endif
template <typename T, typename Traits>
INLINE void CTList<T, Traits>::AddHeadList(const CTList<T, Traits>& aSrc)
{
    if (this == &aSrc)
    {
        return;
    }
    PINDEX p = aSrc.GetTailIndex();
    while (p != nullptr)
    {
        INARGTYPE t = aSrc.GetPrev(p);
        AddHead(t);
    }
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddTail(void)
{
    NODE* pNode = NewNode(m_pTail, nullptr);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pTail != nullptr)
    {
        m_pTail->m_pNext = pNode;
    }
    else
    {
        m_pHead = pNode;
    }
    m_pTail = pNode;

    return ((PINDEX)pNode);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddTail(INARGTYPE t)
{
    NODE* pNode = NewNode(t, m_pTail, nullptr);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pTail != nullptr)
    {
        m_pTail->m_pNext = pNode;
    }
    else
    {
        m_pHead = pNode;
    }
    m_pTail = pNode;

    return ((PINDEX)pNode);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::AddTail(RVARGTYPE t)
{
    NODE* pNode = NewNode(std::move(t), m_pTail, nullptr);
    if (pNode == nullptr)
    {
        return nullptr;
    }

    if (m_pTail != nullptr)
    {
        m_pTail->m_pNext = pNode;
    }
    else
    {
        m_pHead = pNode;
    }
    m_pTail = pNode;

    return ((PINDEX)pNode);
}
#endif
template <typename T, typename Traits>
INLINE void CTList<T, Traits>::AddTailList(const CTList<T, Traits>& aSrc)
{
    if (this == &aSrc)
    {
        return;
    }
    PINDEX p = aSrc.GetHeadIndex();
    while (p != nullptr)
    {
        INARGTYPE t = aSrc.GetNext(p);
        AddTail(t);
    }
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::Attach(CTList<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this == &aSrc)
    {
        RemoveAll();
        m_pHead           = aSrc.m_pHead;
        m_pTail           = aSrc.m_pTail;
        m_pFree           = aSrc.m_pFree;
        m_pAlloc          = aSrc.m_pAlloc;
        m_nAllocSize      = aSrc.m_nAllocSize;
        m_nSize           = aSrc.m_nSize;
        m_nGrow           = aSrc.m_nGrow;
        aSrc.m_pHead      = nullptr;
        aSrc.m_pTail      = nullptr;
        aSrc.m_pFree      = nullptr;
        aSrc.m_pAlloc     = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        return true;
    }
    return false;
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::Detach(void)
{
    m_pHead      = nullptr;
    m_pTail      = nullptr;
    m_pFree      = nullptr;
    m_pAlloc     = nullptr;
    m_nAllocSize = 0;
    m_nSize      = 0;
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::Find(INARGTYPE t, PINDEX pStartAfter) const
{
    NODE* pNode = static_cast<NODE*>(pStartAfter);
    if (pNode == nullptr)
    {
        pNode = m_pHead;  // start at head
    }
    else
    {
        pNode = pNode->m_pNext;  // start after the one specified
    }

    for (; pNode != nullptr; pNode = pNode->m_pNext)
    {
        if (Traits::CompareElements(pNode->m_T, t))
        {
            return ((PINDEX)pNode);
        }
    }
    return nullptr;
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::FindIndex(Int nIndex) const
{
    if (nIndex >= m_nSize)
    {
        return nullptr;
    }
    NODE* pNode = m_pHead;
    for (Int i = 0; i < nIndex; ++i)
    {
        pNode = pNode->m_pNext;
    }
    return ((PINDEX)pNode);
}

template <typename T, typename Traits>
INLINE T CTList<T, Traits>::RemoveHead(void)
{
    assert(m_pHead != nullptr);
    if (m_pHead == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = m_pHead;
    T t(pNode->m_T);

    m_pHead = pNode->m_pNext;
    if (m_pHead != nullptr)
    {
        m_pHead->m_pPrev = nullptr;
    }
    else
    {
        m_pTail = nullptr;
    }
    FreeNode(pNode);
    return t;
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::RemoveHeadDirect(void)
{
    assert(m_pHead != nullptr);
    NODE* pNode = m_pHead;
    if (pNode != nullptr)
    {
        m_pHead = pNode->m_pNext;
        if (m_pHead != nullptr)
        {
            m_pHead->m_pPrev = nullptr;
        }
        else
        {
            m_pTail = nullptr;
        }
        FreeNode(pNode);
    }
}

template <typename T, typename Traits>
INLINE T CTList<T, Traits>::RemoveTail(void)
{
    assert(m_pTail != nullptr);
    if (m_pTail == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = m_pTail;
    T t(pNode->m_T);

    m_pTail = pNode->m_pPrev;
    if (m_pTail != nullptr)
    {
        m_pTail->m_pNext = nullptr;
    }
    else
    {
        m_pHead = nullptr;
    }
    FreeNode(pNode);
    return t;
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::RemoveTailDirect(void)
{
    assert(m_pTail != nullptr);
    NODE* pNode = m_pTail;
    if (pNode != nullptr)
    {
        m_pTail = pNode->m_pPrev;
        if (m_pTail != nullptr)
        {
            m_pTail->m_pNext = nullptr;
        }
        else
        {
            m_pHead = nullptr;
        }
        FreeNode(pNode);
    }
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::Remove(INARGTYPE t)
{
    PINDEX index = Find(t);
    if (index != nullptr)
    {
        return RemoveAt(index);
    }
    return false;
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::RemoveAt(PINDEX index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode == nullptr)
    {
        return false;
    }
    // remove pNode from list
    if (pNode == m_pHead)
    {
        m_pHead = pNode->m_pNext;
    }
    else
    {
        pNode->m_pPrev->m_pNext = pNode->m_pNext;
    }

    if (pNode == m_pTail)
    {
        m_pTail = pNode->m_pPrev;
    }
    else
    {
        pNode->m_pNext->m_pPrev = pNode->m_pPrev;
    }
    FreeNode(pNode);
    return true;
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::RemoveAll(void)
{
    for (NODE* p = m_pHead; p != nullptr; p = m_pHead)
    {
        m_pHead = p->m_pNext;
        p->~NODE();
    }
    if (m_pAlloc != nullptr)
    {
        m_pAlloc->Destroy();
        m_pAlloc = nullptr;
    }
    m_pHead = nullptr;
    m_pTail = nullptr;
    m_pFree = nullptr;
    m_nAllocSize = 0;
    m_nSize = 0;
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::Check(PINDEX index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if ((pNode == nullptr) ||
        (pNode->m_pPrev == reinterpret_cast<NODE*>(-1))) // free flag -1
    {
        return false;
    }
    return true;
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::InsertBefore(PINDEX index, INARGTYPE t)
{
    if (index == nullptr)
    {
        return AddHead(t);
    }
    // Insert it before position
    NODE* pOldNode = static_cast<NODE*>(index);
    NODE* pNewNode = NewNode(t, pOldNode->m_pPrev, pOldNode);

    if (pOldNode->m_pPrev != nullptr)
    {
        pOldNode->m_pPrev->m_pNext = pNewNode;
    }
    else
    {
        assert(pOldNode == m_pHead);
        m_pHead = pNewNode;
    }
    pOldNode->m_pPrev = pNewNode;

    return ((PINDEX)pNewNode);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::InsertAfter(PINDEX index, INARGTYPE t)
{
    if (index == nullptr)
    {
        return AddTail(t);
    }
    // Insert it after position
    NODE* pOldNode = static_cast<NODE*>(index);
    NODE* pNewNode = NewNode(t, pOldNode, pOldNode->m_pNext);

    if (pOldNode->m_pNext != nullptr)
    {
        pOldNode->m_pNext->m_pPrev = pNewNode;
    }
    else
    {
        assert(pOldNode == m_pTail);
        m_pTail = pNewNode;
    }
    pOldNode->m_pNext = pNewNode;

    return ((PINDEX)pNewNode);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::InsertBefore(PINDEX index, RVARGTYPE t)
{
    if (index == nullptr)
    {
        return AddHead(std::move(t));
    }
    // Insert it before position
    NODE* pOldNode = static_cast<NODE*>(index);
    NODE* pNewNode = NewNode(std::move(t), pOldNode->m_pPrev, pOldNode);

    if (pOldNode->m_pPrev != nullptr)
    {
        pOldNode->m_pPrev->m_pNext = pNewNode;
    }
    else
    {
        assert(pOldNode == m_pHead);
        m_pHead = pNewNode;
    }
    pOldNode->m_pPrev = pNewNode;

    return ((PINDEX)pNewNode);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::InsertAfter(PINDEX index, RVARGTYPE t)
{
    if (index == nullptr)
    {
        return AddTail(std::move(t));
    }
    // Insert it after position
    NODE* pOldNode = static_cast<NODE*>(index);
    NODE* pNewNode = NewNode(std::move(t), pOldNode, pOldNode->m_pNext);

    if (pOldNode->m_pNext != nullptr)
    {
        pOldNode->m_pNext->m_pPrev = pNewNode;
    }
    else
    {
        assert(pOldNode == m_pTail);
        m_pTail = pNewNode;
    }
    pOldNode->m_pNext = pNewNode;

    return ((PINDEX)pNewNode);
}
#endif
template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::MoveToHead(PINDEX index)
{
    assert(index != nullptr);
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode == nullptr)
    {
        return false;
    }
    if (pNode != m_pHead)
    {
        if (pNode->m_pNext == nullptr)
        {
            assert(pNode == m_pTail);
            m_pTail = pNode->m_pPrev;
        }
        else
        {
            pNode->m_pNext->m_pPrev = pNode->m_pPrev;
        }
        assert(pNode->m_pPrev != nullptr);
        pNode->m_pPrev->m_pNext = pNode->m_pNext;

        m_pHead->m_pPrev = pNode;
        pNode->m_pNext = m_pHead;
        pNode->m_pPrev = nullptr;
        m_pHead = pNode;
    }
    return true;
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::MoveToTail(PINDEX index)
{
    assert(index != nullptr);
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode == nullptr)
    {
        return false;
    }
    if (pNode != m_pTail)
    {
        if (pNode->m_pPrev == nullptr)
        {
            assert(pNode == m_pHead);
            m_pHead = pNode->m_pNext;
        }
        else
        {
            pNode->m_pPrev->m_pNext = pNode->m_pNext;
        }
        assert(pNode->m_pNext != nullptr);
        pNode->m_pNext->m_pPrev = pNode->m_pPrev;

        m_pTail->m_pNext = pNode;
        pNode->m_pPrev = m_pTail;
        pNode->m_pNext = nullptr;
        m_pTail = pNode;
    }
    return true;
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::Swap(PINDEX index1, PINDEX index2)
{
    assert(index1 != nullptr);
    assert(index2 != nullptr);
    if ((index1 == nullptr) ||
        (index2 == nullptr) ||
        (index1 == index2))
    {
        return false;
    }
    NODE* pNode1 = static_cast<NODE*>(index1);
    NODE* pNode2 = static_cast<NODE*>(index2);
    if (pNode2->m_pNext == pNode1)
    {
        // Swap pNode2 and pNode1 so that the next case works
        NODE* pTemp = pNode1;
        pNode1 = pNode2;
        pNode2 = pTemp;
    }
    if (pNode1->m_pNext == pNode2)
    {
        // Node1 and Node2 are adjacent
        pNode2->m_pPrev = pNode1->m_pPrev;
        if (pNode1->m_pPrev != nullptr)
        {
            pNode1->m_pPrev->m_pNext = pNode2;
        }
        else
        {
            assert(m_pHead == pNode1);
            m_pHead = pNode2;
        }
        pNode1->m_pNext = pNode2->m_pNext;
        if (pNode2->m_pNext != nullptr)
        {
            pNode2->m_pNext->m_pPrev = pNode1;
        }
        else
        {
            assert(m_pTail == pNode2);
            m_pTail = pNode1;
        }
        pNode2->m_pNext = pNode1;
        pNode1->m_pPrev = pNode2;
    }
    else
    {
        // The two nodes are not adjacent
        NODE* pTemp = pNode1->m_pPrev;
        pNode1->m_pPrev = pNode2->m_pPrev;
        pNode2->m_pPrev = pTemp;

        pTemp = pNode1->m_pNext;
        pNode1->m_pNext = pNode2->m_pNext;
        pNode2->m_pNext = pTemp;

        if (pNode1->m_pNext != nullptr)
        {
            pNode1->m_pNext->m_pPrev = pNode1;
        }
        else
        {
            assert(m_pTail == pNode2);
            m_pTail = pNode1;
        }
        if (pNode1->m_pPrev != nullptr)
        {
            pNode1->m_pPrev->m_pNext = pNode1;
        }
        else
        {
            assert(m_pHead == pNode2);
            m_pHead = pNode1;
        }
        if (pNode2->m_pNext != nullptr)
        {
            pNode2->m_pNext->m_pPrev = pNode2;
        }
        else
        {
            assert(m_pTail == pNode1);
            m_pTail = pNode2;
        }
        if (pNode2->m_pPrev != nullptr)
        {
            pNode2->m_pPrev->m_pNext = pNode2;
        }
        else
        {
            assert( m_pHead == pNode1 );
            m_pHead = pNode2;
        }
    }
    return true;
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::SetAt(PINDEX index, INARGTYPE t)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        return false;
    }
    NODE* pNode = static_cast<NODE*>(index);
    pNode->m_T = t;
    return true;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::SetAt(PINDEX index, RVARGTYPE t)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        return false;
    }
    NODE* pNode = static_cast<NODE*>(index);
    pNode->m_T  = std::move(t);
    return true;
}
#endif
template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::operator[](PINDEX index)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = static_cast<NODE*>(index);
    return (pNode->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::operator[](PINDEX index) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = static_cast<NODE*>(index);
    return (pNode->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetAt(PINDEX index)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = static_cast<NODE*>(index);
    return (pNode->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetAt(PINDEX index) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* pNode = static_cast<NODE*>(index);
    return (pNode->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetHead(void)
{
    assert(m_pHead != nullptr);
    if (m_pHead == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    return (m_pHead->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetHead(void) const
{
    assert(m_pHead != nullptr);
    if (m_pHead == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    return (m_pHead->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetHead(PINDEX& index)
{
    assert(m_pHead != nullptr);
    if (m_pHead == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    index = (PINDEX)m_pHead;
    return (m_pHead->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetHead(PINDEX& index) const
{
    assert(m_pHead != nullptr);
    if (m_pHead == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    index = (PINDEX)m_pHead;
    return (m_pHead->m_T);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::GetHeadIndex(void) const
{
    return ((PINDEX)m_pHead);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetTail(void)
{
    assert(m_pTail != nullptr);
    if (m_pTail == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    return (m_pTail->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetTail(void) const
{
    assert(m_pTail != nullptr);
    if (m_pTail == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    return (m_pTail->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetTail(PINDEX& index)
{
    assert(m_pTail != nullptr);
    if (m_pTail == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    index = (PINDEX)m_pTail;
    return (m_pTail->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetTail(PINDEX& index) const
{
    assert(m_pTail != nullptr);
    if (m_pTail == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    index = (PINDEX)m_pTail;
    return (m_pTail->m_T);
}

template <typename T, typename Traits>
INLINE PINDEX CTList<T, Traits>::GetTailIndex(void) const
{
    return ((PINDEX)m_pTail);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetNext(PINDEX& index)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    index = PINDEX(p->m_pNext);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetNext(PINDEX& index) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    index = PINDEX(p->m_pNext);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetPrev(PINDEX& index)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    index = PINDEX(p->m_pPrev);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetPrev(PINDEX& index) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    index = PINDEX(p->m_pPrev);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetNext(PINDEX& index, PINDEX& indexT)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    indexT = index;
    index  = PINDEX(p->m_pNext);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetNext(PINDEX& index, PINDEX& indexT) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    indexT = index;
    index  = PINDEX(p->m_pNext);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE T& CTList<T, Traits>::GetPrev(PINDEX& index, PINDEX& indexT)
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    indexT = index;
    index  = PINDEX(p->m_pPrev);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE const T& CTList<T, Traits>::GetPrev(PINDEX& index, PINDEX& indexT) const
{
    assert(index != nullptr);
    if (index == nullptr)
    {
        throw (E_ACCESS_VIOLATION);
    }
    NODE* p = static_cast<NODE*>(index);
    indexT = index;
    index  = PINDEX(p->m_pPrev);
    return (p->m_T);
}

template <typename T, typename Traits>
INLINE void CTList<T, Traits>::FreeNode(NODE* pNode)
{
    pNode->~NODE();
    pNode->m_pNext = m_pFree;
    pNode->m_pPrev = reinterpret_cast<NODE*>(-1);// free node flag
    m_pFree = pNode;
    assert(m_nSize > 0);
    --m_nSize;
    if (m_nSize == 0)
    {
        RemoveAll();
    }
}

template <typename T, typename Traits>
INLINE bool CTList<T, Traits>::GetFreeNode(void)
{
    if (m_pFree == nullptr)
    {
        NODE* pNode = CTContainerAlloc<NODE>::Create(m_pAlloc, (size_t)m_nGrow);
        if (pNode == nullptr)
        {
            throw (E_OUTOFMEMORY);
        }
        pNode += (size_t)((size_t)m_nGrow - 1);
        for (Int i = m_nGrow - 1; i >= 0; --i)
        {
            pNode->m_pNext = m_pFree;
            pNode->m_pPrev = reinterpret_cast<NODE*>(-1);// free node flag
            m_pFree = pNode;
            --pNode;
        }
        m_nAllocSize += m_nGrow;
    }
    return (m_pFree != nullptr);
}

#pragma push_macro("new")
#undef new
template <typename T, typename Traits>
INLINE typename CTList<T, Traits>::NODE* CTList<T, Traits>::NewNode(NODE* pPrev, NODE* pNext)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_pPrev == reinterpret_cast<NODE*>(-1)); // free flag -1

        GNEW(pNewNode) NODE(pPrev, pNext);

        ++m_nSize;
        assert(m_nSize > 0);

        return pNewNode;
    }
    return nullptr;
}

template <typename T, typename Traits>
INLINE typename CTList<T, Traits>::NODE* CTList<T, Traits>::NewNode(INARGTYPE t, NODE* pPrev, NODE* pNext)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_pPrev == reinterpret_cast<NODE*>(-1)); // free flag -1

        GNEW(pNewNode) NODE(t, pPrev, pNext);

        ++m_nSize;
        assert(m_nSize > 0);

        return pNewNode;
    }
    return nullptr;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE typename CTList<T, Traits>::NODE* CTList<T, Traits>::NewNode(RVARGTYPE t, NODE* pPrev, NODE* pNext)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_pPrev == reinterpret_cast<NODE*>(-1)); // free flag -1

        GNEW(pNewNode) NODE(std::move(t), pPrev, pNext);

        ++m_nSize;
        assert(m_nSize > 0);

        return pNewNode;
    }
    return nullptr;

}
#endif
#pragma pop_macro("new")

///////////////////////////////////////////////////////////////////
// CTQueue
template <typename T, typename Traits>
INLINE CTQueue<T, Traits>::CTQueue(Int nGrow)
: m_List(nGrow)
{
}

template <typename T, typename Traits>
INLINE CTQueue<T, Traits>::~CTQueue(void)
{
}

template <typename T, typename Traits>
INLINE CTQueue<T, Traits>::CTQueue(const CTQueue<T, Traits>& aSrc)
: m_List(aSrc.m_List)
{
}

template <typename T, typename Traits>
INLINE CTQueue<T, Traits>& CTQueue<T, Traits>::operator=(const CTQueue<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        m_List = aSrc.m_List;
    }
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE CTQueue<T, Traits>::CTQueue(CTQueue<T, Traits>&& aSrc)
: m_List(std::move(aSrc.m_List))
{
}

template <typename T, typename Traits>
INLINE CTQueue<T, Traits>& CTQueue<T, Traits>::operator=(CTQueue<T, Traits>&& aSrc)
{
    if (this != &aSrc)
    {
        m_List = std::move(aSrc.m_List);
    }
    return (*this);
}
#endif
template <typename T, typename Traits>
INLINE Int CTQueue<T, Traits>::GetSize(void) const
{
    return m_List.GetSize();
}

template <typename T, typename Traits>
INLINE void CTQueue<T, Traits>::SetGrow(Int nGrow)
{
    m_List.SetGrow(nGrow);
}

template <typename T, typename Traits>
INLINE bool CTQueue<T, Traits>::IsEmpty(void) const
{
    return (m_List.GetSize() == 0);
}

template <typename T, typename Traits>
INLINE bool CTQueue<T, Traits>::IsFull(void) const
{
    return false;
}

template <typename T, typename Traits>
INLINE bool CTQueue<T, Traits>::In(INARGTYPE t)
{
    if (IsFull() == false)
    {
        return (m_List.AddTail(t) != nullptr);
    }
    return false;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE bool CTQueue<T, Traits>::In(RVARGTYPE t)
{
    if (IsFull() == false)
    {
        return (m_List.AddTail(std::move(t)) != nullptr);
    }
    return false;
}
#endif
template <typename T, typename Traits>
INLINE T CTQueue<T, Traits>::Out(void)
{
    if (IsEmpty() == false)
    {
        return m_List.RemoveHead();
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename T, typename Traits>
INLINE void CTQueue<T, Traits>::RemoveAll(void)
{
    m_List.RemoveAll();
}

template <typename T, typename Traits>
INLINE T& CTQueue<T, Traits>::GetItem(void)
{
    return m_List.GetHead();
}

template <typename T, typename Traits>
INLINE const T& CTQueue<T, Traits>::GetItem(void) const
{
    return m_List.GetHead();
}

template <typename T, typename Traits>
INLINE bool CTQueue<T, Traits>::Attach(CTQueue& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        return m_List.Attach(aSrc.m_List);
    }
    return false;
}

template <typename T, typename Traits>
INLINE void CTQueue<T, Traits>::Detach(void)
{
    m_List.Detach();
}

///////////////////////////////////////////////////////////////////
// CTStack
template <typename T, typename Traits>
INLINE CTStack<T, Traits>::CTStack(Int nGrow)
: m_List(nGrow)
{
}

template <typename T, typename Traits>
INLINE CTStack<T, Traits>::~CTStack(void)
{
}

template <typename T, typename Traits>
INLINE CTStack<T, Traits>::CTStack(const CTStack<T, Traits>& aSrc)
: m_List(aSrc.m_List)
{
}

template <typename T, typename Traits>
INLINE CTStack<T, Traits>& CTStack<T, Traits>::operator=(const CTStack<T, Traits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        m_List = aSrc.m_List;
    }
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE CTStack<T, Traits>::CTStack(CTStack<T, Traits>&& aSrc)
: m_List(std::move(aSrc.m_List))
{
}

template <typename T, typename Traits>
INLINE CTStack<T, Traits>& CTStack<T, Traits>::operator=(CTStack<T, Traits>&& aSrc)
{
    if (this != &aSrc)
    {
        m_List = std::move(aSrc.m_List);
    }
    return (*this);
}
#endif
template <typename T, typename Traits>
INLINE Int CTStack<T, Traits>::GetSize(void) const
{
    return m_List.GetSize();
}

template <typename T, typename Traits>
INLINE void CTStack<T, Traits>::SetGrow(Int nGrow)
{
    m_List.SetGrow(nGrow);
}

template <typename T, typename Traits>
INLINE bool CTStack<T, Traits>::IsEmpty(void) const
{
    return (m_List.GetSize() == 0);
}

template <typename T, typename Traits>
INLINE bool CTStack<T, Traits>::IsFull(void) const
{
    return false;
}

template <typename T, typename Traits>
INLINE bool CTStack<T, Traits>::Push(INARGTYPE t)
{
    if (IsFull() == false)
    {
        return (m_List.AddHead(t) != nullptr);
    }
    return false;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T, typename Traits>
INLINE bool CTStack<T, Traits>::Push(RVARGTYPE t)
{
    if (IsFull() == false)
    {
        return (m_List.AddHead(std::move(t)) != nullptr);
    }
    return false;
}
#endif
template <typename T, typename Traits>
INLINE T CTStack<T, Traits>::Pop(void)
{
    if (IsEmpty() == false)
    {
        return m_List.RemoveHead();
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename T, typename Traits>
INLINE void CTStack<T, Traits>::RemoveAll(void)
{
    m_List.RemoveAll();
}

template <typename T, typename Traits>
INLINE T& CTStack<T, Traits>::GetItem(void)
{
    return m_List.GetHead();
}

template <typename T, typename Traits>
INLINE const T& CTStack<T, Traits>::GetItem(void) const
{
    return m_List.GetHead();
}

template <typename T, typename Traits>
INLINE bool CTStack<T, Traits>::Attach(CTStack& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        return m_List.Attach(aSrc.m_List);
    }
    return false;
}

template <typename T, typename Traits>
INLINE void CTStack<T, Traits>::Detach(void)
{
    m_List.Detach();
}

///////////////////////////////////////////////////////////////////
// CTMap
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>::CTMap(Int nGrow, Int nHash)
: m_pFree(nullptr)
, m_ppHash(nullptr)
, m_pAlloc(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(0)
, m_nHash(0)
, m_nBuild(0)
, m_nHead(0)
, m_nTail(0)
{
    SetGrow(nGrow);
    SetHash(nHash);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>::~CTMap(void)
{
    RemoveAll();
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>::CTMap(const CTMap<K, V, KTraits, VTraits>& aSrc)
: m_pFree(nullptr)
, m_ppHash(nullptr)
, m_pAlloc(nullptr)
, m_nAllocSize(0)
, m_nSize(0)
, m_nGrow(aSrc.m_nGrow)
, m_nHash(aSrc.m_nHash)
, m_nBuild(aSrc.m_nBuild)
, m_nHead(0)
, m_nTail(0)
{
    Copy(aSrc);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>& CTMap<K, V, KTraits, VTraits>::operator=(const CTMap<K, V, KTraits, VTraits>& aSrc)
{
    Copy(aSrc);
    return (*this);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>::CTMap(CTMap<K, V, KTraits, VTraits>&& aSrc)
: m_pFree(aSrc.m_pFree)
, m_ppHash(aSrc.m_ppHash)
, m_pAlloc(aSrc.m_pAlloc)
, m_nAllocSize(aSrc.m_nAllocSize)
, m_nSize(aSrc.m_nSize)
, m_nGrow(aSrc.m_nGrow)
, m_nHash(aSrc.m_nHash)
, m_nBuild(aSrc.m_nBuild)
, m_nHead(aSrc.m_nHead)
, m_nTail(aSrc.m_nTail)
{
    aSrc.m_pFree      = nullptr;
    aSrc.m_ppHash     = nullptr;
    aSrc.m_pAlloc     = nullptr;
    aSrc.m_nAllocSize = 0;
    aSrc.m_nSize      = 0;
    //aSrc.m_nGrow      = 0;
    //aSrc.m_nHash      = 0;
    //aSrc.m_nBuild     = 0;
    aSrc.m_nHead      = 0;
    aSrc.m_nTail      = 0;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE CTMap<K, V, KTraits, VTraits>& CTMap<K, V, KTraits, VTraits>::operator=(CTMap<K, V, KTraits, VTraits>&& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll();
        m_pFree           = aSrc.m_pFree;
        m_ppHash          = aSrc.m_ppHash;
        m_pAlloc          = aSrc.m_pAlloc;
        m_nAllocSize      = aSrc.m_nAllocSize;
        m_nSize           = aSrc.m_nSize;
        m_nGrow           = aSrc.m_nGrow;
        m_nHash           = aSrc.m_nHash;
        m_nBuild          = aSrc.m_nBuild;
        m_nHead           = aSrc.m_nHead;
        m_nTail           = aSrc.m_nTail;
        aSrc.m_pFree      = nullptr;
        aSrc.m_ppHash     = nullptr;
        aSrc.m_pAlloc     = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        //aSrc.m_nGrow      = 0;
        //aSrc.m_nHash      = 0;
        //aSrc.m_nBuild     = 0;
        aSrc.m_nHead      = 0;
        aSrc.m_nTail      = 0;
    }
    return (*this);
}
#endif
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE Int CTMap<K, V, KTraits, VTraits>::GetAllocSize(void) const
{
    return m_nAllocSize;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE Int CTMap<K, V, KTraits, VTraits>::GetSize(void) const
{
    return m_nSize;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE Int CTMap<K, V, KTraits, VTraits>::GetGrow(void) const
{
    return m_nGrow;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::SetGrow(Int nGrow)
{
    assert(nGrow > 0);
    nGrow   = DEF::Maxmin<Int>(nGrow, SIZED_MIN, SIZED_MAX);
    m_nGrow = DEF::Align<Int>(nGrow, SIZED_GROW);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE Int CTMap<K, V, KTraits, VTraits>::GetHash(void) const
{
    return m_nHash;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::SetHash(Int nHash)
{
    assert(nHash > 0);
    nHash   = DEF::Maxmin<Int>(nHash, HASHD_MIN, HASHD_MAX);
    m_nHash = DEF::Align<Int>(nHash, HASHD_MIN);

    if (m_ppHash == nullptr)
    {
        m_nBuild = m_nHash;
        m_nHead  = m_nHash - 1;
    }
    else if (m_nBuild != m_nHash)
    {
        BuildHash(m_nHash);
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KINARGTYPE Key)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(Key);
    return ((PINDEX)pNode);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KINARGTYPE Key, VINARGTYPE Val)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(Key, Val);
    return ((PINDEX)pNode);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KRVARGTYPE Key)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(std::move(Key));
    return ((PINDEX)pNode);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KINARGTYPE Key, VRVARGTYPE Val)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(Key, std::move(Val));
    return ((PINDEX)pNode);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KRVARGTYPE Key, VRVARGTYPE Val)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(std::move(Key), std::move(Val));
    return ((PINDEX)pNode);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::Add(KRVARGTYPE Key, VINARGTYPE Val)
{
    if ((m_ppHash == nullptr) && (InitHash() == false))
    {
        return nullptr;
    }

    NODE* pNode = NewNode(std::move(Key), Val);
    return ((PINDEX)pNode);
}
#endif
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::Find(KINARGTYPE Key, PINDEX pStartAfter)
{
    if (m_ppHash != nullptr)
    {
        NODE* pNode = nullptr;
        if (pStartAfter != nullptr)
        {
            pNode = static_cast<NODE*>(pStartAfter);
            if (pNode->m_nHash == (Int)(KTraits::HashElements(Key) & (size_t)((size_t)m_nBuild - 1)))
            {
                pNode = pNode->m_pNext;
                if (pNode == nullptr)
                {
                    return nullptr;
                }
            }
            else
            {
                pNode = nullptr;
            }
        }
        if (pNode == nullptr)
        {
            pNode = m_ppHash[KTraits::HashElements(Key) & (size_t)((size_t)m_nBuild - 1)];
        }
        for (; pNode != nullptr; pNode = pNode->m_pNext)
        {
            if (KTraits::CompareElements(pNode->m_K, Key))
            {
                return ((PAIR*)pNode);
            }
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::Find(KINARGTYPE Key, PINDEX pStartAfter) const
{
    if (m_ppHash != nullptr)
    {
        NODE* pNode = nullptr;
        if (pStartAfter != nullptr)
        {
            pNode = static_cast<NODE*>(pStartAfter);
            if (pNode->m_nHash == (Int)(KTraits::HashElements(Key) & (m_nBuild - 1)))
            {
                pNode = pNode->m_pNext;
                if (pNode == nullptr)
                {
                    return nullptr;
                }
            }
            else
            {
                pNode = nullptr;
            }
        }
        if (pNode == nullptr)
        {
            pNode = m_ppHash[KTraits::HashElements(Key) & (m_nBuild - 1)];
        }
        for (; pNode != nullptr; pNode = pNode->m_pNext)
        {
            if (KTraits::CompareElements(pNode->m_K, Key))
            {
                return ((const PAIR*)pNode);
            }
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::FindIndex(KINARGTYPE Key, PINDEX pStartAfter) const
{
    if (m_ppHash != nullptr)
    {
        NODE* pNode = nullptr;
        if (pStartAfter != nullptr)
        {
            pNode = static_cast<NODE*>(pStartAfter);
            if (pNode->m_nHash == (Int)(KTraits::HashElements(Key) & (size_t)((size_t)m_nBuild - 1)))
            {
                pNode = pNode->m_pNext;
                if (pNode == nullptr)
                {
                    return nullptr;
                }
            }
            else
            {
                pNode = nullptr;
            }
        }
        if (pNode == nullptr)
        {
            pNode = m_ppHash[KTraits::HashElements(Key) & (size_t)((size_t)m_nBuild - 1)];
        }
        for (; pNode != nullptr; pNode = pNode->m_pNext)
        {
            if (KTraits::CompareElements(pNode->m_K, Key))
            {
                return ((PINDEX)pNode);
            }
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::Remove(KINARGTYPE Key)
{
    NODE* pNode = static_cast<NODE*>(Find(Key));
    if (pNode != nullptr)
    {
        DeleteNode(pNode);
    }
    return (pNode != nullptr);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::RemoveAt(PINDEX index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        DeleteNode(pNode);
    }
    return (pNode != nullptr);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::RemoveAll(void)
{
    if (m_ppHash != nullptr)
    {
        for (Int i = m_nHead; i <= m_nTail; ++i)
        {
            NODE* pHead = m_ppHash[i];
            for (NODE* p = pHead; p != nullptr; p = pHead)
            {
                assert(p->m_nHash >= 0); // free flag -1
                pHead = p->m_pNext;
                p->~NODE();
            }
        }
    }
    if (m_pAlloc != nullptr)
    {
        m_pAlloc->Destroy();
        m_pAlloc = nullptr;
    }
    m_pFree = nullptr;
    if (m_ppHash != nullptr)
    {
        FREE( m_ppHash );
        m_ppHash = nullptr;
    }
    m_nAllocSize = 0;
    m_nSize  = 0;
    m_nBuild = m_nHash;
    m_nHead  = m_nHash - 1;
    m_nTail  = 0;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::Check(PINDEX index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if ((pNode == nullptr) || (pNode->m_nHash == -1)) // free flag -1
    {
        return false;
    }
    return true;
}

// if can not find the key, add a new node
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::SetAt(KINARGTYPE Key, VINARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(Find(Key));
    if (pNode != nullptr)
    {
        pNode->m_V = Val;
        return ((PINDEX)pNode);
    }
    else
    {
        return Add(Key, Val);
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::SetAtIndex(PINDEX index, VINARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        pNode->m_V = Val;
    }
    return (pNode != nullptr);
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::SetAt(KINARGTYPE Key, VRVARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(Find(Key));
    if (pNode != nullptr)
    {
        pNode->m_V = std::move(Val);
        return ((PINDEX)pNode);
    }
    else
    {
        return Add(Key, std::move(Val));
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::SetAt(KRVARGTYPE Key, VRVARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(Find(Key));
    if (pNode != nullptr)
    {
        pNode->m_V = std::move(Val);
        return ((PINDEX)pNode);
    }
    else
    {
        return Add(std::move(Key), std::move(Val));
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::SetAt(KRVARGTYPE Key, VINARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(Find(Key));
    if (pNode != nullptr)
    {
        pNode->m_V = Val;
        return ((PINDEX)pNode);
    }
    else
    {
        return Add(std::move(Key), Val);
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::SetAtIndex(PINDEX index, VRVARGTYPE Val)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        pNode->m_V = std::move(Val);
    }
    return (pNode != nullptr);
}
#endif
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetAt(PINDEX index)
{
    assert(index != nullptr);
    return static_cast<PAIR*>(index);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetAt(PINDEX index) const
{
    assert(index != nullptr);
    return static_cast<const PAIR*>(index);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const K& CTMap<K, V, KTraits, VTraits>::GetKeyAt(PINDEX index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        return (pNode->m_K);
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE V& CTMap<K, V, KTraits, VTraits>::GetValueAt(PINDEX index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        return (pNode->m_V);
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const V& CTMap<K, V, KTraits, VTraits>::GetValueAt(PINDEX index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        return (pNode->m_V);
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE V& CTMap<K, V, KTraits, VTraits>::operator[](PINDEX index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        return (pNode->m_V);
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const V& CTMap<K, V, KTraits, VTraits>::operator[](PINDEX index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        return (pNode->m_V);
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetFirst(void)
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nHead] != nullptr)
        {
            return ((PAIR*)m_ppHash[m_nHead]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetFirst(void) const
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nHead] != nullptr)
        {
            return ((const PAIR*)m_ppHash[m_nHead]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::GetFirstIndex(void) const
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nHead] != nullptr)
        {
            return ((PINDEX)m_ppHash[m_nHead]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetLast(void)
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nTail] != nullptr)
        {
            return ((PAIR*)m_ppHash[m_nTail]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetLast(void) const
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nTail] != nullptr)
        {
            return ((const PAIR*)m_ppHash[m_nTail]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE PINDEX CTMap<K, V, KTraits, VTraits>::GetLastIndex(void) const
{
    if ((m_ppHash != nullptr) && (m_nHead <= m_nTail))
    {
        if (m_ppHash[m_nTail] != nullptr)
        {
            return ((PINDEX)m_ppHash[m_nTail]);
        }
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetNext(PINDEX& index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        index = ((PINDEX)pNode->m_pNext);
        if (index == nullptr)
        {
            for (Int i = pNode->m_nHash + 1; i <= m_nTail; ++i)
            {
                if (m_ppHash[i] != nullptr)
                {
                    index = (PINDEX)m_ppHash[i];
                    break;
                }
            }
        }
        return ((PAIR*)pNode);
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetNext(PINDEX& index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        index = ((PINDEX)pNode->m_pNext);
        if (index == nullptr)
        {
            for (Int i = pNode->m_nHash + 1; i <= m_nTail; ++i)
            {
                if (m_ppHash[i] != nullptr)
                {
                    index = (PINDEX)m_ppHash[i];
                    break;
                }
            }
        }
        return ((const PAIR*)pNode);
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetPrev(PINDEX& index)
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        index = ((PINDEX)pNode->m_pNext);
        if (index == nullptr)
        {
            for (Int i = pNode->m_nHash - 1; i >= m_nHead; --i)
            {
                if (m_ppHash[i] != nullptr)
                {
                    index = (PINDEX)m_ppHash[i];
                    break;
                }
            }
        }
        return ((PAIR*)pNode);
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE const typename CTMap<K, V, KTraits, VTraits>::PAIR* CTMap<K, V, KTraits, VTraits>::GetPrev(PINDEX& index) const
{
    NODE* pNode = static_cast<NODE*>(index);
    if (pNode != nullptr)
    {
        index = ((PINDEX)pNode->m_pNext);
        if (index == nullptr)
        {
            for (Int i = pNode->m_nHash - 1; i >= m_nHead; --i)
            {
                if (m_ppHash[i] != nullptr)
                {
                    index = (PINDEX)m_ppHash[i];
                    break;
                }
            }
        }
        return ((const PAIR*)pNode);
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::Append(const CTMap<K, V, KTraits, VTraits>& aSrc)
{
    if (this != &aSrc)
    {
        PINDEX index = aSrc.GetFirstIndex();
        while (index != nullptr)
        {
            const PAIR* p = aSrc.GetNext(index);
            Add(p->m_K, p->m_V);
        }
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::Copy(const CTMap<K, V, KTraits, VTraits>& aSrc)
{
    if (this != &aSrc)
    {
        RemoveAll();
        PINDEX index = aSrc.GetFirstIndex();
        while (index != nullptr)
        {
            const PAIR* p = aSrc.GetNext(index);
            Add(p->m_K, p->m_V);
        }
    }
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::Attach(CTMap<K, V, KTraits, VTraits>& aSrc)
{
    assert(this != &aSrc);
    if (this != &aSrc)
    {
        RemoveAll();
        m_pFree           = aSrc.m_pFree;
        m_ppHash          = aSrc.m_ppHash;
        m_pAlloc          = aSrc.m_pAlloc;
        m_nAllocSize      = aSrc.m_nAllocSize;
        m_nSize           = aSrc.m_nSize;
        m_nGrow           = aSrc.m_nGrow;
        m_nHash           = aSrc.m_nHash;
        m_nBuild          = aSrc.m_nBuild;
        m_nHead           = aSrc.m_nHead;
        m_nTail           = aSrc.m_nTail;
        aSrc.m_pFree      = nullptr;
        aSrc.m_ppHash     = nullptr;
        aSrc.m_pAlloc     = nullptr;
        aSrc.m_nAllocSize = 0;
        aSrc.m_nSize      = 0;
        //aSrc.m_nGrow      = 0;
        //aSrc.m_nHash      = 0;
        //aSrc.m_nBuild     = 0;
        aSrc.m_nHead      = 0;
        aSrc.m_nTail      = 0;
        return true;
    }
    return false;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::Detach(void)
{
    m_pFree      = nullptr;
    m_ppHash     = nullptr;
    m_pAlloc     = nullptr;
    m_nAllocSize = 0;
    m_nSize      = 0;
    //m_nGrow      = 0;
    //m_nHash      = 0;
    //m_nBuild     = 0;
    m_nHead      = 0;
    m_nTail      = 0;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::InitHash(void)
{
    assert(m_ppHash == nullptr);
    m_ppHash = reinterpret_cast<NODE**>( ALLOC( m_nBuild * sizeof(NODE*) ) );
    if (m_ppHash != nullptr)
    {
        MM_SAFE::Set(m_ppHash, 0, m_nBuild * sizeof(NODE*));
        return true;
    }
    return false;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::BuildHash(Int nHash)
{
    assert(m_ppHash != nullptr);
    NODE** ppHash = m_ppHash;
    m_ppHash = reinterpret_cast<NODE**>( ALLOC( nHash * sizeof(NODE*) ) );
    if (m_ppHash != nullptr)
    {
        MM_SAFE::Set(m_ppHash, 0, nHash * sizeof(NODE*));
        m_nBuild = nHash;

        Int nHead = m_nHead;
        m_nHead   = nHash - 1;
        Int nTail = m_nTail;
        m_nTail   = 0;

        for (Int i = nHead; i <= nTail; ++i)
        {
            NODE* pHead = ppHash[i];
            for (NODE* p = pHead; p != nullptr; p = pHead)
            {
                assert(p->m_nHash >= 0); // free flag -1
                pHead = p->m_pNext;

                p->Reset();
                Hash(p);
            }
        }
        return true;
    }
    m_ppHash = ppHash;
    return false;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::Hash(NODE* pNode)
{
    Int nHash = (Int)(KTraits::HashElements(pNode->m_K) & (size_t)((size_t)m_nBuild - 1));
    NODE* pNext = m_ppHash[nHash];
    pNode->m_nHash = nHash;
    pNode->m_pNext = pNext;
    if (pNext != nullptr)
    {
        pNext->m_pPrev = pNode;
    }
    m_ppHash[nHash] = pNode;

    if (nHash < m_nHead)
    {
        m_nHead = nHash;
    }
    if (nHash > m_nTail)
    {
        m_nTail = nHash;
    }
    return true;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::ReHash(NODE* pNode)
{
    Int nHash = pNode->m_nHash;
    if (m_ppHash[nHash] == pNode)
    {
        m_ppHash[nHash] = pNode->m_pNext;
        if (m_ppHash[nHash] == nullptr)
        {
            if ((m_nHead == nHash) && (m_nTail == nHash))
            {
                assert(m_nSize == 1);
                return false;
            }

            if (m_nHead == nHash)
            {
                for (Int i = (m_nHead + 1); i <= m_nTail; ++i)
                {
                    if (m_ppHash[i] != nullptr)
                    {
                        m_nHead = i;
                        break;
                    }
                }
            }
            else if (m_nTail == nHash)
            {
                for (Int i = (m_nTail - 1); i >= m_nHead; --i)
                {
                    if (m_ppHash[i] != nullptr)
                    {
                        m_nTail = i;
                        break;
                    }
                }
            }
            return false;
        }
    }
    if (pNode->m_pPrev != nullptr)
    {
        pNode->m_pPrev->m_pNext = pNode->m_pNext;
    }
    if (pNode->m_pNext != nullptr)
    {
        pNode->m_pNext->m_pPrev = pNode->m_pPrev;
    }
    return true;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE bool CTMap<K, V, KTraits, VTraits>::GetFreeNode(void)
{
    if (m_pFree == nullptr)
    {
        NODE* pNode = CTContainerAlloc<NODE>::Create(m_pAlloc, (size_t)m_nGrow);
        if (pNode == nullptr)
        {
            throw (E_OUTOFMEMORY);
        }
        pNode += (size_t)((size_t)m_nGrow - 1);
        for (Int i = m_nGrow - 1; i >= 0; --i)
        {
            pNode->m_nHash = -1;// free node flag
            pNode->m_pPrev = nullptr;
            pNode->m_pNext = m_pFree;
            m_pFree = pNode;
            --pNode;
        }
        m_nAllocSize += m_nGrow;
    }
    return (m_pFree != nullptr);
}

#pragma push_macro("new")
#undef new
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KINARGTYPE Key)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(Key);

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KINARGTYPE Key, VINARGTYPE Val)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(Key, Val);

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}
#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KRVARGTYPE Key)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(std::move(Key));

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KINARGTYPE Key, VRVARGTYPE Val)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(Key, std::move(Val));

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KRVARGTYPE Key, VRVARGTYPE Val)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(std::move(Key), std::move(Val));

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE typename CTMap<K, V, KTraits, VTraits>::NODE* CTMap<K, V, KTraits, VTraits>::NewNode(KRVARGTYPE Key, VINARGTYPE Val)
{
    if (GetFreeNode())
    {
        NODE* pNewNode = m_pFree;
        m_pFree = m_pFree->m_pNext;
        assert(pNewNode->m_nHash == -1); // free flag -1

        GNEW(pNewNode) NODE(std::move(Key), Val);

        ++m_nSize;
        assert(m_nSize > 0);
        if ((HASHD_RADIX * m_nSize) >= (HASHD_HIGH * m_nBuild))
        {
            BuildHash(m_nBuild * HASHD_LOW);
        }

        Hash(pNewNode);
        return pNewNode;
    }
    return nullptr;
}
#endif
#pragma pop_macro("new")

template <typename K, typename V, typename KTraits, typename VTraits>
INLINE void CTMap<K, V, KTraits, VTraits>::DeleteNode(NODE* pNode)
{
    ReHash(pNode);

    pNode->~NODE();
    pNode->m_nHash = -1;// free node flag
    pNode->m_pNext = m_pFree;
    m_pFree = pNode;

    assert(m_nSize > 0);
    --m_nSize;
    if (m_nSize == 0)
    {
        RemoveAll();
    }
    else if ((m_nBuild > m_nHash) && ((HASHD_RADIX * m_nSize) <= (HASHD_LOW * m_nBuild)))
    {
        BuildHash(m_nBuild / HASHD_LOW);
    }
}

#endif // __CONTAINER_INL__
