// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __SEQUENCE_DEF_H__
#define __SEQUENCE_DEF_H__

#pragma once

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// va args sequence
#ifdef __MODERN_CXX_NOT_SUPPORTED
    template<int nNum>
    struct PLACEHOLDER { };

    typedef PLACEHOLDER<1>  _1;
    typedef PLACEHOLDER<2>  _2;
    typedef PLACEHOLDER<3>  _3;
    typedef PLACEHOLDER<4>  _4;
    typedef PLACEHOLDER<5>  _5;
    typedef PLACEHOLDER<6>  _6;
    typedef PLACEHOLDER<7>  _7;
    typedef PLACEHOLDER<8>  _8;
    typedef PLACEHOLDER<9>  _9;
    typedef PLACEHOLDER<10> _10;
    typedef PLACEHOLDER<11> _11;
    typedef PLACEHOLDER<12> _12;
    typedef PLACEHOLDER<13> _13;
    typedef PLACEHOLDER<14> _14;
    typedef PLACEHOLDER<15> _15;
    typedef PLACEHOLDER<16> _16;
    typedef PLACEHOLDER<17> _17;
    typedef PLACEHOLDER<18> _18;
    typedef PLACEHOLDER<19> _19;
    typedef PLACEHOLDER<20> _20;
    typedef PLACEHOLDER<21> _21;
    typedef PLACEHOLDER<22> _22;
    typedef PLACEHOLDER<23> _23;
    typedef PLACEHOLDER<24> _24;
    typedef PLACEHOLDER<25> _25;
    typedef PLACEHOLDER<26> _26;
    typedef PLACEHOLDER<27> _27;
    typedef PLACEHOLDER<28> _28;
    typedef PLACEHOLDER<29> _29;
#endif
///////////////////////////////////////////////////////////////////
#define __SEQUENCE_SIZE__(_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, N, ...) N
#define __SEQUENCE_INDEX__(...) __SEQUENCE_SIZE__(__VA_ARGS__, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11)
///////////////////////////////////////////////////////////////////
#define VA_ARGS_SEQ_STR(...) CONCAT(VA_ARGS_SEQ_STR_, __SEQUENCE_INDEX__(__VA_ARGS__))(__VA_ARGS__)
#define VA_ARGS_SEQ_STR_11(v11) \
#v11

#define VA_ARGS_SEQ_STR_12(v11, v12) \
#v11 ", " #v12

#define VA_ARGS_SEQ_STR_13(v11, v12, v13) \
#v11 ", " #v12 ", " #v13

#define VA_ARGS_SEQ_STR_14(v11, v12, v13, v14) \
#v11 ", " #v12 ", " #v13 ", " #v14

#define VA_ARGS_SEQ_STR_15(v11, v12, v13, v14, v15) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15

#define VA_ARGS_SEQ_STR_16(v11, v12, v13, v14, v15, v16) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16

#define VA_ARGS_SEQ_STR_17(v11, v12, v13, v14, v15, v16, v17) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17

#define VA_ARGS_SEQ_STR_18(v11, v12, v13, v14, v15, v16, v17, v18) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18

#define VA_ARGS_SEQ_STR_19(v11, v12, v13, v14, v15, v16, v17, v18, v19) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19

#define VA_ARGS_SEQ_STR_20(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20

#define VA_ARGS_SEQ_STR_21(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21

#define VA_ARGS_SEQ_STR_22(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22

#define VA_ARGS_SEQ_STR_23(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23

#define VA_ARGS_SEQ_STR_24(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24

#define VA_ARGS_SEQ_STR_25(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24 ", " #v25

#define VA_ARGS_SEQ_STR_26(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24 ", " #v25 ", " #v26

#define VA_ARGS_SEQ_STR_27(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24 ", " #v25 ", " #v26 ", " #v27

#define VA_ARGS_SEQ_STR_28(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24 ", " #v25 ", " #v26 ", " #v27 ", " #v28

#define VA_ARGS_SEQ_STR_29(v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29) \
#v11 ", " #v12 ", " #v13 ", " #v14 ", " #v15 ", " #v16 ", " #v17 ", " #v18 ", " #v19 ", " #v20 ", " #v21 ", " #v22 ", " #v23 ", " #v24 ", " #v25 ", " #v26 ", " #v27 ", " #v28 ", " #v29
///////////////////////////////////////////////////////////////////
#define VA_ARGS_SEQ_MAROC(DEF_MAROC, ...) CONCAT(VA_ARGS_SEQ_MAROC_, __SEQUENCE_INDEX__(__VA_ARGS__))(DEF_MAROC, __VA_ARGS__)
#define VA_ARGS_SEQ_MAROC_11(m, v11) \
m(v11)

#define VA_ARGS_SEQ_MAROC_12(m, v11, v12) \
m(v11) m(v12)

#define VA_ARGS_SEQ_MAROC_13(m, v11, v12, v13) \
m(v11) m(v12) m(v13)

#define VA_ARGS_SEQ_MAROC_14(m, v11, v12, v13, v14) \
m(v11) m(v12) m(v13) m(v14)

#define VA_ARGS_SEQ_MAROC_15(m, v11, v12, v13, v14, v15) \
m(v11) m(v12) m(v13) m(v14) m(v15)

#define VA_ARGS_SEQ_MAROC_16(m, v11, v12, v13, v14, v15, v16) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16)

#define VA_ARGS_SEQ_MAROC_17(m, v11, v12, v13, v14, v15, v16, v17) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17)

#define VA_ARGS_SEQ_MAROC_18(m, v11, v12, v13, v14, v15, v16, v17, v18) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18)

#define VA_ARGS_SEQ_MAROC_19(m, v11, v12, v13, v14, v15, v16, v17, v18, v19) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19)

#define VA_ARGS_SEQ_MAROC_20(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20)

#define VA_ARGS_SEQ_MAROC_21(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21)

#define VA_ARGS_SEQ_MAROC_22(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22)

#define VA_ARGS_SEQ_MAROC_23(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23)

#define VA_ARGS_SEQ_MAROC_24(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24)

#define VA_ARGS_SEQ_MAROC_25(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24) m(v25)

#define VA_ARGS_SEQ_MAROC_26(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24) m(v25) m(v26)

#define VA_ARGS_SEQ_MAROC_27(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24) m(v25) m(v26) m(v27)

#define VA_ARGS_SEQ_MAROC_28(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24) m(v25) m(v26) m(v27) m(v28)

#define VA_ARGS_SEQ_MAROC_29(m, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29) \
m(v11) m(v12) m(v13) m(v14) m(v15) m(v16) m(v17) m(v18) m(v19) m(v20) m(v21) m(v22) m(v23) m(v24) m(v25) m(v26) m(v27) m(v28) m(v29)

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __SEQUENCE_DEF_H__
