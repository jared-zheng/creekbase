// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __EVENT_H__
#define __EVENT_H__

#pragma once

#include "streambuf.h"
#include "refcount.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CEventBase
class NOVTABLE CEventBase ABSTRACT : public CTRefCount<CEventBase>
{
public:
    virtual size_t Length(void) const PURE;
    virtual void   Serialize(CStream&) PURE;
protected:
    CEventBase(void);
    virtual ~CEventBase(void);

    CEventBase(const CEventBase&);
    CEventBase& operator=(const CEventBase&);
};
typedef CTRefCountPtr<CEventBase> CEventBasePtr;

///////////////////////////////////////////////////////////////////
// CEventHandler
class NOVTABLE CEventHandler ABSTRACT : public CTRefCount<CEventHandler>
{
public:
    // EVENT_TYPE_NONE & EVENT_TYPE_CACHE_REF & EVENT_TYPE_CACHE_MANAGED
    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam) PURE;
    // EVENT_TYPE_REFCOUNT
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam) PURE;
    // EVENT_TYPE_CACHE_QUEUE
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam) PURE;
    // tick event, return not RET_OKAY to close the opposite tick id
    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount) PURE;
protected:
    CEventHandler(void);
    virtual ~CEventHandler(void);
private:
    CEventHandler(const CEventHandler&);
    CEventHandler& operator=(const CEventHandler&);
};
typedef CTRefCountPtr<CEventHandler> CEventHandlerPtr;

///////////////////////////////////////////////////////////////////
// CEventQueue
class CEventQueue;
typedef CTRefCountPtr<CEventQueue> CEventQueuePtr;

class CORECLASS NOVTABLE CEventQueue : public CTRefCount<CEventQueue>
{
public:
    enum EVENT_TYPE
    {
        EVENT_TYPE_NONE          = 0, // utData is basic data or utData is ptr, discard after onhandle->OnHandle(Int, uintptr_t, ULLong)
        EVENT_TYPE_REFCOUNT      = 1, // an eventbase instance ptr, Release after onhandle->OnHandle(Int, CEventBase&, ULLong)
        EVENT_TYPE_CACHE_REF     = 2, // user cache memory, discard after onhandle->OnHandle(Int, uintptr_t, ULLong)
        EVENT_TYPE_CACHE_MANAGED = 3, // user cache memory, MCFree after onhandle->OnHandle(Int, uintptr_t, ULLong)
        EVENT_TYPE_CACHE_QUEUE   = 4, // copy user data to queue managed cache memory, delete after onhandle->OnHandle(Int, CStream&, ULLong)

        EVENT_TYPE_SYNC          = 0x10,
        EVENT_TYPE_MASK          = 0x1F,
    };

    enum EVENT_THREAD
    {
        EVENT_THREAD_MAX       = 1,    // 1 * cpu cores
    };

    enum EVENT_COUNT
    {
        EVENT_COUNT_CHECK      = 2,
        EVENT_COUNT_CACHE      = 3,     // 
        EVENT_COUNT_MAX        = 65536, // default event-queue max count
    };

public:
    // stCacheSize --> max cache size the queue used, 0 -- none cache used
    // nThreads    --> max threads the queue used, 0 -- only one
    // ***EventHandlerRef->AddRef when CEventQueue init, EventHandlerRef->Release when CEventQueue exit***
    // ***MNEW EventHandlerRef inst, set to event-queue, when event-queue exit, the EventHandlerRef inst MDELETE auto***
    // ***to prevent EventHandlerRef inst MDELETE auto, invoke AddRef before***
    static  bool EventQueue(CEventQueuePtr& EventQueuePtrRef, CEventHandler& EventHandlerRef, size_t stCacheSize = 0, UInt uThreads = 0);
    // tick min time 1ms, create a thread wait for timer instance callback
    // uInterval tick is the time between on last EventHandlerRef OnHandle return and next EventHandlerRef OnHandle called
    // EventHandlerRef->AddRef
    static  bool CreateTickEvent(uintptr_t utTick, UInt uInterval, CEventHandler& EventHandlerRef, UInt uCount = (UInt)TIMET_INFINITE);
    // EventHandlerRef->Release
    static  bool DestroyTickEvent(uintptr_t utTick);
    // return true if tick thread timeout
    static  bool CheckTickEvent(uintptr_t utTick, Int nMultInterval = EVENT_COUNT_CHECK);
public:
    virtual bool Init(void) PURE;
    virtual void Exit(void) PURE;

    // EVENT_TYPE_NONE
    // EVENT_TYPE_REFCOUNT(utData should be a valid ptr to a CEventBase instance, MCheck before add, AddRef)
    // EVENT_TYPE_CACHE_REF/EVENT_TYPE_CACHE_MANAGED(utData should be a valid cache ptr, ullParam is cache index, MCCheck before add)
    // EVENT_TYPE_CACHE_QUEUE(utData should be a valid ptr, ullParam is size <= stCacheSize)
    // if pSync ptr is a valid sync-instance, pSync will be signaled after event handled(ensure pSync ptr is valid before signaled)
    // if nType & EVENT_TYPE_SYNC, pSync ptr is ignored
    virtual bool Add(uintptr_t utEvent, uintptr_t utData = 0, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_NONE) PURE;

    // EVENT_TYPE_REFCOUNT(MCheck before add, AddRef)
    // EVENT_TYPE_CACHE_QUEUE(CEventBase's Length <= stCacheSize)
    // if pSync ptr is a valid sync-instance, pSync will be signaled after event handled(ensure pSync ptr is valid before signaled)
    // if nType & EVENT_TYPE_SYNC, pSync ptr is ignored
    virtual bool Add(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_REFCOUNT) PURE;

    // EVENT_TYPE_CACHE_QUEUE(Stream's size <= stCacheSize, otherwise just copy stCacheSize data)
    // CStream mode read(write stream use refer to get a read mode stream)
    // if pSync ptr is a valid sync-instance, pSync will be signaled after event handled(ensure pSync ptr is valid before signaled)
    // if nType & EVENT_TYPE_SYNC, pSync ptr is ignored
    virtual bool Add(uintptr_t utEvent, CStream& Stream, ULLong ullParam = 0, CSyncBase* pSync = nullptr, Int nType = EVENT_TYPE_CACHE_QUEUE) PURE;

    virtual bool MaxCount(UInt uMax) PURE;                     // max queue length
    virtual UInt GetCount(void) const PURE;                    // current queue length
    virtual UInt GetThreads(void) const PURE;                  // current work thread count
    virtual bool SetThreads(UInt uThreadCount = 0) PURE;       // 0 --- default 1, max EVENT_THREAD_MAX * cpu cores
    virtual bool SetThreadEvent(UInt uStart, UInt uStop) PURE; // work thread start/stop use event(!=0) to notify EventHandlerRef
    virtual bool CheckThreads(UInt uTimeout) PURE;             // return true if any work thread check timeout[MS]
protected:
    CEventQueue(void);
    virtual ~CEventQueue(void);
private:
    CEventQueue(const CEventQueue&);
    CEventQueue& operator=(const CEventQueue&);
};

///////////////////////////////////////////////////////////////////
#include "event.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __EVENT_H__
