// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __SINGLETON_H__
#define __SINGLETON_H__

#pragma once

#include "sync.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CTSingleton
template <typename T>
class CTSingleton : public T
{
public:
    static T*   GetInstance(void);

    static T*   NewInstance(void);
    static void DelInstance(void);
private:
    CTSingleton(void);
    ~CTSingleton(void);
    CTSingleton(const CTSingleton&);
    CTSingleton& operator=(const CTSingleton&);
private:
    static CTSingleton*   ms_pSingleton;
};

///////////////////////////////////////////////////////////////////
// CTSingletonSync
template <typename T>
class CTSingletonSync : public T
{
public:
    static T*   GetInstance(void);

    static T*   NewInstance(CSyncLock& Lock);
    static void DelInstance(CSyncLock& Lock);
private:
    CTSingletonSync(void);
    ~CTSingletonSync(void);
    CTSingletonSync(const CTSingletonSync&);
    CTSingletonSync& operator=(const CTSingletonSync&);
private:
    static CTSingletonSync*   ms_pSingleton;
};

///////////////////////////////////////////////////////////////////
// CTSingletonInst
template <typename T>
class CTSingletonInst : public T
{
public:
    static T*   GetInstance(void);

    static T*   NewInstance(void);
    static void DelInstance(void);
public:
    CTSingletonInst(void);
    ~CTSingletonInst(void);
    CTSingletonInst(const CTSingletonInst&);
    CTSingletonInst& operator=(const CTSingletonInst&);
private:
    static CTSingletonInst   ms_Singleton;
};

///////////////////////////////////////////////////////////////////
// CTSingletonUM : singleton create/destroy in system heap
template <typename T>
class CTSingletonUM : public T
{
public:
    static T*   GetInstance(void);

    static T*   NewInstance(void);
    static void DelInstance(void);
private:
    CTSingletonUM(void);
    ~CTSingletonUM(void);
    CTSingletonUM(const CTSingletonUM&);
    CTSingletonUM& operator=(const CTSingletonUM&);
private:
    static CTSingletonUM*   ms_pSingleton;
};

///////////////////////////////////////////////////////////////////
#include "singleton.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __SINGLETON_H__
