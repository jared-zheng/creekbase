// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_HTTP_INL__
#define __NETWORK_HTTP_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// NET_BUFFER
INLINE tagNET_BUFFER::tagNET_BUFFER(void)
: index(nullptr)
, pCache(nullptr)
, nSize(0)
, nOffset(0)
{
}

INLINE tagNET_BUFFER::~tagNET_BUFFER(void)
{
    Free();
}

INLINE bool tagNET_BUFFER::Create(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver)
{
    assert(IsValid() == false);
    if (pParam->nSize >= NetAttr.nJumboSize)
    {
        DEV_DEBUG(TF("NET_BUFFER[%p]::Create Size = %d > %d(limit)"), this, pParam->nSize, NetAttr.nJumboSize);
        return false;
    }

    // pParam->nSize < pParam->nCache ???
    if ((pParam->IsJumbo() == false) && (pParam->nSize < pParam->nCache))
    {
        Bind(NetAttr.inxBuffer, NetAttr.nBufferSize, NetAttr.nBufferOffset);
        bHandOver = pParam->IsHandOver();
        DEV_DEBUG(TF("NET_BUFFER[%p]::Create Bind buffer data-size = %d, index = %p, buffer-size = %d, offset = %d, handover = %d"), this, pParam->nSize, NetAttr.inxBuffer, NetAttr.nBufferSize, NetAttr.nBufferOffset, bHandOver);
    }
    else
    {
        Bind(NetAttr.inxJumbo, NetAttr.nJumboSize, NetAttr.nJumboOffset);
        bHandOver = (pParam->IsJumbo() && pParam->IsHandOver());
        DEV_DEBUG(TF("NET_BUFFER[%p]::Create Bind jumbo data-size = %d, index = %p, jumbo-size = %d, offset = %d, handover = %d"), this, pParam->nSize, NetAttr.inxJumbo, NetAttr.nJumboSize, NetAttr.nJumboOffset, bHandOver);
    }
    if (bHandOver)
    {
        pCache = pParam->pCache;
        return true;
    }

    pCache = MObject::MCAlloc(index);
    if (IsValid())
    {
        DEV_DEBUG(TF("NET_BUFFER[%p]::Create Copy data-size = %d, index = %p, data = %p, handover = %d"), this, pParam->nSize, index, Data(), bHandOver);
        MM_SAFE::Cpy(Data(), (size_t)pParam->nSize, pParam->pData, (size_t)pParam->nSize);
        return true;
    }
    DEV_DEBUG(TF("NET_BUFFER[%p]::Create alloc net-buffer failed, Size = %d[%d---%d]"), this, pParam->nSize, NetAttr.nBufferSize, NetAttr.nJumboSize);
    return false;
}

INLINE bool tagNET_BUFFER::Append(CNETTraits::NET_ATTR& NetAttr, PByte pData, Int nData, Int nCached, bool& bRebind)
{
    assert(IsValid());
    Int nNewSize = nCached + nData;
    if (nNewSize >= NetAttr.nJumboSize)
    {
        DEV_DEBUG(TF("NET_BUFFER[%p]::Append Size = %d > %d(limit)"), this, nNewSize, NetAttr.nJumboSize);
        return false;
    }
    if (nNewSize < nSize)
    {
        MM_SAFE::Cpy((pCache + nOffset + nCached), (size_t)nData, pData, (size_t)nData);
        DEV_DEBUG(TF("NET_BUFFER[%p]::Append Copy buffer data-size = %d, cached-size = %d, index = %p, buffer-size = %d, offset = %d, rebind = %d"), this, nData, nCached, index, nSize, nOffset, bRebind);
        return true;
    }
    PByte pJumbo = MObject::MCAlloc(NetAttr.inxJumbo);
    if (pJumbo != nullptr)
    {
        MM_SAFE::Cpy((pJumbo + NetAttr.nJumboOffset), (size_t)NetAttr.nJumboSize, Data(), (size_t)nCached);
        MM_SAFE::Cpy((pJumbo + NetAttr.nJumboOffset + nCached), (size_t)nData, pData, (size_t)nData);
        Bind(NetAttr.inxJumbo, pJumbo, NetAttr.nJumboSize, NetAttr.nJumboOffset);
        bRebind = true;
        DEV_DEBUG(TF("NET_BUFFER[%p]::Append Bind jumbo data-size = %d, cached-size = %d, index = %p, data = %p, size = %d, offset = %d, rebind = %d"), this, nData, nCached, NetAttr.inxJumbo, Data(), NetAttr.nJumboSize, NetAttr.nJumboOffset, bRebind);
        return true;
    }
    DEV_DEBUG(TF("NET_BUFFER[%p]::Append alloc net-buffer failed data-size = %d, cached-size = %d, Size = %d[%d---%d]"), this, nData, nCached, nNewSize, NetAttr.nBufferSize, NetAttr.nJumboSize);
    return false;
}

INLINE bool tagNET_BUFFER::Check(CNETTraits::PNET_PARAM pParam, Int nCached, Int nAppend, bool bHandOver, bool& bRebind)
{
    if ((nAppend < pParam->nSize) && bHandOver)
    {
        PByte pCopy = MObject::MCAlloc(index);
        if (pCopy == nullptr)
        {
            DEV_DEBUG(TF("NET_BUFFER[%p]::Check alloc copy net-buffer failed, Size = %d[%d---%d]"), this, nCached, nSize, nOffset);
            return false;
        }
        MM_SAFE::Cpy((pCopy + nOffset), (size_t)nSize, Data(), (size_t)nCached);
        pCache  = pCopy;
        bRebind = true;
        DEV_DEBUG(TF("NET_BUFFER[%p]::Check alloc buffer, index = %p, data = %p, size = %d, offset = %d, handover = %d, rebind = %d"), this, index, Data(), nSize, nOffset, bHandOver, bRebind);
    }
    else if (bHandOver)
    {
        pParam->pCache = nullptr;
        DEV_DEBUG(TF("NET_BUFFER[%p]::Check handover buffer, index = %p, data = %p, size = %d, offset = %d"), this, index, Data(), nSize, nOffset);
    }
    pParam->nSize -= nAppend;
    return true;
}

INLINE void tagNET_BUFFER::Bind(PINDEX inxCache, Int nCacheSize, Int nCacheOffset)
{
    assert(IsValid() == false);
    index   = inxCache;
    nSize   = nCacheSize;
    nOffset = nCacheOffset;
}

INLINE void tagNET_BUFFER::Bind(PINDEX inxCache, PByte pCachePtr, Int nCacheSize, Int nCacheOffset)
{
    assert(IsValid());
    Free();
    index   = inxCache;
    pCache  = pCachePtr;
    nSize   = nCacheSize;
    nOffset = nCacheOffset;
}

INLINE PByte tagNET_BUFFER::Data(void) const
{
    assert(IsValid());
    return (pCache + nOffset);
}

INLINE size_t tagNET_BUFFER::Length(void) const
{
    return (sizeof(PINDEX) + sizeof(PByte) + sizeof(Int) * 2);
}
// NET_BUFFER'Serialize only used for current process async event handle
INLINE void tagNET_BUFFER::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> index >> pCache >> nSize >> nOffset;
    }
    else
    {
        Stream << index << pCache << nSize << nOffset;
    }
}

INLINE bool tagNET_BUFFER::IsValid(void) const
{
    return (index != nullptr);
}

INLINE void tagNET_BUFFER::Free(void)
{
    if (index != nullptr)
    {
        DEV_DEBUG(TF("NET_BUFFER[%p]::Free, index = %p, data = %p, size = %d, offset = %d"), this, index, Data(), nSize, nOffset);
        MObject::MCFree(index, pCache);
        index = nullptr;
    }
}

INLINE void tagNET_BUFFER::Reset(void)
{
    DEV_DEBUG(TF("NET_BUFFER[%p]::Reset, index = %p, cache = %p, size = %d, offset = %d"), this, index, pCache, nSize, nOffset);
    //MM_SAFE::Set(this, 0, sizeof(tagNET_BUFFER));
    index = nullptr;
}

///////////////////////////////////////////////////////////////////
// CHTTPTraits
INLINE CPCStr CHTTPTraits::StatusToString(Int nStatus)
{
    switch (nStatus)
    {
    CASE_RETSTR(CONTINUE,                        "Continue")
    CASE_RETSTR(SWITCHING_PROTOCOLS,             "Switching Protocols")
    CASE_RETSTR(OK,                              "OK")
    CASE_RETSTR(CREATED,                         "Created")
    CASE_RETSTR(ACCEPTED,                        "Accepted")
    CASE_RETSTR(NON_AUTHORITATIVE_INFORMATION,   "Non-Authoritative Information")
    CASE_RETSTR(NO_CONTENT,                      "No Content")
    CASE_RETSTR(RESET_CONTENT,                   "Reset Content")
    CASE_RETSTR(PARTIAL_CONTENT,                 "Partial Content")

    CASE_RETSTR(MULTIPLE_CHOICES,                "Multiple Choices")
    CASE_RETSTR(MOVED_PERMANENTLY,               "Moved Permanently")
    CASE_RETSTR(FOUND,                           "Found")
    CASE_RETSTR(SEE_OTHER,                       "See Other")
    CASE_RETSTR(NOT_MODIFIED,                    "Not Modified")
    CASE_RETSTR(USE_PROXY,                       "Use Proxy")
    CASE_RETSTR(TEMPORARY_REDIRECT,              "Temporary Redirect")

    CASE_RETSTR(BAD_REQUEST,                     "Bad Request")
    CASE_RETSTR(UNAUTHORIZED,                    "Unauthorized")
    CASE_RETSTR(PAYMENT_REQUIRED,                "Payment Required")
    CASE_RETSTR(FORBIDDEN,                       "Forbidden")
    CASE_RETSTR(NOT_FOUND,                       "Not Found")
    CASE_RETSTR(METHOD_NOT_ALLOWED,              "Method Not Allowed")
    CASE_RETSTR(NOT_ACCEPTABLE,                  "Not Acceptable")
    CASE_RETSTR(PROXY_AUTHENTICATION_REQUIRED,   "Proxy Authentication Required")
    CASE_RETSTR(REQUEST_TIMEOUT,                 "Request Timeout")
    CASE_RETSTR(CONFLICT,                        "Conflict")
    CASE_RETSTR(GONE,                            "Gone")
    CASE_RETSTR(LENGTH_REQUIRED,                 "Length Required")
    CASE_RETSTR(PRECONDITION_FAILED,             "Precondition Failed")
    CASE_RETSTR(REQUEST_ENTITY_TOO_LARGE,        "Request Entity Too Large")
    CASE_RETSTR(REQUEST_URI_TOO_LONG,            "Request-URI Too Long")
    CASE_RETSTR(UNSUPPORTED_MEDIA_TYPE,          "Unsupported Media Type")
    CASE_RETSTR(REQUESTED_RANGE_NOT_SATISFIABLE, "Request Range Not Satisfiable")
    CASE_RETSTR(EXPECTATION_FAILED,              "Expectation Failed")
    CASE_RETSTR(UPGRADE_REQUIRED,                "Upgrade Required")

    CASE_RETSTR(INTERNAL_SERVER_ERROR,           "Internal Server Error")
    CASE_RETSTR(NOT_IMPLEMENTED,                 "Not Implemented")
    CASE_RETSTR(BAD_GATEWAY,                     "Bad Gateway")
    CASE_RETSTR(SERVICE_UNAVAILABLE,             "Service Unavailable")
    CASE_RETSTR(GATEWAY_TIMEOUT,                 "Gateway Timeout")
    CASE_RETSTR(HTTP_VERSION_NOT_SUPPORTED,      "HTTP Version Not Supported")
    default:
        {
            return "Unknown HTTP Status";
        }
    }
}

INLINE CPCXStr CHTTPTraits::StatusToLocalString(Int nStatus)
{
    switch (nStatus)
    {
    CASE_RETSTR(CONTINUE,                        TF("Continue"))
    CASE_RETSTR(SWITCHING_PROTOCOLS,             TF("Switching Protocols"))
    CASE_RETSTR(OK,                              TF("OK"))
    CASE_RETSTR(CREATED,                         TF("Created"))
    CASE_RETSTR(ACCEPTED,                        TF("Accepted"))
    CASE_RETSTR(NON_AUTHORITATIVE_INFORMATION,   TF("Non-Authoritative Information"))
    CASE_RETSTR(NO_CONTENT,                      TF("No Content"))
    CASE_RETSTR(RESET_CONTENT,                   TF("Reset Content"))
    CASE_RETSTR(PARTIAL_CONTENT,                 TF("Partial Content"))

    CASE_RETSTR(MULTIPLE_CHOICES,                TF("Multiple Choices"))
    CASE_RETSTR(MOVED_PERMANENTLY,               TF("Moved Permanently"))
    CASE_RETSTR(FOUND,                           TF("Found"))
    CASE_RETSTR(SEE_OTHER,                       TF("See Other"))
    CASE_RETSTR(NOT_MODIFIED,                    TF("Not Modified"))
    CASE_RETSTR(USE_PROXY,                       TF("Use Proxy"))
    CASE_RETSTR(TEMPORARY_REDIRECT,              TF("Temporary Redirect"))

    CASE_RETSTR(BAD_REQUEST,                     TF("Bad Request"))
    CASE_RETSTR(UNAUTHORIZED,                    TF("Unauthorized"))
    CASE_RETSTR(PAYMENT_REQUIRED,                TF("Payment Required"))
    CASE_RETSTR(FORBIDDEN,                       TF("Forbidden"))
    CASE_RETSTR(NOT_FOUND,                       TF("Not Found"))
    CASE_RETSTR(METHOD_NOT_ALLOWED,              TF("Method Not Allowed"))
    CASE_RETSTR(NOT_ACCEPTABLE,                  TF("Not Acceptable"))
    CASE_RETSTR(PROXY_AUTHENTICATION_REQUIRED,   TF("Proxy Authentication Required"))
    CASE_RETSTR(REQUEST_TIMEOUT,                 TF("Request Timeout"))
    CASE_RETSTR(CONFLICT,                        TF("Conflict"))
    CASE_RETSTR(GONE,                            TF("Gone"))
    CASE_RETSTR(LENGTH_REQUIRED,                 TF("Length Required"))
    CASE_RETSTR(PRECONDITION_FAILED,             TF("Precondition Failed"))
    CASE_RETSTR(REQUEST_ENTITY_TOO_LARGE,        TF("Request Entity Too Large"))
    CASE_RETSTR(REQUEST_URI_TOO_LONG,            TF("Request-URI Too Long"))
    CASE_RETSTR(UNSUPPORTED_MEDIA_TYPE,          TF("Unsupported Media Type"))
    CASE_RETSTR(REQUESTED_RANGE_NOT_SATISFIABLE, TF("Request Range Not Satisfiable"))
    CASE_RETSTR(EXPECTATION_FAILED,              TF("Expectation Failed"))
    CASE_RETSTR(UPGRADE_REQUIRED,                TF("Upgrade Required"))

    CASE_RETSTR(INTERNAL_SERVER_ERROR,           TF("Internal Server Error"))
    CASE_RETSTR(NOT_IMPLEMENTED,                 TF("Not Implemented"))
    CASE_RETSTR(BAD_GATEWAY,                     TF("Bad Gateway"))
    CASE_RETSTR(SERVICE_UNAVAILABLE,             TF("Service Unavailable"))
    CASE_RETSTR(GATEWAY_TIMEOUT,                 TF("Gateway Timeout"))
    CASE_RETSTR(HTTP_VERSION_NOT_SUPPORTED,      TF("HTTP Version Not Supported"))
    default:
        {
            return TF("Unknown HTTP Status");
        }
    }
}

INLINE bool CHTTPTraits::IsSkipChar(Char c)
{
    return ((c == ' ') || (c == '\r') || (c == '\n'));
}

INLINE bool CHTTPTraits::IsSkipCharWithSpecial(Char c, Char x)
{
    return ((c == x) || (c == ' ') || (c == '\r') || (c == '\n'));
}

INLINE bool CHTTPTraits::SkipChars(PCStr& pszBuf, Char x)
{
    if (x == 0)
    {
        while (IsSkipChar(*pszBuf))
        {
            ++pszBuf;
        }
    }
    else
    {
        while (IsSkipCharWithSpecial(*pszBuf, x))
        {
            ++pszBuf;
        }
    }
    return (*pszBuf != 0);
}

INLINE size_t CHTTPTraits::GetLength(PCStr pszBegin, PCStr pszEnd)
{
    while (IsSkipChar(*(pszEnd - 1)))
    {
        --pszEnd;
    }
    if (pszEnd > pszBegin)
    {
        return  (pszEnd - pszBegin);
    }
    return 0;
}

INLINE void CHTTPTraits::FormatChunkData(CCString& strFormat, PCStr pszChunk, size_t stSize)
{
    strFormat.AppendFormat("%x\r\n", stSize);
    strFormat.AppendBuffer(pszChunk, stSize);
    strFormat += "\r\n";
}

INLINE void CHTTPTraits::FormatChunkData(CStream& Stream, PByte pbChunk, size_t stSize)
{
    CCString strTemp;
    strTemp.Format("%x\r\n", stSize);
    Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
    Stream.Write(pbChunk, stSize);
    Stream.Write((PByte)"\r\n", 2);
}

INLINE void CHTTPTraits::FormatChunkEOF(CCString& strFormat)
{
    strFormat += "0\r\n\r\n";
}

INLINE void CHTTPTraits::FormatChunkEOF(CStream& Stream)
{
    Stream.Write((PByte)"0\r\n\r\n", 5);
}

///////////////////////////////////////////////////////////////////
// CWSTraits
SELECTANY CPCStr CWSTraits::AcceptUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

INLINE CPCStr CWSTraits::StatusToString(Int nStatus)
{
    switch (nStatus)
    {
    CASE_RETSTR(NORMAL,                 "Normal Closure")
    CASE_RETSTR(GOING_AWAY,             "Going Away")
    CASE_RETSTR(PROTOCOL_ERROR,         "Protocol Error")
    CASE_RETSTR(UNSUPPORTED,            "Unsupported")
    CASE_RETSTR(RESERVED1,              "Reserved")
    CASE_RETSTR(NO_STATUS,              "No Status Code")
    CASE_RETSTR(CLOSED_ABNORMALLY,      "Closed Abnormally")
    CASE_RETSTR(DATA_NOT_CONSISTENT,    "Data Not Consistent")
    CASE_RETSTR(POLICY_VIOLATE,         "Policy Violate")
    CASE_RETSTR(TOO_BIG_PROCESS,        "Too Big Process")
    CASE_RETSTR(NO_NEGOTIATE_EXTENSION, "No Negotiate Extension")
    CASE_RETSTR(PREVENT_REQUEST,        "Prevent Request")
    CASE_RETSTR(TLS_HANDSHAKE_FAILED,   "TLS Handshake Failed")
    default:
        {
            return "Unknown WS Status";
        }
    }
}

INLINE CPCXStr CWSTraits::StatusToLocalString(Int nStatus)
{
    switch (nStatus)
    {
    CASE_RETSTR(NORMAL,                 TF("Normal Closure"))
    CASE_RETSTR(GOING_AWAY,             TF("Going Away"))
    CASE_RETSTR(PROTOCOL_ERROR,         TF("Protocol Error"))
    CASE_RETSTR(UNSUPPORTED,            TF("Unsupported"))
    CASE_RETSTR(RESERVED1,              TF("Reserved"))
    CASE_RETSTR(NO_STATUS,              TF("No Status Code"))
    CASE_RETSTR(CLOSED_ABNORMALLY,      TF("Closed Abnormally"))
    CASE_RETSTR(DATA_NOT_CONSISTENT,    TF("Data Not Consistent"))
    CASE_RETSTR(POLICY_VIOLATE,         TF("Policy Violate"))
    CASE_RETSTR(TOO_BIG_PROCESS,        TF("Too Big Process"))
    CASE_RETSTR(NO_NEGOTIATE_EXTENSION, TF("No Negotiate Extension"))
    CASE_RETSTR(PREVENT_REQUEST,        TF("Prevent Request"))
    CASE_RETSTR(TLS_HANDSHAKE_FAILED,   TF("TLS Handshake Failed"))
    default:
        {
            return TF("Unknown WS Status");
        }
    }
}

INLINE bool CWSTraits::IsControlFrame(Int nFrameCode)
{
    return ((nFrameCode & WS_OPCODE_MASK) >= WS_CLOSE);
}

INLINE bool CWSTraits::IsFinalFrame(Int nFrameCode)
{
    return ((nFrameCode & WS_FINAL) == WS_FINAL);
}


///////////////////////////////////////////////////////////////////
// CURI::tagURI
INLINE CURI::tagURI::tagURI(void)
{
    strPath = "/";
    uPort   = 0;
}

INLINE CURI::tagURI::~tagURI(void)
{
}

INLINE CURI::tagURI::tagURI(const CURI::tagURI& aSrc)
: strScheme(aSrc.strScheme)
, strUserInfo(aSrc.strUserInfo)
, strHost(aSrc.strHost)
, strPath(aSrc.strPath)
, strQuery(aSrc.strQuery)
, strFragment(aSrc.strFragment)
, uPort(aSrc.uPort)
{
}

INLINE CURI::tagURI& CURI::tagURI::operator=(const CURI::tagURI& aSrc)
{
    if (this != &aSrc)
    {
        strScheme   = aSrc.strScheme;
        strUserInfo = aSrc.strUserInfo;
        strHost     = aSrc.strHost;
        strPath     = aSrc.strPath;
        strQuery    = aSrc.strQuery;
        strFragment = aSrc.strFragment;
        uPort       = aSrc.uPort;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CURI::tagURI::tagURI(CURI::tagURI&& aSrc)
: strScheme(std::move(aSrc.strScheme))
, strUserInfo(std::move(aSrc.strUserInfo))
, strHost(std::move(aSrc.strHost))
, strPath(std::move(aSrc.strPath))
, strQuery(std::move(aSrc.strQuery))
, strFragment(std::move(aSrc.strFragment))
, uPort(aSrc.uPort)
{
}

INLINE CURI::tagURI& CURI::tagURI::operator=(CURI::tagURI&& aSrc)
{
    if (this != &aSrc)
    {
        strScheme   = std::move(aSrc.strScheme);
        strUserInfo = std::move(aSrc.strUserInfo);
        strHost     = std::move(aSrc.strHost);
        strPath     = std::move(aSrc.strPath);
        strQuery    = std::move(aSrc.strQuery);
        strFragment = std::move(aSrc.strFragment);
        uPort       = aSrc.uPort;
    }
    return (*this);
}
#endif

INLINE void CURI::tagURI::Reset(void)
{
    strScheme.Empty();
    strUserInfo.Empty();
    strHost.Empty();
    strPath = "/";
    strQuery.Empty();
    strFragment.Empty();
    uPort = 0;
}

INLINE CCString CURI::tagURI::Get(void)
{
    strScheme.Lower();
    strHost.Lower();

    if (strHost.IsEmpty() == false)
    {
        if (strPath.IsEmpty())
        {
            strPath = "/";
        }
        else if (strPath[0] != '/')
        {
            strPath.Insert(0, '/');
        }
    }

    CCString strRet;

    if (strScheme.Length() > 0)
    {
        strRet += strScheme;
        strRet += ':';
    }
    if (strHost.Length() > 0)
    {
        strRet += "//";

        if (strUserInfo.Length() > 0)
        {
            strRet += strUserInfo;
            strRet += '@';
        }
        strRet += strHost;
        if (uPort > 0)
        {
            strRet.AppendFormat(":%d", uPort);
        }
    }
    if (strPath.Length() > 0)
    {
        strRet += strPath;
    }
    if (strQuery.Length() > 0)
    {
        strRet += '?';
        strRet += strQuery;
    }
    if (strFragment.Length() > 0)
    {
        strRet += '#';
        strRet += strFragment;
    }
    return strRet;
}

INLINE bool CURI::Encode(const CCString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
    for (size_t i = 0; i < stLen; ++i)
    {
        if (CURI::UnreservedChar(strIn[i]) || CURI::ReservedChar(strIn[i]))
        {
            strOut += strIn[i];
        }
        else
        {
            strOut += '%';
            strOut += (Char)CharToHex(strIn[i] >> 4);
            strOut += (Char)CharToHex(strIn[i] & 0x0F);
        }
    }
    return true;
}

INLINE bool CURI::Decode(const CCString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
    for (size_t i = 0; i < stLen; ++i)
    {
        if (strIn[i] != '%')
        {
            strOut += strIn[i];
        }
        else if (i < (stLen - 2))
        {
            UChar uc1 = HexToChar(strIn[i + 1]);
            UChar uc2 = HexToChar(strIn[i + 2]);
            i += 2;

            strOut += (Char)((uc1 << 4) + uc2);
        }
    }
    return true;
}

INLINE bool CURI::UnreservedChar(Char c)
{
    return (CChar::IsAlnum(c) || (c == '-') || (c == '.') || (c == '_') || (c == '~'));
}

INLINE bool CURI::GenDelimChar(Char c)
{
    switch (c)
    {
    case ':':
    case '/':
    case '?':
    case '#':
    case '[':
    case ']':
    case '@':
        {
            return true;
        }
    default:
        {
            return false;
        }
    }
}

INLINE bool CURI::SubDelimChar(Char c)
{
    switch (c)
    {
    case '!':
    case '$':
    case '&':
    case '\'':
    case '(':
    case ')':
    case '*':
    case '+':
    case ',':
    case ';':
    case '=':
        {
            return true;
        }
    default:
        {
            return false;
        }
    }
}

INLINE bool CURI::ReservedChar(Char c)
{
    return (GenDelimChar(c) || SubDelimChar(c));
}

INLINE bool CURI::SchemeChar(Char c)
{
    return (CChar::IsAlnum(c) || (c == '+') || (c == '-') || (c == '.'));
}

INLINE bool CURI::UserInfoChar(Char c)
{
    return (UnreservedChar(c) || SubDelimChar(c) || (c == '%') || (c == ':'));
}

INLINE bool CURI::AuthorityChar(Char c)
{
    return (UnreservedChar(c) || SubDelimChar(c) || (c == '%') || (c == '@') || (c == ':') || (c == '[') || (c == ']'));
}

INLINE bool CURI::PathChar(Char c)
{
    return (UnreservedChar(c) || SubDelimChar(c) || (c == '%') || (c == '/') || (c == ':') || (c == '@'));
}

INLINE bool CURI::QueryChar(Char c)
{
    return (UnreservedChar(c) || SubDelimChar(c) || (c == '%') || (c == '/') || (c == ':') || (c == '@') || (c == '?'));
}

INLINE bool CURI::FragmentChar(Char c)
{
    return (UnreservedChar(c) || SubDelimChar(c) || (c == '%') || (c == '/') || (c == ':') || (c == '@') || (c == '?'));
}

INLINE UChar CURI::CharToHex(UChar uc)
{
    // 0xa(10)--->'A'(65); 0x0(0)--->'0'(48)
    return (UChar)((uc > 9) ? (uc + 55) : (uc + 48));
}

INLINE UChar CURI::HexToChar(UChar uc)
{
    UChar ucChar = '?';
    if ((uc >= 'A') && (uc <= 'Z'))
    {
        ucChar = uc - 'A' + 10;
    }
    else if ((uc >= 'a') && (uc <= 'z'))
    {
        ucChar = uc - 'a' + 10;
    }
    else if ((uc >= '0') && (uc <= '9'))
    {
        ucChar = uc - '0';
    }
    return ucChar;
}

INLINE bool CURI::Parse(CCString& strUri, URI& Uri)
{
    bool bScheme = false;
    Int  nPos = 0;
    for (Int i = 0; (strUri[i] != '/') && (strUri[i] != 0); ++i)
    {
        if (strUri[i] == ':')
        {
            bScheme = true;
            break;
        }
    }
    if (bScheme)
    {
        // scheme's first MUST be a letter
        if (strUri.IsAlphaChar(0) == false)
        {
            return false;
        }
        for (nPos = 0; strUri[nPos] != ':'; ++nPos)
        {
            if (SchemeChar(strUri[nPos]) == false)
            {
                return false;
            }
        }
        Uri.strScheme = strUri.Left((size_t)nPos);
        ++nPos; // skip :
    }
    // authority
    if ((strUri[nPos] == '/') && (strUri[nPos + 1] == '/'))
    {
        Int nStart = nPos + 2; // skip //
        for (nPos = nStart; (strUri[nPos] != '/') && (strUri[nPos] != '?') && (strUri[nPos] != '#') && (strUri[nPos] != 0); ++nPos)
        {
            if (AuthorityChar(strUri[nPos]) == false)
            {
                return false;
            }
        }

        if (nPos > nStart)
        {
            Int nUserInfo = strUri.Find('@', (size_t)nStart);
            Int nHost     = (nUserInfo > 0) ? (nUserInfo + 1) : nStart;
            Int nTemp     = strUri.Find(':', (size_t)nHost, true);
            if (nTemp > 0)
            {
                size_t stIndex = (size_t)nTemp + 1;
                Uri.uPort = (UInt)strUri.ToULong(stIndex);
            }
            else
            {
                nTemp = nPos;
            }
            Uri.strHost = strUri.Mid((size_t)nHost, (size_t)(nTemp - nHost));

            if (nUserInfo > 0)
            {
                for (nTemp = 0; nTemp < nUserInfo; ++nTemp)
                {
                    if (UserInfoChar(strUri[nTemp]) == false)
                    {
                        break;
                    }
                }
                if (nTemp == nUserInfo)
                {
                    Uri.strUserInfo = strUri.Mid((size_t)nStart, (size_t)(nUserInfo - nStart));
                }
            }
        }
    }
    // path
    if ((strUri[nPos] == '/') || PathChar(strUri[nPos]))
    {
        Int nStart = nPos;
        for (; (strUri[nPos] != '?') && (strUri[nPos] != '#') && (strUri[nPos] != 0); ++nPos)
        {
            if (PathChar(strUri[nPos]) == false)
            {
                return false;
            }
        }
        Uri.strPath = strUri.Mid((size_t)nStart, (size_t)(nPos - nStart));
    }
    // query
    if (strUri[nPos] == '?')
    {
        Int nStart = nPos + 1; // skip ?
        for (nPos = nStart; (strUri[nPos] != '#') && (strUri[nPos] != 0); ++nPos)
        {
            if (QueryChar(strUri[nPos]) == false)
            {
                return false;
            }
        }
        Uri.strQuery = strUri.Mid((size_t)nStart, (size_t)(nPos - nStart));
    }
    // fragment
    if (strUri[nPos] == '#')
    {
        Int nStart = nPos + 1; // skip #
        for (nPos = nStart; strUri[nPos] != 0; ++nPos)
        {
            if (FragmentChar(strUri[nPos]) == false)
            {
                return false;
            }
        }
        Uri.strFragment = strUri.Mid((size_t)nStart, (size_t)(nPos - nStart));
    }
    return true;
}

INLINE bool CURI::Check(CCString& strUri)
{
    URI Uri;
    return Parse(strUri, Uri);
}

INLINE CURI::CURI(void)
{
}

INLINE CURI::~CURI(void)
{
}

INLINE CURI::CURI(const CURI& aSrc)
: m_Uri(aSrc.m_Uri)
{
}

INLINE CURI::CURI(const URI& aSrc)
: m_Uri(aSrc)
{

}

INLINE CURI& CURI::operator=(const CURI& aSrc)
{
    if (this != &aSrc)
    {
        m_Uri = aSrc.m_Uri;
    }
    return (*this);
}

INLINE CURI& CURI::operator=(const URI& aSrc)
{
    if (&m_Uri != &aSrc)
    {
        m_Uri = aSrc;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CURI::CURI(CURI&& aSrc)
: m_Uri(std::move(aSrc.m_Uri))
{
}

INLINE CURI::CURI(URI&& aSrc)
: m_Uri(std::move(aSrc))
{
}

INLINE CURI& CURI::operator=(CURI&& aSrc)
{
    if (this != &aSrc)
    {
        m_Uri = std::move(aSrc.m_Uri);
    }
    return (*this);
}

INLINE CURI& CURI::operator=(URI&& aSrc)
{
    if (&m_Uri != &aSrc)
    {
        m_Uri = std::move(aSrc);
    }
    return (*this);
}
#endif

INLINE bool CURI::operator==(const CURI& aSrc) const
{
    return this->operator==(aSrc.m_Uri);
}

INLINE bool CURI::operator==(const URI& Uri) const
{
    if (m_Uri.strScheme != Uri.strScheme)
    {
        return false;
    }
    if (m_Uri.strUserInfo != Uri.strUserInfo)
    {
        return false;
    }
    if (m_Uri.strHost != Uri.strHost)
    {
        return false;
    }
    if (m_Uri.strPath != Uri.strPath)
    {
        return false;
    }
    if (m_Uri.strQuery != Uri.strQuery)
    {
        return false;
    }
    if (m_Uri.strFragment != Uri.strFragment)
    {
        return false;
    }
    if (m_Uri.uPort != Uri.uPort)
    {
        return false;
    }
    return true;
}

INLINE bool CURI::Parse(PCStr pszUri)
{
    m_Uri.Reset();
    CCString strUri = pszUri;
    return Parse(strUri, m_Uri);
}

INLINE bool CURI::Parse(CCString& strUri)
{
    m_Uri.Reset();
    return Parse(strUri, m_Uri);
}

INLINE const CCString& CURI::GetScheme(void) const
{
    return m_Uri.strScheme;
}

INLINE const CCString& CURI::GetUserInfo(void) const
{
    return m_Uri.strUserInfo;
}

INLINE const CCString& CURI::GetHost(void) const
{
    return m_Uri.strHost;
}

INLINE const CCString& CURI::GetPath(void) const
{
    return m_Uri.strPath;
}

INLINE const CCString& CURI::GetQuery(void) const
{
    return m_Uri.strQuery;
}

INLINE const CCString& CURI::GetFragment(void) const
{
    return m_Uri.strFragment;
}

INLINE UInt CURI::GetPort(void) const
{
    return m_Uri.uPort;
}

INLINE CURI::URI CURI::GetAuthority(void) const
{
    URI Uri;
    Uri.strUserInfo = m_Uri.strUserInfo;
    Uri.strHost = m_Uri.strHost;
    Uri.uPort = m_Uri.uPort;
    return Uri;
}

INLINE CURI::URI CURI::GetResource(void) const
{
    URI Uri;
    Uri.strPath = m_Uri.strPath;
    Uri.strQuery = m_Uri.strQuery;
    Uri.strFragment = m_Uri.strFragment;
    return Uri;
}

INLINE CURI& CURI::SetScheme(CCString& strScheme)
{
    m_Uri.strScheme = strScheme;
    return (*this);
}

INLINE CURI& CURI::SetUserInfo(CCString& strUserInfo)
{
    m_Uri.strUserInfo = strUserInfo;
    return (*this);
}

INLINE CURI& CURI::SetHost(CCString& strHost)
{
    m_Uri.strHost = strHost;
    return (*this);
}

INLINE CURI& CURI::SetPath(CCString& strPath)
{
    m_Uri.strPath = strPath;
    return (*this);
}

INLINE CURI& CURI::SetQuery(CCString& strQuery)
{
    m_Uri.strQuery = strQuery;
    return (*this);
}

INLINE CURI& CURI::SetFragment(CCString& strFragment)
{
    m_Uri.strFragment = strFragment;
    return (*this);
}

INLINE CURI& CURI::SetPort(UInt uPort)
{
    m_Uri.uPort = uPort;
    return (*this);
}

INLINE void CURI::Reset(void)
{
    m_Uri.Reset();
}

INLINE CCString CURI::ToString(void)
{
    return m_Uri.Get();
}

///////////////////////////////////////////////////////////////////
// CHTTPHeader
INLINE CHTTPHeader::CHTTPHeader(void)
{
}

INLINE CHTTPHeader::~CHTTPHeader(void)
{
}

INLINE CHTTPHeader::CHTTPHeader(const CHTTPHeader& aSrc)
: m_Header(aSrc.m_Header)
{
}

INLINE CHTTPHeader& CHTTPHeader::operator=(const CHTTPHeader& aSrc)
{
    if (this != &aSrc)
    {
        m_Header = aSrc.m_Header;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPHeader::CHTTPHeader(CHTTPHeader&& aSrc)
: m_Header(std::move(aSrc.m_Header))
{
}

INLINE CHTTPHeader& CHTTPHeader::operator=(CHTTPHeader&& aSrc)
{
    if (this != &aSrc)
    {
        m_Header = std::move(aSrc.m_Header);
    }
    return (*this);
}
#endif

INLINE void CHTTPHeader::SetAt(const CCString& strName, const CCString& strValue)
{
    m_Header.SetAt(strName, strValue);
}

INLINE void CHTTPHeader::SetAt(const CCString& strName, CCString& strValue)
{
    m_Header.SetAt(strName, strValue);
}

INLINE void CHTTPHeader::SetAt(const CCString& strName, PCStr pszValue)
{
    m_Header.SetAt(strName, pszValue);
}

INLINE void CHTTPHeader::SetAt(PCStr pszName, PCStr pszValue)
{
    m_Header.SetAt(pszName, pszValue);
}

INLINE void CHTTPHeader::SetAt(PCStr pszName, const CCString& strValue)
{
    m_Header.SetAt(pszName, strValue);
}

INLINE void CHTTPHeader::SetAt(PCStr pszName, CCString& strValue)
{
    m_Header.SetAt(pszName, strValue);
}

INLINE void CHTTPHeader::SetAt(PCStr pszName, const CCStringRef& strValueRef)
{
    m_Header.SetAt(pszName, strValueRef);
}

INLINE void CHTTPHeader::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Header.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPHeader::Close(void)
{
    m_Header.RemoveAll();
}

INLINE CCString CHTTPHeader::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_HEADER* pPair = m_Header.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPHeader::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_HEADER* pPair = m_Header.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_HEADER& CHTTPHeader::Header(void) const
{
    return m_Header;
}

INLINE CHTTPTraits::MAP_HEADER& CHTTPHeader::Header(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::MAP_HEADER& CHTTPHeader::operator*(void) const
{
    return m_Header;
}

INLINE CHTTPTraits::MAP_HEADER& CHTTPHeader::operator*(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::MAP_HEADER* CHTTPHeader::operator->(void) const
{
    return &m_Header;
}

INLINE CHTTPTraits::MAP_HEADER* CHTTPHeader::operator->(void)
{
    return &m_Header;
}

///////////////////////////////////////////////////////////////////
// CHTTPCookie
INLINE CHTTPCookie::CHTTPCookie(void)
{
}

INLINE CHTTPCookie::~CHTTPCookie(void)
{
}

INLINE CHTTPCookie::CHTTPCookie(const CHTTPCookie& aSrc)
: m_Cookie(aSrc.m_Cookie)
{
}

INLINE CHTTPCookie& CHTTPCookie::operator=(const CHTTPCookie& aSrc)
{
    if (this != &aSrc)
    {
        m_Cookie = aSrc.m_Cookie;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPCookie::CHTTPCookie(CHTTPCookie&& aSrc)
: m_Cookie(std::move(aSrc.m_Cookie))
{
}

INLINE CHTTPCookie& CHTTPCookie::operator=(CHTTPCookie&& aSrc)
{
    if (this != &aSrc)
    {
        m_Cookie = std::move(aSrc.m_Cookie);
    }
    return (*this);
}
#endif

INLINE void CHTTPCookie::SetAt(const CCString& strName, const CCString& strValue)
{
    m_Cookie.SetAt(strName, strValue);
}

INLINE void CHTTPCookie::SetAt(const CCString& strName, CCString& strValue)
{
    m_Cookie.SetAt(strName, strValue);
}

INLINE void CHTTPCookie::SetAt(const CCString& strName, PCStr pszValue)
{
    m_Cookie.SetAt(strName, pszValue);
}

INLINE void CHTTPCookie::SetAt(PCStr pszName, PCStr pszValue)
{
    m_Cookie.SetAt(pszName, pszValue);
}

INLINE void CHTTPCookie::SetAt(PCStr pszName, const CCString& strValue)
{
    m_Cookie.SetAt(pszName, strValue);
}

INLINE void CHTTPCookie::SetAt(PCStr pszName, CCString& strValue)
{
    m_Cookie.SetAt(pszName, strValue);
}

INLINE void CHTTPCookie::SetAt(PCStr pszName, const CCStringRef& strValueRef)
{
    m_Cookie.SetAt(pszName, strValueRef);
}

INLINE void CHTTPCookie::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Cookie.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPCookie::Close(void)
{
    m_Cookie.RemoveAll();
}

INLINE void CHTTPCookie::Parse(PCStr pszCookie)
{
    CCString strName;
    CCString strValue;

    while (SkipChars(pszCookie))
    {
        PCStr pszEnd = CChar::Chr(pszCookie, ';');
        PCStr pszKey = CChar::Chr(pszCookie, '=');
        if (pszKey != nullptr)
        {
            if (pszCookie < pszKey)
            {
                strName.FillBuffer(pszCookie, (pszKey - pszCookie));
                strName.Trim();

                pszCookie = pszKey + 1; //
                if (SkipChars(pszCookie) == false)
                {
                    break;
                }
                if ((pszCookie < pszEnd) || (pszEnd == nullptr))
                {
                    if (pszEnd != nullptr)
                    {
                        strValue.FillBuffer(pszCookie, (pszEnd - pszCookie));
                    }
                    else
                    {
                        strValue = pszCookie;
                    }
                    strValue.Trim();

                    DEV_DEBUG(TF("CHTTPCookie::Parse %s : %s"), *strName, *strValue); // !!!charset
#ifndef __MODERN_CXX_NOT_SUPPORTED
                    m_Cookie.Add(std::move(strName), std::move(strValue));
#else
                    m_Cookie.Add(strName, strValue);
#endif
                }
                if (pszEnd == nullptr)
                {
                    break;
                }
                pszCookie = pszEnd + 1; //
            }
            else
            {
                pszCookie = pszKey + 1; //
            }
        }
        else if (pszEnd != nullptr)
        {
            if (pszCookie < pszEnd)
            {
                strName.FillBuffer(pszCookie, (pszEnd - pszCookie));
                strName.Trim();
                DEV_DEBUG(TF("CHTTPCookie::Parse2 %s : null"), *strName); // !!!charset
#ifndef __MODERN_CXX_NOT_SUPPORTED
                m_Cookie.Add(std::move(strName));
#else
                m_Cookie.Add(strName);
#endif
            }
            pszCookie = pszEnd + 1; //
        }
        else
        {
            DEV_DEBUG(TF("CHTTPCookie::Parse3 %s : null"), pszCookie); // !!!charset
            m_Cookie.Add(pszCookie);
            break;
        }
    }
}

INLINE size_t CHTTPCookie::FormatLength(void) const
{
    size_t stSize = 0;
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE* pPair = m_Cookie.GetNext(index);
        stSize += pPair->m_K.Length() + 1; // '='
        stSize += pPair->m_V.Length();
        if (index != nullptr)
        {
            stSize += 1; // ';'
        }
    }
    return stSize;
}

INLINE void CHTTPCookie::Format(CCString& strCookie) const
{
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE* pPair = m_Cookie.GetNext(index);
        strCookie.AppendFormat("%s=%s", pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
        if (index != nullptr)
        {
            strCookie += ';';
        }
    }
}

INLINE void CHTTPCookie::Format(CStream& Stream) const
{
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE* pPair = m_Cookie.GetNext(index);
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream << '=';
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            Stream <<';';
        }
    }
}

INLINE CCString CHTTPCookie::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_COOKIE* pPair = m_Cookie.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPCookie::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_COOKIE* pPair = m_Cookie.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_COOKIE& CHTTPCookie::Cookie(void) const
{
    return m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE& CHTTPCookie::Cookie(void)
{
    return m_Cookie;
}

INLINE const CHTTPTraits::MAP_COOKIE& CHTTPCookie::operator*(void) const
{
    return m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE& CHTTPCookie::operator*(void)
{
    return m_Cookie;
}

INLINE const CHTTPTraits::MAP_COOKIE* CHTTPCookie::operator->(void) const
{
    return &m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE* CHTTPCookie::operator->(void)
{
    return &m_Cookie;
}

///////////////////////////////////////////////////////////////////
// CHTTPParameter
INLINE CHTTPParameter::CHTTPParameter(void)
{
}

INLINE CHTTPParameter::~CHTTPParameter(void)
{
}

INLINE CHTTPParameter::CHTTPParameter(const CHTTPParameter& aSrc)
: m_Param(aSrc.m_Param)
{
}

INLINE CHTTPParameter& CHTTPParameter::operator=(const CHTTPParameter& aSrc)
{
    if (this != &aSrc)
    {
        m_Param = aSrc.m_Param;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPParameter::CHTTPParameter(CHTTPParameter&& aSrc)
: m_Param(std::move(aSrc.m_Param))
{
}

INLINE CHTTPParameter& CHTTPParameter::operator=(CHTTPParameter&& aSrc)
{
    if (this != &aSrc)
    {
        m_Param = std::move(aSrc.m_Param);
    }
    return (*this);
}
#endif

INLINE void CHTTPParameter::SetAt(const CCString& strName, const CCString& strValue)
{
    m_Param.SetAt(strName, strValue);
}

INLINE void CHTTPParameter::SetAt(const CCString& strName, CCString& strValue)
{
    m_Param.SetAt(strName, strValue);
}

INLINE void CHTTPParameter::SetAt(const CCString& strName, PCStr pszValue)
{
    m_Param.SetAt(strName, pszValue);
}

INLINE void CHTTPParameter::SetAt(PCStr pszName, PCStr pszValue)
{
    m_Param.SetAt(pszName, pszValue);
}

INLINE void CHTTPParameter::SetAt(PCStr pszName, const CCString& strValue)
{
    m_Param.SetAt(pszName, strValue);
}

INLINE void CHTTPParameter::SetAt(PCStr pszName, CCString& strValue)
{
    m_Param.SetAt(pszName, strValue);
}

INLINE void CHTTPParameter::SetAt(PCStr pszName, const CCStringRef& strValueRef)
{
    m_Param.SetAt(pszName, strValueRef);
}

INLINE void CHTTPParameter::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Param.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPParameter::Close(void)
{
    m_Param.RemoveAll();
}

INLINE void CHTTPParameter::Parse(PCStr pszParameter)
{
    CCString strName;
    CCString strValue;

    while (SkipChars(pszParameter))
    {
        PCStr pszEnd = CChar::Chr(pszParameter, '&');
        PCStr pszKey = CChar::Chr(pszParameter, '=');
        if (pszKey != nullptr)
        {
            if (pszParameter < pszKey)
            {
                strName.FillBuffer(pszParameter, (pszKey - pszParameter));
                strName.Trim();

                pszParameter = pszKey + 1; //
                if (SkipChars(pszParameter) == false)
                {
                    break;
                }
                if ((pszParameter < pszEnd) || (pszEnd == nullptr))
                {
                    if (pszEnd != nullptr)
                    {
                        if (pszParameter < pszEnd)
                        {
                            strValue.FillBuffer(pszParameter, (pszEnd - pszParameter));
                        }
                    }
                    else
                    {
                        strValue = pszParameter;
                    }
                    strValue.Trim();

                    DEV_DEBUG(TF("CHTTPParameter::Parse %s : %s"), *strName, *strValue); // !!!charset
    #ifndef __MODERN_CXX_NOT_SUPPORTED
                    m_Param.Add(std::move(strName), std::move(strValue));
    #else
                    m_Param.Add(strName, strValue);
    #endif
                }
                if (pszEnd == nullptr)
                {
                    break;
                }
                pszParameter = pszEnd + 1; //
            }
            else
            {
                pszParameter = pszKey + 1; //
            }
        }
        else if (pszEnd != nullptr)
        {
            if (pszParameter < pszEnd)
            {
                strName.FillBuffer(pszParameter, (pszEnd - pszParameter));
                strName.Trim();
                DEV_DEBUG(TF("CHTTPParameter::Parse2 %s : null"), *strName); // !!!charset
#ifndef __MODERN_CXX_NOT_SUPPORTED
                m_Param.Add(std::move(strName));
#else
                m_Param.Add(strName);
#endif
            }
            pszParameter = pszEnd + 1; //
        }
        else
        {
            DEV_DEBUG(TF("CHTTPParameter::Parse3 %s : null"), pszParameter); // !!!charset
            m_Param.Add(pszParameter);
            break;
        }
    }
}

INLINE size_t CHTTPParameter::FormatLength(void) const
{
    size_t stSize = 0;
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM* pPair = m_Param.GetNext(index);
        stSize += pPair->m_K.Length() + 1; // '='
        stSize += pPair->m_V.Length();
        if (index != nullptr)
        {
            stSize += 1; // '&'
        }
    }
    return stSize;
}

INLINE void CHTTPParameter::Format(CCString& strParameter) const
{
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM* pPair = m_Param.GetNext(index);
        strParameter.AppendFormat("%s=%s", pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
        if (index != nullptr)
        {
            strParameter += '&';
        }
    }
}

INLINE void CHTTPParameter::Format(CStream& Stream) const
{
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM* pPair = m_Param.GetNext(index);
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream << '=';
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            Stream << '&';
        }
    }
}

INLINE CCString CHTTPParameter::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_PARAM* pPair = m_Param.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPParameter::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_PARAM* pPair = m_Param.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_PARAM& CHTTPParameter::Param(void) const
{
    return m_Param;
}

INLINE CHTTPTraits::MAP_PARAM& CHTTPParameter::Param(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_PARAM& CHTTPParameter::operator*(void) const
{
    return m_Param;
}

INLINE CHTTPTraits::MAP_PARAM& CHTTPParameter::operator*(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_PARAM* CHTTPParameter::operator->(void) const
{
    return &m_Param;
}

INLINE CHTTPTraits::MAP_PARAM* CHTTPParameter::operator->(void)
{
    return &m_Param;
}

///////////////////////////////////////////////////////////////////
// CHTTPHeaderRef
INLINE CHTTPHeaderRef::CHTTPHeaderRef(void)
{
}

INLINE CHTTPHeaderRef::~CHTTPHeaderRef(void)
{
}

INLINE CHTTPHeaderRef::CHTTPHeaderRef(const CHTTPHeaderRef& aSrc)
: m_Header(aSrc.m_Header)
{
}

INLINE CHTTPHeaderRef& CHTTPHeaderRef::operator=(const CHTTPHeaderRef& aSrc)
{
    if (this != &aSrc)
    {
        m_Header = aSrc.m_Header;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPHeaderRef::CHTTPHeaderRef(CHTTPHeaderRef&& aSrc)
: m_Header(std::move(aSrc.m_Header))
{
}

INLINE CHTTPHeaderRef& CHTTPHeaderRef::operator=(CHTTPHeaderRef&& aSrc)
{
    if (this != &aSrc)
    {
        m_Header = std::move(aSrc.m_Header);
    }
    return (*this);
}
#endif

INLINE void CHTTPHeaderRef::SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue)
{
    CCStringRef strNameRef(pszName, stName);
    CCStringRef strValueRef(pszValue, stValue);
    m_Header.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPHeaderRef::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Header.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPHeaderRef::Close(void)
{
    m_Header.RemoveAll();
}

INLINE CCString CHTTPHeaderRef::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_HEADER_REF* pPair = m_Header.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPHeaderRef::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_HEADER_REF* pPair = m_Header.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_HEADER_REF& CHTTPHeaderRef::Header(void) const
{
    return m_Header;
}

INLINE CHTTPTraits::MAP_HEADER_REF& CHTTPHeaderRef::Header(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::MAP_HEADER_REF& CHTTPHeaderRef::operator*(void) const
{
    return m_Header;
}

INLINE CHTTPTraits::MAP_HEADER_REF& CHTTPHeaderRef::operator*(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::MAP_HEADER_REF* CHTTPHeaderRef::operator->(void) const
{
    return &m_Header;
}

INLINE CHTTPTraits::MAP_HEADER_REF* CHTTPHeaderRef::operator->(void)
{
    return &m_Header;
}

///////////////////////////////////////////////////////////////////
// CHTTPCookieRef
INLINE CHTTPCookieRef::CHTTPCookieRef(void)
{
}

INLINE CHTTPCookieRef::~CHTTPCookieRef(void)
{
}

INLINE CHTTPCookieRef::CHTTPCookieRef(const CHTTPCookieRef& aSrc)
: m_Cookie(aSrc.m_Cookie)
{
}

INLINE CHTTPCookieRef& CHTTPCookieRef::operator=(const CHTTPCookieRef& aSrc)
{
    if (this != &aSrc)
    {
        m_Cookie = aSrc.m_Cookie;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPCookieRef::CHTTPCookieRef(CHTTPCookieRef&& aSrc)
: m_Cookie(std::move(aSrc.m_Cookie))
{
}

INLINE CHTTPCookieRef& CHTTPCookieRef::operator=(CHTTPCookieRef&& aSrc)
{
    if (this != &aSrc)
    {
        m_Cookie = std::move(aSrc.m_Cookie);
    }
    return (*this);
}
#endif

INLINE void CHTTPCookieRef::SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue)
{
    CCStringRef strNameRef(pszName, stName);
    CCStringRef strValueRef(pszValue, stValue);
    m_Cookie.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPCookieRef::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Cookie.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPCookieRef::Close(void)
{
    m_Cookie.RemoveAll();
}

INLINE void CHTTPCookieRef::Parse(PCStr pszCookie, PCStr pszCookieEnd)
{
    while (SkipChars(pszCookie) && (pszCookie < pszCookieEnd))
    {
        PCStr pszEnd = CChar::Chr(pszCookie, ';');
        PCStr pszKey = CChar::Chr(pszCookie, '=');
        if ((pszKey != nullptr) && (pszKey < pszCookieEnd))
        {
            if (pszCookie < pszKey)
            {
                PCStr  pszName = pszCookie;
                size_t stName  = GetLength(pszName, pszKey);

                pszCookie = pszKey + 1; //
                if (SkipChars(pszCookie) == false)
                {
                    break;
                }
                if ((pszCookie < pszEnd) || (pszEnd == nullptr))
                {
                    size_t stValue  = 0;
                    PCStr  pszValue = pszCookie;
                    if (pszEnd != nullptr)
                    {
                        stValue = GetLength(pszValue, pszEnd);
                    }
                    else
                    {
                        stValue = GetLength(pszCookie, pszCookieEnd);
                    }
                    DEV_DEBUG(TF("CHTTPCookieRef::Parse %p---%d : %p---%d"), pszName, stName, pszValue, stValue);
                    CCStringRef strNameRef(pszName, stName);
                    CCStringRef strValueRef(pszValue, stValue);
                    m_Cookie.Add(strNameRef, strValueRef);
                }
                if (pszEnd == nullptr)
                {
                    break;
                }
                pszCookie = pszEnd + 1; //
            }
            else
            {
                pszCookie = pszKey + 1; //
            }
            if (pszCookie >= pszCookieEnd)
            {
                break;
            }
        }
        else if ((pszEnd != nullptr) && (pszEnd < pszCookieEnd))
        {
            if (pszCookie < pszEnd)
            {
                size_t stName = GetLength(pszCookie, pszEnd);
                DEV_DEBUG(TF("CHTTPCookieRef::Parse2 %p---%d : null"), pszCookie, stName);

                CCStringRef strNameRef(pszCookie, stName);
                m_Cookie.Add(strNameRef);
            }
            pszCookie = pszEnd + 1; //
            if (pszCookie >= pszCookieEnd)
            {
                break;
            }
        }
        else
        {
            size_t stValue = GetLength(pszCookie, pszCookieEnd);
            DEV_DEBUG(TF("CHTTPCookieRef::Parse3 %p---%d : null"), pszCookie, stValue);
            CCStringRef strNameRef(pszCookie, stValue);
            m_Cookie.Add(strNameRef);
            break;
        }
    }
}

INLINE size_t CHTTPCookieRef::FormatLength(void) const
{
    size_t stSize = 0;
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE_REF* pPair = m_Cookie.GetNext(index);
        stSize += pPair->m_K.Length() + 1; // '='
        stSize += pPair->m_V.Length();
        if (index != nullptr)
        {
            stSize += 1; // ';'
        }
    }
    return stSize;
}

INLINE void CHTTPCookieRef::Format(CCString& strCookie) const
{
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE_REF* pPair = m_Cookie.GetNext(index);
        strCookie.AppendBuffer(pPair->m_K.GetBuffer(), pPair->m_K.Length());
        strCookie += '=';
        strCookie.AppendBuffer(pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            strCookie += ';';
        }
    }
}

INLINE void CHTTPCookieRef::Format(CStream& Stream) const
{
    for (PINDEX index = m_Cookie.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_COOKIE_REF* pPair = m_Cookie.GetNext(index);
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream << '=';
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            Stream <<';';
        }
    }
}

INLINE CCString CHTTPCookieRef::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_COOKIE_REF* pPair = m_Cookie.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPCookieRef::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_COOKIE_REF* pPair = m_Cookie.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_COOKIE_REF& CHTTPCookieRef::Cookie(void) const
{
    return m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE_REF& CHTTPCookieRef::Cookie(void)
{
    return m_Cookie;
}

INLINE const CHTTPTraits::MAP_COOKIE_REF& CHTTPCookieRef::operator*(void) const
{
    return m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE_REF& CHTTPCookieRef::operator*(void)
{
    return m_Cookie;
}

INLINE const CHTTPTraits::MAP_COOKIE_REF* CHTTPCookieRef::operator->(void) const
{
    return &m_Cookie;
}

INLINE CHTTPTraits::MAP_COOKIE_REF* CHTTPCookieRef::operator->(void)
{
    return &m_Cookie;
}

///////////////////////////////////////////////////////////////////
// CHTTPParameterRef
INLINE CHTTPParameterRef::CHTTPParameterRef(void)
{
}

INLINE CHTTPParameterRef::~CHTTPParameterRef(void)
{
}

INLINE CHTTPParameterRef::CHTTPParameterRef(const CHTTPParameterRef& aSrc)
: m_Param(aSrc.m_Param)
{
}

INLINE CHTTPParameterRef& CHTTPParameterRef::operator=(const CHTTPParameterRef& aSrc)
{
    if (this != &aSrc)
    {
        m_Param = aSrc.m_Param;
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPParameterRef::CHTTPParameterRef(CHTTPParameterRef&& aSrc)
: m_Param(std::move(aSrc.m_Param))
{
}

INLINE CHTTPParameterRef& CHTTPParameterRef::operator=(CHTTPParameterRef&& aSrc)
{
    if (this != &aSrc)
    {
        m_Param = std::move(aSrc.m_Param);
    }
    return (*this);
}
#endif

INLINE void CHTTPParameterRef::SetAt(PCStr pszName, size_t stName, PCStr pszValue, size_t stValue)
{
    CCStringRef strNameRef(pszName, stName);
    CCStringRef strValueRef(pszValue, stValue);
    m_Param.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPParameterRef::SetAt(const CCStringRef& strNameRef, const CCStringRef& strValueRef)
{
    m_Param.SetAt(strNameRef, strValueRef);
}

INLINE void CHTTPParameterRef::Close(void)
{
    m_Param.RemoveAll();
}

INLINE void CHTTPParameterRef::Parse(PCStr pszParameter, PCStr pszParameterEnd)
{
    while (SkipChars(pszParameter) && (pszParameter < pszParameterEnd))
    {
        PCStr pszEnd = CChar::Chr(pszParameter, '&');
        PCStr pszKey = CChar::Chr(pszParameter, '=');
        if ((pszKey != nullptr) && (pszKey < pszParameterEnd))
        {
            if (pszParameter < pszKey)
            {
                PCStr  pszName = pszParameter;
                size_t stName  = GetLength(pszName, pszKey);

                pszParameter = pszKey + 1; //
                if (SkipChars(pszParameter) == false)
                {
                    break;
                }
                if ((pszParameter < pszEnd) || (pszEnd == nullptr))
                {
                    size_t stValue  = 0;
                    PCStr  pszValue = pszParameter;
                    if (pszEnd != nullptr)
                    {
                        stValue = GetLength(pszValue, pszEnd);
                    }
                    else
                    {
                        stValue = GetLength(pszParameter, pszParameterEnd);
                    }
                    DEV_DEBUG(TF("CHTTPParameterRef::Parse %p---%d : %p---%d"), pszName, stName, pszValue, stValue);
                    CCStringRef strNameRef(pszName, stName);
                    CCStringRef strValueRef(pszValue, stValue);
                    m_Param.Add(strNameRef, strValueRef);
                }
                if (pszEnd == nullptr)
                {
                    break;
                }
                pszParameter = pszEnd + 1; //
            }
            else
            {
                pszParameter = pszKey + 1; //
            }
            if (pszParameter >= pszParameterEnd)
            {
                break;
            }
        }
        else if ((pszEnd != nullptr) && (pszEnd < pszParameterEnd))
        {
            if (pszParameter < pszEnd)
            {
                size_t stName = GetLength(pszParameter, pszEnd);
                DEV_DEBUG(TF("CHTTPParameterRef::Parse2 %p---%d : null"), pszParameter, stName);

                CCStringRef strNameRef(pszParameter, stName);
                m_Param.Add(strNameRef);
            }
            pszParameter = pszEnd + 1; //
            if (pszParameter >= pszParameterEnd)
            {
                break;
            }
        }
        else
        {
            size_t stValue = GetLength(pszParameter, pszParameterEnd);
            DEV_DEBUG(TF("CHTTPParameterRef::Parse3 %p---%d : null"), pszParameter, stValue);
            CCStringRef strNameRef(pszParameter, stValue);
            m_Param.Add(strNameRef);
            break;
        }
    }
}

INLINE size_t CHTTPParameterRef::FormatLength(void) const
{
    size_t stSize = 0;
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM_REF* pPair = m_Param.GetNext(index);
        stSize += pPair->m_K.Length() + 1; // '='
        stSize += pPair->m_V.Length();
        if (index != nullptr)
        {
            stSize += 1; // '&'
        }
    }
    return stSize;
}

INLINE void CHTTPParameterRef::Format(CCString& strParameter) const
{
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM_REF* pPair = m_Param.GetNext(index);
        strParameter.AppendBuffer(pPair->m_K.GetBuffer(), pPair->m_K.Length());
        strParameter += '=';
        strParameter.AppendBuffer(pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            strParameter += '&';
        }
    }
}

INLINE void CHTTPParameterRef::Format(CStream& Stream) const
{
    for (PINDEX index = m_Param.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_PARAM_REF* pPair = m_Param.GetNext(index);
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream << '=';
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        if (index != nullptr)
        {
            Stream << '&';
        }
    }
}

INLINE CCString CHTTPParameterRef::Find(const CCString& strName) const
{
    CCString strValue;
    const PAIR_PARAM_REF* pPair = m_Param.Find(strName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE CCString CHTTPParameterRef::Find(PCStr pszName) const
{
    CCString strValue;
    const PAIR_PARAM_REF* pPair = m_Param.Find(pszName);
    if (pPair != nullptr)
    {
        strValue = pPair->m_V;
    }
    return strValue;
}

INLINE const CHTTPTraits::MAP_PARAM_REF& CHTTPParameterRef::Param(void) const
{
    return m_Param;
}

INLINE CHTTPTraits::MAP_PARAM_REF& CHTTPParameterRef::Param(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_PARAM_REF& CHTTPParameterRef::operator*(void) const
{
    return m_Param;
}

INLINE CHTTPTraits::MAP_PARAM_REF& CHTTPParameterRef::operator*(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_PARAM_REF* CHTTPParameterRef::operator->(void) const
{
    return &m_Param;
}

INLINE CHTTPTraits::MAP_PARAM_REF* CHTTPParameterRef::operator->(void)
{
    return &m_Param;
}

///////////////////////////////////////////////////////////////////
// CHTTPRequest
INLINE CHTTPRequest::CHTTPRequest(HTTP_CONTENT eContent)
: m_eContentType(eContent)
, m_uContentValue(0)
, m_strMethod("GET")
, m_strPath()
, m_strVersion("1.1")
{
}

INLINE CHTTPRequest::~CHTTPRequest(void)
{
}

INLINE CHTTPRequest::CHTTPRequest(const CHTTPRequest& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_uContentValue(aSrc.m_uContentValue)
, m_strMethod(aSrc.m_strMethod)
, m_strPath(aSrc.m_strPath)
, m_strVersion(aSrc.m_strVersion)
, m_Header(aSrc.m_Header)
, m_Cookie(aSrc.m_Cookie)
, m_Param(aSrc.m_Param)
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
}

INLINE CHTTPRequest& CHTTPRequest::operator=(const CHTTPRequest& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType  = aSrc.m_eContentType;
        m_uContentValue = aSrc.m_uContentValue;
        m_strMethod     = aSrc.m_strMethod;
        m_strPath       = aSrc.m_strPath;
        m_strVersion    = aSrc.m_strVersion;
        m_Header        = aSrc.m_Header;
        m_Cookie        = aSrc.m_Cookie;
        m_Param         = aSrc.m_Param;
        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPRequest::CHTTPRequest(CHTTPRequest&& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_uContentValue(aSrc.m_uContentValue)
, m_strMethod(std::move(aSrc.m_strMethod))
, m_strPath(std::move(aSrc.m_strPath))
, m_strVersion(std::move(aSrc.m_strVersion))
, m_Header(std::move(aSrc.m_Header))
, m_Cookie(std::move(aSrc.m_Cookie))
, m_Param(std::move(aSrc.m_Param))
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
}

INLINE CHTTPRequest& CHTTPRequest::operator=(CHTTPRequest&& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType  = aSrc.m_eContentType;
        m_uContentValue = aSrc.m_uContentValue;
        m_strMethod     = std::move(aSrc.m_strMethod);
        m_strPath       = std::move(aSrc.m_strPath);
        m_strVersion    = std::move(aSrc.m_strVersion);
        m_Header        = std::move(aSrc.m_Header);
        m_Cookie        = std::move(aSrc.m_Cookie);
        m_Param         = std::move(aSrc.m_Param);

        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
    }
    return (*this);
}
#endif

INLINE bool CHTTPRequest::SetContentValue(UInt uValue)
{
    m_uContentValue = uValue;
    return (uValue > 0);
}

INLINE size_t CHTTPRequest::FormatLength(void) const
{
    size_t stSize = m_strMethod.Length() + 1; // method + ' '
    if (m_strPath.Length() > 0)
    {
        stSize += m_strPath.Length() + 1; // path + ' '
        if (m_Param->GetSize() > 0)
        {
            stSize += m_Param.FormatLength() + 1; // '?'
        }
    }
    else
    {
        stSize += 2; // '/' + ' '
    }
    stSize += m_strVersion.Length() + 7; // "HTTP/" + version + "\r\n"
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        stSize += pPair->m_K.Length() + pPair->m_V.Length() + 4; // key + ": " + value + "\r\n";
    }
    if (m_Cookie->GetSize() > 0)
    {
        stSize += m_Cookie.FormatLength() + 10;// "Cookie: " + cookies + "\r\n";
    }
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            stSize += (size_t)CChar::FormatLength("Content-Length: %d\r\n\r\n", m_uContentValue);
        }
        else if (m_Content.GetSize() > 0)
        {
            stSize += (size_t)CChar::FormatLength("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            stSize += m_Content[0].Size();
        }
        else
        {
            stSize += 21; // "Content-Length: 0\r\n\r\n"
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        if (m_uContentValue > 0)
        {
            stSize += 30; // "Transfer-Encoding: chunked\r\n\r\n"
        }
        else
        {
            stSize += 35; // "Transfer-Encoding: chunked\r\n\r\n" + "0\r\n\r\n"
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                stSize += (size_t)CChar::FormatLength("%x\r\n\r\n", m_Content[i].Size());
                stSize += m_Content[i].Size();
            }
        }
    }
    else
    {
        stSize += 2;//"\r\n";
    }
    return stSize;
}

INLINE void CHTTPRequest::Format(CCString& strFormat) const
{
    // 
    strFormat += m_strMethod;
    strFormat += ' ';

    if (m_strPath.Length() > 0)
    {
        strFormat += m_strPath;
        if (m_Param->GetSize() > 0)
        {
            strFormat += '?';
            m_Param.Format(strFormat);
        }
        strFormat += ' ';
    }
    else
    {
        strFormat += "/ ";
    }

    strFormat += "HTTP/";
    strFormat += m_strVersion;
    strFormat += "\r\n";
    //
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        strFormat.AppendFormat("%s: %s\r\n", pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
    }
    if (m_Cookie->GetSize() > 0)
    {
        strFormat += "Cookie: ";
        m_Cookie.Format(strFormat);
        strFormat += "\r\n";
    }
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            strFormat.AppendFormat("Content-Length: %d\r\n\r\n", m_uContentValue);
        }
        else if (m_Content.GetSize() > 0)
        {
            strFormat.AppendFormat("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            if (m_Content[0].Size() > 0)
            {
                strFormat.AppendBuffer((PCStr)m_Content[0].GetBuf(), m_Content[0].Size());
            }
        }
        else
        {
            strFormat += "Content-Length: 0\r\n\r\n";
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        strFormat += "Transfer-Encoding: chunked\r\n\r\n";
        if (m_uContentValue == 0)
        {
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                FormatChunkData(strFormat, (PCStr)m_Content[i].GetBuf(), m_Content[i].Size());
            }
            FormatChunkEOF(strFormat);
        }
    }
    else
    {
        strFormat += "\r\n";
    }
}

INLINE void CHTTPRequest::Format(CStream& Stream) const
{
    assert(Stream.IsWrite());
    // 
    Stream.Write((PByte)m_strMethod.GetBuffer(), m_strMethod.Length());
    Stream << ' ';

    if (m_strPath.Length() > 0)
    {
        Stream.Write((PByte)m_strPath.GetBuffer(), m_strPath.Length());

        if (m_Param->GetSize() > 0)
        {
            Stream << '?';
            m_Param.Format(Stream);
        }
        Stream << ' ';
    }
    else
    {
        Stream << '/' << ' ';
    }

    Stream.Write((PByte)"HTTP/", 5);
    Stream.Write((PByte)m_strVersion.GetBuffer(), m_strVersion.Length());
    Stream.Write((PByte)"\r\n", 2);
    //
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream.Write((PByte)": ", 2);
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        Stream.Write((PByte)"\r\n", 2);
    }
    if (m_Cookie->GetSize() > 0)
    {
        Stream.Write((PByte)"Cookie: ", 8);
        m_Cookie.Format(Stream);
        Stream.Write((PByte)"\r\n", 2);
    }
    CCString strTemp;
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            strTemp.Format("Content-Length: %d\r\n\r\n", m_uContentValue);
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        }
        else if (m_Content.GetSize() > 0)
        {
            strTemp.Format("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
            if (m_Content[0].Size() > 0)
            {
                Stream.Write(m_Content[0].GetBuf(), m_Content[0].Size());
            }
        }
        else
        {
            strTemp = "Content-Length: 0\r\n\r\n";
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        strTemp = "Transfer-Encoding: chunked\r\n\r\n";
        Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        if (m_uContentValue == 0)
        {
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                FormatChunkData(Stream, m_Content[i].GetBuf(), m_Content[i].Size());
            }
            FormatChunkEOF(Stream);
        }
    }
    else
    {
        Stream.Write((PByte)"\r\n", 2);
    }
}

INLINE void CHTTPRequest::Close(void)
{
    m_uContentValue = 0;
    m_strMethod = "GET";
    m_strPath.Empty();
    m_strVersion = "1.1";
    m_Header.Close();
    m_Cookie.Close();
    m_Param.Close();
    m_Content.RemoveAll();
}

INLINE void CHTTPRequest::SetMethod(const CCString& strMethod)
{
    m_strMethod = strMethod;
}

INLINE void CHTTPRequest::SetMethod(PCStr pszMethod)
{
    m_strMethod = pszMethod;
}

INLINE const CCString& CHTTPRequest::GetMethod(void) const
{
    return m_strMethod;
}

INLINE void CHTTPRequest::SetPath(const CCString& strPath)
{
    m_strPath = strPath;
}

INLINE void CHTTPRequest::SetPath(PCStr pszPath)
{
    m_strPath = pszPath;
}

INLINE const CCString& CHTTPRequest::GetPath(void) const
{
    return m_strPath;
}

INLINE void CHTTPRequest::SetVersion(const CCString& strVersion)
{
    m_strVersion = strVersion;
}

INLINE void CHTTPRequest::SetVersion(PCStr pszVersion)
{
    m_strVersion = pszVersion;
}

INLINE const CCString& CHTTPRequest::GetVersion(void) const
{
    return m_strVersion;
}

INLINE bool CHTTPRequest::SetContent(const CCString& strContent)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            m_Content[0].Attach(strContent.Length());
            MM_SAFE::Cpy(m_Content[0].GetBuf(), m_Content[0].Size(), strContent.GetBuffer(), m_Content[0].Size());
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPRequest::SetContent(PCStr pszContent)
{
    if ((pszContent != nullptr) && (m_eContentType != HTTP_CONTENT_CHUNKED))
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            size_t stSize = CChar::Length(pszContent);
            m_Content[0].Attach(stSize);
            MM_SAFE::Cpy(m_Content[0].GetBuf(), stSize, pszContent, stSize);
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPRequest::SetContent(PByte pContent, size_t stSize, bool bManage)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            return m_Content[0].Attach(stSize, pContent, bManage);
        }
    }
    return false;
}

INLINE bool CHTTPRequest::SetContent(CBufStream& BufRef, bool bCopy)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            return m_Content[0].Attach(BufRef, bCopy);
        }
    }
    return false;
}

INLINE bool CHTTPRequest::AddContent(const CCString& strContent)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        m_Content[nIndex].Attach(strContent.Length());
        MM_SAFE::Cpy(m_Content[nIndex].GetBuf(), m_Content[nIndex].Size(), strContent.GetBuffer(), m_Content[nIndex].Size());
        return true;
    }
    return false;
}

INLINE bool CHTTPRequest::AddContent(PCStr pszContent)
{
    if (pszContent != nullptr)
    {
        Int nIndex = -1;
        if (m_eContentType == HTTP_CONTENT_CHUNKED)
        {
            nIndex = m_Content.Add(1, false);
        }
        if (nIndex >= 0)
        {
            size_t stSize = CChar::Length(pszContent);
            m_Content[nIndex].Attach(stSize);
            MM_SAFE::Cpy(m_Content[nIndex].GetBuf(), stSize, pszContent, stSize);
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPRequest::AddContent(PByte pContent, size_t stSize, bool bManage)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        return m_Content[nIndex].Attach(stSize, pContent, bManage);
    }
    return false;
}

INLINE bool CHTTPRequest::AddContent(CBufStream& BufRef, bool bCopy)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        return m_Content[nIndex].Attach(BufRef, bCopy);
    }
    return false;
}

INLINE void CHTTPRequest::CleanContent(void)
{
    m_Content.RemoveAll();
}

INLINE const CHTTPTraits::ARY_BUF& CHTTPRequest::GetContent(void) const
{
    return m_Content;
}

INLINE CHTTPTraits::ARY_BUF& CHTTPRequest::GetContent(void)
{
    return m_Content;
}

INLINE const CHTTPHeader& CHTTPRequest::GetHeader(void) const
{
    return m_Header;
}

INLINE CHTTPHeader& CHTTPRequest::GetHeader(void)
{
    return m_Header;
}

INLINE const CHTTPCookie& CHTTPRequest::GetCookie(void) const
{
    return m_Cookie;
}

INLINE CHTTPCookie& CHTTPRequest::GetCookie(void)
{
    return m_Cookie;
}

INLINE const CHTTPParameter& CHTTPRequest::GetParam(void) const
{
    return m_Param;
}

INLINE CHTTPParameter& CHTTPRequest::GetParam(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_HEADER& CHTTPRequest::Header(void) const
{
    return m_Header.Header();
}

INLINE CHTTPTraits::MAP_HEADER& CHTTPRequest::Header(void)
{
    return m_Header.Header();
}

INLINE const CHTTPTraits::MAP_COOKIE& CHTTPRequest::Cookie(void) const
{
    return m_Cookie.Cookie();
}

INLINE CHTTPTraits::MAP_COOKIE& CHTTPRequest::Cookie(void)
{
    return m_Cookie.Cookie();
}

INLINE const CHTTPTraits::MAP_PARAM& CHTTPRequest::Param(void) const
{
    return m_Param.Param();
}

INLINE CHTTPTraits::MAP_PARAM& CHTTPRequest::Param(void)
{
    return m_Param.Param();
}

///////////////////////////////////////////////////////////////////
// CHTTPResponse
INLINE CHTTPResponse::CHTTPResponse(HTTP_CONTENT eContent)
: m_eContentType(eContent)
, m_uContentValue(0)
, m_strVersion("1.1")
{
    m_strStatus.ToString(OK);
    m_strMessage = StatusToString(OK);
}

INLINE CHTTPResponse::~CHTTPResponse(void)
{
}

INLINE CHTTPResponse::CHTTPResponse(const CHTTPResponse& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_uContentValue(aSrc.m_uContentValue)
, m_strVersion(aSrc.m_strVersion)
, m_strStatus(aSrc.m_strStatus)
, m_strMessage(aSrc.m_strMessage)
, m_Header(aSrc.m_Header)
, m_Cookies(aSrc.m_Cookies)
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
}

INLINE CHTTPResponse& CHTTPResponse::operator=(const CHTTPResponse& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType  = aSrc.m_eContentType;
        m_uContentValue = aSrc.m_uContentValue;
        m_strVersion    = aSrc.m_strVersion;
        m_strStatus     = aSrc.m_strStatus;
        m_strMessage    = aSrc.m_strMessage;
        m_Header        = aSrc.m_Header;
        m_Cookies       = aSrc.m_Cookies;
        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPResponse::CHTTPResponse(CHTTPResponse&& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_uContentValue(aSrc.m_uContentValue)
, m_strVersion(std::move(aSrc.m_strVersion))
, m_strStatus(std::move(aSrc.m_strStatus))
, m_strMessage(std::move(aSrc.m_strMessage))
, m_Header(std::move(aSrc.m_Header))
, m_Cookies(std::move(aSrc.m_Cookies))
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
}

INLINE CHTTPResponse& CHTTPResponse::operator=(CHTTPResponse&& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType  = aSrc.m_eContentType;
        m_uContentValue = aSrc.m_uContentValue;
        m_strVersion    = std::move(aSrc.m_strVersion);
        m_strStatus     = std::move(aSrc.m_strStatus);
        m_strMessage    = std::move(aSrc.m_strMessage);
        m_Header        = std::move(aSrc.m_Header);
        m_Cookies       = std::move(aSrc.m_Cookies);
        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
    }
    return (*this);
}
#endif

INLINE bool CHTTPResponse::SetContentValue(UInt uValue)
{
    m_uContentValue = uValue;
    return (uValue > 0);
}

INLINE size_t CHTTPResponse::FormatLength(void) const
{
    size_t stSize = m_strVersion.Length() + m_strStatus.Length() + 8; // "HTTP/" + version + ' ' + status + "\r\n"
    if (m_strMessage.Length() > 0)
    {
        stSize += m_strMessage.Length() + 1; // message + ' ';
    }
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        stSize += pPair->m_K.Length() + pPair->m_V.Length() + 4; // key + ": " + value + "\r\n";
    }
    if (m_Cookies.GetSize() > 0)
    {
        for (Int i = 0; i < m_Cookies.GetSize(); ++i)
        {
            stSize += m_Cookies[i].FormatLength() + 14;// "Set-Cookie: " + cookies + "\r\n";
        }
    }
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            stSize += (size_t)CChar::FormatLength("Content-Length: %d\r\n\r\n", m_uContentValue);
        }
        else if (m_Content.GetSize() > 0)
        {
            stSize += (size_t)CChar::FormatLength("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            stSize += m_Content[0].Size();
        }
        else
        {
            stSize += 21; // "Content-Length: 0\r\n\r\n"
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        if (m_uContentValue > 0)
        {
            stSize += 30; // "Transfer-Encoding: chunked\r\n\r\n"
        }
        else
        {
            stSize += 35; // "Transfer-Encoding: chunked\r\n\r\n" + "0\r\n\r\n"
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                stSize += (size_t)CChar::FormatLength("%x\r\n\r\n", m_Content[i].Size());
                stSize += m_Content[i].Size();
            }
        }
    }
    else
    {
        stSize += 2;//"\r\n";
    }
    return stSize;
}

INLINE void CHTTPResponse::Format(CCString& strFormat) const
{
    //
    strFormat += "HTTP/";
    strFormat += m_strVersion;
    strFormat += ' ';
    strFormat += m_strStatus;
    if (m_strMessage.Length() > 0)
    {
        strFormat += ' ';
        strFormat += m_strMessage;
    }
    strFormat += "\r\n";
    //
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        strFormat.AppendFormat("%s: %s\r\n", pPair->m_K.GetBuffer(), pPair->m_V.GetBuffer());
    }
    if (m_Cookies.GetSize() > 0)
    {
        for (Int i = 0; i < m_Cookies.GetSize(); ++i)
        {
            strFormat += "Set-Cookie: ";
            m_Cookies[i].Format(strFormat);
            strFormat += "\r\n";
        }
    }
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            strFormat.AppendFormat("Content-Length: %d\r\n\r\n", m_uContentValue);
        }
        else if (m_Content.GetSize() > 0)
        {
            strFormat.AppendFormat("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            if (m_Content[0].Size() > 0)
            {
                strFormat.AppendBuffer((PCStr)m_Content[0].GetBuf(), m_Content[0].Size());
            }
        }
        else
        {
            strFormat += "Content-Length: 0\r\n\r\n";
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        strFormat += "Transfer-Encoding: chunked\r\n\r\n";
        if (m_uContentValue == 0)
        {
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                FormatChunkData(strFormat, (PCStr)m_Content[i].GetBuf(), m_Content[i].Size());
            }
            FormatChunkEOF(strFormat);
        }
    }
    else
    {
        strFormat += "\r\n";
    }
}

INLINE void CHTTPResponse::Format(CStream& Stream) const
{
    assert(Stream.IsWrite());
    //
    Stream.Write((PByte)"HTTP/", 5);
    Stream.Write((PByte)m_strVersion.GetBuffer(), m_strVersion.Length());
    Stream << ' ';
    Stream.Write((PByte)m_strStatus.GetBuffer(), m_strStatus.Length());
    if (m_strMessage.Length() > 0)
    {
        Stream << ' ';
        Stream.Write((PByte)m_strMessage.GetBuffer(), m_strMessage.Length());
    }
    Stream.Write((PByte)"\r\n", 2);
    //
    for (PINDEX index = m_Header->GetFirstIndex(); index != nullptr; )
    {
        const PAIR_HEADER* pPair = m_Header->GetNext(index);
        if ((pPair->m_K.Cmpin("Content-Length", 14) == 0) || (pPair->m_K.Cmpin("Transfer-Encoding", 17) == 0))
        {
            continue;
        }
        Stream.Write((PByte)pPair->m_K.GetBuffer(), pPair->m_K.Length());
        Stream.Write((PByte)": ", 2);
        Stream.Write((PByte)pPair->m_V.GetBuffer(), pPair->m_V.Length());
        Stream.Write((PByte)"\r\n", 2);
    }
    if (m_Cookies.GetSize() > 0)
    {
        for (Int i = 0; i < m_Cookies.GetSize(); ++i)
        {
            Stream.Write((PByte)"Set-Cookie: ", 12);
            m_Cookies[i].Format(Stream);
            Stream.Write((PByte)"\r\n", 2);
        }
    }
    CCString strTemp;
    if (m_eContentType == HTTP_CONTENT_LENGTH)
    {
        if (m_uContentValue > 0)
        {
            strTemp.Format("Content-Length: %d\r\n\r\n", m_uContentValue);
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        }
        else if (m_Content.GetSize() > 0)
        {
            strTemp.Format("Content-Length: %d\r\n\r\n", m_Content[0].Size());
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
            if (m_Content[0].Size() > 0)
            {
                Stream.Write(m_Content[0].GetBuf(), m_Content[0].Size());
            }
        }
        else
        {
            strTemp = "Content-Length: 0\r\n\r\n";
            Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        }
    }
    else if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        strTemp = "Transfer-Encoding: chunked\r\n\r\n";
        Stream.Write((PByte)strTemp.GetBuffer(), strTemp.Length());
        if (m_uContentValue == 0)
        {
            for (Int i = 0; i < m_Content.GetSize(); ++i)
            {
                FormatChunkData(Stream, m_Content[i].GetBuf(), m_Content[i].Size());
            }
            FormatChunkEOF(Stream);
        }
    }
    else
    {
        Stream.Write((PByte)"\r\n", 2);
    }
}

INLINE void CHTTPResponse::Close(void)
{
    m_uContentValue = 0;
    m_strVersion = "1.1";
    m_strStatus.ToString(OK);
    m_strMessage = StatusToString(OK);

    m_Header.Close();
    m_Cookies.RemoveAll();
    m_Content.RemoveAll();
}

INLINE void CHTTPResponse::SetVersion(const CCString& strVersion)
{
    m_strVersion = strVersion;
}

INLINE void CHTTPResponse::SetVersion(PCStr pszVersion)
{
    m_strVersion = pszVersion;
}

INLINE const CCString& CHTTPResponse::GetVersion(void) const
{
    return m_strVersion;
}

INLINE void CHTTPResponse::SetStatus(const CCString& strStatus)
{
    m_strStatus = strStatus;
}

INLINE void CHTTPResponse::SetStatus(PCStr pszStatus)
{
    m_strStatus = pszStatus;
}

INLINE void CHTTPResponse::SetStatusCode(Int nStatus)
{
    m_strStatus.ToString(nStatus);
}

INLINE const CCString& CHTTPResponse::GetStatus(void) const
{
    return m_strStatus;
}

INLINE Int CHTTPResponse::GetStatusCode(void) const 
{
    return m_strStatus.ToInt();
}

INLINE void CHTTPResponse::SetMessage(const CCString& strMessage)
{
    m_strMessage = strMessage;
}

INLINE void CHTTPResponse::SetMessage(PCStr pszMessage)
{
    m_strMessage = pszMessage;
}

INLINE const CCString& CHTTPResponse::GetMessage(void) const
{
    return m_strMessage;
}

INLINE bool CHTTPResponse::SetContent(const CCString& strContent)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            m_Content[0].Attach(strContent.Length());
            MM_SAFE::Cpy(m_Content[0].GetBuf(), m_Content[0].Size(), strContent.GetBuffer(), m_Content[0].Size());
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPResponse::SetContent(PCStr pszContent)
{
    if ((pszContent != nullptr) && (m_eContentType != HTTP_CONTENT_CHUNKED))
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            size_t stSize = CChar::Length(pszContent);
            m_Content[0].Attach(stSize);
            MM_SAFE::Cpy(m_Content[0].GetBuf(), stSize, pszContent, stSize);
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPResponse::SetContent(PByte pContent, size_t stSize, bool bManage)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            return m_Content[0].Attach(stSize, pContent, bManage);
        }
    }
    return false;
}

INLINE bool CHTTPResponse::SetContent(CBufStream& BufRef, bool bCopy)
{
    if (m_eContentType != HTTP_CONTENT_CHUNKED)
    {
        if (m_Content.GetSize() == 0)
        {
            m_Content.Add(1, false);
        }
        if (m_Content.GetSize() > 0)
        {
            return m_Content[0].Attach(BufRef, bCopy);
        }
    }
    return false;
}

INLINE bool CHTTPResponse::AddContent(const CCString& strContent)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        m_Content[nIndex].Attach(strContent.Length());
        MM_SAFE::Cpy(m_Content[nIndex].GetBuf(), m_Content[nIndex].Size(), strContent.GetBuffer(), m_Content[nIndex].Size());
        return true;
    }
    return false;
}

INLINE bool CHTTPResponse::AddContent(PCStr pszContent)
{
    if (pszContent != nullptr)
    {
        Int nIndex = -1;
        if (m_eContentType == HTTP_CONTENT_CHUNKED)
        {
            nIndex = m_Content.Add(1, false);
        }
        if (nIndex >= 0)
        {
            size_t stSize = CChar::Length(pszContent);
            m_Content[nIndex].Attach(stSize);
            MM_SAFE::Cpy(m_Content[nIndex].GetBuf(), stSize, pszContent, stSize);
            return true;
        }
    }
    return false;
}

INLINE bool CHTTPResponse::AddContent(PByte pContent, size_t stSize, bool bManage)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        return m_Content[nIndex].Attach(stSize, pContent, bManage);
    }
    return false;
}

INLINE bool CHTTPResponse::AddContent(CBufStream& BufRef, bool bCopy)
{
    Int nIndex = -1;
    if (m_eContentType == HTTP_CONTENT_CHUNKED)
    {
        nIndex = m_Content.Add(1, false);
    }
    if (nIndex >= 0)
    {
        return m_Content[nIndex].Attach(BufRef, bCopy);
    }
    return false;
}

INLINE void CHTTPResponse::CleanContent(void)
{
    m_Content.RemoveAll();
}

INLINE const CHTTPTraits::ARY_BUF& CHTTPResponse::GetContent(void) const
{
    return m_Content;
}

INLINE CHTTPTraits::ARY_BUF& CHTTPResponse::GetContent(void)
{
    return m_Content;
}

INLINE const CHTTPHeader& CHTTPResponse::GetHeader(void) const
{
    return m_Header;
}

INLINE CHTTPHeader& CHTTPResponse::GetHeader(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::ARY_COOKIE& CHTTPResponse::GetCookies(void) const
{
    return m_Cookies;
}

INLINE CHTTPTraits::ARY_COOKIE& CHTTPResponse::GetCookies(void)
{
    return m_Cookies;
}

INLINE const CHTTPTraits::MAP_HEADER& CHTTPResponse::Header(void) const
{
    return m_Header.Header();
}

INLINE CHTTPTraits::MAP_HEADER& CHTTPResponse::Header(void)
{
    return m_Header.Header();
}

///////////////////////////////////////////////////////////////////
// CHTTPRequestRef
INLINE CHTTPRequestRef::CHTTPRequestRef(HTTP_CONTENT eContent)
: m_eContentType(eContent)
{
}

INLINE CHTTPRequestRef::~CHTTPRequestRef(void)
{
}

INLINE CHTTPRequestRef::CHTTPRequestRef(const CHTTPRequestRef& aSrc)
: m_eContentType(aSrc.m_eContentType)
{
}

INLINE CHTTPRequestRef& CHTTPRequestRef::operator=(const CHTTPRequestRef&)
{
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPRequestRef::CHTTPRequestRef(CHTTPRequestRef&& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_NetBuffer(aSrc.m_NetBuffer)
, m_strMethod(std::move(aSrc.m_strMethod))
, m_strPath(std::move(aSrc.m_strPath))
, m_strVersion(std::move(aSrc.m_strVersion))
, m_Header(std::move(aSrc.m_Header))
, m_Cookie(std::move(aSrc.m_Cookie))
, m_Param(std::move(aSrc.m_Param))
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
    aSrc.m_NetBuffer.Reset();
}

INLINE CHTTPRequestRef& CHTTPRequestRef::operator=(CHTTPRequestRef&& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType = aSrc.m_eContentType;
        m_NetBuffer    = aSrc.m_NetBuffer;
        m_strMethod    = std::move(aSrc.m_strMethod);
        m_strPath      = std::move(aSrc.m_strPath);
        m_strVersion   = std::move(aSrc.m_strVersion);
        m_Header       = std::move(aSrc.m_Header);
        m_Cookie       = std::move(aSrc.m_Cookie);
        m_Param        = std::move(aSrc.m_Param);

        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
        aSrc.m_NetBuffer.Reset();
    }
    return (*this);
}
#endif

INLINE void CHTTPRequestRef::Close(void)
{
    m_strMethod.Empty();
    m_strPath.Empty();
    m_strVersion.Empty();
    m_Header.Close();
    m_Cookie.Close();
    m_Param.Close();
    m_Content.RemoveAll();
    m_NetBuffer.Free();
}

INLINE const CCStringRef& CHTTPRequestRef::GetMethod(void) const
{
    return m_strMethod;
}

INLINE const void CHTTPRequestRef::GetMethod(CCString& strMethod) const
{
    strMethod = m_strMethod;
}

INLINE const CCStringRef& CHTTPRequestRef::GetPath(void) const
{
    return m_strPath;
}

INLINE const void CHTTPRequestRef::GetPath(CCString& strPath) const
{
    strPath = m_strPath;
}

INLINE const CCStringRef& CHTTPRequestRef::GetVersion(void) const
{
    return m_strVersion;
}

INLINE const void CHTTPRequestRef::GetVersion(CCString& strVersion) const
{
    strVersion = m_strVersion;
}

INLINE const CHTTPTraits::ARY_BUF& CHTTPRequestRef::GetContent(void) const
{
    return m_Content;
}

INLINE CHTTPTraits::ARY_BUF& CHTTPRequestRef::GetContent(void)
{
    return m_Content;
}

INLINE const CHTTPHeaderRef& CHTTPRequestRef::GetHeader(void) const
{
    return m_Header;
}

INLINE CHTTPHeaderRef& CHTTPRequestRef::GetHeader(void)
{
    return m_Header;
}

INLINE const CHTTPCookieRef& CHTTPRequestRef::GetCookie(void) const
{
    return m_Cookie;
}

INLINE CHTTPCookieRef& CHTTPRequestRef::GetCookie(void)
{
    return m_Cookie;
}

INLINE const CHTTPParameterRef& CHTTPRequestRef::GetParam(void) const
{
    return m_Param;
}

INLINE CHTTPParameterRef& CHTTPRequestRef::GetParam(void)
{
    return m_Param;
}

INLINE const CHTTPTraits::MAP_HEADER_REF& CHTTPRequestRef::Header(void) const
{
    return m_Header.Header();
}

INLINE CHTTPTraits::MAP_HEADER_REF& CHTTPRequestRef::Header(void)
{
    return m_Header.Header();
}

INLINE const CHTTPTraits::MAP_COOKIE_REF& CHTTPRequestRef::Cookie(void) const
{
    return m_Cookie.Cookie();
}

INLINE CHTTPTraits::MAP_COOKIE_REF& CHTTPRequestRef::Cookie(void)
{
    return m_Cookie.Cookie();
}

INLINE const CHTTPTraits::MAP_PARAM_REF& CHTTPRequestRef::Param(void) const
{
    return m_Param.Param();
}

INLINE CHTTPTraits::MAP_PARAM_REF& CHTTPRequestRef::Param(void)
{
    return m_Param.Param();
}

INLINE bool CHTTPRequestRef::Attach(const NET_BUFFER& NetBuffer)
{
    Detach();
    m_NetBuffer = NetBuffer;
    return true;
}

INLINE void CHTTPRequestRef::Detach(NET_BUFFER& NetBuffer)
{
    NetBuffer = m_NetBuffer;
    Detach();
}

INLINE void CHTTPRequestRef::Detach(void)
{
    m_NetBuffer.Reset();
}

INLINE const NET_BUFFER& CHTTPRequestRef::NetBuffer(void) const
{
    return m_NetBuffer;
}

INLINE NET_BUFFER& CHTTPRequestRef::NetBuffer(void)
{
    return m_NetBuffer;
}

///////////////////////////////////////////////////////////////////
// CHTTPResponseRef
INLINE CHTTPResponseRef::CHTTPResponseRef(HTTP_CONTENT eContent)
: m_eContentType(eContent)
{
}

INLINE CHTTPResponseRef::~CHTTPResponseRef(void)
{
}

INLINE CHTTPResponseRef::CHTTPResponseRef(const CHTTPResponseRef& aSrc)
: m_eContentType(aSrc.m_eContentType)
{
}

INLINE CHTTPResponseRef& CHTTPResponseRef::operator=(const CHTTPResponseRef&)
{
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPResponseRef::CHTTPResponseRef(CHTTPResponseRef&& aSrc)
: m_eContentType(aSrc.m_eContentType)
, m_NetBuffer(aSrc.m_NetBuffer)
, m_strVersion(std::move(aSrc.m_strVersion))
, m_strStatus(std::move(aSrc.m_strStatus))
, m_strMessage(std::move(aSrc.m_strMessage))
, m_Header(std::move(aSrc.m_Header))
, m_Cookies(std::move(aSrc.m_Cookies))
{
    for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
    {
        m_Content[i].Attach(aSrc.m_Content[i]);
    }
    aSrc.m_NetBuffer.Reset();
}

INLINE CHTTPResponseRef& CHTTPResponseRef::operator=(CHTTPResponseRef&& aSrc)
{
    if (this != &aSrc)
    {
        m_eContentType = aSrc.m_eContentType;
        m_NetBuffer    = aSrc.m_NetBuffer;
        m_strVersion   = std::move(aSrc.m_strVersion);
        m_strStatus    = std::move(aSrc.m_strStatus);
        m_strMessage   = std::move(aSrc.m_strMessage);
        m_Header       = std::move(aSrc.m_Header);
        m_Cookies      = std::move(aSrc.m_Cookies);
        for (Int i = 0; i < aSrc.m_Content.GetSize(); ++i)
        {
            m_Content[i].Attach(aSrc.m_Content[i]);
        }
        aSrc.m_NetBuffer.Reset();
    }
    return (*this);
}
#endif

INLINE void CHTTPResponseRef::Close(void)
{
    m_strVersion.Empty();
    m_strStatus.Empty();
    m_strMessage.Empty();

    m_Header.Close();
    m_Cookies.RemoveAll();
    m_Content.RemoveAll();
    m_NetBuffer.Free();
}

INLINE const CCStringRef& CHTTPResponseRef::GetVersion(void) const
{
    return m_strVersion;
}

INLINE const void CHTTPResponseRef::GetVersion(CCString& strVersion) const
{
    strVersion = m_strVersion;
}

INLINE const CCStringRef& CHTTPResponseRef::GetStatus(void) const
{
    return m_strStatus;
}

INLINE const void CHTTPResponseRef::GetStatus(CCString& strStatus) const
{
    strStatus = m_strStatus;
}

INLINE Int CHTTPResponseRef::GetStatusCode(void) const 
{
    return m_strStatus.ToInt();
}

INLINE const CCStringRef& CHTTPResponseRef::GetMessage(void) const
{
    return m_strMessage;
}

INLINE const void CHTTPResponseRef::GetMessage(CCString& strMessage) const
{
    strMessage = m_strMessage;
}

INLINE const CHTTPTraits::ARY_BUF& CHTTPResponseRef::GetContent(void) const
{
    return m_Content;
}

INLINE CHTTPTraits::ARY_BUF& CHTTPResponseRef::GetContent(void)
{
    return m_Content;
}

INLINE const CHTTPHeaderRef& CHTTPResponseRef::GetHeader(void) const
{
    return m_Header;
}

INLINE CHTTPHeaderRef& CHTTPResponseRef::GetHeader(void)
{
    return m_Header;
}

INLINE const CHTTPTraits::ARY_COOKIE_REF& CHTTPResponseRef::GetCookies(void) const
{
    return m_Cookies;
}

INLINE CHTTPTraits::ARY_COOKIE_REF& CHTTPResponseRef::GetCookies(void)
{
    return m_Cookies;
}

INLINE const CHTTPTraits::MAP_HEADER_REF& CHTTPResponseRef::Header(void) const
{
    return m_Header.Header();
}

INLINE CHTTPTraits::MAP_HEADER_REF& CHTTPResponseRef::Header(void)
{
    return m_Header.Header();
}

INLINE bool CHTTPResponseRef::Attach(const NET_BUFFER& NetBuffer)
{
    Detach();
    m_NetBuffer = NetBuffer;
    return true;
}

INLINE void CHTTPResponseRef::Detach(NET_BUFFER& NetBuffer)
{
    NetBuffer = m_NetBuffer;
    Detach();
}

INLINE void CHTTPResponseRef::Detach(void)
{
    m_NetBuffer.Reset();
}

INLINE const NET_BUFFER& CHTTPResponseRef::NetBuffer(void) const
{
    return m_NetBuffer;
}

INLINE NET_BUFFER& CHTTPResponseRef::NetBuffer(void)
{
    return m_NetBuffer;
}

///////////////////////////////////////////////////////////////////
// CHTTPParser
INLINE CHTTPParser::CHTTPParser(void)
: m_nBufCacheSize(0)
, m_nHeaderLength(0)
, m_nContentLength(0)
, m_nAllChunkLength(0)
, m_bHeaderComplete(false)
, m_bResponseIdent(false)
, m_bContentLength(false)
, m_bContentChunked(false)
, m_bContentComplete(false)
, m_bNetBufferClone(false)
{
}

INLINE CHTTPParser::~CHTTPParser(void)
{
}

INLINE CHTTPParser::CHTTPParser(const CHTTPParser& aSrc)
: m_nBufCacheSize(aSrc.m_nBufCacheSize)
, m_nHeaderLength(aSrc.m_nHeaderLength)
, m_nContentLength(aSrc.m_nContentLength)
, m_nAllChunkLength(aSrc.m_nAllChunkLength)
, m_bHeaderComplete(aSrc.m_bHeaderComplete)
, m_bResponseIdent(aSrc.m_bResponseIdent)
, m_bContentLength(aSrc.m_bContentLength)
, m_bContentChunked(aSrc.m_bContentChunked)
, m_bContentComplete(aSrc.m_bContentComplete)
, m_bNetBufferClone(aSrc.m_bNetBufferClone)
{
    m_Ident[0] = aSrc.m_Ident[0];
    m_Ident[1] = aSrc.m_Ident[1];
    m_Ident[2] = aSrc.m_Ident[2];
    m_Content  = aSrc.m_Content;

    if (aSrc.IsNetBufferCache())
    {
        m_NetBuffer = aSrc.m_NetBuffer;
        AllocNetBuffer(aSrc.m_BufCache);
    }
    else
    {
        m_BufCache.Attach(aSrc.m_BufCache);
    }
}

INLINE CHTTPParser& CHTTPParser::operator=(const CHTTPParser& aSrc)
{
    if (this != &aSrc)
    {
        m_nBufCacheSize    = aSrc.m_nBufCacheSize;
        m_nHeaderLength    = aSrc.m_nHeaderLength;
        m_nContentLength   = aSrc.m_nContentLength;
        m_nAllChunkLength  = aSrc.m_nAllChunkLength;

        m_bHeaderComplete  = aSrc.m_bHeaderComplete;
        m_bResponseIdent   = aSrc.m_bResponseIdent;
        m_bContentLength   = aSrc.m_bContentLength;
        m_bContentChunked  = aSrc.m_bContentChunked;
        m_bContentComplete = aSrc.m_bContentComplete;
        m_bNetBufferClone  = aSrc.m_bNetBufferClone;

        m_Ident[0]         = aSrc.m_Ident[0];
        m_Ident[1]         = aSrc.m_Ident[1];
        m_Ident[2]         = aSrc.m_Ident[2];
        m_Content          = aSrc.m_Content;

        if (aSrc.IsNetBufferCache())
        {
            m_NetBuffer = aSrc.m_NetBuffer;
            AllocNetBuffer(aSrc.m_BufCache);
        }
        else
        {
            m_BufCache.Attach(aSrc.m_BufCache);
        }
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CHTTPParser::CHTTPParser(CHTTPParser&& aSrc)
: m_nBufCacheSize(aSrc.m_nBufCacheSize)
, m_nHeaderLength(aSrc.m_nHeaderLength)
, m_nContentLength(aSrc.m_nContentLength)
, m_nAllChunkLength(aSrc.m_nAllChunkLength)
, m_bHeaderComplete(aSrc.m_bHeaderComplete)
, m_bResponseIdent(aSrc.m_bResponseIdent)
, m_bContentLength(aSrc.m_bContentLength)
, m_bContentChunked(aSrc.m_bContentChunked)
, m_bContentComplete(aSrc.m_bContentComplete)
, m_bNetBufferClone(aSrc.m_bNetBufferClone)
{
    m_Ident[0] = aSrc.m_Ident[0];
    m_Ident[1] = aSrc.m_Ident[1];
    m_Ident[2] = aSrc.m_Ident[2];
    m_Content  = aSrc.m_Content;

    if (aSrc.IsNetBufferCache())
    {
        m_NetBuffer = aSrc.m_NetBuffer;
        aSrc.m_NetBuffer.Reset();
    }
    if (aSrc.m_BufCache.Size() > 0)
    {
        m_BufCache.Attach(aSrc.m_BufCache);
    }
    aSrc.Close();
}

INLINE CHTTPParser& CHTTPParser::operator=(CHTTPParser&& aSrc)
{
    if (this != &aSrc)
    {
        m_nBufCacheSize    = aSrc.m_nBufCacheSize;
        m_nHeaderLength    = aSrc.m_nHeaderLength;
        m_nContentLength   = aSrc.m_nContentLength;
        m_nAllChunkLength  = aSrc.m_nAllChunkLength;

        m_bHeaderComplete  = aSrc.m_bHeaderComplete;
        m_bResponseIdent   = aSrc.m_bResponseIdent;
        m_bContentLength   = aSrc.m_bContentLength;
        m_bContentChunked  = aSrc.m_bContentChunked;
        m_bContentComplete = aSrc.m_bContentComplete;
        m_bNetBufferClone  = aSrc.m_bNetBufferClone;

        m_Ident[0]         = aSrc.m_Ident[0];
        m_Ident[1]         = aSrc.m_Ident[1];
        m_Ident[2]         = aSrc.m_Ident[2];
        m_Content          = aSrc.m_Content;

        if (aSrc.IsNetBufferCache())
        {
            m_NetBuffer = aSrc.m_NetBuffer;
            aSrc.m_NetBuffer.Reset();
        }
        if (aSrc.m_BufCache.Size() > 0)
        {
            m_BufCache.Attach(aSrc.m_BufCache);
        }
        aSrc.Close();
    }
    return (*this);
}
#endif

INLINE Int CHTTPParser::GetCacheSize(void) const
{
    return m_nBufCacheSize;
}

INLINE Int CHTTPParser::GetHeaderLength(void) const
{
    return m_nHeaderLength;
}

INLINE Int CHTTPParser::GetContentLength(void) const
{
    return m_nContentLength;
}

INLINE Int CHTTPParser::GetAllChunkLength(void) const
{
    return m_nAllChunkLength;
}

INLINE bool CHTTPParser::IsHeaderComplete(void) const
{
    return m_bHeaderComplete;
}

INLINE bool CHTTPParser::IsResponseIdent(void) const
{
    return m_bResponseIdent;
}

INLINE bool CHTTPParser::IsContentLength(void) const
{
    return m_bContentLength;
}

INLINE bool CHTTPParser::IsContentChunked(void) const
{
    return m_bContentChunked;
}

INLINE bool CHTTPParser::IsContentComplete(void) const
{
    return m_bContentComplete;
}

INLINE bool CHTTPParser::IsNetBufferClone(void) const
{
    return (IsNetBufferCache() && m_bNetBufferClone);
}

INLINE bool CHTTPParser::IsNetBufferCache(void) const
{
    return m_NetBuffer.IsValid();
}

INLINE CCString CHTTPParser::GetIdent(Int nIndex) const
{
    CCString strIdent;
    if ((nIndex >= 0) && (nIndex < 3) && IsHeaderComplete())
    {
        strIdent.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[nIndex].nPos), (size_t)m_Ident[nIndex].nSize);
    }
    return strIdent;
}

INLINE void CHTTPParser::GetContentMark(ARY_MARK& Mark) const
{
    Mark = m_Content;
}

INLINE const CBufReadStream& CHTTPParser::GetCache(void) const
{
    return m_BufCache;
}

INLINE CBufReadStream& CHTTPParser::GetCache(void)
{
    return m_BufCache;
}

INLINE const PByte CHTTPParser::GetCacheBuf(void) const
{
    return m_BufCache.GetBuf();
}

INLINE const PByte CHTTPParser::GetCacheContent(void) const
{
    return m_BufCache.GetBuf((size_t)m_nHeaderLength);
}

INLINE void CHTTPParser::Close(void)
{
    m_nBufCacheSize    = 0;
    m_nHeaderLength    = 0;
    m_nContentLength   = 0;
    m_nAllChunkLength  = 0;

    m_bHeaderComplete  = false;
    m_bResponseIdent   = false;
    m_bContentLength   = false;
    m_bContentChunked  = false;
    m_bContentComplete = false;
    //m_bNetBufferClone  = false;

    m_Ident[0].Reset();
    m_Ident[1].Reset();
    m_Ident[2].Reset();
    m_Content.RemoveAll();

    m_NetBuffer.Free();

    m_BufCache.Close();
}

INLINE size_t CHTTPParser::Length(void) const
{
    size_t stLength = sizeof(Int);
    if (m_nBufCacheSize > 0)
    {
        stLength  = sizeof(Int)  * 4;
        stLength += sizeof(bool) * 6;
        stLength += sizeof(MARK) * 3;

        stLength += sizeof(Int);
        if (m_Content.GetSize() > 0)
        {
            stLength += sizeof(MARK) * m_Content.GetSize();
        }
        if (IsNetBufferClone() == false)
        {
            stLength += m_nBufCacheSize;
        }
        else
        {
            stLength += m_NetBuffer.Length();
        }
    }
    return stLength;
}

INLINE void CHTTPParser::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> m_nBufCacheSize;
        if (m_nBufCacheSize > 0)
        {
            Stream >> m_nHeaderLength   >> m_nContentLength   >> m_nAllChunkLength;
            Stream >> m_bHeaderComplete >> m_bResponseIdent   >> m_bContentLength
                   >> m_bContentChunked >> m_bContentComplete >> m_bNetBufferClone;

            Stream.Read(&m_Ident, sizeof(MARK) * 3);

            Int nCount = 0;
            Stream >> nCount;
            if (nCount > 0)
            {
                m_Content.Add(nCount, false);
                Stream.Read(m_Content.GetItem(0), sizeof(MARK) * nCount);
            }

            if (m_bNetBufferClone == false)
            {
                m_BufCache.Attach((size_t)m_nBufCacheSize);
                Stream.Read(m_BufCache.GetBuf(), (size_t)m_nBufCacheSize);
            }
            else
            {
                m_NetBuffer.Serialize(Stream);
                m_BufCache.Attach((size_t)m_nBufCacheSize, m_NetBuffer.Data());
            }
        }
    }
    else
    {
        Stream << m_nBufCacheSize;
        if (m_nBufCacheSize > 0)
        {
            Stream << m_nHeaderLength   << m_nContentLength   << m_nAllChunkLength;
            Stream << m_bHeaderComplete << m_bResponseIdent   << m_bContentLength
                   << m_bContentChunked << m_bContentComplete << IsNetBufferClone();

            Stream.Write(&m_Ident, sizeof(MARK) * 3);

            Int nCount = m_Content.GetSize();
            Stream << nCount;
            if (nCount > 0)
            {
                Stream.Write(m_Content.GetItem(0), sizeof(MARK) * nCount);
            }

            if (IsNetBufferClone() == false)
            {
                Stream.Write(m_BufCache.GetBuf(), (size_t)m_nBufCacheSize);
            }
            else
            {
                m_NetBuffer.Serialize(Stream);
            }
        }
    }
}

INLINE bool CHTTPParser::To(CHTTPRequest& Request, bool bOnlyHead) const
{
    if (IsContentComplete() && (IsResponseIdent() == false))
    {
        Request.Close();
        if (IsContentLength())
        {
            Request.m_eContentType = HTTP_CONTENT_LENGTH;
        }
        else if (IsContentChunked())
        {
            Request.m_eContentType = HTTP_CONTENT_CHUNKED;
        }
        else
        {
            Request.m_eContentType = HTTP_CONTENT_NONE;
        }
        DEV_DEBUG(TF("CHTTPParser::To CHTTPRequest m_eContentType = %d"), Request.m_eContentType);
        Request.m_strMethod.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[0].nPos), (size_t)m_Ident[0].nSize);
        DEV_DEBUG(TF("CHTTPParser::To CHTTPRequest Method = %s"), *Request.m_strMethod); // !!!charset

        CCString strQuery;
        CCString strPath = GetIdent(1);
        Int nQuestion = strPath.Find('?');
        if (nQuestion != -1)
        {
            Request.m_strPath  = strPath.Left((size_t)nQuestion);

            ++nQuestion;
            strQuery = strPath.Right(strPath.Length() - (size_t)nQuestion);
        }
        else
        {
            Request.m_strPath  = strPath;
        }
        DEV_DEBUG(TF("CHTTPParser::To CHTTPRequest Path = %s, Query=%s"), *Request.m_strPath, *strQuery); // !!!charset

        if (m_Ident[2].nSize > 0)
        {
            Request.m_strVersion.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[2].nPos), (size_t)m_Ident[2].nSize);
            DEV_DEBUG(TF("CHTTPParser::To CHTTPRequest Version = %s"), *Request.m_strVersion); // !!!charset
        }
        SetHeader(Request);
        if (bOnlyHead == false)
        {
            SetContent(Request.m_Content);
            SetParam(Request, strQuery);
        }
        return true;
    }
    return false;
}

INLINE bool CHTTPParser::To(CHTTPRequest& Request, PCStr pszBuf)
{
    Close();

    size_t stSize = CChar::Length(pszBuf);
    if (Parse((PByte)pszBuf, stSize, true))
    {
        To(Request);
    }
    return false;
}

INLINE bool CHTTPParser::To(CHTTPRequest& Request, PByte pBuf, size_t stSize)
{
    Close();
    if (Parse(pBuf, stSize, true))
    {
        To(Request);
    }
    return false;
}

INLINE bool CHTTPParser::To(CHTTPResponse& Response) const
{
    if (IsContentComplete() && IsResponseIdent())
    {
        Response.Close();
        if (IsContentLength())
        {
            Response.m_eContentType = HTTP_CONTENT_LENGTH;
        }
        else if (IsContentChunked())
        {
            Response.m_eContentType = HTTP_CONTENT_CHUNKED;
        }
        else
        {
            Response.m_eContentType = HTTP_CONTENT_NONE;
        }
        DEV_DEBUG(TF("CHTTPParser::To CHTTPResponse m_eContentType = %d"), Response.m_eContentType);
        Response.m_strVersion.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[0].nPos), (size_t)m_Ident[0].nSize);
        DEV_DEBUG(TF("CHTTPParser::To CHTTPResponse Version = %s"), *Response.m_strVersion); // !!!charset

        Response.m_strStatus.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[1].nPos), (size_t)m_Ident[1].nSize);
        DEV_DEBUG(TF("CHTTPParser::To CHTTPResponse Status = %s"), *Response.m_strStatus); // !!!charset
        if (m_Ident[2].nSize > 0)
        {
            Response.m_strMessage.FillBuffer((PCStr)m_BufCache.GetBuf((size_t)m_Ident[2].nPos), (size_t)m_Ident[2].nSize);
            DEV_DEBUG(TF("CHTTPParser::To CHTTPResponse Message = %s"), *Response.m_strMessage); // !!!charset
        }
        SetHeader(Response);
        SetContent(Response.m_Content);
        return true;
    }
    return false;
}

INLINE bool CHTTPParser::To(CHTTPResponse& Response, PCStr pszBuf)
{
    Close();

    size_t stSize = CChar::Length(pszBuf);
    if (Parse((PByte)pszBuf, stSize, true))
    {
        To(Response);
    }
    return false;
}

INLINE bool CHTTPParser::To(CHTTPResponse& Response, PByte pBuf, size_t stSize)
{
    Close();
    if (Parse(pBuf, stSize, true))
    {
        To(Response);
    }
    return false;
}

INLINE bool CHTTPParser::Attach(CHTTPRequestRef& RequestRef)
{
    if (RequestRef.m_NetBuffer.IsValid() == false)
    {   // net-buffer only
        return false;
    }
    Close();
    AttachNetBuffer(RequestRef.NetBuffer());
    size_t stSize = 0;
    return Parse(nullptr, stSize, true);
}

INLINE bool CHTTPParser::Attach(CHTTPResponseRef& ResponseRef)
{
    if (ResponseRef.m_NetBuffer.IsValid() == false)
    {   // net-buffer only
        return false;
    }
    Close();
    AttachNetBuffer(ResponseRef.NetBuffer());
    size_t stSize = 0;
    return Parse(nullptr, stSize, true);
}

INLINE bool CHTTPParser::Detach(CHTTPRequestRef& RequestRef)
{
    if (IsNetBufferCache() == false)
    {   // net-buffer only
        return false;
    }
    if (IsContentComplete() && (IsResponseIdent() == false))
    {
        RequestRef.Close();
        DetachNetBuffer(RequestRef.NetBuffer());
        if (IsContentLength())
        {
            RequestRef.m_eContentType = HTTP_CONTENT_LENGTH;
        }
        else if (IsContentChunked())
        {
            RequestRef.m_eContentType = HTTP_CONTENT_CHUNKED;
        }
        else
        {
            RequestRef.m_eContentType = HTTP_CONTENT_NONE;
        }
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPRequestRef m_eContentType = %d"), RequestRef.m_eContentType);
        RequestRef.m_strMethod.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[0].nPos), (size_t)m_Ident[0].nSize);
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPRequestRef Method = %c%c%c"), RequestRef.m_strMethod[0], RequestRef.m_strMethod[1], RequestRef.m_strMethod[2]);


        PCStr  pszQuery = nullptr;
        size_t stQuery  = 0;

        RequestRef.m_strPath.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[1].nPos), (size_t)m_Ident[1].nSize);
        Int nQuestion = RequestRef.m_strPath.Find('?');
        if ((nQuestion != -1) && (nQuestion < m_Ident[1].nSize))
        {
            RequestRef.m_strPath.ResetLength((size_t)nQuestion);

            pszQuery = (PCStr)m_BufCache.GetBuf((size_t)(m_Ident[1].nPos + nQuestion + 1));
            stQuery  = (size_t)(m_Ident[1].nSize - nQuestion - 1);
        }
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPRequestRef Path = %d, Query=%d"), RequestRef.m_strPath.Length(), stQuery);

        if (m_Ident[2].nSize > 0)
        {
            RequestRef.m_strVersion.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[2].nPos), (size_t)m_Ident[2].nSize);
            DEV_DEBUG(TF("CHTTPParser::Detach CHTTPRequestRef Version = %c%c%c"), RequestRef.m_strVersion[0], RequestRef.m_strVersion[1], RequestRef.m_strVersion[2]);
        }
        SetHeaderRef(RequestRef);
        SetContentRef(RequestRef.m_Content);
        SetParamRef(RequestRef, pszQuery, stQuery);
        return true;
    }
    return false;
}

INLINE bool CHTTPParser::Detach(CHTTPResponseRef& ResponseRef)
{
    if (IsNetBufferCache() == false)
    {   // net-buffer only
        return false;
    }
    if (IsContentComplete() && IsResponseIdent())
    {
        ResponseRef.Close();
        DetachNetBuffer(ResponseRef.NetBuffer());
        if (IsContentLength())
        {
            ResponseRef.m_eContentType = HTTP_CONTENT_LENGTH;
        }
        else if (IsContentChunked())
        {
            ResponseRef.m_eContentType = HTTP_CONTENT_CHUNKED;
        }
        else
        {
            ResponseRef.m_eContentType = HTTP_CONTENT_NONE;
        }
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPResponseRef m_eContentType = %d"), ResponseRef.m_eContentType);
        ResponseRef.m_strVersion.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[0].nPos), (size_t)m_Ident[0].nSize);
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPResponseRef Version = %c%c%c"), ResponseRef.m_strVersion[0], ResponseRef.m_strVersion[1], ResponseRef.m_strVersion[2]);

        ResponseRef.m_strStatus.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[1].nPos), (size_t)m_Ident[1].nSize);
        DEV_DEBUG(TF("CHTTPParser::Detach CHTTPResponseRef Status = %c%c%c"), ResponseRef.m_strStatus[0], ResponseRef.m_strStatus[1], ResponseRef.m_strStatus[2]);
        if (m_Ident[2].nSize > 0)
        {
            ResponseRef.m_strMessage.SetBufferLength((PCStr)m_BufCache.GetBuf((size_t)m_Ident[2].nPos), (size_t)m_Ident[2].nSize);
            DEV_DEBUG(TF("CHTTPParser::Detach CHTTPResponseRef Message = %d"), ResponseRef.m_strMessage.Length());
        }
        SetHeaderRef(ResponseRef);
        SetContentRef(ResponseRef.m_Content);
        return true;
    }
    return false;
}

INLINE bool CHTTPParser::From(CHTTPRequest& Request)
{
    CCString strFormat;
    Request.Format(strFormat);

    Close();
    size_t stSize = strFormat.Length();
    return Parse((PByte)*strFormat, stSize, true);
}

INLINE bool CHTTPParser::From(CHTTPResponse& Response)
{
    CCString strFormat;
    Response.Format(strFormat);

    Close();
    size_t stSize = strFormat.Length();
    return Parse((PByte)*strFormat, stSize, true);
}

INLINE bool CHTTPParser::Format(CCString& strFormat) const
{
    if (IsContentComplete())
    {
        return strFormat.FillBuffer((PCStr)m_BufCache.GetBuf(), (size_t)m_nBufCacheSize);
    }
    return false;
}

INLINE bool CHTTPParser::Format(CStream& Stream) const
{
    assert(Stream.IsWrite());
    if (IsContentComplete())
    {
        return (Stream.Write(m_BufCache.GetBuf(), (size_t)m_nBufCacheSize) == (size_t)m_nBufCacheSize);
    }
    return false;
}

INLINE bool CHTTPParser::Parse(PByte pBuf, size_t& stSize, bool bComplete)
{
    if (IsContentComplete())
    {
        stSize = 0;
        return true;
    }
    Int nOldCacheSize = Cache(pBuf, stSize);
    if (nOldCacheSize < 0)
    {
        return false;
    }
    return ParseCache(nOldCacheSize, stSize, bComplete);
}

INLINE bool CHTTPParser::Parse(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool bComplete)
{
    if (IsContentComplete())
    {
        stSize = 0;
        return true;
    }
    bool bHandOver = false;
    Int nOldCacheSize = CacheNetBuffer(NetAttr, pParam, bHandOver);
    if (nOldCacheSize < 0)
    {
        return false;
    }
    bool bRet = ParseCache(nOldCacheSize, stSize, bComplete);
    if (bRet)
    {
        bool bRebind = false;
        bRet = m_NetBuffer.Check(pParam, m_nBufCacheSize, (Int)stSize, bHandOver, bRebind);
        if (bRebind)
        {
            m_BufCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
            m_BufCache.GetBuf()[m_nBufCacheSize] = 0;
        }
    }
    return bRet;
}

INLINE bool CHTTPParser::ParseComplete(void)
{
    if (IsContentComplete())
    {
        return true;
    }
    if (IsHeaderComplete())
    {
        m_bContentComplete = true; // connect-closed
        m_nContentLength   = m_nBufCacheSize - m_nHeaderLength;
        if (m_nContentLength == 0)
        {
            return true;
        }

        if (IsContentChunked()) // Transfer-Encoding : chunked first
        {
            // check last chunk size
            Int nCount = m_Content.GetSize();
            if (nCount > 0)
            {
                --nCount;
                if ((m_Content[nCount].nPos + m_Content[nCount].nSize) > m_nBufCacheSize)
                {
                    m_Content[nCount].nSize = m_nBufCacheSize - m_Content[nCount].nPos;
                }
            }
        }
        else
        {
            // m_bContentLength = true;

            MARK Content;
            Content.nPos  = m_nHeaderLength;
            Content.nSize = m_nContentLength;

            m_Content.Add(Content);
        }
    }
    return m_bContentComplete;
}

INLINE const NET_BUFFER& CHTTPParser::NetBuffer(void) const
{
    return m_NetBuffer;
}

INLINE NET_BUFFER& CHTTPParser::NetBuffer(void)
{
    return m_NetBuffer;
}

INLINE void CHTTPParser::SetNetBufferClone(bool bEnable)
{
    m_bNetBufferClone = bEnable;
}

INLINE Int CHTTPParser::Cache(PByte pBuf, size_t stSize)
{
    if (m_NetBuffer.IsValid() == false)
    {
        Int nOldCacheSize = m_nBufCacheSize;
        m_nBufCacheSize  += (Int)(stSize);

        if (m_BufCache.Size() <= (size_t)m_nBufCacheSize)
        {
            size_t stNewBufSize = DEF::Align<size_t>((size_t)m_nBufCacheSize, LMT_BUF) * 2; // double grow
            CBufReadStream BufCache(stNewBufSize);
            if (nOldCacheSize > 0)
            {
                MM_SAFE::Cpy(BufCache.GetBuf(), stNewBufSize, m_BufCache.GetBuf(), (size_t)nOldCacheSize);
            }
            m_BufCache.Attach(BufCache);
        }
        MM_SAFE::Cpy(m_BufCache.GetBuf((size_t)nOldCacheSize), stSize, pBuf, stSize);
        m_BufCache.GetBuf()[m_nBufCacheSize] = 0;

        return nOldCacheSize;
    }
    return -1;
}

INLINE Int CHTTPParser::CacheNetBuffer(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver)
{
    Int nOldCacheSize = m_nBufCacheSize;
    if (m_NetBuffer.IsValid() == false)
    {
        if (m_nBufCacheSize > 0)
        {
            DEV_DEBUG(TF("CHTTPParser::CacheNetBuffer m_NetBuffer.IsValid() = %d m_nBufCacheSize = %d"), m_NetBuffer.IsValid(), m_nBufCacheSize);
            return -1;
        }
        if (m_NetBuffer.Create(NetAttr, pParam, bHandOver) == false)
        {
            return -1;
        }
        m_BufCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
    }
    else
    {
        bool bRebind = false;
        if (m_NetBuffer.Append(NetAttr, pParam->pData, pParam->nSize, m_nBufCacheSize, bRebind) == false)
        {
            return -1;
        }
        if (bRebind)
        {
            m_BufCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
        }
    }
    m_nBufCacheSize += pParam->nSize;
    m_BufCache.GetBuf()[m_nBufCacheSize] = 0;
    return nOldCacheSize;
}

INLINE bool CHTTPParser::AllocNetBuffer(const CBufReadStream& BufCache)
{
    m_NetBuffer.pCache = MObject::MCAlloc(m_NetBuffer.index);
    assert(m_NetBuffer.pCache != nullptr);
    m_BufCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
    MM_SAFE::Cpy(m_BufCache.GetBuf(), (size_t)m_NetBuffer.nSize, BufCache.GetBuf(), (size_t)m_nBufCacheSize);
    return true;
}

INLINE bool CHTTPParser::FreeNetBuffer(void)
{
    m_NetBuffer.Free();
    return true;
}

INLINE bool CHTTPParser::AttachNetBuffer(NET_BUFFER& NetBuffer)
{
    m_NetBuffer = NetBuffer;
    NetBuffer.Reset();
    m_BufCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
    m_nBufCacheSize = (m_NetBuffer.nSize - 1);
    return true;
}

INLINE bool CHTTPParser::DetachNetBuffer(NET_BUFFER& NetBuffer)
{
    NetBuffer = m_NetBuffer;
    m_NetBuffer.Reset();
    return true;
}

INLINE bool CHTTPParser::ParseCache(Int nOldCacheSize, size_t& stSize, bool bComplete)
{
    bool bRet = false;
    if (IsHeaderComplete() == false)
    {
        bRet = ParseHeaderComplete();
    }
    if (IsHeaderComplete())
    {
        bRet = true;            // request Content-Length or Transfer-Encoding : chunked maybe lost, request complete by connect-closed
        if (IsContentChunked()) // Transfer-Encoding : chunked first
        {
            bRet = ParseContentChunked();
        }
        else if (IsContentLength())
        {
            bRet = ParseContentLength();
        }
        else
        {
            bComplete = true;
        }
    }
    if (bComplete)
    {
        bRet = ParseComplete();
    }
    else if (IsContentComplete())
    {
        // get buf size
        stSize = (size_t)(m_nBufCacheSize - nOldCacheSize);
    }
    return bRet;
}

INLINE bool CHTTPParser::ParseHeaderComplete(void)
{
    PCStr pszBegin = (PCStr)m_BufCache.GetBuf();
    if (m_nHeaderLength == 0)
    {
        PCStr pszIdent = pszBegin;
        if (SkipChars(pszIdent) == false)
        {
            return false;
        }
        pszIdent = CChar::Str(pszIdent, "\r\n");
        if (pszIdent != nullptr)
        {
            if (ParseIdent(pszBegin, pszIdent) == false)
            {
                return false;
            }

            PCStr pszHeaderComplete = CChar::Str(pszIdent, "\r\n\r\n");  // find header complete string
            if (pszHeaderComplete != nullptr)
            {
                m_nHeaderLength = (Int)(pszHeaderComplete - pszBegin) + 4; // length of "\r\n\r\n"
            }
        }
    }
    if (m_nHeaderLength > 0)
    {
        m_bHeaderComplete = true;
        if (ParseHeader(pszBegin) == false)
        {
            return false;
        }
    }
    return true;
}

INLINE bool CHTTPParser::ParseIdent(PCStr pszBegin, PCStr pszCRLF)
{
    PCStr pszIdent = pszBegin;
    if (SkipChars(pszIdent))
    {
        if (CChar::Cmpin(pszIdent, "HTTP/", 5) != 0)
        {
            // request
            m_bResponseIdent = false;
            // method
            PCStr p = CChar::Chr(pszIdent, ' ');
            if (p == nullptr)
            {
                return false;
            }
            m_Ident[0].nPos  = (Int)(pszIdent - pszBegin);
            m_Ident[0].nSize = (Int)(p - pszIdent);
            pszIdent = p + 1;
            // path
            p = CChar::Chr(pszIdent, ' ');
            if (p != nullptr)
            {
                m_Ident[1].nPos  = (Int)(pszIdent - pszBegin);
                m_Ident[1].nSize = (Int)(p - pszIdent);
                pszIdent = p + 1;
                // http version
                if (CChar::Cmpin(pszIdent, "HTTP/", 5) == 0)
                {
                    pszIdent += 5;
                }
                m_Ident[2].nPos  = (Int)(pszIdent - pszBegin);
                m_Ident[2].nSize = (Int)(pszCRLF - pszIdent);
            }
            else
            {
                m_Ident[1].nPos  = (Int)(pszIdent - pszBegin);
                m_Ident[1].nSize = (Int)(pszCRLF - pszIdent);
                m_Ident[2].nPos  = m_Ident[1].nPos;
                m_Ident[2].nSize = 0;
            }
        }
        else
        {
            // response
            m_bResponseIdent = true;
            // version
            PCStr p = CChar::Chr(pszIdent, ' ');
            if (p == nullptr)
            {
                return false;
            }
            pszIdent += 5;
            m_Ident[0].nPos  = (Int)(pszIdent - pszBegin);
            m_Ident[0].nSize = (Int)(p - pszIdent);
            pszIdent = p + 1;
            // status
            p = CChar::Chr(pszIdent, ' ');
            if (p != nullptr)
            {
                m_Ident[1].nPos  = (Int)(pszIdent - pszBegin);
                m_Ident[1].nSize = (Int)(p - pszIdent);
                pszIdent = p + 1;
                // message
                if (pszIdent < pszCRLF)
                {
                    m_Ident[2].nPos  = (Int)(pszIdent - pszBegin);
                    m_Ident[2].nSize = (Int)(pszCRLF - pszIdent);
                }
                else
                {
                    m_Ident[2].nPos  = m_Ident[1].nPos;
                    m_Ident[2].nSize = 0;
                }
            }
            else
            {
                m_Ident[1].nPos  = (Int)(pszIdent - pszBegin);
                m_Ident[1].nSize = (Int)(pszCRLF - pszIdent);
                m_Ident[2].nPos  = m_Ident[1].nPos;
                m_Ident[2].nSize = 0;
            }
        }
        return true;
    }
    return false;
}

INLINE bool CHTTPParser::ParseHeader(PCStr pszBegin)
{
    PCStr  pszHeader = pszBegin + m_Ident[2].nPos + m_Ident[2].nSize + 2;                    // length of "\r\n"
    size_t stLen     = (size_t)(m_nHeaderLength - (m_Ident[2].nPos + m_Ident[2].nSize + 2)); // length of "\r\n"
    CCString strHeader(pszHeader, stLen);
    strHeader.Lower();

    pszHeader = *strHeader;
    if (SkipChars(pszHeader) == false)
    {
        return false;
    }
    PCStr p = CChar::Str(pszHeader, "transfer-encoding");
    if (p != nullptr)
    {
        p += 17;
        if (SkipChars(p, ':') == false)
        {
            return false;
        }
        m_bContentChunked = (CChar::Cmpin(p, "chunked", 7) == 0);
        return true;
    }
    p = CChar::Str(pszHeader, "content-length");
    if (p != nullptr)
    {
        p += 14;
        if (SkipChars(p, ':') == false)
        {
            return false;
        }
        m_nContentLength = CChar::ToInt(p);
        m_bContentLength = true;
    }
    return true;
}

INLINE bool CHTTPParser::ParseContentChunked(void)
{
    while (m_nBufCacheSize >= (m_nHeaderLength + m_nContentLength))
    {
        if (m_nBufCacheSize == (m_nHeaderLength + m_nContentLength))
        {
            break;
        }
        PCStr pszChunk = (PCStr)m_BufCache.GetBuf(m_nHeaderLength + m_nContentLength);
        PCStr pszEnd   = CChar::Str(pszChunk, "\r\n");
        if (pszEnd == nullptr)
        {
            break;
        }
        Int nChunkSize = (Int)CChar::ToLong(pszChunk, nullptr, RADIXT_HEX);

        m_nAllChunkLength += nChunkSize;
        m_nContentLength  += (Int)(pszEnd - pszChunk) + nChunkSize + 4; // length of "\r\n\r\n"
        
        if (nChunkSize > 0)
        {
            MARK Content;
            Content.nPos  = (Int)(pszEnd + 2 - (PCStr)m_BufCache.GetBuf());
            Content.nSize = nChunkSize;

            m_Content.Add(Content);
        }
        else
        {
            m_bContentComplete = true;
            m_nBufCacheSize = m_nHeaderLength + m_nContentLength;
            m_BufCache.GetBuf()[m_nBufCacheSize] = 0;
            break;
        }
    }
    return true;
}

INLINE bool CHTTPParser::ParseContentLength(void)
{
    m_bContentComplete = (m_nBufCacheSize >= (m_nHeaderLength + m_nContentLength));
    if (IsContentComplete())
    {
        m_nBufCacheSize = m_nHeaderLength + m_nContentLength;
        m_BufCache.GetBuf()[m_nBufCacheSize] = 0;

        MARK Content;
        Content.nPos  = m_nHeaderLength;
        Content.nSize = m_nContentLength;

        m_Content.Add(Content);
    }
    return true;
}

INLINE bool CHTTPParser::SetHeader(CHTTPRequest& Request) const
{
    PCStr pszBegin = (PCStr)m_BufCache.GetBuf() + m_Ident[2].nPos + m_Ident[2].nSize + 2;  // length of "\r\n"
    PCStr pszEnd   = (PCStr)m_BufCache.GetBuf() + m_nHeaderLength - 4;                     // length of "\r\n\r\n"

    CCString strName;
    CCString strValue;

    while (SkipChars(pszBegin) && (pszBegin < pszEnd))
    {
        PCStr pszCRLF = CChar::Str(pszBegin, "\r\n");
        if ((pszCRLF == nullptr) || (pszCRLF > pszEnd))
        {
            break;
        }
        PCStr pszKey = CChar::Chr(pszBegin, ':');
        if ((pszKey != nullptr) && (pszKey < pszCRLF))
        {
            if (pszBegin < pszKey)
            {
                strName.FillBuffer(pszBegin, (pszKey - pszBegin));
                strName.Trim();

                pszBegin = pszKey + 1; //
                if (SkipChars(pszBegin) == false)
                {
                    break;
                }
                if (pszBegin < pszCRLF)
                {
                    strValue.FillBuffer(pszBegin, (pszCRLF - pszBegin));
                    strValue.Trim();
                }
                else
                {
                    strValue.Empty();
                }
                DEV_DEBUG(TF("CHTTPParser::SetHeader CHTTPRequest1 %s : %s"), *strName, *strValue); // !!!charset
                if (strName.Cmpin("Cookie", 6) == 0)
                {
                    Request.m_Cookie.Parse(*strValue);
                }
                else
                {
#ifndef __MODERN_CXX_NOT_SUPPORTED
                    Request.m_Header->Add(std::move(strName), std::move(strValue));
#else
                    Request.m_Header->Add(strName, strValue);
#endif
                }
                if (pszBegin < pszCRLF)
                {
                    pszBegin = pszCRLF + 2;
                }
            }
            else
            {
                pszBegin = pszKey + 1; //
            }
        }
        else
        {
            assert(pszBegin < pszCRLF);
            strName.FillBuffer(pszBegin, (pszCRLF - pszBegin));
            strName.Trim();

            DEV_DEBUG(TF("CHTTPParser::SetHeader CHTTPRequest2 %s : null"), *strName); // !!!charset
#ifndef __MODERN_CXX_NOT_SUPPORTED
            Request.m_Header->Add(std::move(strName));
#else
            Request.m_Header->Add(strName);
#endif
            pszBegin = pszCRLF + 2;
        }
        if (pszBegin >= pszEnd)
        {
            break;
        }
    }
    return true;
}

INLINE bool CHTTPParser::SetHeader(CHTTPResponse& Response) const
{
    PCStr pszBegin = (PCStr)m_BufCache.GetBuf() + m_Ident[2].nPos + m_Ident[2].nSize + 2;  // length of "\r\n"
    PCStr pszEnd   = (PCStr)m_BufCache.GetBuf() + m_nHeaderLength - 4;                     // length of "\r\n\r\n"

    CCString strName;
    CCString strValue;

    while (SkipChars(pszBegin) && (pszBegin < pszEnd))
    {
        PCStr pszCRLF = CChar::Str(pszBegin, "\r\n");
        if ((pszCRLF == nullptr) || (pszCRLF > pszEnd))
        {
            break;
        }
        PCStr pszKey  = CChar::Chr(pszBegin, ':');
        if ((pszKey != nullptr) && (pszKey < pszCRLF))
        {
            if (pszBegin < pszKey)
            {
                strName.FillBuffer(pszBegin, (pszKey - pszBegin));
                strName.Trim();

                pszBegin = pszKey + 1; //
                if (SkipChars(pszBegin) == false)
                {
                    break;
                }
                if (pszBegin < pszCRLF)
                {
                    strValue.FillBuffer(pszBegin, (pszCRLF - pszBegin));
                    strValue.Trim();
                }
                else
                {
                    strValue.Empty();
                }
                DEV_DEBUG(TF("CHTTPParser::SetHeader CHTTPResponse1 %s : %s"), *strName, *strValue); // !!!charset
                if (strName.Cmpin("Set-Cookie", 10) == 0)
                {
                    Int nIndex = Response.m_Cookies.Add(1, false);
                    Response.m_Cookies[nIndex].Parse(*strValue);
                }
                else
                {
#ifndef __MODERN_CXX_NOT_SUPPORTED
                    Response.m_Header->Add(std::move(strName), std::move(strValue));
#else
                    Response.m_Header->Add(strName, strValue);
#endif
                }
                if (pszBegin < pszCRLF)
                {
                    pszBegin = pszCRLF + 2;
                }
            }
            else
            {
                pszBegin = pszKey + 1; //
            }
        }
        else
        {
            assert(pszBegin < pszCRLF);
            strName.FillBuffer(pszBegin, (pszCRLF - pszBegin));
            strName.Trim();

            DEV_DEBUG(TF("CHTTPParser::SetHeader CHTTPResponse2 %s : null"), *strName); // !!!charset
#ifndef __MODERN_CXX_NOT_SUPPORTED
            Response.m_Header->Add(std::move(strName));
#else
            Response.m_Header->Add(strName);
#endif
            pszBegin = pszCRLF + 2;
        }
        if (pszBegin >= pszEnd)
        {
            break;
        }
    }
    return true;
}

INLINE bool CHTTPParser::SetHeaderRef(CHTTPRequestRef& RequestRef) const
{
    PCStr pszBegin = (PCStr)m_BufCache.GetBuf() + m_Ident[2].nPos + m_Ident[2].nSize + 2;  // length of "\r\n"
    PCStr pszEnd   = (PCStr)m_BufCache.GetBuf() + m_nHeaderLength - 4;                     // length of "\r\n\r\n"

    while (SkipChars(pszBegin) && (pszBegin < pszEnd))
    {
        PCStr pszCRLF = CChar::Str(pszBegin, "\r\n");
        if ((pszCRLF == nullptr) || (pszCRLF > pszEnd))
        {
            break;
        }
        PCStr pszKey = CChar::Chr(pszBegin, ':');
        if ((pszKey != nullptr) && (pszKey < pszCRLF))
        {
            if (pszBegin < pszKey)
            {
                PCStr  pszName = pszBegin;
                size_t stName  = GetLength(pszName, pszKey);

                pszBegin = pszKey + 1; //
                if (SkipChars(pszBegin) == false)
                {
                    break;
                }
                size_t stValue  = 0;
                PCStr  pszValue = pszBegin;
                if (pszBegin < pszCRLF)
                {
                    stValue = GetLength(pszValue, pszCRLF);
                }
                DEV_DEBUG(TF("CHTTPParser::SetHeaderRef CHTTPRequestRef1 %p---%d : %p---%d"), pszName, stName, pszValue, stValue);
                if ((stName == 6) && (CChar::Cmpin(pszName, "Cookie", 6) == 0))
                {
                    RequestRef.m_Cookie.Parse(pszValue, (pszValue + stValue));
                }
                else
                {
                    RequestRef.m_Header.SetAt(pszName, stName, pszValue, stValue);
                }
                if (pszBegin < pszCRLF)
                {
                    pszBegin = pszCRLF + 2;
                }
            }
            else
            {
                pszBegin = pszKey + 1; //
            }
        }
        else
        {
            assert(pszBegin < pszCRLF);
            size_t stName = GetLength(pszBegin, pszCRLF);
            DEV_DEBUG(TF("CHTTPParser::SetHeaderRef CHTTPRequestRef2 %p---%d : null"), pszBegin, stName);

            CCStringRef strNameRef(pszBegin, stName);
            RequestRef.m_Header->Add(strNameRef);

            pszBegin = pszCRLF + 2;
        }
        if (pszBegin >= pszEnd)
        {
            break;
        }
    }
    return true;
}

INLINE bool CHTTPParser::SetHeaderRef(CHTTPResponseRef& ResponseRef) const
{
    PCStr pszBegin = (PCStr)m_BufCache.GetBuf() + m_Ident[2].nPos + m_Ident[2].nSize + 2;  // length of "\r\n"
    PCStr pszEnd   = (PCStr)m_BufCache.GetBuf() + m_nHeaderLength - 4;                     // length of "\r\n\r\n"

    while (SkipChars(pszBegin) && (pszBegin < pszEnd))
    {
        PCStr pszCRLF = CChar::Str(pszBegin, "\r\n");
        if ((pszCRLF == nullptr) || (pszCRLF > pszEnd))
        {
            break;
        }
        PCStr pszKey  = CChar::Chr(pszBegin, ':');
        if ((pszKey != nullptr) && (pszKey < pszCRLF))
        {
            if (pszBegin < pszKey)
            {
                PCStr  pszName = pszBegin;
                size_t stName  = GetLength(pszName, pszKey);

                pszBegin = pszKey + 1; //
                if (SkipChars(pszBegin) == false)
                {
                    break;
                }
                size_t stValue  = 0;
                PCStr  pszValue = pszBegin;
                if (pszBegin < pszCRLF)
                {
                    stValue = GetLength(pszValue, pszCRLF);
                }
                DEV_DEBUG(TF("CHTTPParser::SetHeaderRef CHTTPResponseRef1 %p---%d : %p---%d"), pszName, stName, pszValue, stValue);
                if ((stName == 10) && (CChar::Cmpin(pszName, "Set-Cookie", 10) == 0))
                {
                    Int nIndex = ResponseRef.m_Cookies.Add(1, false);
                    ResponseRef.m_Cookies[nIndex].Parse(pszValue, (pszValue + stValue));
                }
                else
                {
                    ResponseRef.m_Header.SetAt(pszName, stName, pszValue, stValue);
                }
                if (pszBegin < pszCRLF)
                {
                    pszBegin = pszCRLF + 2;
                }
            }
            else
            {
                pszBegin = pszKey + 1; //
            }
        }
        else
        {
            assert(pszBegin < pszCRLF);
            size_t stName = GetLength(pszBegin, pszCRLF);
            DEV_DEBUG(TF("CHTTPParser::SetHeaderRef CHTTPResponseRef2 %p---%d : null"), pszBegin, stName);

            CCStringRef strNameRef(pszBegin, stName);
            ResponseRef.m_Header->Add(strNameRef);

            pszBegin = pszCRLF + 2;
        }
        if (pszBegin >= pszEnd)
        {
            break;
        }
    }
    return true;
}

INLINE bool CHTTPParser::SetContent(ARY_BUF& Content) const
{
    if (IsContentChunked())
    {
        Int nCount = m_Content.GetSize();
        if (nCount > 0)
        {
            Content.Add(nCount, false);

            for (Int i = 0; i < nCount; ++i)
            {
                DEV_DEBUG(TF("CHTTPParser::SetContent chunked-%d : pos=%d, size=%d"), i, m_Content[i].nPos, m_Content[i].nSize);

                Content[i].Attach((size_t)m_Content[i].nSize);
                MM_SAFE::Cpy(Content[i].GetBuf(), Content[i].Size(), m_BufCache.GetBuf((size_t)m_Content[i].nPos), (size_t)m_Content[i].nSize);
            }
        }
    }
    else if (m_nContentLength > 0)
    {
        DEV_DEBUG(TF("CHTTPParser::SetContent pos=%d, size=%d"), m_Content[0].nPos, m_Content[0].nSize);
        Content.Add(1, false);
        Content[0].Attach((size_t)m_nContentLength);
        MM_SAFE::Cpy(Content[0].GetBuf(), Content[0].Size(), m_BufCache.GetBuf((size_t)m_Content[0].nPos), (size_t)m_Content[0].nSize);
    }
    return true;
}

INLINE bool CHTTPParser::SetContentRef(ARY_BUF& Content) const
{
    if (IsContentChunked())
    {
        Int nCount = m_Content.GetSize();
        if (nCount > 0)
        {
            Content.Add(nCount, false);
            for (Int i = 0; i < nCount; ++i)
            {
                DEV_DEBUG(TF("CHTTPParser::SetContentRef chunked-%d : pos=%d, size=%d"), i, m_Content[i].nPos, m_Content[i].nSize);
                Content[i].Attach((size_t)m_Content[i].nSize, m_BufCache.GetBuf((size_t)m_Content[i].nPos));
            }
        }
    }
    else if (m_nContentLength > 0)
    {
        DEV_DEBUG(TF("CHTTPParser::SetContentRef pos=%d, size=%d"), m_Content[0].nPos, m_Content[0].nSize);
        Content.Add(1, false);
        Content[0].Attach((size_t)m_Content[0].nSize, m_BufCache.GetBuf((size_t)m_Content[0].nPos));
    }
    return true;
}

INLINE bool CHTTPParser::SetParam(CHTTPRequest& Request, CCString& strQuery) const
{
    if (strQuery.Length() > 0)
    {
        DEV_DEBUG(TF("CHTTPParser::SetParam query=%s"), *strQuery); // !!!charset
        Request.m_Param.Parse(*strQuery);
    }
    else if ((IsContentChunked() == false) && (Request.m_strMethod.Cmpin("POST", 4) == 0))
    {
        PAIR_HEADER* pPair = Request.m_Header->Find("Content-Type");
        if ((pPair == nullptr) || (pPair->m_V.Cmpin("application/x-www-form-urlencoded", 33) == 0))
        {
            strQuery.FillBuffer((PCStr)Request.m_Content[0].GetBuf(), Request.m_Content[0].Size());
            DEV_DEBUG(TF("CHTTPParser::SetParam POST=%s"), *strQuery); // !!!charset
            Request.m_Param.Parse(*strQuery);
        }
    }
    return true;
}

INLINE bool CHTTPParser::SetParamRef(CHTTPRequestRef& RequestRef, PCStr pszQuery, size_t stQuery) const
{
    if (stQuery > 0)
    {
        DEV_DEBUG(TF("CHTTPParser::SetParamRef query=%d"), stQuery);
        RequestRef.m_Param.Parse(pszQuery, (pszQuery + stQuery));
    }
    else if ((IsContentChunked() == false) && (RequestRef.m_strMethod.Cmpin("POST", 4) == 0))
    {
        const PAIR_HEADER_REF* pPair = RequestRef.m_Header->Find("Content-Type");
        if ((pPair == nullptr) || (pPair->m_V.Cmpin("application/x-www-form-urlencoded", 33) == 0))
        {
            pszQuery = (PCStr)RequestRef.m_Content[0].GetBuf();
            stQuery  = (size_t)RequestRef.m_Content[0].Size();
            DEV_DEBUG(TF("CHTTPParser::SetParamRef POST---application/x-www-form-urlencoded=%p -- %d"), pszQuery, stQuery);
            RequestRef.m_Param.Parse(pszQuery, (pszQuery + stQuery));
        }
    }
    return true;
}

///////////////////////////////////////////////////////////////////
// CWSParser
INLINE bool CWSParser::CheckUpgrade(const CHTTPRequest& Request, bool& bRet)
{
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequest Method = %s"), *Request.GetMethod()); // !!!charset
    if (Request.GetMethod().Cmpin("GET", 3) != 0)
    {
        return false;
    }
    const CHTTPHeader& Header = Request.GetHeader();
    CCString strValue = Header.Find("Connection");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequest Connection = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("Upgrade", 7) != 0)
    {
        return false;
    }
    strValue = Header.Find("Upgrade");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequest Upgrade = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("websocket", 9) != 0)
    {
        return false;
    }
    strValue = Header.Find("Sec-WebSocket-Key");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequest Sec-WebSocket-Key = %s, len=%d"), *strValue, strValue.Length()); // !!!charset
    if (strValue.Length() != CWSTraits::WS_KEY_REQUEST)
    {
        bRet = false;
        return false;
    }
    return true;
}

INLINE bool CWSParser::CheckUpgrade(const CHTTPRequestRef& RequestRef, bool& bRet)
{
#ifdef __RUNTIME_DEBUG__
    CCString strMethod;
    RequestRef.GetMethod(strMethod);
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequestRef Method = %s"), *strMethod); // !!!charset
#endif
    if (RequestRef.GetMethod().Cmpin("GET", 3) != 0)
    {
        return false;
    }
    const CHTTPHeaderRef& HeaderRef = RequestRef.GetHeader();
    CCString strValue = HeaderRef.Find("Connection");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequestRef Connection = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("Upgrade", 7) != 0)
    {
        return false;
    }
    strValue = HeaderRef.Find("Upgrade");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequestRef Upgrade = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("websocket", 9) != 0)
    {
        return false;
    }
    strValue = HeaderRef.Find("Sec-WebSocket-Key");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPRequestRef Sec-WebSocket-Key = %s, len=%d"), *strValue, strValue.Length()); // !!!charset
    if (strValue.Length() != CWSTraits::WS_KEY_REQUEST)
    {
        bRet = false;
        return false;
    }
    return true;
}

INLINE bool CWSParser::CheckUpgrade(const CHTTPResponse& Response, bool& bRet)
{
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponse StatusCode = %d"), Response.GetStatusCode());
    if (Response.GetStatusCode() != CHTTPTraits::SWITCHING_PROTOCOLS)
    {
        return false;
    }
    const CHTTPHeader& Header = Response.GetHeader();
    CCString strValue = Header.Find("Connection");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponse Connection = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("Upgrade", 7) != 0)
    {
        return false;
    }
    strValue = Header.Find("Upgrade");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponse Upgrade = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("websocket", 9) != 0)
    {
        return false;
    }
    strValue = Header.Find("Sec-WebSocket-Accept");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponse Sec-WebSocket-Accept = %s, len=%d"), *strValue, strValue.Length()); // !!!charset
    if (strValue.Length() != CWSTraits::WS_KEY_RESPONSE)
    {
        bRet = false;
        return false;
    }
    return true;
}

INLINE bool CWSParser::CheckUpgrade(const CHTTPResponseRef& ResponseRef, bool& bRet)
{
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponseRef StatusCode = %d"), ResponseRef.GetStatusCode());
    if (ResponseRef.GetStatusCode() != CHTTPTraits::SWITCHING_PROTOCOLS)
    {
        return false;
    }
    const CHTTPHeaderRef& HeaderRef = ResponseRef.GetHeader();
    CCString strValue = HeaderRef.Find("Connection");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponseRef Connection = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("Upgrade", 7) != 0)
    {
        return false;
    }
    strValue = HeaderRef.Find("Upgrade");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponseRef Upgrade = %s"), *strValue); // !!!charset
    if (strValue.Cmpin("websocket", 9) != 0)
    {
        return false;
    }
    strValue = HeaderRef.Find("Sec-WebSocket-Accept");
    DEV_DEBUG(TF("CWSParser::CheckUpgrade CHTTPResponse Sec-WebSocket-Accept = %s, len=%d"), *strValue, strValue.Length()); // !!!charset
    if (strValue.Length() != CWSTraits::WS_KEY_RESPONSE)
    {
        bRet = false;
        return false;
    }
    return true;
}

INLINE bool CWSParser::UpgradeRequest(CHTTPRequest& Request, PCStr pszHost, Int nVersion)
{
    CHTTPHeader& Header = Request.GetHeader();
    if (pszHost != nullptr)
    {
        Header.SetAt("Host", pszHost);
    }
    Header.SetAt("Upgrade", "websocket");
    Header.SetAt("Connection", "Upgrade");

    ULLong ullRand = 0;
    ullRand  = (ULLong)((CPlatform::GetOSRunningTime() ^ CWSTraits::WS_KEY_MASK) << 32);
    ullRand += (ULLong)(CPlatform::GetRunningTime() & CWSTraits::WS_KEY_MASK);
    DEV_DEBUG(TF("CWSParser::UpgradeRequest rand=%llx"), ullRand);
    CCString strIn;
    strIn.Format("%016llx", ullRand);
    DEV_DEBUG(TF("CWSParser::UpgradeRequest strIn=%s"), *strIn); // !!!charset

    CCString strValue;
    CBase64::Encode(strValue, strIn);
    DEV_DEBUG(TF("CWSParser::UpgradeRequest Sec-WebSocket-Key=%s"), *strValue); // !!!charset

    Header.SetAt("Sec-WebSocket-Key", strValue);

    strValue.ToString(nVersion);
    Header.SetAt("Sec-WebSocket-Version", strValue);
    return true;
}

INLINE bool CWSParser::UpgradeResponse(const CHTTPRequest& Request, CHTTPResponse& Response)
{
    CCString strValue = Request.GetHeader().Find("Sec-WebSocket-Key");
    assert(strValue.Length() == CWSTraits::WS_KEY_REQUEST);
    strValue += AcceptUID;
    CBufReadStream brs(strValue.Length(), (PByte)strValue.GetBuffer());
    CSHA1::NUM Num;
    CSHA1::Crypto(brs, Num);
    CCString strIn((CCString::PCTStr)Num, CSHA1::SHA1C_NUM);
    CBase64::Encode(strValue, strIn);

    Response.SetStatusCode(CHTTPTraits::SWITCHING_PROTOCOLS);
    Response.SetMessage(CHTTPTraits::StatusToString(CHTTPTraits::SWITCHING_PROTOCOLS));

    CHTTPHeader& Header = Response.GetHeader();
    Header.SetAt("Upgrade", "websocket");
    Header.SetAt("Connection", "Upgrade");
    Header.SetAt("Sec-WebSocket-Accept", strValue);

    const CHTTPTraits::PAIR_HEADER* pHeader = Request.Header().Find("Sec-WebSocket-Protocol");
    if (pHeader != nullptr)
    {
        CCString strProtocol = pHeader->m_V;
        Int nPos = strProtocol.Find(TF(','));
        if (nPos != -1)
        {
            strProtocol.ResetLength((size_t)nPos);
        }
        Header.SetAt("Sec-WebSocket-Protocol", strProtocol);
    }
    return true;
}

INLINE bool CWSParser::UpgradeResponse(const CHTTPRequestRef& RequestRef, CHTTPResponse& Response)
{
    CCString strValue = RequestRef.GetHeader().Find("Sec-WebSocket-Key");
    assert(strValue.Length() == CWSTraits::WS_KEY_REQUEST);
    strValue += AcceptUID;
    CBufReadStream brs(strValue.Length(), (PByte)strValue.GetBuffer());
    CSHA1::NUM Num;
    CSHA1::Crypto(brs, Num);
    CCString strIn((CCString::PCTStr)Num, CSHA1::SHA1C_NUM);
    CBase64::Encode(strValue, strIn);

    Response.SetStatusCode(CHTTPTraits::SWITCHING_PROTOCOLS);
    Response.SetMessage(CHTTPTraits::StatusToString(CHTTPTraits::SWITCHING_PROTOCOLS));

    CHTTPHeader& Header = Response.GetHeader();
    Header.SetAt("Upgrade", "websocket");
    Header.SetAt("Connection", "Upgrade");
    Header.SetAt("Sec-WebSocket-Accept", strValue);

    const CHTTPTraits::PAIR_HEADER_REF* pHeader = RequestRef.Header().Find("Sec-WebSocket-Protocol");
    if (pHeader != nullptr)
    {
        CCString strProtocol = pHeader->m_V;
        Int nPos = strProtocol.Find(TF(','));
        if (nPos != -1)
        {
            strProtocol.ResetLength((size_t)nPos);
        }
        Header.SetAt("Sec-WebSocket-Protocol", strProtocol);
    }
    return true;
}

INLINE bool CWSParser::InitFrameHead(UShort& usFrame, UInt& uMask, size_t& stFrame, size_t stSize)
{
    usFrame &= (UShort)(CWSTraits::WS_FRAME_MASK);
    stFrame  = 0;
    if (stSize < CWSTraits::WS_EXT_SHORT)
    {
        stFrame += CWSTraits::WS_NORMAL_LEN;
        usFrame |= (UShort)stSize;
    }
    else if (stSize <= USHRT_MAX)
    {
        stFrame += CWSTraits::WS_SHORT_LEN;
        usFrame |= (UShort)CWSTraits::WS_EXT_SHORT;
    }
    else
    {
        assert(stSize <= LONG_MAX);
        stFrame += CWSTraits::WS_LONG_LEN;
        usFrame |= (UShort)CWSTraits::WS_EXT_LONG;
    }
    if ((usFrame & CWSTraits::WS_MASK) == CWSTraits::WS_MASK)
    {
        if (stSize > 0)
        {
            stFrame += CWSTraits::WS_MASK_LEN;
        }
        else
        {
            usFrame &= ~CWSTraits::WS_MASK;
        }
    }
    if (((usFrame & CWSTraits::WS_MASK) == CWSTraits::WS_MASK) && (uMask == 0))
    {
        uMask  = (UInt)(CPlatform::GetOSRunningTime() ^ CWSTraits::WS_KEY_MASK);
        uMask += (UInt)(CPlatform::GetRunningTime() & CWSTraits::WS_KEY_MASK);
    }
    return true;
}

INLINE bool CWSParser::InitFrameHead(UShort& usFrame, size_t& stFrame, size_t stSize)
{
    UInt uMask = 0;
    return InitFrameHead(usFrame, uMask, stFrame, stSize);
}

INLINE bool CWSParser::SetFrameHead(PByte pBuf, UShort usFrame, UInt uMask, size_t stFrame, size_t stSize)
{
    assert((usFrame & CWSTraits::WS_FRAME_MASK) != 0);
    assert(stFrame >= CWSTraits::WS_NORMAL_LEN);
    PUShort pusHead = (PUShort)pBuf;
#ifndef __ARCH_TARGET_BIGENDIAN__
    pusHead[0] = CPlatform::ByteSwap(usFrame);
#else
    pusHead[0] = usFrame;
#endif
    ++pusHead;

    if (stSize > USHRT_MAX)
    {
        assert((usFrame & CWSTraits::WS_EXT_MASK) == CWSTraits::WS_EXT_LONG);
        assert(stFrame >= CWSTraits::WS_LONG_LEN);
        PULLong pullLen = (PULLong)(pusHead);
#ifndef __ARCH_TARGET_BIGENDIAN__
        pullLen[0] = CPlatform::ByteSwap((ULLong)stSize);
#else
        pullLen[0] = (ULLong)stSize;
#endif
        pusHead = (PUShort)(pullLen + 1);
    }
    else if (stSize >= CWSTraits::WS_EXT_SHORT)
    {
        assert((usFrame & CWSTraits::WS_EXT_MASK) == CWSTraits::WS_EXT_SHORT);
        assert(stFrame >= CWSTraits::WS_SHORT_LEN);
#ifndef __ARCH_TARGET_BIGENDIAN__
        pusHead[0] = CPlatform::ByteSwap((UShort)stSize);
#else
        pusHead[0] = (UShort)stSize;
#endif
        ++pusHead;
    }
#ifdef __RUNTIME_DEBUG__
    else
    {
        assert((usFrame & CWSTraits::WS_EXT_MASK) < CWSTraits::WS_EXT_SHORT);
    }
#endif
    if (usFrame & CWSTraits::WS_MASK)
    {
        assert(uMask != 0);
        assert(stSize > 0);
        PByte pMask = (PByte)(&uMask);
        MM_SAFE::Cpy(pusHead, sizeof(UInt), pMask, sizeof(UInt));

        pBuf += stFrame;
        assert((uintptr_t)pBuf == (uintptr_t)(pusHead + 2));
        for (size_t i = 0; i < stSize; ++i)
        {
            pBuf[i] = (pBuf[i] ^ pMask[i % sizeof(UInt)]);
        }
    }
    return true;
}

INLINE size_t CWSParser::SetFrameHead(PByte pBuf, UShort usFrame, size_t stSize, UInt uMask)
{
    size_t stFrame = 0;
    InitFrameHead(usFrame, uMask, stFrame, stSize);
    SetFrameHead(pBuf, usFrame, uMask, stFrame, stSize);
    return (stFrame + stSize);
}

INLINE CWSParser::CWSParser(void)
: m_nFrameCache(0)
, m_nFrameSize(0)
, m_nFrameCode(0)
, m_nFrameData(0)
, m_bHeadComplete(false)
, m_bFrameMasked(false)
, m_bFramePayload(false)
, m_bFrameComplete(false)
, m_bNetBufferClone(false)
{
}

INLINE CWSParser::~CWSParser(void)
{
}

INLINE CWSParser::CWSParser(const CWSParser& aSrc)
: m_nFrameCache(aSrc.m_nFrameCache)
, m_nFrameSize(aSrc.m_nFrameSize)
, m_nFrameCode(aSrc.m_nFrameCode)
, m_nFrameData(aSrc.m_nFrameData)
, m_bHeadComplete(aSrc.m_bHeadComplete)
, m_bFrameMasked(aSrc.m_bFrameMasked)
, m_bFramePayload(aSrc.m_bFramePayload)
, m_bFrameComplete(aSrc.m_bFrameComplete)
, m_bNetBufferClone(aSrc.m_bNetBufferClone)
{
    if (aSrc.IsNetBufferCache())
    {
        m_NetBuffer = aSrc.m_NetBuffer;
        AllocNetBuffer(aSrc.m_FrameCache);
    }
    else
    {
        m_FrameCache.Attach(aSrc.m_FrameCache);
    }
}

INLINE CWSParser& CWSParser::operator=(const CWSParser& aSrc)
{
    if (this != &aSrc)
    {
        m_nFrameCache     = aSrc.m_nFrameCache;
        m_nFrameSize      = aSrc.m_nFrameSize;
        m_nFrameCode      = aSrc.m_nFrameCode;
        m_nFrameData      = aSrc.m_nFrameData;

        m_bHeadComplete   = aSrc.m_bHeadComplete;
        m_bFrameMasked    = aSrc.m_bFrameMasked;
        m_bFramePayload   = aSrc.m_bFramePayload;
        m_bFrameComplete  = aSrc.m_bFrameComplete;
        m_bNetBufferClone = aSrc.m_bNetBufferClone;

        if (aSrc.IsNetBufferCache())
        {
            m_NetBuffer = aSrc.m_NetBuffer;
            AllocNetBuffer(aSrc.m_FrameCache);
        }
        else
        {
            m_FrameCache.Attach(aSrc.m_FrameCache);
        }
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CWSParser::CWSParser(CWSParser&& aSrc)
: m_nFrameCache(aSrc.m_nFrameCache)
, m_nFrameSize(aSrc.m_nFrameSize)
, m_nFrameCode(aSrc.m_nFrameCode)
, m_nFrameData(aSrc.m_nFrameData)
, m_bHeadComplete(aSrc.m_bHeadComplete)
, m_bFrameMasked(aSrc.m_bFrameMasked)
, m_bFramePayload(aSrc.m_bFramePayload)
, m_bFrameComplete(aSrc.m_bFrameComplete)
, m_bNetBufferClone(aSrc.m_bNetBufferClone)
{
    if (aSrc.IsNetBufferCache())
    {
        m_NetBuffer = aSrc.m_NetBuffer;
        aSrc.m_NetBuffer.Reset();
    }
    if (aSrc.m_FrameCache.Size() > 0)
    {
        m_FrameCache.Attach(aSrc.m_FrameCache);
    }
    aSrc.Close();
}

INLINE CWSParser& CWSParser::operator=(CWSParser&& aSrc)
{
    if (this != &aSrc)
    {
        m_nFrameCache     = aSrc.m_nFrameCache;
        m_nFrameSize      = aSrc.m_nFrameSize;
        m_nFrameCode      = aSrc.m_nFrameCode;
        m_nFrameData      = aSrc.m_nFrameData;

        m_bHeadComplete   = aSrc.m_bHeadComplete;
        m_bFrameMasked    = aSrc.m_bFrameMasked;
        m_bFramePayload   = aSrc.m_bFramePayload;
        m_bFrameComplete  = aSrc.m_bFrameComplete;
        m_bNetBufferClone = aSrc.m_bNetBufferClone;

        if (aSrc.IsNetBufferCache())
        {
            m_NetBuffer = aSrc.m_NetBuffer;
            aSrc.m_NetBuffer.Reset();
        }
        if (aSrc.m_FrameCache.Size() > 0)
        {
            m_FrameCache.Attach(aSrc.m_FrameCache);
        }
        aSrc.Close();
    }
    return (*this);
}
#endif

INLINE Int CWSParser::GetFrameSize(void) const
{
    return m_nFrameSize;
}

INLINE Int CWSParser::GetFrameCode(void) const
{
    return m_nFrameCode;
}

INLINE Int CWSParser::GetPayloadPos(void) const
{
    return m_nFrameData;
}

INLINE Int CWSParser::GetPayloadSize(void) const
{
    return (m_nFrameSize - m_nFrameData);
}

INLINE bool CWSParser::IsHeadComplete(void) const
{
    return m_bHeadComplete;
}

INLINE bool CWSParser::IsFrameMasked(void) const
{
    return m_bFrameMasked;
}

INLINE bool CWSParser::IsFramePayload(void) const
{
    return m_bFramePayload;
}

INLINE bool CWSParser::IsFrameComplete(void) const
{
    return m_bFrameComplete;
}

INLINE bool CWSParser::IsControlFrame(void) const
{
    return CWSTraits::IsControlFrame(m_nFrameCode);
}

INLINE bool CWSParser::IsFinalFrame(void) const
{
    return CWSTraits::IsFinalFrame(m_nFrameCode);
}

INLINE bool CWSParser::IsNetBufferClone(void) const
{
    return (IsNetBufferCache() && m_bNetBufferClone);
}

INLINE bool CWSParser::IsNetBufferCache(void) const
{
    return m_NetBuffer.IsValid();
}

INLINE const CBufReadStream& CWSParser::GetCache(void) const
{
    return m_FrameCache;
}

INLINE CBufReadStream& CWSParser::GetCache(void)
{
    return m_FrameCache;
}

INLINE const PByte CWSParser::GetCacheBuf(void) const
{
    return m_FrameCache.GetBuf();
}

INLINE const PByte CWSParser::GetCachePayload(void) const
{
    return m_FrameCache.GetBuf((size_t)m_nFrameData);
}

INLINE void CWSParser::Close(void)
{
    m_nFrameCache     = 0;
    m_nFrameSize      = 0;
    m_nFrameCode      = 0;
    m_nFrameData      = 0;

    m_bHeadComplete   = false;
    m_bFrameMasked    = false;
    m_bFramePayload   = false;
    m_bFrameComplete  = false;
    //m_bNetBufferClone = false;

    m_NetBuffer.Free();

    m_FrameCache.Close();
}

INLINE size_t CWSParser::Length(void) const
{
    size_t stLength = sizeof(Int);
    if (m_nFrameCache > 0)
    {
        stLength  = sizeof(Int)  * 4;
        stLength += sizeof(bool) * 5;
        if (IsNetBufferClone() == false)
        {
            stLength += m_nFrameCache;
        }
        else
        {
            stLength += m_NetBuffer.Length();
        }
    }
    return stLength;
}

INLINE void CWSParser::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> m_nFrameCache;
        if (m_nFrameCache > 0)
        {
            Stream >> m_nFrameSize >> m_nFrameCode >> m_nFrameData;
            Stream >> m_bHeadComplete >> m_bFrameMasked >> m_bFramePayload >> m_bFrameComplete >> m_bNetBufferClone;

            if (m_bNetBufferClone == false)
            {
                m_FrameCache.Attach((size_t)m_nFrameCache);
                Stream.Read(m_FrameCache.GetBuf(), (size_t)m_nFrameCache);
            }
            else
            {
                m_NetBuffer.Serialize(Stream);
                m_FrameCache.Attach((size_t)m_nFrameCache, m_NetBuffer.Data());
            }
        }
    }
    else
    {
        Stream << m_nFrameCache;
        if (m_nFrameCache > 0)
        {
            Stream << m_nFrameSize << m_nFrameCode << m_nFrameData;
            Stream << m_bHeadComplete << m_bFrameMasked << m_bFramePayload << m_bFrameComplete << IsNetBufferClone();

            if (IsNetBufferClone() == false)
            {
                Stream.Write(m_FrameCache.GetBuf(), (size_t)m_nFrameCache);
            }
            else
            {
                m_NetBuffer.Serialize(Stream);
            }
        }
    }
}

INLINE bool CWSParser::To(CStream& Stream)  const
{
    assert(Stream.IsWrite());
    if (IsFrameComplete())
    {
        return GetFrame(Stream);
    }
    return false;
}

INLINE bool CWSParser::To(CStream& Stream, PCStr pszBuf)
{
    Close();

    size_t stSize = CChar::Length(pszBuf);
    if (Parse((PByte)pszBuf, stSize) == RET_OKAY)
    {
        return To(Stream);
    }
    return false;
}

INLINE bool CWSParser::To(CStream& Stream, PByte pBuf, size_t stSize)
{
    Close();
    if (Parse(pBuf, stSize) == RET_OKAY)
    {
        return To(Stream);
    }
    return false;
}

INLINE bool CWSParser::To(CCString& strContent) const
{
    if (IsFrameComplete())
    {
        return GetFrame(strContent);
    }
    return false;
}

INLINE bool CWSParser::To(CCString& strContent, PCStr pszBuf)
{
    Close();

    size_t stSize = CChar::Length(pszBuf);
    if (Parse((PByte)pszBuf, stSize) == RET_OKAY)
    {
        return To(strContent);
    }
    return false;
}

INLINE bool CWSParser::To(CCString& strContent, PByte pBuf, size_t stSize)
{
    Close();
    if (Parse(pBuf, stSize) == RET_OKAY)
    {
        return To(strContent);
    }
    return false;
}

INLINE bool CWSParser::From(CStream& Stream, Int nFrame, UInt uMask)
{
    assert(Stream.IsRead());
    Close();
    return SetFrame((UShort)nFrame, uMask, &Stream);
}

INLINE bool CWSParser::From(const CCString& strContent, Int nFrame, UInt uMask)
{
    Close();

    CBufReadStream brs(strContent.Length(), (PByte)strContent.GetBuffer());
    return SetFrame((UShort)nFrame, uMask, &brs);
}

INLINE bool CWSParser::From(Int nFrame, UInt uMask)
{
    Close();
    return SetFrame((UShort)nFrame, uMask);
}

INLINE bool CWSParser::Format(CStream& Stream) const
{
    assert(Stream.IsWrite());
    if (IsFrameComplete())
    {
        Stream.Write(m_FrameCache.GetBuf(), (size_t)m_nFrameSize);
        return true;
    }
    return false;
}

INLINE bool CWSParser::Format(CCString& strFormat) const
{
    if (IsFrameComplete())
    {
        strFormat.FillBuffer((PCStr)m_FrameCache.GetBuf(), (size_t)m_nFrameSize);
        return true;
    }
    return false;
}

INLINE Int CWSParser::Parse(PByte pBuf, size_t& stSize)
{
    if (IsFrameComplete())
    {
        stSize = 0;
        return RET_OKAY;
    }
    Int nFrameCache = Cache(pBuf, stSize);
    if (nFrameCache < 0)
    {
        return DATA_NOT_CONSISTENT;
    }
    return ParseCache(nFrameCache, stSize);
}

INLINE Int CWSParser::Parse(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize)
{
    if (IsFrameComplete())
    {
        stSize = 0;
        return RET_OKAY;
    }
    bool bHandOver = false;
    Int nFrameCache = CacheNetBuffer(NetAttr, pParam, bHandOver);
    if (nFrameCache < 0)
    {
        return DATA_NOT_CONSISTENT;
    }
    Int nRet = ParseCache(nFrameCache, stSize);
    if (nRet == RET_OKAY)
    {
        bool bRebind = false;
        if (m_NetBuffer.Check(pParam, m_nFrameCache, (Int)stSize, bHandOver, bRebind))
        {
            if (bRebind)
            {
                m_FrameCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
                m_FrameCache.GetBuf()[m_nFrameCache] = 0;
            }
        }
        else
        {
            nRet = DATA_NOT_CONSISTENT;
        }
    }
    return nRet;
}

INLINE Int CWSParser::Parse(Int nFrameCode, Int nFrameData, Int nPayload)
{
    assert(m_NetBuffer.IsValid() == false);

    m_nFrameCache = nFrameData + nPayload;
    m_nFrameSize  = m_nFrameCache;
    m_nFrameCode  = (nFrameCode & WS_HEAD_MASK);
    m_nFrameData  = nFrameData;

    m_bHeadComplete  = true;
    m_bFrameMasked   = false;
    m_bFramePayload  = (nPayload > 0);
    m_bFrameComplete = true;
    return RET_OKAY;
}

INLINE bool CWSParser::Attach(const NET_BUFFER& NetBuffer)
{
    Detach();
    m_NetBuffer = NetBuffer;
    return true;
}

INLINE bool CWSParser::Attach(Int nFrameCode, Int nFrameData, Int nPayload)
{
    assert(m_NetBuffer.IsValid());

    m_nFrameCache = nFrameData + nPayload;
    m_nFrameSize  = m_nFrameCache;
    m_nFrameCode  = (nFrameCode & WS_HEAD_MASK);
    m_nFrameData  = nFrameData;

    m_bHeadComplete  = true;
    m_bFrameMasked   = false;
    m_bFramePayload  = (nPayload > 0);
    m_bFrameComplete = true;

    m_FrameCache.Attach((size_t)m_nFrameCache, m_NetBuffer.Data());
    return true;
}

INLINE bool CWSParser::Attach(NET_BUFFER& NetBuffer, Int nFrameCode, Int nFrameData, Int nPayload)
{
    Close();
    m_NetBuffer = NetBuffer;
    return Attach(nFrameCode, nFrameData, nPayload);
}

INLINE void CWSParser::Detach(NET_BUFFER& NetBuffer)
{
    NetBuffer = m_NetBuffer;
    Detach();
}

INLINE void CWSParser::Detach(Int& nFrameSize, Int& nFrameCode, Int& nFrameData)
{
    nFrameSize = m_nFrameSize;
    nFrameCode = m_nFrameCode;
    nFrameData = m_nFrameData;
    Detach();
}

INLINE void CWSParser::Detach(NET_BUFFER& NetBuffer, Int& nFrameSize, Int& nFrameCode, Int& nFrameData)
{
    NetBuffer = m_NetBuffer;
    Detach(nFrameSize, nFrameCode, nFrameData);
}

INLINE void CWSParser::Detach(void)
{
    m_NetBuffer.Reset();
}

INLINE const NET_BUFFER& CWSParser::NetBuffer(void) const
{
    return m_NetBuffer;
}

INLINE NET_BUFFER& CWSParser::NetBuffer(void)
{
    return m_NetBuffer;
}

INLINE void CWSParser::SetNetBufferClone(bool bEnable)
{
    m_bNetBufferClone = bEnable;
}

INLINE Int CWSParser::Cache(PByte pBuf, size_t stSize)
{
    if (m_NetBuffer.IsValid() == false)
    {
        Int nFrameCache = m_nFrameCache;
        m_nFrameCache  += (Int)stSize;

        if (m_FrameCache.Size() <= (size_t)m_nFrameCache)
        {
            size_t stNewCacheSize = DEF::Align<size_t>((size_t)m_nFrameCache, LMT_BUF) * 2; // double grow
            CBufReadStream FrameCache(stNewCacheSize);
            if (nFrameCache > 0)
            {
                MM_SAFE::Cpy(FrameCache.GetBuf(), stNewCacheSize, m_FrameCache.GetBuf(), (size_t)nFrameCache);
            }
            m_FrameCache.Attach(FrameCache);
        }
        MM_SAFE::Cpy(m_FrameCache.GetBuf((size_t)nFrameCache), stSize, pBuf, stSize);
        m_FrameCache.GetBuf()[m_nFrameCache] = 0;

        return nFrameCache;
    }
    return -1;
}

INLINE Int CWSParser::CacheNetBuffer(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, bool& bHandOver)
{
    Int nFrameCache = m_nFrameCache;
    if (m_NetBuffer.IsValid() == false)
    {
        if (m_nFrameCache > 0)
        {
            DEV_DEBUG(TF("CWSParser::CacheNetBuffer m_NetBuffer.IsValid() = %d m_nFrameCache = %d"), m_NetBuffer.IsValid(), m_nFrameCache);
            return -1;
        }
        if (m_NetBuffer.Create(NetAttr, pParam, bHandOver) == false)
        {
            return -1;
        }
        m_FrameCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
    }
    else
    {
        bool bRebind = false;
        if (m_NetBuffer.Append(NetAttr, pParam->pData, pParam->nSize, m_nFrameCache, bRebind) == false)
        {
            return -1;
        }
        if (bRebind)
        {
            m_FrameCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
        }
    }
    m_nFrameCache += pParam->nSize;
    m_FrameCache.GetBuf()[m_nFrameCache] = 0;
    return nFrameCache;
}

INLINE bool CWSParser::AllocNetBuffer(const CBufReadStream& BufCache)
{
    m_NetBuffer.pCache = MObject::MCAlloc(m_NetBuffer.index);
    assert(m_NetBuffer.pCache != nullptr);
    m_FrameCache.Attach((size_t)m_NetBuffer.nSize, m_NetBuffer.Data());
    MM_SAFE::Cpy(m_FrameCache.GetBuf(), (size_t)m_NetBuffer.nSize, BufCache.GetBuf(), (size_t)m_nFrameCache);
    return true;
}

INLINE bool CWSParser::FreeNetBuffer(void)
{
    m_NetBuffer.Free();
    return true;
}

INLINE Int CWSParser::ParseCache(Int nFrameCache, size_t& stSize)
{
    if (m_nFrameCache < CWSTraits::WS_NORMAL_LEN)
    {
        return RET_OKAY;
    }
    Int nRet = PROTOCOL_ERROR;
    if (IsHeadComplete() == false)
    {
        nRet = ParseFrameHead();
    }
    if (IsHeadComplete())
    {
        nRet = ParsePayload();
    }
    DEV_DEBUG(TF("CWSParser::ParseCache usFrame = %x, cache = %d, size = %d, data = %d, nRet=%d, nFrameCache = %d"),
              m_nFrameCode, m_nFrameCache, m_nFrameSize, m_nFrameData, nRet, nFrameCache);
    if (IsFrameComplete())
    {
        // get buf size
        stSize = (size_t)(m_nFrameSize - nFrameCache);
    }
    return nRet;
}

INLINE Int CWSParser::ParseFrameHead(void)
{
    UShort usFrame = (UShort)m_nFrameCode;
    if (usFrame == 0)
    {
        usFrame = *((PUShort)m_FrameCache.GetBuf());
        usFrame = CPlatform::ByteSwap(usFrame);
        m_nFrameCode = (Int)usFrame;

        //m_bHeadComplete  = false;
        //m_bFrameMasked   = false;
        //m_bFramePayload  = false;
        //m_bFrameComplete = false;
    }
    if (m_nFrameSize == 0)
    {
        m_nFrameData = 0;
        if ((usFrame & CWSTraits::WS_MASK) == CWSTraits::WS_MASK)
        {
           m_nFrameData += CWSTraits::WS_MASK_LEN;
        }
        switch (usFrame & CWSTraits::WS_EXT_MASK)
        {
        case CWSTraits::WS_EXT_LONG:
            {
                m_nFrameData += CWSTraits::WS_LONG_LEN;
                if (m_nFrameCache >= CWSTraits::WS_LONG_LEN)
                {
                    PULLong pullLen = (PULLong)(m_FrameCache.GetBuf(CWSTraits::WS_NORMAL_LEN));
#ifndef __ARCH_TARGET_BIGENDIAN__
                    m_nFrameSize    = (Int)CPlatform::ByteSwap(pullLen[0]);
#else
                    m_nFrameSize    = (Int)pullLen[0];
#endif
                    m_nFrameSize   += m_nFrameData;
                }
            }
            break;
        case CWSTraits::WS_EXT_SHORT:
            {
                m_nFrameData += CWSTraits::WS_SHORT_LEN;
                if (m_nFrameCache >= CWSTraits::WS_SHORT_LEN)
                {
                    PUShort pusLen = (PUShort)(m_FrameCache.GetBuf(CWSTraits::WS_NORMAL_LEN));
#ifndef __ARCH_TARGET_BIGENDIAN__
                    m_nFrameSize   = (Int)CPlatform::ByteSwap(pusLen[0]);
#else
                    m_nFrameSize    = (Int)pusLen[0];
#endif
                    m_nFrameSize  += m_nFrameData;
                }
            }
            break;
        default:
            {
                m_nFrameData += CWSTraits::WS_NORMAL_LEN;
                m_nFrameSize  = (Int)(usFrame & CWSTraits::WS_EXT_MASK);
                m_nFrameSize += m_nFrameData;
            }
        }
        DEV_DEBUG(TF("CWSParser::ParseFrameHead usFrame = %x, cache = %d, size = %d, data = %d"), usFrame, m_nFrameCache, m_nFrameSize, m_nFrameData);
    }
    m_bHeadComplete = (m_nFrameSize > 0);
    return RET_OKAY;
}

INLINE Int CWSParser::ParsePayload(void)
{
    DEV_DEBUG(TF("CWSParser::ParsePayload usFrame = %x, cache = %d, size = %d, data = %d"), m_nFrameCode, m_nFrameCache, m_nFrameSize, m_nFrameData);
    if (m_nFrameCache >= m_nFrameSize)
    {
        m_nFrameCache = m_nFrameSize;
        m_FrameCache.GetBuf()[m_nFrameCache] = 0;
        if ((m_nFrameCode & CWSTraits::WS_MASK) == CWSTraits::WS_MASK)
        {
            assert(m_nFrameData > CWSTraits::WS_MASK_LEN);
            PByte pMask = m_FrameCache.GetBuf(m_nFrameData - CWSTraits::WS_MASK_LEN);

            PByte pData = pMask + CWSTraits::WS_MASK_LEN;
            size_t stSize = (size_t)(m_nFrameSize - m_nFrameData);
            for (size_t i = 0; i < stSize; ++i)
            {
                pData[i] = (pData[i] ^ pMask[i % sizeof(UInt)]);
            }
        }
        m_bFramePayload  = (m_nFrameSize > m_nFrameData);
        m_bFrameComplete = true;
    }
    return RET_OKAY;
}

INLINE bool CWSParser::GetFrame(CStream& Stream) const
{
    if (IsFramePayload())
    {
        DEV_DEBUG(TF("CWSParser::GetFrame CStream usFrame = %x, size = %d, data = %d"), m_nFrameCode, m_nFrameSize, m_nFrameData);
        assert(m_nFrameSize > m_nFrameData);
        size_t stSize = (size_t)(m_nFrameSize - m_nFrameData);
        return (Stream.Write(m_FrameCache.GetBuf((size_t)m_nFrameData), stSize) == stSize);
    }
    return true;
}

INLINE bool CWSParser::GetFrame(CCString& strContent) const
{
    if (IsFramePayload())
    {
        DEV_DEBUG(TF("CWSParser::GetFrame CCString usFrame = %x, size = %d, data = %d"), m_nFrameCode, m_nFrameSize, m_nFrameData);
        assert(m_nFrameSize > m_nFrameData);
        size_t stSize = (size_t)(m_nFrameSize - m_nFrameData);
        return strContent.FillBuffer((PCStr)m_FrameCache.GetBuf((size_t)m_nFrameData), stSize);
    }
    return true;
}

INLINE bool CWSParser::SetFrame(UShort usFrame, UInt uMask, CStream* pStream)
{
    size_t stSize  = (pStream != nullptr) ? pStream->Rest() : 0;
    size_t stFrame = 0;
    InitFrameHead(usFrame, uMask, stFrame, stSize);
    DEV_DEBUG(TF("CWSParser::SetFrame usFrame = %x, stFrame = %d, stSize = %d"), usFrame, stFrame, stSize);

    assert(m_nFrameCache == 0);
    assert(m_nFrameSize  == 0);
    if (m_FrameCache.Attach(stFrame + stSize))
    {
        m_nFrameCache = (Int)(stFrame + stSize);
        m_nFrameSize  = m_nFrameCache;
        m_nFrameCode  = (Int)usFrame;
        m_nFrameData  = (Int)stFrame;
        if (stSize > 0)
        {
            pStream->Read(m_FrameCache.GetBuf(stFrame), stSize);
        }
        m_FrameCache.GetBuf()[m_nFrameCache] = 0;
        SetFrameHead(m_FrameCache.GetBuf(), usFrame, uMask, stFrame, stSize);

        m_bHeadComplete  = true;
        m_bFrameMasked   = ((usFrame & CWSTraits::WS_MASK) == CWSTraits::WS_MASK);
        m_bFramePayload  = (stSize > 0);
        m_bFrameComplete = true;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CWEBSession
INLINE CWEBSession::CWEBSession(CNETTraits::Socket sSocket, UInt uType)
: m_sSocket(sSocket)
, m_uType(uType)
, m_uStatus(0)
, m_uUpgraded(FALSE)
, m_uHandled(FALSE)
, m_stMaxBlock(WEB_DEF_SIZE_BLOCK)
, m_stMaxCache(WEB_DEF_SIZE_CACHE)
{
}

INLINE CWEBSession::~CWEBSession(void)
{
}

INLINE CNETTraits::Socket CWEBSession::GetSocket(void) const
{
    return m_sSocket;
}

INLINE UInt CWEBSession::GetType(void) const
{
    return m_uType;
}

INLINE UInt CWEBSession::GetStatus(void) const
{
    return m_uStatus;
}

INLINE void CWEBSession::SetStatus(UInt uStatus)
{
    m_uStatus = uStatus;
}

INLINE bool CWEBSession::CheckUpgrade(void) const
{
    return (m_uUpgraded == TRUE);
}

INLINE void CWEBSession::SetUpgraded(void)
{
    if (m_uUpgraded == FALSE)
    {
        m_uUpgraded = TRUE;
    }
}

INLINE bool CWEBSession::ApplyHandle(bool bApply)
{
    if (bApply)
    {
        return (CAtomics::CompareExchange<UInt>(&m_uHandled, TRUE, FALSE) == FALSE);
    }
    CAtomics::Exchange<UInt>((PUInt)&m_uHandled, FALSE);
    return true;
}

INLINE size_t CWEBSession::GetMaxBlockSize(void) const
{
    return m_stMaxBlock;
}

INLINE void CWEBSession::SetMaxBlockSize(size_t stMaxBlock)
{
    m_stMaxBlock = stMaxBlock;
}

INLINE size_t CWEBSession::GetMaxCacheSize(void) const
{
    return m_stMaxCache;
}

INLINE void CWEBSession::SetMaxCacheSize(size_t stMaxCache)
{
    m_stMaxCache = stMaxCache;
}

///////////////////////////////////////////////////////////////////
// CHTTPSession
INLINE CHTTPSession::CHTTPSession(CNETTraits::Socket sSocket)
: CWEBSession(sSocket, WEB_SESSION_HTTP)
{
}

INLINE CHTTPSession::CHTTPSession(CNETTraits::Socket sSocket, UInt uType)
: CWEBSession(sSocket, uType)
{
}

INLINE CHTTPSession::CHTTPSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef)
: CWEBSession(sSocket, WEB_SESSION_HTTP)
, m_EventHandlerPtr(EventHandlerRef)
{
}

INLINE CHTTPSession::CHTTPSession(CNETTraits::Socket sSocket, UInt uType, CEventHandlerPtr& EventHandlerRef)
: CWEBSession(sSocket, uType)
, m_EventHandlerPtr(EventHandlerRef)
{
}

INLINE CHTTPSession::~CHTTPSession(void)
{
}

INLINE bool CHTTPSession::OnData(PByte pData, size_t& stSize, bool& bComplete)
{
    if (m_Parser.Parse(pData, stSize, bComplete))
    {
        bComplete = m_Parser.IsContentComplete();
        if (bComplete)
        {
            return true;
        }
        if ( ((size_t)m_Parser.GetHeaderLength() <= GetMaxBlockSize()) &&
             ((size_t)m_Parser.GetCacheSize() < GetMaxCacheSize()) )
        {
            return true;
        }
        DEV_DEBUG(TF("CHTTPSession::OnData parser header len = %d, max-header len = %d, parser cache size = %d, max-cache size = %d"),
                  m_Parser.GetHeaderLength(), GetMaxBlockSize(), m_Parser.GetCacheSize(), GetMaxCacheSize());
    }
    bComplete = false;
    return false;
}

INLINE bool CHTTPSession::OnData(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool& bComplete)
{
    if (m_Parser.Parse(NetAttr, pParam, stSize, bComplete))
    {
        bComplete = m_Parser.IsContentComplete();
        if (bComplete)
        {
            return true;
        }
        if ( ((size_t)m_Parser.GetHeaderLength() <= GetMaxBlockSize()) &&
             ((size_t)m_Parser.GetCacheSize() < GetMaxCacheSize()) )
        {
            return true;
        }
        DEV_DEBUG(TF("CHTTPSession::OnData NetBuffer parser header len = %d, max-header len = %d, parser cache size = %d, max-cache size = %d"),
                  m_Parser.GetHeaderLength(), GetMaxBlockSize(), m_Parser.GetCacheSize(), GetMaxCacheSize());
    }
    bComplete = false;
    return false;
}

INLINE bool CHTTPSession::OnDataHandled(void)
{
    m_Parser.Close();
    return ApplyHandle(false);
}

INLINE bool CHTTPSession::CheckDataCompleted(void)
{
    if (m_Parser.ParseComplete())
    {
        return ApplyHandle();
    }
    return false;
}

INLINE size_t CHTTPSession::Length(void) const
{
    return m_Parser.Length();
}

INLINE void CHTTPSession::Serialize(CStream& Stream)
{
    m_Parser.Serialize(Stream);
}

INLINE CEventHandlerPtr& CHTTPSession::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CHTTPSession::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE CHTTPParser& CHTTPSession::GetParser(void)
{
    return m_Parser;
}

INLINE const CHTTPParser& CHTTPSession::GetParser(void) const
{
    return m_Parser;
}

///////////////////////////////////////////////////////////////////
// CWSSession
INLINE CWSSession::CWSSession(CNETTraits::Socket sSocket)
: CWEBSession(sSocket, WEB_SESSION_WS)
, m_llOnline(0)
, m_uLiveCheck(CWEBSession::WEB_DEF_LIVE_CHECK)
, m_uCheckFlag(WS_CHECK_AUTO|WS_CHECK_CLOSE)
, m_nUniteCode(0)
, m_nUniteData(0)
, m_nUniteSize(0)
{
}

INLINE CWSSession::CWSSession(CNETTraits::Socket sSocket, UInt uType)
: CWEBSession(sSocket, uType)
, m_llOnline(0)
, m_uLiveCheck(CWEBSession::WEB_DEF_LIVE_CHECK)
, m_uCheckFlag(WS_CHECK_AUTO|WS_CHECK_CLOSE)
, m_nUniteCode(0)
, m_nUniteData(0)
, m_nUniteSize(0)
{
}

INLINE CWSSession::CWSSession(CNETTraits::Socket sSocket, CEventHandlerPtr& EventHandlerRef)
: CWEBSession(sSocket, WEB_SESSION_WS)
, m_llOnline(0)
, m_uLiveCheck(CWEBSession::WEB_DEF_LIVE_CHECK)
, m_uCheckFlag(WS_CHECK_AUTO|WS_CHECK_CLOSE)
, m_nUniteCode(0)
, m_nUniteData(0)
, m_nUniteSize(0)
, m_EventHandlerPtr(EventHandlerRef)
{
}

INLINE CWSSession::CWSSession(CNETTraits::Socket sSocket, UInt uType, CEventHandlerPtr& EventHandlerRef)
: CWEBSession(sSocket, uType)
, m_llOnline(0)
, m_uLiveCheck(CWEBSession::WEB_DEF_LIVE_CHECK)
, m_uCheckFlag(WS_CHECK_AUTO|WS_CHECK_CLOSE)
, m_nUniteCode(0)
, m_nUniteData(0)
, m_nUniteSize(0)
, m_EventHandlerPtr(EventHandlerRef)
{
}

INLINE CWSSession::~CWSSession(void)
{
}

INLINE bool CWSSession::OnData(PByte pData, size_t& stSize, bool& bComplete)
{
    if (GetStatus() != 0)
    {
        return false;
    }
    m_llOnline = CPlatform::GetRunningTime();
    Int nRet = m_Parser.Parse(pData, stSize);
    if (nRet == RET_OKAY)
    {
        bComplete = m_Parser.IsFrameComplete();
        if (bComplete)
        {
            if (GetUniteFrame() == false)
            {
                return true;
            }
            if (m_Parser.IsControlFrame())
            {
                assert(m_Parser.IsFinalFrame());
                return true;
            }

            if (m_Parser.IsFinalFrame())
            {
                if (m_nUniteCode == 0)
                {
                    // single unfragmented frame
                    assert(IsUniteCache() == false);
                    assert(m_nUniteSize == 0);
                    return true;
                }

                if (UniteCache() == false) // UniteCache set error status
                {
                    return false;
                }
                m_nUniteCode |= CWSTraits::WS_FINAL;
                m_UniteCache.Parse(m_nUniteCode, m_nUniteData, m_nUniteSize);
                DEV_DEBUG(TF("CWSSession::OnData UniteCache m_nUniteCode = %x, m_nUniteData = %d, m_nUniteSize = %d, stSize = %d"),
                          m_nUniteCode, m_nUniteData, m_nUniteSize, stSize);
                return true;
            }
            else
            {
                bComplete = false;
                return UniteCache(); // UniteCache set error status
            }
        }
        if ( (stSize <= GetMaxBlockSize()) &&
             ((size_t)m_Parser.GetFrameSize() < GetMaxCacheSize()) )
        {
            return true;
        }
        DEV_DEBUG(TF("CWSSession::OnData parser recv len = %d, max-block len = %d, parser cache size = %d, max-cache size = %d"),
                  stSize, GetMaxBlockSize(), m_Parser.GetFrameSize(), GetMaxCacheSize());
        nRet = CWSTraits::TOO_BIG_PROCESS;
    }
    SetStatus((UInt)nRet);
    bComplete = false;
    return false;
}

INLINE bool CWSSession::OnData(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam, size_t& stSize, bool& bComplete)
{
    if (GetStatus() != 0)
    {
        return false;
    }
    m_llOnline = CPlatform::GetRunningTime();
    Int nRet = m_Parser.Parse(NetAttr, pParam, stSize);
    if (nRet == RET_OKAY)
    {
        bComplete = m_Parser.IsFrameComplete();
        if (bComplete)
        {
            if (GetUniteFrame() == false)
            {
                return true;
            }
            if (m_Parser.IsControlFrame())
            {
                assert(m_Parser.IsFinalFrame());
                return true;
            }

            if (m_Parser.IsFinalFrame())
            {
                if (m_nUniteCode == 0)
                {
                    // single unfragmented frame
                    assert(IsUniteCache() == false);
                    assert(m_nUniteSize == 0);
                    return true;
                }

                if (UniteCache(NetAttr, pParam) == false) // UniteCache set error status
                {
                    return false;
                }
                m_nUniteCode |= CWSTraits::WS_FINAL;
                m_UniteCache.Attach(m_nUniteCode, m_nUniteData, m_nUniteSize);
                DEV_DEBUG(TF("CWSSession::OnData2 UniteCache m_nUniteCode = %x, m_nUniteData = %d, m_nUniteSize = %d, stSize = %d"),
                          m_nUniteCode, m_nUniteData, m_nUniteSize, stSize);
                return true;
            }
            else
            {
                bComplete = false;
                return UniteCache(NetAttr, pParam); // UniteCache set error status
            }
        }
        if ( (stSize <= GetMaxBlockSize()) &&
             ((size_t)m_Parser.GetFrameSize() < GetMaxCacheSize()) )
        {
            return true;
        }
        DEV_DEBUG(TF("CWSSession::OnData parser recv len = %d, max-block len = %d, parser cache size = %d, max-cache size = %d"),
                  stSize, GetMaxBlockSize(), m_Parser.GetFrameSize(), GetMaxCacheSize());
        nRet = CWSTraits::TOO_BIG_PROCESS;
    }
    SetStatus((UInt)nRet);
    bComplete = false;
    return false;
}

INLINE bool CWSSession::OnDataHandled(void)
{
    if (IsCacheComplete() == false)
    {
        m_Parser.Close();
    }
    else
    {
        DEV_DEBUG(TF("CWSSession::OnDataHandled UniteCache m_nUniteCode = %x, m_nUniteData = %d, m_nUniteSize = %d"),
                  m_nUniteCode, m_nUniteData, m_nUniteSize);
        m_nUniteCode = 0;
        m_nUniteData = 0;
        m_nUniteSize = 0;
        m_UniteCache.Close();
    }
    return ApplyHandle(false);
}

INLINE bool CWSSession::CheckDataCompleted(void)
{
    bool bRet = false;
    if (IsCacheComplete() == false)
    {
        bRet = m_Parser.IsFrameComplete();
    }
    else
    {
        bRet = m_UniteCache.IsFrameComplete();
    }
    if (bRet)
    {
        return ApplyHandle();
    }
    return false;
}

INLINE size_t CWSSession::Length(void) const
{
    if (IsCacheComplete() == false)
    {
        return m_Parser.Length();
    }
    else
    {
        return m_UniteCache.Length();
    }
}

INLINE void CWSSession::Serialize(CStream& Stream)
{
    if (IsCacheComplete() == false)
    {
        m_Parser.Serialize(Stream);
    }
    else
    {
        m_UniteCache.Serialize(Stream);
    }
}

INLINE CEventHandlerPtr& CWSSession::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CWSSession::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE CWSParser& CWSSession::GetParser(void)
{
    if (IsCacheComplete() == false)
    {
        return m_Parser;
    }
    else
    {
        return m_UniteCache;
    }
}

INLINE const CWSParser& CWSSession::GetParser(void) const
{
    if (IsCacheComplete() == false)
    {
        return m_Parser;
    }
    else
    {
        return m_UniteCache;
    }
}

INLINE bool CWSSession::IsLiveCheck(bool bUpdate)
{
    if (GetAutoCheck())
    {
        if (CPlatform::GetRunningTime() >= (m_llOnline + m_uLiveCheck))
        {
            if (bUpdate)
            {
                m_llOnline = CPlatform::GetRunningTime();
            }
            return true;
        }
    }
    return false;
}

INLINE void CWSSession::UpdateLive(void)
{
    m_llOnline = CPlatform::GetRunningTime();
}

INLINE UInt CWSSession::GetLiveCheck(void) const
{
    return m_uLiveCheck;
}

INLINE void CWSSession::SetLiveCheck(UInt uCheck)
{
    m_uLiveCheck = uCheck;
}

INLINE bool CWSSession::GetAutoCheck(void) const
{
    return ((m_uCheckFlag & WS_CHECK_AUTO) != 0);
}

INLINE void CWSSession::SetAutoCheck(bool bEnable)
{
    if (bEnable)
    {
        m_uCheckFlag |= (WS_CHECK_AUTO);
    }
    else
    {
        m_uCheckFlag &= (~WS_CHECK_AUTO);
    }
}

INLINE bool CWSSession::GetControlCheck(void) const
{
    return ((m_uCheckFlag & (WS_CHECK_CLOSE|WS_CHECK_PING|WS_CHECK_PONG)) != 0);
}

INLINE bool CWSSession::GetCloseCheck(void) const
{
    return ((m_uCheckFlag & WS_CHECK_CLOSE) != 0);
}

INLINE void CWSSession::SetCloseCheck(bool bEnable)
{
    if (bEnable)
    {
        m_uCheckFlag |= (WS_CHECK_CLOSE);
    }
    else
    {
        m_uCheckFlag &= (~WS_CHECK_CLOSE);
    }
}

INLINE bool CWSSession::GetPingCheck(void) const
{
    return ((m_uCheckFlag & WS_CHECK_PING) != 0);
}

INLINE void CWSSession::SetPingCheck(bool bEnable)
{
    if (bEnable)
    {
        m_uCheckFlag |= (WS_CHECK_PING);
    }
    else
    {
        m_uCheckFlag &= (~WS_CHECK_PING);
    }
}

INLINE bool CWSSession::GetPongCheck(void) const
{
    return ((m_uCheckFlag & WS_CHECK_PONG) != 0);
}

INLINE void CWSSession::SetPongCheck(bool bEnable)
{
    if (bEnable)
    {
        m_uCheckFlag |= (WS_CHECK_PONG);
    }
    else
    {
        m_uCheckFlag &= (~WS_CHECK_PONG);
    }
}

INLINE bool CWSSession::GetUniteFrame(void) const
{
    return ((m_uCheckFlag & WS_CHECK_UNITE) != 0);
}

INLINE void CWSSession::SetUniteFrame(bool bEnable)
{
    if (bEnable)
    {
        m_uCheckFlag |= (WS_CHECK_UNITE);
    }
    else if (GetUniteFrame())
    {
        m_uCheckFlag &= (~WS_CHECK_UNITE);
        m_nUniteCode = 0;
        m_nUniteData = 0;
        m_nUniteSize = 0;
        m_UniteCache.Close();
    }
}

INLINE bool CWSSession::IsUniteCache(void) const
{
    return (m_UniteCache.GetCache().GetBuf() != nullptr);
}

INLINE bool CWSSession::IsCacheComplete(void) const
{
    return CWSTraits::IsFinalFrame(m_nUniteCode);
}

INLINE bool CWSSession::UniteCache(void)
{
    assert(m_Parser.IsNetBufferCache() == false);
    assert(m_UniteCache.IsNetBufferCache() == false);
    if (m_nUniteCode == 0)
    {
        m_nUniteCode = (m_Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK);
        if (m_nUniteCode == 0)
        {
            m_nUniteCode = CWSTraits::WS_TEXT;
        }
        DEV_DEBUG(TF("CWSSession::UniteCache Code = %x, UniteSize = %d"), m_nUniteCode, m_nUniteSize);
        assert(m_UniteCache.GetFrameSize() == 0);
        assert(m_UniteCache.GetFrameCode() == 0);
        assert(m_UniteCache.GetPayloadPos() == 0);
        assert(m_UniteCache.GetPayloadSize() == 0);
        assert(m_UniteCache.IsHeadComplete() == false);
        assert(m_UniteCache.IsFrameComplete() == false);
    }
    Int nPayload = m_Parser.GetPayloadSize();
    if (nPayload == 0)
    {
        DEV_DEBUG(TF("CWSSession::UniteCache Code = %x, UniteSize = %d, GetPayloadSize = 0"), m_nUniteCode, m_nUniteSize);
        return true;
    }
    Int nNewSize = m_nUniteSize + nPayload;
    CBufReadStream& brs = m_UniteCache.GetCache();
    if (brs.Size() <= (size_t)nNewSize)
    {
        size_t stNewCacheSize = DEF::Align<size_t>((size_t)nNewSize, LMT_BUF) * 2; // double grow
        CBufReadStream UniteCache(stNewCacheSize);
        if (m_nUniteSize > 0)
        {
            MM_SAFE::Cpy(UniteCache.GetBuf(), stNewCacheSize, brs.GetBuf(), (size_t)m_nUniteSize);
        }
        brs.Attach(UniteCache);
    }
    MM_SAFE::Cpy(brs.GetBuf((size_t)m_nUniteSize), (size_t)nPayload, m_Parser.GetCachePayload(), (size_t)nPayload);
    brs.GetBuf()[nNewSize] = 0;
    DEV_DEBUG(TF("CWSSession::UniteCache old-size = %d, frame-size = %d, new-size = %d"), m_nUniteSize, nPayload, nNewSize);
    m_nUniteSize = nNewSize;
    m_Parser.Close();
    return true;
}

INLINE bool CWSSession::UniteCache(CNETTraits::NET_ATTR& NetAttr, CNETTraits::PNET_PARAM pParam)
{
    assert(m_Parser.IsNetBufferCache());
    if (m_nUniteCode == 0)
    {
        m_nUniteCode = (m_Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK);
        if (m_nUniteCode == 0)
        {
            m_nUniteCode = CWSTraits::WS_TEXT;
        }
        DEV_DEBUG(TF("CWSSession::UniteCache net-buffer Code = %x, UniteData = %d, UniteSize = %d"), m_nUniteCode, m_nUniteData, m_nUniteSize);
        assert(m_UniteCache.GetFrameSize() == 0);
        assert(m_UniteCache.GetFrameCode() == 0);
        assert(m_UniteCache.GetPayloadPos() == 0);
        assert(m_UniteCache.GetPayloadSize() == 0);
        assert(m_UniteCache.IsHeadComplete() == false);
        assert(m_UniteCache.IsFrameComplete() == false);
    }
    bool bRebind = false;
    NET_BUFFER& NetBuffer = m_UniteCache.NetBuffer();
    if (m_UniteCache.IsNetBufferCache() == false)
    {
        m_Parser.Detach(NetBuffer);

        m_nUniteData = m_Parser.GetPayloadPos();
        m_nUniteSize = m_Parser.GetPayloadSize();
        bRebind = true;
        DEV_DEBUG(TF("CWSSession::UniteCache attach net-buffer Code = %x, UniteData = %d, UniteSize = %d"), m_nUniteCode, m_nUniteData, m_nUniteSize);
    }
    else 
    {
        PByte pPayload = m_Parser.GetCachePayload();
        Int   nPayload = m_Parser.GetPayloadSize();
        if (NetBuffer.Append(NetAttr, pPayload, nPayload, (m_nUniteData + m_nUniteSize), bRebind) == false)
        {
            SetStatus(CWSTraits::DATA_NOT_CONSISTENT);
            return false;
        }
        m_nUniteSize += nPayload;
        DEV_DEBUG(TF("CWSSession::UniteCache append net-buffer Code = %x, UniteData = %d, UniteSize = %d"), m_nUniteCode, m_nUniteData, m_nUniteSize);
    }
    if (bRebind)
    {
        CBufReadStream& brs = m_UniteCache.GetCache();
        brs.Attach((size_t)NetBuffer.nSize, NetBuffer.Data());
        brs.GetBuf()[m_nUniteData + m_nUniteSize] = 0;
    }
    m_Parser.Close();
    return true;
}

///////////////////////////////////////////////////////////////////
// CWEBServer
INLINE CWEBServer::CWEBServer(void)
: m_stMaxBlock(CWEBSession::WEB_DEF_SIZE_BLOCK)
, m_stMaxCache(CWEBSession::WEB_DEF_SIZE_CACHE)
, m_bManage(false)
, m_bNetBuffer(false)
, m_sSocket(0)
{
}

INLINE CWEBServer::~CWEBServer(void)
{
    Exit();
}

INLINE bool CWEBServer::Init(CNetworkPtr& NetworkPtr, CEventHandler& EventHandlerRef, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        m_bManage = bManage;
        m_EventHandlerPtr = &EventHandlerRef;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_EventHandlerPtr != nullptr);
    }
    return false;
}

INLINE void CWEBServer::Exit(void)
{
    CSyncLockScope scope(m_SessionLock);
    m_NetAddr.Reset();
    if (m_NetworkPtr != nullptr)
    {
        if (m_sSocket != 0)
        {
            m_NetworkPtr->Destroy(m_sSocket);
            m_sSocket = 0;
        }
        if (m_bManage) 
        {
            m_NetworkPtr->Exit();
        }
        else
        {
            for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
            {
                PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
                m_NetworkPtr->Destroy(pPair->m_K);
            }
        }
        m_NetworkPtr = nullptr;
        m_Session.RemoveAll();
    }
    if (m_EventHandlerPtr != nullptr)
    {
        m_EventHandlerPtr = nullptr;
    }
    m_bNetBuffer = false;
}

INLINE bool CWEBServer::Check(void)
{
    if ((m_sSocket == 0) && (m_NetworkPtr != nullptr))
    {
        return ListenHandle();
    }
    return true;
}

INLINE bool CWEBServer::Listen(PCXStr pszIp, UShort usPort)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        m_NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszIp, usPort, m_NetAddr);
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBServer::Listen(STR_ADDR& strAddr)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        m_NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(*strAddr.strIp, strAddr.usPort, m_NetAddr);
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBServer::Listen(NET_ADDR& NetAddr)
{
    if ((m_NetworkPtr != nullptr) && (m_sSocket == 0))
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddr.usAttr & ATTR_IPV6));
        m_NetAddr = NetAddr;
        return ListenHandle();
    }
    return false;
}

INLINE bool CWEBServer::Send(Socket sSocket, PCStr pszData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, (PByte)pszData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBServer::Send(Socket sSocket, const CCString& strData)
{
    assert(strData.Length() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(strData.Length(), (PByte)strData.GetBuffer());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBServer::Send(Socket sSocket, PByte pData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, pData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBServer::Send(Socket sSocket, const CBufStream& Stream)
{
    assert(Stream.Size() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(Stream.Size(), Stream.GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBServer::Send(Socket sSocket, CStreamScopePtr& StreamPtr)
{
    assert(StreamPtr != nullptr);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        bRet = m_NetworkPtr->Send(sSocket, StreamPtr);
    }
    return bRet;
}

INLINE bool CWEBServer::Destory(Socket sSocket)
{
    if (m_NetworkPtr != nullptr)
    {
        return m_NetworkPtr->Destroy(sSocket);
    }
    return false;
}

INLINE void CWEBServer::SetHash(Int nHashSize)
{
    m_Session.SetHash(nHashSize);
}

INLINE bool CWEBServer::IsNetBufferMode(void) const
{
    return m_bNetBuffer;
}

INLINE void CWEBServer::SetNetBufferMode(void)
{
    m_bNetBuffer = true;
}

INLINE size_t CWEBServer::GetMaxBlockSize(void) const
{
    return m_stMaxBlock;
}

INLINE void CWEBServer::SetMaxBlockSize(size_t stMaxBlock)
{
    m_stMaxBlock = stMaxBlock;
}

INLINE size_t CWEBServer::GetMaxCacheSize(void) const
{
    return m_stMaxCache;
}

INLINE void CWEBServer::SetMaxCacheSize(size_t stMaxCache)
{
    m_stMaxCache = stMaxCache;
}

INLINE CNetworkPtr& CWEBServer::GetNetwork(void)
{
    return m_NetworkPtr;
}

INLINE const CNetworkPtr& CWEBServer::GetNetwork(void) const
{
    return m_NetworkPtr;
}

INLINE CEventHandlerPtr& CWEBServer::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CWEBServer::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE bool CWEBServer::OnTcpAccept(Socket sAccept, Socket sListen)
{
    if (m_EventHandlerPtr->OnHandle(EVENT_TCP_ACCEPT, sAccept, sListen) == RET_OKAY)
    {
        return CreateSession(sAccept);
    }
    return false;
}

INLINE bool CWEBServer::OnTcpRecv(size_t stSize, PTCP_PARAM pTcp)
{
    bool bComplete = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(pTcp->sSocket, SessionPtr))
    {
        if (IsNetBufferMode() == false)
        {
            bComplete = OnData(SessionPtr, stSize, pTcp);
        }
        else
        {
            bComplete = OnDataNetBuffer(SessionPtr, stSize, pTcp);
        }
        if (bComplete == false)
        {
            ErrorHandle(SessionPtr);
        }
    }
    return bComplete;
}

INLINE bool CWEBServer::OnTcpClose(Socket sSocket, ULLong ullLiveData)
{
    if (sSocket != m_sSocket)
    {
        CWEBSessionPtr SessionPtr;
        if (RemoveSession(sSocket, SessionPtr))
        {
            CloseHandle(SessionPtr);
        }
        m_EventHandlerPtr->OnHandle(EVENT_TCP_CLOSE, sSocket, ullLiveData);
    }
    else
    {
        m_sSocket = 0;
    }
    return true;
}

INLINE bool CWEBServer::OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    PByte  pData     = pTcp->pData;
    while (SessionPtr->OnData(pData, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBServer::OnData stData = %d >= stSize = %d"), stData, stSize);
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBServer::OnData stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBServer::OnData stData = %d, stSize = %d"), stData, stSize);
        pData  += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBServer::OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    while (SessionPtr->OnData(m_NetAttr, pTcp, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBServer::OnDataNetBuffer RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBServer::OnDataNetBuffer stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBServer::OnDataNetBuffer stData = %d, stSize = %d"), stData, stSize);
        pTcp->pData += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBServer::CreateSession(Socket sSocket)
{
    CHTTPSessionPtr SessionPtr = MNEW CHTTPSession(sSocket);
    if (SessionPtr != nullptr)
    {
        SessionPtr->SetMaxBlockSize(m_stMaxBlock);
        SessionPtr->SetMaxCacheSize(m_stMaxCache);
        if (m_bNetBuffer)
        {
            SessionPtr->GetParser().SetNetBufferClone();
        }
        CSyncLockScope scope(m_SessionLock);
        return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
    }
    return false;
}

INLINE bool CWEBServer::DestroySession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return m_Session.Remove(sSocket);
}

INLINE bool CWEBServer::CheckSession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return (m_Session.FindIndex(sSocket) != nullptr);
}

INLINE bool CWEBServer::GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    CSyncLockScope scope(m_SessionLock);
    PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
    if (pPair != nullptr)
    {
        SessionPtr = pPair->m_V;
        return true;
    }
    return false;
}

INLINE bool CWEBServer::RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    if (m_NetAddr.IsValid())
    {
        CSyncLockScope scope(m_SessionLock);
        PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
        if (pPair != nullptr)
        {
            SessionPtr = pPair->m_V;
            return m_Session.RemoveAt(reinterpret_cast<PINDEX>(pPair));
        }
    }
    return false;
}

INLINE bool CWEBServer::ListenHandle(void)
{
    if (m_NetAddr.IsValid())
    {
        m_sSocket = m_NetworkPtr->Create(*this, m_NetAddr);
        if (m_sSocket != 0)
        {
            m_NetworkPtr->SetAttr(m_sSocket, TRUE, SOCKET_RECV_EVENT);
            m_NetworkPtr->SetAttr(m_sSocket, TRUE, SOCKET_EXCLUDE_HEAD);
            return m_NetworkPtr->Listen(m_sSocket);
        }
    }
    return false;
}

INLINE bool CWEBServer::SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs)
{
    size_t stSize = 0;
    bool   bRet   = true;
    while (brs.IsEnd() == false)
    {
        CStreamScopePtr StreamPtr;
        bRet = m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            stSize = DEF::Min<size_t>(StreamPtr->Size(), brs.Rest());
            bRet = (StreamPtr->Write(brs.GetBufPos(), stSize) == stSize);
        }
        if (bRet)
        {
            brs.Seek((SeekPos)stSize, SEEKO_CURRENT);
            bRet = m_NetworkPtr->Send(SessionPtr->GetSocket(), StreamPtr);
        }
        if (bRet == false)
        {
            break;
        }
    }
    return bRet;
}

INLINE bool CWEBServer::RecvHandle(CWEBSessionPtr& SessionPtr)
{
    bool bRet = UpgradeHandle(SessionPtr);
    if (bRet == false)
    {
        return false;
    }
    if (SessionPtr->CheckDataCompleted())
    {
        bRet = CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return bRet;
}

INLINE bool CWEBServer::CloseHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->CheckDataCompleted())
    {
        SessionPtr->SetStatus((UInt)RET_ERROR); // closed
        CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBServer::ErrorHandle(CWEBSessionPtr& SessionPtr)
{
    if ((SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS) && 
        (SessionPtr->GetStatus() != 0) &&
        (SessionPtr->ApplyHandle() != false))
    {
        CStreamScopePtr StreamPtr;
        m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            CPCStr pszStatus = CWSTraits::StatusToString((Int)SessionPtr->GetStatus());
            size_t stSize    = CChar::Length(pszStatus);
            // code = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE) | (UShort)(stSize + sizeof(UShort))
            // head = CWSTraits::WS_NORMAL_LEN
            UShort usFrame = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE)|(UShort)(stSize + sizeof(UShort));
            CWSParser::SetFrameHead(StreamPtr->GetBuf(), usFrame, 0, CWSTraits::WS_NORMAL_LEN, (stSize + sizeof(UShort)));
            StreamPtr->Seek(CWSTraits::WS_NORMAL_LEN);
#ifndef __ARCH_TARGET_BIGENDIAN__
            (*StreamPtr) << CPlatform::ByteSwap((UShort)SessionPtr->GetStatus());
#else
            (*StreamPtr) << (UShort)SessionPtr->GetStatus();
#endif
            StreamPtr->Write((PByte)pszStatus, stSize);
            m_NetworkPtr->Send(SessionPtr->GetSocket(), StreamPtr);
        }
        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBServer::CompleteHandle(CWEBSessionPtr& SessionPtr)
{
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = SessionPtr->GetSocket();
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP)
    {
        CHTTPSession* pSession = static_cast<CHTTPSession*>(SessionPtr.Get());
        Param.nSize = pSession->GetParser().GetCacheSize();
        Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
        return (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS);
        CWSSession* pSession = static_cast<CWSSession*>(SessionPtr.Get());
        Int nRet = RET_OKAY;
        if (pSession->GetControlCheck())
        {
            nRet = CompleteWSCheck(pSession, SessionPtr);
        }
        if (nRet == RET_OKAY)
        {
            Param.nSize = pSession->GetParser().GetFrameSize();
            Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
            return (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
        }
        return (nRet != CWSTraits::WS_CLOSE_FRAME);
    }
}

INLINE Int CWEBServer::CompleteWSCheck(CWSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSParser& Parser = pSession->GetParser();
    if (Parser.IsControlFrame())
    {
        DEV_DEBUG(TF("CWEBServer::CompleteWSCheck opcode = %x"), (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK));
        if (SessionPtr->GetStatus() == (UInt)RET_ERROR)
        {
            return CWSTraits::WS_CLOSE_FRAME;
        }
        Int nFrame = 0;
        switch (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK)
        {
        case CWSTraits::WS_CLOSE:
            {
                if (pSession->GetCloseCheck())
                {
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE;
                }
            }
            break;
        case CWSTraits::WS_PING:
            {
                if (pSession->GetPingCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
            }
            break;
        case CWSTraits::WS_PONG:
            {
                if (pSession->GetPongCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
            }
            break;
        default:{}
        }
        DEV_DEBUG(TF("CWEBServer::CompleteWSCheck ret opcode = %x"), nFrame);
        if (nFrame != 0)
        {
            CStreamScopePtr StreamPtr;
            m_NetworkPtr->AllocBuffer(StreamPtr);
            if (StreamPtr != nullptr)
            {
                Int nPayload = Parser.GetPayloadSize();
                if (nPayload > CWSTraits::WS_PAY_LOAD)
                {
                    nPayload = CWSTraits::WS_PAY_LOAD;
                }
                // code = nFrame | nPayload;
                // head = CWSTraits::WS_NORMAL_LEN
                CWSParser::SetFrameHead(StreamPtr->GetBuf(), (UShort)(nFrame|nPayload), 0, CWSTraits::WS_NORMAL_LEN, (size_t)nPayload);
                StreamPtr->Seek(CWSTraits::WS_NORMAL_LEN);
                if (nPayload > 0)
                {
                    StreamPtr->Write(Parser.GetCachePayload(), (size_t)nPayload);
                }
                m_NetworkPtr->Send(SessionPtr->GetSocket(), StreamPtr);
                DEV_DEBUG(TF("CWEBServer::CompleteWSCheck ret stream size = %d"), (CWSTraits::WS_NORMAL_LEN + nPayload));
                return nFrame;
            }
        }
    }
    return RET_OKAY;
}

INLINE bool CWEBServer::UpgradeHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->ApplyHandle() == false)
    {
        return false;
    }
    bool bRet = true;
    if (SessionPtr->CheckUpgrade() == false)
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP);
        CHTTPSession* pSession = static_cast<CHTTPSession*>(SessionPtr.Get());
        CHTTPRequest Request;
        pSession->GetParser().To(Request, true);

        //SetStatus(SWITCHING_PROTOCOLS) after set http-parser->from(custom CHTTPResponse)
        if (CWSParser::CheckUpgrade(Request, bRet))
        {
            bRet = UpgradeWSSession(Request, pSession, SessionPtr);
        }
        // else if (CHTTP2Parser::CheckUpgrade(Request))
        // {
        //     bRet = UpgradeH2CSession(Request, pSession, SessionPtr);
        // }
        if (bRet)
        {
            SessionPtr->SetUpgraded();
        }
    }
    SessionPtr->ApplyHandle(false);
    return bRet;
}

INLINE bool CWEBServer::UpgradeWSSession(const CHTTPRequest& Request, CHTTPSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSSessionPtr WSSessionPtr;

    bool bRet = (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_BEFORE_UPGRADE, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    if (bRet)
    {   // http session --> ws-session
        bRet = false;
        WSSessionPtr = MNEW CWSSession(pSession->GetSocket(), pSession->GetEventHandler());
        if (WSSessionPtr != nullptr)
        {
            WSSessionPtr->SetMaxBlockSize(m_stMaxBlock);
            WSSessionPtr->SetMaxCacheSize(m_stMaxCache);
            WSSessionPtr->SetPingCheck();
            if (m_bNetBuffer)
            {
                WSSessionPtr->GetParser().SetNetBufferClone();
            }

            CSyncLockScope scope(m_SessionLock);
            PAIR_WEBSESSION* pPair = m_Session.Find(pSession->GetSocket());
            if (pPair != nullptr)
            {
                pPair->m_V = WSSessionPtr;
                bRet = true;
            }
        }
    }
    if (bRet)
    {   // response
        if (pSession->GetStatus() != CHTTPTraits::SWITCHING_PROTOCOLS)
        {
            CHTTPResponse Response(CHTTPTraits::HTTP_CONTENT_NONE);
            CWSParser::UpgradeResponse(Request, Response);
            bRet = pSession->GetParser().From(Response);
        }
        CBufReadStream brs;
        if (bRet)
        {
            brs.Attach(pSession->GetParser().GetCacheSize(), pSession->GetParser().GetCacheBuf());
            bRet = SendHandle(SessionPtr, brs);
        }
        SessionPtr = WSSessionPtr;
        bRet = (m_EventHandlerPtr->OnHandle(CNETTraits::EVENT_AFTER_UPGRADE, reinterpret_cast<uintptr_t>(SessionPtr.Get()), (ULLong)SessionPtr->GetType()) == RET_OKAY);
    }
    return bRet;
}

///////////////////////////////////////////////////////////////////
// CWEBClient
INLINE CWEBClient::CWEBClient(void)
: m_stMaxBlock(CWEBSession::WEB_DEF_SIZE_BLOCK)
, m_stMaxCache(CWEBSession::WEB_DEF_SIZE_CACHE)
, m_bManage(false)
, m_bExited(false)
, m_bNetBuffer(false)
, m_bAutoCheck(false)
, m_uControlCode(CWSTraits::WS_PING)
{
}

INLINE CWEBClient::~CWEBClient(void)
{
    Exit();
}

INLINE bool CWEBClient::Init(CNetworkPtr& NetworkPtr, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        m_bManage = bManage;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_NetworkPtr != nullptr);
    }
    return false;
}

INLINE bool CWEBClient::Init(CNetworkPtr& NetworkPtr, CEventHandler& EventHandlerRef, bool bManage)
{
    if ((m_NetworkPtr == nullptr) && (m_EventHandlerPtr == nullptr))
    {
        m_bManage = bManage;
        m_EventHandlerPtr = &EventHandlerRef;
        m_NetworkPtr = NetworkPtr;
        NetworkPtr->Attr(m_NetAttr);
        return (m_EventHandlerPtr != nullptr);
    }
    return false;
}

INLINE void CWEBClient::Exit(void)
{
    CSyncLockScope scope(m_SessionLock);
    m_bExited = true;
    if (m_NetworkPtr != nullptr)
    {
        if (m_bManage) 
        {
            m_NetworkPtr->Exit();
        }
        else
        {
            for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
            {
                PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
                m_NetworkPtr->Destroy(pPair->m_K);
            }
        }
        m_NetworkPtr = nullptr;
        m_Session.RemoveAll();
    }
    if (m_EventHandlerPtr != nullptr)
    {
        m_EventHandlerPtr = nullptr;
    }
    m_bNetBuffer   = false;
    m_bAutoCheck   = false;
    m_uControlCode = CWSTraits::WS_PING;
}

INLINE bool CWEBClient::Check(void)
{
    if (m_bAutoCheck == false)
    {
        return true;
    }
    ARY_WEBSESSION LiveChecks;
    if (m_Session.GetSize() > 0)
    {
        CSyncLockScope scope(m_SessionLock);
        for (PINDEX index = m_Session.GetFirstIndex(); index != nullptr;)
        {
            PAIR_WEBSESSION* pPair = m_Session.GetNext(index);
            if (pPair->m_V->GetType() == CWEBSession::WEB_SESSION_WS)
            {
                CWSSession* pSession = static_cast<CWSSession*>(pPair->m_V.Get());
                if (pSession->IsLiveCheck())
                {
                    LiveChecks.Add(pPair->m_V);
                }
            }
        }
    }
    if (LiveChecks.GetSize() > 0)
    {
        CStreamScopePtr StreamPtr;
        m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            StreamPtr->Seek(CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN);
            PUShort pusFrame = (PUShort)StreamPtr->GetBuf();
#ifndef __ARCH_TARGET_BIGENDIAN__
            pusFrame[0] = CPlatform::ByteSwap((UShort)(m_uControlCode|CWSTraits::WS_MASK));
#else
            pusFrame[0] = (UShort)(m_uControlCode|CWSTraits::WS_MASK);
#endif
            *((PUInt)(pusFrame + 1)) = (UInt)CPlatform::GetRunningTime();

            for (int i = 0; i < LiveChecks.GetSize(); ++i)
            {
                m_NetworkPtr->SendCopy(LiveChecks[i]->GetSocket(), StreamPtr);
            }
        }
    }
    return true;
}

INLINE bool CWEBClient::Create(Socket& sSocket, UShort usLocalPort, PCXStr pszLocalAddr)
{
    if (m_NetworkPtr != nullptr)
    {
        NET_ADDR NetAddr;
        NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszLocalAddr, usLocalPort, NetAddr);
        return CreateSession(sSocket, NetAddr);
    }
    return false;
}

INLINE bool CWEBClient::Create(Socket& sSocket, CEventHandler& EventHandlerRef, UShort usLocalPort, PCXStr pszLocalAddr)
{
    if (m_NetworkPtr != nullptr)
    {
        NET_ADDR NetAddr;
        NetAddr.usAttr = (UShort)((m_NetAttr.nAttrs & ATTR_IPV6) ? ATTR_IPV6 : ATTR_IPV4);
        m_NetworkPtr->TranslateAddr(pszLocalAddr, usLocalPort, NetAddr);
        return CreateSession(sSocket, NetAddr, EventHandlerRef);
    }
    return false;
}

INLINE bool CWEBClient::Create(Socket& sSocket, NET_ADDR& NetAddrRef)
{
    if ((m_NetworkPtr != nullptr) && (m_EventHandlerPtr != nullptr))
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddrRef.usAttr & ATTR_IPV6));
        return CreateSession(sSocket, NetAddrRef);
    }
    return false;
}

INLINE bool CWEBClient::Create(Socket& sSocket, CEventHandler& EventHandlerRef, NET_ADDR& NetAddrRef)
{
    if (m_NetworkPtr != nullptr)
    {
        assert((m_NetAttr.nAttrs & ATTR_IPV6) == (NetAddrRef.usAttr & ATTR_IPV6));
        return CreateSession(sSocket, NetAddrRef, EventHandlerRef);
    }
    return false;
}

INLINE bool CWEBClient::Destory(Socket sSocket)
{
    if (m_NetworkPtr != nullptr)
    {
        return m_NetworkPtr->Destroy(sSocket);
    }
    return false;
}

INLINE bool CWEBClient::Connect(Socket sSocket, PCXStr pszIp, UShort usPort)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, usPort, pszIp);
    }
    return false;
}

INLINE bool CWEBClient::Connect(Socket sSocket, STR_ADDR& strAddr)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, strAddr.usPort, *strAddr.strIp);
    }
    return false;
}

INLINE bool CWEBClient::Connect(Socket sSocket, NET_ADDR& NetAddr)
{
    if (CheckSession(sSocket))
    {
        return m_NetworkPtr->Connect(sSocket, NetAddr);
    }
    return false;
}

INLINE bool CWEBClient::Send(Socket sSocket, PCStr pszData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, (PByte)pszData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBClient::Send(Socket sSocket, const CCString& strData)
{
    assert(strData.Length() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(strData.Length(), (PByte)strData.GetBuffer());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBClient::Send(Socket sSocket, PByte pData, size_t stSize)
{
    assert(stSize > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(stSize, pData);
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBClient::Send(Socket sSocket, const CBufStream& Stream)
{
    assert(Stream.Size() > 0);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        CBufReadStream brs(Stream.Size(), Stream.GetBuf());
        bRet = SendHandle(SessionPtr, brs);
    }
    return bRet;
}

INLINE bool CWEBClient::Send(Socket sSocket, CStreamScopePtr& StreamPtr)
{
    assert(StreamPtr != nullptr);
    bool bRet = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sSocket, SessionPtr))
    {
        assert(m_NetworkPtr != nullptr);
        bRet = m_NetworkPtr->Send(sSocket, StreamPtr);
    }
    return bRet;
}

INLINE void CWEBClient::SetHash(Int nHashSize)
{
    m_Session.SetHash(nHashSize);
}

INLINE bool CWEBClient::IsNetBufferMode(void) const
{
    return m_bNetBuffer;
}

INLINE void CWEBClient::SetNetBufferMode(void)
{
    m_bNetBuffer = true;
}

INLINE bool CWEBClient::IsAutoCheckMode(void) const
{
    return m_bAutoCheck;
}

INLINE void CWEBClient::SetAutoCheckMode(void)
{
    m_bAutoCheck = true;
}

INLINE UInt CWEBClient::GetControlCode(void) const
{
    return m_uControlCode;
}

INLINE void CWEBClient::SetControlCode(bool bPing)
{
    if (bPing)
    {
        m_uControlCode = CWSTraits::WS_PING;
    }
    else
    {
        m_uControlCode = CWSTraits::WS_PONG;
    }
}

INLINE size_t CWEBClient::GetMaxBlockSize(void) const
{
    return m_stMaxBlock;
}

INLINE void CWEBClient::SetMaxBlockSize(size_t stMaxBlock)
{
    m_stMaxBlock = stMaxBlock;
}

INLINE size_t CWEBClient::GetMaxCacheSize(void) const
{
    return m_stMaxCache;
}

INLINE void CWEBClient::SetMaxCacheSize(size_t stMaxCache)
{
    m_stMaxCache = stMaxCache;
}

INLINE CNetworkPtr& CWEBClient::GetNetwork(void)
{
    return m_NetworkPtr;
}

INLINE const CNetworkPtr& CWEBClient::GetNetwork(void) const
{
    return m_NetworkPtr;
}

INLINE CEventHandlerPtr& CWEBClient::GetEventHandler(void)
{
    return m_EventHandlerPtr;
}

INLINE const CEventHandlerPtr& CWEBClient::GetEventHandler(void) const
{
    return m_EventHandlerPtr;
}

INLINE bool CWEBClient::OnTcpConnect(UInt uError, Socket sConnect)
{
    CEventHandlerPtr EventHandlerPtr;
    CWEBSessionPtr SessionPtr;
    if (GetSession(sConnect, SessionPtr) && GetHandler(SessionPtr, EventHandlerPtr))
    {
        assert(m_NetworkPtr != nullptr);
        return (EventHandlerPtr->OnHandle(EVENT_TCP_CONNECT, uError, sConnect) == RET_OKAY);
    }
    return false;
}

INLINE bool CWEBClient::OnTcpRecv(size_t stSize, PTCP_PARAM pTcp)
{
    bool bComplete = false;
    CWEBSessionPtr SessionPtr;
    if (GetSession(pTcp->sSocket, SessionPtr))
    {
        if (IsNetBufferMode() == false)
        {
            bComplete = OnData(SessionPtr, stSize, pTcp);
        }
        else
        {
            bComplete = OnDataNetBuffer(SessionPtr, stSize, pTcp);
        }
        if (bComplete == false)
        {
            ErrorHandle(SessionPtr);
        }
    }
    return bComplete;
}

INLINE bool CWEBClient::OnData(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    PByte  pData     = pTcp->pData;
    while (SessionPtr->OnData(pData, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBClient::OnData RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBClient::OnData stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBClient::OnData stData = %d, stSize = %d"), stData, stSize);
        pData  += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBClient::OnDataNetBuffer(CWEBSessionPtr& SessionPtr, size_t stSize, PTCP_PARAM pTcp)
{
    bool   bComplete = false;
    size_t stData    = stSize;
    while (SessionPtr->OnData(m_NetAttr, pTcp, stData, bComplete))
    {
        // data pack complete?
        if ((bComplete == false) && (stData == stSize))
        {
            bComplete = true;
            break;
        }
        if (bComplete)
        {
            // handle okay?
            bComplete = RecvHandle(SessionPtr);
            if (bComplete == false)
            {
                DEV_DEBUG(TF("CWEBClient::OnDataNetBuffer RecvHandle fail"));
                break;
            }
        }
        // recv empty?
        if (stData >= stSize)
        {
            DEV_DEBUG(TF("CWEBClient::OnDataNetBuffer stData = %d >= stSize = %d"), stData, stSize);
            break;
        }
        DEV_DEBUG(TF("CWEBClient::OnDataNetBuffer stData = %d, stSize = %d"), stData, stSize);
        pTcp->pData += stData;
        stSize -= stData;
        stData  = stSize;
    }
    return bComplete;
}

INLINE bool CWEBClient::OnTcpClose(Socket sSocket, ULLong ullLiveData)
{
    CWEBSessionPtr SessionPtr;
    if (RemoveSession(sSocket, SessionPtr))
    {
        CloseHandle(SessionPtr);
    }
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr))
    {
        EventHandlerPtr->OnHandle(EVENT_TCP_CLOSE, sSocket, ullLiveData);
    }
    return true;
}

INLINE bool CWEBClient::CreateSession(Socket& sSocket, NET_ADDR& NetAddr)
{
    sSocket = m_NetworkPtr->Create(*this, NetAddr);
    if (sSocket != 0)
    {
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_RECV_EVENT);
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_EXCLUDE_HEAD);

        CHTTPSessionPtr SessionPtr = MNEW CHTTPSession(sSocket);
        if (SessionPtr != nullptr)
        {
            SessionPtr->SetMaxBlockSize(m_stMaxBlock);
            SessionPtr->SetMaxCacheSize(m_stMaxCache);
            if (m_bNetBuffer)
            {
                SessionPtr->GetParser().SetNetBufferClone();
            }
            CSyncLockScope scope(m_SessionLock);
            return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
        }
        m_NetworkPtr->Destroy(sSocket);
        sSocket = 0;
    }
    return false;
}

INLINE bool CWEBClient::CreateSession(Socket& sSocket, NET_ADDR& NetAddr, CEventHandler& EventHandlerRef)
{
    sSocket = m_NetworkPtr->Create(*this, NetAddr);
    if (sSocket != 0)
    {
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_RECV_EVENT);
        m_NetworkPtr->SetAttr(sSocket, TRUE, SOCKET_EXCLUDE_HEAD);

        CEventHandlerPtr EventHandlerPtr = &EventHandlerRef;
        CHTTPSessionPtr SessionPtr = MNEW CHTTPSession(sSocket, EventHandlerPtr);
        if (SessionPtr != nullptr)
        {
            SessionPtr->SetMaxBlockSize(m_stMaxBlock);
            SessionPtr->SetMaxCacheSize(m_stMaxCache);
            if (m_bNetBuffer)
            {
                SessionPtr->GetParser().SetNetBufferClone();
            }
            CSyncLockScope scope(m_SessionLock);
            return (m_Session.Add(sSocket, SessionPtr.Get()) != nullptr);
        }
        m_NetworkPtr->Destroy(sSocket);
        sSocket = 0;
    }
    return false;
}

INLINE bool CWEBClient::DestroySession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return m_Session.Remove(sSocket);
}

INLINE bool CWEBClient::CheckSession(Socket sSocket)
{
    CSyncLockScope scope(m_SessionLock);
    return (m_Session.FindIndex(sSocket) != nullptr);
}

INLINE bool CWEBClient::GetSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    CSyncLockScope scope(m_SessionLock);
    PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
    if (pPair != nullptr)
    {
        SessionPtr = pPair->m_V;
        return true;
    }
    return false;
}

INLINE bool CWEBClient::RemoveSession(Socket sSocket, CWEBSessionPtr& SessionPtr)
{
    if (m_bExited == false)
    {
        CSyncLockScope scope(m_SessionLock);
        PAIR_WEBSESSION* pPair = m_Session.Find(sSocket);
        if (pPair != nullptr)
        {
            SessionPtr = pPair->m_V;
            return m_Session.RemoveAt(reinterpret_cast<PINDEX>(pPair));
        }
    }
    return false;
}

INLINE bool CWEBClient::GetHandler(CWEBSessionPtr& SessionPtr, CEventHandlerPtr& EventHandlerPtr)
{
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP)
    {
        CHTTPSession* pSession = static_cast<CHTTPSession*>(SessionPtr.Get());
        EventHandlerPtr = pSession->GetEventHandler();
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS);
        CWSSession* pSession = static_cast<CWSSession*>(SessionPtr.Get());
        EventHandlerPtr = pSession->GetEventHandler();
    }
    if (EventHandlerPtr == nullptr)
    {
        EventHandlerPtr = m_EventHandlerPtr;
    }
    return (EventHandlerPtr != nullptr);
}

INLINE bool CWEBClient::SendHandle(CWEBSessionPtr& SessionPtr, CBufReadStream& brs)
{
    size_t stSize = 0;
    bool   bRet   = false;
    while (brs.IsEnd() == false)
    {
        CStreamScopePtr StreamPtr;
        bRet = m_NetworkPtr->AllocBuffer(StreamPtr);
        if (StreamPtr != nullptr)
        {
            stSize = DEF::Min<size_t>(StreamPtr->Size(), brs.Rest());
            bRet = (StreamPtr->Write(brs.GetBufPos(), stSize) == stSize);
        }
        if (bRet)
        {
            brs.Seek((SeekPos)stSize, SEEKO_CURRENT);
            bRet = m_NetworkPtr->Send(SessionPtr->GetSocket(), StreamPtr);
        }
        if (bRet == false)
        {
            break;
        }
    }
    return bRet;
}

INLINE bool CWEBClient::RecvHandle(CWEBSessionPtr& SessionPtr)
{
    bool bRet = UpgradeHandle(SessionPtr);
    if (bRet == false)
    {
        return false;
    }
    if (SessionPtr->CheckDataCompleted())
    {
        bRet = CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return bRet;
}

INLINE bool CWEBClient::CloseHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->CheckDataCompleted())
    {
        SessionPtr->SetStatus((UInt)RET_ERROR); // closed
        CompleteHandle(SessionPtr);
        SessionPtr->OnDataHandled();
    }
    return true;
}

INLINE bool CWEBClient::ErrorHandle(CWEBSessionPtr&)// SessionPtr)
{
//     if ((SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS) && 
//         (SessionPtr->GetStatus() != 0) &&
//         (SessionPtr->ApplyHandle() != false))
//     {
//         CStreamScopePtr StreamPtr;
//         m_NetworkPtr->AllocBuffer(StreamPtr);
//         if (StreamPtr != nullptr)
//         {
//             CPCStr pszStatus = CWSTraits::StatusToString((Int)SessionPtr->GetStatus());
//             size_t stSize    = CChar::Length(pszStatus);
//             // code = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE|CWSTraits::WS_MASK) | (UShort)(stSize + sizeof(UShort))
//             // head = CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN
//             // mask = (UInt)CPlatform::GetRunningTime();
//             StreamPtr->Seek(CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN);
// #ifndef __ARCH_TARGET_BIGENDIAN__
//             (*StreamPtr) << CPlatform::ByteSwap((UShort)SessionPtr->GetStatus());
// #else
//             (*StreamPtr) << (UShort)SessionPtr->GetStatus();
// #endif
//             StreamPtr->Write((PByte)pszStatus, stSize);
//
//             UShort usFrame = (CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE|CWSTraits::WS_MASK)|(UShort)(stSize + sizeof(UShort));
//             CWSParser::SetFrameHead(StreamPtr->GetBuf(), usFrame, (UInt)CPlatform::GetRunningTime(), (CWSTraits::WS_NORMAL_LEN + CWSTraits::WS_MASK_LEN), (stSize + sizeof(UShort)));
//
//             m_NetworkPtr->Send(SessionPtr->GetSocket(), StreamPtr);
//         }
//         SessionPtr->OnDataHandled();
//     }
    return true;
}

INLINE bool CWEBClient::CompleteHandle(CWEBSessionPtr& SessionPtr)
{
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr) == false)
    {
        return false;
    }
    CNETTraits::TCP_PARAM Param;
    Param.sSocket = SessionPtr->GetSocket();
    if (SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP)
    {
        CHTTPSession* pSession = static_cast<CHTTPSession*>(SessionPtr.Get());
        Param.nSize = pSession->GetParser().GetCacheSize();
        Param.pData = (PByte)pSession->GetParser().GetCacheBuf();
        return (EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    }
    else
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_WS);
        CWSSession* pSession = static_cast<CWSSession*>(SessionPtr.Get());
        Int nRet = RET_OKAY;
        if (pSession->GetControlCheck())
        {
            nRet = CompleteWSCheck(pSession, SessionPtr);
        }
        if (nRet == RET_OKAY)
        {
            Param.nSize = pSession->GetParser().GetFrameSize();
            Param.pData  = (PByte)pSession->GetParser().GetCacheBuf();
            return (EventHandlerPtr->OnHandle(CNETTraits::EVENT_WEB_SESSION, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
        }
        return (nRet != CWSTraits::WS_CLOSE_FRAME);
    }
}

INLINE Int CWEBClient::CompleteWSCheck(CWSSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CWSParser& Parser = pSession->GetParser();
    if (Parser.IsControlFrame())
    {
        DEV_DEBUG(TF("CWEBClient::CompleteWSCheck opcode = %x"), (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK));
        if (SessionPtr->GetStatus() == (UInt)RET_ERROR)
        {
            return CWSTraits::WS_CLOSE_FRAME;
        }
        Int nFrame = 0;
        switch (Parser.GetFrameCode() & CWSTraits::WS_OPCODE_MASK)
        {
        case CWSTraits::WS_CLOSE:
            {
                if (pSession->GetCloseCheck())
                {
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_CLOSE;
                }
            }
            break;
        case CWSTraits::WS_PING:
            {
                if (m_uControlCode == CWSTraits::WS_PONG)
                {
                    // return from server
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
                else if (pSession->GetPingCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
            }
            break;
        case CWSTraits::WS_PONG:
            {
                if (m_uControlCode == CWSTraits::WS_PING)
                {
                    // return from server
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PONG;
                }
                else if (pSession->GetPongCheck())
                {
                    // !!! avoid ping-pong infinite loops
                    nFrame = CWSTraits::WS_FINAL|CWSTraits::WS_PING;
                }
            }
            break;
        default:{}
        }
        if (nFrame != 0)
        {
            return nFrame;
        }
    }
    return RET_OKAY;
}

INLINE bool CWEBClient::UpgradeHandle(CWEBSessionPtr& SessionPtr)
{
    if (SessionPtr->ApplyHandle() == false)
    {
        return false;
    }
    bool bRet = true;
    if (SessionPtr->CheckUpgrade() == false)
    {
        assert(SessionPtr->GetType() == CWEBSession::WEB_SESSION_HTTP);
        CHTTPSession* pSession = static_cast<CHTTPSession*>(SessionPtr.Get());
        CHTTPResponse Response;
        pSession->GetParser().To(Response);

        if (CWSParser::CheckUpgrade(Response, bRet))
        {
            bRet = UpgradeWSSession(Response, pSession, SessionPtr);
        }
        // else if (CHTTP2Parser::CheckUpgrade(Response))
        // {
        //     bRet = UpgradeH2CSession(Response, pSession, SessionPtr);
        // }
        if (bRet)
        {
            SessionPtr->SetUpgraded();
        }
    }
    SessionPtr->ApplyHandle(false);
    return bRet;
}

INLINE bool CWEBClient::UpgradeWSSession(const CHTTPResponse& Response, CHTTPSession* pSession, CWEBSessionPtr& SessionPtr)
{
    CEventHandlerPtr EventHandlerPtr;
    if (GetHandler(SessionPtr, EventHandlerPtr) == false)
    {
        return false;
    }
    CWSSessionPtr WSSessionPtr;
    bool bRet = (EventHandlerPtr->OnHandle(CNETTraits::EVENT_BEFORE_UPGRADE, reinterpret_cast<uintptr_t>(pSession), (ULLong)pSession->GetType()) == RET_OKAY);
    if (bRet)
    {   // http session --> ws-session
        bRet = false;
        WSSessionPtr = MNEW CWSSession(pSession->GetSocket(), pSession->GetEventHandler());
        if (WSSessionPtr != nullptr)
        {
            WSSessionPtr->SetMaxBlockSize(m_stMaxBlock);
            WSSessionPtr->SetMaxCacheSize(m_stMaxCache);
            WSSessionPtr->SetPongCheck();
            if (m_bNetBuffer)
            {
                WSSessionPtr->GetParser().SetNetBufferClone();
            }
            if (m_bAutoCheck == false)
            {
                WSSessionPtr->SetAutoCheck(false);
            }

            CSyncLockScope scope(m_SessionLock);
            PAIR_WEBSESSION* pPair = m_Session.Find(pSession->GetSocket());
            if (pPair != nullptr)
            {
                pPair->m_V = WSSessionPtr;
                bRet = true;
            }
        }
    }
    if (bRet)
    {   
        SessionPtr = WSSessionPtr;
        bRet = (EventHandlerPtr->OnHandle(CNETTraits::EVENT_AFTER_UPGRADE, reinterpret_cast<uintptr_t>(SessionPtr.Get()), (ULLong)SessionPtr->GetType()) == RET_OKAY);
    }
    return bRet;
}


#endif // __NETWORK_HTTP_INL__
