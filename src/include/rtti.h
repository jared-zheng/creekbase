// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __RTTI_H__
#define __RTTI_H__

#pragma once

#ifndef __MODERN_CXX_NOT_SUPPORTED
#include <type_traits>
// C++11   201103L
// C++14   201402L
// C++17   201703L
#endif
#include "stream.h"
#include "sequencedef.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
class CObject;
class CRTTI;
class CRTTIType;
class CRTTIEnum;

typedef CTMap<CString, CRTTIType*>           MAP_RTTI_TYPE, *PMAP_RTTI_TYPE;
typedef CTMap<CString, CRTTIType*>::PAIR     PAIR_RTTI_TYPE;
// cobject's rtti member enum
typedef MAP_RTTI_TYPE                        MAP_RTTI_ENUM, *PMAP_RTTI_ENUM;
typedef PAIR_RTTI_TYPE                       PAIR_RTTI_ENUM;
// cobject's rtti member variables
typedef MAP_RTTI_TYPE                        MAP_RTTI_VAR, *PMAP_RTTI_VAR;
typedef PAIR_RTTI_TYPE                       PAIR_RTTI_VAR;
// cobject's rtti member functions
typedef MAP_RTTI_TYPE                        MAP_RTTI_FUNC, *PMAP_RTTI_FUNC;
typedef PAIR_RTTI_TYPE                       PAIR_RTTI_FUNC;

// cobject's default constructor function invoke by rtti
typedef CObject* (CreateObject)(void);
// cobject's reflect type function
typedef bool     (ReflectType)(const CRTTI& rtti);

///////////////////////////////////////////////////////////////////
// CRTTI
class CORECLASS CRTTI : public MObject
{
public:
    enum RTTI_TRAITS
    {
        TRAITS_NONE        = 0x00000000,
        TRAITS_OBJECT      = 0x00000001, // root class CObject
        TRAITS_ABSTRACT    = 0x00000002, // class is abstract
        TRAITS_CREATE      = 0x00000004, // class has default constructor
        TRAITS_TYPE        = 0x00000008, // class has vars&funcs type info
        TRAITS_BUILTIN     = (TRAITS_CREATE|TRAITS_TYPE),
        // special traits add
    };
public:
    static CRTTI*         Find(PCXStr pszName);

    static CreateObject*  FindCreate(PCXStr pszName);

    static CObject*       CreateByName(PCXStr pszName);
public:
    // !!!pszName is global const string
    CRTTI(CPCXStr pszName, const CRTTI* pBase, const uintptr_t utTraits, const CreateObject* pCreate = nullptr, const ReflectType* pReflect = nullptr);
    ~CRTTI(void);

    CPCXStr               GetName(void)   const;
    const  CRTTI*         GetBase(void)   const;
    const  uintptr_t      GetTraits(void) const;
    const  CreateObject*  GetCreate(void) const;

    const  bool           IsExactClass(const CRTTI& rtti) const;
    const  bool           IsKindOf(const CRTTI& rtti) const;
public:
    CObject*              Create(void);

    // 1. current class-type enum var/func's name exist, return current class-type
    // 2. base class-type enum var/func's name exist, return base class-type
    const CRTTIEnum*      GetEnum(PCXStr pszName) const;
    const CRTTIEnum*      GetEnum(const CString& strName) const;

    const CRTTIType*      GetVar(PCXStr pszName) const;
    const CRTTIType*      GetVar(const CString& strName) const;

    const CRTTIType*      GetFunc(PCXStr pszName) const;
    const CRTTIType*      GetFunc(const CString& strName) const;
    // current class-type enum, var/func's size
    Int                   GetEnumSize(void) const;
    Int                   GetVarSize(void) const;
    Int                   GetFuncSize(void) const;
    // current class-type enum var/func's map
    const MAP_RTTI_ENUM&  GetEnumMap(void) const;
    const MAP_RTTI_VAR&   GetVarMap(void) const;
    const MAP_RTTI_FUNC&  GetFuncMap(void) const;
    // current class-type reflect
    bool                  SetEnumMap(MAP_RTTI_ENUM& EnumMap) const;
    bool                  SetVarMap(MAP_RTTI_VAR& VarMap) const;
    bool                  SetFuncMap(MAP_RTTI_FUNC& FuncMap) const;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    bool                  SetEnumMap(MAP_RTTI_ENUM&& EnumMap) const;
    bool                  SetVarMap(MAP_RTTI_VAR&& VarMap) const;
    bool                  SetFuncMap(MAP_RTTI_FUNC&& FuncMap) const;
#endif
private:
    CRTTI(const CRTTI&);
    CRTTI& operator=(const CRTTI&);
public:
    static const CRTTIEnum   msc_EnumNone;
private:
    CPCXStr                m_pszName;
    const CRTTI*           m_pBase;
    const uintptr_t        m_utTraits;
    const CreateObject*    m_pCreate;
};

///////////////////////////////////////////////////////////////////
// CRTTIType
class NOVTABLE CRTTIType ABSTRACT : public MObject
{
public:
    virtual ~CRTTIType(void);

    CRTTIType(const CRTTIType& aSrc);
    CRTTIType& operator=(const CRTTIType& aSrc);

    const CRTTI& GetRTTI(void)   const;
    PCXStr       GetOrigin(void) const;
    const size_t GetType(void)   const;

    bool  IsEnum(void) const;
    
    bool  IsPointer(void) const;
    bool  IsStatic(void) const;
    bool  IsConst(void) const;
    bool  IsFunction(void) const;
protected:
    CRTTIType(const CRTTI& rtti, PCXStr pszOrigin, size_t stType);
protected:
    const CRTTI*   m_pRTTI;
    PCXStr         m_pszOrigin; // !!!m_pszOrigin is global const string
    size_t         m_stType;
};

///////////////////////////////////////////////////////////////////
// CRTTIEnum : member enum wrapper
class CRTTIEnum : public CRTTIType
{
public:
    typedef struct tagENUM_ITEM
    {
        // !!!pszName is global const string
        CPCXStr   pszName;
        ULLong    ullValue;
    }ENUM_ITEM, *PENUM_ITEM;

    typedef CTArray<ENUM_ITEM>   ARY_ITEM;
public:
    CRTTIEnum(const CRTTI& rtti, PCXStr pszOrigin, const ARY_ITEM& Items);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CRTTIEnum(const CRTTI& rtti, PCXStr pszOrigin, ARY_ITEM&& Items);
#endif
    virtual ~CRTTIEnum(void);

    CRTTIEnum(const CRTTIEnum& aSrc);
    CRTTIEnum& operator=(const CRTTIEnum& aSrc);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CRTTIEnum(CRTTIEnum&& aSrc);
    CRTTIEnum& operator=(CRTTIEnum&& aSrc);
#endif

    ULLong At(PCXStr pszName) const;
    ULLong At(const CString& strName) const;

    PCXStr At(ULLong ullValue) const;
    bool   At(ULLong ullValue, CString& strName) const;

    const  ARY_ITEM& GetItems(void) const;
protected:
    ARY_ITEM   m_Items;
};
typedef CRTTIEnum::ARY_ITEM ENUM_ARY_ITEM;

///////////////////////////////////////////////////////////////////
// CTRTTIVar : member variable wrapper
template <typename TOBJECT, typename TVAR>
class CTRTTIVar : public CRTTIType
{
public:
    CTRTTIVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TVAR TOBJECT::* pVar = nullptr);
    virtual ~CTRTTIVar(void);

    CTRTTIVar(const CTRTTIVar& aSrc);
    CTRTTIVar& operator=(const CTRTTIVar& aSrc);

    bool  IsValid(void) const;

    TVAR& At(TOBJECT& ObjRef) const;
    TVAR& At(TOBJECT* pObj) const;
    const TVAR& At(const TOBJECT& ObjRef) const;
    const TVAR& At(const TOBJECT* pObj) const;
private:
    TVAR TOBJECT::* Get(void) const;
protected:
    TVAR TOBJECT::*   m_pVar;
};

///////////////////////////////////////////////////////////////////
// CTRTTICVar : member const variable wrapper
template <typename TOBJECT, typename TVAR>
class CTRTTICVar : public CRTTIType
{
public:
    CTRTTICVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, const TVAR TOBJECT::* pVarConst = nullptr);
    virtual ~CTRTTICVar(void);

    CTRTTICVar(const CTRTTICVar& aSrc);
    CTRTTICVar& operator=(const CTRTTICVar& aSrc);

    bool  IsValid(void) const;

    const TVAR& At(const TOBJECT& ObjRef) const;
    const TVAR& At(const TOBJECT* pObj) const;
private:
    const TVAR TOBJECT::* Get(void) const;
protected:
    const TVAR TOBJECT::*   m_pVarConst;
};

///////////////////////////////////////////////////////////////////
// CTRTTISVar : static member variable wrapper
template <typename TOBJECT, typename TVAR>
class CTRTTISVar : public CRTTIType
{
public:
    CTRTTISVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TVAR* pVarStatic = nullptr);
    virtual ~CTRTTISVar(void);

    CTRTTISVar(const CTRTTISVar& aSrc);
    CTRTTISVar& operator=(const CTRTTISVar& aSrc);

    bool  IsValid(void) const;

    TVAR& At(void) const;
private:
    TVAR* Get(void) const;
protected:
    TVAR*   m_pVarStatic;
};

///////////////////////////////////////////////////////////////////
// CTRTTISCVar : static const member variable wrapper
template <typename TOBJECT, typename TVAR>
class CTRTTISCVar : public CRTTIType
{
public:
    CTRTTISCVar(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, const TVAR* pVarStaticConst = nullptr);
    virtual ~CTRTTISCVar(void);

    CTRTTISCVar(const CTRTTISCVar& aSrc);
    CTRTTISCVar& operator=(const CTRTTISCVar& aSrc);

    bool  IsValid(void) const;

    const TVAR& At(void) const;
private:
    const TVAR* Get(void) const;
protected:
    const TVAR*   m_pVarStaticConst;
};

#ifdef __MODERN_CXX_NOT_SUPPORTED
///////////////////////////////////////////////////////////////////
// CTRTTIFunc : member function wrapper
template <typename TOBJECT, typename TFUNC>
class CTRTTIFunc : public CRTTIType
{
public:
    CTRTTIFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TFUNC pFunc = nullptr);
    virtual ~CTRTTIFunc(void);

    CTRTTIFunc(const CTRTTIFunc& aSrc);
    CTRTTIFunc& operator=(const CTRTTIFunc& aSrc);

    bool  IsValid(void) const;

    TFUNC Get(void) const;
protected:
    TFUNC   m_pFunc;
};

///////////////////////////////////////////////////////////////////
// CTRTTICFunc : member const function wrapper
template <typename TOBJECT, typename TCFUNC>
class CTRTTICFunc : public CRTTIType
{
public:
    CTRTTICFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TCFUNC pFuncConst = nullptr);
    virtual ~CTRTTICFunc(void);

    CTRTTICFunc(const CTRTTICFunc& aSrc);
    CTRTTICFunc& operator=(const CTRTTICFunc& aSrc);

    bool   IsValid(void) const;

    TCFUNC Get(void) const;
protected:
    TCFUNC   m_pFuncConst;
};

///////////////////////////////////////////////////////////////////
// CTRTTISFunc : member static function wrapper
template <typename TOBJECT, typename TSFUNC>
class CTRTTISFunc : public CRTTIType
{
public:
    CTRTTISFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, TSFUNC pFuncStatic = nullptr);
    virtual ~CTRTTISFunc(void);

    CTRTTISFunc(const CTRTTISFunc& aSrc);
    CTRTTISFunc& operator=(const CTRTTISFunc& aSrc);

    bool   IsValid(void) const;

    TSFUNC Get(void) const;
protected:
    TSFUNC   m_pFuncStatic;
};
#else
///////////////////////////////////////////////////////////////////
// CTRTTIFunc : member function wrapper
template <typename TOBJECT, typename TFUNC>
class CTRTTIFunc;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTRTTIFunc<TOBJECT, TRET(TARGS...)> : public CRTTIType
{
public:
    typedef TRET (TOBJECT::* RTTI_FUNC)(TARGS...);
public:
    CTRTTIFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_FUNC pFunc = nullptr);
    virtual ~CTRTTIFunc(void);

    CTRTTIFunc(const CTRTTIFunc& aSrc);
    CTRTTIFunc& operator=(const CTRTTIFunc& aSrc);

    bool  IsValid(void) const;
    RTTI_FUNC Get(void) const;

    TRET Invoke(TOBJECT& ObjRef, TARGS&&... Args) const;
    TRET Invoke(const TOBJECT* pObj, TARGS&&... Args) const;
protected:
    RTTI_FUNC   m_pFunc;
};

///////////////////////////////////////////////////////////////////
// CTRTTICFunc : member const function wrapper
template <typename TOBJECT, typename TFUNC>
class CTRTTICFunc;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTRTTICFunc<TOBJECT, TRET(TARGS...) const> : public CRTTIType
{
public:
    typedef TRET (TOBJECT::* RTTI_CFUNC)(TARGS...) const;
public:
    CTRTTICFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_CFUNC pFuncConst = nullptr);
    virtual ~CTRTTICFunc(void);

    CTRTTICFunc(const CTRTTICFunc& aSrc);
    CTRTTICFunc& operator=(const CTRTTICFunc& aSrc);

    bool  IsValid(void) const;
    RTTI_CFUNC Get(void) const;

    TRET Invoke(TOBJECT& ObjRef, TARGS&&... Args) const;
    TRET Invoke(const TOBJECT* pObj, TARGS&&... Args) const;
protected:
    RTTI_CFUNC   m_pFuncConst;
};

///////////////////////////////////////////////////////////////////
// CTRTTISFunc : member static function wrapper
template <typename TOBJECT, typename TFUNC>
class CTRTTISFunc;
template <typename TOBJECT, typename TRET, typename... TARGS>
class CTRTTISFunc<TOBJECT, TRET(TARGS...)> : public CRTTIType
{
public:
    typedef TRET (* RTTI_SFUNC)(TARGS...);
public:
    CTRTTISFunc(const CRTTI& rtti, PCXStr pszOrigin, size_t stType, RTTI_SFUNC pFuncStatic = nullptr);
    virtual ~CTRTTISFunc(void);

    CTRTTISFunc(const CTRTTISFunc& aSrc);
    CTRTTISFunc& operator=(const CTRTTISFunc& aSrc);

    bool  IsValid(void) const;
    RTTI_SFUNC Get(void) const;

    TRET Invoke(TARGS&&... Args) const;
protected:
    RTTI_SFUNC   m_pFuncStatic;
};
#endif


///////////////////////////////////////////////////////////////////
// CRTTITraits
class CRTTITraits
{
public:
    template <typename TOBJECT, typename TBASE>
    static bool IsExactClass(TBASE* pObj);

    template <typename TOBJECT, typename TBASE>
    static bool IsKindOf(TBASE* pObj);

    template <typename TOBJECT, typename TBASE>
    static TOBJECT* DynamicCast(TBASE* pObj);

    template <typename TOBJECT, typename TBASE>
    static TOBJECT* DynamicCast(TBASE* pObj, PCXStr pszName);

    template <typename TOBJECT>
    static bool AddEnum(MAP_RTTI_ENUM& EnumMap, PCXStr pszName, ENUM_ARY_ITEM& Items, PCXStr pszOrigin);

    template <typename TOBJECT>
    static const CRTTIEnum& GetEnum(PCXStr pszName);

    template <typename TOBJECT>
    static const CRTTIEnum& GetEnum(const CString& strName);

    template <typename TOBJECT, typename TVAR>
    static bool AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, TVAR TOBJECT::* pVar, PCXStr pszOrigin, size_t stType = VART_POINTER);

    template <typename TOBJECT, typename TVAR>
    static bool AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, const TVAR TOBJECT::* pVar, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static bool AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, TVAR* pVar, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_STATIC));

    template <typename TOBJECT, typename TVAR>
    static bool AddVar(MAP_RTTI_VAR& VarMap, PCXStr pszName, const TVAR* pVar, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_STATIC|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTIVar<TOBJECT, TVAR> GetVar(const CRTTIType* pRTTIType, size_t stType = VART_POINTER);

    template <typename TOBJECT, typename TVAR>
    static const CTRTTIVar<TOBJECT, TVAR> GetVar(PCXStr pszName, size_t stType = VART_POINTER);

    template <typename TOBJECT, typename TVAR>
    static const CTRTTIVar<TOBJECT, TVAR> GetVar(const CString& strName, size_t stType = VART_POINTER);

    template <typename TOBJECT, typename TVAR>
    static const CTRTTICVar<TOBJECT, TVAR> GetConstVar(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTICVar<TOBJECT, TVAR> GetConstVar(PCXStr pszName, size_t stType = (VART_POINTER|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTICVar<TOBJECT, TVAR> GetConstVar(const CString& strName, size_t stType = (VART_POINTER|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISVar<TOBJECT, TVAR> GetStaticVar(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_STATIC));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISVar<TOBJECT, TVAR> GetStaticVar(PCXStr pszName, size_t stType = (VART_POINTER|VART_STATIC));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISVar<TOBJECT, TVAR> GetStaticVar(const CString& strName, size_t stType = (VART_POINTER|VART_STATIC));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISCVar<TOBJECT, TVAR> GetStaticConstVar(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_STATIC|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISCVar<TOBJECT, TVAR> GetStaticConstVar(PCXStr pszName, size_t stType = (VART_POINTER|VART_STATIC|VART_CONST));

    template <typename TOBJECT, typename TVAR>
    static const CTRTTISCVar<TOBJECT, TVAR> GetStaticConstVar(const CString& strName, size_t stType = (VART_POINTER|VART_STATIC|VART_CONST));

#ifdef __MODERN_CXX_NOT_SUPPORTED
    template <typename TOBJECT, typename TFUNC>
    static bool AddFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TFUNC pFunc, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TCFUNC>
    static bool AddConstFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TCFUNC pFunc, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TSFUNC>
    static bool AddStaticFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TSFUNC pFunc, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TFUNC>
    static const CTRTTIFunc<TOBJECT, TFUNC> GetFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TFUNC>
    static const CTRTTIFunc<TOBJECT, TFUNC> GetFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TFUNC>
    static const CTRTTIFunc<TOBJECT, TFUNC> GetFunc(const CString& strName, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TCFUNC>
    static const CTRTTICFunc<TOBJECT, TCFUNC> GetConstFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TCFUNC>
    static const CTRTTICFunc<TOBJECT, TCFUNC> GetConstFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TCFUNC>
    static const CTRTTICFunc<TOBJECT, TCFUNC> GetConstFunc(const CString& strName, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TSFUNC>
    static const CTRTTISFunc<TOBJECT, TSFUNC> GetStaticFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TSFUNC>
    static const CTRTTISFunc<TOBJECT, TSFUNC> GetStaticFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TSFUNC>
    static const CTRTTISFunc<TOBJECT, TSFUNC> GetStaticFunc(const CString& strName, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));
#else
    template <typename TOBJECT, typename TRET, typename... TARGS>
    static bool AddFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...), PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static bool AddConstFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (TOBJECT::* pFunc)(TARGS...) const, PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static bool AddStaticFunc(MAP_RTTI_FUNC& FuncMap, PCXStr pszName, TRET (* pFunc)(TARGS...), PCXStr pszOrigin, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTIFunc<TOBJECT, TRET(TARGS...)> GetFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTIFunc<TOBJECT, TRET(TARGS...)> GetFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTIFunc<TOBJECT, TRET(TARGS...)> GetFunc(const CString& strName, size_t stType = (VART_POINTER|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> GetConstFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> GetConstFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTICFunc<TOBJECT, TRET(TARGS...) const> GetConstFunc(const CString& strName, size_t stType = (VART_POINTER|VART_CONST|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTISFunc<TOBJECT, TRET(TARGS...)> GetStaticFunc(const CRTTIType* pRTTIType, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTISFunc<TOBJECT, TRET(TARGS...)> GetStaticFunc(PCXStr pszName, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));

    template <typename TOBJECT, typename TRET, typename... TARGS>
    static const CTRTTISFunc<TOBJECT, TRET(TARGS...)> GetStaticFunc(const CString& strName, size_t stType = (VART_POINTER|VART_STATIC|VART_FUNCTION));
#endif
};

///////////////////////////////////////////////////////////////////
// cast
///////////////////////////////////////////////////////////////////
#define EXACT_CLASS( xclass, xbase, pobj ) CRTTITraits::IsExactClass<xclass, xbase>(pobj)
#define KIND_OF( xclass, xbase, pobj ) CRTTITraits::IsKindOf<xclass, xbase>(pobj)
#define DYNAMIC_CAST( xclass, xbase, pobj ) CRTTITraits::DynamicCast<xclass, xbase>(pobj)
#define DYNAMIC_CAST_NAME( xclass, xbase, pobj, xname ) CRTTITraits::DynamicCast<xclass, xbase>(pobj, TF(#xname))


///////////////////////////////////////////////////////////////////
// declare type
///////////////////////////////////////////////////////////////////
// declare member variable type
#define DECL_TYPE_VAR( xname, xtype, xclass, xvar )
// declare const member variable type
#define DECL_TYPE_CVAR( xname, xtype, xclass, xvar )
// declare static member variable type
#define DECL_TYPE_SVAR( xname, xtype, xclass, xvar )
// declare static const member variable type
#define DECL_TYPE_SCVAR( xname, xtype, xclass, xvar )

#ifdef __MODERN_CXX_NOT_SUPPORTED
///////////////////////////////////////////////////////////////////
// declare member function type
#define DECL_TYPE_FUNC( xname, xret, xclass, xfunc ) typedef xret (xclass::* FUNC_##xname)(void)
// declare const member function type
#define DECL_TYPE_CFUNC( xname, xret, xclass, xfunc ) typedef xret (xclass::* CFUNC_##xname)(void) const
// declare static member/global function type
#define DECL_TYPE_SFUNC( xname, xret, xclass, xfunc ) typedef xret (*SFUNC_##xname)(void)

///////////////////////////////////////////////////////////////////
// declare member function type with args
#define DECL_TYPE_FUNCV( xname, xret, xclass, xfunc, ... ) typedef xret (xclass::* FUNCV_##xname)(__VA_ARGS__)
// declare const member function type with args
#define DECL_TYPE_CFUNCV( xname, xret, xclass, xfunc, ... ) typedef xret (xclass::* CFUNCV_##xname)(__VA_ARGS__) const
// declare static member/global function type with args
#define DECL_TYPE_SFUNCV( xname, xret, xclass, xfunc, ... ) typedef xret (*SFUNCV_##xname)(__VA_ARGS__)

#else
///////////////////////////////////////////////////////////////////
// declare member function type
#define DECL_TYPE_FUNC( xname, xret, xclass, xfunc ) private : constexpr static CPCXStr FUNC_##xname {TF(#xret " " #xfunc "(void)")};
// declare const member function type
#define DECL_TYPE_CFUNC( xname, xret, xclass, xfunc ) private : constexpr static CPCXStr CFUNC_##xname {TF(#xret " " #xfunc "(void) const")};
// declare static member/global function type
#define DECL_TYPE_SFUNC( xname, xret, xclass, xfunc ) private : constexpr static CPCXStr SFUNC_##xname {TF("static " #xret " " #xfunc "(void)")};

///////////////////////////////////////////////////////////////////
// declare member function type with args
#define DECL_TYPE_FUNCV( xname, xret, xclass, xfunc, ... ) private : constexpr static CPCXStr FUNCV_##xname {TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")")};
// declare const member function type with args
#define DECL_TYPE_CFUNCV( xname, xret, xclass, xfunc, ... ) private : constexpr static CPCXStr CFUNCV_##xname {TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ") const")};
// declare static member/global function type with args
#define DECL_TYPE_SFUNCV( xname, xret, xclass, xfunc, ... ) private : constexpr static CPCXStr SFUNCV_##xname {TF("static " #xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")")};

#endif


///////////////////////////////////////////////////////////////////
#define DECL_TYPE_END( xclass )


///////////////////////////////////////////////////////////////////
// defined type
///////////////////////////////////////////////////////////////////
#define DEF_TYPE_ENUM_BEGIN( xenum ) Items.RemoveAll()
#define DEF_TYPE_ENUM_ITEM( xitem ) CRTTIEnum::ENUM_ITEM ITEM_##xitem = {TF(#xitem), (ULLong)xitem}; Items.Add(ITEM_##xitem)
#define DEF_TYPE_ENUM_END( xenum, xclass ) CRTTITraits::AddEnum<xclass>(EnumMap, TF(#xenum), Items, TF(#xenum))


///////////////////////////////////////////////////////////////////
// defined [static const] member variable type 
#define DEF_TYPE_VAR( xname, xtype, xclass, xvar, xinit ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF(#xtype " " #xvar " = " #xinit))
#define DEF_TYPE_CVAR( xname, xtype, xclass, xvar, xinit ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("const " #xtype " " #xvar " = " #xinit))
#define DEF_TYPE_SVAR( xname, xtype, xclass, xvar, xinit ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("static " #xtype " " #xvar " = " #xinit))
#define DEF_TYPE_SCVAR( xname, xtype, xclass, xvar, xinit ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("static const " #xtype " " #xvar " = " #xinit))


// defined [static const] member variable type with VAR_TYPE
#define DEF_TYPE_VAR_TYPE( xname, xtype, xclass, xvar, xinit, xvartype ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF(#xtype " " #xvar " = " #xinit), xvartype)
#define DEF_TYPE_CVAR_TYPE( xname, xtype, xclass, xvar, xinit, xvartype ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("const " #xtype " " #xvar " = " #xinit), xvartype)
#define DEF_TYPE_SVAR_TYPE( xname, xtype, xclass, xvar, xinit, xvartype ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("static " #xtype " " #xvar " = " #xinit), xvartype)
#define DEF_TYPE_SCVAR_TYPE( xname, xtype, xclass, xvar, xinit, xvartype ) \
    CRTTITraits::AddVar<xclass, xtype>(VarMap, TF(#xname), (&xclass::xvar), TF("static const " #xtype " " #xvar " = " #xinit), xvartype)

#ifdef __MODERN_CXX_NOT_SUPPORTED
///////////////////////////////////////////////////////////////////
// defined member function type
#define DEF_TYPE_FUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddFunc<xclass, xclass::FUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(void)"))
// defined const member function type
#define DEF_TYPE_CFUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddConstFunc<xclass, xclass::CFUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(void) const"))
// defined static member/global function type
#define DEF_TYPE_SFUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddStaticFunc<xclass, xclass::SFUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF("static " #xret " " #xfunc "(void)"))

///////////////////////////////////////////////////////////////////
// defined member function type with args
#define DEF_TYPE_FUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddFunc<xclass, xclass::FUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")"))
// defined const member function type with args
#define DEF_TYPE_CFUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddConstFunc<xclass, xclass::CFUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ") const"))
// defined static member/global function type with args
#define DEF_TYPE_SFUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddStaticFunc<xclass, xclass::SFUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF("static " #xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")"))

///////////////////////////////////////////////////////////////////
// defined member function type with VAR_TYPE
#define DEF_TYPE_FUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddFunc<xclass, xclass::FUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(void)"), xvartype)
// defined const member function type with VAR_TYPE
#define DEF_TYPE_CFUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddConstFunc<xclass, xclass::CFUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(void) const"), xvartype)
// defined static member/global function type with VAR_TYPE
#define DEF_TYPE_SFUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddStaticFunc<xclass, xclass::SFUNC_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF("static " #xret " " #xfunc "(void)"), xvartype)

///////////////////////////////////////////////////////////////////
// defined member function type with args and VAR_TYPE
#define DEF_TYPE_FUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddFunc<xclass, xclass::FUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")"), xvartype)
// defined const member function type with args and VAR_TYPE
#define DEF_TYPE_CFUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddConstFunc<xclass, xclass::CFUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF(#xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ") const"), xvartype)
// defined static member/global function type with args and VAR_TYPE
#define DEF_TYPE_SFUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddStaticFunc<xclass, xclass::SFUNCV_##xname>(FuncMap, TF(#xname), (&xclass::xfunc), TF("static " #xret " " #xfunc "(" VA_ARGS_SEQ_STR(__VA_ARGS__) ")"), xvartype)

#else
///////////////////////////////////////////////////////////////////
// defined [static const] member/global function type 
#define DEF_TYPE_FUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::FUNC_##xname)
#define DEF_TYPE_CFUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddConstFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::CFUNC_##xname)
#define DEF_TYPE_SFUNC( xname, xret, xclass, xfunc ) \
    CRTTITraits::AddStaticFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::SFUNC_##xname)

///////////////////////////////////////////////////////////////////
// defined [static const] member/global function type with args
#define DEF_TYPE_FUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::FUNCV_##xname)
#define DEF_TYPE_CFUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddConstFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::CFUNCV_##xname)
#define DEF_TYPE_SFUNCV( xname, xret, xclass, xfunc, ... ) \
    CRTTITraits::AddStaticFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::SFUNCV_##xname)

///////////////////////////////////////////////////////////////////
// defined [static const] member/global function type with VAR_TYPE
#define DEF_TYPE_FUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::FUNC_##xname, xvartype)
#define DEF_TYPE_CFUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddConstFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::CFUNC_##xname, xvartype)
#define DEF_TYPE_SFUNC_TYPE( xname, xret, xclass, xfunc, xvartype ) \
    CRTTITraits::AddStaticFunc<xclass, xret>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::SFUNC_##xname, xvartype)

///////////////////////////////////////////////////////////////////
// defined [static const] member/global function type with args and VAR_TYPE
#define DEF_TYPE_FUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::FUNCV_##xname, xvartype)
#define DEF_TYPE_CFUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddConstFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::CFUNCV_##xname, xvartype)
#define DEF_TYPE_SFUNCV_TYPE( xname, xret, xclass, xfunc, xvartype, ... ) \
    CRTTITraits::AddStaticFunc<xclass, xret, __VA_ARGS__>(FuncMap, TF(#xname), (&xclass::xfunc), xclass::SFUNCV_##xname, xvartype)

#endif


///////////////////////////////////////////////////////////////////
// type helpers
///////////////////////////////////////////////////////////////////
#define GET_TYPE_ENUM( xenum, xclass ) CRTTITraits::GetEnum<xclass>(TF(#xenum))

///////////////////////////////////////////////////////////////////
#define TYPE_VAR( xtype, xclass ) \
    CTRTTIVar<xclass, xtype>
///////////////////////////////////////////////////////////////////
#define TYPE_CVAR( xtype, xclass ) \
    CTRTTICVar<xclass, xtype>
///////////////////////////////////////////////////////////////////
#define TYPE_SVAR( xtype, xclass ) \
    CTRTTISVar<xclass, xtype>
///////////////////////////////////////////////////////////////////
#define TYPE_SCVAR( xtype, xclass ) \
    CTRTTISCVar<xclass, xtype>

///////////////////////////////////////////////////////////////////
#define GET_TYPE_VAR( xname, xtype, xclass ) \
    CRTTITraits::GetVar<xclass, xtype>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CVAR( xname, xtype, xclass ) \
    CRTTITraits::GetConstVar<xclass, xtype>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SVAR( xname, xtype, xclass ) \
    CRTTITraits::GetStaticVar<xclass, xtype>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SCVAR( xname, xtype, xclass ) \
    CRTTITraits::GetStaticConstVar<xclass, xtype>(TF(#xname))

///////////////////////////////////////////////////////////////////
#define GET_TYPE_VAR_PTR( xptr, xtype, xclass ) \
    CRTTITraits::GetVar<xclass, xtype>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CVAR_PTR( xptr, xtype, xclass ) \
    CRTTITraits::GetConstVar<xclass, xtype>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SVAR_PTR( xptr, xtype, xclass ) \
    CRTTITraits::GetStaticVar<xclass, xtype>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SCVAR_PTR( xptr, xtype, xclass ) \
    CRTTITraits::GetStaticConstVar<xclass, xtype>(xptr)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_VAR_TYPE( xname, xtype, xclass, xvartype ) \
    CRTTITraits::GetVar<xclass, xtype>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CVAR_TYPE( xname, xtype, xclass, xvartype ) \
    CRTTITraits::GetConstVar<xclass, xtype>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SVAR_TYPE( xname, xtype, xclass, xvartype ) \
    CRTTITraits::GetStaticVar<xclass, xtype>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SCVAR_TYPE( xname, xtype, xclass, xvartype ) \
    CRTTITraits::GetStaticConstVar<xclass, xtype>(TF(#xname), xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_VAR_PTR_TYPE( xptr, xtype, xclass, xvartype ) \
    CRTTITraits::GetVar<xclass, xtype>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CVAR_PTR_TYPE( xptr, xtype, xclass, xvartype ) \
    CRTTITraits::GetConstVar<xclass, xtype>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SVAR_PTR_TYPE( xptr, xtype, xclass, xvartype ) \
    CRTTITraits::GetStaticVar<xclass, xtype>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SCVAR_PTR_TYPE( xptr, xtype, xclass, xvartype ) \
    CRTTITraits::GetStaticConstVar<xclass, xtype>(xptr, xvartype)

#ifdef __MODERN_CXX_NOT_SUPPORTED
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_FUNC( xname, xret, xclass ) \
    CTRTTIFunc<xclass, xclass::FUNC_##xname>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_CFUNC( xname, xret, xclass ) \
    CTRTTICFunc<xclass, xclass::CFUNC_##xname>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_SFUNC( xname, xret, xclass ) \
    CTRTTISFunc<xclass, xclass::SFUNC_##xname>

///////////////////////////////////////////////////////////////////
#define NAME_TYPE_FUNCV( xname, xret, xclass, ...  ) \
    CTRTTIFunc<xclass, xclass::FUNCV_##xname>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_CFUNCV( xname, xret, xclass, ...  ) \
    CTRTTICFunc<xclass, xclass::CFUNCV_##xname>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_SFUNCV( xname, xret, xclass, ...  ) \
    CTRTTISFunc<xclass, xclass::SFUNCV_##xname>

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC( xname, xret, xclass ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNC_##xname>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC( xname, xret, xclass ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNC_##xname>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC( xname, xret, xclass ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNC_##xname>(TF(#xname))

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_PTR( xptr, xname, xret, xclass ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNC_##xname>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_PTR( xptr, xname, xret, xclass ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNC_##xname>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_PTR( xptr, xname, xret, xclass ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNC_##xname>(xptr)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNCV_##xname>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNCV_##xname>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNCV_##xname>(TF(#xname))

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_PTR( xptr, xname, xret, xclass, ... ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNCV_##xname>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_PTR( xptr, xname, xret, xclass, ... ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNCV_##xname>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_PTR( xptr, xname, xret, xclass, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNCV_##xname>(xptr)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNC_##xname>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNC_##xname>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNC_##xname>(TF(#xname), xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_PTR_TYPE( xptr, xname, xret, xclass, xvartype ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNC_##xname>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_PTR_TYPE( xptr, xname, xret, xclass, xvartype ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNC_##xname>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_PTR_TYPE( xptr, xname, xret, xclass, xvartype ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNC_##xname>(xptr, xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNCV_##xname>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNCV_##xname>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNCV_##xname>(TF(#xname), xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_PTR_TYPE( xptr, xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetFunc<xclass, xclass::FUNCV_##xname>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_PTR_TYPE( xptr, xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetConstFunc<xclass, xclass::CFUNCV_##xname>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_PTR_TYPE( xptr, xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xclass::SFUNCV_##xname>(xptr, xvartype)

#else
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_FUNC( xname, xret, xclass ) \
    CTRTTIFunc<xclass, xret()>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_CFUNC( xname, xret, xclass ) \
    CTRTTICFunc<xclass, xret() const>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_SFUNC( xname, xret, xclass ) \
    CTRTTISFunc<xclass, xret()>

///////////////////////////////////////////////////////////////////
#define TYPE_FUNC( xret, xclass ) \
    CTRTTIFunc<xclass, xret()>
///////////////////////////////////////////////////////////////////
#define TYPE_CFUNC( xret, xclass ) \
    CTRTTICFunc<xclass, xret() const>
///////////////////////////////////////////////////////////////////
#define TYPE_SFUNC( xret, xclass ) \
    CTRTTISFunc<xclass, xret()>

///////////////////////////////////////////////////////////////////
#define NAME_TYPE_FUNCV( xname, xret, xclass, ... ) \
    CTRTTIFunc<xclass, xret(__VA_ARGS__)>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_CFUNCV( xname, xret, xclass, ... ) \
    CTRTTICFunc<xclass, xret(__VA_ARGS__) const>
///////////////////////////////////////////////////////////////////
#define NAME_TYPE_SFUNCV( xname, xret, xclass, ... ) \
    CTRTTISFunc<xclass, xret(__VA_ARGS__)>

///////////////////////////////////////////////////////////////////
#define TYPE_FUNCV( xret, xclass, ... ) \
    CTRTTIFunc<xclass, xret(__VA_ARGS__)>
///////////////////////////////////////////////////////////////////
#define TYPE_CFUNCV( xret, xclass, ... ) \
    CTRTTICFunc<xclass, xret(__VA_ARGS__) const>
///////////////////////////////////////////////////////////////////
#define TYPE_SFUNCV( xret, xclass, ... ) \
    CTRTTISFunc<xclass, xret(__VA_ARGS__)>

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC( xname, xret, xclass ) \
    CRTTITraits::GetFunc<xclass, xret>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC( xname, xret, xclass ) \
    CRTTITraits::GetConstFunc<xclass, xret>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC( xname, xret, xclass ) \
    CRTTITraits::GetStaticFunc<xclass, xret>(TF(#xname))

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_PTR( xptr, xret, xclass ) \
    CRTTITraits::GetFunc<xclass, xret>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_PTR( xptr, xret, xclass ) \
    CRTTITraits::GetConstFunc<xclass, xret>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_PTR( xptr, xret, xclass ) \
    CRTTITraits::GetStaticFunc<xclass, xret>(xptr)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetFunc<xclass, xret, __VA_ARGS__>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetConstFunc<xclass, xret, __VA_ARGS__>(TF(#xname))
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV( xname, xret, xclass, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xret, __VA_ARGS__>(TF(#xname))

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_PTR( xptr, xret, xclass, ... ) \
    CRTTITraits::GetFunc<xclass, xret, __VA_ARGS__>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_PTR( xptr, xret, xclass, ... ) \
    CRTTITraits::GetConstFunc<xclass, xret, __VA_ARGS__>(xptr)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_PTR( xptr, xret, xclass, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xret, __VA_ARGS__>(xptr)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetFunc<xclass, xret>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetConstFunc<xclass, xret>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_TYPE( xname, xret, xclass, xvartype ) \
    CRTTITraits::GetStaticFunc<xclass, xret>(TF(#xname), xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNC_PTR_TYPE( xptr, xret, xclass, xvartype ) \
    CRTTITraits::GetFunc<xclass, xret>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNC_PTR_TYPE( xptr, xret, xclass, xvartype ) \
    CRTTITraits::GetConstFunc<xclass, xret>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNC_PTR_TYPE( xptr, xret, xclass, xvartype ) \
    CRTTITraits::GetStaticFunc<xclass, xret>(xptr, xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetFunc<xclass, xret, __VA_ARGS__>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetConstFunc<xclass, xret, __VA_ARGS__>(TF(#xname), xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_TYPE( xname, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xret, __VA_ARGS__>(TF(#xname), xvartype)

///////////////////////////////////////////////////////////////////
#define GET_TYPE_FUNCV_PTR_TYPE( xptr, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetFunc<xclass, xret, __VA_ARGS__>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_CFUNCV_PTR_TYPE( xptr, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetConstFunc<xclass, xret, __VA_ARGS__>(xptr, xvartype)
///////////////////////////////////////////////////////////////////
#define GET_TYPE_SFUNCV_PTR_TYPE( xptr, xret, xclass, xvartype, ... ) \
    CRTTITraits::GetStaticFunc<xclass, xret, __VA_ARGS__>(xptr, xvartype)

#endif


///////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////
// declare RTTI default constructor trait
#define DECL_CREATE_TRAIT( xclass ) \
public: \
    static CObject* CreateByRTTI(void) \
    { \
        return MNEW xclass; \
    }

///////////////////////////////////////////////////////////////////
// declare RTTI type trait
#define DECL_TYPE_TRAIT( xclass ) \
private: \
    static bool  ReflectByRTTI(const CRTTI& rtti); \
public: \
    static const ReflectType* GetReflect(void) \
    { \
        return &(xclass::ReflectByRTTI); \
    }
    
///////////////////////////////////////////////////////////////////
// defined type trait
#define DEF_TYPE_TRAIT( xclass ) \
bool xclass::ReflectByRTTI(const CRTTI& rtti) \
{ \
    if (xclass::ClassRTTI().IsExactClass(rtti)) \
    { \
        ENUM_ARY_ITEM  Items; \
        MAP_RTTI_ENUM  EnumMap; \
        MAP_RTTI_VAR   VarMap; \
        MAP_RTTI_FUNC  FuncMap;

///////////////////////////////////////////////////////////////////
#ifdef __MODERN_CXX_NOT_SUPPORTED
#define DEF_TYPE_END( xclass ) \
        if (EnumMap.GetSize() > 0) \
        { \
            rtti.SetEnumMap(EnumMap); \
        } \
        if (VarMap.GetSize() > 0) \
        { \
            rtti.SetVarMap(VarMap); \
        } \
        if (FuncMap.GetSize() > 0) \
        { \
            rtti.SetFuncMap(FuncMap); \
        } \
        return true; \
    } \
    return false; \
}
#else
#define DEF_TYPE_END( xclass ) \
        if (EnumMap.GetSize() > 0) \
        { \
            rtti.SetEnumMap(std::move(EnumMap)); \
        } \
        if (VarMap.GetSize() > 0) \
        { \
            rtti.SetVarMap(std::move(VarMap)); \
        } \
        if (FuncMap.GetSize() > 0) \
        { \
            rtti.SetFuncMap(std::move(FuncMap)); \
        } \
        return true; \
    } \
    return false; \
}
#endif


///////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////
// RTTI helpers
#define DECL_RTTI( xclass ) \
public: \
    static  const CRTTI  ms_RTTI; \
    static  const CRTTI& ClassRTTI(void) \
    { \
        return ms_RTTI; \
    } \
    virtual const CRTTI& GetRTTI(void) const \
    { \
        return ms_RTTI; \
    }

///////////////////////////////////////////////////////////////////
#define DEF_RTTI_ROOT( xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xroot), nullptr, (size_t)xtraits)
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_ROOT_NAME( xname, xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xname), nullptr, (size_t)xtraits)
///////////////////////////////////////////////////////////////////
#define DEF_RTTI( xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xclass), &(xbase::ms_RTTI), (size_t)xtraits)
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_NAME( xname, xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xname), &(xbase::ms_RTTI), (size_t)xtraits)

///////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////
// RTTI with default create-object helpers
#define DECL_RTTI_CREATE( xclass ) \
    DECL_RTTI( xclass ) \
    DECL_CREATE_TRAIT( xclass )

///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_ROOT( xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xroot), nullptr, (size_t)(xtraits|CRTTI::TRAITS_CREATE), &(xroot::CreateByRTTI))
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_ROOT_NAME( xname, xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xname), nullptr, (size_t)(xtraits|CRTTI::TRAITS_CREATE), &(xroot::CreateByRTTI))
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE( xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xclass), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_CREATE), &(xclass::CreateByRTTI))
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_NAME( xname, xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xname), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_CREATE), &(xclass::CreateByRTTI))

///////////////////////////////////////////////////////////////////
// RTTI with reflect type helpers
#define DECL_RTTI_TYPE( xclass ) \
    DECL_RTTI( xclass ) \
    DECL_TYPE_TRAIT( xclass )

///////////////////////////////////////////////////////////////////
#define DEF_RTTI_TYPE_ROOT( xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xroot), nullptr, (size_t)(xtraits|CRTTI::TRAITS_TYPE), nullptr, xroot::GetReflect()); \
    DEF_TYPE_TRAIT( xroot )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_TYPE_ROOT_NAME( xname, xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xname), nullptr, (size_t)(xtraits|CRTTI::TRAITS_TYPE), nullptr, xroot::GetReflect()); \
    DEF_TYPE_TRAIT( xroot )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_TYPE( xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xclass), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_TYPE), nullptr, xclass::GetReflect()); \
    DEF_TYPE_TRAIT( xclass )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_TYPE_NAME( xname, xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xname), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_TYPE), nullptr, xclass::GetReflect()); \
    DEF_TYPE_TRAIT( xclass )

///////////////////////////////////////////////////////////////////
// RTTI with default create-object & reflect type helpers
#define DECL_RTTI_CREATE_TYPE( xclass ) \
    DECL_RTTI( xclass ) \
    DECL_CREATE_TRAIT( xclass ) \
    DECL_TYPE_TRAIT( xclass )

///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_TYPE_ROOT( xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xroot), nullptr, (size_t)(xtraits|CRTTI::TRAITS_BUILTIN), &(xroot::CreateByRTTI), xroot::GetReflect()); \
    DEF_TYPE_TRAIT( xroot )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_TYPE_ROOT_NAME( xname, xroot, xtraits ) \
    SELECTANY const CRTTI xroot::ms_RTTI(TF(#xname), nullptr, (size_t)(xtraits|CRTTI::TRAITS_BUILTIN), &(xroot::CreateByRTTI), xroot::GetReflect()); \
    DEF_TYPE_TRAIT( xroot )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_TYPE( xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xclass), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_BUILTIN), &(xclass::CreateByRTTI), xclass::GetReflect()); \
    DEF_TYPE_TRAIT( xclass )
///////////////////////////////////////////////////////////////////
#define DEF_RTTI_CREATE_TYPE_NAME( xname, xclass, xbase, xtraits ) \
    SELECTANY const CRTTI xclass::ms_RTTI(TF(#xname), &(xbase::ms_RTTI), (size_t)(xtraits|CRTTI::TRAITS_BUILTIN), &(xclass::CreateByRTTI), xclass::GetReflect()); \
    DEF_TYPE_TRAIT( xclass )

///////////////////////////////////////////////////////////////////
#include "rtti.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __RTTI_H__
