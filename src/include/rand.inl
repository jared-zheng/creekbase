// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __RAND_INL__
#define __RAND_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CLCGRand
INLINE CLCGRand::CLCGRand(UInt uSeed)
: m_uSeed(uSeed)
{
    m_uSeed = (m_uSeed == 0) ? ((UInt)CPlatform::GetOSRunningTime()) : (m_uSeed);
}

INLINE CLCGRand::~CLCGRand(void)
{
}

INLINE CLCGRand::CLCGRand(const CLCGRand&)
: m_uSeed(0)
{
}

INLINE CLCGRand& CLCGRand::operator=(const CLCGRand&)
{
    return (*this);
}

INLINE void CLCGRand::SetSeed(UInt uSeed)
{
    m_uSeed = uSeed;
}

INLINE Int CLCGRand::Rand(void)
{
    m_uSeed = (m_uSeed * LCGS_RADIXA + LCGS_RADIXB) >> 16;
    return (Int)(m_uSeed & LCGS_MAX);
}

///////////////////////////////////////////////////////////////////
// CMPGRand
INLINE CMPGRand::CMPGRand(UInt uSeed)
: m_uSeed(uSeed)
, m_uM(MPGS_INIT_M)
, m_uA(MPGS_INIT_A)
, m_uB(MPGS_INIT_B)
{
    m_uSeed = (m_uSeed == 0) ? (MPGS_INIT_R) : (m_uSeed);
    CPlatform::TIMEINFO ti = { 0 };
    CPlatform::GetTimeInfo(ti, false);
    PUInt puRadix = reinterpret_cast<PUInt>(&ti);

    UInt uTime = puRadix[3];
    uTime ^= puRadix[2];
    uTime ^= puRadix[1];
    uTime ^= puRadix[0];
    uTime += CPlatform::GetCurrentPId();

    m_uSeed += uTime;
}

INLINE CMPGRand::~CMPGRand(void)
{
}

INLINE CMPGRand::CMPGRand(const CMPGRand&)
: m_uSeed(0)
, m_uM(MPGS_INIT_M)
, m_uA(MPGS_INIT_A)
, m_uB(MPGS_INIT_B)
{
}

INLINE CMPGRand& CMPGRand::operator=(const CMPGRand&)
{
    return (*this);
}

INLINE void CMPGRand::SetSeed(UInt uSeed)
{
    m_uSeed = uSeed;
}

INLINE Int CMPGRand::Rand(void)
{
    m_uM += MPGS_STEP_M;
    if (m_uM >= MPGS_MAX_M)
    {
        m_uM -= MPGS_ZOOM_M;
    }
    m_uA += MPGS_STEP_A;
    if (m_uA >= MPGS_MAX_A)
    {
        m_uA -= MPGS_ZOOM_A;
    }
    m_uB += MPGS_STEP_B;
    if (m_uB >= MPGS_MAX_B)
    {
        m_uB -= MPGS_ZOOM_B;
    }
    m_uSeed = (m_uSeed * m_uM) + m_uA + m_uB;
    return (Int)((m_uSeed >> 16) ^ (m_uSeed & MPGS_RAND_M));
}

#endif // __RAND_INL__
