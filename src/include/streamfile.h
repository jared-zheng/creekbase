// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __STREAM_FILE_H__
#define __STREAM_FILE_H__

#pragma once

#include "stream.h"
#include "handle.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CFileReadStream
// m_stBufBase  m_stBufBase + m_stBufPos     m_stPos
//     V               V                          V
// +---.---------------.--------------------------.---------+
// |   |<-             m_pBuf[...     ]         ->|          |
// |                                                         |
// |<-                m_stSize                             ->|
class CFileReadStream : public CStream
{
public:
    enum READ_BUF
    {
        READ_BUF_MINSIZE = 4096,
        READ_BUF_MAXSIZE = 4096 * 1024,
    };
public:
    CFileReadStream(void);
    virtual ~CFileReadStream(void);

    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;

    virtual void   Close(void) OVERRIDE;

    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_EXIST|FILEF_SHARE_DEFAULT, UInt uAttrs = 0);
    const CFileHandle& GetFileHandle(void) const;
    bool  IsValid(void) const; 
private:
    CFileReadStream(const CFileReadStream&);
    CFileReadStream& operator=(const CFileReadStream&);

    size_t         PreRead(void);
private:
    size_t         m_stSize;
    size_t         m_stPos;
    size_t         m_stBufSize;
    size_t         m_stBufBase;
    size_t         m_stBufPos;
    PByte          m_pBuf;
    CFileHandle    m_hFile;
};

///////////////////////////////////////////////////////////////////
// CFileWriteStream
//                       m_stPos      m_stBufPos
//                         V              V
// +-----------------------+              .
//                         |<-    m_pBuf[...    ]       ->|
class CFileWriteStream : public CStream
{
public:
    enum WRITE_BUF
    {
        WRITE_BUF_MINSIZE = 4096,
        WRITE_BUF_MAXSIZE = 4096 * 1024,
    };
public:
    CFileWriteStream(size_t stBuf = (size_t)WRITE_BUF_MINSIZE);
    virtual ~CFileWriteStream(void);

    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;
    virtual bool   Flush(Int nFlag = FLUSHO_DEFAULT) OVERRIDE;

    virtual void   Close(void) OVERRIDE;
    // pszFile == nullptr and uFlag & FILEF_EXT_TEMP open a temp file in the system temp folder
    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_ALWAYS|FILEF_SHARE_DEFAULT, UInt uAttrs = 0);
    const CFileHandle& GetFileHandle(void) const;
    bool  IsValid(void) const; 
private:
    CFileWriteStream(const CFileWriteStream&);
    CFileWriteStream& operator=(const CFileWriteStream&);
private:
    size_t         m_stPos;
    size_t         m_stBufSize;
    size_t         m_stBufPos;
    PByte          m_pBuf;
    CFileHandle    m_hFile;
};

///////////////////////////////////////////////////////////////////
// CAFileReadStream
class CAFileReadStream : public CStream
{
public:
    CAFileReadStream(void);
    virtual ~CAFileReadStream(void);
    // RET_OKAY or RET_FAIL
    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;

    virtual size_t Size(void) const OVERRIDE;
    // aio seek set AIOATTR offset
    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_CURRENT) OVERRIDE;

    virtual void   Close(void) OVERRIDE;

    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_EXIST|FILEF_SHARE_DEFAULT, UInt uAttrs = 0, AIOHANDLER pHandler = nullptr, Handle hSigEvent = HANDLE_INVALID);
    bool           Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bWaitCompleted = false);
    Int            Result(size_t& stTransferred, bool bWaitCompleted = false, UInt uWait = (UInt)TIMET_INFINITE);
    Int            Cancel(void);

    const AIOATTR&     GetAttr(void) const;
    const CFileHandle& GetFileHandle(void) const;
    bool  IsValid(void) const;
private:
    CAFileReadStream(const CAFileReadStream&);
    CAFileReadStream& operator=(const CAFileReadStream&);
private:
    size_t         m_stSize;
    AIOHANDLER     m_pHandler;
    AIOATTR        m_Attr;
    CFileHandle    m_hFile;
};

///////////////////////////////////////////////////////////////////
// CAFileWriteStream
class CAFileWriteStream : public CStream
{
public:
    CAFileWriteStream(void);
    virtual ~CAFileWriteStream(void);
    // RET_OKAY or RET_FAIL
    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;
    // aio seek set AIOATTR offset
    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_CURRENT) OVERRIDE;
    virtual bool   Flush(Int nFlag = FLUSHO_DEFAULT) OVERRIDE;

    virtual void   Close(void) OVERRIDE;
    // pszFile == nullptr and uFlag & FILEF_EXT_TEMP open a temp file in the system temp folder
    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_ALWAYS|FILEF_SHARE_DEFAULT, UInt uAttrs = 0, AIOHANDLER pHandler = nullptr, Handle hSigEvent = HANDLE_INVALID);
    bool           Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bWaitCompleted = false);
    Int            Result(size_t& stTransferred, bool bWaitCompleted = false, UInt uWait = (UInt)TIMET_INFINITE);
    Int            Cancel(void);

    const AIOATTR&     GetAttr(void) const;
    const CFileHandle& GetFileHandle(void) const;
    bool  IsValid(void) const;
private:
    CAFileWriteStream(const CAFileWriteStream&);
    CAFileWriteStream& operator=(const CAFileWriteStream&);
private:
    AIOHANDLER     m_pHandler;
    AIOATTR        m_Attr;
    CFileHandle    m_hFile;
};

///////////////////////////////////////////////////////////////////
// CMapReadStream
class CMapReadStream : public CStream
{
public:
    // nMode = 0 share memory, nMode = STREAMM_FILE mapping file
    CMapReadStream(Int nMode = STREAMM_FILE);
    virtual ~CMapReadStream(void);

    virtual size_t Read(void* pV, size_t stLenBytes) OVERRIDE;

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;

    virtual void   Close(void) OVERRIDE;

    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_EXIST|FILEF_SHARE_DEFAULT, UInt uAttrs = 0);

    PByte          InitMap(size_t stSize, size_t stOffset = 0, UInt uProtect = 0, UInt uAttrs = 0, PCXStr pszName = nullptr);
    void           ExitMap(void);
    const PByte    GetMap(void) const;
    bool           IsValid(void) const;
private:
    CMapReadStream(const CMapReadStream&);
    CMapReadStream& operator=(const CMapReadStream&);
private:
    size_t           m_stSize;
    size_t           m_stPos;
    PByte            m_pBuf;
    CFileHandle      m_hFile;
    CMappingHandle   m_hMapping;
};

///////////////////////////////////////////////////////////////////
// CMapWriteStream
class CMapWriteStream : public CStream
{
public:
    // nMode = 0 share memory, nMode = STREAMM_FILE mapping file
    CMapWriteStream(Int nMode = STREAMM_FILE);
    virtual ~CMapWriteStream(void);

    virtual size_t Write(const void* pV, size_t stLenBytes) OVERRIDE;

    virtual size_t Tell(void) const OVERRIDE;
    virtual size_t Size(void) const OVERRIDE;
    virtual size_t Rest(void) const OVERRIDE;

    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) OVERRIDE;
    virtual bool   Flush(Int nFlag = FLUSHO_DEFAULT) OVERRIDE;

    virtual void   Close(void) OVERRIDE;
    // pszFile == nullptr and uFlag & FILEF_EXT_TEMP open a temp file in the system temp folder
    UInt           Create(PCXStr pszFile, UInt uFlag = FILEF_OPEN_ALWAYS|FILEF_SHARE_DEFAULT, UInt uAttrs = 0);

    PByte          InitMap(size_t stSize, size_t stOffset = 0, UInt uProtect = 0, UInt uAttrs = 0, PCXStr pszName = nullptr);
    void           ExitMap(void);
    const PByte    GetMap(void) const;
    bool           IsValid(void) const;
private:
    CMapWriteStream(const CMapWriteStream&);
    CMapWriteStream& operator=(const CMapWriteStream&);
private:
    size_t           m_stSize;
    size_t           m_stPos;
    PByte            m_pBuf;
    CFileHandle      m_hFile;
    CMappingHandle   m_hMapping;
};

///////////////////////////////////////////////////////////////////
#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetstreamfile.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetstreamfile.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __STREAM_FILE_H__
