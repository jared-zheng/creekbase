// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_H__
#define __NETWORK_H__

#pragma once

#include "subsystem.h"

///////////////////////////////////////////////////////////////////
#if defined(NETWORK_EXPORT)
    #define NETWORKAPI                C_EXPORT
    #define NETWORKCLASS              CXX_EXPORT
#elif defined(__RUNTIME_STATIC__)
    #define NETWORKAPI
    #define NETWORKCLASS
    DECLARE_STATIC_CREATE_FUNC( NetworkStatic );
#else   // NETWORK_EXPORT
    #define NETWORKAPI                C_IMPORT
    #define NETWORKCLASS              CXX_IMPORT
#endif  // NETWORK_EXPORT

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CNETTraits
class CNETTraits
{
public:
    // common
    // network invoke CEventHandler inst event
    // 1. network socket event : EVENT_TCP_ACCEPT, EVENT_TCP_CONNECT, EVENT_TCP_SEND, EVENT_TCP_CLOSE, EVENT_UDP_SEND, EVENT_UDP_CLOSE
    //    CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    //    utEvent = EVENT_TCP_ACCEPT,     utData = accept socket, ullParam = listen socket
    //    utEvent = EVENT_TCP_CONNECT,    utData = connect error(0 succeeded), ullParam = connect socket
    //    utEvent = EVENT_TCP_SEND,       utData = send queue limit size, ullParam = send socket
    //    utEvent = EVENT_TCP_CLOSE,      utData = close socket, ullParam = close socket live data
    //    utEvent = EVENT_UDP_SEND,       utData = send queue limit size, ullParam = send socket
    //    utEvent = EVENT_UDP_CLOSE,      utData = close socket, ullParam = close socket live data
    // 2. network socket data : EVENT_TCP_RECV, EVENT_UDP_RECV
    //    CEventHandler->OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
    //    utEvent = EVENT_TCP_RECV,       Stream = recv data, ullParam = PTCP_PARAM
    //    utEvent = EVENT_UDP_RECV,       Stream = recv data, ullParam = PUDP_PARAM
    //    [SOCKET_RECV_EVENT]CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    //    utEvent = EVENT_TCP_RECV,       utData = data size, ullParam = PTCP_PARAM
    //    utEvent = EVENT_UDP_RECV,       utData = data size, ullParam = PUDP_PARAM
    // 3. tls event : EVENT_TLS_HANDSHAKE, EVENT_TLS_RECV, EVENT_TLS_SEND, EVENT_TLS_ERROR
    //    CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    //    utEvent = EVENT_TCP_ACCEPT,     utData = accept socket, ullParam = listen socket
    //    utEvent = EVENT_TCP_CONNECT,    utData = connect error(0 succeeded), ullParam = connect socket
    //    utEvent = EVENT_TLS_HANDSHAKE,  utData = error id(0 succeeded), ullParam = handshake socket(if error id != 0, socket will be closed)
    //    utEvent = EVENT_TLS_RECV,       utData = data size, ullParam = PTCP_PARAM(only sSocket, nSize & pData is valid)
    //    utEvent = EVENT_TLS_SEND,       utData = data size, ullParam = PTCP_PARAM(only sSocket, nSize & pData is valid)
    //    utEvent = EVENT_TLS_ERROR,      utData = error id,  ullParam = error socket(socket will be closed)
    //    utEvent = EVENT_TCP_CLOSE,      utData = close socket, ullParam = close socket live data
    // 4. web event : EVENT_WEB_SESSION
    //    CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    //    utEvent = EVENT_TCP_ACCEPT,     utData = accept socket, ullParam = listen socket
    //    utEvent = EVENT_TCP_CONNECT,    utData = connect error(0 succeeded), ullParam = connect socket
    //    utEvent = EVENT_TLS_HANDSHAKE,  utData = error id(0 succeeded), ullParam = handshake socket(HTTPS|WSS only, if error id != 0, socket will be closed)
    //    utEvent = EVENT_BEFORE_UPGRADE, utData = CWEBSession ptr, ullParam = session type(set CWEBSession ptr->SetStatus(SWITCHING_PROTOCOLS) after set http-parser->from(custom CHTTPResponse))
    //    utEvent = EVENT_AFTER_UPGRADE,  utData = CWEBSession ptr, ullParam = session type(set CWEBSession auto-check flag + time, close control frame flag, ping/pong control frame flag)
    //    utEvent = EVENT_WEB_SESSION,    utData = CWEBSession ptr, ullParam = session type
    //    utEvent = EVENT_TLS_ERROR,      utData = error id,  ullParam = error socket(HTTPS|WSS only, socket will be closed)
    //    utEvent = EVENT_TCP_CLOSE,      utData = close socket, ullParam = close socket live data
    // 5. network socket error : EVENT_TCP_UNKNOWN, EVENT_UDP_UNKNOWN
    //    reserved
    enum EVENT_TYPE
    {
        EVENT_NONE            = 0,
        // tcp
        EVENT_TCP_ACCEPT,            // after listen socket accept a connect socket(alias : accept socket), return false close accept socket
        EVENT_TCP_CONNECT,           // after connect socket connect to server(utData include connect error if failed), return false close connect socket
        EVENT_TCP_RECV,              // socket recv data, return false to close socket
        EVENT_TCP_SEND,              // socket send queue data complete, return false to close socket
        EVENT_TCP_CLOSE,             // socket close
        // udp
        EVENT_UDP_RECV,              // socket recv data, return false to discard all recved pack from current remote udp
        EVENT_UDP_SEND,              // socket send queue data complete, return false to close socket
        EVENT_UDP_CLOSE,             // socket close
        // tls
        EVENT_TLS_HANDSHAKE,         // tls handshake result
        EVENT_TLS_RECV,              // tls recv data
        EVENT_TLS_SEND,              // tls send data
        EVENT_TLS_ERROR,             // tls handle error id
        // web
        EVENT_BEFORE_UPGRADE,        // web session before upgrade
        EVENT_AFTER_UPGRADE,         // web session after upgrade
        EVENT_WEB_SESSION,           // web session recv data
        //
        EVENT_TCP_UNKNOWN     = 98,  // tcp unknown data : exception
        EVENT_UDP_UNKNOWN     = 99,  // udp unknown data : exception
        EVENT_RESERVED        = 100, // user defined network event >= EVENT_RESERVED
    };

    enum SEND_FLAG
    {
        SEND_NORMAL           = 0,
        SEND_WAIT,                   // tcp only, send buffer add to send queue, until no WAIT flag to send the queue
        SEND_LIVE,                   // live-data != 0
        SEND_CLOSE,                  // close socket after send okay
        // // ***windows special : RIO tcp not support broadcast
        SEND_BROADCAST        = 16,  // tcp : exclude sender; udp : all
        // tcp only
        SEND_BROADCAST_ALL,          // all accept socket
        SEND_BROADCAST_AS,           // accept socket live-data == ullParam
        SEND_BROADCAST_NOT,          // accept socket live-data != ullParam
        SEND_BROADCAST_OR,           // accept socket (live-data | ullParam) != 0
        SEND_BROADCAST_AND,          // accept socket (live-data & ullParam) != 0
        SEND_BROADCAST_XOR,          // accept socket (live-data ^ ullParam) != 0

        SEND_BROADCAST_GT,           // accept socket  live-data >  ullParam
        SEND_BROADCAST_GTOE,         // accept socket  live-data >= ullParam
        SEND_BROADCAST_LT,           // accept socket  live-data <  ullParam
        SEND_BROADCAST_LTOE,         // accept socket  live-data <= ullParam
    };

    enum ATTR_CONST
    {
        ATTR_NONE             = 0x00000000,
        ATTR_THREAD           = 0x00000001, // work thread count, 1 <--> CPU core * 2, default CPU core

        ATTR_NETORDER         = 0x00000010, // NET_ADDR
        ATTR_IPV4             = 0x00000020,
        ATTR_IPV6             = 0x00000040, // ipv6

        ATTR_NAGLE            = 0x00000400, // enable TCP Nagle algorithm(http://en.wikipedia.org/wiki/Nagle%27s_algorithm)
        ATTR_REUSEADDR        = 0x00000800,

        ATTR_ACK_DETECT       = 0x00010000, // TCP accept socket ack flag, TRUE or FALSE, default[FALSE]
        ATTR_TIMEOUT_DETECT   = 0x00020000, // TCP accept socket timeout flag, TRUE or FALSE, default[FALSE]
        ATTR_ACK_TIME         = 0x00050000, // ack time[MS], default[4000], set ack time also will set ack [TRUE]
        ATTR_TIMEOUT_TIME     = 0x000A0000, // timeout time[MS], default[80000], set timeout time also will set timeout [TRUE]

        // http://en.wikipedia.org/wiki/TCP/IP
        // http://en.wikipedia.org/wiki/Jumbo_frame
        ATTR_MAX_BUFFER       = 0x00100000, // send/recv buffer size, default[1024 - ATTR_DEF_PACK_HEAD = 1016]
        ATTR_MAX_JUMBOBUF     = 0x00200000, // send/recv jumbo buffer size, default[8192 - ATTR_DEF_PACK_HEAD = 8184]

        ATTR_MAX_SEND         = 0x01000000, // max send queue size, default[32 * 1024]
        ATTR_MAX_EVENT        = 0x02000000, // max wait event array
        ATTR_MAX_LARGE        = 0x04000000,
        ATTR_MAX_MASSIVE      = 0x08000000,

        ATTR_PARAM_TCP        = 0x10000000,
        ATTR_PARAM_UDP        = 0x20000000,
    };

    enum ATTR_LIMIT
    {
        ATTR_LMT_MIN_TIME     = 200,
        ATTR_LMT_MAX_TIME     = (60 * 60 * 1000),
        ATTR_LMT_MIN_BUFFER   = 256,
        ATTR_LMT_MAX_BUFFER   = (1024 * 1024),
        ATTR_LMT_MIN_JUMBOBUF = 4096,
        ATTR_LMT_MAX_JUMBOBUF = (16 * 1024 * 1024),
        ATTR_LMT_MIN_QUEUE    = 64,
        ATTR_LMT_MAX_QUEUE    = (128 * 1024),
        ATTR_LMT_MIN_EVENT    = 4,
        ATTR_LMT_MAX_EVENT    = 128,
        ATTR_LMT_MIN_BACKLOG  = 5,
        ATTR_LMT_MAX_BACKLOG  = 255,
    };

    enum ATTR_DEFAULT
    {
        ATTR_DEF_PACK_HEAD    = 8,           // default pack head size
        ATTR_DEF_ACK_TIME     = 4000,        // 200 <--> 60 * 60 * 1000, N * 1000
        ATTR_DEF_TICK_TIME    = 30000,       // 
        ATTR_DEF_TIMEOUT_TIME = 80000,       // 200 <--> 60 * 60 * 1000, N * 1000
        ATTR_DEF_MAX_BUFFER   = 1024,        // (256 <--> 1024 * 1024)
        ATTR_DEF_MAX_JUMBOBUF = 8192,        // (4096 <--> 16 * 1024 * 1024)
        ATTR_DEF_MAX_SEND     = (32 * 1024), // 64 <--> 128 * 1024, send queue
        ATTR_DEF_MAX_EVENT    = 16,          // 4 <--> 512, max wait event size

        ATTR_DEF_BOUND        = 80,
        ATTR_DEF_TIMES        = 2,           // JUMBOBUF >= BUFFER * 2, max work thread CPU core * 2, tick-thread check time * 2
    };

    enum SOCKET_CONST
    {
        SOCKET_LIVEDATA       = 0x00000000,  // socket live data[!!!set after create okay, after EVENT_TCP_ACCEPT handled set ack state accept socket change to live state]
        // tcp
        SOCKET_TCP,                          // socket type flag, TRUE is TCP socket
        SOCKET_TCP_BROADCAST,                // socket broadcast flag, TRUE or FALSE, default[TRUE][!!!tcp set after create okay]
        // udp
        SOCKET_UDP,                          // socket type flag, TRUE is UDP socket
        SOCKET_UDP_BROADCAST,
        // special---SOCKET_RECV_EVENT + SOCKET_EXCLUDE_HEAD
        SOCKET_RAW,                          // raw socket, high 2-bytes is IPPROTO special(as IPPROTO_TCP/IPPROTO_UDP/IPPROTO_ICMPVX)
        SOCKET_RAW_HDRINCL,                  // raw socket with the ip header, high 2-bytes is IPPROTO special(as IPPROTO_TCP/IPPROTO_UDP/IPPROTO_ICMPVX)
        // [linux/unix]NOTE:ipv6---SOCKET_RAW_HDRINCL == SOCKET_PACKET, high 2-bytes is Ethernet Protocol ID

        SOCKET_PACKET,                       // [linux/unix]device level socket, high 2-bytes is Ethernet Protocol ID
        SOCKET_PACKET_HDRINCL,               // [linux/unix]device level socket with the link-level header, high 2-bytes is Ethernet Protocol ID

        SOCKET_CONST_LOW      = 0x0000FFFF,
        SOCKET_CONST_HIGH     = 0xFFFF0000,
        //
        SOCKET_OPTION         = 0x00000040,  // socket option set/get
        //
        // SOCKET_SEND_EVENT, SOCKET_RECV_EVENT, SOCKET_EXCLUDE_HEAD, SOCKET_INCLUDE_HEAD, SOCKET_TCP_LIVEHEAD set to listen socket, all listen's accept sockets derive the attr
        SOCKET_SEND_EVENT,                   // socket send/sendto queue empty event, invoke CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t[send queue limit size], ULLong [send socket])[!!!set after create okay]
        SOCKET_RECV_EVENT,                   // socket recv/recvfrom data event, invoke CEventHandler->OnHandle(uintptr_t utEvent, uintptr_t[data size], ULLong [PTCP_PARAM/PUDP_PARAM])[!!!set after create okay]
        //
        SOCKET_EXCLUDE_HEAD,                 // socket send/sendto exclude pack-build head-data, when recv/recvfrom data event, CEventHandler must check pack complete size[!!!set after create okay]
        SOCKET_INCLUDE_HEAD,                 // socket recv/recvfrom data event include pack-build head-data, SOCKET_EXCLUDE_HEAD recv/recvfrom data already include(set SOCKET_EXCLUDE_HEAD do not need set SOCKET_INCLUDE_HEAD)[!!!set after create okay]
        //
        SOCKET_LIVE_HEAD,                    // keep live by recv pack-build head-data only[!!!set after create okay]
        //
        SOCKET_TCP_KEEPALIVE,                // tcp keepalive, TRUE/FALSE[!!!tcp set after create okay]
        SOCKET_TCP_ALIVETIME,                // tcp keepalive time[MS], time > 0 set keepalive true, interval = time * 0.45[!!!tcp set after create okay]
        //
        SOCKET_SEND_TIMEOUT,                 // socket send timeout[MS][!!!set after create okay]
        SOCKET_RECV_TIMEOUT,                 // socket recv timeout[MS][!!!set after create okay]
        //
        SOCKET_UDP_TIMEOUT,                  // udp socket timeout check[!!!udp set after create okay]
        SOCKET_CONNECT_TIMEOUT,              // tcp connect socket timeout check[!!!tcp set after create okay]
        //
        SOCKET_MANUAL_TIMEOUT,               // set timeout manual add value[!!!set after create okay]
        SOCKET_ACK_TIMEOUT,                  // set live socket to ack state, clean SOCKET_MANUAL_TIMEOUT[!!!set after live socket]
    };

    typedef uintptr_t   Socket;

    typedef struct tagNET_ATTR
    {
    public:
        tagNET_ATTR(void)  { MM_SAFE::Set(this, 0, sizeof(tagNET_ATTR)); }
        ~tagNET_ATTR(void) { }
    public:
        Int      nAttrs;
        Int      nThread;
        Int      nAckTime;      // during ack time, use SetAttr[SOCKET_LIVEDATA] to accept as live socket
        Int      nTimeout;
        Int      nMaxJumbo;     // valid jumbo size  = nMaxJumbo  - pack head size[default is ATTR_DEF_PACK_HEAD], nMaxJumbo >= nMaxBuffer * ATTR_DEF_TIMES
        Int      nMaxBuffer;    // valid buffer size = nMaxBuffer - pack head size[default is ATTR_DEF_PACK_HEAD]
        Int      nMaxSend;
        Int      nMaxEvent;
        // out param
        Int      nBufferSize;
        Int      nJumboSize;
        Int      nBufferOffset;
        Int      nJumboOffset;
        PINDEX   inxBuffer;
        PINDEX   inxJumbo;
    }NET_ATTR, *PNET_ATTR;

    typedef struct tagNET_DUMP
    {
    public:
        tagNET_DUMP(void)  { Reset(); }
        ~tagNET_DUMP(void) { }
        void Reset(void)   { MM_SAFE::Set(this, 0, sizeof(tagNET_DUMP)); }
    public:
        LLong   llTCPConnectCount;
        LLong   llTCPCloseCount;
        LLong   llUDPCreateCount;
        LLong   llUDPCloseCount;
        LLong   llSendSize;
        LLong   llRecvSize;
        LLong   llSendTick; // CPU tick - MS : (itSendTick * TIMET_S2MS) / CPlatform::GetOSBaseTick();
        LLong   llRecvTick; // CPU tick - MS : (itRecvTick * TIMET_S2MS) / CPlatform::GetOSBaseTick();
    }NET_DUMP, *PNET_DUMP;

    struct tagSTR_ADDR : public MObject
    {
    public:
        tagSTR_ADDR(void)
        : usAttr(CNETTraits::ATTR_NONE)
        , usPort(0)
        {
        }

        tagSTR_ADDR(PCXStr pszAddr, UShort usSetPort, UShort usSetAttr = CNETTraits::ATTR_IPV6)
        : usAttr(usSetAttr)
        , usPort(usSetPort)
        , strIp(pszAddr)
        {
        }

        tagSTR_ADDR(UShort usSetPort, UShort usSetAttr = CNETTraits::ATTR_IPV6)
        : usAttr(usSetAttr)
        , usPort(usSetPort)
        {
        }

        ~tagSTR_ADDR(void)
        {
        }

        tagSTR_ADDR(const tagSTR_ADDR& aSrc)
        : usAttr(aSrc.usAttr)
        , usPort(aSrc.usPort)
        , strIp(aSrc.strIp)
        {
        }

        tagSTR_ADDR& operator=(const tagSTR_ADDR& aSrc)
        {
            if (&aSrc != this)
            {
                usAttr = aSrc.usAttr;
                usPort = aSrc.usPort;
                strIp  = aSrc.strIp;
            }
            return (*this);
        }
#ifndef __MODERN_CXX_NOT_SUPPORTED
        tagSTR_ADDR(tagSTR_ADDR&& aSrc)
        : usAttr(aSrc.usAttr)
        , usPort(aSrc.usPort)
        , strIp(std::move(aSrc.strIp))
        {

        }

        tagSTR_ADDR& operator=(tagSTR_ADDR&& aSrc)
        {
            if (&aSrc != this)
            {
                usAttr = aSrc.usAttr;
                usPort = aSrc.usPort;
                strIp  = std::move(aSrc.strIp);
            }
            return (*this);
        }
#endif
        void Reset(PCXStr pszIp = nullptr, UShort usSetPort = 0, UShort usSetAttr = CNETTraits::ATTR_NONE)
        {
            usAttr = usSetAttr;
            usPort = usSetPort;
            strIp  = pszIp;
        }

        void Serialize(CStream& Stream)
        {
            if (Stream.IsRead())
            {
                Stream >> usAttr;
                if (IsValid())
                {
                    Stream >> usPort;
                    Stream >> strIp;
                }
            }
            else
            {
                Stream << usAttr;
                if (IsValid())
                {
                    Stream << usPort;
                    Stream << strIp;
                }
            }
        }

        size_t Length(void) const
        {
            if (IsValid())
            { 
                return (sizeof(UShort) + sizeof(UShort) + strIp.Length(true));
            }
            return (sizeof(UShort));
        }

        bool IsValid(void) const
        {
            return (usAttr > ATTR_NETORDER);
        }
    public:
        UShort    usAttr;
        UShort    usPort;
        CString   strIp;
    };
    typedef struct tagSTR_ADDR   STR_ADDR, *PSTR_ADDR;

    struct tagNET_ADDR : public MObject
    {
    public:
        enum LEN_LIMIT
        {
            LEN_ULLONG = 2,
            LEN_UINT   = 4,
            LEN_USHORT = 8,
            LEN_BYTE   = 16,
        };

        typedef union tagADDR
        {
            ULLong   ullAddr[LEN_ULLONG];
            UInt     uAddr[LEN_UINT];
            UShort   usAddr[LEN_USHORT];
            Byte     bAddr[LEN_BYTE];
        }ADDR;
    public:
        class CElementTraits
        {
        public:
            typedef const tagNET_ADDR& INARGTYPE;
            typedef tagNET_ADDR&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
            typedef tagNET_ADDR&& RVARGTYPE;
#endif
        public:
            static void CopyElements(tagNET_ADDR* pDst, const tagNET_ADDR* pSrc, size_t stElements)
            {
                for (size_t i = 0; i < stElements; ++i)
                {
                    pDst[i] = pSrc[i];
                }
            }

            static void RelocateElements(tagNET_ADDR* pDst, tagNET_ADDR* pSrc, size_t stElements)
            {
                MM_SAFE::Mov(pDst, stElements * sizeof(tagNET_ADDR), pSrc, stElements * sizeof(tagNET_ADDR));
            }

            static bool CompareElements(INARGTYPE t1, INARGTYPE t2)
            {
                if ((t1.usAttr == t2.usAttr) && (t1.usPort == t2.usPort))
                {
                    return ((t1.Addr.ullAddr[1] == t2.Addr.ullAddr[1]) &&
                            (t1.Addr.ullAddr[0] == t2.Addr.ullAddr[0]));
                }
                return false;
            }

            static Int CompareElementsOrdered(INARGTYPE t1, INARGTYPE t2)
            {
                if (t1.usAttr == t2.usAttr)
                {
                    if (t1.usPort == t2.usPort)
                    {
                        return ((t1.Addr.ullAddr[1] == t2.Addr.ullAddr[1]) &&
                                (t1.Addr.ullAddr[0] == t2.Addr.ullAddr[0]));
                    }
                    return (Int)(t1.usPort - t2.usPort);
                }
                return ((t1.usAttr > t2.usAttr) ? (Int)(1) : (Int)(-1));
            }

            static size_t HashElements(INARGTYPE t)
            {
                return (CHash::Hash((PByte)(&t), sizeof(tagNET_ADDR)));
            }
        };
    public:
        tagNET_ADDR(void)  { Reset(); }
        ~tagNET_ADDR(void) { }
        void Reset(void)   { MM_SAFE::Set(this, 0, sizeof(tagNET_ADDR)); }
        void Serialize(CStream& Stream)
        {
            if (Stream.IsRead())
            {
                Stream >> usAttr;
                if (IsValid())
                {
                    Stream >> usPort;
                    if (usAttr & ATTR_IPV6)
                    {
                        Stream.Read(Addr.bAddr, LEN_BYTE);
                    }
                    else // ATTR_IPV4
                    {
                        Stream >> Addr.uAddr[0];
                    }
                }
            }
            else
            {
                Stream << usAttr;
                if (IsValid())
                {
                    Stream << usPort;
                    if (usAttr & ATTR_IPV6)
                    {
                        Stream.Write(Addr.bAddr, LEN_BYTE);
                    }
                    else // ATTR_IPV4
                    {
                        Stream << Addr.uAddr[0];
                    }
                }
            }
        }

        size_t Length(void) const
        {
            if (IsValid())
            { 
                if (usAttr & ATTR_IPV6)
                {
                    return (sizeof(UShort) + sizeof(UShort) + LEN_BYTE);
                }
                else // ATTR_IPV4
                {
                    return (sizeof(UShort) + sizeof(UShort) + LEN_UINT);
                }
            }
            return (sizeof(UShort));
        }

        bool IsValid(void) const
        {
            return (usAttr > ATTR_NETORDER);
        }
    public:
        UShort   usAttr; // ATTR_IPV6 ATTR_IPV4 ATTR_NETORDER
        UShort   usPort; // host bits; if usAttr & ATTR_NETORDER, net bits
        ADDR     Addr;
    };
    typedef struct tagNET_ADDR NET_ADDR, *PNET_ADDR;

    typedef NET_ADDR::CElementTraits CNetAddrRTraits;

    typedef struct tagNET_PARAM
    {
    public:
        tagNET_PARAM(void)
        : sSocket(0)
        , index(nullptr)
        , pCache(nullptr)
        , pData(nullptr)
        , nCache(0)
        , nSize(0)
        , nOffset(0)
        , nAttr(0)
        {
        }

        ~tagNET_PARAM(void)
        {
        }

        void Reset(void)
        {
            MM_SAFE::Set(this, 0, sizeof(tagNET_PARAM));
        }

        bool IsCache(void) const
        {
            return (pCache != nullptr);
        }

        bool IsHandOver(void) const
        {
            if (IsCache())
            {
                ((pCache + nOffset) == pData);
            }
            return false;
        }

        bool IsTCP(void) const
        {
            return ((nAttr & ATTR_PARAM_TCP) != 0);
        }

        bool IsUDP(void) const
        {
            return ((nAttr & ATTR_PARAM_UDP) != 0);
        }

        bool IsBuffer(void) const
        {
            return ((nAttr & ATTR_MAX_BUFFER) != 0);
        }

        bool IsJumbo(void) const
        {
            return ((nAttr & ATTR_MAX_JUMBOBUF) != 0);
        }
    public:
        Socket   sSocket;
        PINDEX   index;   // cache index
        PByte    pCache;  // cache ptr
        PByte    pData;   // data  ptr
        Int      nCache;  // cache size
        Int      nSize;   // data  size
        Int      nOffset; // offset size from pCache to read/write buffer
        Int      nAttr;   // ATTR_PARAM_TCP/ATTR_PARAM_UDP | ATTR_MAX_BUFFER/ATTR_MAX_JUMBOBUF
    }NET_PARAM, *PNET_PARAM;

    typedef tagNET_PARAM TCP_PARAM, *PTCP_PARAM;

    struct tagADDR_PARAM : public tagNET_PARAM
    {
        NET_ADDR   NetAddr;
    };
    typedef tagADDR_PARAM ADDR_PARAM, *PADDR_PARAM;
    typedef tagADDR_PARAM UDP_PARAM,  *PUDP_PARAM;
    // common
    typedef CTScopePtr<CBufStreamBase>   CStreamScopePtr;
    typedef CTArray<CString>             ARY_STRING;
    typedef CTArray<STR_ADDR>            ARY_STRADDR;
    typedef CTArray<NET_ADDR>            ARY_NETADDR;
    ///////////////////////////////////////////////////////////////////
    // CNETPackBuild
    class NOVTABLE CNETPackBuild ABSTRACT: public CTRefCount<CNETPackBuild>
    {
    public:
        // send package head ptr, package data size
        // return package all size, include package head
        virtual size_t Build(PByte pPack, size_t stSize) PURE;
        // check recv package valid check
        // return package all size, include package head
        // return 0 when check fail
        virtual size_t Check(PByte pPack, size_t stSize) PURE;
        // package head size
        virtual size_t HeadSize(void) const PURE;
        // package offset size
        virtual size_t OffsetSize(void) const { return HeadSize(); }
    protected:
        CNETPackBuild(void) { }
        virtual ~CNETPackBuild(void) { }
    private:
        CNETPackBuild(const CNETPackBuild& aSrc) : CTRefCount<CNETPackBuild>(aSrc) { }
        CNETPackBuild& operator=(const CNETPackBuild& aSrc) { if (this != &aSrc) { CTRefCount<CNETPackBuild>::operator=(aSrc); } return (*this); }
    };
    typedef CTRefCountPtr<CNETPackBuild> CNETPackBuildPtr;
public:
    static UInt MakeCreateFlag(UInt uProtocol, UInt uTraits)
    {
        return ((UInt)((uProtocol << 16) & SOCKET_CONST_HIGH) + (UInt)(uTraits & SOCKET_CONST_LOW));
    }

    static UInt GetCreateTraits(UInt uFlag)
    {
        return (UInt)(uFlag & SOCKET_CONST_LOW);
    }

    static UInt GetCreateProtocol(UInt uFlag)
    {
        return (UInt)((uFlag >> 16) & SOCKET_CONST_LOW);
    }
};

template <>
class CTElementTraits<CNETTraits::NET_ADDR> : public CNETTraits::CNetAddrRTraits { };

///////////////////////////////////////////////////////////////////
// CNetwork
DECLARE_UUID( CNetwork, {312212B3-9B2F-476E-8072-637BA9BA542A} )
class NOVTABLE CNetwork ABSTRACT : public CComponent, public CNETTraits
{
public:
    // ***pEventHandler or EventHandlerRef AddRef when a new socket inst is created***
    // ***pEventHandler or EventHandlerRef Release when a socket inst is destroyed***
    // ***to prevent pEventHandler or EventHandlerRef inst MDELETE when a socket inst is destroyed, invoke AddRef before***
    virtual UInt   Init(const NET_ATTR& Attr, CEventHandler* pEventHandler = nullptr, CNETPackBuild* pPackBuild = nullptr) PURE;
    virtual void   Exit(void) PURE;

    // uFlag : SOCKET_TCP            - unicast TCP socket
    //         SOCKET_TCP_BROADCAST  - broadcast TCP socket
    //         SOCKET_UDP            - unicast UDP socket
    //         SOCKET_UDP_BROADCAST  - broadcast UDP socket
    //         SOCKET_RAW            - raw socket, high 2-bytes is IPPROTO special(as IPPROTO_TCP/IPPROTO_UDP/IPPROTO_ICMPVX, 0---IPPROTO_ICMPV6/IPPROTO_ICMP)
    //         SOCKET_RAW_HDRINCL    - raw socket with the ip header, high 2-bytes is IPPROTO special(as IPPROTO_TCP/IPPROTO_UDP/IPPROTO_ICMPVX, 0---IPPROTO_ICMPV6/IPPROTO_ICMP)
    //         SOCKET_PACKET         - [linux/unix]device level socket, high 2-bytes is Ethernet Protocol ID
    //         SOCKET_PACKET_HDRINCL - [linux/unix]device level socket with the link-level header, high 2-bytes is Ethernet Protocol ID
    // uFlag high-2 byte : protocol
    virtual Socket Create(UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr, UInt uFlag = SOCKET_TCP_BROADCAST) PURE;
    virtual Socket Create(CEventHandler& EventHandlerRef, UShort usLocalPort = 0, PCXStr pszLocalAddr = nullptr, UInt uFlag = SOCKET_TCP_BROADCAST) PURE;
    virtual Socket Create(const NET_ADDR& NetAddrRef, UInt uFlag = SOCKET_TCP_BROADCAST) PURE; // NetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual Socket Create(CEventHandler& EventHandlerRef, const NET_ADDR& NetAddrRef, UInt uFlag = SOCKET_TCP_BROADCAST) PURE; // NetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   Destroy(Socket sSocket) PURE;
    // TCP socket only
    // To understand the nAccept argument, we must realize that for a given listening socket, the kernel maintains two queues
    // 1.An incomplete connection queue, which contains an entry for each SYN that has arrived from a client for which the server is awaiting completion of the TCP three-way handshake.
    //   These sockets are in the SYN_RCVD state .
    // 2.A completed connection queue, which contains an entry for each client with whom the TCP three-way handshake has completed. These sockets are in the ESTABLISHED state
    //
    // The nBlock argument to the listen function has historically specified the maximum value for the sum of both queues.
    // Berkeley-derived implementations add a fudge factor to the backlog: It is multiplied by 1.5, and the default value is 5
    //
    // When a SYN arrives from a client, TCP creates a new entry on the incomplete queue and then responds with the second segment of the three-way handshake:
    // the server's SYN with an ACK of the client's SYN (Section 2.6).
    // This entry will remain on the incomplete queue until the third segment of the three-way handshake arrives (the client's ACK of the server's SYN),
    // or until the entry times out. (Berkeley-derived implementations have a timeout of 75 seconds for these incomplete entries.)
    //
    // If the queues are full when a client SYN arrives, TCP ignores the arriving SYN (pp. 930-931 of TCPv2);
    // it does not send an RST. This is because the condition is considered temporary, and the client TCP will retransmit its SYN,
    // hopefully finding room on the queue in the near future. If the server TCP immediately responded with an RST,
    // the client's connect would return an error, forcing the application to handle this condition instead of letting TCP's normal retransmission take over.
    // Also, the client could not differentiate between an RST in response to a SYN meaning "there is no server at this port" versus "there is a server at this port but its queues are full."
    // THREE-WAY HANDSHAKE : see wiki's[tcp option.txt]
    virtual bool   Listen(Socket sSocket, Int nAccept = ATTR_LMT_MIN_BACKLOG) PURE;
    virtual bool   Connect(Socket sSocket, UShort usRemotePort, PCXStr pszRemoteAddr) PURE;
    virtual bool   Connect(Socket sSocket, const NET_ADDR& RemoteNetAddrRef) PURE; // RemoteNetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    // Stream must alloc from AllocBuffer or AllocJumboBuffer
    // send & sendto return false indicate send queue is full, otherwise error will close socket
    // TCP
    virtual bool   Send(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) PURE;
    virtual bool   Send(const CTArray<Socket>& aSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) PURE;
    virtual bool   SendCopy(Socket sSocket, CStreamScopePtr& StreamPtr, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) PURE;

    virtual bool   Send(Socket sSocket, CEventBase& EventRef, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) PURE;
    virtual bool   Send(const CTArray<Socket>& aSocket, CEventBase& EventRef, Int nFlag = SEND_NORMAL, ULLong ullParam = 0) PURE;
    // UDP
    virtual bool   SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) PURE;
    virtual bool   SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const ARY_STRADDR& aRemoteStrAddr, Int nFlag = SEND_NORMAL) PURE; // aRemoteStrAddr[N].usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) PURE;

    virtual bool   SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) PURE;     // RemoteNetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   SendTo(Socket sSocket, CStreamScopePtr& StreamPtr, const ARY_NETADDR& aRemoteNetAddr, Int nFlag = SEND_NORMAL) PURE;    // aRemoteNetAddr[N].usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   SendToCopy(Socket sSocket, CStreamScopePtr& StreamPtr, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) PURE; // RemoteNetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]

    virtual bool   SendTo(Socket sSocket, CEventBase& EventRef, PCXStr pszRemoteAddr, UShort usRemotePort, Int nFlag = SEND_NORMAL) PURE;
    virtual bool   SendTo(Socket sSocket, CEventBase& EventRef, const ARY_STRADDR& aRemoteStrAddr, Int nFlag = SEND_NORMAL) PURE; // aRemoteStrAddr[N].usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   SendTo(Socket sSocket, CEventBase& EventRef, const NET_ADDR& RemoteNetAddrRef, Int nFlag = SEND_NORMAL) PURE;  // RemoteNetAddrRef.usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    virtual bool   SendTo(Socket sSocket, CEventBase& EventRef, const ARY_NETADDR& aRemoteNetAddr, Int nFlag = SEND_NORMAL) PURE; // aRemoteNetAddr[N].usAttr.ATTR_IPVX==Init(Attr.nAttrs.ATTR_IPVX)[|ATTR_NETORDER]
    // TCP : listen can not get remote addr, connect can not get remote addr until connect okay
    // UDP : get local addr only
    virtual bool   GetAddr(Socket sSocket, CString& strAddr, UShort& usPort, bool bRemote = true) PURE;
    virtual bool   GetAddr(Socket sSocket, NET_ADDR& NetAddrRef, bool bRemote = true) PURE; // ATTR_NETORDER

    virtual bool   GetLocalAddr(ARY_STRING& strAddrs, Int nAttr = ATTR_IPV6) const PURE;  // nAttr=ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]
    virtual bool   GetLocalAddr(ARY_NETADDR& NetAddrs, Int nAttr = ATTR_IPV6) const PURE; // nAttr=ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]
    virtual bool   GetRemoteAddr(PCXStr pszRemoteAddr, ARY_STRING& strAddrs, Int nAttr = ATTR_IPV6) const PURE;  // nAttr=ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]
    virtual bool   GetRemoteAddr(PCXStr pszRemoteAddr, ARY_NETADDR& NetAddrs, Int nAttr = ATTR_IPV6) const PURE; // nAttr=ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]
    virtual bool   TranslateAddr(CString& strAddr, UShort& usPort, NET_ADDR& NetAddrRef, bool bString2Addr = true) const PURE; // NetAddrRef.usAttr==ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]
    virtual bool   TranslateAddr(PCXStr pszAddr, UShort usPort, NET_ADDR& NetAddrRef) const PURE; // NetAddrRef.usAttr==ATTR_IPV6/ATTR_IPV4[|ATTR_NETORDER]

    // uAttr : SOCKET_LIVEDATA        - TCP & UDP socket get/set live-data
    //         SOCKET_TCP             - TCP socket return TRUE, else FALSE
    //         SOCKET_TCP_BROADCAST   - TCP get/set broadcast flag
    //         SOCKET_UDP             - UDP socket return TRUE, else FALSE
    //         SOCKET_UDP_BROADCAST   - UDP get TRUE if broadcast
    //
    //         SOCKET_SEND_EVENT      - socket send/sendto complete event
    //         SOCKET_RECV_EVENT      - socket recv/recvfrom data event
    //         SOCKET_EXCLUDE_HEAD    - socket send/sendto/recv/recvfrom exclude head-size
    //         SOCKET_INCLUDE_HEAD    - recv/recvfrom include head-size
    //
    //         SOCKET_LIVE_HEAD       - keep live by recv pack-build head-data only
    //
    //         SOCKET_TCP_KEEPALIVE   - tcp keepalive, TRUE/FALSE
    //         SOCKET_TCP_ALIVETIME   - tcp keepalive time[MS], time > 0 set keepalive true, interval = time * 0.45
    //
    //         SOCKET_SEND_TIMEOUT    - socket send timeout[MS]
    //         SOCKET_RECV_TIMEOUT    - socket recv timeout[MS]
    //
    //         SOCKET_UDP_TIMEOUT     - udp timeout check
    //         SOCKET_CONNECT_TIMEOUT - connect tcp timeout check
    //         SOCKET_MANUAL_TIMEOUT  - set timeout manual add value
    //         SOCKET_ACK_TIMEOUT     - set live socket to ack state
    virtual ULLong GetAttr(Socket sSocket, UInt uAttr = SOCKET_LIVEDATA) PURE;
    virtual bool   SetAttr(Socket sSocket, ULLong ullData, UInt uAttr = SOCKET_LIVEDATA) PURE;
    // ***recommend use this***
    // pCache from TCP_PARAM.pCache or UDP_PARAM.pCache
    // index  from TCP_PARAM.index  or UDP_PARAM.index
    virtual bool   AllocBuffer(NET_PARAM& NetParam, bool bJumbo = false) const PURE;
    virtual bool   AllocBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache = nullptr, size_t stOffset = 0) const PURE;
    virtual bool   AllocJumboBuffer(CStreamScopePtr& StreamScopePtrRef, PByte pCache = nullptr, size_t stOffset = 0) const PURE;
    virtual bool   ReuseBuffer(CStreamScopePtr& StreamScopePtrRef, PINDEX index, PByte pCache, size_t stOffset = 0) const PURE;
    // CStream's Refer
    virtual bool   ReferBuffer(CStreamScopePtr& StreamScopePtrRef, CStreamScopePtr& StreamPtr) const PURE;

    virtual bool   Check(UInt uTimeout) PURE; // MS

    virtual void   Dump(NET_DUMP& Dump) const PURE;
    virtual void   Attr(NET_ATTR& Attr) const PURE;
};
typedef CTRefCountPtr<CNetwork> CNetworkPtr;

///////////////////////////////////////////////////////////////////
// CNetworkSystem : CSubSystem
DECLARE_UUID( CNetworkSystem, {8AC3DF4C-0EFC-4A65-833E-6FE0ECD96190} )

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __NETWORK_H__
