// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TSTRING_H__
#define __TSTRING_H__

#pragma once

#include "traits.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
template <typename T>
class CTStringRef;

template <typename T, size_t stFix>
class CTStringFix;

template <typename T>
class CTString;

///////////////////////////////////////////////////////////////////
// CTStringRef
template <typename T>
class CTStringRef : public T
{
public:
    typedef typename T::TChar     TChar;
    typedef typename T::PTStr     PTStr;
    typedef typename T::PCTStr    PCTStr;
    typedef typename T::CPCTStr   CPCTStr;
public:
    class CElementTraits
    {
    public:
        typedef const CTStringRef& INARGTYPE;
        typedef CTStringRef&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
        typedef CTStringRef&& RVARGTYPE;
#endif
    public:
        static void CopyElements(CTStringRef* pDst, const CTStringRef* pSrc, size_t stElements)
        {
            for (size_t i = 0; i < stElements; ++i)
            {
                pDst[i] = pSrc[i];
            }
        }

        static void RelocateElements(CTStringRef* pDst, CTStringRef* pSrc, size_t stElements)
        {
            MM_SAFE::Mov(pDst, stElements * sizeof(CTStringRef), pSrc, stElements * sizeof(CTStringRef));
        }

        static bool CompareElements(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return (T::Cmpn(t1.m_pszRef, t2.m_pszRef, t1.m_stLen) == 0);
            }
            return false;
        }

        static Int CompareElementsOrdered(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return T::Cmpn(t1.m_pszRef, t2.m_pszRef, t1.m_stLen);
            }
            return (Int)((intptr_t)t1.m_stLen - (intptr_t)t2.m_stLen);
        }

        static size_t HashElements(INARGTYPE t)
        {
            return (CHash::Hash(t.m_pszRef, t.m_stLen));
        }
    };
public:
    CTStringRef(PCTStr psz = nullptr);
    CTStringRef(PCTStr psz, size_t stLen);
    ~CTStringRef(void);

#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTStringRef(CTStringRef&& aSrc);
    CTStringRef& operator=(CTStringRef&& aSrc);
#endif

    CTStringRef(const CTStringRef& aSrc);
    template <size_t stLenT> CTStringRef(const CTStringFix<T, stLenT>& aSrc);
    CTStringRef(const CTString<T>& aSrc);

    CTStringRef& operator=(PCTStr psz);
    CTStringRef& operator=(const CTStringRef& aSrc);
    template <size_t stLenT> CTStringRef& operator=(const CTStringFix<T, stLenT>& aSrc);
    CTStringRef& operator=(const CTString<T>& aSrc);

    bool      operator<=(PCTStr psz) const;
    bool      operator<(PCTStr psz) const;
    bool      operator>=(PCTStr psz) const;
    bool      operator>(PCTStr psz) const;
    bool      operator==(PCTStr psz) const;
    bool      operator!=(PCTStr psz) const;

    bool      operator<=(const CTStringRef<T>& aSrc) const;
    bool      operator<(const CTStringRef<T>& aSrc) const;
    bool      operator>=(const CTStringRef<T>& aSrc) const;
    bool      operator>(const CTStringRef<T>& aSrc) const;
    bool      operator==(const CTStringRef<T>& aSrc) const;
    bool      operator!=(const CTStringRef<T>& aSrc) const;

    template <size_t stLenT> bool operator<=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator<(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator==(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator!=(const CTStringFix<T, stLenT>& aSrc) const;

    bool      operator<=(const CTString<T>& aSrc) const;
    bool      operator<(const CTString<T>& aSrc) const;
    bool      operator>=(const CTString<T>& aSrc) const;
    bool      operator>(const CTString<T>& aSrc) const;
    bool      operator==(const CTString<T>& aSrc) const;
    bool      operator!=(const CTString<T>& aSrc) const;

    TChar        operator[](size_t stIndex);
    const TChar  operator[](size_t stIndex) const;

    PTStr        operator*(void);
    PCTStr       operator*(void) const;

    PCTStr       GetBuffer(size_t stIndex = 0) const;
    TChar        GetAt(size_t stIndex);
    const TChar  GetAt(size_t stIndex) const;

    bool         SetBufferLength(PCTStr psz, size_t stLen);

    bool         IsEmpty(void) const;
    void         Empty(void);
    size_t       ResetLength(size_t stLen);
    size_t       Length(bool bStream = false) const;
    // string-ref just save the start address and length(maybe not indicate terminate \0),
    // the next maybe not correct, because string is not be truncated by length
    Int          Find(TChar ch, size_t stStart = 0, bool bRev = false) const;
    Int          Find(PCTStr pszSub, size_t stStart = 0) const;
    Int          OneOf(PCTStr pszMatch, size_t stStart = 0, bool bInMatch = true) const;
    Int          Cmp(PCTStr psz) const;
    Int          Cmpi(PCTStr psz) const;
    Int          Cmpn(PCTStr psz, size_t stLen = 0) const;
    Int          Cmpin(PCTStr psz, size_t stLen = 0) const;
    // compare strings using locale-specific information
    Int          Coll(PCTStr pszCmp) const;
    Int          Colli(PCTStr pszCmp) const;
    Int          Colln(PCTStr pszCmp, size_t stLen = 0) const;
    Int          Collin(PCTStr pszCmp, size_t stLen = 0) const;
public:
    // Character Classification
    bool         IsAlnumChar(size_t stIndex) const;
    bool         IsAlphaChar(size_t stIndex) const;
    bool         IsPrintChar(size_t stIndex) const;
    bool         IsGraphChar(size_t stIndex) const;
    bool         IsDigitChar(size_t stIndex) const;
    bool         IsXDigitChar(size_t stIndex) const;
    bool         IsSpaceChar(size_t stIndex) const;
    bool         IsLowerChar(size_t stIndex) const;
    bool         IsUpperChar(size_t stIndex) const;
    // string-ref just save the start address and length(maybe not indicate terminate \0),
    // the next maybe not correct, because string is not be truncated by length
    Int          ToInt(void) const;
    Long         ToLong(Int nRadix = RADIXT_DEC) const;
    ULong        ToULong(Int nRadix = RADIXT_DEC) const;
    LLong        ToLLong(Int nRadix = RADIXT_DEC) const;
    ULLong       ToULLong(Int nRadix = RADIXT_DEC) const;
    Double       ToDouble(void) const;
    // stIndex set to -1 if can not find end, else return end index
    Long         ToLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULong        ToULong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    LLong        ToLLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULLong       ToULLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    Double       ToDouble(size_t& stIndex) const;
private:
    size_t       m_stLen;
    PCTStr       m_pszRef;
};

template <typename T>
class CTElementTraits<CTStringRef<T> > : public CTStringRef<T>::CElementTraits { };

///////////////////////////////////////////////////////////////////
// CTStringFix
template <typename T, size_t stFix>
class CTStringFix : public T
{
public:
    typedef typename T::TChar     TChar;
    typedef typename T::PTStr     PTStr;
    typedef typename T::PCTStr    PCTStr;
    typedef typename T::CPCTStr   CPCTStr;
public:
    class CElementTraits
    {
    public:
        typedef const CTStringFix& INARGTYPE;
        typedef CTStringFix&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
        typedef CTStringFix&& RVARGTYPE;
#endif
    public:
        static void CopyElements(CTStringFix* pDst, const CTStringFix* pSrc, size_t stElements)
        {
            for (size_t i = 0; i < stElements; ++i)
            {
                pDst[i] = pSrc[i];
            }
        }

        static void RelocateElements(CTStringFix* pDst, CTStringFix* pSrc, size_t stElements)
        {
            MM_SAFE::Mov(pDst, stElements * sizeof(CTStringFix), pSrc, stElements * sizeof(CTStringFix));
        }

        static bool CompareElements(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return (T::Cmpn(t1.m_szBuffer, t2.m_szBuffer, t1.m_stLen) == 0);
            }
            return false;
        }

        static Int CompareElementsOrdered(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return T::Cmpn(t1.m_szBuffer, t2.m_szBuffer, t1.m_stLen);
            }
            return (Int)((intptr_t)t1.m_stLen - (intptr_t)t2.m_stLen);
        }

        static size_t HashElements(INARGTYPE t)
        {
            return (CHash::Hash(t.m_szBuffer, t.m_stLen));
        }
    };
public:
    CTStringFix(PCTStr psz = nullptr, size_t stLen = 0);
    ~CTStringFix(void);

#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTStringFix(CTStringFix&& aSrc);
    CTStringFix& operator=(CTStringFix&& aSrc);
#endif

    CTStringFix(const CTStringFix& aSrc);
    template <size_t stLenT> CTStringFix(const CTStringFix<T, stLenT>& aSrc);
    CTStringFix(const CTStringRef<T>& aSrc);
    CTStringFix(const CTString<T>& aSrc);

    CTStringFix& operator=(PCTStr psz);
    CTStringFix& operator=(const CTStringFix& aSrc);
    template <size_t stLenT> CTStringFix& operator=(const CTStringFix<T, stLenT>& aSrc);
    CTStringFix& operator=(const CTStringRef<T>& aSrc);
    CTStringFix& operator=(const CTString<T>& aSrc);

    CTStringFix& operator+=(TChar ch);
    CTStringFix& operator+=(PCTStr psz);
    CTStringFix& operator+=(const CTStringFix& aSrc);
    template <size_t stLenT> CTStringFix& operator+=(const CTStringFix<T, stLenT>& aSrc);
    CTStringFix& operator+=(const CTStringRef<T>& aSrc);
    CTStringFix& operator+=(const CTString<T>& aSrc);

    bool         operator<=(PCTStr psz) const;
    bool         operator<(PCTStr psz) const;
    bool         operator>=(PCTStr psz) const;
    bool         operator>(PCTStr psz) const;
    bool         operator==(PCTStr psz) const;
    bool         operator!=(PCTStr psz) const;

    bool         operator<=(const CTStringFix& aSrc) const;
    bool         operator<(const CTStringFix& aSrc) const;
    bool         operator>=(const CTStringFix& aSrc) const;
    bool         operator>(const CTStringFix& aSrc) const;
    bool         operator==(const CTStringFix& aSrc) const;
    bool         operator!=(const CTStringFix& aSrc) const;

    template <size_t stLenT> bool operator<=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator<(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator==(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator!=(const CTStringFix<T, stLenT>& aSrc) const;

    bool         operator<=(const CTStringRef<T>& aSrc) const;
    bool         operator<(const CTStringRef<T>& aSrc) const;
    bool         operator>=(const CTStringRef<T>& aSrc) const;
    bool         operator>(const CTStringRef<T>& aSrc) const;
    bool         operator==(const CTStringRef<T>& aSrc) const;
    bool         operator!=(const CTStringRef<T>& aSrc) const;

    bool         operator<=(const CTString<T>& aSrc) const;
    bool         operator<(const CTString<T>& aSrc) const;
    bool         operator>=(const CTString<T>& aSrc) const;
    bool         operator>(const CTString<T>& aSrc) const;
    bool         operator==(const CTString<T>& aSrc) const;
    bool         operator!=(const CTString<T>& aSrc) const;

    TChar        operator[](size_t stIndex);
    const TChar  operator[](size_t stIndex) const;

    PTStr        operator*(void);
    PCTStr       operator*(void) const;

    PCTStr       GetBuffer(size_t stIndex = 0) const;
    TChar        GetAt(size_t stIndex);
    const TChar  GetAt(size_t stIndex) const;
    bool         SetAt(size_t stIndex, TChar ch);

    bool         FillBuffer(TChar ch, size_t stCount = 1);
    bool         FillBuffer(PCTStr psz, size_t stLen = 0);
    bool         AppendBuffer(PCTStr psz, size_t stLen = 0);
    bool         AppendBuffer(TChar ch, size_t stCount = 1);

    bool         IsEmpty(void) const;
    void         Empty(void);
    size_t       ResetLength(size_t stLen = 0);
    size_t       Length(bool bStream = false) const;
    size_t       BufferLength(void) const;

    Int          Find(TChar ch, size_t stStart = 0, bool bRev = false) const;
    Int          Find(PCTStr pszSub, size_t stStart = 0) const;
    Int          OneOf(PCTStr pszMatch, size_t stStart = 0, bool bInMatch = true) const;
    Int          Cmp(PCTStr psz) const;
    Int          Cmpi(PCTStr psz) const;
    Int          Cmpn(PCTStr psz, size_t stLen = 0) const;
    Int          Cmpin(PCTStr psz, size_t stLen = 0) const;
    // compare strings using locale-specific information
    Int          Coll(PCTStr pszCmp) const;
    Int          Colli(PCTStr pszCmp) const;
    Int          Colln(PCTStr pszCmp, size_t stLen = 0) const;
    Int          Collin(PCTStr pszCmp, size_t stLen = 0) const;

    size_t       Delete(size_t stIndex, size_t stCount = 1);
    size_t       Insert(size_t stIndex, TChar ch);
    size_t       Insert(size_t stIndex, PCTStr psz);
    size_t       Replace(TChar chOld, TChar chNew);
    size_t       Replace(PCTStr pszOld, PCTStr pszNew);
    size_t       Remove(TChar ch);

    size_t       Load(UInt uId, Module mInst = nullptr);
    size_t       Format(PCTStr pszFormat, ...);
    size_t       FormatV(PCTStr pszFormat, va_list vl);
    size_t       AppendFormat(PCTStr pszFormat, ...);
    size_t       AppendFormatV(PCTStr pszFormat, va_list vl);

    void         Upper(size_t stIndex);
    void         Lower(size_t stIndex);
    void         Upper(void);
    void         Lower(void);
    void         Reverse(void);
    void         TrimLeft(TChar ch = TF(' '));
    void         TrimRight(TChar ch = TF(' '));
    void         Trim(TChar ch = TF(' '));

    CTStringFix  Left(size_t stCount) const;
    CTStringFix  Right(size_t stCount) const;
    CTStringFix  RightPos(size_t stStart) const;
    CTStringFix  Mid(size_t stStart, size_t stCount) const;
public:
    // Character Classification
    bool         IsAlnumChar(size_t stIndex) const;
    bool         IsAlphaChar(size_t stIndex) const;
    bool         IsPrintChar(size_t stIndex) const;
    bool         IsGraphChar(size_t stIndex) const;
    bool         IsDigitChar(size_t stIndex) const;
    bool         IsXDigitChar(size_t stIndex) const;
    bool         IsSpaceChar(size_t stIndex) const;
    bool         IsLowerChar(size_t stIndex) const;
    bool         IsUpperChar(size_t stIndex) const;

    Int          ToInt(void) const;
    Long         ToLong(Int nRadix = RADIXT_DEC) const;
    ULong        ToULong(Int nRadix = RADIXT_DEC) const;
    LLong        ToLLong(Int nRadix = RADIXT_DEC) const;
    ULLong       ToULLong(Int nRadix = RADIXT_DEC) const;
    Double       ToDouble(void) const;
    // stIndex set to -1 if can not find end, else return end index
    Long         ToLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULong        ToULong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    LLong        ToLLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULLong       ToULLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    Double       ToDouble(size_t& stIndex) const;

    bool         ToString(Int nValue, Int nRadix = RADIXT_DEC);
    bool         ToString(UInt uValue, Int nRadix = RADIXT_DEC);
    bool         ToString(Long lValue, Int nRadix = RADIXT_DEC);
    bool         ToString(ULong ulValue, Int nRadix = RADIXT_DEC);
    bool         ToString(LLong llValue, Int nRadix = RADIXT_DEC);
    bool         ToString(ULLong ullValue, Int nRadix = RADIXT_DEC);
    bool         ToString(Double dValue, Int nRadix = RADIXT_DIGITS);
private:
    size_t       m_stLen;
    TChar        m_szBuffer[stFix];
};

template <typename T, size_t stFix>
class CTElementTraits<CTStringFix<T, stFix> > : public CTStringFix<T, stFix>::CElementTraits { };

///////////////////////////////////////////////////////////////////
// CTString
template <typename T>
class CTString : public T
{
public:
    typedef typename T::TChar     TChar;
    typedef typename T::PTStr     PTStr;
    typedef typename T::PCTStr    PCTStr;
    typedef typename T::CPCTStr   CPCTStr;
public:
    class CElementTraits
    {
    public:
        typedef const CTString& INARGTYPE;
        typedef CTString&  OUTARGTYPE;
#ifndef __MODERN_CXX_NOT_SUPPORTED
        typedef CTString&& RVARGTYPE;
#endif
    public:
        static void CopyElements(CTString* pDst, const CTString* pSrc, size_t stElements)
        {
            for (size_t i = 0; i < stElements; ++i)
            {
                pDst[i] = pSrc[i];
            }
        }

        static void RelocateElements(CTString* pDst, CTString* pSrc, size_t stElements)
        {
            MM_SAFE::Mov(pDst, stElements * sizeof(CTString), pSrc, stElements * sizeof(CTString));
        }

        static bool CompareElements(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return (T::Cmpn(t1.m_pszBuffer, t2.m_pszBuffer, t1.m_stLen) == 0);
            }
            return false;
        }

        static Int CompareElementsOrdered(INARGTYPE t1, INARGTYPE t2)
        {
            if (t1.m_stLen == t2.m_stLen)
            {
                return T::Cmpn(t1.m_pszBuffer, t2.m_pszBuffer, t1.m_stLen);
            }
            return (Int)((intptr_t)t1.m_stLen - (intptr_t)t2.m_stLen);
        }

        static size_t HashElements(INARGTYPE t)
        {
            return (CHash::Hash(t.m_pszBuffer, t.m_stLen));
        }
    };
public:
    CTString(void);
    ~CTString(void);
    CTString(PCTStr psz, size_t stLen = 0);
    CTString(const CTString& aSrc);
    CTString(const CTStringRef<T>& aSrc);
    template <size_t stLenT> CTString(const CTStringFix<T, stLenT>& aSrc);

#ifndef __MODERN_CXX_NOT_SUPPORTED
    CTString(CTString&& aSrc);
    CTString& operator=(CTString&& aSrc);
#endif

    CTString& operator=(PCTStr psz);
    CTString& operator=(const CTString& aSrc);
    CTString& operator=(const CTStringRef<T>& aSrc);
    template <size_t stLenT> CTString& operator=(const CTStringFix<T, stLenT>& aSrc);

    CTString& operator+=(TChar ch);
    CTString& operator+=(PCTStr psz);
    CTString& operator+=(const CTString& aSrc);
    CTString& operator+=(const CTStringRef<T>& aSrc);
    template <size_t stLenT> CTString& operator+=(const CTStringFix<T, stLenT>& aSrc);

    CTString  operator+(TChar ch);
    CTString  operator+(PCTStr psz);
    CTString  operator+(const CTString& aSrc);
    CTString  operator+(const CTStringRef<T>& aSrc);
    template <size_t stLenT> CTString operator+(const CTStringFix<T, stLenT>& aSrc);

    bool      operator<=(PCTStr psz) const;
    bool      operator<(PCTStr psz) const;
    bool      operator>=(PCTStr psz) const;
    bool      operator>(PCTStr psz) const;
    bool      operator==(PCTStr psz) const;
    bool      operator!=(PCTStr psz) const;

    bool      operator<=(const CTString& aSrc) const;
    bool      operator<(const CTString& aSrc) const;
    bool      operator>=(const CTString& aSrc) const;
    bool      operator>(const CTString& aSrc) const;
    bool      operator==(const CTString& aSrc) const;
    bool      operator!=(const CTString& aSrc) const;

    bool      operator<=(const CTStringRef<T>& aSrc) const;
    bool      operator<(const CTStringRef<T>& aSrc) const;
    bool      operator>=(const CTStringRef<T>& aSrc) const;
    bool      operator>(const CTStringRef<T>& aSrc) const;
    bool      operator==(const CTStringRef<T>& aSrc) const;
    bool      operator!=(const CTStringRef<T>& aSrc) const;

    template <size_t stLenT> bool operator<=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator<(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>=(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator>(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator==(const CTStringFix<T, stLenT>& aSrc) const;
    template <size_t stLenT> bool operator!=(const CTStringFix<T, stLenT>& aSrc) const;

    TChar       operator[](size_t stIndex);
    const TChar operator[](size_t stIndex) const;

    PTStr       operator*(void);
    PCTStr      operator*(void) const;

    PCTStr      GetBuffer(size_t stIndex = 0) const;
    TChar       GetAt(size_t stIndex);
    const TChar GetAt(size_t stIndex) const;
    bool        SetAt(size_t stIndex, TChar ch);

    bool      FillBuffer(TChar ch, size_t stCount = 1);
    bool      FillBuffer(PCTStr psz, size_t stLen = 0);
    bool      AppendBuffer(PCTStr psz, size_t stLen = 0);
    bool      AppendBuffer(TChar ch, size_t stCount = 1);
    bool      SetBufferLength(size_t stLen); // stLen include '\0'

    bool      IsEmpty(void) const;
    void      Empty(bool bRelease = true);
    size_t    ResetLength(size_t stLen = 0);
    size_t    Length(bool bStream = false) const;
    size_t    BufferLength(void) const;

    Int       Find(TChar ch, size_t stStart = 0, bool bRev = false) const;
    Int       Find(PCTStr pszSub, size_t stStart = 0) const;
    Int       OneOf(PCTStr pszMatch, size_t stStart = 0, bool bInMatch = true) const;
    Int       Cmp(PCTStr psz) const;
    Int       Cmpi(PCTStr psz) const;
    Int       Cmpn(PCTStr psz, size_t stLen = 0) const;
    Int       Cmpin(PCTStr psz, size_t stLen = 0) const;
    // compare strings using locale-specific information
    Int       Coll(PCTStr pszCmp) const;
    Int       Colli(PCTStr pszCmp) const;
    Int       Colln(PCTStr pszCmp, size_t stLen = 0) const;
    Int       Collin(PCTStr pszCmp, size_t stLen = 0) const;

    size_t    Delete(size_t stIndex, size_t stCount = 1);
    size_t    Insert(size_t stIndex, TChar ch);
    size_t    Insert(size_t stIndex, PCTStr psz);
    size_t    Replace(TChar chOld, TChar chNew);
    size_t    Replace(PCTStr pszOld, PCTStr pszNew);
    size_t    Remove(TChar ch);

    void      Shrink(void);
    bool      Attach(CTString& aSrc);
    bool      Attach(PByte pBuf, size_t stBuf, size_t stLen);
    void      Detach(PByte& pBuf, size_t& stBuf, size_t& stLen);
    void      Detach(void);

    size_t    Load(UInt uId, Module mInst = nullptr);
    size_t    Format(PCTStr pszFormat, ...);
    size_t    FormatV(PCTStr pszFormat, va_list vl);
    size_t    AppendFormat(PCTStr pszFormat, ...);
    size_t    AppendFormatV(PCTStr pszFormat, va_list vl);

    void      Upper(size_t stIndex);//
    void      Lower(size_t stIndex);//
    void      Upper(void);
    void      Lower(void);
    void      Reverse(void);
    void      TrimLeft(TChar ch = TF(' '));
    void      TrimRight(TChar ch = TF(' '));
    void      Trim(TChar ch = TF(' '));

    CTString  Left(size_t stCount) const;
    CTString  Right(size_t stCount) const;
    CTString  RightPos(size_t stStart) const;
    CTString  Mid(size_t stStart, size_t stCount) const;
public:
    // Character Classification
    bool      IsAlnumChar(size_t stIndex) const;
    bool      IsAlphaChar(size_t stIndex) const;
    bool      IsPrintChar(size_t stIndex) const;
    bool      IsGraphChar(size_t stIndex) const;
    bool      IsDigitChar(size_t stIndex) const;
    bool      IsXDigitChar(size_t stIndex) const;
    bool      IsSpaceChar(size_t stIndex) const;
    bool      IsLowerChar(size_t stIndex) const;
    bool      IsUpperChar(size_t stIndex) const;

    Int       ToInt(void) const;
    Long      ToLong(Int nRadix = RADIXT_DEC) const;
    ULong     ToULong(Int nRadix = RADIXT_DEC) const;
    LLong     ToLLong(Int nRadix = RADIXT_DEC) const;
    ULLong    ToULLong(Int nRadix = RADIXT_DEC) const;
    Double    ToDouble(void) const;
    // stIndex set to -1 if can not find end, else return end index
    Long      ToLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULong     ToULong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    LLong     ToLLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    ULLong    ToULLong(size_t& stIndex, Int nRadix = RADIXT_DEC) const;
    Double    ToDouble(size_t& stIndex) const;

    bool      ToString(Int nValue, Int nRadix = RADIXT_DEC);
    bool      ToString(UInt uValue, Int nRadix = RADIXT_DEC);
    bool      ToString(Long lValue, Int nRadix = RADIXT_DEC);
    bool      ToString(ULong ulValue, Int nRadix = RADIXT_DEC);
    bool      ToString(LLong llValue, Int nRadix = RADIXT_DEC);
    bool      ToString(ULLong ullValue, Int nRadix = RADIXT_DEC);
    bool      ToString(Double dValue, Int nRadix = RADIXT_DIGITS);
private:
    bool      AllocBuffer(size_t stBufferLen, bool bAlloc = false);
    bool      AllocBuffer(size_t stBufferLen, PTStr& pszBuffer);
private:
    size_t    m_stLen;
    size_t    m_stBufferLen;
    PTStr     m_pszBuffer;
};

template <typename T>
class CTElementTraits<CTString<T> > : public CTString<T>::CElementTraits { };

///////////////////////////////////////////////////////////////////
#include "tstring.inl"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targettstring.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targettstring.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __TSTRING_H__
