// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __STREAM_H__
#define __STREAM_H__

#pragma once

#include "tstring.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CStream
class NOVTABLE CStream ABSTRACT : public MObject
{
public:
    enum STREAM_MODE
    {
        STREAMM_NONE       = 0x00000000,
        STREAMM_READ       = 0x00000001,
        STREAMM_WRITE      = 0x00000002,
        STREAMM_FILE       = 0x00000004,
        STREAMM_MAPPING    = 0x00000008,
        STREAMM_BUFFER     = 0x00000010,

        STREAMM_BYTESWAP   = 0x20000000,
        STREAMM_ERROR      = 0x40000000,
    };
public:
    virtual size_t Read(void* pV, size_t stLenBytes);
    virtual size_t Write(const void* pV, size_t stLenBytes);

    virtual size_t Tell(void) const; // stream current read/write pos
    virtual size_t Size(void) const; // read stream size, write return current data size
    virtual size_t Rest(void) const; // size - pos

    // return the pos offset from stream begin
    virtual size_t Seek(SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN) PURE;
    virtual bool   Flush(Int nFlag = FLUSHO_DEFAULT);
    // reset all data to init status
    virtual void   Close(void);
    // refer source stream, create a scope read stream, invalid after source stream is closed
    // refer a write stream, scope read stream size is write stream tell
    // refer a read  stream, scope read stream size is read  stream size
    virtual bool   Refer(CStream& aSrc);

    virtual ~CStream(void);

    Int   GetMode(void) const;
    bool  IsError(void) const;
    bool  IsByteSwap(void) const;
    bool  IsEnd(void) const;
    bool  IsRead(void) const;
    bool  IsWrite(void) const;

    void  SetByteSwap(bool bEnabled);

    // when read  a file after end or stop, no error flag set
    // when write a file after stop, no error flag set, too

    // write operations
    // a read stream set error flag if use write operations
    CStream& operator<<(Char   c);
    CStream& operator<<(Short  s);
    CStream& operator<<(Int    n);
    CStream& operator<<(Long   l);
    CStream& operator<<(LLong  ll);

    CStream& operator<<(UChar  uc);
    CStream& operator<<(UShort us);
    CStream& operator<<(UInt   u);
    CStream& operator<<(ULong  ul);
    CStream& operator<<(ULLong ull);

    CStream& operator<<(Float  f);
    CStream& operator<<(Double d);

    CStream& operator<<(bool   b);
    CStream& operator<<(WChar  w);
    template <typename T>
    CStream& operator<<(T*     p);

    CStream& operator<<(const CString& str);
    template <size_t stLenT>
    CStream& operator<<(const CTStringFix<CXChar, stLenT>& strFix);
    CStream& operator<<(const CStringRef& strRef);
    CStream& operator<<(PCXStr psz);
    // text stream sepcial
    CStream& operator<=(const CString& str);
    template <size_t stLenT>
    CStream& operator<=(const CTStringFix<CXChar, stLenT>& strFix);
    CStream& operator<=(const CStringRef& strRef);
    CStream& operator<=(PCXStr psz);

    // read operations
    // a write stream set error flag if use read operations
    CStream& operator>>(Char&   c);
    CStream& operator>>(Short&  s);
    CStream& operator>>(Int&    n);
    CStream& operator>>(Long&   l);
    CStream& operator>>(LLong&  ll);

    CStream& operator>>(UChar&  uc);
    CStream& operator>>(UShort& us);
    CStream& operator>>(UInt&   u);
    CStream& operator>>(ULong&  ul);
    CStream& operator>>(ULLong& ull);

    CStream& operator>>(Float&  f);
    CStream& operator>>(Double& d);

    CStream& operator>>(bool&   b);
    CStream& operator>>(WChar&  w);
    template <typename T>
    CStream& operator>>(T*&     p);

    CStream& operator>>(CString& str);
    template <size_t stLenT>
    CStream& operator>>(CTStringFix<CXChar, stLenT>& strFix);
    CStream& operator>>(CStringRef& strRef);
    // text stream sepcial
    CStream& operator>=(CString& str);
    template <size_t stLenT>
    CStream& operator>=(CTStringFix<CXChar, stLenT>& strFix);
    CStream& operator>=(CStringRef& strRef);
protected:
    CStream(Int nMode = 0);
    CStream(const CStream&);
    CStream& operator=(const CStream&);

    void  SetError(void);
    void  WriteSwapString(PCXStr psz, SizeLen sLen);
    void  ReadSwapString(PXStr psz, SizeLen sLen);
private:
    Int   m_nMode;
};

///////////////////////////////////////////////////////////////////
// CStreamSeekScope
class CStreamSeekScope : public MObject
{
public:
    CStreamSeekScope(CStream& Stream, SeekPos skPos, SEEK_OP eFrom = SEEKO_BEGIN);
    CStreamSeekScope(CStream& Stream);
    ~CStreamSeekScope(void);
private:
    CStreamSeekScope(const CStreamSeekScope&);
    CStreamSeekScope& operator=(const CStreamSeekScope&);
private:
    CStream*   m_pStream;
    size_t     m_stPos;
};

///////////////////////////////////////////////////////////////////
#include "stream.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __STREAM_H__