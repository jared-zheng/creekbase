// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __KEY_VALUE_INL__
#define __KEY_VALUE_INL__

#pragma once

/////////////////////////////////////////////////////////////////////
// CKeyValue
INLINE CKeyValue::CKeyValue(void)
: m_KeyValue(CContainerTraits::SIZED_GROW, CContainerTraits::HASHD_SIZE)
{
}

INLINE CKeyValue::~CKeyValue(void)
{
    m_KeyValue.RemoveAll();
}

INLINE CKeyValue::CKeyValue(const CKeyValue&)
{
}

INLINE CKeyValue& CKeyValue::operator=(const CKeyValue&)
{
    return (*this);
}

INLINE Int CKeyValue::GetCount(void) const
{
    return m_KeyValue.GetSize();
}

INLINE size_t CKeyValue::GetSize(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if (pPair != nullptr)
    {
        return pPair->m_V.GetSize();
    }
    return 0;
}

INLINE VAR_TYPE CKeyValue::GetType(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if (pPair != nullptr)
    {
        return pPair->m_V.GetType();
    }
    return VART_NONE;
}

template <typename T>
INLINE T& CKeyValue::AnyCast(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsAnyType<T>())
    {
        return pPair->m_V.AnyCast<T>();
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename T>
INLINE T* CKeyValue::TryCast(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsAnyType<T>())
    {
        return pPair->m_V.TryCast<T>();
    }
    return nullptr;
}

template <typename T>
INLINE T* CKeyValue::GetValue(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsPtrType<T>())
    {
        return pPair->m_V.GetPtr<T>();
    }
    return nullptr;
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE bool CKeyValue::GetValue(PCXStr pszKey, CCString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsCacheType(VART_CHAR))
    {
        return pPair->m_V.GetValue(strValue, bAttach);
    }
    return false;
}
#else
INLINE bool CKeyValue::GetValue(PCXStr pszKey, CWString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsCacheType(VART_WCHAR))
    {
        return pPair->m_V.GetValue(strValue, bAttach);
    }
    return false;
}
#endif

INLINE bool CKeyValue::GetValue(PCXStr pszKey, CString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsCacheType(VART_XCHAR))
    {
        return pPair->m_V.GetValue(strValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, CBufStream& bsValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsCacheType(VART_UCHAR))
    {
        return pPair->m_V.GetValue(bsValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, Short& sValue)
{
    sValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_SHORT))
    {
        return pPair->m_V.GetValue(sValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, UShort& usValue)
{
    usValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_USHORT))
    {
        return pPair->m_V.GetValue(usValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, Int& nValue)
{
    nValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_INT))
    {
        return pPair->m_V.GetValue(nValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, UInt& uValue)
{
    uValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_UINT))
    {
        return pPair->m_V.GetValue(uValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, Long& lValue)
{
    lValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_LONG))
    {
        return pPair->m_V.GetValue(lValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, ULong& ulValue)
{
    ulValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_ULONG))
    {
        return pPair->m_V.GetValue(ulValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, LLong& llValue)
{
    llValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_LLONG))
    {
        return pPair->m_V.GetValue(llValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, ULLong& ullValue)
{
    ullValue = 0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_ULLONG))
    {
        return pPair->m_V.GetValue(ullValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, Double& dValue)
{
    dValue = 0.0;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_DOUBLE))
    {
        return pPair->m_V.GetValue(dValue);
    }
    return false;
}

INLINE bool CKeyValue::GetValue(PCXStr pszKey, bool& bValue)
{
    bValue = false;
    assert(pszKey != nullptr);
    const PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if ((pPair != nullptr) && pPair->m_V.IsType(VART_BOOL))
    {
        return pPair->m_V.GetValue(bValue);
    }
    return false;
}

template <typename T>
INLINE bool CKeyValue::SetAny(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue<T>();
    }
    return false;
}

template <typename T>
INLINE bool CKeyValue::SetAny(PCXStr pszKey, const T& tValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue<T>(tValue);
    }
    return false;
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
template <typename T>
INLINE bool CKeyValue:: SetAny(PCXStr pszKey, T&& tValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue<T>(std::move(tValue));
    }
    return false;
}
#endif

template <typename T>
INLINE bool CKeyValue::SetValue(PCXStr pszKey, T* pValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue<T>(pValue);
    }
    return false;
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE bool CKeyValue::SetValue(PCXStr pszKey, const CCString& strValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, CCString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, PCStr pszValue, size_t stSize)
{
    assert(pszValue != nullptr);
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetCString(pszValue, stSize);
    }
    return false;
}
#else
INLINE bool CKeyValue::SetValue(PCXStr pszKey, const CWString& strValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, CWString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, PCWStr pszValue, size_t stSize)
{
    assert(pszValue != nullptr);
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetWString(pszValue, stSize);
    }
    return false;
}
#endif

INLINE bool CKeyValue::SetValue(PCXStr pszKey, const CString& strValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, CString& strValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(strValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, PCXStr pszValue, size_t stSize)
{
    assert(pszValue != nullptr);
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetXString(pszValue, stSize);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, const CBufStream& bsValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(bsValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, CBufStream& bsValue, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(bsValue, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, PByte pbValue, size_t stSize, bool bAttach)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(pbValue, stSize, bAttach);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, Short sValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(sValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, UShort usValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(usValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, Int nValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(nValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, UInt uValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(uValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, Long lValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(lValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, ULong ulValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(ulValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, LLong llValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(llValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, ULLong ullValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(ullValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, Double dValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(dValue);
    }
    return false;
}

INLINE bool CKeyValue::SetValue(PCXStr pszKey, bool bValue)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey, true);
    if (pPair != nullptr)
    {
        return pPair->m_V.SetValue(bValue);
    }
    return false;
}

INLINE bool CKeyValue::Remove(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    PAIR_KEYVALUE* pPair = FindKV(pszKey);
    if (pPair != nullptr)
    {
        pPair->m_V.Reset();
        return true;
    }
    return false;
}

INLINE bool CKeyValue::Find(PCXStr pszKey)
{
    assert(pszKey != nullptr);
    return (FindKV(pszKey) != nullptr);
}

INLINE CKeyValue::PAIR_KEYVALUE* CKeyValue::FindKV(PCXStr pszKey, bool bAdd)
{
    CSyncLockScope scope(m_KVLock);
    PAIR_KEYVALUE* pPair = m_KeyValue.Find(pszKey);
    if ((pPair == nullptr) && bAdd)
    {
        PINDEX index = m_KeyValue.Add(pszKey);
        if (index != nullptr)
        {
            pPair = m_KeyValue.GetAt(index);
        }
    }
    return pPair;
}

#endif // __KEY_VALUE_INL__
