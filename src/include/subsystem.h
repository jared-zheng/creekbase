// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __SUBSYSTEM_H__
#define __SUBSYSTEM_H__

#pragma once

#include "container.h"
#include "event.h"
#include "module.h"
#include "uid.h"

///////////////////////////////////////////////////////////////////
// DECLARE
#ifdef __RUNTIME_STATIC__
    #define DECLARE_STATIC_CREATE_FUNC( name )                               \
    extern CREEK::CSubSystem* Create##name(const CREEK::CUUID&)

    #define IMPLEMENT_CREATE_SUBSYSTEM( name )                               \
    CREEK::CSubSystem* CONCAT(Create, name)(const CREEK::CUUID& uuId)
#else
    #define IMPLEMENT_CREATE_SUBSYSTEM( name )                               \
    CREEK::CSubSystem* CreateSubSystem(const CREEK::CUUID& uuId)
#endif

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// lib class loader
#define DECLARE_LIB_CLASS_LOADER( cls, name )                                \
public:                                                                      \
    static CTRefCountPtr<CSubSystem>  ms_##name##LibPtr;                     \
    static CTRefCountPtr<CSubSystem>& Get##name##LibPtr(void)                \
    {                                                                        \
        return ms_##name##LibPtr;                                            \
    }
#define IMPLEMENT_LIB_CLASS_LOADER( cls, name )                              \
    SELECTANY CTRefCountPtr<CSubSystem> cls::ms_##name##LibPtr;
#define INIT_LIB_CLASS_LOADER( cls, name, uuid, func )                       \
    ((cls::ms_##name##LibPtr = CONCAT(Create, func)((uuid))) != nullptr)
#define EXIT_LIB_CLASS_LOADER( cls, name )                                   \
    if (cls::ms_##name##LibPtr != nullptr)                                   \
    {                                                                        \
        cls::ms_##name##LibPtr->Exit();                                      \
        cls::ms_##name##LibPtr = nullptr;                                    \
    }
#define CHECK_LIB_CLASS_LOADER( cls, name )                                  \
    (cls::ms_##name##LibPtr != nullptr)
#define LOADER_LIB_CLASS_CREEATE( cls, name, uuid, ptr)                      \
    if ((cls::ms_##name##LibPtr != nullptr) &&                               \
        (cls::ms_##name##LibPtr->Init() == (UInt)RET_OKAY))                  \
    {                                                                        \
        cls::ms_##name##LibPtr->CreateComponent((uuid), (ptr));              \
    }
#define GET_LIB_CLASS_LOADER( cls, name )                                    \
    (cls::Get##name##LibPtr())
#define GET_LIB_CLASS_SUBSYSTEM( cls, name )                                 \
    (cls::Get##name##LibPtr().Get())
///////////////////////////////////////////////////////////////////
// lib global loader
#define DECLARE_LIB_GLOBAL_LOADER( name )                                    \
    extern CTRefCountPtr<CSubSystem>  gs_##name##LibPtr;                     \
    static CTRefCountPtr<CSubSystem>& Get##name##LibPtr(void)                \
    {                                                                        \
        return gs_##name##LibPtr;                                            \
    }
#define IMPLEMENT_LIB_GLOBAL_LOADER( name )                                  \
    SELECTANY CTRefCountPtr<CSubSystem> gs_##name##LibPtr;
#define INIT_LIB_GLOBAL_LOADER( name, uuid, func )                           \
    ((gs_##name##LibPtr = CONCAT(Create, func)((uuid))) != nullptr)
#define EXIT_LIB_GLOBAL_LOADER( name )                                       \
    if (gs_##name##LibPtr != nullptr)                                        \
    {                                                                        \
        gs_##name##LibPtr->Exit();                                           \
        gs_##name##LibPtr = nullptr;                                         \
    }
#define CHECK_LIB_GLOBAL_LOADER( name )                                      \
    (gs_##name##LibPtr != nullptr)
#define LOADER_LIB_GLOBAL_CREEATE( name, uuid, ptr)                          \
    if ((gs_##name##LibPtr != nullptr) &&                                    \
        (gs_##name##LibPtr->Init() == (UInt)RET_OKAY))                       \
    {                                                                        \
        gs_##name##LibPtr->CreateComponent((uuid), (ptr));                   \
    }
#define GET_LIB_GLOBAL_LOADER( name )                                        \
    (Get##name##LibPtr())
#define GET_LIB_GLOBAL_SUBSYSTEM( name )                                     \
    (Get##name##LibPtr().Get())
///////////////////////////////////////////////////////////////////
// dync class loader
#define DECLARE_DYNC_CLASS_LOADER( cls, name )                               \
public:                                                                      \
    static CLoader  ms_##name##Loader;                                       \
    static CLoader& Get##name##Loader(void)                                  \
    {                                                                        \
        return ms_##name##Loader;                                            \
    }
#define IMPLEMENT_DYNC_CLASS_LOADER( cls, name )                             \
    SELECTANY CLoader cls::ms_##name##Loader;
#define INIT_DYNC_CLASS_LOADER( cls, name, uuid, path )                      \
    cls::Get##name##Loader().Load((uuid), (path))
#define EXIT_DYNC_CLASS_LOADER( cls, name )                                  \
    cls::Get##name##Loader().Unload()
#define CHECK_DYNC_CLASS_LOADER( cls, name )                                 \
    (cls::Get##name##Loader().IsInited())
#define LOADER_DYNC_CLASS_CREEATE( cls, name, uuid, ptr)                     \
    if (cls::Get##name##Loader().IsInited())                                 \
    {                                                                        \
        cls::Get##name##Loader()->CreateComponent((uuid), (ptr));            \
    }
#define GET_DYNC_CLASS_LOADER( cls, name )                                   \
    (cls::Get##name##Loader())
#define GET_DYNC_CLASS_SUBSYSTEM( cls, name )                                \
    (cls::Get##name##Loader().Get())
///////////////////////////////////////////////////////////////////
// dync global loader
#define DECLARE_DYNC_GLOBAL_LOADER( name )                                   \
    extern CLoader  gs_##name##Loader;                                       \
    static CLoader& Get##name##Loader(void)                                  \
    {                                                                        \
        return gs_##name##Loader;                                            \
    }
#define IMPLEMENT_DYNC_GLOBAL_LOADER( name )                                 \
    SELECTANY CLoader gs_##name##Loader;
#define INIT_DYNC_GLOBAL_LOADER( name, uuid, path )                          \
    Get##name##Loader().Load((uuid), (path))
#define EXIT_DYNC_GLOBAL_LOADER( name )                                      \
    Get##name##Loader().Unload()
#define CHECK_DYNC_GLOBAL_LOADER( name )                                     \
    (Get##name##Loader().IsInited())
#define LOADER_DYNC_GLOBAL_CREEATE( name, uuid, ptr)                         \
    if (Get##name##Loader().IsInited())                                      \
    {                                                                        \
        Get##name##Loader()->CreateComponent((uuid), (ptr));                 \
    }
#define GET_DYNC_GLOBAL_LOADER( name )                                       \
    (Get##name##Loader())
#define GET_DYNC_GLOBAL_SUBSYSTEM( name )                                    \
    (Get##name##Loader().Get())
///////////////////////////////////////////////////////////////////
//
#ifdef __RUNTIME_STATIC__
    #define DECLARE_CLASS_LOADER      DECLARE_LIB_CLASS_LOADER
    #define IMPLEMENT_CLASS_LOADER    IMPLEMENT_LIB_CLASS_LOADER
    #define INIT_CLASS_LOADER         INIT_LIB_CLASS_LOADER
    #define EXIT_CLASS_LOADER         EXIT_LIB_CLASS_LOADER
    #define CHECK_CLASS_LOADER        CHECK_LIB_CLASS_LOADER
    #define LOADER_CLASS_CREEATE      LOADER_LIB_CLASS_CREEATE
    #define GET_CLASS_LOADER          GET_LIB_CLASS_LOADER
    #define GET_CLASS_SUBSYSTEM       GET_LIB_CLASS_SUBSYSTEM

    #define DECLARE_GLOBAL_LOADER     DECLARE_LIB_GLOBAL_LOADER
    #define IMPLEMENT_GLOBAL_LOADER   IMPLEMENT_LIB_GLOBAL_LOADER
    #define INIT_GLOBAL_LOADER        INIT_LIB_GLOBAL_LOADER
    #define EXIT_GLOBAL_LOADER        EXIT_LIB_GLOBAL_LOADER
    #define CHECK_GLOBAL_LOADER       CHECK_LIB_GLOBAL_LOADER
    #define LOADER_GLOBAL_CREEATE     LOADER_LIB_GLOBAL_CREEATE
    #define GET_GLOBAL_LOADER         GET_LIB_GLOBAL_LOADER
    #define GET_GLOBAL_SUBSYSTEM      GET_LIB_GLOBAL_SUBSYSTEM
#else
    #define DECLARE_CLASS_LOADER      DECLARE_DYNC_CLASS_LOADER
    #define IMPLEMENT_CLASS_LOADER    IMPLEMENT_DYNC_CLASS_LOADER
    #define INIT_CLASS_LOADER         INIT_DYNC_CLASS_LOADER
    #define EXIT_CLASS_LOADER         EXIT_DYNC_CLASS_LOADER
    #define CHECK_CLASS_LOADER        CHECK_DYNC_CLASS_LOADER
    #define LOADER_CLASS_CREEATE      LOADER_DYNC_CLASS_CREEATE
    #define GET_CLASS_LOADER          GET_DYNC_CLASS_LOADER
    #define GET_CLASS_SUBSYSTEM       GET_DYNC_CLASS_SUBSYSTEM

    #define DECLARE_GLOBAL_LOADER     DECLARE_DYNC_GLOBAL_LOADER
    #define IMPLEMENT_GLOBAL_LOADER   IMPLEMENT_DYNC_GLOBAL_LOADER
    #define INIT_GLOBAL_LOADER        INIT_DYNC_GLOBAL_LOADER
    #define EXIT_GLOBAL_LOADER        EXIT_DYNC_GLOBAL_LOADER
    #define CHECK_GLOBAL_LOADER       CHECK_DYNC_GLOBAL_LOADER
    #define LOADER_GLOBAL_CREEATE     LOADER_DYNC_GLOBAL_CREEATE
    #define GET_GLOBAL_LOADER         GET_DYNC_GLOBAL_LOADER
    #define GET_GLOBAL_SUBSYSTEM      GET_DYNC_GLOBAL_SUBSYSTEM
#endif

///////////////////////////////////////////////////////////////////
// CComponent
class NOVTABLE CComponent ABSTRACT : public CTRefCount<CComponent>
{
public:
    // execute command and time tick
    virtual UInt Command(PCXStr pszCMD, uintptr_t utParam) PURE;
    virtual UInt Update(void) PURE;
protected:
    CComponent(void);
    virtual ~CComponent(void);
private:
    CComponent(const CComponent&);
    CComponent& operator=(const CComponent&);
};
typedef CTRefCountPtr<CComponent> CComponentPtr;

///////////////////////////////////////////////////////////////////
// CSubSystem
class NOVTABLE CSubSystem ABSTRACT : public CComponent
{
public:
    static CPCStr CreateSubSystem;

    typedef CTMap<CUUID, CStringKey>         MAP_COMPONENT_INFO, *PMAP_COMPONENT_INFO;
    typedef CTMap<CUUID, CStringKey>::PAIR   PAIR_COMPONENT_INFO;
public:
    virtual UInt  Init(void) PURE;
    virtual void  Exit(void) PURE;
    virtual Int   GetComponentInfo(MAP_COMPONENT_INFO& Components) const PURE;
    virtual bool  FindComponent(const CUUID& uuId) const PURE;
    virtual bool  CreateComponent(const CUUID& uuId, CComponentPtr& CComponentPtrRef) PURE;
protected:
    CSubSystem(void);
    virtual ~CSubSystem(void);
};

///////////////////////////////////////////////////////////////////
// CTLoader
typedef class CSubSystem* (CreateSubSystemFunc)(const CUUID&);

template <typename T>
class CTLoader : public MObject
{
public:
    CTLoader(void);
    ~CTLoader(void);

    T*     operator->(void) const;
    T*     Get(void) const;
    Module GetModule(void) const;

    bool   IsInited(void) const;
    bool   IsLoaded(void) const;
    bool   Load(const CUUID& uuId, PCXStr pszLoadPath, bool bInit = true);
    void   Unload(void);
private:
    CTLoader(const CTLoader&);
    CTLoader& operator=(const CTLoader&);
private:
    T*        m_pSubSystem;
    bool      m_bInited;
    CModule   m_LoadModule;
};
typedef CTLoader<CSubSystem> CLoader;

///////////////////////////////////////////////////////////////////
// CTLoaderRef
template <typename T>
class CTLoaderRef : public MObject
{
public:
    explicit CTLoaderRef(Module m);
    ~CTLoaderRef(void);

    T*     operator->(void) const;
    T*     Get(void) const;
    Module GetModule(void) const;

    bool   IsInited(void) const;
    bool   IsLoaded(void) const;
    bool   Load(const CUUID& uuId, bool bInit = true);
    void   Unload(void);
private:
    CTLoaderRef(const CTLoaderRef&);
    CTLoaderRef& operator=(const CTLoaderRef&);
private:
    T*        m_pSubSystem;
    bool      m_bInited;
    CModule   m_RefModule;
};
typedef CTLoaderRef<CSubSystem> CLoaderRef;

///////////////////////////////////////////////////////////////////
#include "subsystem.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __SUBSYSTEM_H__
