// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __FILE_PATH_H__
#define __FILE_PATH_H__

#pragma once

#include "container.h"
#include "sync.h"
#include "tstring.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CFileAttr
class CFileAttr : public MObject
{
public:
    CFileAttr(void);
    CFileAttr(const FILEATTR& Attr);
    ~CFileAttr(void);

    CFileAttr(const CFileAttr& aSrc);
    CFileAttr& operator=(const CFileAttr& aSrc);

    FILEATTR& Attr(void);
    const FILEATTR& Attr(void) const;

    ULLong Size(void) const;
    time_t CTime(void) const;
    time_t ATime(void) const;
    time_t MTime(void) const;

    bool   Size(ULLong ullSize) const;
    bool   CTime(time_t tc) const;
    bool   ATime(time_t ta) const;
    bool   MTime(time_t tm) const;
public:
    FILEATTR   m_Attr;
};

///////////////////////////////////////////////////////////////////
// CFileFind
class CFileFind : public MObject
{
public:
    void SetAttr(const FILEFIND& Find);

    CFileAttr&       GetAttr(void);
    const CFileAttr& GetAttr(void) const;

    FILEATTR& Attr(void);
    const FILEATTR& Attr(void) const;

    ULLong Size(void) const;
    time_t CTime(void) const;
    time_t ATime(void) const;
    time_t MTime(void) const;

    bool   Size(ULLong ullSize) const;
    bool   CTime(time_t tc) const;
    bool   ATime(time_t ta) const;
    bool   MTime(time_t tm) const;
public:
    CString     strFile;
    CFileAttr   attr;
};

///////////////////////////////////////////////////////////////////
// CFilePath
class CFilePath : public MObject
{
public:
    enum FIND_FLAG
    {
        FIND_FLAG_FULLPATH    = 0x00000001, // full path, otherwise only name
        FIND_FLAG_FILE        = 0x00000002, // only file
        FIND_FLAG_FOLDER      = 0x00000004, // only dir
        FIND_FLAG_ALL         = 0x00000007, // file + dir
        FIND_FLAG_RELATIVE    = 0x00000008, // relative path 
        FIND_FLAG_NOSUBFOLDER = 0x00000010, // current dir only
    };
public:
    static CPCXStr ModulePath;
    static CPCXStr CurFolder;
    static CPCXStr UpperFolder;
    static XChar   DotChar;
    static XChar   SlashChar;
    static XChar   BackSlashChar;
    static XChar   PathSepChar;
public:
    CFilePath(bool bModulePath = true);
    ~CFilePath(void);

    bool   IsExist(PCXStr pszPath, PCXStr pszKey = nullptr);
    bool   IsFile(PCXStr pszPath, PCXStr pszKey = nullptr);
    bool   IsFolder(PCXStr pszPath, PCXStr pszKey = nullptr);
    bool   FileAttr(CFileAttr& FileAttr, PCXStr pszPath, PCXStr pszKey = nullptr);

    bool   Copy(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting = false, PCXStr pszDstKey = nullptr, PCXStr pszSrcKey = nullptr);
    bool   Move(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting = false, PCXStr pszDstKey = nullptr, PCXStr pszSrcKey = nullptr);

    bool   Delete(PCXStr pszPath, PCXStr pszKey = nullptr);

    Int    Find(CTList<CString>& strFiles, PCXStr pszPath, PCXStr pszKey = nullptr, Int nFlag = FIND_FLAG_ALL);
    Int    Find(CTList<CFileFind>& lFinds, PCXStr pszPath, PCXStr pszKey = nullptr, Int nFlag = FIND_FLAG_ALL, Int nLevel = INT_MAX);

    bool   CreateFolder(PCXStr pszPath, PCXStr pszKey = nullptr);
    bool   DeleteFolder(PCXStr pszPath, PCXStr pszKey = nullptr);
    // set-path auto concat folder slash/backslash
    bool   SetPath(PCXStr pszKey, PCXStr pszPath);
    // strPath & pszFile can set file name to concat after path 
    bool   GetFullPath(PCXStr pszKey, CString& strPath);
    bool   GetFullPath(PCXStr pszKey, PXStr pszPath, size_t stLen);
private:
    CFilePath(const CFilePath&);
    CFilePath& operator=(const CFilePath&);

    bool   CheckFilePath(const CString& strPath, bool bReplaceExisting = false);

    Int    FindFiles(CTList<CString>& strFiles, CString& strPath, const CString& strMatch, Int nFlag, size_t stRelative);
    Int    FindFiles(CTList<CFileFind>& lFinds, CString& strPath, const CString& strMatch, Int nFlag, Int nLevel, size_t stRelative);

    bool   CreateFolders(CString& strPath);
    bool   DeleteFolders(CString& strPath);

    bool   GetPath(PCXStr pszKey, CString& strPath);
    void   SetModulePath(void);
private:
    typedef CTMap<CString, CString>         MAP_FILEPATH, *PMAP_FILEPATH;
    typedef CTMap<CString, CString>::PAIR   PAIR_FILEPATH;
private:
    MAP_FILEPATH   m_FilePath;
    CSyncLock      m_FPLock;
};

///////////////////////////////////////////////////////////////////
#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    #include "windows/targetfilepath.inl"
#elif (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
    #include "linux/targetfilepath.inl"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __FILE_PATH_H__
