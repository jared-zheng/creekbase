// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __MARKUP_INL__
#define __MARKUP_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CXMLTraits
SELECTANY CPCXStr CXMLTraits::CommentTag        = TF("<!--");
SELECTANY CPCXStr CXMLTraits::CDataTag          = TF("<![CDATA[");
SELECTANY CPCXStr CXMLTraits::DeclarationTag    = TF("<?xml");
SELECTANY CPCXStr CXMLTraits::XMLTag            = TF("xml");

SELECTANY CPCXStr CXMLTraits::CommentEndTag     = TF("-->");
SELECTANY CPCXStr CXMLTraits::CDataEndTag       = TF("]]>");
SELECTANY CPCXStr CXMLTraits::DeclarationEndTag = TF("?>");

SELECTANY CPCXStr CXMLTraits::LessEntity        = TF("&lt;");
SELECTANY CPCXStr CXMLTraits::GreaterEntity     = TF("&gt;");
SELECTANY CPCXStr CXMLTraits::AndEntity         = TF("&amp;");
SELECTANY CPCXStr CXMLTraits::SingleQuoteEntity = TF("&apos;");
SELECTANY CPCXStr CXMLTraits::DoubleQuoteEntity = TF("&quot;");

SELECTANY CPCXStr CXMLTraits::DeclDefault       = TF("version=\"1.0\"");
SELECTANY CPCXStr CXMLTraits::DeclVersion       = TF("version");
SELECTANY CPCXStr CXMLTraits::DeclEncoding      = TF("encoding");
SELECTANY CPCXStr CXMLTraits::DeclStandalone    = TF("standalone");

SELECTANY CPCXStr CXMLTraits::EncodingDefault   = TF("encoding=\"UTF-8\" ");
SELECTANY CPCXStr CXMLTraits::EncodingUTF8      = TF("UTF-8");
SELECTANY CPCXStr CXMLTraits::EncodingUTF16     = TF("UTF-16");
SELECTANY CPCXStr CXMLTraits::EncodingUTF32     = TF("UTF-32");
SELECTANY CPCXStr CXMLTraits::EncodingUnicode   = TF("Unicode");
SELECTANY CPCXStr CXMLTraits::EncodingCodePage  = TF("cp");

INLINE bool CXMLTraits::IsAlnum(UInt u) const
{
    if (u < 127)
    {
        return CXChar::IsAlnum((XChar)u);
    }
    return true;
}

INLINE bool CXMLTraits::IsAlpha(UInt u) const
{
    if (u < 127)
    {
        return CXChar::IsAlpha((XChar)u);
    }
    return true;
}

INLINE bool CXMLTraits::IsSkipChar(XChar c) const
{
    return ((c == TF(' '))  ||
            (c == TF('\r')) ||
            (c == TF('\n')) ||
            (c == TF('\t')));
}

INLINE bool CXMLTraits::SkipChars(PXStr& pszBuf) const
{
    while (IsSkipChar(*pszBuf))
    {
        ++pszBuf;
    }
    return (*pszBuf != 0);
}

INLINE bool CXMLTraits::ToName(PXStr& pszBuf, CString& strName) const
{
    strName.Empty();
    if (*pszBuf != 0)
    {
        if (IsAlpha((UInt)*pszBuf) &&
            (CXChar::Cmpin(pszBuf, XMLTag, TAGL_XML) != 0))
        {
            PXStr p = pszBuf;
            while ((*pszBuf != 0) &&
                   (IsAlnum((UInt)*pszBuf) || (*pszBuf == TF('_'))))
            {
                ++pszBuf;
            }
            if (pszBuf > p)
            {
                strName.FillBuffer(p, (pszBuf - p));
            }
            return (strName.IsEmpty() != true);
        }
    }
    return false;
}

INLINE bool CXMLTraits::CheckName(PCXStr pszName) const
{
    if ((pszName != nullptr) && (*pszName != 0))
    {
        if (IsAlpha((UInt)*pszName) &&
            (CXChar::Cmpin(pszName, XMLTag, TAGL_XML) != 0))
        {
            while ((*pszName != 0) &&
                   (IsAlnum((UInt)*pszName) || (*pszName == TF('_'))))
            {
                ++pszName;
            }
            return (*pszName == 0);
        }
    }
    return false;
}

INLINE bool CXMLTraits::ToEntity(const CString& strValue, CString& strEntity) const
{
    PCXStr pszBuf = *strValue;
    for (size_t i = 0; i < strValue.Length(); ++i)
    {
        if (pszBuf[i] == TF('<'))
        {
            strEntity += LessEntity;
        }
        else if (pszBuf[i] == TF('>'))
        {
            strEntity += GreaterEntity;
        }
        else if (pszBuf[i] == TF('&'))
        {
            strEntity += AndEntity;
        }
        else if (pszBuf[i] == TF('\''))
        {
            strEntity += SingleQuoteEntity;
        }
        else if (pszBuf[i] == TF('\"'))
        {
            strEntity += DoubleQuoteEntity;
        }
        else
        {
            strEntity += pszBuf[i];
        }
    }
    return true;
}

INLINE bool CXMLTraits::ToChars(CString& strValue) const
{
    CString strTemp;

    PCXStr pszBuf = *strValue;
    PCXStr p = CXChar::Chr(pszBuf, TF('&'));
    if (p == nullptr)
    {
        return true;
    }

    do
    {
        if (p > pszBuf)
        {
            strTemp.AppendBuffer(pszBuf, (p - pszBuf));
        }
        if (CXChar::Cmpn(p, LessEntity, ENTITYL_LESS) == 0)
        {
            strTemp.AppendBuffer(TF('<'));
            pszBuf = p + ENTITYL_LESS;
        }
        else if (CXChar::Cmpn(p, GreaterEntity, ENTITYL_GREATER) == 0)
        {
            strTemp.AppendBuffer(TF('>'));
            pszBuf = p + ENTITYL_GREATER;
        }
        else if (CXChar::Cmpn(p, AndEntity, ENTITYL_AND) == 0)
        {
            strTemp.AppendBuffer(TF('&'));
            pszBuf = p + ENTITYL_AND;
        }
        else if (CXChar::Cmpn(p, SingleQuoteEntity, ENTITYL_SINGLE) == 0)
        {
            strTemp.AppendBuffer(TF('\''));
            pszBuf = p + ENTITYL_SINGLE;
        }
        else if (CXChar::Cmpn(p, DoubleQuoteEntity, ENTITYL_DOUBLE) == 0)
        {
            strTemp.AppendBuffer(TF('\"'));
            pszBuf = p + ENTITYL_DOUBLE;
        }
        else if (p[1] == TF('#'))
        {
            p += (TAGL_CHAR + TAGL_CHAR);
            Int nRadix = RADIXT_DEC;
            if ((p[0] == TF('x')) || (p[0] == TF('X')))
            {
                nRadix = RADIXT_HEX;
                ++p;
            }
            PXStr pszEnd = nullptr;
            XChar szChar[ENTITYL_MIN] = { 0 };
            PUInt puChar = (PUInt)szChar;
            puChar[0] = (UInt)CXChar::ToULong(p, &pszEnd, nRadix);
            if (szChar[0] <= ENTITYL_MIN_CHAR)
            {
                return false;
            }
            if ((pszEnd == nullptr) || (*pszEnd != TF(';')))
            {
                return false;
            }
            pszBuf = (pszEnd + TAGL_CHAR);
            strTemp.AppendBuffer(szChar);
        }
        p = CXChar::Chr(pszBuf, TF('&'));
    }while (p != nullptr);

    if (*pszBuf != 0)
    {
        strTemp.AppendBuffer(pszBuf);
    }
    strValue = strTemp;
    return true;
}

///////////////////////////////////////////////////////////////////
// CXMLNode
INLINE CXMLNode::CXMLNode(CXMLDocument* pDocument, XML_TYPE eType)
: m_eType(eType)
, m_pDocument(pDocument)
, m_Index(nullptr)
, m_pParent(nullptr)
, m_pFirst(nullptr)
, m_pLast(nullptr)
, m_pPrev(nullptr)
, m_pNext(nullptr)
, m_stHash(0)
{
    m_stHash = (size_t)this;
}

INLINE CXMLNode::~CXMLNode(void)
{
    assert(m_pParent == nullptr);
    assert(m_pFirst  == nullptr);
    assert(m_pLast   == nullptr);
    assert(m_pPrev   == nullptr);
    assert(m_pNext   == nullptr);
}

INLINE CXMLNode::CXMLNode(const CXMLNode&)
: m_eType(XMLT_NODE)
, m_pDocument(nullptr)
, m_Index(nullptr)
, m_pParent(nullptr)
, m_pFirst(nullptr)
, m_pLast(nullptr)
, m_pPrev(nullptr)
, m_pNext(nullptr)
, m_stHash(0)
{
}

INLINE CXMLNode& CXMLNode::operator=(const CXMLNode&)
{
    return (*this);
}

INLINE CXMLTraits::XML_TYPE CXMLNode::GetType(void) const
{
    return m_eType;
}

template <typename T, CXMLTraits::XML_TYPE eType>
INLINE T* CXMLNode::To(void)
{
    if (GetType() == eType)
    {
        return static_cast<T*>(this);
    }
    return nullptr;
}

template <typename T, CXMLTraits::XML_TYPE eType>
INLINE const T* CXMLNode::To(void) const
{
    if (GetType() == eType)
    {
        return static_cast<const T*>(this);
    }
    return nullptr;
}

template <size_t stLenT>
INLINE void CXMLNode::GetValue(CTStringFix<CXChar, stLenT>& strFixValue) const
{
    strFixValue = m_strValue;
}

INLINE void CXMLNode::GetValue(CString& strValue) const
{
    strValue = m_strValue;
}

INLINE void CXMLNode::SetValue(PCXStr pszValue)
{
    if ((pszValue != nullptr) && (*pszValue != 0))
    {
        if ((GetType() != XMLT_ELEMENT) || CheckName(pszValue))
        {
            m_strValue = pszValue;
            m_stHash   = CHash::Hash(*m_strValue, m_strValue.Length());
        }
    }
}

INLINE bool CXMLNode::HasChild(void) const
{
    return (m_pFirst != nullptr);
}

INLINE bool CXMLNode::IsRootNode(void) const
{
    return ((m_pParent != nullptr) && (m_pParent == (CXMLNode*)m_pDocument));
}

INLINE bool CXMLNode::IsChildNode(void) const
{
    return ((m_pParent != nullptr) && (m_pParent != (CXMLNode*)m_pDocument));
}

INLINE bool CXMLNode::AddFirst(CXMLNode* pNode)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == m_pDocument);
    if ((pNode != this) && (pNode->GetDocument() == m_pDocument))
    {
        if (pNode->GetParent() != nullptr)
        {
            pNode->GetParent()->Remove(pNode);
        }
        if (m_pFirst != nullptr)
        {
            assert(m_pLast != nullptr);
            pNode->m_pNext = m_pFirst;
            m_pFirst->m_pPrev = pNode;
        }
        else
        {
            assert(m_pLast == nullptr);
            pNode->m_pNext = nullptr;
            m_pLast = pNode;
        }
        m_pFirst = pNode;
        pNode->m_pParent = this;
        pNode->m_pPrev   = nullptr;
        return true;
    }
    return false;
}

INLINE bool CXMLNode::AddLast(CXMLNode* pNode)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == m_pDocument);
    if ((pNode != this) && (pNode->GetDocument() == m_pDocument))
    {
        if (pNode->GetParent() != nullptr)
        {
            pNode->GetParent()->Remove(pNode);
        }
        if (m_pFirst != nullptr)
        {
            assert(m_pLast != nullptr);
            pNode->m_pPrev = m_pLast;
            m_pLast->m_pNext = pNode;
        }
        else
        {
            assert(m_pLast == nullptr);
            pNode->m_pPrev = nullptr;
            m_pFirst = pNode;
        }
        m_pLast = pNode;
        pNode->m_pParent = this;
        pNode->m_pNext   = nullptr;
        return true;
    }
    return false;
}

INLINE CXMLNode* CXMLNode::RemoveFirst(void)
{
    if (m_pFirst != nullptr)
    {
        CXMLNode* pNode = m_pFirst;
        m_pFirst = pNode->m_pNext;
        if (m_pFirst != nullptr)
        {
            m_pFirst->m_pPrev = nullptr;
        }
        else
        {
            m_pLast = nullptr;
        }
        pNode->m_pParent = nullptr;
        return pNode;
    }
    return nullptr;
}

INLINE CXMLNode* CXMLNode::RemoveLast(void)
{
    if (m_pFirst != nullptr)
    {
        CXMLNode* pNode = m_pLast;
        if (pNode->m_pPrev != nullptr)
        {
            m_pLast = pNode->m_pPrev;
            m_pLast->m_pNext = nullptr;
        }
        else
        {
            m_pFirst = nullptr;
            m_pLast  = nullptr;
        }
        pNode->m_pParent = nullptr;
        return pNode;
    }
    return nullptr;
}

INLINE bool CXMLNode::Remove(CXMLNode* pNode)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == m_pDocument);
    if ((pNode != nullptr) && (pNode->GetParent() == this))
    {
        if (pNode == m_pFirst)
        {
            return (RemoveFirst() != nullptr);
        }
        else if (pNode == m_pLast)
        {
            return (RemoveLast() != nullptr);
        }
        else
        {
            assert(pNode->m_pPrev != nullptr);
            assert(pNode->m_pNext != nullptr);
            pNode->m_pPrev->m_pNext = pNode->m_pNext;
            pNode->m_pNext->m_pPrev = pNode->m_pPrev;
            pNode->m_pParent = nullptr;
        }
        return true;
    }
    return false;
}

INLINE void CXMLNode::RemoveAll(void)
{
    if (m_pFirst != nullptr)
    {
        CXMLNode* pNext = nullptr;
        CXMLNode* pNode = m_pFirst;
        do
        {
            pNext = pNode->m_pNext;

            pNode->m_pPrev   = nullptr;
            pNode->m_pNext   = nullptr;
            pNode->m_pParent = nullptr;

            pNode = pNext;
        } while (pNode != nullptr);

        m_pFirst = nullptr;
        m_pLast  = nullptr;
    }
}

INLINE bool CXMLNode::InsertBefore(CXMLNode* pNode, CXMLNode* pBefore)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == m_pDocument);
    assert(pBefore != nullptr);
    assert(pBefore->GetDocument() == m_pDocument);

    if ((pNode != this) &&
        (pNode->GetDocument() == m_pDocument) &&
        (pNode != pBefore) &&
        (pBefore->GetParent() == this))
    {
        if (pNode->GetParent() != nullptr)
        {
            pNode->GetParent()->Remove(pNode);
        }

        if (pBefore == m_pFirst)
        {
            return AddFirst(pNode);
        }
        else
        {
            assert(pBefore->m_pPrev != nullptr);
            pNode->m_pPrev = pBefore->m_pPrev;
            pNode->m_pNext = pBefore;
            pBefore->m_pPrev->m_pNext = pNode;
            pBefore->m_pPrev = pNode;

            pNode->m_pParent = nullptr;
        }
        return true;
    }
    return false;
}

INLINE bool CXMLNode::InsertAfter(CXMLNode* pNode, CXMLNode* pAfter)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == m_pDocument);
    assert(pAfter != nullptr);
    assert(pAfter->GetDocument() == m_pDocument);

    if ((pNode != this) &&
        (pNode->GetDocument() == m_pDocument) &&
        (pNode != pAfter) &&
        (pAfter->GetParent() == this))
    {
        if (pNode->GetParent() != nullptr)
        {
            pNode->GetParent()->Remove(pNode);
        }

        if (pAfter == m_pLast)
        {
            return AddLast(pNode);
        }
        else
        {
            assert(pAfter->m_pNext != nullptr);
            pNode->m_pNext = pAfter->m_pNext;
            pNode->m_pPrev = pAfter;
            pAfter->m_pNext->m_pPrev = pNode;
            pAfter->m_pNext = pNode;

            pNode->m_pParent = nullptr;
        }
        return true;
    }
    return false;
}

INLINE CXMLNode* CXMLNode::Find(PCXStr pszValue, CXMLNode* pStartAfter) const
{
    if (pszValue != nullptr)
    {
        size_t stHash = CHash::Hash(pszValue);

        CXMLNode* pNode = m_pFirst;
        if ((pStartAfter != nullptr) &&
            (pStartAfter->GetParent() == this))
        {
            pNode = pStartAfter->m_pNext;
        }
        while (pNode != nullptr)
        {
            if (stHash == pNode->m_stHash)
            {
                return pNode;
            }
            pNode = pNode->m_pNext;
        }
    }
    return nullptr;
}

INLINE CXMLDocument* CXMLNode::GetDocument(void)
{
    return m_pDocument;
}

INLINE const CXMLDocument* CXMLNode::GetDocument(void) const
{
    return m_pDocument;
}

INLINE CXMLNode* CXMLNode::GetParent(void)
{
    return m_pParent;
}

INLINE const CXMLNode* CXMLNode::GetParent(void) const
{
    return m_pParent;
}

INLINE CXMLNode* CXMLNode::GetFirst(void)
{
    return m_pFirst;
}

INLINE const CXMLNode* CXMLNode::GetFirst(void) const
{
    return m_pFirst;
}

INLINE CXMLNode* CXMLNode::GetLast(void)
{
    return m_pLast;
}

INLINE const CXMLNode* CXMLNode::GetLast(void) const
{
    return m_pLast;
}

INLINE CXMLNode* CXMLNode::GetPrev(void)
{
    return m_pPrev;
}

INLINE const CXMLNode* CXMLNode::GetPrev(void) const
{
    return m_pPrev;
}

INLINE CXMLNode* CXMLNode::GetNext(void)
{
    return m_pNext;
}

INLINE const CXMLNode* CXMLNode::GetNext(void) const
{
    return m_pNext;
}

INLINE bool CXMLNode::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        CXMLNode* pNode = nullptr;
        if (DetectNode(pszBuf, pNode) == false)
        {
            return false;
        }
        DEV_TRACE(TF("CXMLNode::ParseNode create node type : %d"), pNode->GetType());
        if (pNode->ParseNode(pszBuf) == false)
        {
            m_pDocument->DestroyNode(pNode);
            return false;
        }
        return AddLast(pNode);
    }
    return false;
}

INLINE bool CXMLNode::DetectNode(PXStr& pszBuf, CXMLNode*& pNode)
{
    if (pszBuf[0] == TF('<'))
    {
        pNode = nullptr;
        if (pszBuf[1] == TF('!')) // dtd, cdata or comment
        {
            if (CXChar::Cmpn(pszBuf, CommentTag, TAGL_COMMENT) == 0)
            {
                CXMLComment* pComment = m_pDocument->CreateComment(nullptr);
                pNode = pComment;
                pszBuf += TAGL_COMMENT;
            }
            else if (CXChar::Cmpn(pszBuf, CDataTag, TAGL_CDATA) == 0)
            {
                CXMLText* pText =  m_pDocument->CreateText(nullptr);
                pNode = pText;
                pText->SetCData(true);
                pszBuf += TAGL_CDATA;
            }
            else
            {
                CXMLUnknown* pUnknown =  m_pDocument->CreateUnknown(nullptr);
                pNode = pUnknown;
                pszBuf += TAGL_UNKNOWN;
            }
        }
        else if (pszBuf[1] == TF('?'))
        {
            if (CXChar::Cmpn(pszBuf, DeclarationTag, TAGL_DECL) == 0)
            {
                CXMLDeclaration* pDeclaration = m_pDocument->CreateDeclaration();
                pNode = pDeclaration;
                pszBuf += TAGL_DECL;
            }
            else
            {
                CXMLUnknown* pUnknown =  m_pDocument->CreateUnknown(nullptr);
                pUnknown->SetDtd(false);
                pNode = pUnknown;
                pszBuf += TAGL_UNKNOWN;
            }
        }
        else // element
        {
            CXMLElement* pElement =  m_pDocument->CreateElement(nullptr);
            pNode = pElement;
            pszBuf += TAGL_CHAR;
        }
    }
    else // text
    {
        CXMLText* pText =  m_pDocument->CreateText(nullptr);
        pNode = pText;
    }
    return (pNode != nullptr);
}

INLINE void CXMLNode::Destroy(bool bSelfOnly)
{
    if (bSelfOnly == false)
    {
        CXMLNode* pNext = nullptr;
        CXMLNode* pNode = m_pFirst;
        do
        {
            pNext = pNode->m_pNext;
            m_pDocument->DestroyNode(pNode);
            pNode = pNext;
        } while (pNode != nullptr);
    }
    m_pParent = nullptr;
    m_pFirst  = nullptr;
    m_pLast   = nullptr;
    m_pPrev   = nullptr;
    m_pNext   = nullptr;
}

///////////////////////////////////////////////////////////////////
// CXMLDocument
INLINE CXMLDocument::CXMLDocument(void)
: CXMLNode(nullptr, XMLT_DOCUMENT)
, m_eENCODING(ENT_LOCAL)
{
    m_pDocument = this;
}

INLINE CXMLDocument::~CXMLDocument(void)
{
    RemoveAll();
}

INLINE bool CXMLDocument::Load(CStream& stream, size_t stSize, ENCODING_TYPE eENCODING)
{
    RemoveAll();
    assert(stream.IsRead());
    size_t stLen = stream.Rest();
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            stream.Read(pBuf, stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE bool CXMLDocument::Load(CString& strSrc, size_t stSize, ENCODING_TYPE eENCODING)
{
    RemoveAll();
    size_t stLen = strSrc.Length() * sizeof(XChar);
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            MM_SAFE::Cpy(pBuf, stLen, strSrc.GetBuffer(), stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE bool CXMLDocument::Load(PCXStr pszSrc, size_t stSize, ENCODING_TYPE eENCODING)
{
    RemoveAll();
    size_t stLen = CXChar::Length(pszSrc) * sizeof(XChar);
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            MM_SAFE::Cpy(pBuf, stLen, pszSrc, stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE CXMLElement* CXMLDocument::CreateElement(PCXStr pszName)
{
    CXMLElement* pNode = MNEW CXMLElement(this);
    if (pNode != nullptr)
    {
        pNode->SetValue(pszName);
        pNode->m_Index = m_XMLNode.AddTail(pNode);
    }
    return pNode;
}

INLINE CXMLComment* CXMLDocument::CreateComment(PCXStr pszComment)
{
    CXMLComment* pNode = MNEW CXMLComment(this);
    if (pNode != nullptr)
    {
        pNode->SetValue(pszComment);
        pNode->m_Index = m_XMLNode.AddTail(pNode);
    }
    return pNode;
}

INLINE CXMLText* CXMLDocument::CreateText(PCXStr pszText)
{
    CXMLText* pNode = MNEW CXMLText(this);
    if (pNode != nullptr)
    {
        pNode->SetValue(pszText);
        pNode->m_Index = m_XMLNode.AddTail(pNode);
    }
    return pNode;
}

INLINE CXMLDeclaration* CXMLDocument::CreateDeclaration(PCXStr pszDeclaration)
{
    CXMLDeclaration* pNode = MNEW CXMLDeclaration(this);
    if (pNode != nullptr)
    {
        pNode->SetValue(pszDeclaration);
        pNode->m_Index = m_XMLNode.AddTail(pNode);
    }
    return pNode;
}

INLINE CXMLUnknown* CXMLDocument::CreateUnknown(PCXStr pszUnknown)
{
    CXMLUnknown* pNode = MNEW CXMLUnknown(this);
    if (pNode != nullptr)
    {
        pNode->SetValue(pszUnknown);
        pNode->m_Index = m_XMLNode.AddTail(pNode);
    }
    return pNode;
}

INLINE bool CXMLDocument::DestroyNode(CXMLNode* pNode)
{
    assert(pNode != nullptr);
    assert(pNode->GetDocument() == this);
    if ((pNode != this) && Remove(pNode))
    {
        pNode->Destroy();
        RemoveNode(pNode);
        return true;
    }
    return false;
}

INLINE ENCODING_TYPE CXMLDocument::GetEncoding(void) const
{
    return m_eENCODING;
}

INLINE CXMLNode* CXMLDocument::Clone(CXMLDocument&) const
{
    return nullptr;
}

INLINE bool CXMLDocument::Equal(const CXMLNode&) const
{
    return false;
}

template <size_t stLenT>
INLINE bool CXMLDocument::Save(CTStringFix<CXChar, stLenT>& strFixBuf, ENCODING_TYPE eENCODING) const
{
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        size_t stSize = Size();
        if (stSize < stLenT)
        {
            CString strBuf;
            strBuf.SetBufferLength(Size());
            for (CXMLNode* pNode = m_pFirst; pNode != nullptr; pNode = pNode->GetNext())
            {
                pNode->Save(strBuf, eENCODING);
            }
            strFixBuf = strBuf;
            return true;
        }
    }
    return false;
}

INLINE void CXMLDocument::Save(CString& strBuf, ENCODING_TYPE eENCODING) const
{
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        strBuf.SetBufferLength(Size());
        for (CXMLNode* pNode = m_pFirst; pNode != nullptr; pNode = pNode->GetNext())
        {
            pNode->Save(strBuf, eENCODING);
        }
    }
}

INLINE size_t CXMLDocument::Size(void) const
{
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);
        size_t stSize = 0;
        for (CXMLNode* pNode = m_pFirst; pNode != nullptr; pNode = pNode->GetNext())
        {
            stSize += pNode->Size();
        }
        return stSize;
    }
    return 0;
}

INLINE void CXMLDocument::RemoveNode(CXMLNode* pNode)
{
    m_XMLNode.RemoveAt(pNode->m_Index);
    MDELETE pNode;
}

INLINE void CXMLDocument::RemoveAll(void)
{
    PINDEX index = m_XMLNode.GetHeadIndex();
    while (index != nullptr)
    {
        CXMLNode* pNode = m_XMLNode.GetNext(index);
        pNode->Destroy(true);
        MDELETE pNode;
    }
    m_XMLNode.RemoveAll();
    m_pParent = nullptr;
    m_pFirst  = nullptr;
    m_pLast   = nullptr;
    m_pPrev   = nullptr;
    m_pNext   = nullptr;
    DEV_TRACE(TF("CXMLDocument::RemoveAll"));
}

INLINE bool CXMLDocument::Parse(PXStr pszBuf)
{
    RemoveAll();
    while ((pszBuf != nullptr) && (*pszBuf != 0))
    {
        if (ParseNode(pszBuf) == false)
        {
            break;
        }
    }
    return (m_XMLNode.GetSize() > 0);
}

///////////////////////////////////////////////////////////////////
// CXMLElement
INLINE CXMLElement::CXMLElement(CXMLDocument* pDocument)
: CXMLNode(pDocument, XMLT_ELEMENT)
, m_pFirstAttribute(nullptr)
, m_pLastAttribute(nullptr)
{
}

INLINE CXMLElement::~CXMLElement(void)
{
    assert(m_pFirstAttribute == nullptr);
    assert(m_pLastAttribute  == nullptr);
}

template <size_t stLenT>
INLINE void CXMLElement::GetName(CTStringFix<CXChar, stLenT>& strFixName) const
{
    GetValue<stLenT>(strFixName);
}

INLINE void CXMLElement::GetName(CString& strName) const
{
    GetValue(strName);
}

INLINE void CXMLElement::SetName(PCXStr pszName)
{
    SetValue(pszName);
}

INLINE bool CXMLElement::HasAttribute(PCXStr pszName, PCXStr pszValue) const
{
    if (pszName == nullptr)
    {
        return (m_pFirstAttribute != nullptr);
    }
    else if (m_pFirstAttribute != nullptr)
    {
        size_t stName  = CHash::Hash(pszName);
        size_t stValue = 0;
        if (pszValue != nullptr)
        {
            stValue = CHash::Hash(pszValue);
        }
        for (CXMLAttribute* pAttr = m_pFirstAttribute; pAttr != nullptr; pAttr = pAttr->m_pNext)
        {
            if (stName == pAttr->m_stName)
            {
                if ((stValue == 0) || (stValue == pAttr->m_stValue))
                {
                    return true;
                }
            }
        }
    }
    return false;
}

template <size_t stLenT>
INLINE bool CXMLElement::GetAttribute(PCXStr pszName, CTStringFix<CXChar, stLenT>& strFixValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue<stLenT>(strFixValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, CString& strValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(strValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, Short& sValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(sValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, UShort& usValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(usValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, Int& nValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(nValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, UInt& uValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(uValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, Long& lValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(lValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, ULong& ulValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(ulValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, LLong& llValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(llValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, ULLong& ullValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(ullValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::GetAttribute(PCXStr pszName, Double& dValue) const
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->GetValue(dValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, PCXStr pszValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(pszValue);
        }
        else
        {
            return (AddAttributeAtLast(pszName, pszValue) != nullptr);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, Short sValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(sValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, UShort usValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(usValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, Int nValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(nValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, UInt uValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(uValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, Long lValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(lValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, ULong ulValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(ulValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, LLong llValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(llValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, ULLong ullValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(ullValue);
        }
    }
    return false;
}

INLINE bool CXMLElement::SetAttribute(PCXStr pszName, Double dValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName);
        }
        if (pAttr != nullptr)
        {
            return pAttr->SetValue(dValue);
        }
    }
    return false;
}

INLINE CXMLAttribute* CXMLElement::AddFirstAttribute(PCXStr pszName, PCXStr pszValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtFirst(pszName, pszValue);
        }
        else
        {
            if (pszValue != nullptr)
            {
                pAttr->SetValue(pszValue);
            }
            MoveAttributeToFirst(pAttr);
        }
        return pAttr;
    }
    return nullptr;
}

INLINE CXMLAttribute* CXMLElement::AddLastAttribute(PCXStr pszName, PCXStr pszValue)
{
    if (pszName != nullptr)
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = AddAttributeAtLast(pszName, pszValue);
        }
        else
        {
            if (pszValue != nullptr)
            {
                pAttr->SetValue(pszValue);
            }
            MoveAttributeToLast(pAttr);
        }
        return pAttr;
    }
    return nullptr;
}

INLINE bool CXMLElement::MoveAttributeToFirst(CXMLAttribute* pAttr)
{
    assert(pAttr != nullptr);
    assert(pAttr->GetElement() == this);
    if (pAttr->GetElement() == this)
    {
        assert(m_pFirstAttribute != nullptr);
        if (pAttr != m_pFirstAttribute)
        {
            RemoveAttributeList(pAttr);
            if (m_pFirstAttribute != nullptr)
            {
                assert(m_pLastAttribute != nullptr);
                pAttr->m_pNext = m_pFirstAttribute;
                m_pFirstAttribute->m_pPrev = pAttr;
            }
            else
            {
                assert(m_pLastAttribute == nullptr);
                pAttr->m_pNext   = nullptr;
                m_pLastAttribute = pAttr;
            }
            pAttr->m_pPrev    = nullptr;
            m_pFirstAttribute = pAttr;
        }
        return true;
    }
    return false;
}

INLINE bool CXMLElement::MoveAttributeToLast(CXMLAttribute* pAttr)
{
    assert(pAttr != nullptr);
    assert(pAttr->GetElement() == this);
    if (pAttr->GetElement() == this)
    {
        assert(m_pFirstAttribute != nullptr);
        if (pAttr != m_pLastAttribute)
        {
            RemoveAttributeList(pAttr);
            if (m_pFirstAttribute != nullptr)
            {
                assert(m_pLastAttribute != nullptr);
                pAttr->m_pPrev = m_pLastAttribute;
                m_pLastAttribute->m_pNext = pAttr;
            }
            else
            {
                assert(m_pLastAttribute == nullptr);
                pAttr->m_pPrev    = nullptr;
                m_pFirstAttribute = pAttr;
            }
            pAttr->m_pNext   = nullptr;
            m_pLastAttribute = pAttr;
        }
        return true;
    }
    return false;
}

INLINE bool CXMLElement::RemoveFirstAttribute(void)
{
    if (m_pFirstAttribute != nullptr)
    {
        CXMLAttribute* pAttr = m_pFirstAttribute;
        m_pFirstAttribute = pAttr->m_pNext;
        if (m_pFirstAttribute != nullptr)
        {
            m_pFirstAttribute->m_pPrev = nullptr;
        }
        else
        {
            m_pLastAttribute = nullptr;
        }
        MDELETE pAttr;
        return true;
    }
    return false;
}

INLINE bool CXMLElement::RemoveLastAttribute(void)
{
    if (m_pFirstAttribute != nullptr)
    {
        CXMLAttribute* pAttr = m_pLastAttribute;
        if (pAttr->m_pPrev != nullptr)
        {
            m_pLastAttribute = pAttr->m_pPrev;
            m_pLastAttribute->m_pNext = nullptr;
        }
        else
        {
            m_pFirstAttribute = nullptr;
            m_pLastAttribute  = nullptr;
        }
        MDELETE pAttr;
        return true;
    }
    return false;
}

INLINE bool CXMLElement::RemoveAttribute(PCXStr pszName)
{
    if ((pszName != nullptr) && (m_pFirstAttribute != nullptr))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr != nullptr)
        {
            return RemoveAttribute(pAttr);
        }
    }
    return false;
}

INLINE bool CXMLElement::RemoveAttribute(CXMLAttribute* pAttr)
{
    assert(pAttr != nullptr);
    assert(pAttr->GetElement() == this);
    if (pAttr->GetElement() == this)
    {
        assert(m_pFirstAttribute != nullptr);
        if (pAttr == m_pFirstAttribute)
        {
            return RemoveFirstAttribute();
        }
        else if (pAttr == m_pLastAttribute)
        {
            return RemoveLastAttribute();
        }
        assert(pAttr->m_pPrev != nullptr);
        assert(pAttr->m_pNext != nullptr);
        pAttr->m_pPrev->m_pNext = pAttr->m_pNext;
        pAttr->m_pNext->m_pPrev = pAttr->m_pPrev;
        MDELETE pAttr;
        return true;
    }
    return false;
}

INLINE void CXMLElement::RemoveAllAttribute(void)
{
    if (m_pFirstAttribute != nullptr)
    {
        CXMLAttribute* pNext = nullptr;
        CXMLAttribute* pAttr = m_pFirstAttribute;
        do
        {
            pNext = pAttr->m_pNext;

            MDELETE pAttr;

            pAttr = pNext;
        } while (pAttr != nullptr);

        m_pFirstAttribute = nullptr;
        m_pLastAttribute  = nullptr;
    }
}

INLINE bool CXMLElement::InsertAttributeBefore(PCXStr pszName, PCXStr pszValue, CXMLAttribute* pBefore)
{
    assert(pBefore != nullptr);
    assert(pBefore->GetElement() == this);

    if (CheckName(pszName) &&
        (pBefore != nullptr) &&
        (pBefore->GetElement() == this))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = MNEW CXMLAttribute(this, pszName, pszValue);
        }
        else if (pszValue != nullptr)
        {
            pAttr->SetValue(pszValue);
        }
        if (pAttr != nullptr)
        {
            return InsertAttributeBefore(pAttr, pBefore);
        }
    }
    return false;
}

INLINE bool CXMLElement::InsertAttributeAfter(PCXStr pszName, PCXStr pszValue, CXMLAttribute* pAfter)
{
    assert(pAfter != nullptr);
    assert(pAfter->GetElement() == this);

    if (CheckName(pszName) &&
        (pAfter != nullptr) &&
        (pAfter->GetElement() == this))
    {
        CXMLAttribute* pAttr = FindAttribute(pszName);
        if (pAttr == nullptr)
        {
            pAttr = MNEW CXMLAttribute(this, pszName, pszValue);
        }
        else if (pszValue != nullptr)
        {
            pAttr->SetValue(pszValue);
        }
        if (pAttr != nullptr)
        {
            return InsertAttributeAfter(pAttr, pAfter);
        }
    }
    return false;
}

INLINE bool CXMLElement::InsertAttributeBefore(CXMLAttribute* pAttr, CXMLAttribute* pBefore)
{
    assert(pAttr != nullptr);
    assert(pAttr->GetElement() == this);
    assert(pBefore != nullptr);
    assert(pBefore->GetElement() == this);

    if ((pAttr != pBefore) && (pAttr->GetElement() == pBefore->GetElement()))
    {
        if (pBefore == m_pFirstAttribute)
        {
            return MoveAttributeToFirst(pAttr);
        }
        else
        {
            RemoveAttributeList(pAttr);
            assert(pBefore->m_pPrev != nullptr);
            pAttr->m_pPrev = pBefore->m_pPrev;
            pAttr->m_pNext = pBefore;
            pBefore->m_pPrev->m_pNext = pAttr;
            pBefore->m_pPrev = pAttr;
        }
        return true;
    }
    return false;
}

INLINE bool CXMLElement::InsertAttributeAfter(CXMLAttribute* pAttr, CXMLAttribute* pAfter)
{
    assert(pAttr != nullptr);
    assert(pAttr->GetElement() == this);
    assert(pAfter != nullptr);
    assert(pAfter->GetElement() == this);

    if ((pAttr != pAfter) && (pAttr->GetElement() == pAfter->GetElement()))
    {
        if (pAfter == m_pLastAttribute)
        {
            return MoveAttributeToLast(pAttr);
        }
        else
        {
            assert(pAfter->m_pNext != nullptr);
            pAttr->m_pNext = pAfter->m_pNext;
            pAttr->m_pPrev = pAfter;
            pAfter->m_pNext->m_pPrev = pAttr;
            pAfter->m_pNext = pAttr;
        }
        return true;
    }
    return false;
}

INLINE CXMLAttribute* CXMLElement::FindAttribute(PCXStr pszName, CXMLAttribute* pStartAfter) const
{
    if (pszName != nullptr)
    {
        size_t stHash = CHash::Hash(pszName);

        CXMLAttribute* pAttr = m_pFirstAttribute;
        if ((pStartAfter != nullptr) && (pStartAfter->GetElement() == this))
        {
            pAttr = pStartAfter->m_pNext;
        }
        while (pAttr != nullptr)
        {
            if (stHash == pAttr->m_stName)
            {
                return pAttr;
            }
            pAttr = pAttr->m_pNext;
        }
    }
    return nullptr;
}

INLINE CXMLAttribute* CXMLElement::GetFirstAttribute(void)
{
    return m_pFirstAttribute;
}

INLINE const CXMLAttribute* CXMLElement::GetFirstAttribute(void) const
{
    return m_pFirstAttribute;
}

INLINE CXMLAttribute* CXMLElement::GetLastAttribute(void)
{
    return m_pLastAttribute;
}

INLINE const CXMLAttribute* CXMLElement::GetLastAttribute(void) const
{
    return m_pLastAttribute;
}

INLINE CXMLNode* CXMLElement::Clone(CXMLDocument& aSrc) const
{
    CXMLElement* pNode = aSrc.CreateElement(*m_strValue);
    if (pNode != nullptr)
    {
        for (CXMLAttribute* pAttr = m_pFirstAttribute; pAttr != nullptr; pAttr = pAttr->m_pNext)
        {
            pNode->AddLastAttribute(*(pAttr->m_strName), *(pAttr->m_strValue));
        }
        return pNode;
    }
    return nullptr;
}

INLINE bool CXMLElement::Equal(const CXMLNode& aSrc) const
{
    if (&aSrc == this)
    {
        return true;
    }
    const CXMLElement* pNode = aSrc.To<CXMLElement, XMLT_ELEMENT>();
    if ((pNode == nullptr) || (pNode->m_strValue != m_strValue))
    {
        return false;
    }

    CXMLAttribute* pAttr1 = m_pFirstAttribute;
    CXMLAttribute* pAttr2 = pNode->m_pFirstAttribute;
    while ((pAttr1 != nullptr) && (pAttr2 != nullptr))
    {
        if (pAttr1->m_stName != pAttr2->m_stName)
        {
            return false;
        }
        if (pAttr1->m_stValue != pAttr2->m_stValue)
        {
            return false;
        }
    }
    if ((pAttr1 != nullptr) || (pAttr2 != nullptr))
    {
        return false;
    }
    return true;
}

INLINE void CXMLElement::Save(CString& strBuf, ENCODING_TYPE eENCODING) const
{
    strBuf += TF('<');
    strBuf += m_strValue;
    for (CXMLAttribute* pAttr = m_pFirstAttribute; pAttr != nullptr; pAttr = pAttr->m_pNext)
    {
        pAttr->Save(strBuf, eENCODING);
    }
    if (HasChild())
    {
        strBuf += TF('>');
        for (CXMLNode* pNode = m_pFirst; pNode != nullptr; pNode = pNode->GetNext())
        {
            pNode->Save(strBuf, eENCODING);
        }
        strBuf += TF("</");
        strBuf += m_strValue;
        strBuf += TF('>');
    }
    else
    {
        strBuf += TF("/>");
    }
    strBuf += TF("\r\n");
}

INLINE size_t CXMLElement::Size(void) const
{
    size_t stSize = TAGL_CHAR_MAX + m_strValue.Length();
    stSize += stSize;

    for (CXMLAttribute* pAttr = m_pFirstAttribute; pAttr != nullptr; pAttr = pAttr->m_pNext)
    {
        stSize += pAttr->Size();
    }
    for (CXMLNode* pNode = m_pFirst; pNode != nullptr; pNode = pNode->GetNext())
    {
        stSize += pNode->Size();
    }
    return stSize;
}

INLINE CXMLAttribute* CXMLElement::AddAttributeAtFirst(PCXStr pszName, PCXStr pszValue)
{
    if (CheckName(pszName))
    {
        CXMLAttribute* pAttr = MNEW CXMLAttribute(this, pszName, pszValue);
        if (pAttr != nullptr)
        {
            if (m_pFirstAttribute != nullptr)
            {
                assert(m_pLastAttribute != nullptr);
                pAttr->m_pNext = m_pFirstAttribute;
                m_pFirstAttribute->m_pPrev = pAttr;
            }
            else
            {
                assert(m_pLastAttribute == nullptr);
                pAttr->m_pNext   = nullptr;
                m_pLastAttribute = pAttr;
            }
            pAttr->m_pPrev = nullptr;
            m_pFirstAttribute = pAttr;
        }
        return pAttr;
    }
    return nullptr;
}

INLINE CXMLAttribute* CXMLElement::AddAttributeAtLast(PCXStr pszName, PCXStr pszValue)
{
    if (CheckName(pszName))
    {
        CXMLAttribute* pAttr = MNEW CXMLAttribute(this, pszName, pszValue);
        if (pAttr != nullptr)
        {
            if (m_pFirstAttribute != nullptr)
            {
                assert(m_pLastAttribute != nullptr);
                pAttr->m_pPrev = m_pLastAttribute;
                m_pLastAttribute->m_pNext = pAttr;
            }
            else
            {
                assert(m_pLastAttribute == nullptr);
                pAttr->m_pPrev = nullptr;
                m_pFirstAttribute = pAttr;
            }
            pAttr->m_pNext = nullptr;
            m_pLastAttribute = pAttr;
        }
    }
    return nullptr;
}

INLINE void CXMLElement::RemoveAttributeList(CXMLAttribute* pAttr)
{
    if (pAttr == m_pFirstAttribute)
    {
        m_pFirstAttribute = pAttr->m_pNext;
        if (m_pFirstAttribute != nullptr)
        {
            m_pFirstAttribute->m_pPrev = nullptr;
        }
        else
        {
            m_pLastAttribute = nullptr;
        }
    }
    else if (pAttr == m_pLastAttribute)
    {
        if (pAttr->m_pPrev != nullptr)
        {
            m_pLastAttribute = pAttr->m_pPrev;
            m_pLastAttribute->m_pNext = nullptr;
        }
        else
        {
            m_pFirstAttribute = nullptr;
        }
    }
    else
    {
        assert(pAttr->m_pPrev != nullptr);
        assert(pAttr->m_pNext != nullptr);
        pAttr->m_pPrev->m_pNext = pAttr->m_pNext;
        pAttr->m_pNext->m_pPrev = pAttr->m_pPrev;
    }
    pAttr->m_pPrev = nullptr;
    pAttr->m_pNext = nullptr;
}

INLINE bool CXMLElement::ParseAttributes(PXStr& pszBuf)
{
    CString strName;
    CString strValue;
    while (SkipChars(pszBuf))
    {
        if ((pszBuf[0] == TF('>')) || (pszBuf[0] == TF('/')))
        {
            return true;
        }
        if (ToName(pszBuf, strName) == false)
        {
            break;
        }
        if (SkipChars(pszBuf) == false)
        {
            break;
        }
        if (pszBuf[0] != TF('='))
        {
            break;
        }
        ++pszBuf;
        if (SkipChars(pszBuf) == false)
        {
            break;
        }
        if ((pszBuf[0] != TF('\'')) && (pszBuf[0] != TF('\"')))
        {
            break;
        }
        XChar cQuote = pszBuf[0];
        ++pszBuf;
        PCXStr p = CXChar::Chr(pszBuf, cQuote);
        if (p == nullptr)
        {
            break;
        }
        if (p > pszBuf)
        {
            strValue.FillBuffer(pszBuf, (p - pszBuf));
            ToChars(strValue);
        }
        else
        {
            strValue.Empty();
        }

        pszBuf = (PXStr)p;
        ++pszBuf;

        DEV_TRACE(TF("CXMLElement::ParseAttributes  %s=\"%s\""), *strName, *strValue);

        AddLastAttribute(*strName, *strValue);
    }
    return false;
}

INLINE bool CXMLElement::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        if (ToName(pszBuf, m_strValue) == false)
        {
            return false;
        }
        m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());
        DEV_TRACE(TF("CXMLElement::ParseNode name %s"), *m_strValue);

        if (ParseAttributes(pszBuf) == false)
        {
            return false;
        }
        if (pszBuf[0] == TF('>'))
        {
            pszBuf += TAGL_CHAR;
            while (SkipChars(pszBuf))
            {
                if ((pszBuf[0] == TF('<')) && (pszBuf[1] == TF('/')))
                {    // close tag
                    pszBuf += (TAGL_CHAR + TAGL_CHAR);
                    CString strCloseTag;
                    if (ToName(pszBuf, strCloseTag) && (strCloseTag == m_strValue))
                    {
                        if (SkipChars(pszBuf) && (pszBuf[0] == TF('>')))
                        {
                            DEV_TRACE(TF("CXMLElement::ParseNode close tag %s find"), *m_strValue);
                            pszBuf += TAGL_CHAR;
                            return true;
                        }
                    }
                    DEV_TRACE(TF("CXMLElement::ParseNode close tag name mismatch %s <> %s"), *strCloseTag, *m_strValue);
                    break;
                }
                else if (CXMLNode::ParseNode(pszBuf) == false)
                {
                    break;
                }
            }
        }
        else if ((pszBuf[0] == TF('/')) && (pszBuf[1] == TF('>')))
        {
            DEV_TRACE(TF("CXMLElement::ParseNode close with /"));
            pszBuf += (TAGL_CHAR + TAGL_CHAR);
            return true;
        }
    }
    return false;
}

INLINE void CXMLElement::Destroy(bool bSelfOnly)
{
    RemoveAllAttribute();
    CXMLNode::Destroy(bSelfOnly);
}

///////////////////////////////////////////////////////////////////
// CXMLComment
INLINE CXMLComment::CXMLComment(CXMLDocument* pDocument)
: CXMLNode(pDocument, XMLT_COMMENT)
{
}

INLINE CXMLComment::~CXMLComment(void)
{
}

INLINE CXMLNode* CXMLComment::Clone(CXMLDocument& aSrc) const
{
    return aSrc.CreateComment(*m_strValue);
}

INLINE bool CXMLComment::Equal(const CXMLNode& aSrc) const
{
    if (&aSrc == this)
    {
        return true;
    }
    const CXMLComment* pNode = aSrc.To<CXMLComment, XMLT_COMMENT>();
    if ((pNode == nullptr) || (pNode->m_strValue != m_strValue))
    {
        return false;
    }
    return true;
}

INLINE void CXMLComment::Save(CString& strBuf, ENCODING_TYPE) const
{
    strBuf += CommentTag;
    strBuf += m_strValue;
    strBuf += CommentEndTag;
    strBuf += TF("\r\n");
}

INLINE size_t CXMLComment::Size(void) const
{
    return (TAGL_COMMENT + TAGL_COMMENT_END + TAGL_CHAR_MAX + m_strValue.Length());
}

INLINE bool CXMLComment::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        PCXStr p = CXChar::Str(pszBuf, CommentEndTag);
        if (p != nullptr)
        {
            if (p > pszBuf)
            {    // do nothing, copy string only
                m_strValue.FillBuffer(pszBuf, (p - pszBuf));
                m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());
            }
            DEV_TRACE(TF("CXMLComment::ParseNode value %s"), *m_strValue);
            pszBuf = (PXStr)(p + TAGL_COMMENT_END);
            return true;
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_TRACE(TF("CXMLComment::ParseNode find end string %s failed"), CommentEndTag);
        }
#endif
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLText
INLINE CXMLText::CXMLText(CXMLDocument* pDocument)
: CXMLNode(pDocument, XMLT_TEXT)
, m_bCData(false)
{
}

INLINE CXMLText::~CXMLText(void)
{
}

INLINE bool CXMLText::IsCData(void) const
{
    return m_bCData;
}

INLINE void CXMLText::SetCData(bool bCData)
{
    m_bCData = bCData;
}

INLINE CXMLNode* CXMLText::Clone(CXMLDocument& aSrc) const
{
    return aSrc.CreateText(*m_strValue);
}

INLINE bool CXMLText::Equal(const CXMLNode& aSrc) const
{
    if (&aSrc == this)
    {
        return true;
    }
    const CXMLText* pNode = aSrc.To<CXMLText, XMLT_TEXT>();
    if ((pNode == nullptr) || (pNode->m_strValue != m_strValue))
    {
        return false;
    }
    return true;
}

INLINE void CXMLText::Save(CString& strBuf, ENCODING_TYPE) const
{
    if (m_bCData)
    {
        strBuf += CDataTag;
        strBuf += m_strValue;
        strBuf += CDataEndTag;
    }
    else
    {
        CString strEntity;
        ToEntity(m_strValue, strEntity);
        strBuf += strEntity;
    }
}

INLINE size_t CXMLText::Size(void) const
{
    return (TAGL_CHAR_MAX + TAGL_CHAR_MAX + m_strValue.Length());
}

INLINE bool CXMLText::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        if (m_bCData)
        {
            PCXStr p = CXChar::Str(pszBuf, CDataEndTag);
            if (p != nullptr)
            {
                if (p > pszBuf)
                {    // copy only
                    m_strValue.FillBuffer(pszBuf, (p - pszBuf));
                    m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());
                }
                DEV_TRACE(TF("CXMLText::ParseNode cdata %s"), *m_strValue);
                pszBuf = (PXStr)(p + TAGL_CDATA_END);
                return true;
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                DEV_TRACE(TF("CXMLText::ParseNode cdata end %s failed"), CDataEndTag);
            }
#endif
        }
        else
        {
            PCXStr p = CXChar::Chr(pszBuf, TF('<'));
            if (p != nullptr)
            {    // convert entities
                m_strValue.FillBuffer(pszBuf, (p - pszBuf));
                ToChars(m_strValue);
                m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());

                DEV_TRACE(TF("CXMLText::ParseNode value %s"), *m_strValue);
                pszBuf = (PXStr)p;
                return true;
            }
#ifdef __RUNTIME_DEBUG__
            else
            {
                DEV_TRACE(TF("CXMLText::ParseNode find end char < failed"));
            }
#endif
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLDeclaration
INLINE CXMLDeclaration::CXMLDeclaration(CXMLDocument* pDocument)
: CXMLNode(pDocument, XMLT_DECL)
{
}

INLINE CXMLDeclaration::~CXMLDeclaration(void)
{
}

INLINE CXMLNode* CXMLDeclaration::Clone(CXMLDocument& aSrc) const
{
    return aSrc.CreateDeclaration();
}

INLINE bool CXMLDeclaration::Equal(const CXMLNode& aSrc) const
{
    if (&aSrc == this)
    {
        return true;
    }
    const CXMLDeclaration* pNode = aSrc.To<CXMLDeclaration, XMLT_DECL>();
    return (pNode != nullptr);
}

INLINE void CXMLDeclaration::Save(CString& strBuf, ENCODING_TYPE eENCODING) const
{
    strBuf += DeclarationTag;
    strBuf += TF(' ');

    strBuf += DeclVersion;
    strBuf += TF("=\"");
    strBuf += m_strVersion;
    strBuf += TF("\" ");

    switch (eENCODING)
    {
    case ENT_UTF8:
        {
            strBuf += EncodingDefault;
        }
        break;
    case ENT_UTF16:
        {
            strBuf += DeclEncoding;
            strBuf += TF("=\"");
            strBuf += EncodingUTF16;
            strBuf += TF("\" ");
        }
        break;
    case ENT_UTF32:
        {
            strBuf += DeclEncoding;
            strBuf += TF("=\"");
            strBuf += EncodingUTF32;
            strBuf += TF("\" ");
        }
        break;
    default:
        {
            CString strCodePage;
            CodePage(strCodePage);

            strBuf += DeclEncoding;
            strBuf += TF("=\"");
            if (m_strEncoding.IsEmpty() == false)
            {
                if ((m_strEncoding.Cmpi(EncodingUTF8)    == 0) ||
                    (m_strEncoding.Cmpi(EncodingUTF16)   == 0) ||
                    (m_strEncoding.Cmpi(EncodingUTF32)   == 0) ||
                    (m_strEncoding.Cmpi(EncodingUnicode) == 0))
                {
                    strBuf += strCodePage;
                }
                else
                {
                    strBuf += m_strEncoding;
                }
            }
            else
            {
                strBuf += strCodePage;
            }
            strBuf += TF("\" ");
        }
    }

    if (m_strStandalone.IsEmpty() == false)
    {
        strBuf += DeclStandalone;
        strBuf += TF("=\"");
        strBuf += m_strStandalone;
        strBuf += TF("\" ");
    }

    strBuf += DeclarationEndTag;
    strBuf += TF("\r\n");
}

INLINE size_t CXMLDeclaration::Size(void) const
{
    return (DECLL_MAX + m_strVersion.Length() + m_strEncoding.Length() + m_strStandalone.Length());
}

template <size_t stLenT>
INLINE void CXMLDeclaration::GetVersion(CTStringFix<CXChar, stLenT>& strFixVersion) const
{
    strFixVersion = m_strVersion;
}

INLINE void CXMLDeclaration::GetVersion(CString& strVersion) const
{
    strVersion = m_strVersion;
}

INLINE void CXMLDeclaration::SetVersion(PCXStr pszVersion)
{
    m_strVersion = pszVersion;
}

template <size_t stLenT>
INLINE void CXMLDeclaration::GetEncoding(CTStringFix<CXChar, stLenT>& strFixEncoding) const
{
    strFixEncoding = m_strEncoding;
}

INLINE void CXMLDeclaration::GetEncoding(CString& strEncoding) const
{
    strEncoding = m_strEncoding;
}

INLINE void CXMLDeclaration::SetEncoding(PCXStr pszEncoding)
{
    m_strEncoding = pszEncoding;
}

template <size_t stLenT>
INLINE void CXMLDeclaration::GetStandalone(CTStringFix<CXChar, stLenT>& strFixStandalone) const
{
    strFixStandalone = m_strStandalone;
}

INLINE void CXMLDeclaration::GetStandalone(CString& strStandalone) const
{
    strStandalone = m_strStandalone;
}

INLINE void CXMLDeclaration::SetStandalone(PCXStr pszStandalone)
{
    m_strStandalone = pszStandalone;
}

INLINE bool CXMLDeclaration::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        PCXStr p = CXChar::Str(pszBuf, DeclarationEndTag);
        if (p != nullptr)
        {
            if (p > pszBuf)
            {    // do nothing, just copy string only
                m_strValue.FillBuffer(pszBuf, (p - pszBuf));
                m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());

                PXStr pszAttr = *m_strValue;
                ParseAttributes(pszAttr, DeclVersion,    DECLL_VERSION,    m_strVersion);
                ParseAttributes(pszAttr, DeclEncoding,   DECLL_ENCODING,   m_strEncoding);
                ParseAttributes(pszAttr, DeclStandalone, DECLL_STANDALONE, m_strStandalone);
            }
            DEV_TRACE(TF("CXMLDeclaration::ParseNode value %s"), *m_strValue);
            pszBuf = (PXStr)(p + TAGL_DECL_END);
            return true;
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_TRACE(TF("CXMLDeclaration::ParseNode end %s failed"), DeclarationEndTag);
        }
#endif
    }
    return false;
}

// XML Declaration
// The XML declaration is a processing instruction that identifies
// the document as being XML.
// All XML documents should begin with an XML declaration.
// Example: <?xml version="1.0" encoding="UTF-8" standalone="no" ?>
// Rules :
// #If the XML declaration is included,
//     it must be situated at the first position of
//     the first line in the XML documentwell - formedness constraint.
// #If the XML declaration is included, it must contain
//     the version number attributewell - formedness constraint.
// #If all of the attributesglossary are declared in an XML declaration,
//     they must be placed in the order shown abovewell-formedness constraint.
// #If any elements, attributes, or entities are used in
//     the XML document that are referenced or defined in an external DTD,
//     standalone="no" must be includedvalidity constraint.
// #The XML declaration must be in lower case
//     (except for the encoding declarations)well - formedness constraint.
INLINE void CXMLDeclaration::ParseAttributes(PXStr& pszBuf, PCXStr pszName, size_t stNameLen, CString& strAttribute)
{
    strAttribute.Empty();
    while (pszBuf != nullptr)
    {
        if (SkipChars(pszBuf) == false)
        {
            break;
        }
        if (CXChar::Cmpn(pszBuf, pszName, stNameLen) != 0)
        {
            break;
        }
        pszBuf += stNameLen;
        if (SkipChars(pszBuf) == false)
        {
            break;
        }
        if (pszBuf[0] != TF('='))
        {
            break;
        }
        ++pszBuf;
        if (SkipChars(pszBuf) == false)
        {
            break;
        }
        if (pszBuf[0] != TF('\"'))
        {
            break;
        }
        ++pszBuf;
        PCXStr p = CXChar::Chr(pszBuf, TF('\"'));
        if (p == nullptr)
        {
            break;
        }
        if (p > pszBuf)
        {
            strAttribute.FillBuffer(pszBuf, (p - pszBuf));
        }
        else
        {
            strAttribute.Empty();
        }
        pszBuf = (PXStr)p;
        ++pszBuf;
        DEV_TRACE(TF("CXMLDeclaration::ParseAttributes  %s=\"%s\""), pszName, *strAttribute);
        break;
    }
}

///////////////////////////////////////////////////////////////////
// CXMLUnknown
INLINE CXMLUnknown::CXMLUnknown(CXMLDocument* pDocument)
: CXMLNode(pDocument, XMLT_UNKNOWN)
, m_bDtd(true)
{
}

INLINE CXMLUnknown::~CXMLUnknown(void)
{
}

INLINE bool CXMLUnknown::IsDtd(void) const
{
    return m_bDtd;
}

INLINE void CXMLUnknown::SetDtd(bool bDtd)
{
    m_bDtd = bDtd;
}

INLINE CXMLNode* CXMLUnknown::Clone(CXMLDocument& aSrc) const
{
    return aSrc.CreateUnknown(*m_strValue);
}

INLINE bool CXMLUnknown::Equal(const CXMLNode& aSrc) const
{
    if (&aSrc == this)
    {
        return true;
    }
    const CXMLUnknown* pNode = aSrc.To<CXMLUnknown, XMLT_UNKNOWN>();
    if ((pNode == nullptr) || (pNode->m_strValue != m_strValue))
    {
        return false;
    }
    return true;
}

INLINE void CXMLUnknown::Save(CString& strBuf, ENCODING_TYPE) const
{
    strBuf += TF('<');
    if (IsDtd())
    {
        strBuf += TF('!');
    }
    else
    {
        strBuf += TF('?');
    }
    strBuf += m_strValue;
    if (IsDtd() == false)
    {
        strBuf += TF('?');
    }
    strBuf += TF(">\r\n");
}

INLINE size_t CXMLUnknown::Size(void) const
{
    return (TAGL_CHAR_MAX + m_strValue.Length());
}

INLINE bool CXMLUnknown::ParseNode(PXStr& pszBuf)
{
    if (SkipChars(pszBuf))
    {
        PCXStr p = CXChar::Chr(pszBuf, TF('>'));
        if (p != nullptr)
        {
            if (p > pszBuf)
            {    // do nothing, just copy string only
                m_strValue.FillBuffer(pszBuf, (p - pszBuf));
                m_stHash = CHash::Hash(*m_strValue, m_strValue.Length());
            }
            DEV_TRACE(TF("CXMLUnknown::ParseNode value %s"), *m_strValue);
            pszBuf = (PXStr)(p + TAGL_CHAR);
            return true;
        }
#ifdef __RUNTIME_DEBUG__
        else
        {
            DEV_TRACE(TF("CXMLUnknown::ParseNode find end char > failed"));
        }
#endif
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLAttribute
INLINE CXMLAttribute::CXMLAttribute(CXMLElement* pElement, PCXStr pszName, PCXStr pszValue)
: m_pElement(pElement)
, m_stName(0)
, m_stValue(0)
, m_pPrev(nullptr)
, m_pNext(nullptr)
{
    if ((pszName != nullptr) && (*pszName != 0))
    {
        m_strName = pszName;
        m_stName  = CHash::Hash(*m_strName, m_strName.Length());
    }
    if ((pszValue != nullptr) && (*pszValue != 0))
    {
        m_strValue = pszValue;
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
    }
}

INLINE CXMLAttribute::~CXMLAttribute(void)
{
}

INLINE CXMLAttribute::CXMLAttribute(const CXMLAttribute&)
: m_pElement(nullptr)
, m_stName(0)
, m_stValue(0)
, m_pPrev(nullptr)
, m_pNext(nullptr)
{
}

INLINE CXMLAttribute& CXMLAttribute::operator=(const CXMLAttribute&)
{
    return (*this);
}

INLINE CXMLElement* CXMLAttribute::GetElement(void)
{
    return m_pElement;
}

INLINE const CXMLElement* CXMLAttribute::GetElement(void) const
{
    return m_pElement;
}

template <size_t stLenT>
INLINE bool CXMLAttribute::GetName(CTStringFix<CXChar, stLenT>& strFixName) const
{
    strFixName = m_strName;
    return true;
}

INLINE bool CXMLAttribute::GetName(CString& strName) const
{
    strName = m_strName;
    return true;
}

template <size_t stLenT>
INLINE bool CXMLAttribute::GetValue(CTStringFix<CXChar, stLenT>& strFixValue) const
{
    strFixValue = m_strValue;
    return true;
}

INLINE bool CXMLAttribute::GetValue(CString& strValue) const
{
    strValue = m_strValue;
    return true;
}

INLINE bool CXMLAttribute::SetValue(PCXStr pszValue)
{
    if ((pszValue != nullptr) && (*pszValue != 0))
    {
        m_strValue = pszValue;
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(Short& sValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        sValue = (Short)m_strValue.ToInt();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(UShort& usValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        usValue = (UShort)m_strValue.ToULong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(Int& nValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        nValue = m_strValue.ToInt();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(UInt& uValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        uValue = (UInt)m_strValue.ToULong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(Long& lValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        lValue = m_strValue.ToLong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(ULong& ulValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        ulValue = m_strValue.ToULong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(LLong& llValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        llValue = m_strValue.ToLLong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(ULLong& ullValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        ullValue = m_strValue.ToULLong();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::GetValue(Double& dValue) const
{
    if (m_strValue.IsEmpty() == false)
    {
        dValue = m_strValue.ToDouble();
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(Short sValue)
{
    if (m_strValue.ToString((Int)sValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(UShort usValue)
{
    if (m_strValue.ToString((ULong)usValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(Int nValue)
{
    if (m_strValue.ToString(nValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(UInt uValue)
{
    if (m_strValue.ToString((ULong)uValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(Long lValue)
{
    if (m_strValue.ToString(lValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(ULong ulValue)
{
    if (m_strValue.ToString(ulValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(LLong llValue)
{
    if (m_strValue.ToString(llValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(ULLong ullValue)
{
    if (m_strValue.ToString(ullValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE bool CXMLAttribute::SetValue(Double dValue)
{
    if (m_strValue.ToString(dValue))
    {
        m_stValue  = CHash::Hash(*m_strValue, m_strValue.Length());
        return true;
    }
    return false;
}

INLINE const CXMLAttribute* CXMLAttribute::Prev(void) const
{
    return m_pPrev;
}

INLINE const CXMLAttribute* CXMLAttribute::Next(void) const
{
    return m_pNext;
}

INLINE void CXMLAttribute::Save(CString& strBuf, ENCODING_TYPE) const
{
    CString strEntity;
    ToEntity(m_strValue, strEntity);

    strBuf += TF(' ');
    strBuf += m_strName;
    strBuf += TF("=\"");
    strBuf += strEntity;
    strBuf += TF('\"');
}

INLINE size_t CXMLAttribute::Size(void) const
{
    // space + name + equal + quote + value + quote
    size_t stSize = TAGL_CHAR_MAX;
    stSize += m_strName.Length() + m_strValue.Length();
    return stSize;
}

///////////////////////////////////////////////////////////////////
// CKVTraits
INLINE bool CKVTraits::IsAlnum(UInt u) const
{
    if (u < 127)
    {
        return CXChar::IsAlnum((XChar)u);
    }
    return true;
}

INLINE bool CKVTraits::IsAlpha(UInt u) const
{
    if (u < 127)
    {
        return CXChar::IsAlpha((XChar)u);
    }
    return true;
}

INLINE bool CKVTraits::IsSkipChar(XChar c) const
{
    return ((c == TF(' '))  ||
            (c == TF('\r')) ||
            (c == TF('\n')) ||
            (c == TF('\t')));
}

INLINE bool CKVTraits::IsSkipCharWithComma(XChar c) const
{
    return ((c == TF(' '))  ||
            (c == TF(','))  ||
            (c == TF('\r')) ||
            (c == TF('\n')) ||
            (c == TF('\t')));
}

INLINE bool CKVTraits::SkipChars(PXStr& pszBuf, bool SkipComma) const
{
    if (SkipComma == false)
    {
        while (IsSkipChar(*pszBuf))
        {
            ++pszBuf;
        }
    }
    else
    {
        while (IsSkipCharWithComma(*pszBuf))
        {
            ++pszBuf;
        }
    }
    return (*pszBuf != 0);
}

///////////////////////////////////////////////////////////////////
// CKVNode
SELECTANY CKVNode const CKVNode::ms_None;

INLINE CKVNode::CKVNode(VAR_TYPE eType)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(eType);
}

INLINE CKVNode::CKVNode(Short sValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(sValue);
}

INLINE CKVNode::CKVNode(UShort usValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(usValue);
}

INLINE CKVNode::CKVNode(Int nValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(nValue);
}

INLINE CKVNode::CKVNode(UInt uValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(uValue);
}

INLINE CKVNode::CKVNode(Long lValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(lValue);
}

INLINE CKVNode::CKVNode(ULong ulValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(ulValue);
}

INLINE CKVNode::CKVNode(LLong llValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(llValue);
}

INLINE CKVNode::CKVNode(ULLong ullValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(ullValue);
}

INLINE CKVNode::CKVNode(Double dValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(dValue);
}

INLINE CKVNode::CKVNode(bool bValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(bValue);
}

INLINE CKVNode::CKVNode(PCXStr pszValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(pszValue);
}

INLINE CKVNode::CKVNode(const CString& strValue)
: m_eType(VART_NONE)
, m_eENCODING(ENT_LOCAL)
{
    SetValue(strValue);
}

INLINE CKVNode::CKVNode(const CKVNode& aSrc)
: m_eType(VART_NONE)
, m_eENCODING(aSrc.m_eENCODING)
{
    SetValue(aSrc);
}

INLINE CKVNode& CKVNode::operator=(VAR_TYPE eType)
{
    SetValue(eType);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(Short sValue)
{
    SetValue(sValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(UShort usValue)
{
    SetValue(usValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(Int nValue)
{
    SetValue(nValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(UInt uValue)
{
    SetValue(uValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(Long lValue)
{
    SetValue(lValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(ULong ulValue)
{
    SetValue(ulValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(LLong llValue)
{
    SetValue(llValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(ULLong ullValue)
{
    SetValue(ullValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(Double dValue)
{
    SetValue(dValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(bool bValue)
{
    SetValue(bValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(PCXStr pszValue)
{
    SetValue(pszValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(const CString& strValue)
{
    SetValue(strValue);
    return (*this);
}

INLINE CKVNode& CKVNode::operator=(const CKVNode& aSrc)
{
    if (&aSrc != this)
    {
        SetValue(aSrc);
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CKVNode::CKVNode(CKVNode&& aSrc)
: m_eType(VART_NONE)
, m_eENCODING(aSrc.m_eENCODING)
{
    SetValue(std::move(aSrc));
}

INLINE CKVNode& CKVNode::operator=(CKVNode&& aSrc)
{
    if (&aSrc != this)
    {
        SetValue(std::move(aSrc));
    }
    return (*this);
}
#endif

INLINE CKVNode::~CKVNode(void)
{
    Remove();
}

INLINE ENCODING_TYPE CKVNode::GetEncoding(void) const
{
    return m_eENCODING;
}

INLINE VAR_TYPE CKVNode::GetType(void) const
{
    return m_eType;
}

INLINE Int CKVNode::GetSize(void) const
{
    switch (m_eType)
    {
    case VART_ARRAY:
        {
            assert(m_Value.pArray != nullptr);
            return m_Value.pArray->GetSize();
        }
        break;
    case VART_OBJECT:
        {
            assert(m_Value.pObject != nullptr);
            return m_Value.pObject->GetSize();
        }
        break;
    default: { }
    }
    return -1;
}

INLINE Short CKVNode::GetShort(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (Short)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (Short)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (Short)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return (Short)(m_Value.bValue ? 1 : 0);
        }
        break;
    case VART_STRING:
        {
            return (Short)(m_Value.pString->ToInt());
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE UShort CKVNode::GetUShort(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (UShort)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (UShort)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (UShort)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return (UShort)(m_Value.bValue ? 1 : 0);
        }
        break;
    case VART_STRING:
        {
            return (UShort)m_Value.pString->ToULong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Int CKVNode::GetInt(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (Int)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (Int)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (Int)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToInt();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE UInt CKVNode::GetUInt(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (UInt)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (UInt)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (UInt)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return (UInt)m_Value.pString->ToULong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Long CKVNode::GetLong(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (Long)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (Long)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (Long)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToLong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE ULong CKVNode::GetULong(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (ULong)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (ULong)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (ULong)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToULong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE LLong CKVNode::GetLLong(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (LLong)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (LLong)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (LLong)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToLLong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE ULLong CKVNode::GetULLong(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (ULLong)(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return (ULLong)(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (ULLong)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1 : 0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToULLong();
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Double CKVNode::GetDouble(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return static_cast<Double>(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            return static_cast<Double>(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            return (Double)(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue ? 1.0 : 0.0;
        }
        break;
    case VART_STRING:
        {
            return m_Value.pString->ToDouble();
        }
        break;
    default:
        {
            return 0.0;
        }
    }
}

INLINE bool CKVNode::GetBoolean(void) const
{
    switch (m_eType)
    {
    case VART_LLONG:
        {
            return (m_Value.llValue != 0) ? true : false;
        }
        break;
    case VART_ULLONG:
        {
            return (m_Value.ullValue != 0) ? true : false;
        }
        break;
    case VART_DOUBLE:
        {
            return (m_Value.dValue != 0.0) ? true : false;
        }
        break;
    case VART_BOOL:
        {
            return m_Value.bValue;
        }
        break;
    case VART_STRING:
        {
            return (m_Value.pString->Cmpi(TF("true")) == 0) ? true : false;
        }
        break;
    default:
        {
            return false;
        }
    }
}

INLINE bool CKVNode::GetString(CString& strValue) const
{
    strValue.Empty();
    switch (m_eType)
    {
    case VART_LLONG:
        {
            strValue.ToString(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            strValue.ToString(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            strValue.ToString(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            strValue = m_Value.bValue ? TF("true") : TF("false");
        }
        break;
    case VART_STRING:
        {
            strValue = (*m_Value.pString);
        }
        break;
    default:
        {
            return false;
        }
    }
    return true;
}

INLINE CString CKVNode::GetString(void)
{
    CString strValue;
    switch (m_eType)
    {
    case VART_LLONG:
        {
            strValue.ToString(m_Value.llValue);
        }
        break;
    case VART_ULLONG:
        {
            strValue.ToString(m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            strValue.ToString(m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            strValue = m_Value.bValue ? TF("true") : TF("false");
        }
        break;
    case VART_STRING:
        {
            strValue = (*m_Value.pString);
        }
        break;
    default:
        {
        }
    }
    return strValue;
}

INLINE bool CKVNode::SetValue(VAR_TYPE eType)
{
    Remove();
    switch (eType)
    {
    case VART_CHAR:
    case VART_SHORT:
    case VART_INT:
    case VART_LONG:
    case VART_LLONG:
        {
            m_eType = VART_LLONG;
        }
        break;
    case VART_UCHAR:
    case VART_USHORT:
    case VART_UINT:
    case VART_ULONG:
    case VART_ULLONG:
        {
            m_eType = VART_ULLONG;
        }
        break;
    case VART_FLOAT:
    case VART_DOUBLE:
        {
            m_eType = VART_DOUBLE;
        }
        break;
    case VART_BOOL:
        {
            m_eType = VART_BOOL;
        }
        break;
    case VART_STRING:
        {
            m_Value.pString = MNEW CString;
            if (m_Value.pString != nullptr)
            {
                m_eType = VART_STRING;
            }
        }
        break;
    case VART_ARRAY:
        {
            m_Value.pArray = MNEW MAP_ARRAY;
            if (m_Value.pArray != nullptr)
            {
                m_eType = VART_ARRAY;
            }
        }
        break;
    case VART_OBJECT:
        {
            m_Value.pObject = MNEW MAP_OBJECT;
            if (m_Value.pObject != nullptr)
            {
                m_eType = VART_OBJECT;
            }
        }
        break;
    default: { }
    }
    return (m_eType != VART_NONE);
}

INLINE bool CKVNode::SetValue(Short sValue)
{
    Remove();
    m_eType = VART_LLONG;
    m_Value.llValue = sValue;
    return true;
}

INLINE bool CKVNode::SetValue(UShort usValue)
{
    Remove();
    m_eType = VART_ULLONG;
    m_Value.ullValue = usValue;
    return true;
}

INLINE bool CKVNode::SetValue(Int nValue)
{
    Remove();
    m_eType = VART_LLONG;
    m_Value.llValue = nValue;
    return true;
}

INLINE bool CKVNode::SetValue(UInt uValue)
{
    Remove();
    m_eType = VART_ULLONG;
    m_Value.ullValue = uValue;
    return true;
}

INLINE bool CKVNode::SetValue(Long lValue)
{
    Remove();
    m_eType = VART_LLONG;
    m_Value.llValue = lValue;
    return true;
}

INLINE bool CKVNode::SetValue(ULong ulValue)
{
    Remove();
    m_eType = VART_ULLONG;
    m_Value.ullValue = ulValue;
    return true;
}

INLINE bool CKVNode::SetValue(LLong llValue)
{
    Remove();
    m_eType = VART_LLONG;
    m_Value.llValue = llValue;
    return true;
}

INLINE bool CKVNode::SetValue(ULLong ullValue)
{
    Remove();
    m_eType = VART_ULLONG;
    m_Value.ullValue = ullValue;
    return true;
}

INLINE bool CKVNode::SetValue(Double dValue)
{
    Remove();
    m_eType = VART_DOUBLE;
    m_Value.dValue = dValue;
    return true;
}

INLINE bool CKVNode::SetValue(bool bValue)
{
    Remove();
    m_eType = VART_BOOL;
    m_Value.bValue = bValue;
    return true;
}

INLINE bool CKVNode::SetValue(PCXStr pszValue)
{
    Remove();
    m_Value.pString = MNEW CString;
    if (m_Value.pString != nullptr)
    {
        m_Value.pString->FillBuffer(pszValue);
        m_eType = VART_STRING;
    }
    return (m_eType != VART_NONE);
}

INLINE bool CKVNode::SetValue(const CString& strValue)
{
    Remove();
    m_Value.pString = MNEW CString;
    if (m_Value.pString != nullptr)
    {
        m_Value.pString->FillBuffer(strValue.GetBuffer());
        m_eType = VART_STRING;
    }
    return (m_eType != VART_NONE);
}

INLINE bool CKVNode::SetValue(const CKVNode& aSrc)
{
    if (&aSrc == this)
    {
        return false;
    }
    Remove();
    switch (aSrc.m_eType)
    {
    case VART_LLONG:
    case VART_ULLONG:
    case VART_DOUBLE:
    case VART_BOOL:
        {
            m_eType = aSrc.m_eType;
            MM_SAFE::Cpy(&m_Value, sizeof(VALUE), &aSrc.m_Value, sizeof(VALUE));
        }
        break;
    case VART_STRING:
        {
            m_Value.pString = MNEW CString;
            if (m_Value.pString != nullptr)
            {
                m_Value.pString->FillBuffer(aSrc.m_Value.pString->GetBuffer(), aSrc.m_Value.pString->Length());
                m_eType = VART_STRING;
            }
        }
        break;
    case VART_ARRAY:
        {
            m_Value.pArray = MNEW MAP_ARRAY;
            if (m_Value.pArray != nullptr)
            {
                m_Value.pArray->Copy(*(aSrc.m_Value.pArray));
                m_eType = VART_ARRAY;
            }
        }
        break;
    case VART_OBJECT:
        {
            m_Value.pObject = MNEW MAP_OBJECT;
            if (m_Value.pObject != nullptr)
            {
                m_Value.pObject->Copy(*(aSrc.m_Value.pObject));
                m_eType = VART_OBJECT;
            }
        }
        break;
    default: { }
    }
    return (m_eType != VART_NONE);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE bool CKVNode::SetValue(CKVNode&& aSrc)
{
    if (&aSrc == this)
    {
        return false;
    }
    Remove();
    switch (aSrc.m_eType)
    {
    case VART_LLONG:
    case VART_ULLONG:
    case VART_DOUBLE:
    case VART_BOOL:
        {
            m_eType = aSrc.m_eType;
            MM_SAFE::Cpy(&m_Value, sizeof(VALUE), &aSrc.m_Value, sizeof(VALUE));
        }
        break;
    case VART_STRING:
        {
            m_Value.pString = aSrc.m_Value.pString;
            m_eType = VART_STRING;
            aSrc.m_Value.pString = nullptr;
            aSrc.Remove();
        }
        break;
    case VART_ARRAY:
        {
            m_Value.pArray = aSrc.m_Value.pArray;
            m_eType = VART_ARRAY;
            aSrc.m_Value.pArray = nullptr;
            aSrc.Remove();
        }
        break;
    case VART_OBJECT:
        {
            m_Value.pObject = aSrc.m_Value.pObject;
            m_eType = VART_OBJECT;
            aSrc.m_Value.pObject = nullptr;
            aSrc.Remove();
        }
        break;
    default: { }
    }
    return (m_eType != VART_NONE);
}

INLINE bool CKVNode::SetValue(CString&& strValue)
{
    Remove();
    m_Value.pString = MNEW CString(std::move(strValue));
    if (m_Value.pString != nullptr)
    {
        m_eType = VART_STRING;
    }
    return (m_eType != VART_NONE);
}
#endif

INLINE CKVNode& CKVNode::operator[](Int nIndex)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_ARRAY);
    }
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        PAIR_ARRAY* pPair = m_Value.pArray->Find(nIndex);
        if (pPair == nullptr)
        {
            PINDEX index = m_Value.pArray->Add(nIndex);
            pPair = m_Value.pArray->GetAt(index);
        }
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE const CKVNode& CKVNode::operator[](Int nIndex) const
{
    if (m_eType == VART_NONE)
    {
        return ms_None;
    }
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        PAIR_ARRAY* pPair = m_Value.pArray->Find(nIndex);
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
        return ms_None;
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE CKVNode& CKVNode::operator[](PCXStr pszName)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_OBJECT);
    }
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->Find(pszName);
        if (pPair == nullptr)
        {
            PINDEX index = m_Value.pObject->Add(pszName);
            pPair = m_Value.pObject->GetAt(index);
        }
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE const CKVNode& CKVNode::operator[](PCXStr pszName) const
{
    if (m_eType == VART_NONE)
    {
        return ms_None;
    }
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->Find(pszName);
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
        return ms_None;
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE CKVNode& CKVNode::Add(const CKVNode& aSrc)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_ARRAY);
    }
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        PINDEX index = m_Value.pArray->Add(m_Value.pArray->GetSize(), aSrc);
        PAIR_ARRAY* pPair = m_Value.pArray->GetAt(index);
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE CKVNode& CKVNode::Add(PCXStr pszName, const CKVNode& aSrc)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_OBJECT);
    }
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->Find(pszName);
        if (pPair != nullptr)
        {
            pPair->m_V.SetValue(aSrc);
            return pPair->m_V;
        }
        PINDEX index = m_Value.pObject->Add(pszName, aSrc);
        pPair = m_Value.pObject->GetAt(index);
        if (pPair != nullptr)
        {
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CKVNode& CKVNode::Add(CKVNode&& aSrc)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_ARRAY);
    }
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        PINDEX index = m_Value.pArray->Add(m_Value.pArray->GetSize());
        PAIR_ARRAY* pPair = m_Value.pArray->GetAt(index);
        if (pPair != nullptr)
        {
            pPair->m_V.SetValue(std::move(aSrc));
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}

INLINE CKVNode& CKVNode::Add(PCXStr pszName, CKVNode&& aSrc)
{
    if (m_eType == VART_NONE)
    {
        SetValue(VART_OBJECT);
    }
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->Find(pszName);
        if (pPair == nullptr)
        {
            PINDEX index = m_Value.pObject->Add(pszName);
            pPair = m_Value.pObject->GetAt(index);
        }
        if (pPair != nullptr)
        {
            pPair->m_V.SetValue(std::move(aSrc));
            return pPair->m_V;
        }
    }
    throw (E_ACCESS_VIOLATION);
}
#endif

INLINE bool CKVNode::Remove(Int nIndex)
{
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        if (m_Value.pArray->Find(nIndex) != nullptr)
        {
            Int nSize = m_Value.pArray->GetSize();
            for (Int i = nIndex + 1; i < nSize; ++i)
            {
                PAIR_ARRAY* pPair = m_Value.pArray->Find(i);
#ifndef __MODERN_CXX_NOT_SUPPORTED
                m_Value.pArray->SetAt(nIndex, std::move(pPair->m_V));
#else
                m_Value.pArray->SetAt(nIndex, pPair->m_V);
#endif
                ++nIndex;
            }
            return m_Value.pArray->Remove(nIndex);
        }
    }
    return false;
}

INLINE bool CKVNode::Remove(PCXStr pszName)
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        return m_Value.pObject->Remove(pszName);
    }
    return false;
}

INLINE void CKVNode::Remove(void)
{
    switch (m_eType)
    {
    case VART_STRING:
        {
            if (m_Value.pString != nullptr)
            {
                MDELETE m_Value.pString;
                m_Value.pString = nullptr;
            }
        }
        break;
    case VART_ARRAY:
        {
            if (m_Value.pArray != nullptr)
            {
                MDELETE m_Value.pArray;
                m_Value.pArray = nullptr;
            }
        }
        break;
    case VART_OBJECT:
        {
            if (m_Value.pObject != nullptr)
            {
                MDELETE m_Value.pObject;
                m_Value.pObject = nullptr;
            }
        }
        break;
    default: { }
    }
    MM_SAFE::Set(&m_Value, 0, sizeof(VALUE));
    m_eType = VART_NONE;
}

INLINE bool CKVNode::Find(Int nIndex) const
{
    if (m_eType == VART_ARRAY)
    {
        assert(m_Value.pArray != nullptr);
        return (m_Value.pArray->Find(nIndex) != nullptr);
    }
    return false;
}

INLINE bool CKVNode::Find(PCXStr pszName) const
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        return (m_Value.pObject->Find(pszName) != nullptr);
    }
    return false;
}

INLINE PINDEX CKVNode::First(void) const
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        return m_Value.pObject->GetFirstIndex();
    }
    return nullptr;
}

INLINE PINDEX CKVNode::Last(void) const
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        return m_Value.pObject->GetLastIndex();
    }
    return nullptr;
}

INLINE bool CKVNode::Next(CString& strName, PINDEX& index) const
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->GetNext(index);
        if (pPair != nullptr)
        {
            strName = pPair->m_K;
            return true;
        }
    }
    return false;
}

INLINE bool CKVNode::Prev(CString& strName, PINDEX& index) const
{
    if (m_eType == VART_OBJECT)
    {
        assert(m_Value.pObject != nullptr);
        PAIR_OBJECT* pPair = m_Value.pObject->GetPrev(index);
        if (pPair != nullptr)
        {
            strName = pPair->m_K;
            return true;
        }
    }
    return false;
}

INLINE bool CKVNode::Load(CStream& stream, size_t stSize, ENCODING_TYPE eENCODING)
{
    Remove();
    assert(stream.IsRead());
    size_t stLen = stream.Rest();
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            stream.Read(pBuf, stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE bool CKVNode::Load(CString& strSrc, size_t stSize, ENCODING_TYPE eENCODING)
{
    Remove();
    size_t stLen = strSrc.Length() * sizeof(XChar);
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            MM_SAFE::Cpy(pBuf, stLen, strSrc.GetBuffer(), stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE bool CKVNode::Load(PCXStr pszSrc, size_t stSize, ENCODING_TYPE eENCODING)
{
    Remove();
    size_t stLen = CXChar::Length(pszSrc) * sizeof(XChar);
    if ((stSize > 0) && (stLen > stSize))
    {
        stLen = stSize;
    }
    if (stLen > BOML_MAXLEN)
    {
        bool bRet  = false;
        PByte pBuf = (PByte)ALLOC( stLen + BOML_MAXLEN + BOML_MAXLEN );
        if (pBuf != nullptr)
        {
            MM_SAFE::Set((pBuf + stLen), 0, sizeof(PUInt));
            MM_SAFE::Cpy(pBuf, stLen, pszSrc, stLen);

            m_eENCODING = eENCODING;

            bRet = Load(pBuf, stLen);

            FREE( pBuf );
        } // pBuf != nullptr
        return bRet;
    } // stLen > BOML_MAXLEN
    return false;
}

INLINE bool CKVNode::Save(CString& strBuf, bool bStyle)
{
    if (GetType() != VART_NONE)
    {
        size_t stLen = Size(bStyle ? 0 : -1);
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(bStyle ? 0 : -1, strBuf);
        return true;
    }
    return false;
}

INLINE bool CKVNode::Parse(PXStr& pszBuf)
{
    if (ParseComment(pszBuf) == false)
    {
        return true; // end
    }
    TOKEN_TYPE eToken = ParseToken(pszBuf);
    switch (eToken)
    {
    case TOKEN_TYPE_ARRAY:
        {
            return ParseArray(pszBuf);
        }
        break;
    case TOKEN_TYPE_OBJECT:
        {
            return ParseObject(pszBuf);
        }
        break;
    default: { }
    }
    return (eToken != TOKEN_TYPE_ERROR);
}

INLINE bool CKVNode::ParseComment(PXStr& pszBuf, bool SkipComma)
{
    while (SkipChars(pszBuf, SkipComma))
    {
        if (pszBuf[0] == TF('/'))
        {
            PXStr pszEnd = nullptr;
            if (pszBuf[1] == TF('/'))
            {
                pszEnd = (PXStr)CXChar::Chr(pszBuf + 2, TF('\n'));
                if (pszEnd != nullptr)
                {
                    pszBuf = pszEnd + 1;
                }
            }
            else if (pszBuf[1] == TF('*'))
            {
                pszEnd = (PXStr)CXChar::Str(pszBuf + 2, TF("*/"));
                if (pszEnd != nullptr)
                {
                    pszBuf = pszEnd + 2;
                }
            }
        }
        else
        {
            return true;
        }
    }
    return false;
}

INLINE CKVNode::TOKEN_TYPE CKVNode::ParseToken(PXStr& pszBuf)
{
    TOKEN_TYPE eToken = TOKEN_TYPE_ERROR;
    if (SkipChars(pszBuf))
    {
        switch (pszBuf[0])
        {
        case TF('N'):
        case TF('n'):
            {
                if (CXChar::Cmpin(pszBuf, TF("null"), 4) == 0)
                {
                    pszBuf += 4;
                    eToken = TOKEN_TYPE_NONE;
                }
            }
            break;
        case TF('0'):
        case TF('1'):
        case TF('2'):
        case TF('3'):
        case TF('4'):
        case TF('5'):
        case TF('6'):
        case TF('7'):
        case TF('8'):
        case TF('9'):
        case TF('-'):
        case TF('+'):
            {
                PCXStr pszSkip = CXChar::OneOf(pszBuf, TF(" ,\r\n\t]}"));
                if (pszSkip != nullptr)
                {
                    PXStr  pszEnd = nullptr;
                    PCXStr pszMatch = CXChar::OneOf(pszBuf, TF(".xX"));
                    if ((pszMatch == nullptr) ||
                        (pszMatch >= pszSkip) ||
                        (pszMatch[0] != TF('.')))
                    {
                        Int nRadix = RADIXT_DEC;
                        if ((pszMatch != nullptr) && (pszMatch < pszSkip))
                        {
                            if ((pszMatch[0] == TF('x')) || (pszMatch[0] == TF('X')))
                            {
                                nRadix = RADIXT_HEX;
                                pszBuf += 2; // 0x/0X
                            }
                        }
                        LLong ll = CXChar::ToLLong(pszBuf, &pszEnd, nRadix);
                        SetValue(ll);
                        pszBuf = pszEnd;

                        eToken = TOKEN_TYPE_INTEGER;
                    }
                    else
                    {
                        Double d = CXChar::ToDouble(pszBuf, &pszEnd);
                        SetValue(d);
                        pszBuf = pszEnd;

                        eToken = TOKEN_TYPE_FLOAT;
                    }
                }
            }
            break;
        case TF('\"'):
        case TF('\''):
            {
                PXStr pszEnd = (PXStr)CXChar::Chr(pszBuf + 1, pszBuf[0]);
                if (pszEnd != nullptr)
                {
                    *pszEnd = 0;
                    SetValue(pszBuf + 1);

                    pszBuf = pszEnd + 1;

                    eToken = TOKEN_TYPE_STRING;
                }
            }
            break;
        case TF('T'):
        case TF('t'):
            {
                if (CXChar::Cmpin(pszBuf, TF("true"), 4) == 0)
                {
                    SetValue(true);
                    pszBuf += 4;

                    eToken = TOKEN_TYPE_TRUE;
                }
            }
            break;
        case TF('F'):
        case TF('f'):
            {
                if (CXChar::Cmpin(pszBuf, TF("false"), 5) == 0)
                {
                    SetValue(false);
                    pszBuf += 5;

                    eToken = TOKEN_TYPE_FALSE;
                }
            }
            break;
        case TF('['):
            {
                eToken = TOKEN_TYPE_ARRAY;
                ++pszBuf;
            }
            break;
        case TF('{'):
            {
                eToken = TOKEN_TYPE_OBJECT;
                ++pszBuf;
            }
            break;
        case 0:
            {
                eToken = TOKEN_TYPE_END;
            }
            break;
        default: { }
        }
    }
    return eToken;
}

INLINE bool CKVNode::ParseArray(PXStr& pszBuf)
{
    SetValue(VART_ARRAY);
    while (ParseComment(pszBuf, true))
    {
        if (pszBuf[0] == TF(']'))
        {
            ++pszBuf;
            return true;
        }
        CKVNode& Node = (*this)[GetSize()];
        if (Node.Parse(pszBuf) == false)
        {
            break;
        }
    }
    return false;
}

INLINE bool CKVNode::ParseObject(PXStr& pszBuf)
{
    SetValue(VART_OBJECT);
    while (ParseComment(pszBuf, true))
    {
        if (pszBuf[0] == TF('}'))
        {
            ++pszBuf;
            return true;
        }
        // name
        CString strName;
        if ((pszBuf[0] == TF('\"')) || (pszBuf[0] == TF('\'')))
        {
            PXStr pszEnd = (PXStr)CXChar::Chr(pszBuf + 1, pszBuf[0]);
            if (pszEnd != nullptr)
            {
                strName.FillBuffer(pszBuf + 1, (pszEnd - pszBuf - 1));
                pszBuf = pszEnd + 1;
            }
        }
        if ((strName.Length() > 0) && SkipChars(pszBuf) && (pszBuf[0] == TF(':')))
        {
            ++pszBuf;
            CKVNode& Node = (*this)[*strName];
            if (Node.Parse(pszBuf))
            {
                continue;
            }
        }
        break;
    }
    return false;
}

INLINE void CKVNode::Save(Int nIndent, CString& strBuf) const
{
    if (nIndent > 0)
    {
        strBuf.AppendBuffer(TF('\t'), (size_t)nIndent);
    }
    switch (m_eType)
    {
    case VART_INT:
    case VART_LONG:
    case VART_LLONG:
        {
            strBuf.AppendFormat(TF("%lld"), m_Value.llValue);
        }
        break;
    case VART_UINT:
    case VART_ULONG:
    case VART_ULLONG:
        {
            strBuf.AppendFormat(TF("%llu"), m_Value.ullValue);
        }
        break;
    case VART_DOUBLE:
        {
            strBuf.AppendFormat(TF("%#g"), m_Value.dValue);
        }
        break;
    case VART_BOOL:
        {
            strBuf += m_Value.bValue ? TF("true") : TF("false");
        }
        break;
    case VART_STRING:
        {
            strBuf += TF('\"');
            strBuf += (*m_Value.pString);
            strBuf += TF('\"');
        }
        break;
    case VART_ARRAY:
        {
            assert(m_Value.pArray != nullptr);
            if (m_Value.pArray->GetSize() == 0)
            {
                strBuf += TF("[]");
            }
            else if (nIndent >= 0)
            {
                strBuf += TF("[\r\n");
                for (PINDEX index = m_Value.pArray->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_ARRAY* pPair = m_Value.pArray->GetNext(index);
                    pPair->m_V.Save(nIndent + 1, strBuf);
                    if (index != nullptr)
                    {
                        strBuf += TF(",\r\n");
                    }
                }
                if (strBuf.GetAt(strBuf.Length() - 1) != TF('\n'))
                {
                    strBuf += TF("\r\n");
                }
                if (nIndent > 0)
                {
                    strBuf.AppendBuffer(TF('\t'), (size_t)nIndent);
                }
                strBuf += TF(']');
            }
            else
            {
                strBuf += TF('[');
                for (PINDEX index = m_Value.pArray->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_ARRAY* pPair = m_Value.pArray->GetNext(index);
                    pPair->m_V.Save(-1, strBuf);
                    if (index != nullptr)
                    {
                        strBuf += TF(',');
                    }
                }
                strBuf += TF(']');
            }
        }
        break;
    case VART_OBJECT:
        {
            assert(m_Value.pObject != nullptr);
            if (m_Value.pObject->GetSize() == 0)
            {
                strBuf += TF("{}");
            }
            else if (nIndent >= 0)
            {
                strBuf += TF("{\r\n");
                for (PINDEX index = m_Value.pObject->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_OBJECT* pPair = m_Value.pObject->GetNext(index);
                    strBuf.AppendBuffer(TF('\t'), (size_t)nIndent + 1);
                    if ((pPair->m_V.GetType() == VART_ARRAY) || (pPair->m_V.GetType() == VART_OBJECT))
                    {
                        strBuf.AppendFormat(TF("\"%s\" :\r\n"), *(pPair->m_K));
                        pPair->m_V.Save(nIndent + 1, strBuf);
                    }
                    else
                    {
                        strBuf.AppendFormat(TF("\"%s\" : "), *(pPair->m_K));
                        pPair->m_V.Save(0, strBuf);
                    }
                    if (index != nullptr)
                    {
                        strBuf += TF(",\r\n");
                    }
                }
                if (strBuf.GetAt(strBuf.Length() - 1) != TF('\n'))
                {
                    strBuf += TF("\r\n");
                }
                if (nIndent > 0)
                {
                    strBuf.AppendBuffer(TF('\t'), (size_t)nIndent);
                }
                strBuf += TF('}');
            }
            else
            {
                strBuf += TF('{');
                for (PINDEX index = m_Value.pObject->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_OBJECT* pPair = m_Value.pObject->GetNext(index);
                    strBuf.AppendFormat(TF("\"%s\":"), *(pPair->m_K));
                    pPair->m_V.Save(-1, strBuf);
                    if (index != nullptr)
                    {
                        strBuf += TF(',');
                    }
                }
                strBuf += TF('}');
            }
        }
        break;
    default:
        {
            strBuf += TF("null");
        }
    }
}

INLINE size_t CKVNode::Size(Int nIndent) const
{
    switch (m_eType)
    {
    case VART_STRING:
        {
            assert(m_Value.pString != nullptr);
            // \"string\"
            if (nIndent >= 0)
            {
                return (m_Value.pString->Length() + TOKEN_LEN_STRING + (size_t)nIndent);
            }
            else
            {
                return (m_Value.pString->Length() + TOKEN_LEN_STRING);
            }
        }
        break;
    case VART_ARRAY:
        {
            assert(m_Value.pArray != nullptr);
            size_t stSize = 0;
            if (nIndent >= 0)
            {
                // [\r\n...,\r\n...\r\n]
                // 3 + size() * (node-size + 3) + 3
                stSize = (size_t)nIndent + (size_t)nIndent + TOKEN_LEN_ARRAY + TOKEN_LEN_ARRAY;
                for (PINDEX index = m_Value.pArray->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_ARRAY* pPair = m_Value.pArray->GetNext(index);
                    stSize += (pPair->m_V.Size(nIndent + TOKEN_LEN_CHAR) + TOKEN_LEN_ARRAY);
                }
            }
            else
            {
                // [...,...]
                // 1 + size() * (node-size + 1) + 1
                stSize = TOKEN_LEN_CHAR + TOKEN_LEN_CHAR;
                for (PINDEX index = m_Value.pArray->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_ARRAY* pPair = m_Value.pArray->GetNext(index);
                    stSize += (pPair->m_V.Size(-1) + TOKEN_LEN_CHAR);
                }
            }
            return stSize;
        }
        break;
    case VART_OBJECT:
        {
            assert(m_Value.pObject != nullptr);
            size_t stSize = 0;
            if (nIndent >= 0)
            {
                // {\r\n..."name" : ,\r\n...\r\n}
                // 3 + size() * (indent + name + sep-size + node-size + 3) + 3
                stSize = (size_t)nIndent + (size_t)nIndent + TOKEN_LEN_OBJECT + TOKEN_LEN_OBJECT;
                for (PINDEX index = m_Value.pObject->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_OBJECT* pPair = m_Value.pObject->GetNext(index);
                    if ((pPair->m_V.GetType() == VART_ARRAY) || (pPair->m_V.GetType() == VART_OBJECT))
                    {
                        stSize += ((size_t)nIndent + TOKEN_LEN_CHAR + pPair->m_K.Length() + TOKEN_LEN_NAME + pPair->m_V.Size(nIndent + TOKEN_LEN_CHAR) + TOKEN_LEN_OBJECT);
                    }
                    else
                    {
                        stSize += ((size_t)nIndent + TOKEN_LEN_CHAR + pPair->m_K.Length() + TOKEN_LEN_NAME + pPair->m_V.Size(0) + TOKEN_LEN_OBJECT);
                    }
                }
            }
            else
            {
                // {..."name":,...}
                // 1 + size() * (indent + name + sep-size + node-size + 1) + 1
                stSize = TOKEN_LEN_CHAR + TOKEN_LEN_CHAR;
                for (PINDEX index = m_Value.pObject->GetFirstIndex(); index != nullptr;)
                {
                    PAIR_OBJECT* pPair = m_Value.pObject->GetNext(index);
                    stSize += (pPair->m_K.Length() + TOKEN_LEN_NAMEMIN + pPair->m_V.Size(-1) + TOKEN_LEN_CHAR);
                }
            }
            return stSize;
        }
        break;
    default:
        {
            if (nIndent >= 0)
            {
                return ((size_t)nIndent + TOKEN_LEN_DEFAULT);
            }
            else
            {
                return TOKEN_LEN_DEFAULT;
            }
        }
    }
}

#endif // __MARKUP_INL__
