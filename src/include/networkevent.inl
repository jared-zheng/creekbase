// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __NETWORK_EVENT_INL__
#define __NETWORK_EVENT_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CNETPacket
INLINE CNETPacket* CNETPacket::Create(uintptr_t, CStream&, ULLong)
{
    return nullptr;
}

INLINE CNETPacket::CNETPacket(PByte pData, size_t stSize)
: m_pData(pData)
, m_stSize(stSize)
{
}

INLINE CNETPacket::~CNETPacket(void)
{
}

INLINE CNETPacket::CNETPacket(const CNETPacket& aSrc)
: CEventBase(aSrc)
, m_pData(aSrc.m_pData)
, m_stSize(aSrc.m_stSize)
{
}

INLINE CNETPacket& CNETPacket::operator=(const CNETPacket& aSrc)
{
    if (&aSrc != this)
    {
        CEventBase::operator=(aSrc);
        m_pData  = aSrc.m_pData;
        m_stSize = aSrc.m_stSize;
    }
    return (*this);
}

INLINE size_t CNETPacket::Length(void) const
{
    return m_stSize;
}

INLINE void CNETPacket::Serialize(CStream& Stream)
{
    if (Stream.IsWrite() && (m_pData != nullptr))
    {
        Stream.Write(m_pData, m_stSize);
    }
}

///////////////////////////////////////////////////////////////////
// CTNETDispatch
template <typename TPACKET, bool bNetByteOrder>
INLINE CTNETDispatch<TPACKET, bNetByteOrder>::CTNETDispatch(void)
{
}

template <typename TPACKET, bool bNetByteOrder>
INLINE CTNETDispatch<TPACKET, bNetByteOrder>::~CTNETDispatch(void)
{
}

template <typename TPACKET, bool bNetByteOrder>
INLINE CTNETDispatch<TPACKET, bNetByteOrder>::CTNETDispatch(const CTNETDispatch<TPACKET, bNetByteOrder>&)
{
}

template <typename TPACKET, bool bNetByteOrder>
INLINE CTNETDispatch<TPACKET, bNetByteOrder>& CTNETDispatch<TPACKET, bNetByteOrder>::operator=(const CTNETDispatch<TPACKET, bNetByteOrder>&)
{
    return (*this);
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnDispatch(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    assert((utEvent == EVENT_TCP_RECV) || (utEvent == EVENT_UDP_RECV));
    Stream.SetByteSwap(bNetByteOrder);
    PacketPtr PktPtrRef = TPACKET::Create(utEvent, Stream, ullParam);
    if (PktPtrRef != nullptr)
    {
        if (utEvent == EVENT_TCP_RECV)
        {
            PTCP_PARAM pTcp = reinterpret_cast<PTCP_PARAM>(ullParam);
            return OnTcpDispatch(PktPtrRef, pTcp);
        }
        else
        {
            PUDP_PARAM pUdp = reinterpret_cast<PUDP_PARAM>(ullParam);
            return OnUdpDispatch(PktPtrRef, pUdp);
        }
    }
    else
    {
        DumpHexPacketData(utEvent, Stream, ullParam);
    }
    return false;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnEvent(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    switch (utEvent)
    {
    case EVENT_TCP_RECV:
        {
            return OnTcpRecv((size_t)(utData), reinterpret_cast<PTCP_PARAM>(ullParam));
        }
        break;
    case EVENT_TCP_SEND:
        {
            return OnTcpSend((UInt)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_TCP_ACCEPT:
        {
            return OnTcpAccept((Socket)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_TCP_CONNECT:
        {
            return OnTcpConnect((UInt)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_TCP_CLOSE:
        {
            return OnTcpClose((Socket)(utData), ullParam);
        }
        break;
    case EVENT_UDP_RECV:
        {
            return OnUdpRecv((size_t)(utData), reinterpret_cast<PUDP_PARAM>(ullParam));
        }
        break;
    case EVENT_UDP_SEND:
        {
            return OnUdpSend((UInt)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_UDP_CLOSE:
        {
            return OnUdpClose((Socket)(utData), ullParam);
        }
        break;
    case EVENT_TLS_HANDSHAKE:
        {
            return OnTlsHandShake((UInt)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_TLS_RECV:
        {
            return OnTlsRecv((size_t)(utData), reinterpret_cast<PTCP_PARAM>(ullParam));
        }
        break;
    case EVENT_TLS_SEND:
        {
            return OnTlsSend((size_t)(utData), reinterpret_cast<PTCP_PARAM>(ullParam));
        }
        break;
    case EVENT_TLS_ERROR:
        {
            return OnTlsError((UInt)(utData), (Socket)(ullParam));
        }
        break;
    case EVENT_BEFORE_UPGRADE:
        {
            return OnBeforeUpgrade(utData, ullParam);
        }
        break;
    case EVENT_AFTER_UPGRADE:
        {
            return OnAfterUpgrade(utData, ullParam);
        }
        break;
    case EVENT_WEB_SESSION:
        {
            return OnWebSession(utData, ullParam);
        }
        break;
    default:
        {
            DEV_WARN(TF("OnEvent can not diapatch event : %d[%p, %p]"), utEvent, utData, ullParam);
        }
    }
    return false;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpDispatch(const PacketPtr&, PTCP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnUdpDispatch(const PacketPtr&, PUDP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpAccept(Socket, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpConnect(UInt, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpRecv(size_t, PTCP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpSend(UInt, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTcpClose(Socket, ULLong)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnUdpRecv(size_t, PUDP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnUdpSend(UInt, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnUdpClose(Socket, ULLong)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTlsHandShake(UInt, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTlsRecv(size_t, PTCP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTlsSend(size_t, PTCP_PARAM)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnTlsError(UInt, Socket)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnBeforeUpgrade(uintptr_t, ULLong)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnAfterUpgrade(uintptr_t, ULLong)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE bool CTNETDispatch<TPACKET, bNetByteOrder>::OnWebSession(uintptr_t, ULLong)
{
    return true;
}

template <typename TPACKET, bool bNetByteOrder>
INLINE void CTNETDispatch<TPACKET, bNetByteOrder>::DumpHexPacketData(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    Byte bHead[LMT_KEY] = { 0 };
    UInt uRet = (UInt)Stream.Read(bHead, LMT_KEY);

    XChar szDump[LMT_BUF] = { 0 };
    Int nPos = 0;
    if (utEvent == EVENT_TCP_RECV)
    {
        PTCP_PARAM pTcp = reinterpret_cast<PTCP_PARAM>(ullParam);
        nPos = CXChar::Format(szDump, LMT_BUF, TF("Socket %p recv TCP unknown packet[size=%d], packet head data :"), pTcp->sSocket, Stream.Size());
    }
    else
    {
        PUDP_PARAM pUdp = reinterpret_cast<PUDP_PARAM>(ullParam);
        if (pUdp->NetAddr.usAttr & ATTR_IPV6)
        {
            PUShort pIP = pUdp->NetAddr.Addr.usAddr;
            nPos = CXChar::Format(szDump, LMT_BUF, TF("Socket %p recv UDP[%X:%X:%X:%X:%X:%X:%X:%X]:%d unknown packet[size=%d], packet head data :"), pUdp->sSocket,
                                  pIP[0], pIP[1], pIP[2], pIP[3], pIP[4], pIP[5], pIP[6], pIP[7], pUdp->NetAddr.usPort, Stream.Size());
        }
        else // ATTR_IPV4
        {
            PByte pIP = pUdp->NetAddr.Addr.bAddr;
            nPos = CXChar::Format(szDump, LMT_BUF, TF("Socket %p recv UDP[%d.%d.%d.%d]:%d unknown packet[size=%d], packet head data :"), pUdp->sSocket,
                                  pIP[0], pIP[1], pIP[2], pIP[3], pUdp->NetAddr.usPort, Stream.Size());
        }
    }
    for (UInt i = 0; i < uRet; ++i)
    {
        if ((i % DUMP_HEX_LINE) == 0)
        {
            szDump[nPos] = TF('\n');
            ++nPos;
        }
        else if ((i % DUMP_HEX_DWORD) == 0)
        {
            szDump[nPos] = TF('-');
            ++nPos;
        }
        nPos += CXChar::Format((szDump + nPos), (LMT_BUF - nPos), TF("%02X"), bHead[i]);
    }
    DEV_DUMP(szDump);
}

///////////////////////////////////////////////////////////////////
// CTNetworkEventHandler
template <typename TDISPATCH>
INLINE CTNetworkEventHandler<TDISPATCH>::CTNetworkEventHandler(void)
{
}

template <typename TDISPATCH>
INLINE CTNetworkEventHandler<TDISPATCH>::~CTNetworkEventHandler(void)
{
}

template <typename TDISPATCH>
INLINE UInt CTNetworkEventHandler<TDISPATCH>::OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
{
    return (TDispath::OnEvent(utEvent, utData, ullParam) ? (UInt)RET_OKAY : (UInt)RET_FAIL);
}

template <typename TDISPATCH>
INLINE UInt CTNetworkEventHandler<TDISPATCH>::OnHandle(uintptr_t, CEventBase&, ULLong)
{
    return (UInt)RET_FAIL;
}

template <typename TDISPATCH>
INLINE UInt CTNetworkEventHandler<TDISPATCH>::OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
{
    return (TDispath::OnDispatch(utEvent, Stream, ullParam) ? (UInt)RET_OKAY : (UInt)RET_FAIL);
}

template <typename TDISPATCH>
INLINE UInt CTNetworkEventHandler<TDISPATCH>::OnHandle(uintptr_t, UInt)
{
    return (UInt)RET_FAIL;
}



#endif // __NETWORK_EVENT_INL__
