// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __VAR_TYPE_INL__
#define __VAR_TYPE_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CVarType
INLINE CVarType::CVarType(void)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    MM_SAFE::Set(&m_xValue, 0, sizeof(VALUE));
}

INLINE CVarType::CVarType(Short sValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(sValue);
}

INLINE CVarType::CVarType(UShort usValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(usValue);
}

INLINE CVarType::CVarType(Int nValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(nValue);
}

INLINE CVarType::CVarType(UInt uValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(uValue);
}

INLINE CVarType::CVarType(Long lValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(lValue);
}

INLINE CVarType::CVarType(ULong ulValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(ulValue);
}

INLINE CVarType::CVarType(LLong llValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(llValue);
}

INLINE CVarType::CVarType(ULLong ullValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(ullValue);
}

INLINE CVarType::CVarType(Double dValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(dValue);
}

INLINE CVarType::CVarType(bool bValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(bValue);
}

template <typename T>
INLINE CVarType::CVarType(T* pValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue<T>(pValue);
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE CVarType::CVarType(const CCString& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue);
}

INLINE CVarType::CVarType(CCString& strValue, bool bAttach)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue, bAttach);
}

INLINE CVarType::CVarType(PCStr pszValue, size_t stSize)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetCString(pszValue, stSize);
}
#else
INLINE CVarType::CVarType(const CWString& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue);
}

INLINE CVarType::CVarType(CWString& strValue, bool bAttach)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue, bAttach);
}

INLINE CVarType::CVarType(PCWStr pszValue, size_t stSize)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetWString(pszValue, stSize);
}
#endif

INLINE CVarType::CVarType(const CString& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue);
}

INLINE CVarType::CVarType(CString& strValue, bool bAttach)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(strValue, bAttach);
}

INLINE CVarType::CVarType(PCXStr pszValue, size_t stSize)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetXString(pszValue, stSize);
}

INLINE CVarType::CVarType(const CBufStream& bsValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(bsValue);
}

INLINE CVarType::CVarType(CBufStream& bsValue, bool bAttach)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(bsValue, bAttach);
}

INLINE CVarType::CVarType(PByte pbValue, size_t stSize, bool bAttach)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(pbValue, stSize, bAttach);
}

template <typename T>
INLINE CVarType::CVarType(const T& t)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue<T>(t);
}

INLINE CVarType::CVarType(const CVarType& aSrc)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(aSrc);
}

INLINE CVarType& CVarType::operator=(Short sValue)
{
    SetValue(sValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(UShort usValue)
{
    SetValue(usValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(Int nValue)
{
    SetValue(nValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(UInt uValue)
{
    SetValue(uValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(Long lValue)
{
    SetValue(lValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(ULong ulValue)
{
    SetValue(ulValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(LLong llValue)
{
    SetValue(llValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(ULLong ullValue)
{
    SetValue(ullValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(Double dValue)
{
    SetValue(dValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(bool bValue)
{
    SetValue(bValue);
    return (*this);
}

template <typename T>
INLINE CVarType& CVarType::operator=(T* pValue)
{
    SetValue<T>(pValue);
    return (*this);
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE CVarType& CVarType::operator=(const CCString& strValue)
{
    SetValue(strValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(PCStr pszValue)
{
    SetCString(pszValue);
    return (*this);
}
#else
INLINE CVarType& CVarType::operator=(const CWString& strValue)
{
    SetValue(strValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(PCWStr pszValue)
{
    SetWString(pszValue);
    return (*this);
}
#endif
INLINE CVarType& CVarType::operator=(const CString& strValue)
{
    SetValue(strValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(PCXStr pszValue)
{
    SetXString(pszValue);
    return (*this);
}

INLINE CVarType& CVarType::operator=(const CBufStream& bsValue)
{
    SetValue(bsValue);
    return (*this);
}

template <typename T>
INLINE CVarType& CVarType::operator=(const T& t)
{
    SetValue<T>(t);
    return (*this);
}

INLINE CVarType& CVarType::operator=(const CVarType& aSrc)
{
    if (&aSrc != this)
    {
        SetValue(aSrc);
    }
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE CVarType::CVarType(CCString&& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(std::move(strValue));
}
#else
INLINE CVarType::CVarType(CWString&& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(std::move(strValue));
}
#endif
INLINE CVarType::CVarType(CString&& strValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(std::move(strValue));
}

INLINE CVarType::CVarType(CBufStream&& bsValue)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(std::move(bsValue));
}

template <typename T, typename X>
INLINE CVarType::CVarType(T&& t)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue<T, X>(std::move(t));
}

INLINE CVarType::CVarType(CVarType&& aSrc)
: m_pCType(nullptr)
, m_stType(VART_NONE)
, m_stCache(0)
, m_stSize(0)
{
    SetValue(std::move(aSrc));
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE CVarType& CVarType::operator=(CCString&& strValue)
{
    SetValue(std::move(strValue));
    return (*this);
}
#else
INLINE CVarType& CVarType::operator=(CWString&& strValue)
{
    SetValue(std::move(strValue));
    return (*this);
}
#endif
INLINE CVarType& CVarType::operator=(CString&& strValue)
{
    SetValue(std::move(strValue));
    return (*this);
}

INLINE CVarType& CVarType::operator=(CBufStream&& bsValue)
{
    SetValue(std::move(bsValue));
    return (*this);
}

template <typename T, typename X>
INLINE CVarType& CVarType::operator=(T&& t)
{
    SetValue<T, X>(std::move(t));
    return (*this);
}

INLINE CVarType& CVarType::operator=(CVarType&& aSrc)
{
    if (&aSrc != this)
    {
        SetValue(std::move(aSrc));
    }
    return (*this);
}
#endif

INLINE CVarType::~CVarType(void)
{
    Free();
}

INLINE VAR_TYPE CVarType::GetType(void) const
{
    return (VAR_TYPE)m_stType;
}

INLINE size_t CVarType::GetSize(void) const
{
    return m_stSize;
}

INLINE bool CVarType::IsNone(void) const
{
    return (m_stType == VART_NONE);
}

INLINE bool CVarType::IsType(VAR_TYPE eType) const
{
    return ((m_stType & VART_MASK) == (size_t)eType);
}

template <typename T>
INLINE bool CVarType::IsPtrType(void) const
{
    if (m_stType == (size_t)VART_POINTER)
    {
#ifndef __MODERN_CXX_NOT_SUPPORTED
        return ((m_pCType != nullptr) && (*m_pCType == typeid(typename std::decay<T>::type)));
#else
        return ((m_pCType != nullptr) && (*m_pCType == typeid(T)));
#endif
    }
    return false;
}

INLINE bool CVarType::IsCacheType(VAR_TYPE eType) const
{
    return ((m_stType & (VART_MEMCACHE|VART_MASK)) == (VART_MEMCACHE|(size_t)eType));
}

template <typename T>
INLINE bool CVarType::IsAnyType(void) const
{
    if (m_stType == (size_t)VART_ANYTYPE)
    {
#ifndef __MODERN_CXX_NOT_SUPPORTED
        return ((m_pCType != nullptr) && (*m_pCType == typeid(typename std::decay<T>::type)));
#else
        return ((m_pCType != nullptr) && (*m_pCType == typeid(T)));
#endif
    }
    return false;
}

INLINE Short CVarType::GetShort(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (Short)m_xValue.llValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (Short)(LLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE UShort CVarType::GetUShort(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (UShort)m_xValue.ullValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (UShort)(ULLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Int CVarType::GetInt(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (Int)m_xValue.llValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (Int)(LLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE UInt CVarType::GetUInt(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (UInt)m_xValue.ullValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (UInt)(ULLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Long CVarType::GetLong(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (Long)m_xValue.llValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (Long)(LLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE ULong CVarType::GetULong(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (ULong)m_xValue.ullValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (ULong)(ULLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE LLong CVarType::GetLLong(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (LLong)m_xValue.llValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (LLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE ULLong CVarType::GetULLong(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return (ULLong)m_xValue.ullValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (ULLong)m_xValue.dValue;
        }
        break;
    default:
        {
            return 0;
        }
    }
}

INLINE Double CVarType::GetDouble(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_BOOL:
        {
            return static_cast<Double>(m_xValue.llValue);
        }
        break;
    case VART_DOUBLE:
        {
            return m_xValue.dValue;
        }
        break;
    default:
        {
            return 0.0;
        }
    }
}

INLINE bool CVarType::GetBoolean(void) const
{
    switch (m_stType)
    {
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
        {
            return (m_xValue.llValue != 0) ? true : false;
        }
        break;
    case VART_BOOL:
        {
            return m_xValue.bValue;
        }
        break;
    case VART_DOUBLE:
        {
            return (m_xValue.dValue != 0.0) ? true : false;
        }
        break;
    default:
        {
            return false;
        }
    }
}

template <typename T>
INLINE T* CVarType::GetPtr(void) const
{
    if (IsPtrType<T>())
    {
        return reinterpret_cast<T*>(m_xValue.pValue);
    }
    return nullptr;
}

INLINE CCString CVarType::GetCString(bool bAttach)
{
    CCString strValue;
    GetCString(strValue, bAttach);
    return strValue;
}

INLINE CWString CVarType::GetWString(bool bAttach)
{
    CWString strValue;
    GetWString(strValue, bAttach);
    return strValue;
}

INLINE CString CVarType::GetXString(bool bAttach)
{
    CString strValue;
    GetXString(strValue, bAttach);
    return strValue;
}

template <typename T>
const T& CVarType::AnyCast(void) const
{
    if (IsAnyType<T>())
    {
        CTAny<T>* pAny = dynamic_cast<CTAny<T>*>(m_xValue.ptValue);
        return pAny->m_Any;
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename T>
T& CVarType::AnyCast(void)
{
    if (IsAnyType<T>())
    {
        CTAny<T>* pAny = dynamic_cast<CTAny<T>*>(m_xValue.ptValue);
        return pAny->m_Any;
    }
    throw (E_ACCESS_VIOLATION);
}

template <typename T>
T& CVarType::NewCast(void)
{
    if (IsNone())
    {
        SetValue<T>();
    }
    return AnyCast<T>();
}

template <typename T>
T& CVarType::ResetCast(void)
{
    if (IsAnyType<T>() == false)
    {
        SetValue<T>();
    }
    return AnyCast<T>();
}

INLINE bool CVarType::GetValue(Short& sValue) const
{
    sValue = GetShort();
    return (sValue != 0);
}

INLINE bool CVarType::GetValue(UShort& usValue) const
{
    usValue = GetUShort();
    return (usValue != 0);
}

INLINE bool CVarType::GetValue(Int& nValue) const
{
    nValue = GetInt();
    return (nValue != 0);
}

INLINE bool CVarType::GetValue(UInt& uValue) const
{
    uValue = GetUInt();
    return (uValue != 0);
}

INLINE bool CVarType::GetValue(Long& lValue) const
{
    lValue = GetLong();
    return (lValue != 0);
}

INLINE bool CVarType::GetValue(ULong& ulValue) const
{
    ulValue = GetULong();
    return (ulValue != 0);
}

INLINE bool CVarType::GetValue(LLong& llValue) const
{
    llValue = GetLLong();
    return (llValue != 0);
}

INLINE bool CVarType::GetValue(ULLong& ullValue) const
{
    ullValue = GetULLong();
    return (ullValue != 0);
}

INLINE bool CVarType::GetValue(Double& dValue) const
{
    dValue = GetDouble();
    return (dValue != 0.0);
}

INLINE bool CVarType::GetValue(bool& bValue) const
{
    bValue = GetBoolean();
    return bValue;
}

template <typename T>
INLINE bool CVarType::GetValue(T*& pValue) const
{
    pValue = GetPtr<T>();
    return (pValue != nullptr);
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE bool CVarType::GetValue(CCString& strValue, bool bAttach)
{
    return GetCString(strValue, bAttach);
}
#else
INLINE bool CVarType::GetValue(CWString& strValue, bool bAttach)
{
    return GetWString(strValue, bAttach);
}
#endif

INLINE bool CVarType::GetValue(CString& strValue, bool bAttach)
{
    return GetXString(strValue, bAttach);
}

INLINE bool CVarType::GetValue(CBufStream& bsValue, bool bAttach)
{
    if (IsCacheType(VART_UCHAR) && (m_xValue.pbValue != nullptr))
    {
        bsValue.Attach(m_stSize, m_xValue.pbValue, bAttach);
        if (bAttach)
        {
            Reset(false);
        }
        return true;
    }
    return false;
}

template <typename T>
const T* CVarType::TryCast(void) const
{
    if (IsAnyType<T>())
    {
        CTAny<T>* pAny = dynamic_cast<CTAny<T>*>(m_xValue.ptValue);
        return &(pAny->m_Any);
    }
    return nullptr;
}

template <typename T>
T* CVarType::TryCast(void)
{
    if (IsAnyType<T>())
    {
        CTAny<T>* pAny = dynamic_cast<CTAny<T>*>(m_xValue.ptValue);
        return &(pAny->m_Any);
    }
    return nullptr;
}

template <typename T>
T* CVarType::TryNew(void)
{
    if (IsNone())
    {
        SetValue<T>();
    }
    return TryCast<T>();
}

template <typename T>
T* CVarType::TryReset(void)
{
    if (IsAnyType<T>() == false)
    {
        SetValue<T>();
    }
    return TryCast<T>();
}

INLINE bool CVarType::SetValue(Short sValue)
{
    Reset();
    m_xValue.llValue = sValue;
    m_stType = (size_t)VART_SHORT;
    return true;
}

INLINE bool CVarType::SetValue(UShort usValue)
{
    Reset();
    m_xValue.ullValue = usValue;
    m_stType = (size_t)VART_USHORT;
    return true;
}

INLINE bool CVarType::SetValue(Int nValue)
{
    Reset();
    m_xValue.llValue = nValue;
    m_stType = (size_t)VART_INT;
    return true;
}

INLINE bool CVarType::SetValue(UInt uValue)
{
    Reset();
    m_xValue.ullValue = uValue;
    m_stType = (size_t)VART_UINT;
    return true;
}

INLINE bool CVarType::SetValue(Long lValue)
{
    Reset();
    m_xValue.llValue = lValue;
    m_stType = (size_t)VART_LONG;
    return true;
}

INLINE bool CVarType::SetValue(ULong ulValue)
{
    Reset();
    m_xValue.ullValue = ulValue;
    m_stType = (size_t)VART_ULONG;
    return true;
}

INLINE bool CVarType::SetValue(LLong llValue)
{
    Reset();
    m_xValue.llValue = llValue;
    m_stType = (size_t)VART_LLONG;
    return true;
}

INLINE bool CVarType::SetValue(ULLong ullValue)
{
    Reset();
    m_xValue.ullValue = ullValue;
    m_stType = (size_t)VART_ULLONG;
    return true;
}

INLINE bool CVarType::SetValue(Double dValue)
{
    Reset();
    m_xValue.dValue = dValue;
    m_stType = (size_t)VART_DOUBLE;
    return true;
}

INLINE bool CVarType::SetValue(bool bValue)
{
    Reset();
    m_xValue.bValue = bValue;
    m_stType = (size_t)VART_BOOL;
    return true;
}

template <typename T>
INLINE bool CVarType::SetValue(T* pValue)
{
    Reset();
    m_xValue.pValue = (void*)pValue;
    m_stType = (size_t)VART_POINTER;
#ifndef __MODERN_CXX_NOT_SUPPORTED
    m_pCType = &(typeid(typename std::decay<T>::type));
#else
    m_pCType = &(typeid(T));
#endif
    return true;
}

#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE bool CVarType::SetValue(const CCString& strValue)
{
    return SetCString(*strValue, strValue.Length());
}

INLINE bool CVarType::SetValue(CCString& strValue, bool bAttach)
{
    if (bAttach)
    {
        Reset();
        strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_CHAR);
        return true;
    }
    else
    {
        return SetCString(*strValue, strValue.Length());
    }
}
#else
INLINE bool CVarType::SetValue(const CWString& strValue)
{
    return SetWString(*strValue, strValue.Length());
}

INLINE bool CVarType::SetValue(CWString& strValue, bool bAttach)
{
    if (bAttach)
    {
        Reset();
        strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_WCHAR);
        return true;
    }
    else
    {
        return SetWString(*strValue, strValue.Length());
    }
}
#endif

INLINE bool CVarType::SetValue(const CString& strValue)
{
    return SetXString(*strValue, strValue.Length());
}

INLINE bool CVarType::SetValue(CString& strValue, bool bAttach)
{
    if (bAttach)
    {
        Reset();
        strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_XCHAR);
        return true;
    }
    else
    {
        return SetXString(*strValue, strValue.Length());
    }
}

INLINE bool CVarType::SetCString(PCStr pszValue, size_t stSize)
{
    assert(pszValue);
    if (stSize == 0)
    {
        stSize = CChar::Length(pszValue);
    }
    if ((stSize > 0) && Alloc(stSize))
    {
        MM_SAFE::Cpy(m_xValue.pValue, m_stSize, pszValue, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_CHAR);
        return true;
    }
    return false;
}

INLINE bool CVarType::SetWString(PCWStr pszValue, size_t stSize)
{
    assert(pszValue);
    if (stSize == 0)
    {
        stSize = CWChar::Length(pszValue) * sizeof(WChar);
    }
    if ((stSize > 0) && Alloc(stSize))
    {
        MM_SAFE::Cpy(m_xValue.pValue, m_stSize, pszValue, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_WCHAR);
        return true;
    }
    return false;
}

INLINE bool CVarType::SetXString(PCXStr pszValue, size_t stSize)
{
    assert(pszValue);
    if (stSize == 0)
    {
        stSize = CXChar::Length(pszValue) * sizeof(XChar);
    }
    if ((stSize > 0) && Alloc(stSize))
    {
        MM_SAFE::Cpy(m_xValue.pValue, m_stSize, pszValue, m_stSize);
        m_stType = (size_t)(VART_MEMCACHE|VART_XCHAR);
        return true;
    }
    return false;
}

INLINE bool CVarType::SetValue(const CBufStream& bsValue)
{
    return SetValue(bsValue.GetBuf(), bsValue.Size());
}

INLINE bool CVarType::SetValue(CBufStream& bsValue, bool bAttach)
{
    if (SetValue(bsValue.GetBuf(), bsValue.Size(), bAttach))
    {
        if (bAttach)
        {
            bsValue.Detach();
        }
        return true;
    }
    return false;
}

INLINE bool CVarType::SetValue(PByte pbValue, size_t stSize, bool bAttach)
{
    assert(pbValue != nullptr);
    if (stSize > 0)
    {
        if (bAttach)
        {
            Attach(pbValue, stSize);
            m_stType = (size_t)(VART_MEMCACHE | VART_UCHAR);
            return true;
        }
        else if (Alloc(stSize, false))
        {
            MM_SAFE::Cpy(m_xValue.pValue, m_stSize, pbValue, m_stSize);
            m_stType = (size_t)(VART_MEMCACHE|VART_UCHAR);
            return true;
        }
    }
    return false;
}

template <typename T>
INLINE bool CVarType::SetValue(void)
{
    Reset();
#ifndef __MODERN_CXX_NOT_SUPPORTED
    m_xValue.ptValue = MNEW CTAny<typename std::decay<T>::type>();
    if (m_xValue.ptValue != nullptr)
    {
        m_stType = (size_t)VART_ANYTYPE;
        m_pCType = &(typeid(typename std::decay<T>::type));
        DEV_DEBUG(TF("CVarType::SetValue() at %p as %s"), m_xValue.ptValue, m_pCType->name());
        return true;
    }
#else
    m_xValue.ptValue = MNEW CTAny<T>();
    if (m_xValue.ptValue != nullptr)
    {
        m_stType = (size_t)VART_ANYTYPE;
        m_pCType = &(typeid(T));
        DEV_DEBUG(TF("CVarType::SetValue() at %p as %s"), m_xValue.ptValue, m_pCType->name());
        return true;
    }
#endif
    return false;
}

template <typename T>
INLINE bool CVarType::SetValue(const T& t)
{
    Reset();
#ifndef __MODERN_CXX_NOT_SUPPORTED
    m_xValue.ptValue = MNEW CTAny<typename std::decay<T>::type>(t);
    if (m_xValue.ptValue != nullptr)
    {
        m_stType = (size_t)VART_ANYTYPE;
        m_pCType = &(typeid(typename std::decay<T>::type));
        DEV_DEBUG(TF("CVarType::SetValue(T&) at %p as %s"), m_xValue.ptValue, m_pCType->name());
        return true;
    }
#else
    m_xValue.ptValue = MNEW CTAny<T>(t);
    if (m_xValue.ptValue != nullptr)
    {
        m_stType = (size_t)VART_ANYTYPE;
        m_pCType = &(typeid(T));
        DEV_DEBUG(TF("CVarType::SetValue(T&) at %p as %s"), m_xValue.ptValue, m_pCType->name());
        return true;
    }
#endif
    return false;
}

INLINE bool CVarType::SetValue(const CVarType& aSrc)
{
    if (&aSrc == this)
    {
        return false;
    }
    Reset();
    switch (aSrc.m_stType)
    {
    case VART_POINTER:
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_DOUBLE:
    case VART_BOOL:
        {
            m_pCType = aSrc.m_pCType;
            m_stType = aSrc.m_stType;
            MM_SAFE::Cpy(&m_xValue, sizeof(VALUE), &aSrc.m_xValue, sizeof(VALUE));
        }
        break;
    case (VART_MEMCACHE|VART_CHAR):
    case (VART_MEMCACHE|VART_WCHAR):
    case (VART_MEMCACHE|VART_XCHAR):
    case (VART_MEMCACHE|VART_UCHAR):
        {
            assert(aSrc.m_pCType == nullptr);
            if ((aSrc.m_xValue.pValue != nullptr) && Alloc(aSrc.m_stSize))
            {
                MM_SAFE::Cpy(m_xValue.pValue, m_stSize, aSrc.m_xValue.pValue, m_stSize);
                m_stType = aSrc.m_stType;
            }
        }
        break;
    case VART_ANYTYPE:
        {
            if (aSrc.Clone(m_xValue.ptValue) == false)
            {
                return false;
            }
            m_pCType = aSrc.m_pCType;
            m_stType = aSrc.m_stType;
        }
        break;
    default: { }
    }
    return true;
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
#ifdef __RUNTIME_CHARSET_WCHAR__
INLINE bool CVarType::SetValue(CCString&& strValue)
{
    Reset();
    strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
    m_stType = (size_t)(VART_MEMCACHE|VART_CHAR);
    return true;
}
#else
INLINE bool CVarType::SetValue(CWString&& strValue)
{
    Reset();
    strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
    m_stType = (size_t)(VART_MEMCACHE|VART_WCHAR);
    return true;
}
#endif
INLINE bool CVarType::SetValue(CString&& strValue)
{
    Reset();
    strValue.Detach(m_xValue.pbValue, m_stCache, m_stSize);
    m_stType = (size_t)(VART_MEMCACHE|VART_XCHAR);
    return true;
}

INLINE bool CVarType::SetValue(CBufStream&& bsValue)
{
    Attach(bsValue.GetBuf(), bsValue.Size());
    bsValue.Detach();
    m_stType = (size_t)(VART_MEMCACHE|VART_UCHAR);
    return true;
}

template <typename T, typename X>
INLINE bool CVarType::SetValue(T&& t)
{
    Reset();
    m_xValue.ptValue = MNEW CTAny<X>(std::forward<T>(t));
    if (m_xValue.ptValue != nullptr)
    {
        m_stType = (size_t)VART_ANYTYPE;
        m_pCType = &(typeid(X));
        DEV_DEBUG(TF("CVarType::SetValue(T&&) at %p as %s"), m_xValue.ptValue, m_pCType->name());
        return true;
    }
    return false;
}

INLINE bool CVarType::SetValue(CVarType&& aSrc)
{
    if (&aSrc == this)
    {
        return false;
    }
    Reset();
    switch (aSrc.m_stType)
    {
    case VART_POINTER:
    case VART_SHORT:
    case VART_USHORT:
    case VART_INT:
    case VART_UINT:
    case VART_LONG:
    case VART_ULONG:
    case VART_LLONG:
    case VART_ULLONG:
    case VART_DOUBLE:
    case VART_BOOL:
        {
            m_pCType = aSrc.m_pCType;
            m_stType = aSrc.m_stType;
            MM_SAFE::Cpy(&m_xValue, sizeof(VALUE), &aSrc.m_xValue, sizeof(VALUE));
            aSrc.Reset(false);
        }
        break;
    case (VART_MEMCACHE|VART_CHAR):
    case (VART_MEMCACHE|VART_WCHAR):
    case (VART_MEMCACHE|VART_XCHAR):
    case (VART_MEMCACHE|VART_UCHAR):
        {
            assert(aSrc.m_pCType == nullptr);
            m_stType  = aSrc.m_stType;
            m_stCache = aSrc.m_stCache;
            m_stSize  = aSrc.m_stSize;
            m_xValue.pbValue = aSrc.m_xValue.pbValue;

            aSrc.Reset(false);
        }
        break;
    case VART_ANYTYPE:
        {
            m_xValue.ptValue = aSrc.m_xValue.ptValue;
            m_pCType = aSrc.m_pCType;
            m_stType = aSrc.m_stType;

            aSrc.Reset(false);
        }
        break;
    default: { }
    }
    return true;
}
#endif

INLINE void CVarType::Reset(bool bFree)
{
    if (bFree)
    {
        Free();
    }
    if (m_stType != (size_t)VART_NONE)
    {
        m_stCache = 0;
        m_stSize  = 0;
        m_stType  = (size_t)VART_NONE;
        m_pCType  = nullptr;
        MM_SAFE::Set(&m_xValue, 0, sizeof(VALUE));
    }
}

INLINE bool CVarType::GetCString(CCString& strValue, bool bAttach)
{
    if (IsCacheType(VART_CHAR) && (m_xValue.pbValue != nullptr))
    {
        if (bAttach)
        {
            strValue.Attach(m_xValue.pbValue, m_stCache, m_stSize);
            Reset(false);
        }
        else
        {
            strValue.FillBuffer((PCStr)m_xValue.pValue, m_stSize);
        }
        return true;
    }
    return false;
}

INLINE bool CVarType::GetWString(CWString& strValue, bool bAttach)
{
    if (IsCacheType(VART_WCHAR) && (m_xValue.pbValue != nullptr))
    {
        if (bAttach)
        {
            strValue.Attach(m_xValue.pbValue, m_stCache, m_stSize);
            Reset(false);
        }
        else
        {
            strValue.FillBuffer((PCWStr)m_xValue.pValue, (m_stSize / sizeof(WChar)));
        }
        return true;
    }
    return false;
}

INLINE bool CVarType::GetXString(CString& strValue, bool bAttach)
{
    if (IsCacheType(VART_XCHAR) && (m_xValue.pbValue != nullptr))
    {
        if (bAttach)
        {
            strValue.Attach(m_xValue.pbValue, m_stCache, m_stSize);
            Reset(false);
        }
        else
        {
            strValue.FillBuffer((PCXStr)m_xValue.pValue, (m_stSize / sizeof(XChar)));
        }
        return true;
    }
    return false;
}

INLINE void CVarType::Attach(PByte pData, size_t stData)
{
    Free();
    m_xValue.pbValue = pData;
    m_pCType  = nullptr;
    m_stCache = stData;
    m_stSize  = stData;
    DEV_DEBUG(TF("CVarType::Attach at %p as MEMCACHE"), m_xValue.pValue);
}

INLINE bool CVarType::Alloc(size_t stNew, bool bAlign)
{
    if (stNew > m_stCache)
    {
        size_t stNewCache = bAlign ? DEF::Align<size_t>(stNew, (size_t)LMT_KEY) : stNew;
        void* pNew = ALLOC( stNewCache );
        if (pNew != nullptr)
        {
            Free();
            m_stCache = stNewCache;
            m_stSize  = stNew;
            m_xValue.pValue = pNew;
            DEV_DEBUG(TF("CVarType::Alloc at %p as MEMCACHE"), m_xValue.pValue);
            return true;
        }
    }
    else
    {
        m_stSize = stNew;
        return true;
    }
    return false;
}

INLINE void CVarType::Free(void)
{
    if (m_xValue.pValue != nullptr)
    {
        if ((m_stType & VART_MEMCACHE) != 0)
        {
            FREE( m_xValue.pValue );
            DEV_DEBUG(TF("CVarType::Free at %p as MEMCACHE"), m_xValue.pValue);
        }
        else if (m_stType == VART_ANYTYPE)
        {
            MDELETE m_xValue.ptValue;
            DEV_DEBUG(TF("CVarType::Free at %p as %s"), m_xValue.ptValue, m_pCType->name());
        }
        m_xValue.pValue = nullptr;
    }
}

INLINE bool CVarType::Clone(CAny*& pAny) const
{
    assert(m_pCType != nullptr);
    if (m_xValue.ptValue != nullptr)
    {
        return m_xValue.ptValue->Clone(pAny);
    }
    return false;
}

#endif // __VAR_TYPE_INL__
