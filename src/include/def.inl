// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __DEF_INL__
#define __DEF_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// global INLINE functions
template <typename T>
INLINE T DEF::Abs(const T t)
{
    return (t >= (T)0) ? t : -t;
}

template <typename T>
INLINE T DEF::Max(const T a, const T b)
{
    return (a >= b) ? a : b;
}

template <typename T>
INLINE T DEF::Min(const T a, const T b)
{
    return (a <= b) ? a : b;
}

template <typename T>
INLINE T DEF::Maxmin(const T maxa, const T maxb, const T minc)
{
    const T t = (maxa >= maxb) ? maxa : maxb;
    return (t <= minc) ? t : minc;
}

template <typename T>
INLINE T DEF::Align(const T t, const T align)
{
    return (T)((t + (align - 1)) & ~(align - 1));
}

template <typename T>
INLINE T DEF::Swap(T& a, T& b)
{
    const T t = a;
    a = b;
    b = t;
    return t;
}

#endif // __DEF_INL__
