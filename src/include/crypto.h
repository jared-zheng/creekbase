// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CRYPTO_H__
#define __CRYPTO_H__

#pragma once

#include "streambuf.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CMD5
class CMD5 : public MObject
{
public:
    enum MD5_CONST
    {
        MD5C_NUM = 16,
        MD5C_STR = 36,
    };

    typedef Byte NUM[MD5C_NUM];

    struct tagMD5_CONTEXT : public MObject
    {
        UInt   uTotal[2];
        UInt   uState[4];
        Byte   bBuffer[64];
    };
    typedef tagMD5_CONTEXT   MD5_CONTEXT, *PMD5_CONTEXT;

    static CPCXStr StrFormat;
public:
    static bool Crypto(const CString& strSrc, CString& str, size_t stSize = 0, bool bAppend = false);
    static bool Crypto(const CString& strSrc, CStream& StreamDst, size_t stSize = 0);
    static bool Crypto(const CString& strSrc, NUM& Num, size_t stSize = 0);

    static bool Crypto(CStream& StreamSrc, CString& str, size_t stSize = 0, bool bAppend = false);
    static bool Crypto(CStream& StreamSrc, CStream& StreamDst, size_t stSize = 0);
    static bool Crypto(CStream& StreamSrc, NUM& Num, size_t stSize = 0);

    static bool ToString(const NUM& Num, CString& str, bool bAppend = false);
    static bool ToString(const NUM& Num, CString& str, PCXStr pszFormat, bool bAppend = false);
private:
    CMD5(void);
    ~CMD5(void);

    CMD5(const CMD5&);
    CMD5& operator=(const CMD5&);
private:
    static void Init(MD5_CONTEXT& ctx);
    static void Update(MD5_CONTEXT& ctx, CStream& StreamSrc, size_t stSize);
    static void Final(MD5_CONTEXT& ctx, PByte pOut);

    static void Transform(MD5_CONTEXT& ctx, PByte pIn);

    static void FromUInt(PByte pOut, UInt uIn);
    static void ToUInt(UInt& uOut, PByte pIn);

    static UInt F1(UInt x, UInt y, UInt z);
    static UInt F2(UInt x, UInt y, UInt z);
    static UInt F3(UInt x, UInt y, UInt z);
    static UInt F4(UInt x, UInt y, UInt z);

    static UInt S(UInt x, UInt n);

    static void T1(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t);
    static void T2(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t);
    static void T3(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t);
    static void T4(UInt& a, UInt b, UInt c, UInt d, UInt x, UInt s, UInt t);
};

///////////////////////////////////////////////////////////////////
// CSHA1
class CSHA1 : public MObject
{
public:
    enum SHA1_CONST
    {
        SHA1C_NUM = 20,
        SHA1C_STR = 48,
    };

    typedef Byte NUM[SHA1C_NUM];

    struct tagSHA1_CONTEXT : public MObject
    {
        UInt   uTotal[2];
        UInt   uState[5];
        Byte   bBuffer[64];
    };
    typedef tagSHA1_CONTEXT   SHA1_CONTEXT, *PSHA1_CONTEXT;

    static CPCXStr StrFormat;
public:
    static bool Crypto(const CString& strSrc, CString& str, size_t stSize = 0, bool bAppend = false);
    static bool Crypto(const CString& strSrc, CStream& StreamDst, size_t stSize = 0);
    static bool Crypto(const CString& strSrc, NUM& Num, size_t stSize = 0);

    static bool Crypto(CStream& StreamSrc, CString& str, size_t stSize = 0, bool bAppend = false);
    static bool Crypto(CStream& StreamSrc, CStream& StreamDst, size_t stSize = 0);
    static bool Crypto(CStream& StreamSrc, NUM& Num, size_t stSize = 0);

    static bool ToString(const NUM& Num, CString& str, bool bAppend = false);
    static bool ToString(const NUM& Num, CString& str, PCXStr pszFormat, bool bAppend = false);
private:
    CSHA1(void);
    ~CSHA1(void);
    CSHA1(const CSHA1&);
    CSHA1& operator=(const CSHA1&);

    static void Init(SHA1_CONTEXT& ctx);
    static void Update(SHA1_CONTEXT& ctx, CStream& StreamSrc, size_t stSize);
    static void Final(SHA1_CONTEXT& ctx, PByte pOut);

    static void Transform(SHA1_CONTEXT& ctx, PByte pIn);

    static void FromUInt(PByte pOut, UInt uIn);
    static void ToUInt(UInt& uOut, PByte pIn);

    static UInt F1(UInt x, UInt y, UInt z);
    static UInt F2(UInt x, UInt y, UInt z);
    static UInt F3(UInt x, UInt y, UInt z);
    static UInt F4(UInt x, UInt y, UInt z);

    static UInt S(UInt x, UInt n);
    static UInt R(PUInt pw, UInt n);

    static void T1(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x);
    static void T2(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x);
    static void T3(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x);
    static void T4(UInt a, UInt& b, UInt c, UInt d, UInt& e, UInt x);
};

///////////////////////////////////////////////////////////////////
#include "crypto.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CRYPTO_H__
