// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TRAITS_INL__
#define __TRAITS_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CTElementTraitsBase
template<typename T>
INLINE void CTElementTraitsBase<T>::CopyElements(T* pDst, const T* pSrc, size_t stElements)
{
    for (size_t i = 0; i < stElements; ++i)
    {
        pDst[i] = pSrc[i];
    }
}

template<typename T>
INLINE void CTElementTraitsBase<T>::RelocateElements(T* pDst, T* pSrc, size_t stElements)
{
    // A simple memmove works for nearly all types.
    // You'll have to override this for
    // types that have pointers to their own members.
    MM_SAFE::Mov(pDst, stElements * sizeof(T), pSrc, stElements * sizeof(T));
}

///////////////////////////////////////////////////////////////////
// CTElementTraitsCompare
template <typename T>
INLINE bool CTElementTraitsCompare<T>::CompareElements(const T& t1, const T& t2)
{
    return ((t1 == t2) != 0);
}

template <typename T>
INLINE Int CTElementTraitsCompare<T>::CompareElementsOrdered(const T& t1, const T& t2)
{
    if (t1 < t2)
    {
        return (Int)(-1);
    }
    else if (t1 == t2)
    {
        return (Int)(0);
    }
    else
    {
        assert(t1 > t2);
        return (Int)(1);
    }
}

///////////////////////////////////////////////////////////////////
// CTElementTraitsHash
template<typename T>
INLINE size_t CTElementTraitsHash<T>::HashElements(const T& t)
{
    return (CHash::Hash(t));
}

///////////////////////////////////////////////////////////////////
// CTXCharTraitsBase
template <typename T>
SELECTANY const Byte CTXCharTraitsBase<T>::UTF8_PREFIX[] =
{
    0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC
};

template <typename T>
SELECTANY const Byte CTXCharTraitsBase<T>::UTF8_MASKS[] =
{
    0x7F, 0x1F, 0x0F, 0x07, 0x03, 0x01
};

template <typename T>
SELECTANY const Byte CTXCharTraitsBase<T>::UTF8_LEN[] =
{
    2, 2, 2, 2,  2, 2, 2, 2,  2, 2, 2, 2,  2, 2, 2, 2,
    3, 3, 3, 3,  3, 3, 3, 3,
    4, 4, 4, 4,
    5
};

template <typename T>
SELECTANY const UInt CTXCharTraitsBase<T>::UTF32_CODEUP[] =
{
    0x80,       // 0x00000000 - 0x0000007F
    0x800,      // 0x00000080 - 0x000007FF
    0x10000,    // 0x00000800 - 0x0000FFFF
    0x200000,   // 0x00010000 - 0x001FFFFF
    0x4000000,  // 0x00200000 - 0x03FFFFFF
    0x80000000, // 0x04000000 - 0x7FFFFFFF
};

template <typename T>
INLINE Int CTXCharTraitsBase<T>::CheckBOM(PUChar& psz, bool bOffset)
{
    assert((psz != nullptr) && (psz[0] > 0));
    if ((psz[0] == BOML_UTF80) && (psz[1] == BOML_UTF81) && (psz[2] == BOML_UTF82))
    {
        if (bOffset)
        {
            psz += BOML_UTF8;
        }
        return ENT_UTF8;
    }
    else if ((psz[0] == BOML_UTF160) && (psz[1] == BOML_UTF161) && (psz[2] != 0))
    {
        if (bOffset)
        {
            psz += BOML_UTF16;
        }
        return ENT_UTF16;
    }
    else if ((psz[0] == BOML_UTF161) && (psz[1] == BOML_UTF160) && (psz[2] != 0))
    {
        if (bOffset)
        {
            psz += BOML_UTF16;
        }
        return ENT_UTF16R;
    }
    else if ((psz[0] == BOML_UTF320) && (psz[1] == BOML_UTF321) && (psz[2] == BOML_UTF322) && (psz[3] == BOML_UTF323))
    {
        if (bOffset)
        {
            psz += BOML_UTF32;
        }
        return ENT_UTF32;
    }
    else if ((psz[0] == BOML_UTF323) && (psz[1] == BOML_UTF322) && (psz[2] == BOML_UTF321) && (psz[3] == BOML_UTF320))
    {
        if (bOffset)
        {
            psz += BOML_UTF32;
        }
        return ENT_UTF32R;
    }
    return ENT_LOCAL;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf32ToUtf8(UInt u32, PUChar psz8, Int nSize8)
{
    assert((psz8 != nullptr) && (nSize8 > 0));
    if ((u32 == 0) || (psz8 == nullptr) || (nSize8 <= 0))
    {
        return 0;
    }
    Int i = 0;
    for (; i < CONVERTC_UTF32_CODEUP; ++i)
    {
        if (u32 < UTF32_CODEUP[i]) break;
    }
    if (i < CONVERTC_UTF32_CODEUP)
    {
        Int nLen = i + CONVERTC_UTF8_BIT_LEAD;
        if (nSize8 >= nLen)
        {
            for (; i > 0; --i)
            {
                psz8[i] = (UChar)((u32 & CONVERTC_UTF8_BIT_MASK) | CONVERTC_UTF8_BIT_SINGLE);
                u32 >>= CONVERTC_UTF8_BIT_OFF;
            }
            psz8[0] = (UChar)(u32 | UTF8_PREFIX[nLen - 1]);
            return nLen;
        }
    }
    return 0;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf8ToUtf32(const PUChar psz8, UInt& u32)
{
    assert((psz8 != nullptr) && (*psz8 != 0));
    if ((psz8 == nullptr) || (*psz8 == 0))
    {
        return 0;
    }
    UChar uc = psz8[0];
    if (uc < CONVERTC_UTF8_BIT_SINGLE) // ascii
    {
        u32 = uc;
        return CONVERTC_UTF8_BIT_LEAD;
    }
    if ((uc < CONVERTC_UTF8_LEAD_MIN) || (uc > CONVERTC_UTF8_LEAD_MAX))
    {
        u32 = 0;
        return 0;
    }
    Int nLen = CONVERTC_UTF8_BIT_LEAD;
    if (uc >= CONVERTC_UTF8_LEAD_MASK)
    {
        nLen = UTF8_LEN[uc - CONVERTC_UTF8_LEAD_MASK];
    }
    u32 = uc & UTF8_MASKS[nLen];
    nLen += CONVERTC_UTF8_BIT_LEAD;

    for (Int i = CONVERTC_UTF8_BIT_LEAD; i < nLen; ++i)
    {
        uc = psz8[i];
        if ((uc < CONVERTC_UTF8_BIT_SINGLE) || (uc > CONVERTC_UTF8_BIT_MAX))
        {   // data invalid
            return 0;
        }
        u32 = (u32 << CONVERTC_UTF8_BIT_OFF) + (uc & CONVERTC_UTF8_BIT_MASK);
    }
    return nLen;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf32ToUtf16(UInt u32, PUShort psz16, Int nSize16)
{
    assert((psz16 != nullptr) && (nSize16 > 0));
    if ((u32 == 0) || (psz16 == nullptr) || (nSize16 <= 0))
    {
        return 0;
    }
    Int nLen = CONVERTC_UTF16_BIT_LEAD;
    if (u32 <= CONVERTC_UTF16_BIT_SINGLE)
    {
        *psz16 = (UShort)u32;
        return nLen;
    }
    else if (u32 <= CONVERTC_UTF16_BIT_MAX)
    {
        ++nLen;
        if (nSize16 >= nLen)
        {
            psz16[0] = (UShort)(CONVERTC_UTF16_LEAD_MIN + (u32 >> CONVERTC_UTF16_BIT_OFF) - CONVERTC_UTF16_BIT_DATA);
            psz16[1] = (UShort)(CONVERTC_UTF16_LEAD_MAX + (u32 & CONVERTC_UTF16_BIT_MASK));
            return nLen;
        }
    }
    return 0;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf16ToUtf32(const PUShort psz16, UInt& u32)
{
    assert((psz16 != nullptr) && (*psz16 != 0));
    if ((psz16 == nullptr) || (*psz16 == 0))
    {
        return 0;
    }
    UShort us = psz16[0];
    if ((us >= CONVERTC_UTF16_LEAD_MIN) && (us <= CONVERTC_UTF16_LEAD_MASK))
    {
        if (us < CONVERTC_UTF16_LEAD_MAX)
        {
            Int nLen = CONVERTC_UTF16_BIT_LEAD;
            UShort usData = psz16[1];
            if ((usData >= CONVERTC_UTF16_LEAD_MAX) &&
                (usData <= CONVERTC_UTF16_LEAD_MASK))
            {
                u32  = (usData & CONVERTC_UTF16_BIT_MASK);
                u32 += (((us & CONVERTC_UTF16_BIT_MASK) + CONVERTC_UTF16_BIT_DATA) << CONVERTC_UTF16_BIT_OFF);
                ++nLen;
                return nLen;
            }
        }
        return 0; // invalid
    }
    else
    {
        u32 = us;
        return CONVERTC_UTF16_BIT_LEAD;
    }
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf32ToUtf8(const PUInt psz32, Int nSize32, PUChar psz8, Int nSize8)
{
    assert((psz32 != nullptr) && (*psz32 != 0));
    assert((psz8 != nullptr) && (nSize8 > 0));
    if ((psz32 == nullptr) || (*psz32 == 0) || (nSize32 == 0))
    {
        return 0;
    }
    if ((psz8 == nullptr) || (nSize8 <= 0))
    {
        return 0;
    }

    Int nRet   = 0;
    Int nCount = 0;

    UInt uLen = (UInt)nSize32;
    for (UInt i = 0; (psz32[i] != 0) && (i < uLen); ++i)
    {
        nRet = Utf32ToUtf8(psz32[i], psz8 + nCount, nSize8);
        if (nRet == 0)
        {
            break;
        }
        nCount += nRet;
        nSize8 -= nRet;
    }
    psz8[nCount] = 0;
    return nCount;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf8ToUtf32(const PUChar psz8, Int nSize8, PUInt psz32, Int nSize32)
{
    assert((psz8 != nullptr) && (*psz8 != 0));
    assert((psz32 != nullptr) && (nSize32 > 0));
    if ((psz8 == nullptr) || (*psz8 == 0) || (nSize8 == 0))
    {
        return 0;
    }
    if ((psz32 == nullptr) || (nSize32 <= 0))
    {
        return 0;
    }

    Int  nRet   = 0;
    Int  nCount = 0;
    UInt u32    = 0;

    UInt uLen = (UInt)nSize8;
    for (UInt i = 0; (psz8[i] != 0) && (i < uLen); i += nRet)
    {
        nRet = Utf8ToUtf32(psz8 + i, u32);
        if (nRet == 0)
        {
            break;
        }
        psz32[nCount] = u32;
        ++nCount;
        if (nCount >= nSize32)
        {
            break;
        }
    }
    psz32[nCount] = 0;
    return nCount;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf32ToUtf16(const PUInt psz32, Int nSize32, PUShort psz16, Int nSize16)
{
    assert((psz32 != nullptr) && (*psz32 != 0));
    assert((psz16 != nullptr) && (nSize16 > 0));
    if ((psz32 == nullptr) || (*psz32 == 0) || (nSize32 == 0))
    {
        return 0;
    }
    if ((psz16 == nullptr) || (nSize16 <= 0))
    {
        return 0;
    }

    Int nRet   = 0;
    Int nCount = 0;

    UInt uLen = (UInt)nSize32;
    for (UInt i = 0; (psz32[i] != 0) && (i < uLen); ++i)
    {
        nRet = Utf32ToUtf16(psz32[i], psz16 + nCount, nSize16);
        if (nRet == 0)
        {
            break;
        }
        nCount  += nRet;
        nSize16 -= nRet;
    }
    psz16[nCount] = 0;
    return nCount;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Utf16ToUtf32(const PUShort psz16, Int nSize16, PUInt psz32, Int nSize32)
{
    assert((psz16 != nullptr) && (*psz16 != 0));
    assert((psz32 != nullptr) && (nSize32 > 0));
    if ((psz16 == nullptr) || (*psz16 == 0) || (nSize16 == 0))
    {
        return 0;
    }
    if ((psz32 == nullptr) || (nSize32 <= 0))
    {
        return 0;
    }

    Int  nRet   = 0;
    Int  nCount = 0;
    UInt u32    = 0;

    UInt uLen = (UInt)nSize16;
    for (UInt i = 0; (psz16[i] != 0) && (i < uLen); i += nRet)
    {
        nRet = Utf16ToUtf32(psz16 + i, u32);
        if (nRet == 0)
        {
            break;
        }
        psz32[nCount] = u32;
        ++nCount;
        if (nCount >= nSize32)
        {
            break;
        }
    }
    psz32[nCount] = 0;
    return nCount;
}

///////////////////////////////////////////////////////////////////
// CTSize
template <typename T>
INLINE CTSize<T>::CTSize(T InX, T InY)
: m_tX(InX)
, m_tY(InY)
{
}

template <typename T>
INLINE CTSize<T>::~CTSize(void)
{
}

template <typename T>
INLINE CTSize<T>::CTSize(const CTSize<T>& aSrc)
: m_tX(aSrc.m_tX)
, m_tY(aSrc.m_tY)
{
}

template <typename T>
INLINE CTSize<T>& CTSize<T>::operator=(const CTSize<T>& aSrc)
{
    if (this != &aSrc)
    {
        m_tX = aSrc.m_tX;
        m_tY = aSrc.m_tY;
    }
    return (*this);
}

template <typename T>
INLINE CTSize<T>& CTSize<T>::operator+=(const CTSize<T>& aSrc)
{
    m_tX += aSrc.m_tX;
    m_tY += aSrc.m_tY;
    return (*this);
}

template <typename T>
INLINE CTSize<T>& CTSize<T>::operator-=(const CTSize<T>& aSrc)
{
    m_tX -= aSrc.m_tX;
    m_tY -= aSrc.m_tY;
    return (*this);
}

template <typename T>
INLINE CTSize<T> CTSize<T>::operator+(const CTSize<T>& aSrc)
{
    return CTSize<T>(m_tX + aSrc.m_tX, m_tY + aSrc.m_tY);
}

template <typename T>
INLINE CTSize<T> CTSize<T>::operator-(const CTSize<T>& aSrc)
{
    return CTSize<T>(m_tX - aSrc.m_tX, m_tY - aSrc.m_tY);
}

template <typename T>
INLINE CTSize<T> CTSize<T>::operator-(void)
{
    return CTSize<T>(-m_tX, -m_tY);
}

template <typename T>
INLINE bool CTSize<T>::operator==(const CTSize<T>& aSrc) const
{
    return ((m_tX == aSrc.m_tX) && (m_tY == aSrc.m_tY));
}

template <typename T>
INLINE bool CTSize<T>::operator!=(const CTSize<T>& aSrc) const
{
    return ((m_tX != aSrc.m_tX) || (m_tY != aSrc.m_tY));
}

template <typename T>
INLINE T CTSize<T>::X(void) const
{
    return m_tX;
}

template <typename T>
INLINE void CTSize<T>::X(T InX)
{
    m_tX = InX;
}

template <typename T>
INLINE T CTSize<T>::Y(void) const
{
    return m_tY;
}

template <typename T>
INLINE void CTSize<T>::Y(T InY)
{
    m_tY = InY;
}

template <typename T>
INLINE void CTSize<T>::SetValue(T InX, T InY)
{
    m_tX = InX;
    m_tY = InY;
}

template <typename T>
INLINE void CTSize<T>::AddValue(T InX, T InY)
{
    m_tX += InX;
    m_tY += InY;
}

template <typename T>
INLINE void CTSize<T>::SubValue(T InX, T InY)
{
    m_tX -= InX;
    m_tY -= InY;
}

///////////////////////////////////////////////////////////////////
// CTRect
template <typename T>
INLINE CTRect<T>::CTRect(T InLeft, T InTop, T InRight, T InBottom)
: m_tLeft(InLeft)
, m_tTop(InTop)
, m_tRight(InRight)
, m_tBottom(InBottom)
{
}

template <typename T>
INLINE CTRect<T>::~CTRect(void)
{
}

template <typename T>
INLINE CTRect<T>::CTRect(const CTRect<T>& aSrc)
: m_tLeft(aSrc.m_tLeft)
, m_tTop(aSrc.m_tTop)
, m_tRight(aSrc.m_tRight)
, m_tBottom(aSrc.m_tBottom)
{
}

template <typename T>
INLINE CTRect<T>& CTRect<T>::operator=(const CTRect<T>& aSrc)
{
    if (this != &aSrc)
    {
        m_tLeft   = aSrc.m_tLeft;
        m_tTop    = aSrc.m_tTop;
        m_tRight  = aSrc.m_tRight;
        m_tBottom = aSrc.m_tBottom;
    }
    return (*this);
}

template <typename T>
INLINE CTRect<T>& CTRect<T>::operator+=(const CTRect<T>& aSrc)
{
    m_tLeft   += aSrc.m_tLeft;
    m_tTop    += aSrc.m_tTop;
    m_tRight  += aSrc.m_tRight;
    m_tBottom += aSrc.m_tBottom;
    return (*this);
}

template <typename T>
INLINE CTRect<T>& CTRect<T>::operator-=(const CTRect<T>& aSrc)
{
    m_tLeft   -= aSrc.m_tLeft;
    m_tTop    -= aSrc.m_tTop;
    m_tRight  -= aSrc.m_tRight;
    m_tBottom -= aSrc.m_tBottom;
    return (*this);
}

template <typename T>
INLINE CTRect<T>& CTRect<T>::operator+=(const CTSize<T>& tSize)
{
    m_tLeft   += tSize.m_tX;
    m_tTop    += tSize.m_tY;
    m_tRight  += tSize.m_tX;
    m_tBottom += tSize.m_tY;
    return (*this);
}

template <typename T>
INLINE CTRect<T>& CTRect<T>::operator-=(const CTSize<T>& tSize)
{
    m_tLeft   -= tSize.m_tX;
    m_tTop    -= tSize.m_tY;
    m_tRight  -= tSize.m_tX;
    m_tBottom -= tSize.m_tY;
    return (*this);
}

template <typename T>
INLINE CTRect<T> CTRect<T>::operator+(const CTRect<T>& aSrc)
{
    return CTRect<T>(m_tLeft   + aSrc.m_tLeft,
                     m_tTop    + aSrc.m_tTop,
                     m_tRight  + aSrc.m_tRight,
                     m_tBottom + aSrc.m_tBottom);
}

template <typename T>
INLINE CTRect<T> CTRect<T>::operator-(const CTRect<T>& aSrc)
{
    return CTRect<T>(m_tLeft   - aSrc.m_tLeft,
                     m_tTop    - aSrc.m_tTop,
                     m_tRight  - aSrc.m_tRight,
                     m_tBottom - aSrc.m_tBottom);
}

template <typename T>
INLINE CTRect<T> CTRect<T>::operator+(const CTSize<T>& tSize)
{
    return CTRect<T>(m_tLeft   + tSize.m_tX,
                     m_tTop    + tSize.m_tY,
                     m_tRight  + tSize.m_tX,
                     m_tBottom + tSize.m_tY);
}

template <typename T>
INLINE CTRect<T> CTRect<T>::operator-(const CTSize<T>& tSize)
{
    return CTRect<T>(m_tLeft   - tSize.m_tX,
                     m_tTop    - tSize.m_tY,
                     m_tRight  - tSize.m_tX,
                     m_tBottom - tSize.m_tY);
}

template <typename T>
INLINE CTRect<T> CTRect<T>::operator-(void)
{
    return CTRect<T>(-m_tLeft, -m_tTop, -m_tRight, -m_tBottom);
}

template <typename T>
INLINE bool CTRect<T>::operator==(const CTRect<T>& aSrc) const
{
    if ((m_tLeft == aSrc.m_tLeft) && (m_tTop == aSrc.m_tTop))
    {
        return ((m_tRight == aSrc.m_tRight) && (m_tBottom == aSrc.m_tBottom));
    }
    return false;
}

template <typename T>
INLINE bool CTRect<T>::operator!=(const CTRect<T>& aSrc) const
{
    if ((m_tLeft != aSrc.m_tLeft) || (m_tTop != aSrc.m_tTop))
    {
        return true;
    }
    return ((m_tRight != aSrc.m_tRight) || (m_tBottom != aSrc.m_tBottom));
}

template <typename T>
INLINE T CTRect<T>::Width(void) const
{
    return (m_tRight - m_tLeft);
}

template <typename T>
INLINE T CTRect<T>::Height(void) const
{
    return (m_tBottom - m_tTop);
}

template <typename T>
INLINE CTSize<T> CTRect<T>::Size(void) const
{
    return CTSize<T>(m_tRight - m_tLeft, m_tBottom - m_tTop);
}

template <typename T>
INLINE CTSize<T> CTRect<T>::LeftTop(void) const
{
    return CTSize<T>(m_tLeft, m_tTop);
}

template <typename T>
INLINE CTSize<T> CTRect<T>::RightBottom(void) const
{
    return CTSize<T>(m_tRight, m_tBottom);
}

template <typename T>
INLINE CTSize<T> CTRect<T>::Center(void) const
{
    return CTSize<T>((m_tLeft + m_tRight) / 2, (m_tTop + m_tBottom) / 2);
}

template <typename T>
INLINE bool CTRect<T>::IsEmpty(void) const
{
    return ((m_tLeft == m_tRight) || (m_tTop == m_tBottom));
}

template <typename T>
INLINE bool CTRect<T>::IsNull(void) const
{
    if ((m_tLeft == 0) && (m_tTop == 0))
    {
        return ((m_tRight == 0) && (m_tBottom == 0));
    }
    return false;
}

template <typename T>
INLINE bool CTRect<T>::IsEqual(const CTRect<T>& tRect) const
{
    if ((m_tLeft == tRect.m_tLeft) && (m_tTop == tRect.m_tTop))
    {
        return ((m_tRight == tRect.m_tRight) && (m_tBottom == tRect.m_tBottom));
    }
    return false;
}

template <typename T>
INLINE bool CTRect<T>::InRect(const CTSize<T>& tSize) const
{
    if ((tSize.m_tX >= m_tLeft) && (tSize.m_tX <= m_tRight))
    {
        return ((tSize.m_tY >= m_tTop) && (tSize.m_tY <= m_tBottom));
    }
    return false;
}

template <typename T>
INLINE T CTRect<T>::Left(void) const
{
    return m_tLeft;
}

template <typename T>
INLINE void CTRect<T>::Left(T InLeft)
{
    m_tLeft = InLeft;
}

template <typename T>
INLINE T CTRect<T>::Top(void) const
{
    return m_tTop;
}

template <typename T>
INLINE void CTRect<T>::Top(T InTop)
{
    m_tTop = InTop;
}

template <typename T>
INLINE T CTRect<T>::Right(void) const
{
    return m_tRight;
}

template <typename T>
INLINE void CTRect<T>::Right(T InRight)
{
    m_tRight = InRight;
}

template <typename T>
INLINE T CTRect<T>::Bottom(void) const
{
    return m_tBottom;
}

template <typename T>
INLINE void CTRect<T>::Bottom(T InBottom)
{
    m_tBottom = InBottom;
}

template <typename T>
INLINE void CTRect<T>::SetValue(T InLeft, T InTop, T InRight, T InBottom)
{
    m_tLeft   = InLeft;
    m_tTop    = InTop;
    m_tRight  = InRight;
    m_tBottom = InBottom;
}

template <typename T>
INLINE void CTRect<T>::AddValue(T InLeft, T InTop, T InRight, T InBottom)
{
    m_tLeft   += InLeft;
    m_tTop    += InTop;
    m_tRight  += InRight;
    m_tBottom += InBottom;
}

template <typename T>
INLINE void CTRect<T>::SubValue(T InLeft, T InTop, T InRight, T InBottom)
{
    m_tLeft   -= InLeft;
    m_tTop    -= InTop;
    m_tRight  -= InRight;
    m_tBottom -= InBottom;
}

template <typename T>
INLINE void CTRect<T>::InflateRect(T x, T y)
{
    m_tLeft   -= x;
    m_tTop    -= y;
    m_tRight  += x;
    m_tBottom += y;
}

template <typename T>
INLINE void CTRect<T>::OffseteRect(T x, T y)
{
    m_tLeft   += x;
    m_tTop    += y;
    m_tRight  += x;
    m_tBottom += y;
}

template <typename T>
INLINE void CTRect<T>::Normalize(void)
{
    T tSwap;
    if (m_tLeft > m_tRight)
    {
        tSwap    = m_tLeft;
        m_tLeft  = m_tRight;
        m_tRight = tSwap;
    }
    if (m_tTop > m_tBottom)
    {
        tSwap     = m_tTop;
        m_tTop    = m_tBottom;
        m_tBottom = tSwap;
    }
}

#endif // __TRAITS_INL__
