// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __CLANG_CONFIG_H__
#define __CLANG_CONFIG_H__

#pragma once

#include "config.h"

///////////////////////////////////////////////////////////////////
#if defined(__clang__)
///////////////////////////////////////////////////////////////////
// __ARCH_TARGET__ && __ARCH_TARGET_BIGENDIAN__
#if defined(__i386__)
    #define __ARCH_TARGET__           ARCH_TARGET_32
    #define __ARCH_TARGET_STR__       "ARCH_TARGET_32"
#elif defined(__x86_64__) || defined(__amd64__)
    #define __ARCH_TARGET__           ARCH_TARGET_64
    #define __ARCH_TARGET_STR__       "ARCH_TARGET_64"
#else
    #error "__ARCH_TARGET__ No Implement"
#endif

///////////////////////////////////////////////////////////////////
// __COMPILER_TYPE__
#define __COMPILER_TYPE__             COMPILER_TYPE_CLANG

///////////////////////////////////////////////////////////////////
// __PLATFORM_TARGET__
#if defined(__linux) || defined(__linux__)
    #define __PLATFORM_TARGET__       PLATFORM_TARGET_LINUX
    #define __PLATFORM_TARGET_STR__   "PLATFORM_TARGET_LINUX"
#else
    #error "__PLATFORM_TARGET__ No Implement"
#endif
///////////////////////////////////////////////////////////////////
// __RUNTIME_CONFIG__
#if defined(_UNICODE) || defined(UNICODE)
    #define __RUNTIME_CHARSET_WCHAR__
    #if (__SIZEOF_WCHAR_T__ == 4)
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_UTF32
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_UTF32"
    #else
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_UTF16
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_UTF16"
    #endif
#else
    #define __RUNTIME_CHARSET__       RUNTIME_CONFIG_UTF8
    #define __RUNTIME_CHARSET_STR__   "RUNTIME_CONFIG_UTF8"
#endif

#if defined(_DEBUG) || defined(DEBUG)
    #define __RUNTIME_DEBUG__
#endif

#if defined(_LIB) || defined(LIB_RUNTIME)
    #define __RUNTIME_STATIC__
#endif

#define __CREEK_BASE__

///////////////////////////////////////////////////////////////////
// linux Header Files
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/eventfd.h>
#include <sys/sendfile.h>
#include <errno.h>
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>
#include <aio.h>
#include <fcntl.h>
#include <dirent.h>
#include <fnmatch.h>
#include <dlfcn.h>
#include <pthread.h>
#include <semaphore.h>
#include <langinfo.h>
#include <pwd.h>
#include <cstddef>
#if __cplusplus > 199711L
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <climits>
#include <cctype>
#include <cwctype>
#include <cwchar>
#include <cstring>
#include <cassert>
#include <ctime>
#include <clocale>
#include <exception>
#include <utility>

///////////////////////////////////////////////////////////////////
// placement new operator from <new>
#include <new>
#define GNEW(adr)                     ::new( (adr) )

///////////////////////////////////////////////////////////////////
// disable warning
#pragma clang diagnostic ignored "-Wunused"
#pragma clang diagnostic ignored "-Wunused-value"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wignored-qualifiers"
#pragma clang diagnostic ignored "-Wformat-security"

///////////////////////////////////////////////////////////////////
//
#ifdef __cplusplus
    #define LINE_EXTERN_C             extern "C"
    #define BEGIN_EXTERN_C            extern "C" {
    #define END_EXTERN_C              }
#else
    #define LINE_EXTERN_C
    #define BEGIN_EXTERN_C
    #define END_EXTERN_C
#endif

#define PURE                          = 0

#define CXX_EXPORT                    __attribute__((visibility("default")))
#define CXX_IMPORT
#define C_EXPORT                      LINE_EXTERN_C CXX_EXPORT
#define C_IMPORT                      LINE_EXTERN_C

#define ASM_BEGIN                     __asm__ __volatile__(
#define ASM_END                       );
#define INLINE                        inline
#define FORCEINLINE                   inline __attribute__((always_inline))
#define FORCENOINLINE                 __attribute__((noinline))
#define NOVTABLE
#define SELECTANY                     __attribute__((weak))
#define NAKED                         __attribute__((naked))

#if __cplusplus > 199711L
    #if (__clang_major__ > 3) || ((__clang_major__ == 3) && (__clang_minor__ >= 3)) // 3.3
        #define THREADLS                  thread_local
    #else
        #define THREADLS                  __thread
    #endif

    #if (__clang_major__ > 3) || ((__clang_major__ == 3) && (__clang_minor__ >= 3)) // 3.3
        #define ABSTRACT
        #define FINAL                     final
        #define OVERRIDE                  override
    #else
        #define ABSTRACT
        #define FINAL
        #define OVERRIDE
    #endif

    #if (__clang_major__ < 3) || ((__clang_major__ == 3) && (__clang_minor__ < 3)) // 3.3
        #define nullptr                   NULL
        #define __MODERN_CXX_NOT_SUPPORTED
    #else
        #define __MODERN_CXX_LANG __cplusplus
    #endif
#else
    #define THREADLS __thread
    
    #define ABSTRACT
    #define FINAL
    #define OVERRIDE

    #define nullptr NULL
    
    #define __MODERN_CXX_NOT_SUPPORTED
#endif

#else
    #error "config defines file for linux Clang compiler only"
#endif  // __clang__

///////////////////////////////////////////////////////////////////
// common header files
#include "def.h"
#include "platform.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#
//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // __CLANG_CONFIG_H__
