// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_FILE_PATH_INL__
#define __TARGET_FILE_PATH_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CFileAttr
INLINE CFileAttr::CFileAttr(void)
{
    MM_SAFE::Set(&m_Attr, sizeof(FILEATTR), 0);
}

INLINE CFileAttr::CFileAttr(const FILEATTR& Attr)
{
    MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &Attr, sizeof(FILEATTR));
}

INLINE CFileAttr::~CFileAttr(void)
{
}

INLINE CFileAttr::CFileAttr(const CFileAttr& aSrc)
{
    MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &(aSrc.m_Attr), sizeof(FILEATTR));
}

INLINE CFileAttr& CFileAttr::operator=(const CFileAttr& aSrc)
{
    if (this != &aSrc)
    {
        MM_SAFE::Cpy(&m_Attr, sizeof(FILEATTR), &(aSrc.m_Attr), sizeof(FILEATTR));
    }
    return (*this);
}

INLINE FILEATTR& CFileAttr::Attr(void)
{
    return m_Attr;
}

INLINE const FILEATTR& CFileAttr::Attr(void) const
{
    return m_Attr;
}

INLINE ULLong CFileAttr::Size(void) const
{
    return (ULLong)m_Attr.st_size;
}

INLINE time_t CFileAttr::CTime(void) const
{
    return m_Attr.st_ctime;
}

INLINE time_t CFileAttr::ATime(void) const
{
    return m_Attr.st_atime;
}

INLINE time_t CFileAttr::MTime(void) const
{
    return m_Attr.st_mtime;
}

INLINE bool CFileAttr::Size(ULLong ullSize) const
{
    return (m_Attr.st_size == (off_t)ullSize);
}

INLINE bool CFileAttr::CTime(time_t tc) const
{
    return (m_Attr.st_ctime == tc);
}

INLINE bool CFileAttr::ATime(time_t ta) const
{
    return (m_Attr.st_atime == ta);
}

INLINE bool CFileAttr::MTime(time_t tm) const
{
    return (m_Attr.st_mtime == tm);
}

///////////////////////////////////////////////////////////////////
// CFileFind
INLINE void CFileFind::SetAttr(const FILEFIND& Find)
{
    attr.m_Attr = Find;
}

INLINE CFileAttr& CFileFind::GetAttr(void)
{
    return attr;
}

INLINE const CFileAttr& CFileFind::GetAttr(void) const
{
    return attr;
}

INLINE FILEATTR& CFileFind::Attr(void)
{
    return attr.Attr();
}

INLINE const FILEATTR& CFileFind::Attr(void) const
{
    return attr.Attr();
}

INLINE ULLong CFileFind::Size(void) const
{
    return attr.Size();
}

INLINE time_t CFileFind::CTime(void) const
{
    return attr.CTime();
}

INLINE time_t CFileFind::ATime(void) const
{
    return attr.ATime();
}

INLINE time_t CFileFind::MTime(void) const
{
    return attr.MTime();
}

INLINE bool CFileFind::Size(ULLong ullSize) const
{
    return attr.Size(ullSize);
}

INLINE bool CFileFind::CTime(time_t tc) const
{
    return attr.CTime(tc);
}

INLINE bool CFileFind::ATime(time_t ta) const
{
    return attr.ATime(ta);
}

INLINE bool CFileFind::MTime(time_t tm) const
{
    return attr.MTime(tm);
}

///////////////////////////////////////////////////////////////////
// CFilePath
SELECTANY CPCXStr CFilePath::ModulePath    = TF("ModulePath");
SELECTANY CPCXStr CFilePath::CurFolder     = TF(".");
SELECTANY CPCXStr CFilePath::UpperFolder   = TF("..");
SELECTANY XChar   CFilePath::DotChar       = TF('.');
SELECTANY XChar   CFilePath::SlashChar     = TF('/');
SELECTANY XChar   CFilePath::BackSlashChar = TF('\\');
SELECTANY XChar   CFilePath::PathSepChar   = TF('/');

INLINE CFilePath::CFilePath(bool bModulePath)
: m_FilePath(CContainerTraits::SIZED_GROW, CContainerTraits::HASHD_SIZE)
{
    if (bModulePath)
    {
        SetModulePath();
    }
}

INLINE CFilePath::~CFilePath(void)
{
    m_FilePath.RemoveAll();
}

INLINE CFilePath::CFilePath(const CFilePath&)
{
}

INLINE CFilePath& CFilePath::operator=(const CFilePath&)
{
    return (*this);
}

INLINE bool CFilePath::IsExist(PCXStr pszPath, PCXStr pszKey)
{
    Int nRet = RET_ERROR;
    if (pszKey == nullptr)
    {
        nRet = _taccess(pszPath, F_OK);
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is exist file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        nRet = _taccess(*strPath, F_OK);
    }
    return (nRet == RET_OKAY);
}

INLINE bool CFilePath::IsFile(PCXStr pszPath, PCXStr pszKey)
{
    struct stat sttAttr;
    Int nRet = RET_ERROR;
    if (pszKey == nullptr)
    {
        nRet = _taccess(pszPath, F_OK);
        if (nRet == RET_OKAY)
        {
            nRet = _tstat(pszPath, &sttAttr);
        }
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is file file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        nRet = _taccess(*strPath, F_OK);
        if (nRet == RET_OKAY)
        {
            nRet = _tstat(*strPath, &sttAttr);
        }
    }
    if (nRet == RET_OKAY)
    {
        return (S_ISREG(sttAttr.st_mode) ? true : false);
    }
    return false;
}

INLINE bool CFilePath::IsFolder(PCXStr pszPath, PCXStr pszKey)
{
    struct stat sttAttr;
    Int nRet = RET_ERROR;
    if (pszKey == nullptr)
    {
        nRet = _taccess(pszPath, F_OK);
        if (nRet == RET_OKAY)
        {
            nRet = _tstat(pszPath, &sttAttr);
        }
    }
    else
    {
        CString strPath;
        if (GetPath(pszKey, strPath) == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] is folder file path %s[key %s] failed"), this, pszPath, pszKey);
            return false;
        }
        strPath += pszPath;
        nRet = _taccess(*strPath, F_OK);
        if (nRet == RET_OKAY)
        {
            nRet = _tstat(*strPath, &sttAttr);
        }
    }
    if (nRet == RET_OKAY)
    {
        return (S_ISDIR(sttAttr.st_mode) ? true : false);
    }
    return false;
}

INLINE bool CFilePath::FileAttr(CFileAttr& FileAttr, PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] file attr get file path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    if (_tstat(*strPath, &(FileAttr.m_Attr)) == RET_ERROR) // file or dir
    {
        DEV_DEBUG(TF("  File-Path[%p] file attr get file path %s return error code %d"), this, *strPath, errno);
        return false;
    }
    return true;
}

INLINE bool CFilePath::Copy(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting, PCXStr pszDstKey, PCXStr pszSrcKey)
{
    CString strDst;
    if ((pszDstKey != nullptr) && (GetPath(pszDstKey, strDst) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] copy get dst file path %s[key %s] failed"), this, pszDst, pszDstKey);
        return false;
    }
    strDst += pszDst;
    if (CheckFilePath(strDst, bReplaceExisting))
    {
        CString strSrc;
        if ((pszSrcKey != nullptr) && (GetPath(pszSrcKey, strSrc) == false))
        {
            DEV_DEBUG(TF("  File-Path[%p] copy get src file path %s[key %s] failed"), this, pszSrc, pszSrcKey);
            return false;
        }
        strSrc += pszSrc;
        DEV_DEBUG(TF("  File-Path[%p] copy %s[key %s] to %s[key %s]"), this, *strSrc, pszSrcKey, *strDst, pszDstKey);

        bool bRet = false;
        Handle hSrc = _topen(*strSrc, O_RDONLY);
        if (hSrc != HANDLE_INVALID)
        {
            struct stat sttSrc;
            fstat(hSrc, &sttSrc);

            Handle hDst = _topen(*strDst, O_RDWR|O_CREAT|O_TRUNC, sttSrc.st_mode);
            if (hDst != HANDLE_INVALID)
            {
                bRet = (sendfile(hDst, hSrc, nullptr, sttSrc.st_size) ==  sttSrc.st_size);

                close(hDst);
            }
            close(hSrc);
        }
        return bRet;
    }
    return false;
}

INLINE bool CFilePath::Move(PCXStr pszDst, PCXStr pszSrc, bool bReplaceExisting, PCXStr pszDstKey, PCXStr pszSrcKey)
{
    CString strDst;
    if ((pszDstKey != nullptr) && (GetPath(pszDstKey, strDst) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] move get dst file path %s[key %s] failed"), this, pszDst, pszDstKey);
        return false;
    }
    strDst += pszDst;
    if (CheckFilePath(strDst, bReplaceExisting))
    {
        CString strSrc;
        if ((pszSrcKey != nullptr) && (GetPath(pszSrcKey, strSrc) == false))
        {
            DEV_DEBUG(TF("  File-Path[%p] move get src file path %s[key %s] failed"), this, pszSrc, pszSrcKey);
            return false;
        }
        strSrc += pszSrc;
        DEV_DEBUG(TF("  File-Path[%p] move %s[key %s] to %s[key %s]"), this, *strSrc, pszSrcKey, *strDst, pszDstKey);
        return (_trename(*strSrc, *strDst) == RET_OKAY);
    }
    return false;
}

INLINE bool CFilePath::Delete(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] delete get file path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    DEV_DEBUG(TF("  File-Path[%p] delete %s[key %s]"), this, *strPath, pszKey);
    if (_taccess(*strPath, F_OK) == RET_OKAY)
    {
        return (_tremove(*strPath) == RET_OKAY);
    }
    return false;
}

INLINE Int CFilePath::Find(CTList<CString>& strFiles, PCXStr pszPath, PCXStr pszKey, Int nFlag)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] find get file path %s[key %s] failed"), this, pszPath, pszKey);
        return RET_ERROR;
    }
    strPath += pszPath;
    struct stat sttAttr;
    if (_tstat(*strPath, &sttAttr) == RET_OKAY) // file or dir
    {
        DEV_DEBUG(TF("  File-Path[%p] find %s is full path as a file or folder"), this, *strPath);
    }
    else
    {
        Int nPos = strPath.Find(PathSepChar, 0, true);
        if (nPos >= 0)
        {
            ++nPos;
            CString strMatch = strPath.RightPos((size_t)nPos);
            strPath.ResetLength((size_t)nPos);
            if ((nFlag & FIND_FLAG_RELATIVE) == 0)
            {
                nPos = 0;
            }
            return FindFiles(strFiles, strPath, strMatch, nFlag, (size_t)nPos);
        }
        DEV_DEBUG(TF("  File-Path[%p] find %s CString::Find find / failed"), this, *strPath);
    }
    return RET_ERROR;
}

INLINE Int CFilePath::Find(CTList<CFileFind>& lFinds, PCXStr pszPath, PCXStr pszKey, Int nFlag, Int nLevel)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path 2[%p] find get file path %s[key %s] failed"), this, pszPath, pszKey);
        return RET_ERROR;
    }
    strPath += pszPath;
    struct stat sttAttr;
    if (_tstat(*strPath, &sttAttr) == RET_OKAY) // file or dir
    {
        DEV_DEBUG(TF("  File-Path 2[%p] find %s is full path as a file or folder"), this, *strPath);
    }
    else
    {
        Int nPos = strPath.Find(PathSepChar, 0, true);
        if (nPos >= 0)
        {
            ++nPos;
            CString strMatch = strPath.RightPos((size_t)nPos);
            strPath.ResetLength((size_t)nPos);
            if ((nFlag & FIND_FLAG_RELATIVE) == 0)
            {
                nPos = 0;
            }
            return FindFiles(lFinds, strPath, strMatch, nFlag, nLevel, (size_t)nPos);
        }
        DEV_DEBUG(TF("  File-Path 2[%p] find %s CString::Find find / failed"), this, *strPath);
    }
    return RET_ERROR;
}

INLINE bool CFilePath::CreateFolder(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] create folder get folder path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    if (strPath.Length() > 0)
    {
        if (strPath[strPath.Length() - 1] != PathSepChar)
        {
            strPath += PathSepChar;
        }
        return CreateFolders(strPath);
    }
    return false;
}

INLINE bool CFilePath::DeleteFolder(PCXStr pszPath, PCXStr pszKey)
{
    CString strPath;
    if ((pszKey != nullptr) && (GetPath(pszKey, strPath) == false))
    {
        DEV_DEBUG(TF("  File-Path[%p] delete folder get folder path %s[key %s] failed"), this, pszPath, pszKey);
        return false;
    }
    strPath += pszPath;
    DEV_DEBUG(TF("  File-Path[%p] delete folder path %s"), this, *strPath);
    if (strPath.Length() > 0)
    {
        if (strPath[strPath.Length() - 1] != PathSepChar)
        {
            strPath += PathSepChar;
        }
        return DeleteFolders(strPath);
    }
    return false;
}

INLINE bool CFilePath::SetPath(PCXStr pszKey, PCXStr pszPath)
{
    CSyncLockScope scope(m_FPLock);
    if ((pszKey != nullptr) && (pszPath != nullptr) && (*pszPath != 0))
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair == nullptr)
        {
            PINDEX index = m_FilePath.Add(pszKey);
            if (index != nullptr)
            {
                pPair = m_FilePath.GetAt(index);
                DEV_DEBUG(TF("  File-Path[%p] set path add [key %s]"), this, pszKey);
            }
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] key already has path %s"), this, pszPath, pszKey, pPair->m_V.GetBuffer());
        }
        if (pPair != nullptr)
        {
            pPair->m_V = pszPath;
            if (pPair->m_V[pPair->m_V.Length() - 1] != PathSepChar)
            {
                pPair->m_V += PathSepChar;
            }
            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] okay"), this, pPair->m_V.GetBuffer(), pszKey);
            return true;
        }
    }
    else if (pszKey != nullptr)
    {
        m_FilePath.Remove(pszKey);
        DEV_DEBUG(TF("  File-Path[%p] set path [key %s] remove okay"), this, pszKey);
        return true;
    }
    return false;
}

INLINE bool CFilePath::GetFullPath(PCXStr pszKey, CString& strPath)
{
    CSyncLockScope scope(m_FPLock);
    if (pszKey != nullptr)
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair != nullptr)
        {
            if (strPath.IsEmpty() == false)
            {
                CString strFullPath = pPair->m_V;
                strFullPath += strPath;
#ifndef __MODERN_CXX_NOT_SUPPORTED
                strPath = std::move(strFullPath);
#else
                strPath = strFullPath;
#endif
            }
            else
            {
                strPath = pPair->m_V;
            }
            return true;
        }
    }
    return false;
}

INLINE bool CFilePath::GetFullPath(PCXStr pszKey, PXStr pszPath, size_t stLen)
{
    CSyncLockScope scope(m_FPLock);
    if ((pszKey != nullptr) && (pszPath != nullptr) && (stLen > 0))
    {
        PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
        if (pPair != nullptr)
        {
            if (*pszPath != 0)
            {
                CString strFullPath = pPair->m_V;
                strFullPath += pszPath;
                CXChar::Copy(pszPath, stLen, strFullPath.GetBuffer());
            }
            else
            {
                CXChar::Copy(pszPath, stLen, pPair->m_V.GetBuffer());
            }
            return true;
        }
    }
    return false;
}

INLINE bool CFilePath::CheckFilePath(const CString& strPath, bool bReplaceExisting)
{
    if (_taccess(*strPath, F_OK) == RET_OKAY)
    {
        if (bReplaceExisting == false)
        {
            DEV_DEBUG(TF("  File-Path[%p] CheckFilePath %s file exist"), this, *strPath);
            return false;
        }
        return (_taccess(*strPath, W_OK) == RET_OKAY);
    }
    return true;
}

INLINE Int CFilePath::FindFiles(CTList<CString>& strFiles, CString& strPath, const CString& strMatch, Int nFlag, size_t stRelative)
{
    DIR *pDir = _topendir(*strPath);
    if (pDir == nullptr)
    {
        DEV_DEBUG(TF("  File-Path[%p] FindFiles open dir %s errno=%d"), this, *strPath, errno);
        return 0;
    }
    assert(strPath.Length() > 0);
    size_t stLen = strPath.Length();
    assert(strPath[stLen - 1] == PathSepChar);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    Int nCount = 0;
    struct stat sttAttr = { 0 };
    for (struct dirent* pDirEnt = readdir(pDir); pDirEnt != nullptr; pDirEnt = readdir(pDir))
    {
        if ((CXChar::Cmp(pDirEnt->d_name, CurFolder)   == 0) ||
            (CXChar::Cmp(pDirEnt->d_name, UpperFolder) == 0))
        {
            continue;
        }
        strPath += pDirEnt->d_name;
        PCXStr pszName = (nFlag & FIND_FLAG_FULLPATH) ? strPath.GetBuffer(stRelative) : strPath.GetBuffer(stLen);

        _tstat(*strPath, &sttAttr);
        if (S_ISREG(sttAttr.st_mode) && (nFlag & FIND_FLAG_FILE))
        {
            if (_tfnmatch(*strMatch, pDirEnt->d_name, 0) == RET_OKAY)
            {
                strFiles.AddTail(pszName);
                ++nCount;
            }
        }
        else if (S_ISDIR(sttAttr.st_mode))
        {
            if ((_tfnmatch(*strMatch, pDirEnt->d_name, 0) == RET_OKAY) && (nFlag & FIND_FLAG_FOLDER))
            {
                strFiles.AddTail(pszName);
                ++nCount;
            }
            if ((nFlag & FIND_FLAG_NOSUBFOLDER) == 0)
            {
                strPath += PathSepChar;
                nCount += FindFiles(strFiles, strPath, strMatch, nFlag, stRelative);
            }
        }
        strPath.ResetLength(stLen);
    }
    closedir(pDir);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    return nCount;
}

INLINE Int CFilePath::FindFiles(CTList<CFileFind>& lFinds, CString& strPath, const CString& strMatch, Int nFlag, Int nLevel, size_t stRelative)
{
    DIR *pDir = _topendir(*strPath);
    if (pDir == nullptr)
    {
        DEV_DEBUG(TF("  File-Path 2[%p] FindFiles open dir %s errno=%d"), this, *strPath, errno);
        return 0;
    }
    assert(strPath.Length() > 0);
    size_t stLen = strPath.Length();
    assert(strPath[stLen - 1] == PathSepChar);

    Int nThisLevel = (nLevel - 1);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    Int nCount = 0;
    struct stat sttAttr = { 0 };
    for (struct dirent* pDirEnt = readdir(pDir); pDirEnt != nullptr; pDirEnt = readdir(pDir))
    {
        if ((CXChar::Cmp(pDirEnt->d_name, CurFolder)   == 0) ||
            (CXChar::Cmp(pDirEnt->d_name, UpperFolder) == 0))
        {
            continue;
        }
        strPath += pDirEnt->d_name;
        PCXStr pszName = (nFlag & FIND_FLAG_FULLPATH) ? strPath.GetBuffer(stRelative) : strPath.GetBuffer(stLen);

        _tstat(*strPath, &sttAttr);
        if (S_ISREG(sttAttr.st_mode) && (nFlag & FIND_FLAG_FILE))
        {
            if (_tfnmatch(*strMatch, pDirEnt->d_name, 0) == RET_OKAY)
            {
                lFinds.AddTail();
                CFileFind& Find = lFinds.GetTail();
                Find.strFile = pszName;
                Find.SetAttr(sttAttr);
                ++nCount;
            }
        }
        else if (S_ISDIR(sttAttr.st_mode))
        {
            if ((_tfnmatch(*strMatch, pDirEnt->d_name, 0) == RET_OKAY) && (nFlag & FIND_FLAG_FOLDER))
            {
                lFinds.AddTail();
                CFileFind& Find = lFinds.GetTail();
                Find.strFile = pszName;
                Find.SetAttr(sttAttr);
                ++nCount;
            }
            if ((nFlag & FIND_FLAG_NOSUBFOLDER) == 0)
            {
                if (nThisLevel > 0)
                {
                    strPath += PathSepChar;
                    nCount += FindFiles(lFinds, strPath, strMatch, nFlag, nThisLevel, stRelative);
                }
            }
        }
        strPath.ResetLength(stLen);
    }
    closedir(pDir);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    return nCount;
}

INLINE bool CFilePath::CreateFolders(CString& strPath)
{
    bool bRet = true;
    PXStr pszPath = *strPath;

    if (_tmkdir(pszPath, FOLDER_ACS) == RET_ERROR)
    {
        Int nRet = errno;
        if (nRet == EEXIST)
        {
            DEV_DEBUG(TF("  File-Path[%p] create folder %s exists already"), this, pszPath);
            // return true since folder exists
        }
        else if (nRet == ENOENT)
        {
///////////////////////////////////////////////////////////////////////////////////////////////////////////
            PXStr p = pszPath + 1; // skip root /
            do
            {
                p = (PXStr)CXChar::Chr(p, PathSepChar);
                if (p != nullptr)
                {
                    *p = 0;
                    if ((_taccess(pszPath, F_OK) != 0) &&
                        (_tmkdir(pszPath, FOLDER_ACS) == RET_ERROR))
                    {
                        DEV_DEBUG(TF("  File-Path[%p] create folder %s failed return error code %d"), this, pszPath, errno);
                        bRet = false;
                        break;
                    }
                    *p = PathSepChar;
                    ++p;
                }
                else
                {
                    DEV_DEBUG(TF("  File-Path[%p] create folder %s failed"), this, pszPath);
                    bRet = false;
                    break;
                }
            } while ((p != nullptr) && (*p != 0));
///////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] create folder %s return error code %d"), this, pszPath, nRet);
            bRet = false;
        }
    }
    return bRet;
}

INLINE bool CFilePath::DeleteFolders(CString& strPath)
{
    bool   bRet = true;
    size_t stLen = strPath.Length();
    DEV_DEBUG(TF("  File-Path[%p] delete folder %s length = %d"), this, *strPath, stLen);

    if (_trmdir(*strPath) == RET_ERROR)
    {
        Int nRet = errno;
        if (nRet == ENOTEMPTY)
        {
            DIR *pDir = _topendir(*strPath);
            if (pDir != nullptr)
            {
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                struct stat sttAttr = { 0 };

                for (struct dirent* pDirEnt = readdir(pDir); pDirEnt != nullptr; pDirEnt = readdir(pDir))
                {
                    if ((CXChar::Cmp(pDirEnt->d_name, CurFolder)   == 0) ||
                        (CXChar::Cmp(pDirEnt->d_name, UpperFolder) == 0))
                    {
                        continue;
                    }
                    strPath += pDirEnt->d_name;
                    _tstat(*strPath, &sttAttr);
                    if (S_ISREG(sttAttr.st_mode))
                    {
                        DEV_DEBUG(TF("  File-Path[%p] delete folder delete file %s"), this, *strPath);
                        _tunlink(*strPath);
                    }
                    else if (S_ISDIR(sttAttr.st_mode))
                    {
                        strPath += PathSepChar;
                        DEV_DEBUG(TF("  File-Path[%p] delete folder delete sub-dir %s"), this, *strPath);
                        if (DeleteFolders(strPath) == false)
                        {
                            bRet = false;
                            break;
                        }
                    }
                    strPath.ResetLength(stLen);
                    DEV_DEBUG(TF("  File-Path[%p] delete folder reset %s"), this, *strPath);
                }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                nRet = closedir(pDir);
                DEV_DEBUG(TF("  File-Path[%p] delete folder closedir %s ret %d, errno = %d"), this, *strPath, nRet, errno);
                nRet = _trmdir(*strPath);
                DEV_DEBUG(TF("  File-Path[%p] delete folder delete %s ret %d, errno = %d"), this, *strPath, nRet, errno);
            }
            else
            {
                DEV_DEBUG(TF("  File-Path[%p] delete folder open dir %s errno=%d"), this, *strPath, errno);
                bRet = false;
            }
        }
        else
        {
            DEV_DEBUG(TF("  File-Path[%p] delete folder %s return error code %d"), this, *strPath, nRet);
            bRet = false;
        }
    }
    return bRet;
}

INLINE bool CFilePath::GetPath(PCXStr pszKey, CString& strPath)
{
    CSyncLockScope scope(m_FPLock);
    PAIR_FILEPATH* pPair = m_FilePath.Find(pszKey);
    if (pPair != nullptr)
    {
        strPath = pPair->m_V;
        return true;
    }
    return false;
}

INLINE void CFilePath::SetModulePath(void)
{
    PAIR_FILEPATH* pPair = m_FilePath.Find(ModulePath);
    if (pPair == nullptr)
    {
        PINDEX index = m_FilePath.Add(ModulePath);
        if (index != nullptr)
        {
            pPair = m_FilePath.GetAt(index);

            XChar szPath[LMT_MAX_PATH];
            CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);

            pPair->m_V = szPath;

            assert(pPair->m_V.Length() > 0);
            assert(pPair->m_V[pPair->m_V.Length() - 1] == PathSepChar);

            DEV_DEBUG(TF("  File-Path[%p] set path %s[key %s] okay"), this, pPair->m_V.GetBuffer(), ModulePath);
        } // index != nullptr
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_FILE_PATH_INL__
