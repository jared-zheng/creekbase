// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_TIME_SCOPE_INL__
#define __TARGET_TIME_SCOPE_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CTime
INLINE CTime::CTime(ULLong ullFileTime, Int)
: m_llTime(0)
{
    m_llTime = (LLong)ullFileTime;
}

INLINE struct tm* CTime::GetTm(struct tm& TmRef, bool bLocal) const
{
    struct tm* pRet = nullptr;
    struct tm  atm;
    if (bLocal)
    {
        pRet = localtime_r((time_t*)&m_llTime, &atm);
    }
    else
    {
        pRet = gmtime_r((time_t*)&m_llTime, &atm);
    }
    if (pRet == nullptr)
    {
        return nullptr;    // indicates that m_llTime was not initialized!
    }
    TmRef = atm;
    return (&TmRef);
}

INLINE CString CTime::Format(PCXStr pszFormat) const
{
    CString strFormat;
    if ((pszFormat != nullptr) && (*pszFormat != 0))
    {
        struct tm atm;
        if (GetTm(atm) != nullptr)
        {
            XChar szBuf[LMT_BUF];
            if (_txftime(szBuf, LMT_BUF, pszFormat, &atm) > 0)
            {
                strFormat = szBuf;
            }
        }
    }
    return strFormat;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_TIME_SCOPE_INL__
