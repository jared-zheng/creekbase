// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_MOBJECT_INL__
#define __TARGET_MOBJECT_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)
///////////////////////////////////////////////////////////////////
// MM_SAFE
template <typename T>
INLINE void MM_SAFE::DELETE_PTR(T* pV)
{
    if (pV != nullptr)
    {
        MDELETE pV;
    }
}

template <typename T>
INLINE void MM_SAFE::DELETE_PTRREF(T*& pV)
{
    if (pV != nullptr)
    {
        MDELETE pV;
        pV = nullptr;
    }
}

template <typename X, typename T>
INLINE void MM_SAFE::DELETE_PTR(X* pV)
{
    if (pV != nullptr)
    {
        MDELETE (static_cast<T*>(pV));
    }
}

template <typename X, typename T>
INLINE void MM_SAFE::DELETE_PTRREF(X*& pV)
{
    if (pV != nullptr)
    {
        MDELETE (static_cast<T*>(pV));
        pV = nullptr;
    }
}

INLINE Int MM_SAFE::Cmp(const void* pDst, const void* pSrc, size_t stSize)
{
    return memcmp(pDst, pSrc, stSize);
}

INLINE void* MM_SAFE::Set(void* pDst, Int nValue, size_t stSize)
{
    return memset(pDst, nValue, stSize);
}

INLINE void* MM_SAFE::Cpy(void* pDst, size_t stDst, const void* pSrc, size_t stSrc)
{
    assert(stDst >= stSrc);
    memcpy(pDst, pSrc, stSrc);
    return pDst;
}

INLINE void* MM_SAFE::Mov(void* pDst, size_t stDst, const void* pSrc, size_t stSrc)
{
    assert(stDst >= stSrc);
    memmove(pDst, pSrc, stSrc);
    return pDst;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_MOBJECT_INL__
