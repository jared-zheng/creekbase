// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_EXCEPTION_INL__
#define __TARGET_EXCEPTION_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CExceptionTracer
class CExceptionTracer : public MObject
{
public:
    static void Dump(PCXStr pszName, Int nSig = 0);
    static void Dump(CStream& Stream, Int nSig = 0);
public:
    CExceptionTracer(void);
    ~CExceptionTracer(void);
    CExceptionTracer(const CExceptionTracer&);
    CExceptionTracer& operator=(const CExceptionTracer&);
};

#define CATCH_EXCEPTION_DUMP( desc )   UNSET_EXCEPTION(); } else { CExceptionTracer::Dump(TF(#desc), SIGSEGV);

INLINE CExceptionTracer::CExceptionTracer(void)
{
}

INLINE CExceptionTracer::~CExceptionTracer(void)
{
}

INLINE CExceptionTracer::CExceptionTracer(const CExceptionTracer&)
{
}

INLINE CExceptionTracer& CExceptionTracer::operator=(const CExceptionTracer&)
{
    return (*this);
}

INLINE void CExceptionTracer::Dump(PCXStr pszName, Int nSig)
{
    XChar szPath[LMT_MAX_PATH] = { 0 };
    CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);

    CFileLog Dumplog(true, LOGL_ALL, szPath, pszName);
    if (nSig != 0)
    {
        LOGV_ERROR(Dumplog, TF("Exception SIG : %d"), nSig);
    }
    LOG_ERROR(Dumplog, TF("Exception Stack : -->"));

    void*  pSymbols[LMT_KEY];
    Int    nSize = backtrace(pSymbols, LMT_KEY);
    PXStr* pszSymbols = _tbacktrace_symbols(pSymbols, nSize);
    for (Int i = 0; i < nSize; ++i)
    {
        LOGV_ERROR(Dumplog, TF("Frame %02d : %s"), i, pszSymbols[i]);
    }
    free(pszSymbols);
}

INLINE void CExceptionTracer::Dump(CStream& Stream, Int nSig)
{
    CString strFormat;
    if (nSig != 0)
    {
        strFormat.Format(TF("\n#========Exception SIG : %d========\nException Stack : -->\n"), nSig);
        Stream <= strFormat;
    }
    else
    {
        Stream <= TF("\n#===================================\nException Stack : -->\n");
    }

    void*  pSymbols[LMT_KEY];
    Int    nSize = backtrace(pSymbols, LMT_KEY);
    PXStr* pszSymbols = _tbacktrace_symbols(pSymbols, nSize);
    for (Int i = 0; i < nSize; ++i)
    {
        strFormat.Format(TF("Frame %02d : %s\n"), i, pszSymbols[i]);
        Stream <= strFormat;
    }
    free(pszSymbols);
    Stream <= TF("===================================#\n");
    // stdout
    backtrace_symbols_fd(pSymbols, nSize, STDOUT_FILENO);
}

/////////////////////////////////////////////////////////////////
// CExceptionTerminater
class CExceptionTerminater : public MObject
{
public:
    static CPCXStr DumpFileName;
public:
    static bool Init(std::terminate_handler pHandler = nullptr);
    static void Exit(void);
public:
    static void TerminateHandler(void);
    static void SignalHandler(Int nSig);
private:
    CExceptionTerminater(void);
    ~CExceptionTerminater(void);
    CExceptionTerminater(const CExceptionTerminater&);
    CExceptionTerminater& operator=(const CExceptionTerminater&);
};

SELECTANY CPCXStr CExceptionTerminater::DumpFileName = TF("[%d-%d].%04d-%02d-%02d(%02d-%02d-%02d).log");


INLINE CExceptionTerminater::CExceptionTerminater(void)
{
}

INLINE CExceptionTerminater::~CExceptionTerminater(void)
{
}

INLINE CExceptionTerminater::CExceptionTerminater(const CExceptionTerminater&)
{
}

INLINE CExceptionTerminater& CExceptionTerminater::operator=(const CExceptionTerminater&)
{
    return (*this);
}

INLINE bool CExceptionTerminater::Init(std::terminate_handler pHandler)
{
    struct sigaction sAct;
    sigemptyset(&sAct.sa_mask);
    sAct.sa_handler = &CExceptionTerminater::SignalHandler;
    sAct.sa_flags   = SA_RESETHAND|SA_NODEFER;
    sigaction(SIGSEGV, &sAct, nullptr);
    // other sig handler

    if (pHandler == nullptr)
    {
        pHandler = &CExceptionTerminater::TerminateHandler;
    }
    std::set_terminate(pHandler);
    return true;
}

INLINE void CExceptionTerminater::Exit(void)
{
    std::set_terminate(nullptr);
}

INLINE void CExceptionTerminater::TerminateHandler(void)
{
    CPlatform::TIMEINFO ti;
    CPlatform::GetTimeInfo(ti);

    XChar szPath[LMT_MAX_PATH] = { 0 };
    size_t stSize = CPlatform::GetPathInfo(szPath, LMT_MAX_PATH, true);
    CXChar::Format(szPath + stSize, (LMT_MAX_PATH - stSize), DumpFileName, CPlatform::GetCurrentPId(), CPlatform::GetCurrentTId(),
                   ti.usYear, ti.usMonth, ti.usDay, ti.usHour, ti.usMinute, ti.usSecond);
    DEV_DEBUG(TF("Dump file name is : %s"), szPath);
    CFileWriteStream fws;
    if (fws.Create(szPath) == RET_OKAY)
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        Byte  bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
        fws.Write(bHead, BOML_UTF8);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        Byte  bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
        fws.Write(bHead, BOML_UTF32);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
        Byte  bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
        fws.Write(bHead, BOML_UTF16);
#endif
        try
        {
            throw; // throw again
        }
        catch (CException& e)
        {
            CExceptionTracer::Dump(fws, (Int)e.GetCode());
        }
        catch (...)
        {
            CExceptionTracer::Dump(fws);
        }
    }
}

INLINE void CExceptionTerminater::SignalHandler(Int nSig)
{
    throw CException(nSig);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_EXCEPTION_INL__
