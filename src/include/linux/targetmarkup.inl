// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_MARKUP_INL__
#define __TARGET_MARKUP_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CUTF8
INLINE bool CUTF8::LocalToUTF8(const CString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    strOut = strIn;
    return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    size_t stLen = strIn.Length();
    size_t stBuf = (stLen + 1) * ENT_UTF32_UTF8;
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PByte psz8 = Buf.GetBuf();
    if (psz8 != nullptr)
    {
        Int nRet = CChar::Utf32ToUtf8(*strIn, -1, psz8, (Int)(stLen * ENT_UTF32_UTF8));
        assert(nRet > 0);
        psz8[nRet] = 0;
        strOut.Attach(psz8, stBuf, (size_t)nRet);
        Buf.Detach();
        return true;
    }
#endif
    return false;
}

INLINE bool CUTF8::UTF8ToLocal(const CCString& strIn, CString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    strOut = strIn;
    return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    size_t stLen = strIn.Length();
    size_t stBuf = (stLen + 1) * sizeof(UInt);
    stBuf = DEF::Align<size_t>(stBuf, (size_t)LMT_KEY);
    CBufReadStream Buf(stBuf);
    PUInt psz32 = (PUInt)Buf.GetBuf();
    if (psz32 != nullptr)
    {
        Int nRet = CChar::Utf8ToUtf32(*strIn, (Int)stLen, psz32, (Int)(stLen));
        assert(nRet > 0);
        psz32[nRet] = 0;
        strOut.Attach((PByte)psz32, stBuf, (size_t)nRet * sizeof(UInt));
        Buf.Detach();
        return true;
    }
#endif
    return false;
}

///////////////////////////////////////////////////////////////////
// CXMLTraits
INLINE bool CXMLTraits::CodePage(CString& strCodePage) const
{
    CPlatform::SetLocal(LC_CTYPE, TF(""));
    strCodePage = nl_langinfo(CODESET);
    return (strCodePage.IsEmpty() == false);
}

///////////////////////////////////////////////////////////////////
// CXMLDocument
INLINE bool CXMLDocument::Load(PByte pData, size_t stLen)
{
    bool bRet  = false;
    Int nEncoding = CXChar::CheckBOM(pData);
    if ((m_eENCODING == ENT_UTF8) || (nEncoding == ENT_UTF8))
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        m_eENCODING = ENT_UTF8;
        bRet = Parse((PXStr)(pData));
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf8ToUtf32((PUChar)(pData), -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING = ENT_UTF32;
            bRet = Parse(psz32);
            FREE( psz32 );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF16) || (nEncoding == ENT_UTF16) || (nEncoding == ENT_UTF16R))
    {
        PUShort psz16 = (PUShort)(pData);
        if (nEncoding == ENT_UTF16R)
        {
            size_t stSize = (stLen + 1) / sizeof(UShort);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz16[i] = CPlatform::ByteSwap(psz16[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        PXStr psz8 = (PXStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 + (stLen + 1) * sizeof(UInt) );
        if (psz8 != nullptr)
        {
            PUInt psz32 = (PUInt)(psz8 + (stLen + 1) * ENT_UTF32_UTF8);
            Int nRet = CXChar::Utf16ToUtf32(psz16, -1, psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            nRet = CXChar::Utf32ToUtf8(psz32, nRet, (PUChar)psz8, (Int)(stLen * ENT_UTF32_UTF8));
            assert(nRet > 0);
            psz8[nRet] = 0;

            m_eENCODING = ENT_UTF8;
            bRet = Parse(psz8);
            FREE( psz8 );
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf16ToUtf32(psz16, -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING = ENT_UTF32;
            bRet = Parse(psz32);
            FREE( psz32 );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF32) || (nEncoding == ENT_UTF32) || (nEncoding == ENT_UTF32R))
    {
        PUInt psz32 = (PUInt)(pData);
        if (nEncoding == ENT_UTF32R)
        {
            size_t stSize = (stLen + 1) / sizeof(UInt);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz32[i] = CPlatform::ByteSwap(psz32[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        PXStr psz8 = (PXStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
        if (psz8 != nullptr)
        {
            Int nRet = CXChar::Utf32ToUtf8(psz32, -1, (PUChar)psz8, (Int)(stLen * ENT_UTF32_UTF8));
            assert(nRet > 0);
            psz8[nRet] = 0;

            m_eENCODING = ENT_UTF8;
            bRet = Parse(psz8);
            FREE( psz8 );
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        m_eENCODING = ENT_UTF32;
        bRet = Parse((PXStr)psz32);
#endif
    }
    else // ENT_LOCAL
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        m_eENCODING = ENT_UTF8;
        bRet = Parse((PStr)(pData));
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        // SetLocale(LC_ALL, TF(""));
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf8ToUtf32((PUChar)(pData), -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING = ENT_UTF32;
            bRet = Parse(psz32);
            FREE( psz32 );
        }
#endif
    }
    return bRet;
}

INLINE bool CXMLDocument::Save(CStream& stream, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    assert(stream.IsWrite());
    assert(stream.Size() > 0);
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        size_t stLen = Size();
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
            case ENT_UTF8:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF80 << (UChar)BOML_UTF81 << (UChar)BOML_UTF82;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                stream.Write(*strBuf, stLen);
                return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    stream.Write(psz8, (size_t)nRet);

                    FREE( psz8 );
                    return true;
                }
#endif
            }
            break;
            case ENT_UTF16:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF160 << (UChar)BOML_UTF161;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) + (stLen + 1) * sizeof(UInt));
                if (psz16 != nullptr)
                {
                    PUInt psz32 = (PUInt)(psz16 + (stLen + 1) * ENT_UTF32_UTF16);

                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    nRet = CXChar::Utf32ToUtf16(psz32, nRet, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    stream.Write(psz16, (size_t)nRet * sizeof(UShort));

                    FREE( psz16 );
                    return true;
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) );
                if (psz16 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf16((PUInt)*strBuf, (Int)stLen, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    stream.Write(psz16, (size_t)nRet * sizeof(UShort));

                    FREE( psz16 );
                    return true;
                }
#endif
            }
            break;
            case ENT_UTF32:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF320 << (UChar)BOML_UTF321 << (UChar)BOML_UTF322 << (UChar)BOML_UTF323;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( psz32 );
                    return true;
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                stream.Write(*strBuf, stLen * sizeof(UInt));
                return true;
#endif
            }
            break;
            default: // ENT_LOCAL, set CXMLDeclaration.SetEncoding as local before save
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                stream.Write(*strBuf, stLen);
                return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                // SetLocale(LC_ALL, TF(""));
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    stream.Write(psz8, (size_t)nRet);

                    FREE( psz8 );
                    return true;
                }
#endif
            }
        }
    }
    return false;
}

INLINE bool CXMLDocument::Save(CBufWriteStream& stream, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    if (m_XMLNode.GetSize() > 0)
    {
        assert(m_pPrev == nullptr);
        assert(m_pNext == nullptr);

        size_t stLen = Size();
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                size_t stSize = bAddBOM ? (stLen + BOML_UTF8) : stLen;
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                        stream.Write(bHead, BOML_UTF8);
                    }
                    stream.Write(*strBuf, (size_t)stLen);
                }
                return (stream.GetBuf() != nullptr);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(psz8, (size_t)nRet);
                    }
                    FREE( psz8 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) + (stLen + 1) * sizeof(UInt));
                if (psz16 != nullptr)
                {
                    PUInt psz32 = (PUInt)(psz16 + (stLen + 1) * ENT_UTF32_UTF16);

                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    nRet = CXChar::Utf32ToUtf16(psz32, nRet, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(UShort) + BOML_UTF16) : (size_t)nRet * sizeof(UShort);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(psz16, (size_t)nRet * sizeof(UShort));
                    }
                    FREE( psz16 );
                    return (stream.GetBuf() != nullptr);
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) );
                if (psz16 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf16((PUInt)*strBuf, (Int)stLen, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(UShort) + BOML_UTF16) : (size_t)nRet * sizeof(UShort);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(psz16, (size_t)nRet * sizeof(UShort));
                    }
                    FREE( psz16 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)((size_t)nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( psz32 );
                    return (stream.GetBuf() != nullptr);
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                size_t stSize = bAddBOM ? (stLen * sizeof(UInt) + BOML_UTF32) : stLen * sizeof(UInt);
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                        stream.Write(bHead, BOML_UTF32);
                    }
                    stream.Write(*strBuf, (size_t)stLen * sizeof(UInt));
                }
                return (stream.GetBuf() != nullptr);
#endif
            }
            break;
        default: // ENT_LOCAL, set CXMLDeclaration.SetEncoding as local before save
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                if (stream.Attach(stLen))
                {
                    stream.Write(*strBuf, stLen);
                }
                return (stream.GetBuf() != nullptr);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                // SetLocale(LC_ALL, TF(""));
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    if (stream.Attach((size_t)nRet))
                    {
                        stream.Write(psz8, (size_t)nRet);
                    }
                    FREE( psz8 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CKVNode
INLINE bool CKVNode::Load(PByte pData, size_t stLen)
{
    bool bRet = false;
    Int nEncoding = CXChar::CheckBOM(pData);
    if ((m_eENCODING == ENT_UTF8) || (nEncoding == ENT_UTF8))
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        m_eENCODING  = ENT_UTF8;
        PXStr psz8 = (PXStr)(pData);
        bRet = Parse(psz8);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        PUChar psz8 = (PUChar)(pData);
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf8ToUtf32(psz8, -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING  = ENT_UTF32;
            PXStr pszBuf = (PXStr)psz32;
            bRet = Parse(pszBuf);
            FREE( psz32 );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF16) || (nEncoding == ENT_UTF16) || (nEncoding == ENT_UTF16R))
    {
        PUShort psz16 = (PUShort)(pData);
        if (nEncoding == ENT_UTF16R)
        {
            size_t stSize = (stLen + 1) / sizeof(UShort);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz16[i] = CPlatform::ByteSwap(psz16[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        PXStr psz8 = (PXStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 + (stLen + 1) * sizeof(UInt) );
        if (psz8 != nullptr)
        {
            PUInt psz32 = (PUInt)(psz8 + (stLen + 1) * ENT_UTF32_UTF8);
            Int nRet = CXChar::Utf16ToUtf32(psz16, -1, psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            nRet = CXChar::Utf32ToUtf8(psz32, nRet, (PUChar)psz8, (Int)(stLen * ENT_UTF32_UTF8));
            assert(nRet > 0);
            psz8[nRet] = 0;

            m_eENCODING  = ENT_UTF8;
            bRet = Parse(psz8);
            FREE( psz8 );
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf16ToUtf32(psz16, -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING  = ENT_UTF32;
            bRet = Parse(psz32);
            FREE( psz32 );
        }
#endif
    }
    else if ((m_eENCODING == ENT_UTF32) || (nEncoding == ENT_UTF32) || (nEncoding == ENT_UTF32R))
    {
        PUInt psz32 = (PUInt)(pData);
        if (nEncoding == ENT_UTF32R)
        {
            size_t stSize = (stLen + 1) / sizeof(UInt);
            for (size_t i = 0; i < stSize; ++i)
            {
                psz32[i] = CPlatform::ByteSwap(psz32[i]);
            }
        }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        PXStr psz8 = (PXStr)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
        if (psz8 != nullptr)
        {
            Int nRet = CXChar::Utf32ToUtf8(psz32, -1, (PUChar)psz8, (Int)(stLen * ENT_UTF32_UTF8));
            assert(nRet > 0);
            psz8[nRet] = 0;

            m_eENCODING  = ENT_UTF8;
            bRet = Parse(psz8);
            FREE( psz8 );
        }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        m_eENCODING  = ENT_UTF32;
        PXStr pszBuf = (PXStr)psz32;
        bRet = Parse(pszBuf);
#endif
    }
    else // ENT_LOCAL
    {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        m_eENCODING  = ENT_LOCAL;
        PXStr psz8 = (PXStr)(pData);
        bRet = Parse(psz8);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        // SetLocale(LC_ALL, TF(""));
        PXStr psz32 = (PXStr)ALLOC( (stLen + 1) * sizeof(UInt) );
        if (psz32 != nullptr)
        {
            Int nRet = CXChar::Utf8ToUtf32((PUChar)(pData), -1, (PUInt)psz32, (Int)(stLen));
            assert(nRet > 0);
            psz32[nRet] = 0;

            m_eENCODING  = ENT_UTF32;
            bRet = Parse(psz32);
            FREE( psz32 );
        }
#endif
    }
    return bRet;
}

INLINE bool CKVNode::Save(CStream& stream, bool bStyle, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    assert(stream.IsWrite());
    assert(stream.Size() > 0);
    if (GetType() != VART_NONE)
    {
        size_t stLen = Size(bStyle ? 0 : -1);
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(bStyle ? 0 : -1, strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF80 << (UChar)BOML_UTF81 << (UChar)BOML_UTF82;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                stream.Write(*strBuf, stLen);
                return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    stream.Write(psz8, (size_t)nRet);

                    FREE( psz8 );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF160 << (UChar)BOML_UTF161;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) + (stLen + 1) * sizeof(UInt));
                if (psz16 != nullptr)
                {
                    PUInt psz32 = (PUInt)(psz16 + (stLen + 1) * ENT_UTF32_UTF16);

                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    nRet = CXChar::Utf32ToUtf16(psz32, nRet, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    stream.Write(psz16, (size_t)nRet * sizeof(UShort));

                    FREE( psz16 );
                    return true;
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) );
                if (psz16 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf16((PUInt)*strBuf, (Int)stLen, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    stream.Write(psz16, (size_t)nRet * sizeof(UShort));

                    FREE( psz16 );
                    return true;
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
                if (bAddBOM)
                {
                    stream << (UChar)BOML_UTF320 << (UChar)BOML_UTF321 << (UChar)BOML_UTF322 << (UChar)BOML_UTF323;
                }
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    stream.Write(psz32, (size_t)nRet * sizeof(UInt));

                    FREE( psz32 );
                    return true;
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                stream.Write(*strBuf, stLen * sizeof(UInt));
                return true;
#endif
            }
            break;
        default: // ENT_LOCAL
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                stream.Write(*strBuf, stLen);
                return true;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                // SetLocale(LC_ALL, TF(""));
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    stream.Write(psz8, (size_t)nRet);

                    FREE( psz8 );
                    return true;
                }
#endif
            }
        }
    }
    return false;
}

INLINE bool CKVNode::Save(CBufWriteStream& stream, bool bStyle, ENCODING_TYPE eENCODING, bool bAddBOM)
{
    if (GetType() != VART_NONE)
    {
        size_t stLen = Size(bStyle ? 0 : -1);
        CString strBuf;
        if (strBuf.SetBufferLength(stLen + 1) == false)
        {
            return false;
        }
        Save(bStyle ? 0 : -1, strBuf);
        stLen = strBuf.Length();

        switch (eENCODING)
        {
        case ENT_UTF8:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                size_t stSize = bAddBOM ? (stLen + BOML_UTF8) : stLen;
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                        stream.Write(bHead, BOML_UTF8);
                    }
                    stream.Write(*strBuf, (size_t)stLen);
                }
                return (stream.GetBuf() != nullptr);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet + BOML_UTF8) : (size_t)nRet;
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF8] = { BOML_UTF80, BOML_UTF81, BOML_UTF82 };
                            stream.Write(bHead, BOML_UTF8);
                        }
                        stream.Write(psz8, (size_t)nRet);
                    }
                    FREE( psz8 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF16:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) + (stLen + 1) * sizeof(UInt));
                if (psz16 != nullptr)
                {
                    PUInt psz32 = (PUInt)(psz16 + (stLen + 1) * ENT_UTF32_UTF16);

                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    nRet = CXChar::Utf32ToUtf16(psz32, nRet, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(UShort) + BOML_UTF16) : (size_t)nRet * sizeof(UShort);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(psz16, (size_t)nRet * sizeof(UShort));
                    }
                    FREE( psz16 );
                    return (stream.GetBuf() != nullptr);
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                PUShort psz16 = (PUShort)ALLOC( (stLen + 1) * ENT_UTF32_UTF16 * sizeof(UShort) );
                if (psz16 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf16((PUInt)*strBuf, (Int)stLen, psz16, (Int)(stLen * ENT_UTF32_UTF16));
                    assert(nRet > 0);
                    psz16[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(UShort) + BOML_UTF16) : (size_t)nRet * sizeof(UShort);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF16] = { BOML_UTF160, BOML_UTF161 };
                            stream.Write(bHead, BOML_UTF16);
                        }
                        stream.Write(psz16, (size_t)nRet * sizeof(UShort));
                    }
                    FREE( psz16 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
            break;
        case ENT_UTF32:
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                PUInt psz32 = (PUInt)ALLOC( (stLen + 1) * sizeof(UInt) );
                if (psz32 != nullptr)
                {
                    Int nRet = CXChar::Utf8ToUtf32((PUChar)*strBuf, (Int)stLen, psz32, (Int)(stLen));
                    assert(nRet > 0);
                    psz32[nRet] = 0;

                    size_t stSize = bAddBOM ? (size_t)(nRet * sizeof(UInt) + BOML_UTF32) : (size_t)nRet * sizeof(UInt);
                    if (stream.Attach(stSize))
                    {
                        if (bAddBOM)
                        {
                            Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                            stream.Write(bHead, BOML_UTF32);
                        }
                        stream.Write(psz32, (size_t)nRet * sizeof(UInt));
                    }
                    FREE( psz32 );
                    return (stream.GetBuf() != nullptr);
                }
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                size_t stSize = bAddBOM ? (size_t)(stLen * sizeof(UInt) + BOML_UTF32) : (size_t)stLen * sizeof(UInt);
                if (stream.Attach(stSize))
                {
                    if (bAddBOM)
                    {
                        Byte bHead[BOML_UTF32] = { BOML_UTF320, BOML_UTF321, BOML_UTF322, BOML_UTF323 };
                        stream.Write(bHead, BOML_UTF32);
                    }
                    stream.Write(*strBuf, (size_t)stLen * sizeof(UInt));
                }
                return (stream.GetBuf() != nullptr);
#endif
            }
            break;
        default: // ENT_LOCAL
            {
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
                if (stream.Attach(stLen))
                {
                    stream.Write(*strBuf, stLen);
                }
                return (stream.GetBuf() != nullptr);
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
                // SetLocale(LC_ALL, TF(""));
                PUChar psz8 = (PUChar)ALLOC( (stLen + 1) * ENT_UTF32_UTF8 );
                if (psz8 != nullptr)
                {
                    Int nRet = CXChar::Utf32ToUtf8((PUInt)*strBuf, (Int)stLen, psz8, (Int)(stLen * ENT_UTF32_UTF8));
                    assert(nRet > 0);
                    psz8[nRet] = 0;

                    if (stream.Attach((size_t)nRet))
                    {
                        stream.Write(psz8, (size_t)nRet);
                    }
                    FREE( psz8 );
                    return (stream.GetBuf() != nullptr);
                }
#endif
            }
        }
    }
    return false;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_MARKUP_INL__
