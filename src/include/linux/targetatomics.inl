// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_ATOMICS_INL__
#define __TARGET_ATOMICS_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CAtomics
template <typename T>
FORCEINLINE T CAtomics::Increment16(T volatile* pDst)
{
    return (T)__sync_add_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Increment(T volatile* pDst)
{
    return (T)__sync_add_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Increment64(T volatile* pDst)
{
    return (T)__sync_add_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Decrement16(T volatile* pDst)
{
    return (T)__sync_sub_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Decrement(T volatile* pDst)
{
    return (T)__sync_sub_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Decrement64(T volatile* pDst)
{
    return (T)__sync_sub_and_fetch(pDst, 1);
}

template <typename T>
FORCEINLINE T CAtomics::Add16(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_add(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Add(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_add(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Add64(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_add(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::And16(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_and(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::And(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_and(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::And64(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_and(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Or16(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_or(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Or(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_or(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Or64(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_or(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Xor16(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_xor(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Xor(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_xor(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Xor64(T volatile* pDst, T tVal)
{
    return (T)__sync_fetch_and_xor(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Exchange16(T volatile* pDst, T tVal)
{
    return (T)__sync_lock_test_and_set(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Exchange(T volatile* pDst, T tVal)
{
    return (T)__sync_lock_test_and_set(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::Exchange64(T volatile* pDst, T tVal)
{
    return (T)__sync_lock_test_and_set(pDst, tVal);
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange16(T volatile* pDst, T tExchange, T tComparand)
{
    return (T)__sync_val_compare_and_swap(pDst, tComparand, tExchange);
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange(T volatile* pDst, T tExchange, T tComparand)
{
    return (T)__sync_val_compare_and_swap(pDst, tComparand, tExchange);
}

template <typename T>
FORCEINLINE T CAtomics::CompareExchange64(T volatile* pDst, T tExchange, T tComparand)
{
    return (T)__sync_val_compare_and_swap(pDst, tComparand, tExchange);
}

FORCEINLINE void* CAtomics::ExchangePtr(void* volatile* ppDst, void* pVal)
{
    return (void*)__sync_lock_test_and_set(ppDst, pVal);
}

FORCEINLINE void* CAtomics::CompareExchangePtr(void* volatile* ppDst, void* pExchange, void* pComparand)
{
    return (void*)__sync_val_compare_and_swap(ppDst, pComparand, pExchange);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_ATOMICS_INL__
