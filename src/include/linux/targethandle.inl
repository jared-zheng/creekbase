// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_HANDLE_INL__
#define __TARGET_HANDLE_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CFileHandle
INLINE CFileHandle::CFileHandle(Handle h)
: m_Handle(h)
{
}

INLINE CFileHandle::~CFileHandle(void)
{
    Close();
}

INLINE CFileHandle::CFileHandle(const CFileHandle& aSrc)
: m_Handle(aSrc.m_Handle)
{
}

INLINE CFileHandle& CFileHandle::operator=(const CFileHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_Handle);
    }
    return (*this);
}

INLINE CFileHandle& CFileHandle::operator=(Handle h)
{
    if (m_Handle != h)
    {
        Attach(h);
    }
    return (*this);
}

INLINE bool CFileHandle::operator==(Handle h) const
{
    return (m_Handle == h);
}

INLINE bool CFileHandle::operator!=(Handle h) const
{
    return (m_Handle != h);
}

INLINE bool CFileHandle::operator==(const CFileHandle& aSrc) const
{
    return (m_Handle == aSrc.m_Handle);
}

INLINE bool CFileHandle::operator!=(const CFileHandle& aSrc) const
{
    return (m_Handle != aSrc.m_Handle);
}

INLINE CFileHandle::operator Handle(void) const
{
    return m_Handle;
}

INLINE bool CFileHandle::IsValid(void) const
{
    return (m_Handle != HANDLE_INVALID);
}

INLINE void CFileHandle::Attach(Handle h)
{
    Close();
    m_Handle = h;
}

INLINE Handle CFileHandle::Detach(void)
{
    Handle h = m_Handle;
    m_Handle = HANDLE_INVALID;
    return h;
}

INLINE void CFileHandle::Close(void)
{
    if (m_Handle != HANDLE_INVALID)
    {
        close(m_Handle);
    }
    m_Handle = HANDLE_INVALID;
}

///////////////////////////////////////////////////////////////////
// CThreadAttr
INLINE CThreadAttr::CThreadAttr(void)
{
    pthread_attr_init(&m_Attr);
}

INLINE CThreadAttr::~CThreadAttr(void)
{
    pthread_attr_destroy(&m_Attr);
}

INLINE CThreadAttr::CThreadAttr(const CThreadAttr& aSrc)
{
}

INLINE CThreadAttr& CThreadAttr::operator=(const CThreadAttr& aSrc)
{
    return (*this);
}

INLINE void CThreadAttr::Reset(void)
{
}

///////////////////////////////////////////////////////////////////
// CThreadHandle
INLINE CThreadHandle::CThreadHandle(TId Id)
: m_TId(Id)
{
}

INLINE CThreadHandle::~CThreadHandle(void)
{
}

INLINE CThreadHandle::CThreadHandle(const CThreadHandle& aSrc)
: m_TId(aSrc.m_TId)
{
}

INLINE CThreadHandle& CThreadHandle::operator=(const CThreadHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_TId);
    }
    return (*this);
}

INLINE CThreadHandle& CThreadHandle::operator=(TId Id)
{
    if (pthread_equal(m_TId, Id) == 0)
    {
        Attach(Id);
    }
    return (*this);
}

INLINE bool CThreadHandle::operator==(TId Id) const
{
    return (pthread_equal(m_TId, Id) != 0);
}

INLINE bool CThreadHandle::operator!=(TId Id) const
{
    return (pthread_equal(m_TId, Id) == 0);
}

INLINE bool CThreadHandle::operator==(const CThreadHandle& aSrc) const
{
    return (pthread_equal(m_TId, aSrc.m_TId) != 0);
}

INLINE bool CThreadHandle::operator!=(const CThreadHandle& aSrc) const
{
    return (pthread_equal(m_TId, aSrc.m_TId) == 0);
}

INLINE CThreadHandle::operator TId(void) const
{
    return m_TId;
}

INLINE bool CThreadHandle::IsValid(void) const
{
    return (m_TId != 0);
}

INLINE void CThreadHandle::Attach(TId Id)
{
    Close();
    m_TId = Id;
}

INLINE TId CThreadHandle::Detach(void)
{
    TId Id = m_TId;
    m_TId = 0;
    return Id;
}

INLINE void CThreadHandle::Close(void)
{
    m_TId = 0;
}

INLINE void CThreadHandle::Reset(void)
{
    m_TId = 0;
}

///////////////////////////////////////////////////////////////////
// module: dynamic load, link with -ldl
INLINE CModuleHandle::CModuleHandle(Module m)
: m_Module(m)
{
}

INLINE CModuleHandle::~CModuleHandle(void)
{
    Close();
}

INLINE CModuleHandle::CModuleHandle(const CModuleHandle& aSrc)
: m_Module(aSrc.m_Module)
{
}

INLINE CModuleHandle& CModuleHandle::operator=(const CModuleHandle& aSrc)
{
    if (this != &aSrc)
    {
        Attach(aSrc.m_Module);
    }
    return (*this);
}

INLINE CModuleHandle& CModuleHandle::operator=(Module m)
{
    if (m_Module != m)
    {
        Attach(m);
    }
    return (*this);
}

INLINE bool CModuleHandle::operator==(Module m) const
{
    return (m_Module == m);
}

INLINE bool CModuleHandle::operator!=(Module m) const
{
    return (m_Module != m);
}

INLINE bool CModuleHandle::operator==(const CModuleHandle& aSrc) const
{
    return (m_Module == aSrc.m_Module);
}

INLINE bool CModuleHandle::operator!=(const CModuleHandle& aSrc) const
{
    return (m_Module != aSrc.m_Module);
}

INLINE CModuleHandle::operator Module(void) const
{
    return m_Module;
}

INLINE bool CModuleHandle::IsValid(void) const
{
    return (m_Module != nullptr);
}

INLINE void CModuleHandle::Attach(Module m)
{
    Close();
    m_Module = m;
}

INLINE Module CModuleHandle::Detach(void)
{
    Module m = m_Module;
    m_Module = nullptr;
    return m;
}

INLINE void CModuleHandle::Close(void)
{
    if (m_Module != nullptr)
    {
        dlclose(m_Module);
        m_Module = nullptr;
    }
}

INLINE void* CModuleHandle::Find(PCStr pszSymbol)
{
    if (m_Module != nullptr)
    {
        return _tdlsym(m_Module, pszSymbol);
    }
    return nullptr;
}

INLINE bool CModuleHandle::Load(PCXStr pszPath, Int nFlag)
{
    Close();
    m_Module = _tdlopen(pszPath, nFlag);
    return (m_Module != nullptr);
}

INLINE PCXStr CModuleHandle::Error(void)
{
    return (_tdlerror());
}

///////////////////////////////////////////////////////////////////
// pipe open return pipe pid
INLINE PId CProcessHandle::GetParentId(void)
{
    return getppid();
}

INLINE PId CProcessHandle::GetGroupId(PId pid)
{
    return ((pid == 0) ? getpgrp() : getpgid(pid));
}

INLINE CProcessHandle::CProcessHandle(void)
: m_PId(0)
, m_pFILE(nullptr)
{
}

INLINE CProcessHandle::~CProcessHandle(void)
{
    Close();
}

INLINE CProcessHandle::CProcessHandle(const CProcessHandle&)
: m_PId(0)
, m_pFILE(nullptr)
{
}

INLINE CProcessHandle& CProcessHandle::operator=(const CProcessHandle&)
{
    return (*this);
}

#ifndef __MODERN_CXX_NOT_SUPPORTED
INLINE CProcessHandle::CProcessHandle(CProcessHandle&& aSrc)
: m_PId(0)
, m_pFILE(nullptr)
{
    m_PId   = aSrc.m_PId;
    m_pFILE = aSrc.m_pFILE;
    aSrc.Detach();
}

INLINE CProcessHandle& CProcessHandle::operator=(CProcessHandle&& aSrc)
{
    if (this != &aSrc)
    {
        Close();

        m_PId   = aSrc.m_PId;
        m_pFILE = aSrc.m_pFILE;
        aSrc.Detach();
    }
    return (*this);
}
#endif

INLINE bool CProcessHandle::operator==(PId pid) const
{
    return (m_PId == pid);
}

INLINE bool CProcessHandle::operator!=(PId pid) const
{
    return (m_PId != pid);
}

INLINE CProcessHandle::operator PId(void) const
{
    return m_PId;
}

INLINE FILE* CProcessHandle::Get(void) const
{
    return m_pFILE;
}

INLINE bool CProcessHandle::IsValid(void) const
{
    return (m_PId != 0);
}

INLINE bool CProcessHandle::Check(void) const
{
    if (IsValid())
    {
        return (kill(m_PId, 0) == RET_OKAY);
    }
    return false;
}

INLINE void CProcessHandle::Attach(CProcessHandle& aSrc)
{
    if (this != &aSrc)
    {
        Close();

        m_PId   = aSrc.m_PId;
        m_pFILE = aSrc.m_pFILE;
        aSrc.Detach();
    }
}

INLINE void CProcessHandle::Detach(void)
{
    m_PId   = 0;
    m_pFILE = nullptr;
}

INLINE Int CProcessHandle::Wait(Int nOption) const
{
    Int nRet = (Int)RET_ERROR;
    if (IsValid())
    {
        while (waitpid(m_PId, &nRet, nOption) == RET_ERROR)
        {
            if (errno != EINTR)
            {
                break;
            }
        }
        DEV_DEBUG(TF("  CProcessHandle[%p] Wait option = %x ret = %d"), this, nOption, nRet);
    }
    return nRet;
}

INLINE bool CProcessHandle::Open(PCXStr pszPath, XChar* const pszArgs[], Int nFlag)
{
    Close();

    Handle fd[PIPE_HANDLE_COUNT];
    if (pipe(fd) == RET_ERROR)
    {
        DEV_DEBUG(TF("  CProcessHandle[%p] path = %s, flag = %d, pipe errno = %d"), this, pszPath, nFlag, errno);
        return false;
    }

    PId ChildPId = fork();
    if (ChildPId == RET_ERROR)
    {
        DEV_DEBUG(TF("  CProcessHandle[%p] path = %s, flag = %d, fork errno = %d"), this, pszPath, nFlag, errno);
        return false;
    }
    if (ChildPId == 0)
    {
        if (nFlag == HANDLE_INVALID)
        {
            Handle fd = _topen(TF("/dev/null"), FILEF_ACS_ALL);
            if (fd == HANDLE_INVALID)
            {
                exit(0);
            }
            dup2(fd, STDIN_FILENO);
            dup2(fd, STDOUT_FILENO);
            dup2(fd, STDERR_FILENO);
            close(fd);
        }
        else if (nFlag == WRITE_HANDLE)
        {
            // parent write close child write fd, dup stdin to read fd
            close(fd[WRITE_HANDLE]);
            dup2(fd[READ_HANDLE], STDIN_FILENO);
        }
        else
        {
            // parent read close child read fd, dup stdout to write fd
            close(fd[READ_HANDLE]);
            dup2(fd[WRITE_HANDLE], STDOUT_FILENO);
        }
        setsid();
        setpgid(0, 0); // parent kill(-ChildPId)
        DEV_DEBUG(TF("  CProcessHandle[%p] path = %s, flag = %d, child execl begin"), this, pszPath, nFlag);
        execvp(pszPath, pszArgs);
        DEV_DEBUG(TF("  CProcessHandle[%p] path = %s, flag = %d, child execl end"), this, pszPath, nFlag);
        exit(0);
    }
    else
    {
        DEV_DEBUG(TF("  CProcessHandle[%p] path = %s, flag = %d, fork ChildPId=%lld"), this, pszPath, nFlag, ChildPId);
        // write close parent read fd, read close parent write fd
        close((nFlag == WRITE_HANDLE) ? fd[READ_HANDLE] : fd[WRITE_HANDLE]);
    }
    m_PId = ChildPId;
    if (nFlag != HANDLE_INVALID)
    {
        if (nFlag == WRITE_HANDLE)
        {
            m_pFILE = _tfdopen(fd[WRITE_HANDLE], TF("w"));
        }
        else
        {
            m_pFILE = _tfdopen(fd[READ_HANDLE], TF("r"));
        }
        return (m_pFILE != nullptr);
    }
    return true;
}

INLINE void CProcessHandle::Close(void)
{
    if (m_pFILE != nullptr)
    {
        fclose(m_pFILE);
        m_pFILE = nullptr;
    }
    if (m_PId != 0)
    {
        PId pid = (0 - m_PId);
        m_PId = 0;

        Int nRet = (Int)RET_ERROR;
        while (waitpid(pid, &nRet, WNOHANG) == RET_ERROR)
        {
            if (errno != EINTR)
            {
                break;
            }
        }
        DEV_DEBUG(TF("  CProcessHandle[%p] Close ret = %d"), this, nRet);
        kill(pid, SIGTERM);
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_HANDLE_INL__
