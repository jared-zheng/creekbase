// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_NETWORK_HTTP_INL__
#define __TARGET_NETWORK_HTTP_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CURI
INLINE bool CURI::EncodeLocal(const CString& strIn, CCString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    Int   nRet = (Int)stLen;
    PByte psz8 = (PByte)strIn.GetBuffer();
    if (psz8 != nullptr)
    {
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    CBufReadStream Buf((stLen + 1) * ENT_UTF32_UTF8);
    PByte psz8 = Buf.GetBuf();
    if (psz8 != nullptr)
    {
        Int nRet = CChar::Utf32ToUtf8(*strIn, -1, psz8, (Int)(stLen * ENT_UTF32_UTF8));
        assert(nRet > 0);
        psz8[nRet] = 0;
#endif
        for (Int i = 0; i < nRet; ++i)
        {
            Byte b = psz8[i];
            if (CURI::UnreservedChar((Char)b) || CURI::ReservedChar((Char)b))
            {
                strOut += (Char)b;
            }
            else
            {
                strOut += '%';
                strOut += (Char)CharToHex(b >> 4);
                strOut += (Char)CharToHex(b & 0x0F);
            }
        }
        return true;
    }
    return false;
}

INLINE bool CURI::DecodeLocal(const CCString& strIn, CString& strOut)
{
    strOut.Empty();
    if (strIn.IsEmpty())
    {
        return false;
    }
    size_t stLen = strIn.Length();
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    CBufReadStream Buf(stLen + 1);
    PStr psz8 = (PStr)Buf.GetBuf();
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    CBufReadStream Buf((stLen + 1) + (stLen + 1) * sizeof(UInt));
    PStr  psz8  = (PStr)Buf.GetBuf();
    PUInt psz32 = (PUInt)(psz8 + (stLen + 1));
#endif
    if (psz8 != nullptr)
    {
        Int nRet = 0;
        for (size_t i = 0; i < stLen; ++i)
        {
            if (strIn[i] != '%')
            {
                psz8[nRet] = strIn[i];
                ++nRet;
            }
            else if (i < (stLen - 2))
            {
                UChar uc1 = HexToChar(strIn[i + 1]);
                UChar uc2 = HexToChar(strIn[i + 2]);
                i += 2;

                psz8[nRet] = (Char)((uc1 << 4) + uc2);
                ++nRet;
            }
        }
        psz8[nRet] = 0;

#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
        strOut = psz8;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
        nRet = CChar::Utf8ToUtf32(*psz8, nRet, psz32, (Int)(stLen));
        assert(nRet > 0);
        psz32[nRet] = 0;
        strOut = psz32;
#endif
        return true;
    }
    return false;
}

INLINE bool CURI::EncodeLocalToUTF8(const CString& strIn, CCString& strOut)
{
    return EncodeLocal(strIn, strOut);
}

INLINE bool CURI::DecodeUTF8ToLocal(const CCString& strIn, CString& strOut)
{
    return DecodeLocal(strIn, strOut);
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_NETWORK_HTTP_INL__
