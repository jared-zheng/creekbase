// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_FILE_LOG_INL__
#define __TARGET_FILE_LOG_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CFileLog
SELECTANY CPCXStr CFileLog::LogFileTime   = TF("/%s_%04d%02d%02d_%02d%02d%02d%03d_%d.log");
SELECTANY CPCXStr CFileLog::LogFileName   = TF("%s/%s.log");
SELECTANY CPCXStr CFileLog::LogFileTrace  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[TRACE][%08lX]%s\n");
SELECTANY CPCXStr CFileLog::LogFileDebug  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DEBUG][%08lX]%s\n");
SELECTANY CPCXStr CFileLog::LogFileInfo   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[INFO] [%08lX]%s\n");
SELECTANY CPCXStr CFileLog::LogFileDump   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[DUMP] [%08lX]%s\n");
SELECTANY CPCXStr CFileLog::LogFileWarn   = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[WARN] [%08lX]%s\n");
SELECTANY CPCXStr CFileLog::LogFileError  = TF("%04d-%02d-%02d %02d:%02d:%02d.%03d[ERROR][%08lX]%s\n");

INLINE bool CFileLogPipe::OpenPipe(bool bCheck)
{
    if ((m_Log.GetLevel() & LOGL_DEVPRINT) != 0)
    {
        return true;
    }

    if (bCheck && (m_hPipe != HANDLE_INVALID))
    {
        return true;
    }
    ClosePipe();

    CString strPipeName;
    strPipeName.Format(TF("%s/%s-%08lX.fifo"), m_Log.m_strPath.GetBuffer(), m_Log.m_strPrefix.GetBuffer(), CPlatform::GetCurrentPId());

    Int nRet = _tmkfifo(strPipeName.GetBuffer(), ALLRW_ACS);
    if ((nRet == RET_ERROR) && (errno != EEXIST))
    {
        DEV_DEBUG(TF("file log pipe _tmkfifo %s failed : %d"), strPipeName.GetBuffer(), errno);
        return false;
    }
    Handle hfd = _topen(strPipeName.GetBuffer(), O_RDONLY|O_NONBLOCK, 0);
    m_hPipe    = _topen(strPipeName.GetBuffer(), O_WRONLY|O_NONBLOCK, 0);

    close(hfd);
    if (m_hPipe == HANDLE_INVALID)
    {
        DEV_DEBUG(TF("file log pipe _topen %s failed : %d"), strPipeName.GetBuffer(), errno);
        return false;
    }
    signal(SIGPIPE, SIG_IGN);
    m_strPipe = strPipeName;
    DEV_DEBUG(TF("file log pipe OpenPipe %s"), strPipeName.GetBuffer());
    return true;
}

INLINE void CFileLogPipe::ClosePipe(void)
{
    if (m_hPipe != HANDLE_INVALID)
    {
        close(m_hPipe);
        m_hPipe = HANDLE_INVALID;
    }
    if (m_strPipe.Length() > 0)
    {
        _tunlink(m_strPipe.GetBuffer());
        m_strPipe.Empty();
    }
}

INLINE void CFileLogPipe::WritePipe(PCXStr pszBuf, size_t stBuf)
{
    if ((m_Log.GetLevel() & LOGL_DEVPRINT) != 0)
    {
        write(STDOUT_FILENO, pszBuf, stBuf * sizeof(XChar));
    }
    else
    {
#ifdef __RUNTIME_DEBUG__
        ssize_t sstRet = write(m_hPipe, pszBuf, stBuf * sizeof(XChar));
        DEV_DEBUG(TF("file log pipe write size = %d, errno : %d"), sstRet, errno);
#else
        write(m_hPipe, pszBuf, stBuf * sizeof(XChar));
#endif
    }
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_FILE_LOG_INL__
