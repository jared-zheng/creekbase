// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_STREAM_FILE_INL__
#define __TARGET_STREAM_FILE_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CFileReadStream
INLINE CFileReadStream::CFileReadStream(void)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufBase(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileReadStream::~CFileReadStream(void)
{
    Close();
}

INLINE CFileReadStream::CFileReadStream(const CFileReadStream&)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufBase(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileReadStream& CFileReadStream::operator=(const CFileReadStream&)
{
    return (*this);
}

INLINE size_t CFileReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file read stream out buf invalid"));
        return 0;
    }
    if ((m_hFile.IsValid() == false) || IsEnd())
    {
        DEV_DEBUG(TF("file read stream invalid handle or EOF"));
        return 0;
    }
    if (m_stBufSize > 0)
    {
        if ((m_pBuf == nullptr) && (PreRead() == 0))
        {
            DEV_DEBUG(TF("file read stream read failed"));
            return 0;
        }
        assert(m_stBufSize >= m_stBufPos);
        size_t stRead = 0;
        size_t stSize = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stBufBase - m_stBufPos));
        if (stSize > (m_stBufSize - m_stBufPos)) // read size > buffer size
        {
            // 1. read data from buffer first
            assert(m_stPos >= (m_stBufBase + m_stBufPos));
            if (m_stPos > (m_stBufBase + m_stBufPos))
            {
                stRead = (m_stBufSize - m_stBufPos);
                MM_SAFE::Cpy(pV, (size_t)stRead, (m_pBuf + m_stBufPos), (size_t)stRead);

                m_stBufPos  = m_stBufSize;
                stSize     -= stRead;
            }

            // 2. read data from file directly(times of buffer size)
            size_t stDirect = (stSize / m_stBufSize) * m_stBufSize;
            if (stDirect > 0)
            {
                if (read(m_hFile, ((PByte)pV + stRead), stDirect) != RET_ERROR)
                {
                    stSize      -= stDirect;
                    stRead      += stDirect;
                    m_stPos     += stDirect;
                    m_stBufBase  = m_stPos;
                    m_stBufPos   = 0;
                }
                else
                {
                    DEV_DEBUG(TF("file read failed1 : %d"), errno);
                    SetError();
                    return stRead;
                }
            }
        }
        if (stSize > 0)// remain data copy to buffer, read data from buffer
        {
            if (m_stPos == (m_stBufBase + m_stBufPos)) // buffer is empty
            {
                stSize = DEF::Min<size_t>(stSize, PreRead());
            }
            if (stSize > 0) // read data size is within buffer size
            {
                MM_SAFE::Cpy(((PByte)pV + stRead), (size_t)stSize, (m_pBuf + m_stBufPos), (size_t)stSize);
                m_stBufPos += stSize;
                stRead     += stSize;
            }
        }
        return stRead;
    }
    else // m_stBufSize = 0
    {
        assert(m_stBufBase == 0);
        assert(m_stBufPos  == m_stPos);
        assert(m_pBuf == nullptr);
        stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
        if (read(m_hFile, pV, stLenBytes) != RET_ERROR)
        {
            m_stPos    += stLenBytes;
            m_stBufPos += stLenBytes;
        }
        else
        {
            DEV_DEBUG(TF("file read failed2 : %d"), errno);
            SetError();
            stLenBytes = 0;
        }
        return stLenBytes;
    }
}

INLINE size_t CFileReadStream::Tell(void) const
{
    return (m_stBufBase + m_stBufPos);
}

INLINE size_t CFileReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CFileReadStream::Rest(void) const
{
    return (Size() - Tell());
}

INLINE size_t CFileReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if ((eFrom == SEEKO_CURRENT) && (m_stBufSize > 0))
        {
            if (skPos > 0)
            {
                if ((size_t)skPos <= (m_stBufSize - m_stBufPos))
                {
                    m_stBufPos += (size_t)skPos;
                    return (m_stBufBase + m_stBufPos);
                }
                else
                {
                    skPos -= (SeekPos)(m_stBufSize - m_stBufPos);
                }
            }
            else if (skPos == 0)
            {
                return (m_stBufBase + m_stBufPos);
            }
            else
            {
                size_t stPos = (size_t)DEF::Abs<SeekPos>(skPos);
                if (stPos <= m_stBufPos)
                {
                    m_stBufPos -= stPos;
                    return (m_stBufBase + m_stBufPos);
                }
                else
                {
                    stPos  = m_stBufSize - m_stBufPos;
                    skPos -= (SeekPos)stPos;
                }
            }
        }
        skPos = lseek(m_hFile, skPos, eFrom);
        if (skPos != RET_ERROR)
        {
            m_stPos     = (size_t)skPos;
            m_stBufBase = m_stPos;
            m_stBufPos  = 0;
        }
    }
    return m_stPos;
}

INLINE void CFileReadStream::Close(void)
{
    m_hFile.Close();

    m_stSize    = 0;
    m_stPos     = 0;
    m_stBufSize = 0;
    m_stBufBase = 0;
    m_stBufPos  = 0;
    if (m_pBuf != nullptr)
    {
        FREE( m_pBuf );
        m_pBuf = nullptr;
    }
    CStream::Close();
}

INLINE UInt CFileReadStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    Int nFlag = O_RDONLY;
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));

    m_hFile = _topen(pszFile, nFlag);
    if (m_hFile != HANDLE_INVALID)
    {
        struct stat sttAttr;
        if (fstat(m_hFile, &sttAttr) != RET_ERROR)
        {
            m_stSize = (size_t)sttAttr.st_size;
            if ((uFlag & FILEF_EXT_NOBUF) == 0)
            {
                m_stBufSize = DEF::Maxmin<size_t>((size_t)READ_BUF_MINSIZE, m_stSize, (size_t)READ_BUF_MAXSIZE);
                m_stBufSize = DEF::Align<size_t>(m_stBufSize, (size_t)READ_BUF_MINSIZE) - MEM_CHUNK_OFFSET;
            }
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)errno;
    DEV_DEBUG(TF("file read create or open %s with flag = %x return : %d"), pszFile, nFlag, uRet);
    return uRet;
}

INLINE const CFileHandle& CFileReadStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CFileReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

INLINE size_t CFileReadStream::PreRead(void)
{
    if ((IsError() == false) && (m_stBufSize > 0))
    {
        if (m_pBuf == nullptr)
        {
            m_pBuf = reinterpret_cast<PByte>( ALLOC( m_stBufSize ) );
        }
        if (m_pBuf != nullptr)
        {
            size_t stSize = DEF::Min<size_t>(m_stBufSize, (m_stSize - m_stPos));
            if (read(m_hFile, m_pBuf, stSize) != RET_ERROR)
            {
                m_stBufBase  = m_stPos;
                m_stBufPos   = 0;
                m_stPos     += stSize;
                return stSize;
            }
            DEV_DEBUG(TF("file read failed3 : %d"), errno);
            SetError();
        }
    }
    return 0;
}

///////////////////////////////////////////////////////////////////
// CFileWriteStream
INLINE CFileWriteStream::CFileWriteStream(size_t stBuf)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_stPos(0)
, m_stBufSize(stBuf)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileWriteStream::~CFileWriteStream(void)
{
    Close();
}

INLINE CFileWriteStream::CFileWriteStream(const CFileWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_stPos(0)
, m_stBufSize(0)
, m_stBufPos(0)
, m_pBuf(nullptr)
{
}

INLINE CFileWriteStream& CFileWriteStream::operator=(const CFileWriteStream&)
{
    return (*this);
}

INLINE size_t CFileWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file write stream in buf invalid"));
        return 0;
    }
    if ((m_hFile.IsValid() == false) || IsError())
    {
        DEV_DEBUG(TF("file write stream invalid handle or error"));
        return 0;
    }
    if (m_pBuf != nullptr)
    {
        assert(m_stBufSize > 0);
        size_t stSize = m_stBufSize - m_stBufPos;
        if (stSize > stLenBytes)
        {
            MM_SAFE::Cpy((m_pBuf + m_stBufPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
            m_stBufPos += stLenBytes;
            stSize = stLenBytes;
        }
        else
        {
            // write buffer data
            if (m_stBufPos > 0)
            {
                if (write(m_hFile, m_pBuf, m_stBufPos) != RET_ERROR)
                {
                    m_stPos    += m_stBufPos;
                    m_stBufPos  = 0;
                }
                else
                {
                    DEV_DEBUG(TF("file write failed0 : %d"), errno);
                    stLenBytes = 0;
                }
            }
            //
            if ((stLenBytes > 0) && (write(m_hFile, pV, stLenBytes) != RET_ERROR))
            {
                m_stPos += stLenBytes;
                stSize   = stLenBytes;
            }
            else
            {
                DEV_DEBUG(TF("file write failed1 : %d"), errno);
                SetError();
                stSize = 0;
            }
        }
        return stSize;
    }
    else
    {
        assert(m_stBufPos == 0);
        if (write(m_hFile, pV, stLenBytes) != RET_ERROR)
        {
            m_stPos += stLenBytes;
            return stLenBytes;
        }
        else
        {
            DEV_DEBUG(TF("file write failed2 : %d"), errno);
            SetError();
        }
    }
    return 0;
}

INLINE size_t CFileWriteStream::Tell(void) const
{
    return (m_stPos + m_stBufPos);
}

INLINE size_t CFileWriteStream::Size(void) const
{
    return (m_stPos + m_stBufPos);
}

INLINE size_t CFileWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    //assert((size_t)DEF::Abs<SeekPos>(skPos) <= Tell());
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        if (m_stBufPos > 0)
        {
            if (write(m_hFile, m_pBuf, m_stBufPos) == RET_ERROR)
            {
                DEV_DEBUG(TF("file write seek failed : %d"), errno);
                SetError();
                return 0;
            }
            m_stPos    += m_stBufPos;
            m_stBufPos  = 0;
        }
        skPos = lseek(m_hFile, skPos, eFrom);
        if (skPos != RET_ERROR)
        {
            m_stPos     = (size_t)skPos;
            m_stBufPos  = 0;
        }
    }
    return m_stPos;
}

INLINE bool CFileWriteStream::Flush(Int nFlag)
{
    if (m_hFile.IsValid())
    {
        if (m_stBufPos > 0)
        {
            if (write(m_hFile, m_pBuf, m_stBufPos) == RET_ERROR)
            {
                DEV_DEBUG(TF("file write flush failed : %d"), errno);
                SetError();
                return false;
            }
            m_stPos    += m_stBufPos;
            m_stBufPos  = 0;
        }
        if (nFlag != FLUSHO_BUF)
        {
            return (fsync(m_hFile) != RET_ERROR);
        }
        return true;
    }
    return false;
}

INLINE void CFileWriteStream::Close(void)
{
    Flush();
    m_hFile.Close();

    m_stPos     = 0;
    m_stBufSize = 0;
    m_stBufPos  = 0;
    if (m_pBuf != nullptr)
    {
        FREE( m_pBuf );
        m_pBuf = nullptr;
    }
    CStream::Close();
}

INLINE UInt CFileWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    Int nFlag = (Int)(uFlag & FILEF_NEW_OPEN_MASK);
    nFlag |= (Int)(uFlag & FILEF_ACS_MASK);
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));
    nFlag |= O_WRONLY;

    Int nMode = ACCESSPERMS;
    if (nFlag & O_CREAT)
    {
        nMode  = (Int)((uFlag & FILEF_SHARE_UALL) >> FILEF_SHARE_UMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_GALL) >> FILEF_SHARE_GMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_OALL) >> FILEF_SHARE_OMASK);
        nMode |= (Int)S_IWUSR;
    }
    if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
    {
        XChar szTemp[LMT_KEY] = { 0 };
        CXChar::Copy(szTemp, LMT_KEY, TF("/tmp/CREEK_XXXXXX"));
        m_hFile = _tmkstemp(szTemp);
        _tunlink(szTemp);
    }
    else
    {
        m_hFile = _topen(pszFile, nFlag, nMode);
    }
    if (m_hFile != HANDLE_INVALID)
    {
        if ((uFlag & FILEF_EXT_NOBUF) == 0)
        {
            m_stBufSize = DEF::Maxmin<size_t>((size_t)WRITE_BUF_MINSIZE, m_stBufSize, (size_t)WRITE_BUF_MAXSIZE);
            m_stBufSize = DEF::Align<size_t>(m_stBufSize, (size_t)WRITE_BUF_MINSIZE) - MEM_CHUNK_OFFSET;
            assert(m_pBuf == nullptr);
            m_pBuf = reinterpret_cast<PByte>( ALLOC( m_stBufSize ) );
            if (m_pBuf != nullptr)
            {
                return RET_OKAY;
            }
        }
        else
        {
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)errno;
    if (uFlag & FILEF_EXT_TEMP)
    {
        DEV_DEBUG(TF("file write create or open temp return : %d"), uRet);
    }
    else
    {
        DEV_DEBUG(TF("file write create or open %s with flag = %x, mode = %x return : %d"), pszFile, nFlag, nMode, uRet);
    }
    return uRet;
}

INLINE const CFileHandle& CFileWriteStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CFileWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CAFileReadStream
INLINE CAFileReadStream::CAFileReadStream(void)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_pHandler(nullptr)
{
}

INLINE CAFileReadStream::~CAFileReadStream(void)
{
    Close();
}

INLINE CAFileReadStream::CAFileReadStream(const CAFileReadStream&)
: CStream(STREAMM_READ | STREAMM_FILE)
, m_stSize(0)
, m_pHandler(nullptr)
{
}

INLINE CAFileReadStream& CAFileReadStream::operator=(const CAFileReadStream&)
{
    return (*this);
}

INLINE size_t CAFileReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file aio read stream out buf invalid"));
        return RET_FAIL;
    }
    if ((m_hFile.IsValid() == false) || IsEnd())
    {
        DEV_DEBUG(TF("file aio read stream invalid handle or EOF"));
        return RET_FAIL;
    }
    m_Attr.aio_buf    = pV;
    m_Attr.aio_nbytes = stLenBytes;
    if (aio_read(&m_Attr) != RET_OKAY)
    {
        return (size_t)errno;
    }
    return RET_OKAY;
}

INLINE size_t CAFileReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CAFileReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_Attr.aio_offset = DEF::Min<off_t>((off_t)skPos, (off_t)m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((off_t)skPos < m_Attr.aio_offset)
                    {
                        m_Attr.aio_offset -= (off_t)skPos;
                    }
                    else
                    {
                        m_Attr.aio_offset = 0;
                    }
                }
                else if ((off_t)skPos <= ((off_t)m_stSize - m_Attr.aio_offset))
                {
                    m_Attr.aio_offset += (off_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_Attr.aio_offset = (off_t)m_stSize - (off_t)skPos;
                    }
                }
            }
            break;
        default:{}
        }
        return (size_t)(m_Attr.aio_offset);
    }
    return 0;
}

INLINE void CAFileReadStream::Close(void)
{
    Cancel();
    m_hFile.Close();

    m_stSize   = 0;
    m_pHandler = nullptr;
    MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
    CStream::Close();
}

INLINE UInt CAFileReadStream::Create(PCXStr pszFile, UInt, UInt uAttrs, AIOHANDLER pHandler, Handle hSigEvent)
{
    Close();

    Int nFlag = O_RDONLY;
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));

    m_hFile = _topen(pszFile, nFlag);
    if (m_hFile != HANDLE_INVALID)
    {
        struct stat sttAttr;
        if (fstat(m_hFile, &sttAttr) != RET_ERROR)
        {
            m_stSize = (size_t)sttAttr.st_size;

            m_pHandler = pHandler;
            MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
            m_Attr.aio_fildes = m_hFile;
            if (pHandler != nullptr)
            {
                m_Attr.aio_sigevent.sigev_notify          = SIGEV_THREAD;
                m_Attr.aio_sigevent.sigev_notify_function = pHandler;
                m_Attr.aio_sigevent.sigev_value.sival_ptr = &m_Attr;
            }
            else if (hSigEvent != HANDLE_INVALID)
            {
                m_Attr.aio_sigevent.sigev_notify          = SIGEV_SIGNAL;
                m_Attr.aio_sigevent.sigev_signo           = hSigEvent;
                m_Attr.aio_sigevent.sigev_value.sival_ptr = &m_Attr;
            }
            else
            {
                m_Attr.aio_sigevent.sigev_notify          = SIGEV_NONE;
            }
            return RET_OKAY;
        }
    }

    SetError();
    UInt uRet = (UInt)errno;
    DEV_DEBUG(TF("file aio read create or open %s with flag = %x return : %d"), pszFile, nFlag, uRet);
    return uRet;
}

INLINE bool CAFileReadStream::Wait(UInt uWait, bool bWaitCompleted)
{
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        Int nRet = RET_OKAY;
        do
        {
            nRet = aio_error(&m_Attr);
            if (nRet != EINPROGRESS)
            {
                break;
            }
            if (uWait > 0)
            {
                CPlatform::SleepEx(uWait);
            }
        } while (bWaitCompleted);
        if (nRet == RET_OKAY)
        {
            ssize_t sstRet = aio_return(&m_Attr);
            DEV_DEBUG(TF("file aio read result size : %d"), sstRet);
            return (sstRet > 0);
        }
        DEV_DEBUG(TF("file aio read result return=%d, errno=%d"), nRet, errno);
    }
    return false;
}

INLINE Int CAFileReadStream::Result(size_t& stTransferred, bool bWaitCompleted, UInt uWait)
{
    stTransferred = 0;
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        Int nRet = RET_OKAY;
        do
        {
            nRet = aio_error(&m_Attr);
            if (nRet != EINPROGRESS)
            {
                break;
            }
            if (uWait > 0)
            {
                CPlatform::SleepEx(uWait);
            }
        } while (bWaitCompleted);
        if (nRet == RET_OKAY)
        {
            stTransferred = (size_t)aio_return(&m_Attr);
            DEV_DEBUG(TF("file aio read result size : %d"), stTransferred);
            if (stTransferred > 0)
            {
                return RET_OKAY;
            }
            nRet = RET_ERROR;
        }
        if (nRet == RET_ERROR)
        {
            nRet = errno;
        }
        DEV_DEBUG(TF("file aio read result return errno=%d"), nRet);
        return nRet;
    }
    return RET_FAIL;
}

INLINE Int CAFileReadStream::Cancel(void)
{
    if (aio_error(&m_Attr) == EINPROGRESS)
    {
        return aio_cancel(m_hFile, &m_Attr);
    }
    return RET_OKAY;
}

INLINE const AIOATTR& CAFileReadStream::GetAttr(void) const
{
    return m_Attr;
}

INLINE const CFileHandle& CAFileReadStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CAFileReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CAFileWriteStream
INLINE CAFileWriteStream::CAFileWriteStream(void)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_pHandler(nullptr)
{
}

INLINE CAFileWriteStream::~CAFileWriteStream(void)
{
    Close();
}

INLINE CAFileWriteStream::CAFileWriteStream(const CAFileWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_FILE)
, m_pHandler(nullptr)
{
}

INLINE CAFileWriteStream& CAFileWriteStream::operator=(const CAFileWriteStream&)
{
    return (*this);
}

INLINE size_t CAFileWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("file aio write stream in buf invalid"));
        return RET_FAIL;
    }
    if ((m_hFile.IsValid() == false) || IsError())
    {
        DEV_DEBUG(TF("file aio write stream invalid handle or error"));
        return RET_FAIL;
    }
    m_Attr.aio_buf    = const_cast<void*>(pV);
    m_Attr.aio_nbytes = stLenBytes;
    if (aio_write(&m_Attr) != RET_OKAY)
    {
        return (size_t)errno;
    }
    return RET_OKAY;
}

INLINE size_t CAFileWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert(eFrom < SEEKO_BOUND);
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_Attr.aio_offset = DEF::Min<off_t>((off_t)skPos, (off_t)m_Attr.aio_offset);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos > 0)
                {
                    m_Attr.aio_offset += (off_t)skPos;
                }
                else if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((off_t)skPos < m_Attr.aio_offset)
                    {
                        m_Attr.aio_offset -= (off_t)skPos;
                    }
                    else
                    {
                        m_Attr.aio_offset = 0;
                    }
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((off_t)skPos <= m_Attr.aio_offset)
                    {
                        m_Attr.aio_offset -= (off_t)skPos;
                    }
                }
            }
            break;
        default:{}
        }
        return (size_t)(m_Attr.aio_offset);
    }
    return 0;
}

INLINE bool CAFileWriteStream::Flush(Int nFlag)
{
    if (m_hFile.IsValid())
    {
        if (nFlag != FLUSHO_BUF)
        {
            return (fsync(m_hFile) != RET_ERROR);
        }
        return true;
    }
    return false;
}

INLINE void CAFileWriteStream::Close(void)
{
    Cancel();
    Flush();
    m_hFile.Close();

    m_pHandler = nullptr;
    MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));

    CStream::Close();
}

INLINE UInt CAFileWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs, AIOHANDLER pHandler, Handle hSigEvent)
{
    Close();

    Int nFlag = (Int)(uFlag & FILEF_NEW_OPEN_MASK);
    nFlag |= (Int)(uFlag & FILEF_ACS_MASK);
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));
    nFlag |= O_WRONLY;

    Int nMode = ACCESSPERMS;
    if (nFlag & O_CREAT)
    {
        nMode  = (Int)((uFlag & FILEF_SHARE_UALL) >> FILEF_SHARE_UMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_GALL) >> FILEF_SHARE_GMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_OALL) >> FILEF_SHARE_OMASK);
        nMode |= (Int)S_IWUSR;
    }
    if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
    {
        XChar szTemp[LMT_KEY] = { 0 };
        CXChar::Copy(szTemp, LMT_KEY, TF("/tmp/CREEK_XXXXXX"));
        m_hFile = _tmkstemp(szTemp);
        _tunlink(szTemp);
    }
    else
    {
        m_hFile = _topen(pszFile, nFlag, nMode);
    }
    if (m_hFile != HANDLE_INVALID)
    {
        m_pHandler = pHandler;
        MM_SAFE::Set(&m_Attr, 0, sizeof(AIOATTR));
        m_Attr.aio_fildes = m_hFile;
        if (pHandler != nullptr)
        {
            m_Attr.aio_sigevent.sigev_notify          = SIGEV_THREAD;
            m_Attr.aio_sigevent.sigev_notify_function = pHandler;
            m_Attr.aio_sigevent.sigev_value.sival_ptr = &m_Attr;
        }
        else if (hSigEvent != HANDLE_INVALID)
        {
            m_Attr.aio_sigevent.sigev_notify          = SIGEV_SIGNAL;
            m_Attr.aio_sigevent.sigev_signo           = hSigEvent;
            m_Attr.aio_sigevent.sigev_value.sival_ptr = &m_Attr;
        }
        else
        {
            m_Attr.aio_sigevent.sigev_notify          = SIGEV_NONE;
        }
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)errno;
    if (uFlag & FILEF_EXT_TEMP)
    {
        DEV_DEBUG(TF("file aio write create or open temp return : %d"), uRet);
    }
    else
    {
        DEV_DEBUG(TF("file aio write create or open %s with flag = %x, mode = %x return : %d"), pszFile, nFlag, nMode, uRet);
    }
    return uRet;
}

INLINE bool CAFileWriteStream::Wait(UInt uWait, bool bWaitCompleted)
{
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        Int nRet = RET_OKAY;
        do
        {
            nRet = aio_error(&m_Attr);
            if (nRet != EINPROGRESS)
            {
                break;
            }
            if (uWait > 0)
            {
                CPlatform::SleepEx(uWait);
            }
        } while (bWaitCompleted);
        if (nRet == RET_OKAY)
        {
            ssize_t sstRet = aio_return(&m_Attr);
            DEV_DEBUG(TF("file aio write result size : %d"), sstRet);
            return (sstRet > 0);
        }
        DEV_DEBUG(TF("file aio write result return=%d, errno=%d"), nRet, errno);
    }
    return false;
}

INLINE Int CAFileWriteStream::Result(size_t& stTransferred, bool bWaitCompleted, UInt uWait)
{
    stTransferred = 0;
    if ((m_hFile.IsValid()) && (IsError() == false))
    {
        Int nRet = RET_OKAY;
        do
        {
            nRet = aio_error(&m_Attr);
            if (nRet != EINPROGRESS)
            {
                break;
            }
            if (uWait > 0)
            {
                CPlatform::SleepEx(uWait);
            }
        } while (bWaitCompleted);
        if (nRet == RET_OKAY)
        {
            stTransferred = (size_t)aio_return(&m_Attr);
            DEV_DEBUG(TF("file aio write result size : %d"), stTransferred);
            if (stTransferred > 0)
            {
                return RET_OKAY;
            }
            nRet = RET_ERROR;
        }
        if (nRet == RET_ERROR)
        {
            nRet = errno;
        }
        DEV_DEBUG(TF("file aio write result return errno=%d"), nRet);
        return nRet;
    }
    return RET_FAIL;
}

INLINE Int CAFileWriteStream::Cancel(void)
{
    if (aio_error(&m_Attr) == EINPROGRESS)
    {
        return aio_cancel(m_hFile, &m_Attr);
    }
    return RET_OKAY;
}

INLINE const AIOATTR& CAFileWriteStream::GetAttr(void) const
{
    return m_Attr;
}

INLINE const CFileHandle& CAFileWriteStream::GetFileHandle(void) const
{
    return (m_hFile);
}

INLINE bool CAFileWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CMapReadStream
INLINE CMapReadStream::CMapReadStream(Int nMode)
: CStream(STREAMM_READ | (nMode & STREAMM_FILE) | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapReadStream::~CMapReadStream(void)
{
    Close();
}

INLINE CMapReadStream::CMapReadStream(const CMapReadStream&)
: CStream(STREAMM_READ | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapReadStream& CMapReadStream::operator=(const CMapReadStream&)
{
    return (*this);
}

INLINE size_t CMapReadStream::Read(void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("map read stream out buf invalid"));
        return 0;
    }
    if ((m_pBuf == nullptr) || IsEnd())
    {
        DEV_DEBUG(TF("map read stream invalid handle or EOF"));
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy(pV, (size_t)stLenBytes, (m_pBuf + m_stPos), (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE size_t CMapReadStream::Tell(void) const
{
    return m_stPos;
}

INLINE size_t CMapReadStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CMapReadStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CMapReadStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE void CMapReadStream::Close(void)
{
    ExitMap();
    m_hFile.Close();
    if (m_hMapping.IsEmpty() == false)
    {
        _tshm_unlink(*m_hMapping);
        m_hMapping.Empty();
    }
    CStream::Close();
}

INLINE UInt CMapReadStream::Create(PCXStr pszFile, UInt, UInt uAttrs)
{
    Close();

    Int nFlag = O_RDONLY;
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));
    if (GetMode() & STREAMM_FILE)
    {
        m_hFile = _topen(pszFile, nFlag);
    }
    else
    {
        m_hFile = _tshm_open(pszFile, nFlag, ACCESSPERMS);
        m_hMapping = pszFile;
    }
    if (m_hFile.IsValid())
    {
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)errno;
    if (GetMode() & STREAMM_FILE)
    {
        DEV_DEBUG(TF("map read stream create or open %s with flag = %x return : %d"), pszFile, nFlag, uRet);
    }
    else
    {
        DEV_DEBUG(TF("map read stream create or open shm %s with flag = %x, mode = %x return : %d"), pszFile, nFlag, ACCESSPERMS, uRet);
    }
    return uRet;
}

INLINE PByte CMapReadStream::InitMap(size_t stSize, size_t stOffset, UInt uProtect, UInt uAttrs, PCXStr)
{
    ExitMap();
    if (m_hFile.IsValid() && (m_pBuf == nullptr))
    {
        if (stSize == 0)
        {
            struct stat sttAttr;
            if (fstat(m_hFile, &sttAttr) != RET_ERROR)
            {
                stSize = sttAttr.st_size;
            }
        }
        if (ftruncate(m_hFile, (off_t)stSize) != RET_ERROR)
        {
            if (uProtect == 0)
            {
                uProtect = PROT_READ;
            }
            if (uAttrs == 0)
            {
                uAttrs = MAP_SHARED;
            }
            stOffset = DEF::Align<size_t>(stOffset, (size_t)CPlatform::GetPageSize());
            m_pBuf = reinterpret_cast<PByte>(mmap(nullptr, stSize, (Int)uProtect, (Int)uAttrs, HANDLE_INVALID, (off_t)stOffset));
            if (m_pBuf != nullptr)
            {
                m_stSize = stSize;
            }
            else
            {
                SetError();
            }
        }
        else
        {
            DEV_DEBUG(TF("file map read truncate length failed : %d"), errno);
        }
    }
    return (m_pBuf);
}

INLINE void CMapReadStream::ExitMap(void)
{
    if (m_pBuf != nullptr)
    {
        munmap(m_pBuf, m_stSize);
        m_pBuf = nullptr;
    }
    m_stSize = 0;
    m_stPos  = 0;
}

INLINE const PByte CMapReadStream::GetMap(void) const
{
    return (m_pBuf);
}

INLINE bool CMapReadStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

///////////////////////////////////////////////////////////////////
// CMapWriteStream
INLINE CMapWriteStream::CMapWriteStream(Int nMode)
: CStream(STREAMM_WRITE | (nMode & STREAMM_FILE) | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapWriteStream::~CMapWriteStream(void)
{
    Close();
}

INLINE CMapWriteStream::CMapWriteStream(const CMapWriteStream&)
: CStream(STREAMM_WRITE | STREAMM_MAPPING)
, m_stSize(0)
, m_stPos(0)
, m_pBuf(nullptr)
{
}

INLINE CMapWriteStream& CMapWriteStream::operator=(const CMapWriteStream&)
{
    return (*this);
}

INLINE size_t CMapWriteStream::Write(const void* pV, size_t stLenBytes)
{
    assert(stLenBytes > 0);
    assert(pV != nullptr);
    if ((pV == nullptr) || (stLenBytes == 0))
    {
        DEV_DEBUG(TF("map write stream out buf invalid"));
        return 0;
    }
    if ((m_pBuf == nullptr) || IsError())
    {
        DEV_DEBUG(TF("map write stream invalid handle or error"));
        return 0;
    }
    stLenBytes = DEF::Min<size_t>(stLenBytes, (m_stSize - m_stPos));
    MM_SAFE::Cpy((m_pBuf + m_stPos), (size_t)stLenBytes, pV, (size_t)stLenBytes);
    m_stPos += stLenBytes;
    return stLenBytes;
}

INLINE size_t CMapWriteStream::Tell(void) const
{
    return m_stPos;
}

INLINE size_t CMapWriteStream::Size(void) const
{
    return m_stSize;
}

INLINE size_t CMapWriteStream::Rest(void) const
{
    return (m_stSize - m_stPos);
}

INLINE size_t CMapWriteStream::Seek(SeekPos skPos, SEEK_OP eFrom)
{
    assert((size_t)DEF::Abs<SeekPos>(skPos) <= m_stSize);
    assert(eFrom < SEEKO_BOUND);
    if (m_stSize > 0)
    {
        switch (eFrom)
        {
        case SEEKO_BEGIN:
            {
                if (skPos >= 0)
                {
                    m_stPos = DEF::Min<size_t>((size_t)skPos, m_stSize);
                }
            }
            break;
        case SEEKO_CURRENT:
            {
                if (skPos < 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos < m_stPos)
                    {
                        m_stPos -= (size_t)skPos;
                    }
                    else
                    {
                        m_stPos  = 0;
                    }
                }
                else if ((size_t)skPos <= (m_stSize - m_stPos))
                {
                    m_stPos += (size_t)skPos;
                }
            }
            break;
        case SEEKO_END:
            {
                if (skPos <= 0)
                {
                    skPos = DEF::Abs<SeekPos>(skPos);
                    if ((size_t)skPos <= m_stSize)
                    {
                        m_stPos = m_stSize - (size_t)skPos;
                    }
                }
            }
            break;
        default:
            {
                assert(0);
            }
        }
    }
    return m_stPos;
}

INLINE bool CMapWriteStream::Flush(Int nFlag)
{
    if (m_pBuf != nullptr)
    {
        if (nFlag != FLUSHO_BUF)
        {
            return (msync(m_pBuf, m_stSize, nFlag) != RET_ERROR);
        }
    }
    return false;
}

INLINE void CMapWriteStream::Close(void)
{
    ExitMap();
    m_hFile.Close();
    if (m_hMapping.IsEmpty() == false)
    {
        _tshm_unlink(*m_hMapping);
        m_hMapping.Empty();
    }
    CStream::Close();
}

INLINE UInt CMapWriteStream::Create(PCXStr pszFile, UInt uFlag, UInt uAttrs)
{
    Close();

    Int nFlag = (Int)(uFlag & FILEF_NEW_OPEN_MASK);
    nFlag |= (Int)(uFlag & FILEF_ACS_MASK);
    nFlag |= (Int)(uAttrs & (O_NOCTTY|O_NONBLOCK|O_SYNC|O_ASYNC|
                             O_DIRECTORY|O_NOFOLLOW|O_CLOEXEC|O_DIRECT|
                             O_NOATIME|O_PATH|O_DSYNC|O_LARGEFILE));
    nFlag |= O_WRONLY;

    Int nMode = ACCESSPERMS;
    if (nFlag & O_CREAT)
    {
        nMode  = (Int)((uFlag & FILEF_SHARE_UALL) >> FILEF_SHARE_UMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_GALL) >> FILEF_SHARE_GMASK);
        nMode |= (Int)((uFlag & FILEF_SHARE_OALL) >> FILEF_SHARE_OMASK);
        nMode |= (Int)S_IWUSR;
    }
    if (GetMode() & STREAMM_FILE)
    {
        if ((pszFile == nullptr) && (uFlag & FILEF_EXT_TEMP))
        {
            XChar szTemp[LMT_KEY] = { 0 };
            CXChar::Copy(szTemp, LMT_KEY, TF("/tmp/CREEK_XXXXXX"));
            m_hFile = _tmkstemp(szTemp);
            _tunlink(szTemp);
            //DEV_DEBUG(TF("file map write create or open temp return : %d"), errno);

        }
        else
        {
            m_hFile = _topen(pszFile, nFlag, nMode);
            //DEV_DEBUG(TF("file map write create or open return : %d"), errno);
        }
    }
    else
    {
        m_hFile = _tshm_open(pszFile, nFlag, nMode);
        m_hMapping = pszFile;
    }
    if (m_hFile.IsValid())
    {
        return RET_OKAY;
    }

    SetError();
    UInt uRet = (UInt)errno;
    if (GetMode() & STREAMM_FILE)
    {
        if ((uFlag & FILEF_EXT_TEMP) != 0)
        {
            DEV_DEBUG(TF("map write stream create or open temp return : %d"), uRet);
        }
        else
        {
            DEV_DEBUG(TF("map write stream create or open %s with flag = %x, mode = %x return : %d"), pszFile, nFlag, nMode, uRet);
        }
    }
    else
    {
        DEV_DEBUG(TF("map write stream create or open shm %s with flag = %x, mode = %x return : %d"), pszFile, nFlag, nMode, uRet);
    }
    return uRet;
}

INLINE PByte CMapWriteStream::InitMap(size_t stSize, size_t stOffset, UInt uProtect, UInt uAttrs, PCXStr)
{
    ExitMap();
    if (m_hFile.IsValid() && (m_pBuf == nullptr))
    {
        if (stSize == 0)
        {
            struct stat sttAttr;
            if (fstat(m_hFile, &sttAttr) != RET_ERROR)
            {
                stSize = sttAttr.st_size;
            }
        }
        if (ftruncate(m_hFile, (off_t)stSize) != RET_ERROR)
        {
            if (uProtect == 0)
            {
                uProtect = PROT_WRITE;
            }
            if (uAttrs == 0)
            {
                uAttrs = MAP_SHARED;
            }
            stOffset = DEF::Align<size_t>(stOffset, (size_t)CPlatform::GetPageSize());
            m_pBuf = reinterpret_cast<PByte>(mmap(nullptr, stSize, (Int)uProtect, (Int)uAttrs, m_hFile, (off_t)stOffset));
            if (m_pBuf != nullptr)
            {
                m_stSize = stSize;
            }
            else
            {
                SetError();
            }
        }
        else
        {
            DEV_DEBUG(TF("file map write truncate length failed : %d"), errno);
        }
    }
    return (m_pBuf);
}

INLINE void CMapWriteStream::ExitMap(void)
{
    if (m_pBuf != nullptr)
    {
        munmap(m_pBuf, m_stSize);
        m_pBuf = nullptr;
    }
    m_stSize = 0;
    m_stPos  = 0;
}

INLINE PByte const CMapWriteStream::GetMap(void) const
{
    return (m_pBuf);
}

INLINE bool CMapWriteStream::IsValid(void) const
{
    return m_hFile.IsValid();
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_STREAM_FILE_INL__
