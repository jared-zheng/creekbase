// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_HANDLE_H__
#define __TARGET_HANDLE_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "mobject.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CFileHandle :
class CFileHandle : public MObject
{
public:
    CFileHandle(Handle h = HANDLE_INVALID);
    ~CFileHandle(void);

    CFileHandle(const CFileHandle& aSrc);
    CFileHandle& operator=(const CFileHandle& aSrc);
    CFileHandle& operator=(Handle h);

    bool   operator==(Handle h) const;
    bool   operator!=(Handle h) const;
    bool   operator==(const CFileHandle& aSrc) const;
    bool   operator!=(const CFileHandle& aSrc) const;

    operator Handle(void) const;

    bool     IsValid(void) const;

    void     Attach(Handle h);
    Handle   Detach(void);

    void     Close(void);
public:
    Handle   m_Handle;
};

///////////////////////////////////////////////////////////////////
// CThreadAttr
class CThreadAttr : public MObject
{
public:
    CThreadAttr(void);
    ~CThreadAttr(void);

    void   Reset(void);
private:
    CThreadAttr(const CThreadAttr& aSrc);
    CThreadAttr& operator=(const CThreadAttr& aSrc);
public:
    pthread_attr_t   m_Attr;
};

///////////////////////////////////////////////////////////////////
// CThreadHandle
class CThreadHandle : public MObject
{
public:
    CThreadHandle(TId Id = 0);
    ~CThreadHandle(void);

    CThreadHandle(const CThreadHandle& aSrc);
    CThreadHandle& operator=(const CThreadHandle& aSrc);
    CThreadHandle& operator=(TId Id);

    bool   operator==(const TId Id) const;
    bool   operator!=(const TId Id) const;
    bool   operator==(const CThreadHandle& aSrc) const;
    bool   operator!=(const CThreadHandle& aSrc) const;

    operator TId(void) const;

    bool     IsValid(void) const;

    void     Attach(TId Id);
    TId      Detach(void);

    void     Close(void);
    void     Reset(void);
public:
    TId   m_TId;
};

///////////////////////////////////////////////////////////////////
// module: dynamic load, link with -ldl
class CModuleHandle : public MObject
{
public:
    CModuleHandle(Module m = nullptr);
    ~CModuleHandle(void);

    CModuleHandle(const CModuleHandle& aSrc);
    CModuleHandle& operator=(const CModuleHandle& aSrc);
    CModuleHandle& operator=(Module m);

    bool   operator==(Module m) const;
    bool   operator!=(Module m) const;
    bool   operator==(const CModuleHandle& aSrc) const;
    bool   operator!=(const CModuleHandle& aSrc) const;

    operator Module(void) const;

    bool     IsValid(void) const;

    void     Attach(Module m);
    Module   Detach(void);

    void     Close(void);

    void*    Find(PCStr pszSymbol);
    bool     Load(PCXStr pszPath, Int nFlag = RTLD_LAZY);
    PCXStr   Error(void);
public:
    Module   m_Module;
};

///////////////////////////////////////////////////////////////////
// pipe open return pipe pid
class CProcessHandle : public MObject
{
public:
    static PId GetParentId(void);
    static PId GetGroupId(PId pid = 0);
public:
    CProcessHandle(void);
    ~CProcessHandle(void);
#ifndef __MODERN_CXX_NOT_SUPPORTED
    CProcessHandle(CProcessHandle&& aSrc);
    CProcessHandle& operator=(CProcessHandle&& aSrc);
#endif

    bool     operator==(PId pid) const;
    bool     operator!=(PId pid) const;

    operator PId(void) const;
    FILE*    Get(void) const;

    bool     IsValid(void) const;
    bool     Check(void) const;

    void     Attach(CProcessHandle& aSrc);
    void     Detach(void);

    Int      Wait(Int nOption = 0) const;

    bool     Open(PCXStr pszPath, XChar* const pszArgs[] = nullptr, Int nFlag = HANDLE_INVALID);
    void     Close(void);
private:
    CProcessHandle(const CProcessHandle& aSrc);
    CProcessHandle& operator=(const CProcessHandle& aSrc);
public:
    PId      m_PId;
    FILE*    m_pFILE;
};

///////////////////////////////////////////////////////////////////
#include "targethandle.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_HANDLE_H__
