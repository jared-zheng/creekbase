// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_UID_GEN_H__
#define __TARGET_UID_GEN_H__

#pragma once

#include "uid.h"

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include <uuid/uuid.h>

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CUUIDGenerator
// source code url : https://sourceforge.net/projects/libuuid/
// centos install :
// sudo yum install e2fsprogs-devel
// sudo yum install uuid-devel
// sudo yum install libuuid-devel
// ubuntu install :
// sudo apt-get install uuid-dev
class NOVTABLE CUUIDGenerator ABSTRACT : public MObject
{
public:
    static bool Create(CUUID& uuid);
    static bool Create(CString& strUUID, bool bAppend = false, CUUID::UUID_FROMAT eFormat = CUUID::UUID_FROMAT_DIGITS_BRACES);
private:
    CUUIDGenerator(void);
    ~CUUIDGenerator(void);
    CUUIDGenerator(const CUUIDGenerator& aSrc);
    CUUIDGenerator& operator=(const CUUIDGenerator& aSrc);
};

///////////////////////////////////////////////////////////////////
#include "targetuidgen.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_UID_GEN_H__