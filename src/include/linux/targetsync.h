// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_SYNC_H__
#define __TARGET_SYNC_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#include "atomics.h"
#include "handle.h"

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
// CSyncTraits
class CSyncTraits
{
public:
    enum SYNC_TYPE
    {
        SYNCT_COUNTER = 1,
        SYNCT_LOCK,
        SYNCT_MUTEX,
        SYNCT_EVENT,
        SYNCT_SEMA,
        SYNCT_SPINLOCK,
    };
};

typedef pthread_mutexattr_t     LockAttr;
typedef pthread_mutexattr_t     MutexAttr;
typedef pthread_condattr_t      ConditionAttr;
typedef pthread_mutexattr_t     EventAttr;
typedef bool                    SemaAttr;
typedef pthread_barrierattr_t   BarrierAttr;

typedef pthread_rwlockattr_t    RWLockAttr;
typedef struct sigevent         TimerAttr;

///////////////////////////////////////////////////////////////////
// CSyncBase
class NOVTABLE CSyncBase ABSTRACT : public MObject
{
public:
    virtual Int  Signal(void) PURE;
    virtual Int  Reset(void)  PURE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) PURE;
    virtual bool IsValid(void) const PURE;
    virtual void Close(void) PURE;

protected:
    CSyncBase(void);
    virtual ~CSyncBase(void);

    CSyncBase(const CSyncBase&);
    CSyncBase& operator=(const CSyncBase&);
};

///////////////////////////////////////////////////////////////////
// CTSyncBase
template <Int nType>
class NOVTABLE CTSyncBase ABSTRACT : public CSyncBase
{
public:
    Int GetType(void) const;
protected:
    CTSyncBase(void);
    virtual ~CTSyncBase(void);

    CTSyncBase(const CTSyncBase&);
    CTSyncBase& operator=(const CTSyncBase&);
};

///////////////////////////////////////////////////////////////////
// CTSyncScope
// This is a utility class that handles scope level locking. It's very useful
// to keep from causing deadlocks due to exceptions being caught and knowing
// about the number of locks a given thread has on a resource. Example:
//
// <code>
//    {
//        // Syncronize thread access to the following data
//        CTSyncScope Scope(SynchObject);
//        // Access data that is shared among multiple threads
//        ...
//        // When Scope goes out of scope, other threads can access data
//    }
// </code>
//
template <Int nType, bool bWait = false>
class CTSyncScope : public MObject
{
public:
    explicit CTSyncScope(CTSyncBase<nType>& Sync, UInt uWait = (UInt)TIMET_INFINITE);
    ~CTSyncScope(void);

    Int GetRet(void) const;
private:
    CTSyncScope(const CTSyncScope&);
    CTSyncScope& operator=(const CTSyncScope&);
private:
    CTSyncBase<nType>*   m_pSync;
    Int                  m_nRet;
};

typedef CTSyncScope<CSyncTraits::SYNCT_COUNTER>        CSyncCounterScope;
typedef CTSyncScope<CSyncTraits::SYNCT_LOCK>           CSyncLockScope;
typedef CTSyncScope<CSyncTraits::SYNCT_MUTEX>          CSyncMutexScope;
typedef CTSyncScope<CSyncTraits::SYNCT_EVENT>          CSyncEventScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SEMA>           CSyncSemaScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SPINLOCK>       CSyncSpinlockScope;
typedef CTSyncScope<CSyncTraits::SYNCT_COUNTER,  true> CSyncCounterWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_LOCK,     true> CSyncLockWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_MUTEX,    true> CSyncMutexWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_EVENT,    true> CSyncEventWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SEMA,     true> CSyncSemaWaitScope;
typedef CTSyncScope<CSyncTraits::SYNCT_SPINLOCK, true> CSyncSpinlockWaitScope;

///////////////////////////////////////////////////////////////////
// CSyncCounter
class CSyncCounter : public CTSyncBase<CSyncTraits::SYNCT_COUNTER>
{
public:
    CSyncCounter(Int nCounter = 0);
    virtual ~CSyncCounter(void);

    virtual Int  Signal(void) OVERRIDE; // return m_nCounter after increment
    virtual Int  Reset(void) OVERRIDE;  // return m_nCounter after decrement
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE; // RET_OKAY, wait always return okay
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    Int     GetCount(void) const;
public:
    CSyncCounter(const CSyncCounter&);
    CSyncCounter& operator=(const CSyncCounter&);
private:
    Int   m_nCounter;
};

///////////////////////////////////////////////////////////////////
// CSyncLock : PTHREAD_PROCESS_PRIVATE & PTHREAD_MUTEX_RECURSIVE mutex
class CSyncLock : public CTSyncBase<CSyncTraits::SYNCT_LOCK>
{
    friend class CCondition;
public:
    CSyncLock(bool bCreate = true, LockAttr* pAttr = nullptr);
    virtual ~CSyncLock(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE; // TIMET_IGNORE return RET_FAIL if other thread owned lock
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(LockAttr* pAttr = nullptr);
private:
    CSyncLock(const CSyncLock&);
    CSyncLock& operator=(const CSyncLock&);
private:
    Int               m_nErr;
    pthread_mutex_t   m_Lock;
};

///////////////////////////////////////////////////////////////////
// CSyncMutex
class CSyncMutex : public CTSyncBase<CSyncTraits::SYNCT_MUTEX>
{
    friend class CCondition;
public:
    CSyncMutex(bool bCreate = true, bool bCreateSignal = false, MutexAttr* pAttr = nullptr);
    virtual ~CSyncMutex(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(bool bCreateSignal = false, MutexAttr* pAttr = nullptr);
private:
    CSyncMutex(const CSyncMutex&);
    CSyncMutex& operator=(const CSyncMutex&);
private:
    Int               m_nErr;
    volatile bool     m_bOwned;
    pthread_mutex_t   m_Mutex;
};

///////////////////////////////////////////////////////////////////
// CCondition :
class CCondition : public MObject
{
public:
    CCondition(bool bCreate = true, ConditionAttr* pAttr = nullptr);
    ~CCondition(void);

    bool Wait(CSyncLock&  Lock, UInt uWait = (UInt)TIMET_INFINITE);
    bool Wait(CSyncMutex& Mutex, UInt uWait = (UInt)TIMET_INFINITE);

    bool Wake(bool bAll = false);
    bool IsValid(void) const;

    void Close(void);
    bool Open(ConditionAttr* pAttr = nullptr);
private:
    CCondition(const CCondition&);
    CCondition& operator=(const CCondition&);

    bool Wait(pthread_mutex_t* pMutex, UInt uWait = (UInt)TIMET_INFINITE);
private:
    Int              m_nErr;
    pthread_cond_t   m_Cond;
};

///////////////////////////////////////////////////////////////////
// CSyncEvent
class CSyncEvent : public CTSyncBase<CSyncTraits::SYNCT_EVENT>
{
public:
    CSyncEvent(bool bCreate = true, bool bCreateSignal = false, bool bManualReset = false, Int npshare = PTHREAD_PROCESS_PRIVATE, EventAttr* pAttr = nullptr);
    virtual ~CSyncEvent(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(bool bCreateSignal = false, bool bManualReset = false, Int npshare = PTHREAD_PROCESS_PRIVATE, EventAttr* pAttr = nullptr);
private:
    CSyncEvent(const CSyncEvent&);
    CSyncEvent& operator=(const CSyncEvent&);
private:
    Int               m_nErr;
    bool              m_bSignal;
    bool              m_bManual;
    pthread_mutex_t   m_Mutex;
    pthread_cond_t    m_Cond;
};

///////////////////////////////////////////////////////////////////
// CSyncSema
class CSyncSema : public CTSyncBase<CSyncTraits::SYNCT_SEMA>
{
public:
    CSyncSema(bool bCreate = true, Int nSignalCount = 0, PCXStr pszName = nullptr, SemaAttr Attr = false); // length of pszName < LMT_KEY
    virtual ~CSyncSema(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    // The uWait argument is specifies an absolute timeout in seconds and nanoseconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC)
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE;
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(Int nSignalCount = 0, PCXStr pszName = nullptr, SemaAttr Attr = false);
private:
    CSyncSema(const CSyncSema&);
    CSyncSema& operator=(const CSyncSema&);
private:
    Int      m_nErr;
    sem_t*   m_pSemaphore;
    sem_t    m_Semaphore;
    XChar    m_szName[LMT_KEY];
};

///////////////////////////////////////////////////////////////////
// CSpinLock :
class CSpinLock : public CTSyncBase<CSyncTraits::SYNCT_SPINLOCK>
{
public:
    CSpinLock(bool bCreate = true, Int npshare = PTHREAD_PROCESS_PRIVATE);
    ~CSpinLock(void);

    virtual Int  Signal(void) OVERRIDE;
    virtual Int  Reset(void) OVERRIDE;
    virtual Int  Wait(UInt uWait = (UInt)TIMET_INFINITE, bool bAlert = false) OVERRIDE; // RET_OKAY, wait always return okay
    virtual bool IsValid(void) const OVERRIDE;
    virtual void Close(void) OVERRIDE;

    bool    Open(Int npshare = PTHREAD_PROCESS_PRIVATE);
private:
    CSpinLock(const CSpinLock&);
    CSpinLock& operator=(const CSpinLock&);
private:
    Int                  m_nErr;
    pthread_spinlock_t   m_Lock;
};

///////////////////////////////////////////////////////////////////
// CSyncBarrier
class CSyncBarrier : MObject
{
public:
    CSyncBarrier(bool bCreate = true, Int nCount = 1, BarrierAttr* pAttr = nullptr);
    ~CSyncBarrier(void);

    bool Wait(void);
    bool IsValid(void) const;

    void Close(void);
    bool Open(Int nCount = 1, BarrierAttr* pAttr = nullptr);
private:
    CSyncBarrier(const CSyncBarrier&);
    CSyncBarrier& operator=(const CSyncBarrier&);
private:
    Int                 m_nErr;
    pthread_barrier_t   m_Barrier;
};

///////////////////////////////////////////////////////////////////
// CRWLock :
class CRWLock : public MObject
{
public:
    CRWLock(bool bCreate = true, bool bManual = false, RWLockAttr* pAttr = nullptr);
    ~CRWLock(void);

    bool LockRead(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockRead(void);

    bool LockWrite(UInt uWait = (UInt)TIMET_INFINITE);
    void UnlockWrite(void);

    bool IsValid(void) const;

    void Close(void);
    bool Open(bool bManual = false, RWLockAttr* pAttr = nullptr);
private:
    CRWLock(const CRWLock&);
    CRWLock& operator=(const CRWLock&);
private:
    Int                m_nErr;
    pthread_rwlock_t   m_RWLock;
};

///////////////////////////////////////////////////////////////////
// CTRWLockScope
template <bool bRead = true>
class CTRWLockScope : public MObject
{
public:
    explicit CTRWLockScope(CRWLock& RWLock, UInt uWait = (UInt)TIMET_INFINITE);
    ~CTRWLockScope(void);
private:
    CTRWLockScope(const CTRWLockScope&);
    CTRWLockScope& operator=(const CTRWLockScope&);
private:
    CRWLock*   m_pRWLock;
};

typedef CTRWLockScope<true>  CRWLockReadScope;
typedef CTRWLockScope<false> CRWLockWriteScope;

///////////////////////////////////////////////////////////////////
// CPosixTimer
class CPosixTimer : public MObject
{
public:
    CPosixTimer(bool bCreate = true, clockid_t tType = CLOCK_REALTIME, TimerAttr* pAttr = nullptr);
    ~CPosixTimer(void);

    Int  SetTimer(const struct itimerspec* tValue, Int nFlags = 0, struct itimerspec* tOld = nullptr);
    Int  CancelTimer(void);

    Int  GetCurTime(struct itimerspec* tValue);
    Int  GetOverrun(void);

    bool IsValid(void) const;

    bool CreateTimer(clockid_t tType = CLOCK_REALTIME, TimerAttr* pAttr = nullptr);
    void CloseTimer(void);
private:
    CPosixTimer(const CPosixTimer&);
    CPosixTimer& operator=(const CPosixTimer&);
private:
    Int       m_nErr;
    timer_t   m_Timer;
};

///////////////////////////////////////////////////////////////////
#include "targetsync.inl"

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_SYNC_H__
