// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_SYNC_INL__
#define __TARGET_SYNC_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CSyncBase
INLINE CSyncBase::CSyncBase(void)
{
}

INLINE CSyncBase::~CSyncBase(void)
{
}

INLINE CSyncBase::CSyncBase(const CSyncBase&)
{
}

INLINE CSyncBase& CSyncBase::operator=(const CSyncBase&)
{
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CTSyncBase
template <Int nType>
INLINE CTSyncBase<nType>::CTSyncBase(void)
{
}

template <Int nType>
INLINE CTSyncBase<nType>::~CTSyncBase(void)
{
}

template <Int nType>
INLINE CTSyncBase<nType>::CTSyncBase(const CTSyncBase<nType>& aSrc)
: CSyncBase(aSrc)
{
}

template <Int nType>
INLINE CTSyncBase<nType>& CTSyncBase<nType>::operator=(const CTSyncBase<nType>& aSrc)
{
    if (this != &aSrc)
    {
        CSyncBase::operator=(aSrc);
    }
    return (*this);
}

template <Int nType>
INLINE Int CTSyncBase<nType>::GetType(void) const
{
    return nType;
}

///////////////////////////////////////////////////////////////////
// CTSyncScope
template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::CTSyncScope(CTSyncBase<nType>& Sync, UInt uWait)
: m_pSync(&Sync)
, m_nRet(RET_OKAY)
{
    assert(m_pSync != nullptr);
    if (bWait == false)
    {
        m_nRet = m_pSync->Signal();
    }
    else
    {
        m_nRet = m_pSync->Wait(uWait);
    }
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::~CTSyncScope(void)
{
    assert(m_pSync != nullptr);
    if (nType != CSyncTraits::SYNCT_COUNTER)
    {
        if (m_nRet == RET_OKAY)
        {
            m_pSync->Reset();
        }
    }
    else if (m_nRet > 0)
    {
        m_pSync->Reset();
    }
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>::CTSyncScope(const CTSyncScope<nType, bWait>&)
: m_pSync(nullptr)
, m_nRet(RET_FAIL)
{
}

template <Int nType, bool bWait>
INLINE CTSyncScope<nType, bWait>& CTSyncScope<nType, bWait>::operator=(const CTSyncScope<nType, bWait>&)
{
    return (*this);
}

template <Int nType, bool bWait>
INLINE Int CTSyncScope<nType, bWait>::GetRet(void) const
{
    return m_nRet;
}

///////////////////////////////////////////////////////////////////
// CSyncCounter
INLINE CSyncCounter::CSyncCounter(Int nCounter)
: m_nCounter(nCounter)
{
}

INLINE CSyncCounter::~CSyncCounter(void)
{
}

INLINE CSyncCounter::CSyncCounter(const CSyncCounter& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_COUNTER>(aSrc)
, m_nCounter(0)
{
}

INLINE CSyncCounter& CSyncCounter::operator=(const CSyncCounter& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_COUNTER>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncCounter::Signal(void)
{
    return (Int)CAtomics::Increment<Int>(&m_nCounter);
}

INLINE Int CSyncCounter::Reset(void)
{
    return (Int)CAtomics::Decrement<Int>(&m_nCounter);
}

INLINE Int CSyncCounter::Wait(UInt uWait, bool bAlert)
{
    CPlatform::SleepEx(uWait, bAlert);
    return (Int)RET_OKAY;
}

INLINE bool CSyncCounter::IsValid(void) const
{
    return (m_nCounter >= 0);
}

INLINE void CSyncCounter::Close(void)
{
    m_nCounter = 0;
}

INLINE Int CSyncCounter::GetCount(void) const
{
    return m_nCounter;
}

///////////////////////////////////////////////////////////////////
// CSyncLock
INLINE CSyncLock::CSyncLock(bool bCreate, LockAttr* pAttr)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        Open(pAttr);
    }
}

INLINE CSyncLock::~CSyncLock(void)
{
    Close();
}

INLINE CSyncLock::CSyncLock(const CSyncLock& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_LOCK>(aSrc)
, m_nErr(RET_ERROR)
{
}

INLINE CSyncLock& CSyncLock::operator=(const CSyncLock& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_LOCK>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncLock::Signal(void)
{
    assert(IsValid());
    Int nRet = pthread_mutex_lock(&m_Lock);
    if (nRet == EOWNERDEAD)
    {
        pthread_mutex_consistent(&m_Lock);
    }
    return nRet;
}

INLINE Int CSyncLock::Reset(void)
{
    assert(IsValid());
    return pthread_mutex_unlock(&m_Lock);
}

INLINE Int CSyncLock::Wait(UInt uWait, bool)
{
    assert(IsValid());
    if (uWait == (UInt)TIMET_IGNORE)
    {
        return (pthread_mutex_trylock(&m_Lock) == RET_OKAY) ? (Int)RET_OKAY : (Int)RET_FAIL;
    }
    return Signal();
}

INLINE bool CSyncLock::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSyncLock::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_mutex_destroy(&m_Lock);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSyncLock::Open(LockAttr* pAttr)
{
    Close();
    if (pAttr != nullptr)
    {
        pthread_mutexattr_setpshared(pAttr, PTHREAD_PROCESS_PRIVATE);
        pthread_mutexattr_settype(pAttr, PTHREAD_MUTEX_RECURSIVE);
        m_nErr = pthread_mutex_init(&m_Lock, pAttr);
    }
    else
    {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        m_nErr = pthread_mutex_init(&m_Lock, &attr);
        pthread_mutexattr_destroy(&attr);
    }
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncMutex
INLINE CSyncMutex::CSyncMutex(bool bCreate, bool bCreateSignal, MutexAttr* pAttr)
: m_nErr(RET_ERROR)
, m_bOwned(false)
{
    if (bCreate)
    {
        Open(bCreateSignal, pAttr);
    }
}

INLINE CSyncMutex::~CSyncMutex(void)
{
    Close();
}

INLINE CSyncMutex::CSyncMutex(const CSyncMutex& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_MUTEX>(aSrc)
, m_nErr(RET_ERROR)
, m_bOwned(false)
{
}

INLINE CSyncMutex& CSyncMutex::operator=(const CSyncMutex& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_MUTEX>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncMutex::Signal(void)
{
    assert(IsValid());
    if (m_bOwned == false)
    {
        Int nRet = pthread_mutex_lock(&m_Mutex);
        if (nRet == EOWNERDEAD)
        {
            pthread_mutex_consistent(&m_Mutex);
        }
        m_bOwned = (nRet == RET_OKAY);
        if (nRet == EDEADLK)
        {
            m_bOwned = true;
        }
        return nRet;
    }
    return (Int)RET_OKAY;
}

INLINE Int CSyncMutex::Reset(void)
{
    assert(IsValid());
    if (m_bOwned)
    {
        Int nRet = pthread_mutex_unlock(&m_Mutex);
        m_bOwned = false;
        return nRet;
    }
    return (Int)RET_OKAY;
}

INLINE Int CSyncMutex::Wait(UInt uWait, bool)
{
    assert(IsValid());
    if (m_bOwned == false)
    {
        if (uWait == (UInt)TIMET_IGNORE)
        {
            if (pthread_mutex_trylock(&m_Mutex) == RET_OKAY)
            {
                m_bOwned = true;
                return (Int)RET_OKAY;
            }
            return (Int)RET_FAIL;
        }
        return Signal();
    }
    return (Int)RET_OKAY;
}

INLINE bool CSyncMutex::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSyncMutex::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_mutex_destroy(&m_Mutex);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSyncMutex::Open(bool bCreateSignal, MutexAttr* pAttr)
{
    Close();
    if (pAttr != nullptr)
    {
        pthread_mutexattr_settype(pAttr, PTHREAD_MUTEX_ERRORCHECK);
        m_nErr = pthread_mutex_init(&m_Mutex, pAttr);
    }
    else
    {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
        m_nErr = pthread_mutex_init(&m_Mutex, &attr);
        pthread_mutexattr_destroy(&attr);
    }
    if ((m_nErr == RET_OKAY) && (bCreateSignal == true))
    {
        Signal();
    }
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CCondition
INLINE CCondition::CCondition(bool bCreate, ConditionAttr* pAttr)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        Open(pAttr);
    }
}

INLINE CCondition::~CCondition(void)
{
    Close();
}

INLINE CCondition::CCondition(const CCondition&)
: m_nErr(RET_ERROR)
{
}

INLINE CCondition& CCondition::operator=(const CCondition&)
{
    return (*this);
}

INLINE bool CCondition::Wait(CSyncLock& Lock, UInt uWait)
{
    assert(IsValid());
    return Wait(&(Lock.m_Lock), uWait);
}

INLINE bool CCondition::Wait(CSyncMutex& Mutex, UInt uWait)
{
    assert(IsValid());
    return Wait(&(Mutex.m_Mutex), uWait);
}

INLINE bool CCondition::Wake(bool bAll)
{
    assert(IsValid());
    if (bAll)
    {
        return (pthread_cond_broadcast(&m_Cond) == RET_OKAY);
    }
    else
    {
        return (pthread_cond_signal(&m_Cond) == RET_OKAY);
    }
}

INLINE bool CCondition::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE bool CCondition::Wait(pthread_mutex_t* pMutex, UInt uWait)
{
    assert(IsValid());
    if (uWait == (UInt)TIMET_INFINITE)
    {
        return (pthread_cond_wait(&m_Cond, pMutex) == RET_OKAY);
    }
    else
    {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);

        ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
        ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
        if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
        {
            ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
            ts.tv_sec  += 1;
        }
        return (pthread_cond_timedwait(&m_Cond, pMutex, &ts) == RET_OKAY);
    }
}

INLINE void CCondition::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_cond_destroy(&m_Cond);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CCondition::Open(ConditionAttr* pAttr)
{
    Close();
    m_nErr = pthread_cond_init(&m_Cond, pAttr);
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncEvent
INLINE CSyncEvent::CSyncEvent(bool bCreate, bool bCreateSignal, bool bManualReset, Int npshare, EventAttr* pAttr)
: m_nErr(RET_ERROR)
, m_bSignal(false)
, m_bManual(false)
{
    if (bCreate)
    {
        Open(bCreateSignal, bManualReset, npshare, pAttr);
    }
}

INLINE CSyncEvent::~CSyncEvent(void)
{
    Close();
}

INLINE CSyncEvent::CSyncEvent(const CSyncEvent& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_EVENT>(aSrc)
, m_nErr(RET_ERROR)
, m_bSignal(false)
, m_bManual(false)
{
}

INLINE CSyncEvent& CSyncEvent::operator=(const CSyncEvent& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_EVENT>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncEvent::Signal(void)
{
    assert(IsValid());
    Int nRet = pthread_mutex_lock(&m_Mutex);
    if (nRet == EOWNERDEAD)
    {
        pthread_mutex_consistent(&m_Mutex);
    }
    if ((nRet == RET_OKAY) || (nRet == EDEADLK))
    {
        if (m_bSignal == false)
        {
            if (m_bManual)
            {
                nRet = pthread_cond_broadcast(&m_Cond);
            }
            else
            {
                nRet = pthread_cond_signal(&m_Cond);
            }
            m_bSignal = (nRet == RET_OKAY);
         }
        else
        {
            nRet = RET_OKAY;
        }
        pthread_mutex_unlock(&m_Mutex);
    }
    return nRet;
}

INLINE Int CSyncEvent::Reset(void)
{
    assert(IsValid());
    Int nRet = pthread_mutex_lock(&m_Mutex);
    if (nRet == EOWNERDEAD)
    {
        pthread_mutex_consistent(&m_Mutex);
    }
    if ((nRet == RET_OKAY) || (nRet == EDEADLK))
    {
        m_bSignal = false;
        nRet = pthread_mutex_unlock(&m_Mutex);
    }
    return nRet;
}

INLINE Int CSyncEvent::Wait(UInt uWait, bool)
{
    assert(IsValid());
    Int nRet = pthread_mutex_lock(&m_Mutex);
    if (nRet == EOWNERDEAD)
    {
        pthread_mutex_consistent(&m_Mutex);
    }
    if ((nRet == RET_OKAY) || (nRet == EDEADLK))
    {
        if (m_bSignal == false)
        {
            if (uWait == (UInt)TIMET_INFINITE)
            {
                nRet = pthread_cond_wait(&m_Cond, &m_Mutex);
            }
            else
            {
                struct timespec ts;
                clock_gettime(CLOCK_REALTIME, &ts);

                ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
                ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
                if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
                {
                    ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
                    ts.tv_sec  += 1;
                }
                nRet = pthread_cond_timedwait(&m_Cond, &m_Mutex, &ts);
            }
        }
        else
        {
            nRet = RET_OKAY;
        }
        if (nRet == RET_OKAY)
        {
            m_bSignal = m_bManual;
        }
        pthread_mutex_unlock(&m_Mutex);
    }
    return nRet;
}

INLINE bool CSyncEvent::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSyncEvent::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_mutex_destroy(&m_Mutex);
        pthread_cond_destroy(&m_Cond);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSyncEvent::Open(bool bCreateSignal, bool bManualReset, Int npshare, EventAttr* pAttr)
{
    Close();
    if (pAttr != nullptr)
    {
        pthread_mutexattr_setpshared(pAttr, npshare);
        pthread_mutexattr_settype(pAttr, PTHREAD_MUTEX_ERRORCHECK);
        m_nErr = pthread_mutex_init(&m_Mutex, pAttr);
    }
    else
    {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_setpshared(&attr, npshare);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
        m_nErr = pthread_mutex_init(&m_Mutex, &attr);
        pthread_mutexattr_destroy(&attr);
    }
    if (m_nErr == RET_OKAY)
    {
        pthread_condattr_t attr;
        pthread_condattr_init(&attr);
        pthread_condattr_setpshared(&attr, npshare);
        m_nErr = pthread_cond_init(&m_Cond, &attr);
        pthread_condattr_destroy(&attr);
    }
    if (m_nErr == RET_OKAY)
    {
        m_bManual = bManualReset;
        if (bCreateSignal == true)
        {
            Signal();
        }
    }
    else
    {
        pthread_mutex_destroy(&m_Mutex);
    }
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncSema
INLINE CSyncSema::CSyncSema(bool bCreate, Int nSignalCount, PCXStr pszName, SemaAttr Attr)
: m_nErr(RET_ERROR)
, m_pSemaphore(nullptr)
{
    if (bCreate)
    {
        Open(nSignalCount, pszName, Attr);
    }
}

INLINE CSyncSema::~CSyncSema(void)
{
    Close();
}

INLINE CSyncSema::CSyncSema(const CSyncSema& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_SEMA>(aSrc)
, m_nErr(RET_ERROR)
, m_pSemaphore(nullptr)
{
}

INLINE CSyncSema& CSyncSema::operator=(const CSyncSema& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_SEMA>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSyncSema::Signal(void)
{
    assert(IsValid());
    if (m_pSemaphore != nullptr)
    {
        return sem_post(m_pSemaphore);
    }
    else
    {
        return sem_post(&m_Semaphore);
    }
}

INLINE Int CSyncSema::Reset(void)
{
    return (Int)RET_FAIL;
}

INLINE Int CSyncSema::Wait(UInt uWait, bool)
{
    assert(IsValid());
    sem_t* pSemaphore = (m_pSemaphore != nullptr) ? m_pSemaphore : &m_Semaphore;

    Int nRet = sem_trywait(pSemaphore);
    if (nRet == RET_OKAY)
    {
        return RET_OKAY;
    }
    if (uWait == (UInt)TIMET_INFINITE)
    {
        nRet = sem_wait(pSemaphore);
    }
    else
    {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);

        ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
        ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
        if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
        {
            ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
            ts.tv_sec  += 1;
        }
        nRet = sem_timedwait(pSemaphore, &ts);
    }
    return nRet;
}

INLINE bool CSyncSema::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSyncSema::Close(void)
{
    if (m_pSemaphore != nullptr)
    {
        sem_close(m_pSemaphore);
        m_pSemaphore = nullptr;
        _tsem_unlink(m_szName);
    }
    else if (m_nErr == RET_OKAY)
    {
        sem_destroy(&m_Semaphore);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSyncSema::Open(Int nSignalCount, PCXStr pszName, SemaAttr Attr)
{
    Close();
    if (pszName != nullptr)
    {
        size_t stLen = _txnlen(pszName, LMT_KEY);
        _txncpy(m_szName, pszName, (LMT_KEY - 1));
        m_szName[stLen - 1] = 0;

        m_pSemaphore = _tsem_open(m_szName, (Attr ? O_CREAT|O_EXCL : O_CREAT), DEFFILEMODE, (UInt)nSignalCount);
        if (m_pSemaphore == SEM_FAILED)
        {
            m_pSemaphore = nullptr;
            m_nErr = errno;
        }
        else
        {
            m_nErr = RET_OKAY;
        }
    }
    else
    {
        m_nErr = sem_init(&m_Semaphore, PTHREAD_PROCESS_PRIVATE, (UInt)nSignalCount);
        m_szName[0] = 0;
    }
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CSpinLock
INLINE CSpinLock::CSpinLock(bool bCreate, Int npshare)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        Open(npshare);
    }
}

INLINE CSpinLock::~CSpinLock(void)
{
    Close();
}

INLINE CSpinLock::CSpinLock(const CSpinLock& aSrc)
: CTSyncBase<CSyncTraits::SYNCT_SPINLOCK>(aSrc)
, m_nErr(RET_ERROR)
{
}

INLINE CSpinLock& CSpinLock::operator=(const CSpinLock& aSrc)
{
    if (this != &aSrc)
    {
        CTSyncBase<CSyncTraits::SYNCT_SPINLOCK>::operator=(aSrc);
    }
    return (*this);
}

INLINE Int CSpinLock::Signal(void)
{
    assert(IsValid());
    return pthread_spin_lock(&m_Lock);
}

INLINE Int CSpinLock::Reset(void)
{
    assert(IsValid());
    return pthread_spin_unlock(&m_Lock);
}

INLINE Int CSpinLock::Wait(UInt uWait, bool)
{
    assert(IsValid());
    if (uWait == (UInt)TIMET_IGNORE)
    {
        return (pthread_spin_trylock(&m_Lock) == RET_OKAY) ? (Int)RET_OKAY : (Int)RET_FAIL;
    }
    return Signal();
}

INLINE bool CSpinLock::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSpinLock::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_spin_destroy(&m_Lock);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSpinLock::Open(Int npshare)
{
    Close();
    m_nErr = pthread_spin_init(&m_Lock, npshare);
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CSyncBarrier
INLINE CSyncBarrier::CSyncBarrier(bool bCreate, Int nCount, BarrierAttr* pAttr)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        Open(nCount, pAttr);
    }
}

INLINE CSyncBarrier::~CSyncBarrier(void)
{
    Close();
}

INLINE CSyncBarrier::CSyncBarrier(const CSyncBarrier&)
: m_nErr(RET_ERROR)
{
}

INLINE CSyncBarrier& CSyncBarrier::operator=(const CSyncBarrier&)
{
    return (*this);
}

INLINE bool CSyncBarrier::Wait(void)
{
    assert(IsValid());
    return (pthread_barrier_wait(&m_Barrier) == RET_OKAY);
}

INLINE bool CSyncBarrier::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CSyncBarrier::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_barrier_destroy(&m_Barrier);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CSyncBarrier::Open(Int nCount, BarrierAttr* pAttr)
{
    Close();
    m_nErr = pthread_barrier_init(&m_Barrier, pAttr, nCount);
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CRWLock
INLINE CRWLock::CRWLock(bool bCreate, bool bManual, RWLockAttr* pAttr)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        Open(bManual, pAttr);
    }
}

INLINE CRWLock::~CRWLock(void)
{
    Close();
}

INLINE CRWLock::CRWLock(const CRWLock&)
: m_nErr(RET_ERROR)
{
}

INLINE CRWLock& CRWLock::operator=(const CRWLock&)
{
    return (*this);
}

INLINE bool CRWLock::LockRead(UInt uWait)
{
    assert(IsValid());
    if (pthread_rwlock_tryrdlock(&m_RWLock) == RET_OKAY)
    {
        return true;
    }
    if (uWait == (UInt)TIMET_INFINITE)
    {
        return (pthread_rwlock_rdlock(&m_RWLock) == RET_OKAY);
    }
    else
    {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);

        ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
        ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
        if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
        {
            ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
            ts.tv_sec  += 1;
        }
        return (pthread_rwlock_timedrdlock(&m_RWLock, &ts) == RET_OKAY);
    }
}

INLINE void CRWLock::UnlockRead(void)
{
    assert(IsValid());
    pthread_rwlock_unlock(&m_RWLock);
}

INLINE bool CRWLock::LockWrite(UInt uWait)
{
    assert(IsValid());
    if (pthread_rwlock_trywrlock(&m_RWLock) == RET_OKAY)
    {
        return true;
    }
    if (uWait == (UInt)TIMET_INFINITE)
    {
        return (pthread_rwlock_wrlock(&m_RWLock) == RET_OKAY);
    }
    else
    {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);

        ts.tv_sec  += (time_t)(uWait / (UInt)TIMET_S2MS);
        ts.tv_nsec += (__syscall_slong_t)(uWait % TIMET_S2MS) * TIMET_NS2MS;
        if (ts.tv_nsec >= (__syscall_slong_t)TIMET_NS2SEC)
        {
            ts.tv_nsec -= (__syscall_slong_t)TIMET_NS2SEC;
            ts.tv_sec  += 1;
        }
        return (pthread_rwlock_timedwrlock(&m_RWLock, &ts) == RET_OKAY);
    }
}

INLINE void CRWLock::UnlockWrite(void)
{
    assert(IsValid());
    pthread_rwlock_unlock(&m_RWLock);
}

INLINE bool CRWLock::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE void CRWLock::Close(void)
{
    if (m_nErr == RET_OKAY)
    {
        pthread_rwlock_destroy(&m_RWLock);
    }
    m_nErr = RET_ERROR;
}

INLINE bool CRWLock::Open(bool, RWLockAttr* pAttr)
{
    Close();
    m_nErr = pthread_rwlock_init(&m_RWLock, pAttr);
    return IsValid();
}

///////////////////////////////////////////////////////////////////
// CTRWLockScope
template <bool bRead>
INLINE CTRWLockScope<bRead>::CTRWLockScope(CRWLock& RWLock, UInt uWait)
: m_pRWLock(&RWLock)
{
    assert(m_pRWLock != nullptr);
    if (bRead == true)
    {
        m_pRWLock->LockRead(uWait);
    }
    else
    {
        m_pRWLock->LockWrite(uWait);
    }
}

template <bool bRead>
INLINE CTRWLockScope<bRead>::~CTRWLockScope(void)
{
    assert(m_pRWLock != nullptr);
    if (bRead == true)
    {
        m_pRWLock->UnlockRead();
    }
    else
    {
        m_pRWLock->UnlockWrite();
    }
}

template <bool bRead>
INLINE CTRWLockScope<bRead>::CTRWLockScope(const CTRWLockScope<bRead>&)
{
}

template <bool bRead>
INLINE CTRWLockScope<bRead>& CTRWLockScope<bRead>::operator=(const CTRWLockScope<bRead>&)
{
    return (*this);
}

///////////////////////////////////////////////////////////////////
// CPosixTimer
INLINE CPosixTimer::CPosixTimer(bool bCreate, clockid_t tType, TimerAttr* pAttr)
: m_nErr(RET_ERROR)
{
    if (bCreate)
    {
        CreateTimer(tType, pAttr);
    }
}

INLINE CPosixTimer::~CPosixTimer(void)
{
    CloseTimer();
}

INLINE CPosixTimer::CPosixTimer(const CPosixTimer&)
: m_nErr(RET_ERROR)
{
}

INLINE CPosixTimer& CPosixTimer::operator=(const CPosixTimer&)
{
    return (*this);
}

INLINE Int CPosixTimer::SetTimer(const struct itimerspec* tValue, Int nFlags, struct itimerspec* tOld)
{
    assert(IsValid());
    Int nRet = timer_settime(m_Timer, nFlags, tValue, tOld);
    if (nRet == RET_ERROR)
    {
        nRet = errno;
    }
    return nRet;
}

INLINE Int CPosixTimer::CancelTimer(void)
{
    Int nRet = RET_OKAY;
    if (m_nErr == RET_OKAY)
    {
        nRet = timer_delete(m_Timer);
    }
    m_nErr = RET_ERROR;
    if (nRet == RET_ERROR)
    {
        nRet = errno;
    }
    return nRet;
}

INLINE Int CPosixTimer::GetCurTime(struct itimerspec* tValue)
{
    assert(IsValid());
    Int nRet = timer_gettime(m_Timer, tValue);
    if (nRet == RET_ERROR)
    {
        nRet = errno;
    }
    return nRet;
}

INLINE Int CPosixTimer::GetOverrun(void)
{
    assert(IsValid());
    Int nRet = timer_getoverrun(m_Timer);
    if (nRet == RET_ERROR)
    {
        nRet = errno;
    }
    return nRet;
}

INLINE bool CPosixTimer::IsValid(void) const
{
    return (m_nErr == RET_OKAY);
}

INLINE bool CPosixTimer::CreateTimer(clockid_t tType, TimerAttr* pAttr)
{
    CloseTimer();
    m_nErr = timer_create(tType, pAttr, &m_Timer);
    if (m_nErr == RET_ERROR)
    {
        m_nErr = errno;
    }
    return IsValid();
}

INLINE void CPosixTimer::CloseTimer(void)
{
    if (m_nErr == RET_OKAY)
    {
        timer_delete(m_Timer);
    }
    m_nErr = RET_ERROR;
}

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_SYNC_INL__
