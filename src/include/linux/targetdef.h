// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_DEF_H__
#define __TARGET_DEF_H__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

//////////////////////////#
namespace CREEK          //
{                        //
//////////////////////////#

///////////////////////////////////////////////////////////////////
//
#define CONCAT(x, y)                  __CONCAT__(x, y)
#define __CONCAT__(x, y)              x ## y
#define TOSTR( x )                    __STR__( x )
#define __STR__( x )                  #x
#define TF( x )                       __T__( x )
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    #define XChar                     Char
    #define __T__( x )                x

    // exception trace
    #define _tbacktrace_symbols       backtrace_symbols
    // file
    #define _topen                    open
    #define _tfopen                   fopen
    #define _tfdopen                  fdopen
    #define _tshm_open                shm_open
    #define _tshm_unlink              shm_unlink

    #define _taccess                  access
    #define _tstat                    stat
    #define _trename                  rename
    #define _tremove                  remove
    #define _tunlink                  unlink

    #define _topendir                 opendir
    #define _tmkdir                   mkdir
    #define _trmdir                   rmdir
    #define _tfnmatch                 fnmatch

    #define _fgetts                   fgets
    #define _fputts                   fputs
    #define _tfprintf                 fprintf
    #define _tvfprintf                vfprintf

    #define _tgetcwd                  getcwd
    #define _treadlink                readlink

    #define _tpopen                   popen
    #define _tmkfifo                  mkfifo
    // sem
    #define _tsem_open                sem_open
    #define _tsem_unlink              sem_unlink
    // string
    #define _stscanf                  sscanf
    #define _txnlen                   strnlen
    #define _txncpy                   strncpy
    #define _txftime                  strftime
    #define _tmkstemp                 mkstemp
    #define _tsetlocale               setlocale
    // dynamic load
    #define _tdlopen                  dlopen
    #define _tdlerror                 dlerror
    #define _tdlsym                   dlsym
    //
    #define _texecl                   execl
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    #define XChar                     WChar
    #define __T__( x )                L ## x

    #warning "make sure linux native API support wchar_t string!"
    // file
    #define _fgetts                   fgetws
    #define _fputts                   fputws
    #define _tfprintf                 fwprintf
    #define _tvfprintf                vfwprintf
    // string
    #define _stscanf                  swscanf
    #define _txnlen                   wcsnlen
    #define _txncpy                   wcsncpy
    #define _txftime                  wcsftime
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    #error "make sure linux native API support utf-16 string!"
#else
    #error "unknown runtime charset"
#endif

#define HANDLE_INVALID                (-1)
#define SEEKTO_INVALID                (-1)
#define INDEX_INVALID                 (-1)

#define CP_DEFAULT                    nullptr
#define CP_UTF8                       ""

#define E_OUTOFMEMORY                 ENOMEM
#define E_ACCESS_VIOLATION            EACCES
#define E_ARRAY_BOUNDS_EXCEEDED       EINVAL

#define ALLOC_TYPE_NORMAL             0
#define ALLOC_TYPE_EXECUTE            1

#define UNREFERENCED_PARAMETER(P)     (P)
#define _TRUNCATE                     ((size_t)-1)
#define FOLDER_ACS                    (S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)
#define ALLRW_ACS                     (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

#define FALSE                         0
#define TRUE                          1

#define TRet                          void*
#define TParam                        void*
#define THREAD_FUNC

#define READ_HANDLE                   0
#define WRITE_HANDLE                  1
#define PIPE_HANDLE_COUNT             2

///////////////////////////////////////////////////////////////////
// CPlatform::SetExceptHandler
// linux : CPlatform sigaction for SIGSEGV handle;
//         SetExceptHandler(true) return RET_OKAY->sigsetjmp & sigaction okay
//         SetExceptHandler(true) return RET_FAIL->SIGSEGV signal & siglongjmp
//         SetExceptHandler(true) return RET_ERROR->failed to set
//         SetExceptHandler(false) to cancel
#define SET_EXCEPTION()               (CPlatform::SetExceptHandler(true))
#define UNSET_EXCEPTION()             (CPlatform::SetExceptHandler(false))
#define TRY_EXCEPTION                 \
if (SET_EXCEPTION() == RET_OKAY)      \
{

#define CATCH_EXCEPTION               \
    UNSET_EXCEPTION();                \
}                                     \
else                                  \
{

#define END_EXCEPTION                 \
}

///////////////////////////////////////////////////////////////////
//
typedef   int                              Handle,   *PHandle;
typedef   void*                            Module,   *PModule;
typedef   pid_t                            PId,      *PPId;
typedef   pthread_t                        TId,      *PTId;
typedef   const XChar*                     CodePage, *PCodePage;
typedef   off_t                            SeekPos,  *PSeekPos;
typedef   UInt                             SizeLen,  *PSizeLen;
typedef   bool                             EXCEPT_HANDLER;
typedef   struct tagUUID
 {
    UInt   Data1;
    UShort Data2;
    UShort Data3;
    UChar  Data4[8];
}                                          UID,      *PUID;

typedef   struct stat                      FILEATTR, *PFILEATTR;
typedef   struct stat                      FILEFIND, *PFILEFIND;

typedef   struct aiocb                     AIOATTR,  *PAIOATTR;
typedef   void  (*AIOHANDLER)(sigval_t);

///////////////////////////////////////////////////////////////////
// limit length
enum LIMIT_LEN
{
    LMT_KEY              = 64,
    LMT_MIN              = 128,
    LMT_MAX_NAME         = 256,
    LMT_BUF              = 1024,
    LMT_MAX_PATH         = PATH_MAX,
    LMT_MAX              = UINT_MAX,
};

enum SEEK_OP
{
    SEEKO_BEGIN          = 0x00000000,
    SEEKO_CURRENT        = 0x00000001,
    SEEKO_END            = 0x00000002,
    SEEKO_BOUND          = 0x00000003,
};

enum FLUSH_OP
{
    FLUSHO_BUF           = 0,                 // flush buffer to file only
    FLUSHO_ASYNC         = MS_ASYNC,
    FLUSHO_INVALIDATE    = MS_INVALIDATE,
    FLUSHO_SYNC          = MS_SYNC,
    FLUSHO_DEFAULT       = FLUSHO_ASYNC,
};

enum FILE_FLAG
{
    FILEF_NEW_NOEXIST    = (O_CREAT|O_EXCL),  // linux : 0100 | 0200
    FILEF_NEW_ALWAYS     = (O_CREAT|O_TRUNC), // linux : 0100 | 01000
    FILEF_OPEN_EXIST     = 0,                 // linux : 0
    FILEF_OPEN_ALWAYS    = O_CREAT,           // linux : 0100
    FILEF_OPEN_TRUNCATE  = O_TRUNC,           // linux : 01000
    FILEF_EXT_APPEND     = O_APPEND,          // linux : 02000
    FILEF_NEW_OPEN_MASK  = (O_CREAT|O_EXCL|O_TRUNC|O_APPEND),

    FILEF_EXT_NOBUF      = 0x00004000,        // read  stream no buffering, for read once
    FILEF_EXT_TEMP       = 0x00008000,        // tmp file

    FILEF_ACS_ALL        = O_RDWR,            // linux : 02
    FILEF_ACS_EXECUTE    = 0,                 // linux : ---
    FILEF_ACS_WRITE      = O_WRONLY,          // linux : 01
    FILEF_ACS_READ       = O_RDONLY,          // linux : 00
    FILEF_ACS_MASK       = (O_RDONLY|O_WRONLY|O_RDWR),
    // linux : mode
    FILEF_SHARE_UREAD    = 0x04000000,        // linux : S_IRUSR
    FILEF_SHARE_UWRITE   = 0x02000000,        // linux : S_IWUSR
    FILEF_SHARE_UDELETE  = 0x00000000,        //
    FILEF_SHARE_UEXECUTE = 0x01000000,        // linux : S_IXUSR
    FILEF_SHARE_UALL     = 0x07000000,        // linux : S_IRWXU
    FILEF_SHARE_GREAD    = 0x00400000,        // linux : S_IRGRP
    FILEF_SHARE_GWRITE   = 0x00200000,        // linux : S_IWGRP
    FILEF_SHARE_GDELETE  = 0x00000000,        //
    FILEF_SHARE_GEXECUTE = 0x00100000,        // linux : S_IXGRP
    FILEF_SHARE_GALL     = 0x00700000,        // linux : S_IRWXG
    FILEF_SHARE_OREAD    = 0x00040000,        // linux : S_IROTH
    FILEF_SHARE_OWRITE   = 0x00020000,        // linux : S_IWOTH
    FILEF_SHARE_ODELETE  = 0x00000000,        //
    FILEF_SHARE_OEXECUTE = 0x00010000,        // linux : S_IXOTH
    FILEF_SHARE_OALL     = 0x00070000,        // linux : S_IRWXO
    FILEF_SHARE_UMASK    = 0x00000012,        // linux : >> 12
    FILEF_SHARE_GMASK    = 0x00000011,        // linux : >> 11
    FILEF_SHARE_OMASK    = 0x00000010,        // linux : >> 10
    FILEF_SHARE_USER     = (FILEF_SHARE_UREAD|FILEF_SHARE_UWRITE|FILEF_SHARE_GREAD|FILEF_SHARE_OREAD),
    FILEF_SHARE_DEFAULT  = (FILEF_SHARE_UREAD|FILEF_SHARE_UWRITE|FILEF_SHARE_GREAD|FILEF_SHARE_GWRITE|FILEF_SHARE_OREAD|FILEF_SHARE_OWRITE),
    FILEF_SHARE_ALL      = (FILEF_SHARE_UALL|FILEF_SHARE_GALL|FILEF_SHARE_OALL),
};

//////////////////////////#
} // namespace CREEK     //
//////////////////////////#

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_DEF_H__
