// Copyright (c) Ruo Creek <ruo.creek at outlook dot com>
// CREEK Base is licensed under the CREEK Source License v1.
// You can use this software according to the terms and conditions of the CREEK Source License v1.
// You may obtain a copy of CREEK Source License v1 at:
//    https://ruo-creek.github.io/CSL
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
// PURPOSE.
// See the CREEK Source License v1 for more details.
//
// Create : 2007-03-06 version 0.1 Ruo Creek <ruo.creek at outlook dot com>
// Update : 

#ifndef __TARGET_TRAITS_INL__
#define __TARGET_TRAITS_INL__

#pragma once

#if   (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

///////////////////////////////////////////////////////////////////
// CBase36
class NOVTABLE CBase36 : public MObject
{
public:
    static CPCStr  CharsTable;
    static CPCWStr WCharsTable;
};
SELECTANY CPCStr  CBase36::CharsTable  = "ZYXWVUTSRQPONMLKJIHGFEDCBA9876543210123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
SELECTANY CPCWStr CBase36::WCharsTable = L"ZYXWVUTSRQPONMLKJIHGFEDCBA9876543210123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

///////////////////////////////////////////////////////////////////
// Primitive Element Traits Define
//_DECLARE_PRIMITIVE_TRAITS( Handle ) == _DECLARE_PRIMITIVE_TRAITS( Int )
//_DECLARE_PRIMITIVE_TRAITS( Module ) == _DECLARE_PRIMITIVE_TRAITS( void* )

///////////////////////////////////////////////////////////////////
// CTXCharTraitsBase
template <typename T>
INLINE Int CTXCharTraitsBase<T>::Convert(PCStr pszC, Int, PWStr pszW, Int nSizeW, CodePage cpPage)
{
    if ((cpPage == CP_DEFAULT) ||
        (CPlatform::SetLocal(LC_ALL, cpPage) != nullptr))
    {
        assert(nSizeW > 1);
        return (Int)mbstowcs(pszW, pszC, (size_t)nSizeW);
    }
    return -1;
}

template <typename T>
INLINE Int CTXCharTraitsBase<T>::Convert(PCWStr pszW, Int, PStr pszC, Int nSizeC, CodePage cpPage)
{
    if ((cpPage == CP_DEFAULT) ||
        (CPlatform::SetLocal(LC_ALL, cpPage) != nullptr))
    {
        assert(nSizeC > 1);
        return (Int)wcstombs(pszC, pszW, (size_t)nSizeC);
    }
    return -1;
}

///////////////////////////////////////////////////////////////////
// CChar
INLINE bool CChar::IsAlnum(TChar ch)
{
    return (isalnum(ch) > 0);
}

INLINE bool CChar::IsAlpha(TChar ch)
{
    return (isalpha(ch) > 0);
}

INLINE bool CChar::IsPrint(TChar ch)
{
    return (isprint(ch) > 0);
}

INLINE bool CChar::IsGraph(TChar ch)
{
    return (isgraph(ch) > 0);
}

INLINE bool CChar::IsDigit(TChar ch)
{
    return (isdigit(ch) > 0);
}

INLINE bool CChar::IsXDigit(TChar ch)
{
    return (isxdigit(ch) > 0);
}

INLINE bool CChar::IsSpace(TChar ch)
{
    return (isspace(ch) > 0);
}

INLINE bool CChar::IsLower(TChar ch)
{
    return (islower(ch) > 0);
}

INLINE bool CChar::IsUpper(TChar ch)
{
    return (isupper(ch) > 0);
}

INLINE CChar::PTStr CChar::Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext)
{
    return strtok_r(pszStr, pszDelimit, &pszContext);
}

INLINE CChar::PTStr CChar::Rev(PTStr pszStr)
{
    // ...
    if ((pszStr != nullptr) && (*pszStr != 0))
    {
        PTStr pszHead = pszStr;
        PTStr pszTail = pszStr + Length(pszStr) - 1;
        TChar c       = 0;
        while (pszHead < pszTail)
        {
            c = *pszHead;
            *pszHead = *pszTail;
            ++pszHead;

            *pszTail = c;
            --pszTail;
        }
    }
    return pszStr;
}

INLINE CChar::PCTStr CChar::Chr(PCTStr pszStr, TChar ch)
{
    return strchr(pszStr, ch);
}

INLINE CChar::PCTStr CChar::RevChr(PCTStr pszStr, TChar ch)
{
    return strrchr(pszStr, ch);
}

INLINE CChar::PCTStr CChar::Str(PCTStr pszStr, PCTStr pszMatch)
{
    return strstr(pszStr, pszMatch);
}

INLINE CChar::PCTStr CChar::OneOf(PCTStr pszStr, PCTStr pszMatch)
{
    return strpbrk(pszStr, pszMatch);
}

INLINE size_t CChar::OneIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return strcspn(pszStr, pszMatch);
}

INLINE size_t CChar::NotIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return strspn(pszStr, pszMatch);
}

INLINE size_t CChar::Length(PCTStr pszStr, size_t stMax)
{
    return strnlen(pszStr, stMax);
}

INLINE Int CChar::Cmp(PCTStr pszA, PCTStr pszB)
{
    return strcmp(pszA, pszB);
}

INLINE Int CChar::Cmpi(PCTStr pszA, PCTStr pszB)
{
    return strcasecmp(pszA, pszB);
}

INLINE Int CChar::Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return strncmp(pszA, pszB, stSize);
}

INLINE Int CChar::Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return strncasecmp(pszA, pszB, stSize);
}

INLINE Int CChar::Coll(PCTStr pszA, PCTStr pszB)
{
    return strcoll(pszA, pszB);
}

INLINE Int CChar::Colli(PCTStr pszA, PCTStr pszB)
{
    return strcasecmp(pszA, pszB); // ...
}

INLINE Int CChar::Colln(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return strncmp(pszA, pszB, stSize); // ...
}

INLINE Int CChar::Collin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return strncasecmp(pszA, pszB, stSize); // ...
}

INLINE Int CChar::FormatLength(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatLengthV(pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CChar::FormatLengthV(PCTStr pszFormat, va_list args)
{
    va_list argstmp;
    va_copy(argstmp, args);
    Int nRet = FormatV(nullptr, 0, pszFormat, argstmp);
    va_end(argstmp);
    return nRet;
}

INLINE Int CChar::Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatV(pszBuf, stSize, pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CChar::FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args)
{
    return vsnprintf(pszBuf, stSize, pszFormat, args);
}

INLINE bool CChar::Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount)
{
    assert(pszStr != nullptr);
    assert(stSize >= stCount);
    if (stSize >= stCount)
    {
        MM_SAFE::Set(pszStr, ch, stCount * sizeof(TChar));
        return true;
    }
    return false;
}

INLINE bool CChar::Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    assert(pszDst != nullptr);
    assert(pszSrc != nullptr);
    if (stSrc == _TRUNCATE)
    {
        stSrc = stDst - 1; // 1 for '\0'
    }
    if (stDst >= stSrc)
    {
        strncpy(pszDst, pszSrc, stSrc);
        return true;
    }
    return false;
}

INLINE bool CChar::Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    assert(pszDst != nullptr);
    assert(pszSrc != nullptr);
    if (stSrc == _TRUNCATE)
    {
        stSrc = stDst - 1; // 1 for '\0'
    }
    if (stDst >= stSrc)
    {
        strncat(pszDst, pszSrc, stSrc);
        return true;
    }
    return false;
}

INLINE bool CChar::ToLower(PTStr pszStr, size_t stSize)
{
    assert(pszStr != nullptr);
    if (pszStr != nullptr)
    {
        for (size_t i = 0; (i < stSize) && (pszStr[i] != 0); ++i)
        {
            pszStr[i] = ToLower(pszStr[i]);
        }
        return true;
    }
    return false;
}

INLINE bool CChar::ToUpper(PTStr pszStr, size_t stSize)
{
    assert(pszStr != nullptr);
    if (pszStr != nullptr)
    {
        for (size_t i = 0; (i < stSize) && (pszStr[i] != 0); ++i)
        {
            pszStr[i] = ToUpper(pszStr[i]);
        }
        return true;
    }
    return false;
}

INLINE CChar::TChar CChar::ToLower(TChar ch)
{
    return (TChar)tolower(ch);
}

INLINE CChar::TChar CChar::ToUpper(TChar ch)
{
    return (TChar)toupper(ch);
}

INLINE Int CChar::ToInt(PCTStr pszStr)
{
    return atoi(pszStr);
}

INLINE Long CChar::ToLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtol(pszStr, ppszEnd, nRadix);
}

INLINE ULong CChar::ToULong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtoul(pszStr, ppszEnd, nRadix);
}

INLINE LLong CChar::ToLLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtoll(pszStr, ppszEnd, nRadix);
}

INLINE ULLong CChar::ToULLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return strtoull(pszStr, ppszEnd, nRadix);
}

INLINE Double CChar::ToDouble(PCTStr pszStr, PTStr* ppszEnd)
{
    return strtod(pszStr, ppszEnd);
}

INLINE bool CChar::ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    LLong llValue = nValue;
    return ToString(llValue, pszStr, stSize, nRadix);
}

INLINE bool CChar::ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    LLong llValue = lValue;
    return ToString(llValue, pszStr, stSize, nRadix);
}

INLINE bool CChar::ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    ULLong ullValue = ulValue;
    return ToString(ullValue, pszStr, stSize, nRadix);
}

INLINE bool CChar::ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    assert(pszStr != nullptr);
    if (pszStr == nullptr)
    {
        return false;
    }
    if ((nRadix < RADIXT_BIN) || (nRadix > RADIXT_BASE36))
    {
        *pszStr = 0;
        return false;
    }

    PTStr pszTemp = pszStr;
    LLong llTemp  = 0;
    do
    {
        llTemp   = llValue;
        llValue /= nRadix;
        *pszTemp = CBase36::CharsTable[(RADIXT_BASE36 - 1) + (llTemp - llValue * nRadix)];
        ++pszTemp;
    } while (llValue > 0);
    if (llTemp < 0)
    {
        *pszTemp = '-';
        ++pszTemp;
    }
    *pszTemp = 0;
    Rev(pszStr);
    return true;
}

INLINE bool CChar::ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    assert(pszStr != nullptr);
    if (pszStr == nullptr)
    {
        return false;
    }
    if ((nRadix < RADIXT_BIN) || (nRadix > RADIXT_BASE36))
    {
        *pszStr = 0;
        return false;
    }

    PTStr  pszTemp = pszStr;
    ULLong ullTemp = 0;
    do
    {
        ullTemp   = ullValue;
        ullValue /= nRadix;
        *pszTemp = CBase36::CharsTable[(RADIXT_BASE36 - 1) + (ullTemp - ullValue * nRadix)];
        ++pszTemp;
    } while (ullValue > 0);
    *pszTemp = 0;
    Rev(pszStr);
    return true;
}

INLINE bool CChar::ToString(Double dValue, PTStr pszStr, size_t, Int nRadix)
{
    return (gcvt(dValue, nRadix, pszStr) != nullptr);
}

///////////////////////////////////////////////////////////////////
// CMChar
INLINE bool CMChar::IsAlnum(TChar)
{
    return false;
}

INLINE bool CMChar::IsAlpha(TChar)
{
    return false;
}

INLINE bool CMChar::IsPrint(TChar)
{
    return false;
}

INLINE bool CMChar::IsGraph(TChar)
{
    return false;
}

INLINE bool CMChar::IsDigit(TChar)
{
    return false;
}

INLINE bool CMChar::IsXDigit(TChar)
{
    return false;
}

INLINE bool CMChar::IsSpace(TChar)
{
    return false;
}

INLINE bool CMChar::IsLower(TChar)
{
    return false;
}

INLINE bool CMChar::IsUpper(TChar)
{
    return false;
}

INLINE CMChar::PTStr CMChar::Tok(PTStr, PCTStr, PTStr&)
{
    return nullptr;
}

INLINE CMChar::PTStr CMChar::Rev(PTStr)
{
    return nullptr;
}

INLINE CMChar::PCTStr CMChar::Chr(PCTStr, TChar)
{
    return nullptr;
}

INLINE CMChar::PCTStr CMChar::RevChr(PCTStr, TChar)
{
    return nullptr;
}

INLINE CMChar::PCTStr CMChar::Str(PCTStr, PCTStr)
{
    return nullptr;
}

INLINE CMChar::PCTStr CMChar::OneOf(PCTStr, PCTStr)
{
    return nullptr;
}

INLINE size_t CMChar::OneIndex(PCTStr, PCTStr)
{
    return 0;
}

INLINE size_t CMChar::NotIndex(PCTStr, PCTStr)
{
    return 0;
}

INLINE size_t CMChar::Length(PCTStr, size_t)
{
    return 0;
}

INLINE Int CMChar::Cmp(PCTStr, PCTStr)
{
    return 0;
}

INLINE Int CMChar::Cmpi(PCTStr, PCTStr)
{
    return 0;
}

INLINE Int CMChar::Cmpn(PCTStr, PCTStr, size_t)
{
    return 0;
}

INLINE Int CMChar::Cmpin(PCTStr, PCTStr, size_t)
{
    return 0;
}

INLINE Int CMChar::Coll(PCTStr, PCTStr)
{
    return 0;
}

INLINE Int CMChar::Colli(PCTStr, PCTStr)
{
    return 0;
}

INLINE Int CMChar::Colln(PCTStr, PCTStr, size_t)
{
    return 0;
}

INLINE Int CMChar::Collin(PCTStr, PCTStr, size_t)
{
    return 0;
}

INLINE Int CMChar::FormatLength(PCTStr, ...)
{
    return 0;
}

INLINE Int CMChar::FormatLengthV(PCTStr, va_list)
{
    return 0;
}

INLINE Int CMChar::Format(PTStr, size_t, PCTStr, ...)
{
    return 0;
}

INLINE Int CMChar::FormatV(PTStr, size_t, PCTStr, va_list)
{
    return 0;
}

INLINE bool CMChar::Replace(PTStr, size_t, TChar, size_t)
{
    return false;
}

INLINE bool CMChar::Copy(PTStr, size_t, PCTStr, size_t)
{
    return false;
}

INLINE bool CMChar::Concat(PTStr, size_t, PCTStr, size_t)
{
    return false;
}

INLINE bool CMChar::ToLower(PTStr, size_t)
{
    return false;
}

INLINE bool CMChar::ToUpper(PTStr, size_t)
{
    return false;
}

INLINE CMChar::TChar CMChar::ToLower(TChar)
{
    return 0;
}

INLINE CMChar::TChar CMChar::ToUpper(TChar)
{
    return 0;
}

INLINE Int CMChar::ToInt(PCTStr)
{
    return 0;
}

INLINE Long CMChar::ToLong(PCTStr, PTStr*, Int)
{
    return 0;
}

INLINE ULong CMChar::ToULong(PCTStr, PTStr*, Int)
{
    return 0;
}

INLINE LLong CMChar::ToLLong(PCTStr, PTStr*, Int)
{
    return 0;
}

INLINE ULLong CMChar::ToULLong(PCTStr, PTStr*, Int)
{
    return 0;
}

INLINE Double CMChar::ToDouble(PCTStr, PTStr*)
{
    return 0;
}

INLINE bool CMChar::ToString(Int, PTStr, size_t, Int)
{
    return false;
}

INLINE bool CMChar::ToString(Long, PTStr, size_t, Int)
{
    return false;
}

INLINE bool CMChar::ToString(ULong, PTStr, size_t, Int)
{
    return false;
}

INLINE bool CMChar::ToString(LLong, PTStr, size_t, Int)
{
    return false;
}

INLINE bool CMChar::ToString(ULLong, PTStr, size_t, Int)
{
    return false;
}

INLINE bool CMChar::ToString(Double, PTStr, size_t, Int)
{
    return false;
}

///////////////////////////////////////////////////////////////////
// CWChar
INLINE bool CWChar::IsAlnum(TChar ch)
{
    return (iswalnum(ch) > 0);
}

INLINE bool CWChar::IsAlpha(TChar ch)
{
    return (iswalpha(ch) > 0);
}

INLINE bool CWChar::IsPrint(TChar ch)
{
    return (iswprint(ch) > 0);
}

INLINE bool CWChar::IsGraph(TChar ch)
{
    return (iswgraph(ch) > 0);
}

INLINE bool CWChar::IsDigit(TChar ch)
{
    return (iswdigit(ch) > 0);
}

INLINE bool CWChar::IsXDigit(TChar ch)
{
    return (iswxdigit(ch) > 0);
}

INLINE bool CWChar::IsSpace(TChar ch)
{
    return (iswspace(ch) > 0);
}

INLINE bool CWChar::IsLower(TChar ch)
{
    return (iswlower(ch) > 0);
}

INLINE bool CWChar::IsUpper(TChar ch)
{
    return (iswupper(ch) > 0);
}

INLINE CWChar::PTStr CWChar::Tok(PTStr pszStr, PCTStr pszDelimit, PTStr& pszContext)
{
    return wcstok(pszStr, pszDelimit, &pszContext);
}

INLINE CWChar::PTStr CWChar::Rev(PTStr pszStr)
{
    // ...
    if ((pszStr != nullptr) && (*pszStr != 0))
    {
        PTStr pszHead = pszStr;
        PTStr pszTail = pszStr + Length(pszStr) - 1;
        TChar c       = 0;
        while (pszHead < pszTail)
        {
            c = *pszHead;
            *pszHead = *pszTail;
            ++pszHead;

            *pszTail = c;
            --pszTail;
        }
    }
    return pszStr;
}

INLINE CWChar::PCTStr CWChar::Chr(PCTStr pszStr, TChar ch)
{
    return wcschr(pszStr, ch);
}

INLINE CWChar::PCTStr CWChar::RevChr(PCTStr pszStr, TChar ch)
{
    return wcsrchr(pszStr, ch);
}

INLINE CWChar::PCTStr CWChar::Str(PCTStr pszStr, PCTStr pszMatch)
{
    return wcsstr(pszStr, pszMatch);
}

INLINE CWChar::PCTStr CWChar::OneOf(PCTStr pszStr, PCTStr pszMatch)
{
    return wcspbrk(pszStr, pszMatch);
}

INLINE size_t CWChar::OneIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return wcscspn(pszStr, pszMatch);
}

INLINE size_t CWChar::NotIndex(PCTStr pszStr, PCTStr pszMatch)
{
    return wcsspn(pszStr, pszMatch);
}

INLINE size_t CWChar::Length(PCTStr pszStr, size_t stMax)
{
    return wcsnlen(pszStr, stMax);
}

INLINE Int CWChar::Cmp(PCTStr pszA, PCTStr pszB)
{
    return wcscmp(pszA, pszB);
}

INLINE Int CWChar::Cmpi(PCTStr pszA, PCTStr pszB)
{
    return wcscasecmp(pszA, pszB);
}

INLINE Int CWChar::Cmpn(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return wcsncmp(pszA, pszB, stSize);
}

INLINE Int CWChar::Cmpin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return wcsncasecmp(pszA, pszB, stSize);
}

INLINE Int CWChar::Coll(PCTStr pszA, PCTStr pszB)
{
    return wcscoll(pszA, pszB);
}

INLINE Int CWChar::Colli(PCTStr pszA, PCTStr pszB)
{
    return wcscasecmp(pszA, pszB); // ...
}

INLINE Int CWChar::Colln(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return wcsncmp(pszA, pszB, stSize); // ...
}

INLINE Int CWChar::Collin(PCTStr pszA, PCTStr pszB, size_t stSize)
{
    return wcsncasecmp(pszA, pszB, stSize); // ...
}

INLINE Int CWChar::FormatLength(PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatLengthV(pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CWChar::FormatLengthV(PCTStr pszFormat, va_list args)
{
    va_list argstmp;
    va_copy(argstmp, args);
    Int nRet = FormatV(nullptr, 0, pszFormat, argstmp);
    va_end(argstmp);
    return nRet;
}

INLINE Int CWChar::Format(PTStr pszBuf, size_t stSize, PCTStr pszFormat, ...)
{
    va_list vl;
    va_start(vl, pszFormat);
    Int nRet = FormatV(pszBuf, stSize, pszFormat, vl);
    va_end(vl);

    return nRet;
}

INLINE Int CWChar::FormatV(PTStr pszBuf, size_t stSize, PCTStr pszFormat, va_list args)
{
    return vswprintf(pszBuf, stSize, pszFormat, args);
}

INLINE bool CWChar::Replace(PTStr pszStr, size_t stSize, TChar ch, size_t stCount)
{
    assert(pszStr != nullptr);
    assert(stSize >= stCount);
    if (stSize >= stCount)
    {
        MM_SAFE::Set(pszStr, ch, stCount * sizeof(TChar));
        return true;
    }
    return false;
}

INLINE bool CWChar::Copy(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    assert(pszDst != nullptr);
    assert(pszSrc != nullptr);
    if (stSrc == _TRUNCATE)
    {
        stSrc = stDst - 1; // 1 for '\0'
    }
    if (stDst >= stSrc)
    {
        wcsncpy(pszDst, pszSrc, stSrc);
        return true;
    }
    return false;
}

INLINE bool CWChar::Concat(PTStr pszDst, size_t stDst, PCTStr pszSrc, size_t stSrc)
{
    assert(pszDst != nullptr);
    assert(pszSrc != nullptr);
    if (stSrc == _TRUNCATE)
    {
        stSrc = stDst - 1; // 1 for '\0'
    }
    if (stDst >= stSrc)
    {
        wcsncat(pszDst, pszSrc, stSrc);
        return true;
    }
    return false;
}

INLINE bool CWChar::ToLower(PTStr pszStr, size_t stSize)
{
    assert(pszStr != nullptr);
    if (pszStr != nullptr)
    {
        for (size_t i = 0; (i < stSize) && (pszStr[i] != 0); ++i)
        {
            pszStr[i] = ToLower(pszStr[i]);
        }
        return true;
    }
    return false;
}

INLINE bool CWChar::ToUpper(PTStr pszStr, size_t stSize)
{
    assert(pszStr != nullptr);
    if (pszStr != nullptr)
    {
        for (size_t i = 0; (i < stSize) && (pszStr[i] != 0); ++i)
        {
            pszStr[i] = ToUpper(pszStr[i]);
        }
        return true;
    }
    return false;
}

INLINE CWChar::TChar CWChar::ToLower(TChar ch)
{
    return (TChar)towlower(ch);
}

INLINE CWChar::TChar CWChar::ToUpper(TChar ch)
{
    return (TChar)towupper(ch);
}

INLINE Int CWChar::ToInt(PCTStr pszStr)
{
    return (Int)wcstol(pszStr, nullptr, RADIXT_DEC);
}

INLINE Long CWChar::ToLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstol(pszStr, ppszEnd, nRadix);
}

INLINE ULong CWChar::ToULong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstoul(pszStr, ppszEnd, nRadix);
}

INLINE LLong CWChar::ToLLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstoll(pszStr, ppszEnd, nRadix);
}

INLINE ULLong CWChar::ToULLong(PCTStr pszStr, PTStr* ppszEnd, Int nRadix)
{
    return wcstoull(pszStr, ppszEnd, nRadix);
}

INLINE Double CWChar::ToDouble(PCTStr pszStr, PTStr* ppszEnd)
{
    return wcstod(pszStr, ppszEnd);
}

INLINE bool CWChar::ToString(Int nValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    LLong llValue = nValue;
    return ToString(llValue, pszStr, stSize, nRadix);
}

INLINE bool CWChar::ToString(Long lValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    LLong llValue = lValue;
    return ToString(llValue, pszStr, stSize, nRadix);
}

INLINE bool CWChar::ToString(ULong ulValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    ULLong ullValue = ulValue;
    return ToString(ullValue, pszStr, stSize, nRadix);
}

INLINE bool CWChar::ToString(LLong llValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    assert(pszStr != nullptr);
    if (pszStr == nullptr)
    {
        return false;
    }
    if ((nRadix < RADIXT_BIN) || (nRadix > RADIXT_BASE36))
    {
        *pszStr = 0;
        return false;
    }

    PTStr pszTemp = pszStr;
    LLong llTemp  = 0;
    do
    {
        llTemp   = llValue;
        llValue /= nRadix;
        *pszTemp = CBase36::WCharsTable[(RADIXT_BASE36 - 1) + (llTemp - llValue * nRadix)];
        ++pszTemp;
    } while (llValue > 0);
    if (llTemp < 0)
    {
        *pszTemp = '-';
        ++pszTemp;
    }
    *pszTemp = 0;
    Rev(pszStr);
    return true;
}

INLINE bool CWChar::ToString(ULLong ullValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    assert(pszStr != nullptr);
    if (pszStr == nullptr)
    {
        return false;
    }
    if ((nRadix < RADIXT_BIN) || (nRadix > RADIXT_BASE36))
    {
        *pszStr = 0;
        return false;
    }

    PTStr  pszTemp = pszStr;
    ULLong ullTemp = 0;
    do
    {
        ullTemp   = ullValue;
        ullValue /= nRadix;
        *pszTemp = CBase36::WCharsTable[(RADIXT_BASE36 - 1) + (ullTemp - ullValue * nRadix)];
        ++pszTemp;
    } while (ullValue > 0);
    *pszTemp = 0;
    Rev(pszStr);
    return true;
}

INLINE bool CWChar::ToString(Double dValue, PTStr pszStr, size_t stSize, Int nRadix)
{
    Char cTemp[LMT_BUF] ={0};
    if (gcvt(dValue, nRadix, cTemp) != nullptr)
    {
        if (stSize > LMT_BUF)
        {
            stSize = LMT_BUF;
        }
        for (size_t i = 0; i < stSize; ++i)
        {
            if (cTemp[i] != 0)
            {
                pszStr[i] = (TChar)cTemp[i];
            }
        }
        pszStr[stSize - 1] = 0;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////
// CXChar
#if (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF8)
    typedef CChar    CXChar;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF32)
    typedef CWChar   CXChar;
#elif (__RUNTIME_CHARSET__ == RUNTIME_CONFIG_UTF16)
    #error "make sure linux native API support utf-16 string!"
#else
    #error "unknown runtime charset"
#endif

#endif // (__PLATFORM_TARGET__ == PLATFORM_TARGET_LINUX)

#endif // __TARGET_TRAITS_INL__
