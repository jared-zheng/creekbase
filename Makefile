WORKDIR := $(shell pwd)
SRCDIR = $(WORKDIR)/src
EXTDIR = $(WORKDIR)/external
# clang
ifdef CLANG_CC
BUILDDIR = $(WORKDIR)/clang-build

CC = clang
CXX = clang++
AR = ar
LD = clang++
WINDRES = windres
else
# gcc
BUILDDIR = $(WORKDIR)/gcc-build
CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres
endif

export BUILDDIR
export SRCDIR
export EXTDIR
export CC
export CXX
export AR
export LD
export WINDRES

ALLMAKES = $(SRCDIR)/private/core.make $(SRCDIR)/private/network.make $(SRCDIR)/public/coretest.make $(SRCDIR)/public/networkclt.make $(SRCDIR)/public/networksvr.make $(SRCDIR)/public/webserver.make $(SRCDIR)/public/webclient.make

all: test_build_flag test_build_dir build_all

debug_x64: test_build_flag test_build_dir build_debug_x64

release_x64: test_build_flag test_build_dir build_release_x64

debug_x86: test_build_flag test_build_dir build_debug_x86

release_x86: test_build_flag test_build_dir build_release_x86

clean: clean_all clean_build_dir

test_build_flag: 
ifdef LIB_RUNTIME
ifdef CLANG_CC
	@echo "Static Library Version Compiled with Clang"
else
	@echo "Static Library Version Compiled with GNU G++"
endif
else
ifdef CLANG_CC
	@echo "Dynamic Library Version Compiled with Clang"
else
	@echo "Dynamic Library Version Compiled with GNU G++"
endif
endif

test_build_dir: 
	test -d $(BUILDDIR) || mkdir -p $(BUILDDIR)

clean_build_dir: 
	rm -rf $(BUILDDIR)

build_all: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "All build $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake;\
	done

build_debug_x64: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "debug_x64 build $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake debug_x64;\
	done

build_release_x64: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "release_x64 build $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake release_x64;\
	done

build_debug_x86: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "debug_x86 build $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake debug_x86;\
	done

build_release_x86: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "release_x86 build $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake release_x86;\
	done

clean_all: 
	@list='$(ALLMAKES)'; for submake in $$list; do \
		echo "All Clean $$submake";\
		submakedir=`dirname $$submake`;\
		echo "$$submake path = $$submakedir";\
		$(MAKE) -C $$submakedir -f $$submake clean;\
	done

.PHONY: all debug_x64 release_x64 debug_x86 release_x86 clean test_build_flag test_build_dir clean_build_dir build_all build_debug_x64 build_release_x64 build_debug_x86 build_release_x86 clean_all 

