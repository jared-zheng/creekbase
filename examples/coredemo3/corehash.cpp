#include "stdafx.h"
#include "hash.h"
#include "corehash.h"

CPCXStr STR_TEST[] =
{
    TF(""),
    TF("a"),
    TF("ab"),
    TF("abc"),
    TF("abcd"),
    TF("abcde"),
    TF("abcdef"),
    TF("abcdefg"),
    TF("A"),
    TF("aB"),
    TF("abC"),
    TF("abcD"),
    TF("abcdE"),
    TF("abcdeF"),
    TF("abcdefG"),
    TF("A"),
    TF("AB"),
    TF("ABC"),
    TF("ABCD"),
    TF("ABCDE"),
    TF("ABCDEF"),
    TF("ABCDEFG"),
};

void DemoHash(void)
{
    for (Int i = 0; i < 21; ++i)
    {
        size_t stData  = CHash::Hash(STR_TEST[i]);
        DEV_INFO(TF("%7s hash   : %p"), STR_TEST[i], stData);

        UInt unData   = CHash::Hash32(STR_TEST[i]);
        DEV_INFO(TF("%7s hash32 : %p"), STR_TEST[i], unData);

        ULLong ullData = CHash::Hash64(STR_TEST[i]);
        DEV_INFO(TF("%7s hash64 : %llX"), STR_TEST[i], ullData);
    }
}

