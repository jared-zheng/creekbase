// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

// 按不同系统包含不同的配置文件
#ifdef _WIN32
    #include <SDKDDKVer.h>
    #include "windows/targetconfig.h"
#else
    #include "linux/targetconfig.h"
    #define _tmain   main
#endif
// 使用对应命名空间
using namespace CREEK;

// TODO:  在此处引用程序需要的其他头文件
