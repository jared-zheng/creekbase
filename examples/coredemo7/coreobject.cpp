#include "stdafx.h"
#include "object.h"
#include "coreobject.h"

class CTest : public CObject
{
    DECL_RTTI_TYPE( CTest )
#ifndef __MODERN_CXX_NOT_SUPPORTED
        DECL_TYPE_CFUNCV( AddKey, size_t, CTest, AddKey, size_t stAdd);
#else
        DECL_TYPE_CFUNCV( AddKey, size_t, CTest, AddKey, size_t);
#endif
    DECL_TYPE_END( CTest )
public:
    virtual size_t  AddKey(size_t stAdd) const PURE;
};

DEF_RTTI_TYPE( CTest, CObject, 0 )
#ifndef __MODERN_CXX_NOT_SUPPORTED  
    DEF_TYPE_CFUNCV( AddKey, size_t, CTest, AddKey, size_t);
#else
    DEF_TYPE_CFUNCV( AddKey, size_t, CTest, AddKey, size_t stAdd);
#endif
DEF_TYPE_END( CTest )

class CBase : public CTest
{
    DECL_RTTI_CREATE_TYPE( CBase )
        DECL_TYPE_VAR( Key, size_t, CBase, m_stKey );
        DECL_TYPE_CVAR( CKey, size_t, CBase, m_stCKey );
        DECL_TYPE_SCVAR( SCKey, size_t, CBase, m_stSCKey );
#ifndef __MODERN_CXX_NOT_SUPPORTED
        DECL_TYPE_CFUNC( GetKey1, size_t, CBase, GetKey1);
        DECL_TYPE_FUNCV( SetKey, void, CBase, SetKey, size_t stKey = 的风格的额的风格);
        DECL_TYPE_SFUNC( GetSKey, size_t, CBase, GetSKey);
#else
        DECL_TYPE_CFUNC( GetKey1, size_t, CBase, GetKey1);
        DECL_TYPE_FUNCV( SetKey, void, CBase, SetKey, size_t);
        DECL_TYPE_SFUNC( GetSKey, size_t, CBase, GetSKey);
#endif
    DECL_TYPE_END( CBase )

public:
    enum TEST1
    {
        TEST1_1 = 0,
        TEST1_2,
        TEST1_3,
    };
public:
    CBase() : m_stKey(0), m_stCKey(999999) {}
    CBase(size_t stKey) : m_stKey(stKey), m_stCKey(999999) {}
    virtual ~CBase(void){}
public:
    // CEventHandler
    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam) { 
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBase OnHandle %lld---%lld---%lld"), utEvent, utData, ullParam);
        return 0;
    }
    virtual UInt OnHandle(uintptr_t, CEventBase&, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, CStream&, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount) {
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBase Tick %p---%d"), utEvent, uCount);
        return 0;
    }
public:
    virtual size_t  AddKey(size_t stAdd) const { return 33333333; }
    size_t  GetKey1(void) const { return m_stKey; }
    void    SetKey(size_t stKey = 0) { m_stKey = stKey; } 
    static size_t GetSKey(void) { return 888888; }
private:
    size_t   m_stKey;
    const size_t m_stCKey;
    static const size_t m_stSCKey;
};

DEF_RTTI_CREATE_TYPE( CBase, CTest, 0 ) 
    DEF_TYPE_ENUM_BEGIN(TEST1);
    DEF_TYPE_ENUM_ITEM(TEST1_1);
    DEF_TYPE_ENUM_ITEM(TEST1_2);
    DEF_TYPE_ENUM_ITEM(TEST1_3);
    DEF_TYPE_ENUM_END(TEST1, CBase);

    DEF_TYPE_VAR( Key, size_t, CBase, m_stKey, 0 );
    DEF_TYPE_CVAR( CKey, size_t, CBase, m_stCKey, 999999 );
    DEF_TYPE_SCVAR( SCKey, size_t, CBase, m_stSCKey, 666666 );
#ifndef __MODERN_CXX_NOT_SUPPORTED  
    DEF_TYPE_CFUNC( GetKey1, size_t, CBase, GetKey1);
    DEF_TYPE_FUNCV( SetKey, void, CBase, SetKey, size_t);
    DEF_TYPE_SFUNC( GetSKey, size_t, CBase, GetSKey);
#else
    DEF_TYPE_CFUNC( GetKey1, size_t, CBase, GetKey1);
    DEF_TYPE_FUNCV( SetKey, void, CBase, SetKey, size_t stKey = 0);
    DEF_TYPE_SFUNC( GetSKey, size_t, CBase, GetSKey);
#endif
DEF_TYPE_END( CBase )

const size_t CBase::m_stSCKey = 666666;

class CDervie : public CBase
{
    DECL_RTTI_TYPE( CDervie )
        DECL_TYPE_VAR( Key2, size_t, CDervie, m_stKey2 );
        DECL_TYPE_SVAR( SCKey2, size_t, CDervie, m_stSCKey2 );
#ifndef __MODERN_CXX_NOT_SUPPORTED  
        DECL_TYPE_CFUNCV( AddKey2, size_t, CDervie, AddKey2, size_t stAdd = 1000, size_t stAdd2 = 2000);
        DECL_TYPE_SFUNCV( LogKey, size_t, CDervie, LogKey, size_t stAdd);
        DECL_TYPE_SFUNCV( CreateDervie, CDervie*, CDervie, CreateDervie, size_t st1, size_t st2);
#else
        DECL_TYPE_CFUNCV( AddKey2, size_t, CDervie, AddKey2, size_t, size_t);
        DECL_TYPE_SFUNCV( LogKey, size_t, CDervie, LogKey, size_t);
        DECL_TYPE_SFUNCV( CreateDervie, CDervie*, CDervie, CreateDervie, size_t, size_t);
#endif
    DECL_TYPE_END( CDervie )
public:
    enum TEST2
    {
        TEST2_1 = 0,
        TEST2_2,
        TEST2_3,
    };
public:
    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam) { 
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CDervie OnHandle %lld---%lld---%lld"), utEvent, utData, ullParam);
        return 0;
    }

    virtual size_t  AddKey(size_t stAdd) const { return GetKey1() + m_stKey2 + stAdd; }
    size_t  AddKey2(size_t stAdd = 1000, size_t stAdd2 = 2000) const { return GetKey1() + m_stKey2 + stAdd + stAdd2; }


    static size_t LogKey(size_t stAdd) { return CBase::GetSKey() + stAdd; }

    static CDervie* CreateDervie(size_t st1, size_t st2) { return MNEW CDervie(st1, st2); }

public:
    CDervie() : m_stKey2(0){}
    CDervie(size_t stKey2) : m_stKey2(stKey2) {}
    CDervie(size_t stKey, size_t stKey2) : CBase(stKey), m_stKey2(stKey2) {}

    virtual ~CDervie(void) {}
private:
    size_t   m_stKey2;
    static size_t m_stSCKey2;
};

DEF_RTTI_TYPE( CDervie, CBase, 0 )
    DEF_TYPE_ENUM_BEGIN(TEST2);
    DEF_TYPE_ENUM_ITEM(TEST2_1);
    DEF_TYPE_ENUM_ITEM(TEST2_2);
    DEF_TYPE_ENUM_ITEM(TEST2_3);
    DEF_TYPE_ENUM_END(TEST2, CDervie);

    DEF_TYPE_VAR( Key2, size_t, CDervie, m_stKey2, 0 );
    DEF_TYPE_SVAR( SCKey2, size_t, CDervie, m_stSCKey2, 777777 );
#ifndef __MODERN_CXX_NOT_SUPPORTED  
    DEF_TYPE_CFUNCV( AddKey2, size_t, CDervie, AddKey2, size_t, size_t);
    DEF_TYPE_SFUNCV( LogKey, size_t, CDervie, LogKey, size_t);
    DEF_TYPE_SFUNCV( CreateDervie, CDervie*, CDervie, CreateDervie, size_t, size_t);
#else
    DEF_TYPE_CFUNCV( AddKey2, size_t, CDervie, AddKey2, size_t stAdd = 1000, size_t stAdd2 = 2000);
    DEF_TYPE_SFUNCV( LogKey, size_t, CDervie, LogKey, size_t stAdd);
    DEF_TYPE_SFUNCV( CreateDervie, CDervie*, CDervie, CreateDervie, size_t st1, size_t st2);
#endif
DEF_TYPE_END( CBase )

size_t CDervie::m_stSCKey2 = 777777;



class Sender : public MObject
{
public:
    CTSignal<size_t()> GetKey1;
    CTSignal<size_t(size_t, size_t)> AddKey2;
    CSignalEvent Events;
public:
    void EventTick(PINDEX index, UInt uInterval, UInt uCount = (UInt)TIMET_INFINITE)
    {
        bool bRet = Events.Start(index, uInterval, uCount);
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!EventTick : %d"), bRet);
    }
    void Test1()
    {
        size_t stRet = GetKey1();
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Sender Test1 : %d"), stRet);
    }
    void Test2()
    {
        size_t stRet = AddKey2(10000, 20000);
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Sender Test2 : %d"), stRet);
    }

    void EventTest1(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        UInt uRet = Events(utEvent, utData, ullParam);
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!EventTest1 : %d"), uRet);
    }

    void EventTest2(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        UInt uRet = Events(utEvent, utData, ullParam, nullptr, CEventQueue::EVENT_TYPE_SYNC);
        DEV_DUMP(TF("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!EventTest2 : %d"), uRet);
    }
};


void DemoObject(void)
{
    Sender sss;
    sss.Events.SetThreadEvent(999999, 888888);

    CTRefCountPtr<CBase> BTestPtr;
    CObjectTraits::Link<CBase>(TF("asdfasdf"), BTestPtr);

    CTRefCountPtr<CBase> B2TestPtr;
    CObjectTraits::LinkRTTI<CBase>(TF("werwer"), B2TestPtr);


    CObjectPtr unk = CObject::StaticLink(TF("haha3"),
                                         TF("hahahahou"),
    #if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
                                        TF("coredemo8.dll"));
    DEV_DUMP(TF("StaticLink object from  coredemo8.dll : %p"), unk.Get());
    #else
                                        TF("coredemo8.so"));
    DEV_DUMP(TF("StaticLink object from  coredemo8.so : %p"), unk.Get());
    #endif

    const CRTTI& rtti = CObject::ClassRTTI();

    const MAP_RTTI_ENUM& enumMap = rtti.GetEnumMap();
    for (PINDEX index = enumMap.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_ENUM* pPair = enumMap.GetNext(index);
        DEV_WARN(TF("CObject enum type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
        const ENUM_ARY_ITEM Items = (static_cast<CRTTIEnum*>(pPair->m_V))->GetItems();
        for (Int i = 0; i < Items.GetSize(); ++i)
        {
            DEV_ERROR(TF("CObject enum type name = %s, key = %s, value = %lld"),
                pPair->m_K.GetBuffer(), Items[i].pszName, Items[i].ullValue);
        }
    }

    const MAP_RTTI_VAR& varMap = rtti.GetVarMap();
    for (PINDEX index = varMap.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_VAR* pPair = varMap.GetNext(index);
        DEV_WARN(TF("CObject var type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }

    const MAP_RTTI_FUNC& funcMap = rtti.GetFuncMap();
    for (PINDEX index = funcMap.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_FUNC* pPair = funcMap.GetNext(index);
        DEV_WARN(TF("CObject func type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }

    const CRTTI& rttib = CBase::ClassRTTI();
    const MAP_RTTI_ENUM& enumMapb = rttib.GetEnumMap();
    for (PINDEX index = enumMapb.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_ENUM* pPair = enumMapb.GetNext(index);
        DEV_WARN(TF("CBase enum type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
        const ENUM_ARY_ITEM Items = (static_cast<CRTTIEnum*>(pPair->m_V))->GetItems();
        for (Int i = 0; i < Items.GetSize(); ++i)
        {
            DEV_ERROR(TF("CBase enum type name = %s, key = %s, value = %lld"),
                pPair->m_K.GetBuffer(), Items[i].pszName, Items[i].ullValue);
        }
    }

    const MAP_RTTI_VAR& varMapb = rttib.GetVarMap();
    for (PINDEX index = varMapb.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_VAR* pPair = varMapb.GetNext(index);
        DEV_WARN(TF("CBase var type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }

    const MAP_RTTI_FUNC& funcMapb = rttib.GetFuncMap();
    for (PINDEX index = funcMapb.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_FUNC* pPair = funcMapb.GetNext(index);
        DEV_WARN(TF("CBase func type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }

    const CRTTI& rttid = CDervie::ClassRTTI();

    const MAP_RTTI_ENUM& enumMapd = rttid.GetEnumMap();
    for (PINDEX index = enumMapd.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_ENUM* pPair = enumMapd.GetNext(index);
        DEV_WARN(TF("CDervie enum type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
        const ENUM_ARY_ITEM Items = (static_cast<CRTTIEnum*>(pPair->m_V))->GetItems();
        for (Int i = 0; i < Items.GetSize(); ++i)
        {
            DEV_ERROR(TF("CDervie enum type name = %s, key = %s, value = %lld"),
                pPair->m_K.GetBuffer(), Items[i].pszName, Items[i].ullValue);
        }
    }

    const MAP_RTTI_VAR& varMapd = rttid.GetVarMap();
    for (PINDEX index = varMapd.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_VAR* pPair = varMapd.GetNext(index);
        DEV_WARN(TF("CDervie var type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }

    const MAP_RTTI_FUNC& funcMapd = rttid.GetFuncMap();
    for (PINDEX index = funcMapd.GetFirstIndex(); index != nullptr; )
    {
        const PAIR_RTTI_FUNC* pPair = funcMapd.GetNext(index);
        DEV_WARN(TF("CDervie func type name = %s, origin = %s, type = %x, wrapper ptr = %p"),
            pPair->m_K.GetBuffer(), pPair->m_V->GetOrigin(), pPair->m_V->GetType(), pPair->m_V);
    }









    const TYPE_SCVAR(size_t, CBase) varsc = GET_TYPE_SCVAR(SCKey, size_t, CBase);
    if (varsc.IsValid())
    {
        DEV_DUMP(TF("CBase.SCKey = %lld"), varsc.At());
    }

    const TYPE_SVAR(size_t, CDervie) vars2 = GET_TYPE_SVAR(SCKey2, size_t, CDervie);
    if (vars2.IsValid())
    {
        DEV_DUMP(TF("CDervie.SCKey2 = %lld"), vars2.At());
    }

    const TYPE_VAR(size_t, CBase) var = GET_TYPE_VAR(Key, size_t, CBase);
    if (var.IsValid())
    {
        CBase base(123);
        base.Link("ssd");

        CObjectTraits::Connect(sss.GetKey1, base, &CBase::GetKey1);
        sss.Test1();
        PINDEX index = CObjectTraits::Connect(sss.Events, base, VART_EVENTTICK | VART_RTTIDYNAMIC);
        sss.EventTick(index, 1000);
        CPlatform::SleepEx(5000);
        sss.Events.Stop(index);

        //const CRTTICFunc<CObject, PINDEX> funcobj = GET_TYPE_CFUNC( GetKey, PINDEX, CObject );
        const NAME_TYPE_CFUNC(GetKey, PINDEX, CObject) funcobj = GET_TYPE_CFUNC(GetKey, PINDEX, CObject);
        DEV_DUMP(TF("0. base.GetKey = %p---%p"), (base.*funcobj.Get())(), base.GetKey());

        DEV_DUMP(TF("1. base.m_stKey = %lld"), var.At(base));


        const CTRTTICFunc<CTest, size_t(size_t) const> func1 = GET_TYPE_CFUNCV(AddKey, size_t, CTest, size_t);
        DEV_DUMP(TF("7. base.Add = %lld"), (base.*func1.Get())(2000));
        DEV_DUMP(TF("8. base.Add = %lld"), func1.Invoke(base, 2000));

        var.At(base) = 100;
        DEV_DUMP(TF("2. base.m_stKey = %lld"), base.GetKey1());

        const TYPE_CVAR(size_t, CBase) varc = GET_TYPE_CVAR(CKey, size_t, CBase);
        if (varc.IsValid())
        {
            //varc.At(base) = 111111;
            DEV_DUMP(TF("3. base.m_stCKey = %lld"), varc.At(base));
        }

        //const CRTTICFunc<CBase, size_t> func = GET_TYPE_CFUNC( GetKey1, size_t, CBase );
        const NAME_TYPE_CFUNC(GetKey1, size_t, CBase) func = GET_TYPE_CFUNC(GetKey1, size_t, CBase);
        //const CRTTIFunc<CBase, void, size_t> funcset = GET_TYPE_FUNCV( SetKey, void, CBase, size_t );
        const NAME_TYPE_FUNCV(SetKey, void, CBase, size_t) funcset = GET_TYPE_FUNCV(SetKey, void, CBase, size_t);
        (base.*funcset.Get())(898);
        DEV_DUMP(TF("4. base.m_stKey = %lld"), (base.*func.Get())());
#ifndef __MODERN_CXX_NOT_SUPPORTED  
        funcset.Invoke(base, 898);
        DEV_DUMP(TF("5. base.m_stKey = %lld"), func.Invoke(base));
#else
        DEV_DUMP(TF("5. base.m_stKey = %lld"), (base.*func.Get())());
#endif

        //const CRTTISFunc<CBase, size_t> funcs = GET_TYPE_SFUNC( GetSKey, size_t, CBase );
        const NAME_TYPE_SFUNC(GetSKey, size_t, CBase) funcs = GET_TYPE_SFUNC(GetSKey, size_t, CBase);
#ifndef __MODERN_CXX_NOT_SUPPORTED  
        DEV_DUMP(TF("6. base.static = %lld"), funcs.Invoke());
#else
        DEV_DUMP(TF("6. base.static = %lld"), (funcs.Get())());
#endif
    }
#ifndef __MODERN_CXX_NOT_SUPPORTED   
    const CTRTTIVar<CBase, size_t> var1 = GET_TYPE_VAR(Key, size_t, CBase);
    const CTRTTIVar<CDervie, size_t> var2 = GET_TYPE_VAR(Key2, size_t, CDervie);
    if (var1.IsValid() && var2.IsValid())
    {
        //CDervie dervie(1234, 1000);
        //dervie.Link("hahahsdasd");   

        CTRefCountPtr<CDervie> dptr;
        const CTRTTISFunc<CDervie, CDervie* (size_t, size_t)> func_create = GET_TYPE_SFUNCV(CreateDervie, CDervie*, CDervie, size_t, size_t);
        dptr = func_create.Invoke(1234, 1000);
        if (dptr != nullptr)
        {
            CDervie& dervie = *dptr;
            dervie.Link("hahahsdasd");

            CString strFunc = "AddKey2";
            //CObjectTraits::Connect(sss.AddKey2, dervie, &CDervie::AddKey2, VART_RTTIDYNAMIC);
            //CObjectTraits::ConnectCFunc<CDervie>(sss.AddKey2, "CDervie", "hahahsdasd", strFunc.GetBuffer(), VART_RTTIDYNAMIC);
            CObjectTraits::Connect<CDervie>(sss.AddKey2, "CDervie", "hahahsdasd", strFunc.GetBuffer(), VART_CONST);
            sss.Test2();

            CObjectTraits::Connect(sss.Events, dervie, VART_EVENTQUEUE | VART_RTTIDYNAMIC);
            sss.EventTest1(1000, 2000, 3000);
            sss.EventTest2(10000, 20000, 30000);
            CPlatform::SleepEx(5000);
            sss.Events.Disconnect();

            const CTRTTICFunc<CObject, PINDEX() const> funcobj = GET_TYPE_CFUNC(GetKey, PINDEX, CObject);
            DEV_DUMP(TF("0. dervie.GetKey = %p---%p"), funcobj.Invoke(dervie), dervie.GetKey());

            DEV_DUMP(TF("1. dervie.m_stKey = %lld"), var1.At(dervie));
            DEV_DUMP(TF("2. dervie.m_stKey2 = %lld"), var2.At(dervie));

            var1.At(dervie) = 2000;
            var2.At(dervie) = 10000;

            DEV_DUMP(TF("3. dervie.m_stKey = %lld"), dervie.GetKey1());
            DEV_DUMP(TF("4. dervie.Add = %lld"), dervie.AddKey(1000));

            const CTRTTIFunc<CBase, void(size_t)> funcset = GET_TYPE_FUNCV(SetKey, void, CBase, size_t);
            funcset.Invoke(dervie, 4000);
            //var1.At(dervie) = 4000; 
            var2.At(dervie) = 20000;

            const CTRTTICFunc<CBase, size_t() const> func = GET_TYPE_CFUNC(GetKey1, size_t, CBase);
            DEV_DUMP(TF("5. dervie.m_stKey = %lld"), (dervie.*func.Get())());
            DEV_DUMP(TF("6. dervie.m_stKey = %lld"), func.Invoke(dervie));

            const CTRTTICFunc<CTest, size_t(size_t) const> func1 = GET_TYPE_CFUNCV(AddKey, size_t, CTest, size_t);
            DEV_DUMP(TF("7. dervie.Add = %lld"), (dervie.*func1.Get())(2000));
            DEV_DUMP(TF("8. dervie.Add = %lld"), func1.Invoke(dervie, 2000));

            //const CTRTTICFunc<CDervie, size_t, size_t, size_t> func2 = GET_TYPE_CFUNCV( AddKey2, size_t, CDervie, size_t, size_t );
            const NAME_TYPE_CFUNCV(AddKey2, size_t, CDervie, size_t, size_t) func2 = GET_TYPE_CFUNCV(AddKey2, size_t, CDervie, size_t, size_t);
            DEV_DUMP(TF("9. dervie.Add2 = %lld"), (dervie.*func2.Get())(80000, 10000));
            DEV_DUMP(TF("a. dervie.Add2 = %lld"), func2.Invoke(dervie, 80000, 10000));

            const CTRTTISFunc<CBase, size_t()> funcs = GET_TYPE_SFUNC(GetSKey, size_t, CBase);
            DEV_DUMP(TF("b. dervie.static = %lld"), funcs.Invoke());

            const CTRTTISFunc<CDervie, size_t(size_t)> funcs1 = GET_TYPE_SFUNCV(LogKey, size_t, CDervie, size_t);
            DEV_DUMP(TF("c. dervie.Log = %lld"), funcs1.Invoke(111111));
        }
    }
#endif
}

