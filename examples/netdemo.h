#include "stdafx.h"

enum DEMO_EVENT
{
    DEMO_EVENT_LIVE,
    DEMO_EVENT_LIVEACK,

    DEMO_EVENT_LOGIN,
    DEMO_EVENT_LOGINACK,

    DEMO_EVENT_LOGOUT,
    DEMO_EVENT_LOGOUTACK,

    DEMO_EVENT_CHAT,
    DEMO_EVENT_CHATACK,
};

typedef struct tagDEMO_DATA
{
    UInt   uEvent;
    UInt   uParam;
}DEMO_DATA, *PDEMO_DATA;

struct tagDEMO_LOGIN : public tagDEMO_DATA
{
    XChar szName[64];
    XChar szPW[64];
};
typedef tagDEMO_LOGIN DEMO_LOGIN, *PDEMO_LOGIN;

struct tagDEMO_CHAT : public tagDEMO_DATA
{
    XChar szChat[1024];
};
typedef tagDEMO_CHAT DEMO_CHAT, *PDEMO_CHAT;
