#include "stdafx.h"
#include "network.h"
#include "..\netdemo.h"

CNetworkPtr          NetPtr;
CSyncEvent           gs_Event;
CNETTraits::Socket   gs_Connect = 0;


class CNetEventHandler : public CEventHandler
{
public:
    CNetEventHandler(void) { }
    virtual ~CNetEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        switch (utEvent)
        {
        case CNETTraits::EVENT_TCP_CONNECT:
            {
                DEV_WARN(TF("Connect socket = %p return %d"), ullParam, utData);
                if (utData == 0)
                {
                    DEMO_LOGIN login;
                    login.uEvent = DEMO_EVENT_LOGIN;
                    login.uParam = 0;
                    CXChar::Copy(login.szName, 64, TF("开发的价格"));
                    CXChar::Copy(login.szPW, 64, TF("势力扩大"));
                    CNETTraits::CStreamScopePtr streamptr;
                    NetPtr->AllocBuffer(streamptr);
                    CStream& Stream = *streamptr;
                    //Stream <= TF("hello sdfdsfsdf!");
                    Stream.Write(&login, sizeof(DEMO_LOGIN));
                    NetPtr->Send(gs_Connect, streamptr);
                }
            }
            break;
        case CNETTraits::EVENT_TCP_CLOSE:
            {
            }
            break;
        case CNETTraits::EVENT_UDP_CLOSE:
            {
            }
            break;
        case CNETTraits::EVENT_TCP_RECV:
            {
                CNETTraits::PTCP_PARAM pTcp = reinterpret_cast<CNETTraits::PTCP_PARAM>(ullParam);
                XChar szTemp[LMT_BUF];
                CXChar::Copy(szTemp, LMT_BUF, (PCXStr)pTcp->pData, utData);
                szTemp[utData] = 0;
                DEV_ERROR(TF("Recv string %s"), szTemp);

                gs_Event.Signal();
            }
            break;
        }
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, UInt) { return RET_FAIL; }
};

int _tmain(int argc, _TCHAR* argv[])
{
    CoreInit();

#ifdef __RUNTIME_STATIC__
    CTRefCountPtr<CSubSystem> load = CreateNetworkStatic(UUID_OF(CNetworkSystem));
#else
    CTLoader<CSubSystem> load;
    if (load.Load(UUID_OF(CNetworkSystem), NETWORK_MODULE_NAME))
#endif
    {
        if (load->CreateComponent(UUID_OF(CNetwork), NetPtr.Cast<CComponent>())) // 
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs  = CNETTraits::ATTR_THREAD;
            attr.nThread = 2;
            if (NetPtr->Init(attr, (MNEW CNetEventHandler)) == RET_OKAY)
            {
                gs_Connect = NetPtr->Create();
                if (gs_Connect != 0)
                {
                    NetPtr->SetAttr(gs_Connect, TRUE, CNETTraits::SOCKET_RECV_EVENT);   // 收发数据直接调用OnHandle, 不需要解析器
                    NetPtr->SetAttr(gs_Connect, TRUE, CNETTraits::SOCKET_EXCLUDE_HEAD); // 没有额外的发送数据包头部
                    NetPtr->Connect(gs_Connect, 19999, TF("localhost"));
                    gs_Event.Wait();
                }
            }
        }
    }

    CoreExit();
    getchar();
    return 0;
}

