#ifndef __TEST_PACK_H__
#define __TEST_PACK_H__

#pragma once

#include "networkevent.h"

#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    #define TEST_IPV6
#endif

enum TEST_EVENT
{
    TEST_EVENT_LIVE     = CNETTraits::EVENT_RESERVED,
    TEST_EVENT_LIVEACK,

    TEST_EVENT_LOGIN,
    TEST_EVENT_LOGINACK,

    TEST_EVENT_CHAT,
    TEST_EVENT_CHATACK,

    TEST_EVENT_LOGOUT,
    TEST_EVENT_LOGOUTACK,
};

// CTestPackBuild
class CTestPackBuild : public CNETTraits::CNETPackBuild
{
public:
    typedef struct tagPACK_HEAD
    {
        UInt    uFlag1;
        UInt    uFlag2;
        UInt    uFlag3;
        UInt    uSize;
    }PACK_HEAD, *PPACK_HEAD;
public:
    CTestPackBuild(void)
    {
        DEV_INFO(TF("TCP>>>>>>>>>Test PackBuild Created"));
    }
    virtual ~CTestPackBuild(void)
    {
        DEV_INFO(TF("TCP>>>>>>>>>Test PackBuild Destroyed"));
    }

    // send package head ptr, package data size
    // return package all size, include package head
    virtual size_t Build(PByte pPack, size_t stSize) OVERRIDE
    {
        PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
        pHead->uFlag1 = 0x11111111;
        pHead->uFlag2 = 0x22222222;
        pHead->uFlag3 = 0x33333333;
        pHead->uSize  = (UInt)(stSize + sizeof(PACK_HEAD));
        return (size_t)(pHead->uSize);
    }
    // check recv package valid check
    // return package all size, include package head
    // return 0 when check fail
    virtual size_t Check(PByte pPack, size_t) OVERRIDE
    {
        PPACK_HEAD pHead = reinterpret_cast<PPACK_HEAD>(pPack);
        if (pHead->uFlag1 == 0x11111111 && pHead->uFlag2 == 0x22222222 && pHead->uFlag3 == 0x33333333)
        {
            return (size_t)(pHead->uSize);
        }
        return 0;
    }
    // package head size
    virtual size_t HeadSize(void) const OVERRIDE
    {
        return sizeof(PACK_HEAD);
    }
};

// CTestPack
class CTestPack : public CEventBase
{
public:
    static CTestPack* Create(uintptr_t, CStream&, ULLong);
public:
    CTestPack(UInt uEvent = 0);
    virtual ~CTestPack(void);

    CTestPack(const CTestPack& aSrc);
    CTestPack& operator=(const CTestPack& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetEvent(void);
private:
    UInt   m_uEvent;
};

// CAckPacket
class CAckPacket : public CTestPack
{
public:
    CAckPacket(UInt uEvent = 0);
    virtual ~CAckPacket(void);

    CAckPacket(const CAckPacket& aSrc);
    CAckPacket& operator=(const CAckPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    UInt   GetAck(void);
    void   SetAck(UInt uAck);
private:
    UInt   m_uAck;
};

// CLoginPacket
class CLoginPacket : public CTestPack
{
public:
    CLoginPacket(UInt uId = 0, UInt uPass = 0);
    virtual ~CLoginPacket(void);

    CLoginPacket(const CLoginPacket& aSrc);
    CLoginPacket& operator=(const CLoginPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    CString& GetName(void);
    void     SetName(CString& strName);
    void     SetName(CString&& strName);
    void     SetName(PCXStr pszName);

    CString& GetPW(void);
    void     SetPW(CString& strPW);
    void     SetPW(CString&& strPW);
    void     SetPW(PCXStr pszPW);
private:
    CString   m_strName;
    CString   m_strPW;
};

// CChatPacket
class CChatPacket : public CTestPack
{
public:
    CChatPacket(void);
    virtual ~CChatPacket(void);

    CChatPacket(const CChatPacket& aSrc);
    CChatPacket& operator=(const CChatPacket& aSrc);

    virtual size_t Length(void) const;
    virtual void   Serialize(CStream&);

    CString& GetChat(void);
    void     SetChat(CString& strChat);
    void     SetChat(CString&& strChat);
    void     SetChat(PCXStr pszChat);
private:
    CString   m_strChat;
};

typedef CTNetworkEventHandler<CTNETDispatch<CTestPack, false>> CTestPackEventHandler;

#include "testpack.inl"

#endif // __TEST_PACK_H__
