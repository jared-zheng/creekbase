#include "stdafx.h"
#include "thread.h"
#include "corethread.h"

class CMyThread : public CThread
{
public:
    CMyThread() : m_nCount(0) {}
protected:
    virtual bool OnStart(void)
    {
        DEV_INFO(TF("Thread %d on start"), GetId());
        return true;
    }

    virtual void OnStop(void)
    {
        DEV_INFO(TF("Thread %d on stop"), GetId());
    }

    virtual void OnKill(void)
    {
        DEV_INFO(TF("Thread %d on kill"), GetId());
    }

    virtual bool Run(void)
    {
        DEV_INFO(TF("Thread %d on Running %d"), GetId(), m_nCount);
        ++m_nCount;
        return (m_nCount < 3); // 任务完成, 线程不需要在运行了
    }
private:
    Int   m_nCount;
};

class CMyRunnable : public CRunnable
{
public:
    CMyRunnable() : m_nCount(0) {}
public:
    virtual bool OnStart(TId tId)
    {
        DEV_INFO(TF("Runnable-Thread[%d---%d] on start"), CPlatform::GetCurrentTId(), tId);
        return true;
    }

    virtual void OnStop(void)
    {
        DEV_INFO(TF("Runnable-Thread[%d] on stop"), CPlatform::GetCurrentTId());
    }

    virtual bool Run(void)
    {
        DEV_INFO(TF("Runnable-Thread[%d] on Running %d"), CPlatform::GetCurrentTId(), m_nCount);
        ++m_nCount;
        return (m_nCount < 3); // 任务完成, 线程不需要在运行了
    }
private:
    Int   m_nCount;
};

class CMyQueueTask : public CQueueTask
{
public:
    virtual void Run(TId tId)
    {
        DEV_INFO(TF("QueueTask-Thread[%d---%d] on finish"), CPlatform::GetCurrentTId(), tId);
    }
};

void DemoThread(void)
{
    CThread::QueueThread(32);
    CMyQueueTask  MyQueueTask[4];
    for (Int i = 0; i < 4; ++i)
    {
        CThread::QueueTask(MyQueueTask[i]);
    }

    CMyThread MyThread[4];
    for (Int i = 0; i < 4; ++i)
    {
        MyThread[i].Start();
    }

    CMyRunnable MyRunnable[20];
    for (Int i = 0; i < 20; ++i)
    {
        CThread::StartRunnable(MyRunnable[i]);
    }

    DEV_INFO(TF("SleepEx 8000"));
    CPlatform::SleepEx(8000);
}

