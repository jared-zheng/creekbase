#include "stdafx.h"
#include "network.h"
#include "..\netdemo.h"

CNetworkPtr          NetPtr;
CSyncEvent           gs_Event;
CNETTraits::Socket   gs_Listen = 0;

class CNetEventHandler : public CEventHandler
{
public:
    CNetEventHandler(void) { }
    virtual ~CNetEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        switch (utEvent)
        {
        case CNETTraits::EVENT_TCP_ACCEPT:
            {
                CString strAddr;
                UShort usPort = 0;
                NetPtr->GetAddr(utData, strAddr, usPort);

                DEV_WARN(TF("Listen socket = %llX accept %p as %s:%d"), ullParam, utData, *strAddr, usPort);
                NetPtr->SetAttr(utData, TRUE, CNETTraits::SOCKET_RECV_EVENT);   // 收发数据直接调用OnHandle, 不需要解析器
                NetPtr->SetAttr(utData, TRUE, CNETTraits::SOCKET_EXCLUDE_HEAD); // 没有额外的发送数据包头部
            }
            break;
        case CNETTraits::EVENT_TCP_CLOSE:
            {
            }
            break;
        case CNETTraits::EVENT_UDP_CLOSE:
            {
            }
            break;
        case CNETTraits::EVENT_TCP_RECV:
            {
                CNETTraits::PTCP_PARAM pTcp = reinterpret_cast<CNETTraits::PTCP_PARAM>(ullParam);
                //XChar szTemp[LMT_BUF];
                //CXChar::Copy(szTemp, LMT_BUF, (PCXStr)pTcp->pData, utData);
                //szTemp[utData] = 0;
                //DEV_ERROR(TF("Recv string %s"), szTemp);

                //CNETTraits::CStreamScopePtr streamptr;
                //NetPtr->AllocBuffer(streamptr);
                ////NetPtr->AllocJumboBuffer(streamptr);
                //CStream& Stream = *streamptr;
                //Stream <= TF("山坡地覅是 欧舒丹覅o(*￣▽￣*)o!");
                //NetPtr->Send(pTcp->sSocket, Stream);

                //gs_Event.Signal();
                PDEMO_DATA pData = (PDEMO_DATA)pTcp->pData;
                switch (pData->uEvent)
                {
                case DEMO_EVENT_LOGIN:
                    {
                        PDEMO_LOGIN pLogin = (PDEMO_LOGIN)pData;
                        DEV_ERROR(TF("Recv Login %s---%s"), pLogin->szName, pLogin->szPW);
                    }
                    break;
                //case DEMO_EVENT_CHAT:
                //    {
                //        PDEMO_LOGIN pLogin = (PDEMO_LOGIN)pData;
                //        DEV_ERROR(TF("Recv Login %s---%s"), pLogin->szName, pLogin->szPW);
                //    }
                //    break;
                }
            }
            break;
        }
        return RET_OKAY;
    }
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, UInt) { return RET_FAIL; }
};



int _tmain(int argc, _TCHAR* argv[])
{
    CoreInit();

#ifdef __RUNTIME_STATIC__
    CTRefCountPtr<CSubSystem> load = CreateNetworkStatic(UUID_OF(CNetworkSystem));
#else
    CTLoader<CSubSystem> load;
    if (load.Load(UUID_OF(CNetworkSystem), NETWORK_MODULE_NAME)) // 类工厂
#endif
    {
        if (load->CreateComponent(UUID_OF(CNetwork), NetPtr.Cast<CComponent>())) // 
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs  = CNETTraits::ATTR_THREAD;
            attr.nThread = 2;
            if (NetPtr->Init(attr, (MNEW CNetEventHandler)) == RET_OKAY)
            {
                gs_Listen = NetPtr->Create(19999);
                if (gs_Listen != 0)
                {
                    DEV_WARN(TF("Listen socket = %p"), gs_Listen);
                    NetPtr->Listen(gs_Listen);
                    gs_Event.Wait();
                }
            }
        }
    }
    CoreExit();
    getchar();
    return 0;
}

