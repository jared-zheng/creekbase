#pragma once

#include "object.h"

class CDervie3 : public CObject
{
    DECL_RTTI_CREATE( CDervie3 )
public:
    CDervie3() { DEV_ERROR(TF("CDervie3::CDervie3")); }
    virtual ~CDervie3() { DEV_ERROR(TF("CDervie3::~CDervie3")); }
    // CEventHandler
    virtual UInt OnHandle(uintptr_t, uintptr_t, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, CEventBase&, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, CStream&, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, UInt) { return RET_FAIL; }
private:
    ULong   m_ulData2;
};

