#include "stdafx.h"
#include "..\testpack.h"

CNetworkPtr          NetPtr;
CSyncEvent           gs_Event;
CNETTraits::Socket   gs_Listen = 0;
CNETTraits::Socket   gs_UDP    = 0;


class CNetEventHandler : public CTestPackEventHandler
{
public:
    CNetEventHandler(void) { }
    virtual ~CNetEventHandler(void) { }

    virtual bool OnTcpDispatch(const PacketPtr& PktPtr, PTCP_PARAM pTcp)
    {
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_LOGIN:
            {
                CLoginPacket* pPack = static_cast<CLoginPacket*>(PktPtr.Get());
                DEV_INFO(TF("#########Login is %s[%s]"), pPack->GetName().GetBuffer(), *pPack->GetPW());

                CAckPacket LoginAck(TEST_EVENT_LOGINACK);
                LoginAck.SetAck(RET_OKAY);

                NetPtr->Send(pTcp->sSocket, LoginAck);
            }
            break;
        case TEST_EVENT_CHAT:
            {
                CChatPacket* pPack = static_cast<CChatPacket*>(PktPtr.Get());
                DEV_INFO(TF("#########Chat---%s"), pPack->GetChat().GetBuffer());

                CAckPacket Ack(TEST_EVENT_CHATACK);
                Ack.SetAck((UInt)pPack->GetChat().Length());
                NetPtr->Send(pTcp->sSocket, Ack);
            }
            break;
        case TEST_EVENT_LOGOUT:
            {
                CTestPack* pPack = static_cast<CTestPack*>(PktPtr.Get());
                DEV_INFO(TF("#########Logout"));

                CAckPacket Ack(TEST_EVENT_LOGOUTACK);
                Ack.SetAck(RET_OKAY);
                NetPtr->Send(pTcp->sSocket, Ack);

                gs_Event.Signal();
            }
            break;
        default:
            {
                // live-packet
            }
        }
        return true;
    }

    virtual bool OnUdpDispatch(const PacketPtr& PktPtr, PUDP_PARAM pUdp)
    {
        assert(pUdp->sSocket == gs_UDP);
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_CHAT:
            {
                CChatPacket* pPack = static_cast<CChatPacket*>(PktPtr.Get());
                DEV_INFO(TF("$$$$$$$$$Chat---%s"), pPack->GetChat().GetBuffer());

                CAckPacket Ack(TEST_EVENT_CHATACK);
                Ack.SetAck((UInt)pPack->GetChat().Length());
                NetPtr->SendTo(pUdp->sSocket, Ack, pUdp->NetAddr);
            }
            break;
        default:
            {
            }
        }
        return true;
    }

    virtual bool OnTcpAccept(Socket sAccept, Socket sListen)
    {
        DEV_INFO(TF("###Accept socket=%p, listen socket %p"), sAccept, sListen);
        assert(sListen == gs_Listen);
        return true;
    }

    virtual bool OnTcpConnect(UInt uError, Socket sConnect)
    {
        DEV_INFO(TF("###Connect return errid=%d, socket %p"), uError, sConnect);
        return true;
    }

    virtual bool OnTcpRecv(size_t, PTCP_PARAM)
    {
        return true;
    }

    virtual bool OnTcpSend(UInt, Socket)
    {
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == gs_Listen)
        {
            // ...
        }
        return true;
    }

    virtual bool OnUdpRecv(size_t, PUDP_PARAM)
    {
        return true;
    }

    virtual bool OnUdpSend(UInt, Socket)
    {
        return true;
    }

    virtual bool OnUdpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("$$$Close udp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == gs_UDP)
        {
            // ...
        }
        return true;
    }
};



int _tmain(int argc, _TCHAR* argv[])
{
    CoreInit();

#ifdef __RUNTIME_STATIC__
    CTRefCountPtr<CSubSystem> load = CreateNetworkStatic(UUID_OF(CNetworkSystem));
#else
    CTLoader<CSubSystem> load;
    if (load.Load(UUID_OF(CNetworkSystem), NETWORK_MODULE_NAME)) // 类工厂
#endif
    {
        if (load->CreateComponent(UUID_OF(CNetwork), NetPtr.Cast<CComponent>())) // 
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs  = CNETTraits::ATTR_THREAD;
            attr.nThread = 2;
            // 注意 : 数据包头部构造器作用范围为当前CNetwork实例, 并且收发双方都需要使用一致的头部构造器
            //       如果单独的连接不需要头部构造器, 可以单独增加SOCKET_EXCLUDE_HEAD属性
            //       或者再创建一个CNetwork实例, 使用其他头部构造器
            if (NetPtr->Init(attr, (MNEW CNetEventHandler), MNEW CTestPackBuild) == RET_OKAY)
            {
                CNETTraits::ARY_STRING strAddrs;
#ifdef TEST_IPV6
                if (NetPtr->GetLocalAddr(strAddrs))
#else
                if (NetPtr->GetLocalAddr(strAddrs, CNETTraits::ATTR_IPV4))
#endif
                {
                    for (Int i = 0; i < strAddrs.GetSize(); ++i)
                    {
                        DEV_INFO(TF(" *IP:%s"), *(strAddrs[i]));
                    }
                }

                gs_UDP    = NetPtr->Create(19998, nullptr, CNETTraits::SOCKET_UDP);
                gs_Listen = NetPtr->Create(19998);
                if (gs_Listen != 0)
                {
                    DEV_WARN(TF("Listen socket = %p"), gs_Listen);
                    NetPtr->Listen(gs_Listen);
                    gs_Event.Wait();
                }
            }
        }
    }
    CoreExit();
    getchar();
    return 0;
}

