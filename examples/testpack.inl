#ifndef __TEST_PACK_INL__
#define __TEST_PACK_INL__

#pragma once

///////////////////////////////////////////////////////////////////
// CTestPack
INLINE CTestPack::CTestPack(UInt uEvent)
: m_uEvent(uEvent)
{
}

INLINE CTestPack::~CTestPack(void)
{
}

INLINE CTestPack::CTestPack(const CTestPack& aSrc)
: m_uEvent(aSrc.m_uEvent)
{
}

INLINE CTestPack& CTestPack::operator=(const CTestPack& aSrc)
{
    if (&aSrc != this)
    {
        m_uEvent = aSrc.m_uEvent;
    }
    return (*this);
}

INLINE size_t CTestPack::Length(void) const
{
    return (sizeof(UInt));
}

INLINE void CTestPack::Serialize(CStream& Stream)
{
    if (Stream.IsRead())
    {
        Stream >> m_uEvent;
    }
    else
    {
        Stream << m_uEvent;
    }
}

INLINE UInt CTestPack::GetEvent(void)
{
    return m_uEvent;
}

///////////////////////////////////////////////////////////////////
// CAckPacket
INLINE CAckPacket::CAckPacket(UInt uEvent)
: CTestPack(uEvent)
, m_uAck(0)
{
}

INLINE CAckPacket::~CAckPacket(void)
{
}

INLINE CAckPacket::CAckPacket(const CAckPacket& aSrc)
: CTestPack(aSrc)
, m_uAck(aSrc.m_uAck)
{
}

INLINE CAckPacket& CAckPacket::operator=(const CAckPacket& aSrc)
{
    if (&aSrc != this)
    {
        CTestPack::operator=(aSrc);
        m_uAck   = aSrc.m_uAck;
    }
    return (*this);
}

INLINE size_t CAckPacket::Length(void) const
{
    return (sizeof(UInt) + CTestPack::Length());
}

INLINE void CAckPacket::Serialize(CStream& Stream)
{
    CTestPack::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_uAck;
    }
    else
    {
        Stream << m_uAck;
    }
}

INLINE UInt CAckPacket::GetAck(void)
{
    return m_uAck;
}

INLINE void CAckPacket::SetAck(UInt ulAck)
{
    m_uAck = ulAck;
}

///////////////////////////////////////////////////////////////////
// CLoginPacket
INLINE CLoginPacket::CLoginPacket(UInt uId, UInt uPass)
: CTestPack(TEST_EVENT_LOGIN)
{
}

INLINE CLoginPacket::~CLoginPacket(void)
{
}

INLINE CLoginPacket::CLoginPacket(const CLoginPacket& aSrc)
: CTestPack(aSrc)
, m_strName(aSrc.m_strName)
, m_strPW(aSrc.m_strPW)
{
}

INLINE CLoginPacket& CLoginPacket::operator=(const CLoginPacket& aSrc)
{
    if (&aSrc != this)
    {
        CTestPack::operator=(aSrc);
        m_strName = aSrc.m_strName;
        m_strPW   = aSrc.m_strPW;
    }
    return (*this);
}

INLINE size_t CLoginPacket::Length(void) const
{
    return (m_strName.Length(true) + m_strPW.Length(true) + CTestPack::Length());
}

INLINE void CLoginPacket::Serialize(CStream& Stream)
{
    CTestPack::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_strName >> m_strPW;
    }
    else
    {
        Stream << m_strName << m_strPW;
    }
}

INLINE CString& CLoginPacket::GetName(void)
{
    return m_strName;
}

INLINE void CLoginPacket::SetName(CString& strName)
{
    m_strName = strName;
}

INLINE void CLoginPacket::SetName(CString&& strName)
{
    m_strName = std::move(strName);
}

INLINE void CLoginPacket::SetName(PCXStr pszName)
{
    m_strName = pszName;
}

INLINE CString& CLoginPacket::GetPW(void)
{
    return m_strPW;
}

INLINE void CLoginPacket::SetPW(CString& strPW)
{
    m_strPW = strPW;
}

INLINE void CLoginPacket::SetPW(CString&& strPW)
{
    m_strPW = std::move(strPW);
}

INLINE void CLoginPacket::SetPW(PCXStr pszPW)
{
    m_strPW = pszPW;
}

///////////////////////////////////////////////////////////////////
// CChatPacket
INLINE CChatPacket::CChatPacket(void)
: CTestPack(TEST_EVENT_CHAT)
{
}

INLINE CChatPacket::~CChatPacket(void)
{
}

INLINE CChatPacket::CChatPacket(const CChatPacket& aSrc)
: CTestPack(aSrc)
, m_strChat(aSrc.m_strChat)
{
}

INLINE CChatPacket& CChatPacket::operator=(const CChatPacket& aSrc)
{
    if (&aSrc != this)
    {
        CTestPack::operator=(aSrc);
        m_strChat  = aSrc.m_strChat;
    }
    return (*this);
}

INLINE size_t CChatPacket::Length(void) const
{
    return (m_strChat.Length(true) + CTestPack::Length());
}

INLINE void CChatPacket::Serialize(CStream& Stream)
{
    CTestPack::Serialize(Stream);
    if (Stream.IsRead())
    {
        Stream >> m_strChat;
    }
    else
    {
        Stream << m_strChat;
    }
}

INLINE CString& CChatPacket::GetChat(void)
{
    return m_strChat;
}

INLINE void CChatPacket::SetChat(CString& strChat)
{
    m_strChat = strChat;
}

INLINE void CChatPacket::SetChat(CString&& strChat)
{
    m_strChat = std::move(strChat);
}

INLINE void CChatPacket::SetChat(PCXStr pszChat)
{
    m_strChat = pszChat;
}

#endif // __TEST_PACK_INL__