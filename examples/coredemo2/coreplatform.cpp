#include "stdafx.h"
#include "coreplatform.h"

PCXStr GetOSType(UInt uOSType)
{
    switch (uOSType)
    {
        CASE_RETSTR(CPlatform::OST_UNKNOWN, TF("Unknown OS"));
        CASE_RETSTR(CPlatform::OST_WINDOWS, TF("Windows"));
        CASE_RETSTR(CPlatform::OST_LINUX, TF("Linux"));
        CASE_RETSTR(CPlatform::OST_MAC, TF("Mac"));
        CASE_RETSTR(CPlatform::OST_IOS, TF("iOS"));
        CASE_RETSTR(CPlatform::OST_ANDROID, TF("Android"));
    default:
        {
            return TF("Unknown OS Type");
        }
    }
}

void DemoPlatform(void)
{
    // 1.基础层编译状态信息
    ULLong ullRuntime = CPlatform::GetRuntimeConfig();
    DEV_ERROR(TF("---Platform Information--- %llX"), ullRuntime);
    DEV_INFO(TF(" %s"), TF(__ARCH_TARGET_STR__));
    DEV_INFO(TF(" %s"), TF(__PLATFORM_TARGET_STR__));
    DEV_INFO(TF(" %s"), TF(__RUNTIME_CHARSET_STR__));
    DEV_INFO(TF(" %s"), (ullRuntime & RUNTIME_CONFIG_DEBUG)       ? TF("Debug Status")   : TF("Release Status"));
    DEV_INFO(TF(" %s"), (ullRuntime & RUNTIME_CONFIG_STATIC)      ? TF("Static Library") : TF("Dynamic Library"));

    CPlatform::SleepEx(2000);
    // 2.CPU, 系统, 内存和时间
    CPlatform::CPUINFO cpui;
    CPlatform::GetCPUInfo(cpui);
    DEV_WARN(TF("---CPU Information---"));
    DEV_INFO(TF(" %s"), cpui.szInfo);
    DEV_INFO(TF(" Cores : %d"), cpui.uCores);

    CPlatform::CPUSTAT cs1, cs2;
    CPlatform::GetCPUStat(cs1);
    DEV_ERROR(TF("CPUStat1 %llu,%llu"), cs1.ullUsed, cs1.ullTotal);
    CPlatform::SleepEx(1000);
    CPlatform::GetCPUStat(cs2);
    DEV_ERROR(TF("CPUStat2 %llu,%llu"), cs2.ullUsed, cs2.ullTotal);
    
    UInt uRate = CPlatform::GetCPURate(cs1, cs2);
    UInt u1 = (uRate / 100);
    UInt u2 = (uRate % 100);

    uRate = 10000 - uRate;
    UInt u3 = (uRate / 100);
    UInt u4 = (uRate % 100);
    DEV_ERROR(TF("Rate = %d.%d   Idle = %d.%d"), u1, u2, u3, u4);
    CPlatform::SleepEx(1000);

    UShort us  = CPlatform::ByteSwap((UShort)0x1234);
    UInt   u   = CPlatform::ByteSwap((UInt)0x12345678);
    ULLong ull = CPlatform::ByteSwap((ULLong)0x1234567890ABCDEF);
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    ULong  ul  = CPlatform::ByteSwap((ULong)0x12345678);
#else
#if (__ARCH_TARGET__ == ARCH_TARGET_64)
    ULong  ul  = CPlatform::ByteSwap((ULong)0x1234567890ABCDEF);
#else
    ULong  ul  = CPlatform::ByteSwap((ULong)0x12345678);
#endif
#endif
    DEV_INFO(TF(" UShort ByteSwap : 0x1234--->%#X"), us);
    DEV_INFO(TF(" UInt   ByteSwap : 0x12345678--->%#X"), u);
    DEV_INFO(TF(" ULLong ByteSwap : 0x1234567890ABCDEF--->%#llX"), ull);

    CPlatform::MEMINFO mem;
    CPlatform::GetMemInfo(mem);
    DEV_ERROR(TF("---Memory Information---"));
    DEV_INFO(TF(" Memory Current Used       : %8d[%%]"), mem.uUsedPercent);
    DEV_INFO(TF(" Memory Page Size          : %8d[KB]"), mem.uPageSize);
    DEV_INFO(TF(" Memory Total Physical Size: %8d[MB]"), mem.uTotalPhys);
    DEV_INFO(TF(" Memory Avail Physical Size: %8d[MB]"), mem.uAvailPhys);
    CPlatform::SleepEx(2000);

    CPlatform::OSINFO os;
    CPlatform::GetOSInfo(os);
    DEV_DEBUG(TF("---Operation System Information---"));
    DEV_INFO(TF(" OS Type    : %s"),             GetOSType(os.uOSType));
    DEV_INFO(TF(" OS Version : %d.%d Build %d"), os.uMajor, os.uMinor, os.uBuild);
    DEV_INFO(TF(" OS Info    : %s"),             os.szInfo);
    CPlatform::SleepEx(2000);

    CPlatform::TIMEINFO time;
    CPlatform::GetTimeInfo(time);
    DEV_INFO(TF("Now : %d-%d-%d(%d), %d-%d-%d-%d"), time.usYear, time.usMonth, time.usDay, time.usWeekDay, time.usHour, time.usMinute, time.usSecond, time.usMSecond);
    CPlatform::SleepEx(2000);

    // 3.进程和线程 + 4.进程路径信息
    XChar szPath[LMT_MAX_PATH] = { 0 };
    CPlatform::GetPathInfo(szPath, LMT_MAX_PATH);
    DEV_INFO(TF("---Process & Thread Information---"));
    DEV_INFO(TF(" Process Id=%d, Thread Id=%d, Process Path=%s"), CPlatform::GetCurrentPId(), CPlatform::GetCurrentTId(), szPath);
    CPlatform::SleepEx(2000);
}

