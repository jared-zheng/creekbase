#include "stdafx.h"
#include "event.h"
#include "coreevent.h"

class CTestTickEventHandler : public CEventHandler
{
public:
    CTestTickEventHandler(void) { }
    virtual ~CTestTickEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t, uintptr_t, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, CEventBase&, ULLong) { return RET_FAIL; }
    virtual UInt OnHandle(uintptr_t, CStream&, ULLong) { return RET_FAIL; }

    virtual UInt OnHandle(uintptr_t utEvent, UInt uCount)
    {
        DEV_INFO(TF("Tick-event %lld, remain %d"), utEvent, uCount);
        if (utEvent == 9999)
        {
            CEventQueue::DestroyTickEvent(9999);
        }
        return RET_OKAY; // 非RET_OKAY返回结束当前定时器
    }
};

class CTestEvent : public CEventBase
{
public:
    CTestEvent(void) { DEV_INFO(TF("CTestEvent::CTestEvent(%p)"), this);  }
    virtual ~CTestEvent(void) { DEV_INFO(TF("CTestEvent::~CTestEvent(%p)"), this); }
    virtual size_t Length(void) const { return 0; }
    virtual void  Serialize(CStream&) { }
};

class CTestEventHandler : public CEventHandler
{
public:
    CTestEventHandler(void) { }
    virtual ~CTestEventHandler(void) { }

    virtual UInt OnHandle(uintptr_t utEvent, uintptr_t utData, ULLong ullParam)
    {
        DEV_INFO(TF("1. event %lld, ptr-->%p"), utEvent, utData);
        CTestEvent* p = (CTestEvent*)utData;
        //MDELETE p;
        return RET_OKAY; // 非RET_OKAY返回结束当前定时器
    }
    virtual UInt OnHandle(uintptr_t utEvent, CEventBase& EventRef, ULLong ullParam)
    {
        DEV_INFO(TF("2. event %lld, ptr-->%p"), utEvent, &EventRef);
        return RET_OKAY; // 非RET_OKAY返回结束当前定时器
    }
    virtual UInt OnHandle(uintptr_t utEvent, CStream& Stream, ULLong ullParam)
    {
        return RET_OKAY; // 非RET_OKAY返回结束当前定时器
    }
    virtual UInt OnHandle(uintptr_t, UInt)
    {
        return RET_OKAY; // 非RET_OKAY返回结束当前定时器
    }
};


void DemoEvent(void)
{
    CEventHandlerPtr p123 = MNEW CTestTickEventHandler;
    CEventQueue::CreateTickEvent(8888, 1000, *p123, 3);
    CEventQueue::CreateTickEvent(9999, 1000, *p123);

    CEventHandlerPtr p345 = MNEW CTestEventHandler;
    CEventQueuePtr EventQueuePtr;
    if (CEventQueue::EventQueue(EventQueuePtr, *p345))
    {
        EventQueuePtr->Init();

        CTestEvent* p = MNEW CTestEvent;
        EventQueuePtr->Add(7777, (uintptr_t)p);
        EventQueuePtr->Add(6666, *(MNEW CTestEvent));
    }
    DEV_INFO(TF("SleepEx 8000"));
    CPlatform::SleepEx(8000);

    if (EventQueuePtr != nullptr)
    {
        EventQueuePtr->Exit();
        EventQueuePtr = nullptr;
    }
}

