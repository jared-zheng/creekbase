// coredemo9.cpp : Platform������ʾ��
//

#include "stdafx.h"
#include "atomics.h"
#include "codec.h"
#include "crypto.h"
#include "streamfile.h"
#include "windows\fileversion.h"
#include "windows\ini.h"
#include "windows\reg.h"

#pragma comment(lib, "version.lib")

int _tmain(int argc, _TCHAR* argv[])
{
    CCString strAESKey = "WangZuKeJi";
    CBufReadStream bAESKey(strAESKey.Length(), (PByte)*strAESKey);
    CString strAESRet;


    Char szNull[] = "";
    Char sz1[] = "a";
    Char sz3[] = "abc";
    Char szLittle[] = "message digest";
    Char szLong[] = "abcdefghijklmnopqrstuvwxyz";
    Char szBig[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    CBufReadStream brs(CChar::Length(szNull), (PByte)szNull);
    CString strKey;
    CMD5::Crypto(brs, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), szNull, *strKey);

    CBufReadStream brs1(CChar::Length(sz1), (PByte)sz1);
    CMD5::Crypto(brs1, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), sz1, *strKey);

    CBufReadStream brs3(CChar::Length(sz3), (PByte)sz3);
    CMD5::Crypto(brs3, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), sz3, *strKey);

    CBufReadStream brsl(CChar::Length(szLittle), (PByte)szLittle);
    CMD5::Crypto(brsl, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), szLittle, *strKey);

    CBufReadStream brso(CChar::Length(szLong), (PByte)szLong);
    CMD5::Crypto(brso, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), szLong, *strKey);

    CBufReadStream brsb(CChar::Length(szBig), (PByte)szBig);
    CMD5::Crypto(brsb, strKey);
    DEV_INFO(TF("[%s]md5 : %s"), szBig, *strKey);


    CBufReadStream  brsAES1(CChar::Length(sz1), (PByte)sz1);
    CBufWriteStream bwsAES1(DEF::Align<size_t>(CChar::Length(sz1), CAES::AES_LEN_BLOCK));
    CAES::Encode(bAESKey, brsAES1, bwsAES1);
    bAESKey.Seek(0);
    brsAES1.Seek(0);
    CAES::Encode(bAESKey, brsAES1, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), sz1, *strAESKey, *strAESRet);
    bAESKey.Seek(0);
    CBufReadStream  brsAES11(bwsAES1.Size(), bwsAES1.GetBuf());
    CAES::Decode(bAESKey, brsAES11, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), sz1, *strAESKey, *strAESRet);

    CBufReadStream  brsAES2(CChar::Length(sz3), (PByte)sz3);
    CBufWriteStream bwsAES2(DEF::Align<size_t>(CChar::Length(sz3), CAES::AES_LEN_BLOCK));
    bAESKey.Seek(0);
    CAES::Encode(bAESKey, brsAES2, bwsAES2);
    bAESKey.Seek(0);
    brsAES2.Seek(0);
    CAES::Encode(bAESKey, brsAES2, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), sz3, *strAESKey, *strAESRet);
    bAESKey.Seek(0);
    CBufReadStream  brsAES22(bwsAES2.Size(), bwsAES2.GetBuf());
    CAES::Decode(bAESKey, brsAES22, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), sz3, *strAESKey, *strAESRet);

    CBufReadStream  brsAES3(CChar::Length(szLittle), (PByte)szLittle);
    CBufWriteStream bwsAES3(DEF::Align<size_t>(CChar::Length(szLittle), CAES::AES_LEN_BLOCK));
    bAESKey.Seek(0);
    CAES::Encode(bAESKey, brsAES3, bwsAES3);
    bAESKey.Seek(0);
    brsAES3.Seek(0);
    CAES::Encode(bAESKey, brsAES3, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szLittle, *strAESKey, *strAESRet);
    bAESKey.Seek(0);
    CBufReadStream  brsAES33(bwsAES3.Size(), bwsAES3.GetBuf());
    CAES::Decode(bAESKey, brsAES33, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szLittle, *strAESKey, *strAESRet);

    CBufReadStream  brsAES4(CChar::Length(szLong), (PByte)szLong);
    CBufWriteStream bwsAES4(DEF::Align<size_t>(CChar::Length(szLong), CAES::AES_LEN_BLOCK));
    bAESKey.Seek(0);
    CAES::Encode(bAESKey, brsAES4, bwsAES4);
    bAESKey.Seek(0);
    brsAES4.Seek(0);
    CAES::Encode(bAESKey, brsAES4, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szLong, *strAESKey, *strAESRet);
    bAESKey.Seek(0);
    CBufReadStream  brsAES44(bwsAES4.Size(), bwsAES4.GetBuf());
    CAES::Decode(bAESKey, brsAES44, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szLong, *strAESKey, *strAESRet);

    CBufReadStream  brsAES5(CChar::Length(szBig), (PByte)szBig);
    CBufWriteStream bwsAES5(DEF::Align<size_t>(CChar::Length(szBig), CAES::AES_LEN_BLOCK));
    bAESKey.Seek(0);
    CAES::Encode(bAESKey, brsAES5, bwsAES5);
    bAESKey.Seek(0);
    brsAES5.Seek(0);
    CAES::Encode(bAESKey, brsAES5, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], RET:[%s]"), szBig, *strAESKey, *strAESRet);
    bAESKey.Seek(0);
    CBufReadStream  brsAES55(bwsAES5.Size(), bwsAES5.GetBuf());
    CAES::Decode(bAESKey, brsAES55, strAESRet);
    DEV_INFO(TF("AES : Src:[%s], Key:[%s], Decode:[%s]"), szBig, *strAESKey, *strAESRet);
    getchar();
    CFileReadStream frs;
    if (frs.Create(TF("D:\\Download\\TCPView.zip")))
    {
        CString strOut;
    
        CMD5::Crypto(frs, strOut);
        DEV_INFO(TF("md5 : %s"), *strOut);
    }
    frs.Close();


    CFileVersion fv;
    if (fv.GetFileVersion(TF("coreDebug.dll")))
    {
        ULong uFMajor = 0;
        ULong uFMinor = 0;
        ULong uPMajor = 0;
        ULong uPMinor = 0;
        fv.GetFileVersion(uFMajor, uFMinor);
        fv.GetProductVersion(uPMajor, uPMinor);
        DEV_INFO(TF("FileVersion=%s[%X, %X], ProductVersion=%s[%X, %X]"),
            fv.GetFileVersion(), uFMajor, uFMinor,
            fv.GetProductVersion(), uPMajor, uPMinor);
    }

    CIni ii(TF("D:\\test.ini"));
    if (ii.IsExist() == false)
    {
        ii.CreateINI(TF("D:\\test.ini"));
    }
    if (ii.IsSection(TF("test1")) == false)
    {
        ii.CreateSetion(TF("test1"));
    }
    DWORD dd = ::GetLastError();
    ii.SetKeyValue(TF("test1"), TF("Name"), TF("Hello"));
    ii.SetKeyIntValue(TF("test1"), TF("Value"), 100);

    CString strName;
    ii.GetKeyValue(TF("test1"), TF("Name"), strName);
    UInt uValue1 = ii.GetKeyIntValue(TF("test1"), TF("Value"), 0);
    DEV_INFO(TF("Name=%s, Value=%d"), *strName, uValue1);


    CReg reg;
    if (reg.Open(HKEY_CURRENT_USER, TF("Software\\WinRAR\\ArcHistory")) == ERROR_SUCCESS)
    {
        XChar szTemp[LMT_BUF] = {0};
        ULong uLen = LMT_BUF;
        reg.QueryStringValue(TF("0"), szTemp, &uLen);
        DEV_INFO(TF("HKEY_CURRENT_USER\\SOFTWARE\\WinRAR\\ArcHistory\\0=%s"), szTemp);
    }
    getchar();
    return 0;
}

