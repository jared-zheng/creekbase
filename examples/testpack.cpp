#include "stdafx.h"
#include "testpack.h"


CTestPack* CTestPack::Create(uintptr_t, CStream& Stream, ULLong)
{
    CTestPack* pPkt = nullptr;

    UInt uEvent = 0;
    {
        CStreamSeekScope scope(Stream);
        Stream >> uEvent;
    }
    switch (uEvent)
    {
    case TEST_EVENT_LIVE:
    case TEST_EVENT_LIVEACK:
    case TEST_EVENT_LOGOUT:
        {
            pPkt = MNEW CTestPack(uEvent);
        }
        break;
    case TEST_EVENT_LOGIN:
        {
            pPkt = MNEW CLoginPacket;
        }
        break;
    case TEST_EVENT_LOGINACK:
        {
            pPkt = MNEW CAckPacket(TEST_EVENT_LOGINACK);
        }
        break;
    case TEST_EVENT_CHAT:
        {
            pPkt = MNEW CChatPacket;
        }
        break;
    case TEST_EVENT_CHATACK:
        {
            pPkt = MNEW CAckPacket(TEST_EVENT_CHATACK);
        }
        break;
    case TEST_EVENT_LOGOUTACK:
        {
            pPkt = MNEW CAckPacket(TEST_EVENT_LOGOUTACK);
        }
        break;
    default:
        {
        }
    }
    if (pPkt != nullptr)
    {
        pPkt->Serialize(Stream);
    }
    return pPkt;
}



