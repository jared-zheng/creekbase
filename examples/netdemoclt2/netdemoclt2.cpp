#include "stdafx.h"
#include "..\testpack.h"

CNetworkPtr          NetPtr;
CSyncEvent           gs_Event;
CNETTraits::Socket   gs_Connect = 0;
CNETTraits::Socket   gs_UDP     = 0;

class CNetEventHandler : public CTestPackEventHandler
{
public:
    CNetEventHandler(void) { }
    virtual ~CNetEventHandler(void) { }

    virtual bool OnTcpDispatch(const PacketPtr& PktPtr, PTCP_PARAM pTcp)
    {
        assert(pTcp->sSocket == gs_Connect);
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_LOGINACK:
            {
                CAckPacket* pPack = static_cast<CAckPacket*>(PktPtr.Get());
                DEV_INFO(TF("#########Login ack is %d"), pPack->GetAck());

                CChatPacket Chat;
                Chat.SetChat(TF("(*^_^*)"));
                NetPtr->SendTo(gs_UDP, Chat, TF("localhost"), 19998);

                NetPtr->Send(gs_Connect, Chat);
            }
            break;
        case TEST_EVENT_CHATACK:
            {
                CAckPacket* pPack = static_cast<CAckPacket*>(PktPtr.Get());
                DEV_INFO(TF("#########Chat ack is %d"), pPack->GetAck());

                CChatPacket Chat;
                Chat.SetChat(TF("ε=ε=ε=(~￣▽￣)~"));
                NetPtr->SendTo(gs_UDP, Chat, TF("localhost"), 19998);

                CTestPack Logout(TEST_EVENT_LOGOUT);
                NetPtr->Send(gs_Connect, Logout);
            }
            break;
        case TEST_EVENT_LOGOUTACK:
            {
                CAckPacket* pPack = static_cast<CAckPacket*>(PktPtr.Get());
                DEV_INFO(TF("#########Logout ack is %d"), pPack->GetAck());

                gs_Event.Signal();
            }
            break;
        default:
            {
                // live-packet
            }
        }
        return true;
    }

    virtual bool OnUdpDispatch(const PacketPtr& PktPtr, PUDP_PARAM pUdp)
    {
        assert(pUdp->sSocket == gs_UDP);
        switch (PktPtr->GetEvent())
        {
        case TEST_EVENT_CHATACK:
            {
                CAckPacket* pPack = static_cast<CAckPacket*>(PktPtr.Get());
                DEV_INFO(TF("$$$$$$$$$ChatAck---%d"), pPack->GetAck());
            }
            break;
        default:
            {
            }
        }
        return true;
    }

    virtual bool OnTcpAccept(Socket sAccept, Socket sListen)
    {
        DEV_INFO(TF("###Accept socket=%p, listen socket %p"), sAccept, sListen);
        return true;
    }

    virtual bool OnTcpConnect(UInt uError, Socket sConnect)
    {
        DEV_INFO(TF("###Connect return errid=%d, socket %p"), uError, sConnect);
        assert(sConnect == gs_Connect);

        CLoginPacket Login;
        Login.SetName(TF("Hello"));
        Login.SetPW(TF("World"));

        NetPtr->Send(gs_Connect, Login);

        CChatPacket Chat;
        Chat.SetChat(TF("o(*￣▽￣*)o"));
        NetPtr->SendTo(gs_UDP, Chat, TF("localhost"), 19998);
        return true;
    }

    virtual bool OnTcpRecv(size_t, PTCP_PARAM)
    {
        return true;
    }

    virtual bool OnTcpSend(UInt, Socket)
    {
        return true;
    }

    virtual bool OnTcpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("###Close tcp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == gs_Connect)
        {
            // ...
        }
        return true;
    }

    virtual bool OnUdpRecv(size_t, PUDP_PARAM)
    {
        return true;
    }

    virtual bool OnUdpSend(UInt, Socket)
    {
        return true;
    }

    virtual bool OnUdpClose(Socket sSocket, ULLong ullLiveData)
    {
        DEV_INFO(TF("$$$Close udp socket=%p data[%llx]"), sSocket, ullLiveData);
        if (sSocket == gs_UDP)
        {
            // ...
        }
        return true;
    }
};

int _tmain(int argc, _TCHAR* argv[])
{
    CoreInit();

#ifdef __RUNTIME_STATIC__
    CTRefCountPtr<CSubSystem> load = CreateNetworkStatic(UUID_OF(CNetworkSystem));
#else
    CTLoader<CSubSystem> load;
    if (load.Load(UUID_OF(CNetworkSystem), NETWORK_MODULE_NAME))
#endif
    {
        if (load->CreateComponent(UUID_OF(CNetwork), NetPtr.Cast<CComponent>())) // 
        {
            CNETTraits::NET_ATTR attr;
            attr.nAttrs  = CNETTraits::ATTR_THREAD;
            attr.nThread = 2;
            // 注意 : 数据包头部构造器作用范围为当前CNetwork实例, 并且收发双方都需要使用一致的头部构造器
            //       如果单独的连接不需要头部构造器, 可以单独增加SOCKET_EXCLUDE_HEAD属性
            //       或者再创建一个CNetwork实例, 使用其他头部构造器
            if (NetPtr->Init(attr, (MNEW CNetEventHandler), MNEW CTestPackBuild) == RET_OKAY)
            {
                gs_UDP     = NetPtr->Create(0, nullptr, CNETTraits::SOCKET_UDP);
                gs_Connect = NetPtr->Create();
                if (gs_Connect != 0)
                {
                    NetPtr->Connect(gs_Connect, 19998, TF("localhost"));
                    gs_Event.Wait();
                }
            }
        }
    }
    CoreExit();
    getchar();
    return 0;
}

