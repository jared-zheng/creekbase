#include "stdafx.h"
#include "exception.h"

void func(void)
{
    Int *p = nullptr;

    *p = 0;
}

void DemoException(void)
{
    try
    {
        throw CException(12, TF("testthrow"));
    }
    catch (CException& e)
    {
        DEV_WARN(TF("Exception %d-%s"), e.GetCode(), e.GetInfo().GetBuffer());
    }
#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    CSEHExceptionFilter::Init();

    func();

    CSEHExceptionFilter::Exit();
#else
    CExceptionTerminater::Init();

    func();

    CExceptionTerminater::Exit();

#endif
}

