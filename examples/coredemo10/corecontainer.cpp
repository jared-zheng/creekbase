#include "stdafx.h"
#include "refcount.h"
#include "container.h"

class CTestCommon : public MObject
{
public:
    CTestCommon(Int nInt = 0, void* pV = nullptr)
    : m_nT(nInt)
    , m_pT(pV)
    {
    }

    CTestCommon(const CTestCommon& t)
    : m_nT(t.m_nT)
    , m_pT(t.m_pT)
    {
    }

    CTestCommon& operator=(const CTestCommon& t)
    {
        m_nT = t.m_nT;
        m_pT = t.m_pT;

        return (*this);
    }

    void Print(void)
    {
        DEV_INFO(TF("%d-->%p"), m_nT, m_pT);
    }

    friend bool operator==(const CTestCommon& t1, const CTestCommon& t2)
    {
        return ((t1.m_nT == t2.m_nT) && (t1.m_pT == t2.m_pT));
    }

private:
    Int   m_nT;
    void* m_pT;
};

class CTTT : public MObject
{
public:
    CTTT(Int nT = 0) : m_nT(nT) { DEV_INFO(TF("###########CTTT::CTTT")); }
    ~CTTT(void) { DEV_INFO(TF("###########CTTT::~CTTT")); }
    CTTT(const CTTT& aSrc) : m_nT(aSrc.m_nT) { DEV_INFO(TF("###########CTTT::CTTTCopy")); }
    CTTT& operator=(const CTTT& aSrc)
    {
        m_nT = aSrc.m_nT;
        DEV_INFO(TF("###########CTTT::CTTT Operator"));
        return *this;
    }
public:
    Int m_nT;
};

typedef CTScopePtr<CTTT>   CTTTPtr;

void DemoContainer(void)
{
    Int i = 0;
    Int j = -1;

    ////////////////////////////////////////////////////////////////////////////////////////////
    // basic type array test
    CTArray<Int> aInt;
    aInt.Add(5000, false);
    for (i = 0; i < 5000; ++i)
    {
        aInt.SetAt(i, rand());
    }
    for (i = 0; i < 2500; ++i)
    {
        aInt.Add(rand());
    }
    aInt.Insert(2500, 2500);
    for (i = 0; i < 2500; ++i)
    {
        aInt.SetAt(i + 2500, 0x7fffff);
    }

    i %= rand();
    j = aInt.Find(i);
    DEV_INFO(TF("Find %d in Array at %d, Array alloc size=%d, size=%d, grow=%d"), i, j, aInt.GetAllocSize(), aInt.GetSize(), aInt.GetGrow());

    aInt.RemoveAt(68);
    DEV_INFO(TF("Array size is =%d"), aInt.GetSize());

    aInt.Remove(rand());
    DEV_INFO(TF("Array size is =%d"), aInt.GetSize());

    for (i = 0; i < aInt.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%d"), i, *aInt.GetItem(i));
        // == DEV_INFO(TF("%d-->%d"), i, aInt[i]);
    }

    DEV_INFO(TF("-------------------------------------------"));
    CTArray<Int> aInt2(1024);
    aInt2.Copy(aInt);
    for (i = 0; i < aInt2.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%d"), i, aInt2[i]);
    }
    aInt2.RemoveAll();

    DEV_INFO(TF("-------------------------------------------"));
    CTArray<Int> aInt3(512);
    for (i = 0; i < 100; ++i)
    {
        aInt3.Add(0x0fffff);
    }
    aInt3.Append(aInt);
    for (i = 0; i < aInt3.GetSize(); ++i)
    {
        DEV_INFO(TF("%d-->%d"), i, aInt3[i]);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////
    // object array test
    CTArray<CTestCommon> aTC;
    aTC.Add(5000, false);
    for (i = 0; i < 5000; ++i)
    {
        aTC.SetAt(i, CTestCommon(i, (void*)(0x00FF0000 + i)));
    }
    for (i = 0; i < 2500; ++i)
    {
        aTC.Add(CTestCommon(5000+i, (void*)(0x00FF1000 + i)));
    }
    aTC.Insert(2500, 2500);
    for (i = 0; i < 2500; ++i)
    {
        aTC.SetAt(i + 2500, CTestCommon(10000+i, (void*)(0x00FF2000 + i)));
    }

    CTestCommon tt(100, (void*)(0x00FF0000 + 100));
    j = aTC.Find(tt);
    DEV_INFO(TF("Find tt in Array at %d, Array alloc size=%d, size=%d, grow=%d"), j, aTC.GetAllocSize(), aTC.GetSize(), aTC.GetGrow());

    aTC.RemoveAt(68);
    DEV_INFO(TF("Array size is =%d"), aTC.GetSize());

    aTC.Remove(tt);
    DEV_INFO(TF("Array size is =%d"), aTC.GetSize());

    for (i = 0; i < aTC.GetSize(); ++i)
    {
        aTC[i].Print();
    }

    DEV_INFO(TF("-------------------------------------------"));
    CTArray<CTestCommon> aTC2(1024);
    //aTC2.Copy(aTC);
    aTC2 = std::move(aTC);
    for (i = 0; i < aTC2.GetSize(); ++i)
    {
        (aTC2.GetItem(i))->Print();
    }
    aTC2.RemoveAll();

    DEV_INFO(TF("-------------------------------------------"));
    CTArray<CTestCommon> aTC3(512);
    for (i = 0; i < 100; ++i)
    {
        aTC3.Add(CTestCommon(80000+i, (void*)(0x00FFFF00 + i)));
    }
    aTC3.Append(aTC);
    for (i = 0; i < aTC3.GetSize(); ++i)
    {
        (aTC3.GetItem(i))->Print();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    // basic type list test
    CTList<Int> lInt;
    for (i = 0; i < 5000; ++i)
    {
        lInt.AddTail(rand());
    }
    for (i = 0; i < 5000; ++i)
    {
        lInt.AddHead(0x7fffff);
    }

    i %= rand();
    PINDEX p = lInt.Find(i);
    j = -1;
    if (p != nullptr)
    {
        j = lInt.GetAt(p);
    }
    DEV_INFO( TF("Find %d in List at %d, List alloc size=%d, size=%d, grow=%d"), i, j, lInt.GetAllocSize(), lInt.GetSize(), lInt.GetGrow());

    lInt.RemoveHeadDirect();
    DEV_INFO( TF("List size is =%d"), lInt.GetSize());

    i = 0;
    p = lInt.GetHeadIndex();
    while(p != nullptr)
    {
        DEV_INFO( TF("%d-->%d"), i, lInt.GetAt(p));
        lInt.GetNext(p);
        ++i;
    }

    DEV_INFO(TF("-------------------------------------------"));
    CTList<Int> lInt2(1024);
    for (i = 0; i < 100; ++i)
    {
        lInt2.AddHead(110011);
    }
    lInt2.AddHeadList(lInt);
    i = 0;
    p = lInt2.GetHeadIndex();
    while(p != nullptr)
    {
        DEV_INFO( TF("%d-->%d"), i, lInt2.GetAt(p));
        lInt2.GetNext(p);
        ++i;
    }
    lInt2.RemoveAll();
    DEV_INFO(TF("-------------------------------------------"));
    CTList<Int> lInt3(347);
    for (i = 0; i < 50; ++i)
    {
        lInt3.AddTail(120012);
    }
    p = lInt3.GetTailIndex();
    for (i = 0; i < 20; ++i)
    {
        lInt3.GetPrev(p);
    }
    for (i = 0; i < 25; ++i)
    {
        lInt3.InsertAfter(p, 121200+i);
    }
    for (i = 0; i < 25; ++i)
    {
        lInt3.InsertBefore(p, 121800+i);
    }

    lInt3.AddTailList(lInt);
    lInt3.MoveToTail(p);
    i = 0;
    p = lInt3.GetHeadIndex();
    while(p != nullptr)
    {
        DEV_INFO( TF("%d-->%d"), i, lInt3.GetAt(p));
        lInt3.GetNext(p);
        ++i;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // object list test
    CTList<CTestCommon> lTC;
    for (i = 0; i < 5000; ++i)
    {
        lTC.AddTail(CTestCommon(5000 + i, (void*)(0x00FF1000 + i)));
    }
    for (i = 0; i < 5000; ++i)
    {
        lTC.AddHead(CTestCommon(i, (void*)(0x00FF0000 + i)));
    }

    CTestCommon lt(100, (void*)(0x00FF0000 + 100));
    p = lTC.Find(lt);
    if (p != nullptr)
    {
        CTestCommon& tref = lTC.GetAt(p);
        tref.Print();
    }
    DEV_INFO( TF("Find tt in List at %p List alloc size=%d, size=%d, grow=%d"), p, lTC.GetAllocSize(), lTC.GetSize(), lTC.GetGrow());

    lTC.RemoveHeadDirect();
    DEV_INFO( TF("List size is =%d"), lTC.GetSize());

    i = 0;
    p = lTC.GetHeadIndex();
    while(p != nullptr)
    {
        lTC.GetNext(p).Print();
    }

    DEV_INFO(TF("-------------------------------------------"));
    CTList<CTestCommon> lTC2(1024);
    for (i = 0; i < 100; ++i)
    {
        lTC2.AddHead(CTestCommon(20000+i, (void*)(0x02FF0000 + i)));
    }
    lTC2.AddHeadList(lTC);
    p = lTC2.GetHeadIndex();
    while(p != nullptr)
    {
        lTC2.GetNext(p).Print();
    }
    lTC2.RemoveAll();
    DEV_INFO(TF("-------------------------------------------"));
    CTList<CTestCommon> lTC3(347);
    for (i = 0; i < 50; ++i)
    {
        lTC3.AddTail(CTestCommon(30000+i, (void*)(0x03FF0000 + i)));
    }
    p = lTC3.GetTailIndex();
    for (i = 0; i < 20; ++i)
    {
        lTC3.GetPrev(p);
    }
    for (i = 0; i < 25; ++i)
    {
        lTC3.InsertAfter(p, CTestCommon(60000+i, (void*)(0x06FF0000 + i)));
    }
    for (i = 0; i < 25; ++i)
    {
        lTC3.InsertBefore(p, CTestCommon(90000+i, (void*)(0x09FF0000 + i)));
    }

    lTC3.AddTailList(lTC);
    lTC3.MoveToTail(p);
    p = lTC3.GetHeadIndex();
    while(p != nullptr)
    {
        lTC3.GetNext(p).Print();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // basic queue
    CTQueue<Int> que;
    for (i = 0; i < 10000; ++i)
    {
        que.In(i);
    }
    while(que.IsEmpty() == false)
    {
        DEV_INFO( TF("%d"), que.Out());
    }
    DEV_INFO( TF("queue size = %d"), que.GetSize());
    ////////////////////////////////////////////////////////////////////////////////////////////
    // object queue
    CTQueue<CTestCommon> quet;
    for (i = 0; i < 10000; ++i)
    {
        quet.In(CTestCommon(i, (void*)(0x04FF0000 + i)));
    }
    while(quet.IsEmpty() == false)
    {
        quet.Out().Print();
    }
    DEV_INFO( TF("queue size = %d"), quet.GetSize());

    ////////////////////////////////////////////////////////////////////////////////////////////
    // basic stack
    CTStack<Int> stk;
    for (i = 0; i < 10000; ++i)
    {
        stk.Push(i);
    }
    while(stk.IsEmpty() == false)
    {
        DEV_INFO( TF("%d"), stk.Pop());
    }
    DEV_INFO( TF("stack size = %d"), stk.GetSize());
    ////////////////////////////////////////////////////////////////////////////////////////////
    // object stack
    CTStack<CTestCommon> stkt;
    for (i = 0; i < 10000; ++i)
    {
        stkt.Push(CTestCommon(i, (void*)(0x05FF0000 + i)));
    }
    while(stkt.IsEmpty() == false)
    {
        stkt.Pop().Print();
    }
    DEV_INFO( TF("stack size = %d"), stkt.GetSize());

    ////////////////////////////////////////////////////////////////////////////////////////////
    // map testCTMap
    CTMap<Int, CTestCommon> mTT;
    for (i = 0; i < 5000; ++i)
    {
        mTT.Add(i, CTestCommon(i, (void*)(0x07FF0000 + i)));
    }
    for (i = 0; i < 5000; ++i)
    {
        mTT.Add(i+5000, CTestCommon(i+5000, (void*)(0x08FF0000 + i)));
    }

    i %= rand();
    CTMap<Int, CTestCommon>::PAIR* pr = mTT.Find(i);
    if (pr != nullptr)
    {
        pr->m_V.Print();
    }
    DEV_INFO( TF("Find obj in Map at %p, Map alloc size=%d, size=%d, grow=%d"), pr, mTT.GetAllocSize(), mTT.GetSize(), mTT.GetGrow());

    mTT.Remove(68);
    DEV_INFO( TF("Map size is =%d"), mTT.GetSize());

    PINDEX index = mTT.GetFirstIndex();
    while (index != nullptr)
    {
        mTT.GetNext(index)->m_V.Print();
    }

    DEV_INFO(TF("-------------------------------------------"));
    CTMap<Int, CTestCommon> mTT2(1024);
    for (i = 0; i < 100; ++i)
    {
        mTT2.Add(i+20000, CTestCommon(i+20000, (void*)(0x20FF0000 + i)));
    }
    mTT2.Append(mTT);

    index = mTT2.GetFirstIndex();
    while (index != nullptr)
    {
        mTT2.GetNext(index)->m_V.Print();
    }
    mTT2.RemoveAll();
    DEV_INFO(TF("-------------------------------------------"));
    CTMap<Int, CTestCommon> mTT3(347);
    for (i = 0; i < 50; ++i)
    {
        mTT3.Add(i+30000, CTestCommon(i+30000, (void*)(0x30FF0000 + i)));
    }
    index = mTT3.GetLastIndex();
    for (i = 0; i < 20; ++i)
    {
        mTT3.GetPrev(index);
    }
    mTT3.RemoveAt(index);
    index = mTT3.GetFirstIndex();
    while (index != nullptr)
    {
        mTT3.GetNext(index)->m_V.Print();
    }
    DEV_INFO(TF("-------------------------------------------"));
    mTT3.Copy(mTT);
    index = mTT3.GetFirstIndex();
    while (index != nullptr)
    {
        mTT3.GetNext(index)->m_V.Print();
    }


    CTMap<Int, CTTTPtr> mmttt;
    for (Int i = 0; i < 100; ++i)
    {
        mmttt.Add(i);
    }
    index = mmttt.GetFirstIndex();
    while (index != nullptr)
    {
        CTMap<Int, CTTTPtr>::PAIR* ttt = mmttt.GetNext(index);
        ttt->m_V = MNEW CTTT;
        DEV_INFO(TF("@@@@@@@@@@@@@@@@@@%d-->%d"), ttt->m_K, ttt->m_V->m_nT);

        ttt->m_V->m_nT = ttt->m_K;
        DEV_INFO(TF("$$$$$$$$$$$$$$$$$%d-->%d"), ttt->m_K, ttt->m_V->m_nT);
    }
}

