#include "stdafx.h"
#include "refcount.h"


class CRefTest : public CTRefCount<CRefTest>
{
public:
    CRefTest(void)
    {
        DEV_INFO(TF("CRefTest ref=%d"), m_Counter.GetCount());
    }
    virtual ~CRefTest(void)
    {
        DEV_INFO(TF("~CRefTest ref=%d"), m_Counter.GetCount());
    }
    virtual void Print(void)
    {
        DEV_INFO(TF("ref=%d"), m_Counter.GetCount());
    }
};

class CRefD : public CRefTest
{
public:
    CRefD(void)
    {
        DEV_INFO(TF("CRefD ref=%d"), m_Counter.GetCount());
    }
    virtual ~CRefD(void)
    {
        DEV_INFO(TF("~CRefD ref=%d"), m_Counter.GetCount());
    }
    virtual void Print(void)
    {
        DEV_INFO(TF("Print CRefD ref=%d"), m_Counter.GetCount());
    }
};

class A : public MObject
{
public:
    A(void) { DEV_INFO(TF("A"));}
    ~A(void) { DEV_INFO(TF("~A"));}
    Int i;
};

class B : public MObject
{
public:
    B(void) { DEV_INFO(TF("B"));}
    virtual ~B(void) { DEV_INFO(TF("~B"));}
    bool b;
};

class C : public B
{
public:
    C(void) { DEV_INFO(TF("C")); }
    virtual ~C(void) { DEV_INFO(TF("~C")); }
    Short s;
};

class D : public MObject
{
public:
    D(void) { DEV_INFO(TF("D")); }
    virtual ~D(void) { DEV_INFO(TF("~D")); }
public:
    CTSharePtr<class E> _e;
    //CTWeakPtr<class E> _e;
};

class E : public MObject
{
public:
    E(void) { DEV_INFO(TF("E")); }
    virtual ~E(void) { DEV_INFO(TF("~E"));}
public:
    CTSharePtr<class D> _d;
    //CTWeakPtr<class D> _d;
};

void Test2(CTRefCountPtr<CRefTest>& tt)
{
    tt = MNEW CRefD;
}


void DemoRefcount(void)
{
    CTRefCountPtr<CRefD> rp;
    Test2(rp.Cast<CRefTest>());
    rp->Print();

    {
        CTRefCountPtr<CRefD> rp(MNEW CRefD);
        CTRefCountPtr<CRefTest> tptr = rp; // == CTRefCountPtr<CRefTest> tptr(rp);

                                           //    CTRefCountPtr<CRefD> rp2 = tptr;

        tptr->Print();
        //tptr->AddRef();
    }
    {
        CTSharePtr<A> sp(MNEW A);
        CTSharePtr<C> spc(MNEW C);

        CTSharePtr<B> sp2(MNEW B);

        CTSharePtr<B> spb1(spc);
        CTSharePtr<B> spb2 = spc;

        //    CTSharePtr<C> spc1(sp2);   // ����
        //    CTSharePtr<C> spc2(spb2);  // ����
        CTScopePtr<A> ssp(MNEW A);

        CTScopePtr<A> sssp(std::move(ssp));

        CTScopePtr<A> ssspp = std::move(sssp);

    }
    {
        CTSharePtr<D> spd(MNEW D);
        CTSharePtr<E> spe(MNEW E);
        spd->_e = spe;
        spe->_d = spd;

        CTWeakPtr<E> wpe;
        {
            CTSharePtr<E> spe2(MNEW E);
            wpe = spe2;
        }
        CTSharePtr<E> spe3(wpe);
        CTWeakPtr<E> wpe2(wpe);
    }

}

