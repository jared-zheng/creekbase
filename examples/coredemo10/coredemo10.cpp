// coredemo10.cpp : Platform功能演示。
//

#include "stdafx.h"
#include "corecontainer.h"
#include "coreexception.h"
#include "corekvfp.h"
#include "corestream.h"
#include "filelog.h"
#include "markup.h"
#include "module.h"
#include "rand.h"
#include "sync.h"
#include "timescope.h"

//CFileLog log_;

int _tmain(int argc, _TCHAR* argv[])
{
    //DemoContainer();
    //DemoException();

//    LOG_INFO(log_, TF("INFO"));
//    LOGV_INFO(log_, TF("INFO=%d"), 100);
//
//    CKVNode kv;
//    CFileReadStream frs;
//    if (frs.Create(TF("./json_data.txt")))
//    {
//        CKVStore s2(ENT_UTF8);
//        s2.Load(kv, frs);
//    }
//    CFileWriteStream fws2;
//    if (fws2.Create(TF("./kv32.txt")))
//    {
//        //kv[TF("test")].SetValue(TF("slkjfslkd加快速度v和"));
//        CKVStore s3(ENT_UTF8);
//        s3.Save(kv, fws2, ENT_UTF32);
//    }
//
//    Int nState = kv[TF("state")].GetInt();
//    VAR_TYPE eType = kv[TF("err")].GetType();
//
//    CKVNode& k0 = kv[TF("data")];
//    CKVNode& k1 = k0[TF("partnerteamlist")];
//    CKVNode& k2 = k1[4];
//
//    Int nId = k2[TF("pteamId")].GetInt();
//
//    CKVString strTitle;
//    k2[TF("ptitle")].GetString(strTitle);
//
//    LOGV_INFO(log_, TF("pteamId=%d, ptitle=%s"), nId, *strTitle);
//
//    CModule module;
//#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
//    module.Attach(TF("C:\\Windows\\system32\\ntdll.dll"));
//#else
//    module.Attach(TF("/usr/lib/librt.so"));
//#endif
//    LOGV_INFO(log_, TF("module handle=%p"), (Module)module);
//
//
//    CLCGRand LCG;
//    CMPGRand MPG;
//    for (Int i = 0; i < 1024; ++i)
//    {
//        LOGV_INFO(log_, TF("%d ------- %d"), LCG.Rand(), MPG.Rand());
//    }
//
//    CSyncCounter Counter;
//    CSyncCounterScope scope1(Counter);
//    DEV_INFO(TF("counter = %d"), Counter.GetCount());
//
//    CSyncLock  Lock;
//    CSyncLockScope scope2(Lock);
//
//    CSyncMutex Mutex;
//    CSyncMutexScope scope3(Mutex);
//
//    CSyncEvent Event;
//    CSyncEventScope scope4(Event);
//
//    CSyncSema SEmaphore;
//    CSyncSemaScope scope5(SEmaphore);
//
//    CRWLock RWLock;
//    {
//        CRWLockReadScope scope6(RWLock);
//    }
//    {
//        CRWLockWriteScope scope7(RWLock);
//    }
//#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
//    CWaitableTimer Timer;
//    Timer.SetTimer(-200 * 10000, 400);
//    for (Int i = 0; i < 4; ++i)
//    {
//        Timer.Wait();
//        DEV_INFO(TF("%dth Timer"), i);
//    }
//#endif

    //DemoStream();

    //CTime tTime1;
    //CTime tTime2;

    //CTimeScope ts(0, 0, 3, 0);
    //tTime2 += ts;

    //if (tTime2 > tTime1)
    //{
    //    DEV_INFO(TF("Time2!!!"));
    //}
    //Int nYear   = tTime2.GetYear();
    //Int nMonth  = tTime2.GetMonth();
    //Int nDay    = tTime2.GetDay();
    //Int nHour   = tTime2.GetHour();
    //Int nMinute = tTime2.GetMinute();
    //Int nSecond = tTime2.GetSecond();
    //DEV_INFO(TF("Time2 %d-%02d-%02d %02d:%02d:%02d"), nYear, nMonth, nDay, nHour, nMinute, nSecond);

    //CString str = tTime2.Format(TF("%Y-%B-%j %H:%M:%S"));
    //DEV_INFO(TF("Time2 = %s"), *str);
    return 0;
}

