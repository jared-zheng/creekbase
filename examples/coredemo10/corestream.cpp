#include "stdafx.h"
#include "streamfile.h"
#include "streambuf.h"

#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
size_t stAll  = 0;
size_t stOkay = 0;
BYTE  bBuf[1024 * 1024];

CAFileReadStream  afrs;
CAFileWriteStream afws;


VOID WINAPI AIO_READ(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
    if (dwErrorCode == 0)
    {
        DEV_INFO(TF("file aio read : %d + %d >= %d"), stOkay, dwNumberOfBytesTransfered, stAll);
        stOkay += dwNumberOfBytesTransfered;
        lpOverlapped->Offset += dwNumberOfBytesTransfered;
        afws.Write(bBuf, dwNumberOfBytesTransfered);
    }
    else
    {
        DEV_INFO(TF("file aio read err = %d, %d"), dwErrorCode, dwNumberOfBytesTransfered);
    }
}

VOID WINAPI AIO_WRITE(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
    if (dwErrorCode == 0)
    {
        DEV_INFO(TF("file aio write : %d"), dwNumberOfBytesTransfered);
        lpOverlapped->Offset += dwNumberOfBytesTransfered;
        if (stAll > stOkay)
        {
            afrs.Read(bBuf, 1024 * 1024);
        }
    }
}
#endif

void DemoStream(void)
{
    CTBufWriteStreamFix<1000> bws;
    bws << 1000 << TF("Hello") << 2000;

    CFileWriteStream fws;
    if (fws.Create(TF("testme.txt")))
    {
        fws.Write(bws.GetBuf(), bws.Tell());
        fws << 3000 << TF("World!") << 4000;
    }
    fws.Close();

    CTBufReadStreamFix<1000> brs;
    CFileReadStream frs;
    if (frs.Create(TF("testme.txt")))
    {
        frs.Read(brs.GetBuf(), 1000);
        Int i1, i2, i3, i4;
        CString str1, str2;
        brs >> i1 >> str1 >> i2 >> i3 >> str2 >> i4;
        DEV_INFO(TF("all = %d, %s---%s"), i1 + i2 + i3 + i4, *str1, *str2);
    }

    if (fws.Create(TF("testme2.txt"), FILEF_NEW_ALWAYS))
    {
        fws <= TF("Hello") <= TF(" World!");
    }














    //#if (__PLATFORM_TARGET__ == PLATFORM_TARGET_WINDOWS)
    //
    //    // 1. callback
    //    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0, 0, AIO_READ))
    //    {
    //        size_t stRead = 0;
    //        stAll = afrs.Size();
    //        if (afws.Create(TF("D:\\Download\\Win81sdkbak1.rar"), 0, 0, AIO_WRITE))
    //        {
    //            afrs.Read(bBuf, 1024 * 1024);
    //            //while (afrs.Wait() && (stAll > stOkay))
    //            while ((afrs.Wait(stRead) == RET_OKAY) && (stAll > stOkay))
    //            {
    //                DEV_INFO(TF("file aio ID : %d, %x --- buf=%X"), ::GetLastError(), stRead, stOkay);
    //            }
    //        }
    //    }
    //
    //    // 2. event
    //    CSyncEvent ReadEvent, WriteEvent;
    //    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0, 0, nullptr, ReadEvent.m_hEvent))
    //    {
    //        stAll = afrs.Size();
    //        if (afws.Create(TF("D:\\Download\\Win81sdkbak2.rar"), 0, 0, nullptr, WriteEvent.m_hEvent))
    //        {
    //            Int    nRet   = 0;
    //            size_t stRead = 0;
    //            while (stAll > stOkay)
    //            {
    //                afrs.Read(bBuf, 1024 * 1024);
    //                nRet = ReadEvent.Wait((UInt)TIMET_INFINITE);
    //                if (nRet == RET_OKAY)
    //                {
    //                    afrs.Wait(stRead);
    //                    afrs.GetAttr().Offset += (DWORD)stRead;
    //                    stOkay += stRead;
    //
    //                    afws.Write(bBuf, stRead);
    //                    nRet = WriteEvent.Wait((UInt)TIMET_INFINITE);
    //                    if (nRet == RET_OKAY)
    //                    {
    //                        afws.Wait(stRead);
    //                        afws.GetAttr().Offset += (DWORD)stRead;
    //                        continue;
    //                    }
    //                }
    //                break;
    //            }
    //        }
    //    }
    //
    //    // 3. wait
    //    if (afrs.Create(TF("D:\\Download\\Win81sdk.rar"), 0))
    //    {
    //        stAll = afrs.Size();
    //        if (afws.Create(TF("D:\\Download\\Win81sdkbak3.rar"), 0))
    //        {
    //            Int    nRet = 0;
    //            size_t stRead = 0;
    //            while (stAll > stOkay)
    //            {
    //                afrs.Read(bBuf, 1024 * 1024);
    //                nRet = afrs.Result(stRead, true);
    //
    //                if (nRet == RET_OKAY)
    //                {
    //                    afrs.Seek((SeekPos)stRead);
    //                    stOkay += stRead;
    //                    afws.Write(bBuf, stRead);
    //
    //                    nRet = afws.Result(stRead, true);
    //                    if (nRet == RET_OKAY)
    //                    {
    //                        afws.Seek((SeekPos)stRead);
    //                        continue;
    //                    }
    //                }
    //                break;
    //            }
    //        }
    //    }
    //#endif
}

