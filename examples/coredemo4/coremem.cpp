#include "stdafx.h"
#include "mobject.h"
#include "coremem.h"

#include <vector>
#include <list>
#include <map>

class CTestMen : public MObject
{
public:
    CTestMen(UInt uCopy = 0)
        : m_uSizeCopy(uCopy)
    {
    }
    virtual ~CTestMen(void)
    {
    }
public:
    UInt   m_uSizeCopy;
};

void DemoMem(void)
{
    // C模式
    size_t stAlloc = 3LL * 1024LL * 1024LL;// *1024LL;
    PInt pTest = (PInt)ALLOC( stAlloc );
    if (pTest != nullptr)
    {
        DUMP(pTest);
        DEV_INFO(TF("1. memory check return %s"), CHECK(pTest) ? TF("Okay") : TF("Fail"));
        FREE( pTest );
    }

    // C++模式
    CTestMen* pMem = MNEW CTestMen;
    if (pMem != nullptr)
    {
        DUMP(pMem);
        DEV_INFO(TF("2. memory check return %s"), CHECK(pMem) ? TF("Okay") : TF("Fail"));
        MDELETE pMem;
    }

    // 缓存模式
    std::vector<PByte> vptr;
    PINDEX index = MObject::MCCreate(6000);
    for (Int i = 0; i < 1024; ++i)
    {
        vptr.push_back(MObject::MCAlloc(index));
    }
    //for (Int i = 0; i < 1024; ++i)
    //{
    //    MObject::MCFree(index, vptr[i]);
    //}
    MObject::MCDestroy(index); // 释放index导致vptr保存的所有内存块无效

    //// std allocator
    //std::vector<int, CTMAlloc<int>> vi;
    //std::list<short, CTMAlloc<short>> ls;
    //std::map<int, double, std::less<int>, CTMAlloc<std::pair<int, double>>> md;

    //double d1 = 1.0;

    //for (int i = 0; i < 100; ++i)
    //{
    //    vi.push_back(i);
    //    ls.push_back((short)i);
    //    md.insert(std::make_pair(i, i * d1));
    //}
}

