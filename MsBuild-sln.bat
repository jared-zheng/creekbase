﻿@ECHO OFF  
ECHO.  
ECHO    *************************************
ECHO    *  windows platform msbuild file  *
ECHO    *************************************

CD /d %~dp0
SET CurPath=%~dp0
ECHO "Current Path: %CurPath%"

SET VS14XCOMNTOOLS=D:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\

if not defined VS14XCOMNTOOLS goto BuildFailed

SET ComnToolsVer=%VS14XCOMNTOOLS%

call "%ComnToolsVer%VsMSBuildCmd.bat"

ECHO    *******************************************
ECHO    *  set msbuild args(architecture target)
ECHO    *  64 Bits Target(x64): x64
ECHO    *  32 Bits Target(x86): x86
ECHO    *******************************************

SET ArchTarget=x64
SET /P ArchTarget="Input Architecture Target(Default: %ArchTarget%):"
ECHO "Set Architecture Target: %ArchTarget%"

ECHO    ***********************************************************
ECHO    *  set msbuild args(C/C++ run-time library)
ECHO    *  use debug   version: Debug
ECHO    *  use release version: Release
ECHO    ***********************************************************

SET CRunTime=Debug
SET /P CRunTime="Input Run-time Library(Default: Debug):"
ECHO "Set Run-time Library: %CRunTime%"

ECHO    **********************************
ECHO    *  set msbuild args(DLL/LIB)
ECHO    *  DLL version(MT): CREEK
ECHO    *  DLL version(MD): CREEKmd
ECHO    *  LIB version(MT): CREEKlib
ECHO    *  LIB version(MD): CREEKmdlib
ECHO    **********************************

SET BuildSolution=CREEK
SET /P BuildSolution="Input DLL/LIB(Default: %BuildSolution%):"
ECHO "Set DLL/LIB: %BuildSolution%"

MSBuild %CurPath%\%BuildSolution%.sln /t:Rebuild /p:Platform=%ArchTarget% /p:Configuration=%CRunTime% -fl1 -fl2 -fl3 -flp1:logfile=BuildOutput.log;verbosity=normal -flp2:logfile=BuildErrors.log;errorsonly -flp3:logfile=BuildWarnings.log;warningsonly

ECHO=  
SET /P a="msbuild build completed, return to exit..."
exit

:BuildFailed

ECHO "environment variable VS14XCOMNTOOLS can not be detected, set local variable VS14XCOMNTOOLS as VS Native Tools Command Path!"
ECHO=  
SET /P a="msbuild build failed, return to exit..."
exit
 